"use strict";

/*! jQuery v1.8.3 jquery.com | jquery.org/license */
function _typeof(obj) {
  "@babel/helpers - typeof";
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj &&
        typeof Symbol === "function" &&
        obj.constructor === Symbol &&
        obj !== Symbol.prototype
        ? "symbol"
        : typeof obj;
    };
  }
  return _typeof(obj);
}

/*! jQuery v1.8.3 jquery.com | jquery.org/license */
(function (e, t) {
  function _(e) {
    var t = (M[e] = {});
    return (
      v.each(e.split(y), function (e, n) {
        t[n] = !0;
      }),
      t
    );
  }

  function H(e, n, r) {
    if (r === t && e.nodeType === 1) {
      var i = "data-" + n.replace(P, "-$1").toLowerCase();
      r = e.getAttribute(i);

      if (typeof r == "string") {
        try {
          r =
            r === "true"
              ? !0
              : r === "false"
              ? !1
              : r === "null"
              ? null
              : +r + "" === r
              ? +r
              : D.test(r)
              ? v.parseJSON(r)
              : r;
        } catch (s) {}

        v.data(e, n, r);
      } else r = t;
    }

    return r;
  }

  function B(e) {
    var t;

    for (t in e) {
      if (t === "data" && v.isEmptyObject(e[t])) continue;
      if (t !== "toJSON") return !1;
    }

    return !0;
  }

  function et() {
    return !1;
  }

  function tt() {
    return !0;
  }

  function ut(e) {
    return !e || !e.parentNode || e.parentNode.nodeType === 11;
  }

  function at(e, t) {
    do {
      e = e[t];
    } while (e && e.nodeType !== 1);

    return e;
  }

  function ft(e, t, n) {
    t = t || 0;
    if (v.isFunction(t))
      return v.grep(e, function (e, r) {
        var i = !!t.call(e, r, e);
        return i === n;
      });
    if (t.nodeType)
      return v.grep(e, function (e, r) {
        return (e === t) === n;
      });

    if (typeof t == "string") {
      var r = v.grep(e, function (e) {
        return e.nodeType === 1;
      });
      if (it.test(t)) return v.filter(t, r, !n);
      t = v.filter(t, r);
    }

    return v.grep(e, function (e, r) {
      return v.inArray(e, t) >= 0 === n;
    });
  }

  function lt(e) {
    var t = ct.split("|"),
      n = e.createDocumentFragment();
    if (n.createElement)
      while (t.length) {
        n.createElement(t.pop());
      }
    return n;
  }

  function Lt(e, t) {
    return (
      e.getElementsByTagName(t)[0] ||
      e.appendChild(e.ownerDocument.createElement(t))
    );
  }

  function At(e, t) {
    if (t.nodeType !== 1 || !v.hasData(e)) return;

    var n,
      r,
      i,
      s = v._data(e),
      o = v._data(t, s),
      u = s.events;

    if (u) {
      delete o.handle, (o.events = {});

      for (n in u) {
        for (r = 0, i = u[n].length; r < i; r++) {
          v.event.add(t, n, u[n][r]);
        }
      }
    }

    o.data && (o.data = v.extend({}, o.data));
  }

  function Ot(e, t) {
    var n;
    if (t.nodeType !== 1) return;
    t.clearAttributes && t.clearAttributes(),
      t.mergeAttributes && t.mergeAttributes(e),
      (n = t.nodeName.toLowerCase()),
      n === "object"
        ? (t.parentNode && (t.outerHTML = e.outerHTML),
          v.support.html5Clone &&
            e.innerHTML &&
            !v.trim(t.innerHTML) &&
            (t.innerHTML = e.innerHTML))
        : n === "input" && Et.test(e.type)
        ? ((t.defaultChecked = t.checked = e.checked),
          t.value !== e.value && (t.value = e.value))
        : n === "option"
        ? (t.selected = e.defaultSelected)
        : n === "input" || n === "textarea"
        ? (t.defaultValue = e.defaultValue)
        : n === "script" && t.text !== e.text && (t.text = e.text),
      t.removeAttribute(v.expando);
  }

  function Mt(e) {
    return typeof e.getElementsByTagName != "undefined"
      ? e.getElementsByTagName("*")
      : typeof e.querySelectorAll != "undefined"
      ? e.querySelectorAll("*")
      : [];
  }

  function _t(e) {
    Et.test(e.type) && (e.defaultChecked = e.checked);
  }

  function Qt(e, t) {
    if (t in e) return t;
    var n = t.charAt(0).toUpperCase() + t.slice(1),
      r = t,
      i = Jt.length;

    while (i--) {
      t = Jt[i] + n;
      if (t in e) return t;
    }

    return r;
  }

  function Gt(e, t) {
    return (
      (e = t || e),
      v.css(e, "display") === "none" || !v.contains(e.ownerDocument, e)
    );
  }

  function Yt(e, t) {
    var n,
      r,
      i = [],
      s = 0,
      o = e.length;

    for (; s < o; s++) {
      n = e[s];
      if (!n.style) continue;
      (i[s] = v._data(n, "olddisplay")),
        t
          ? (!i[s] && n.style.display === "none" && (n.style.display = ""),
            n.style.display === "" &&
              Gt(n) &&
              (i[s] = v._data(n, "olddisplay", nn(n.nodeName))))
          : ((r = Dt(n, "display")),
            !i[s] && r !== "none" && v._data(n, "olddisplay", r));
    }

    for (s = 0; s < o; s++) {
      n = e[s];
      if (!n.style) continue;
      if (!t || n.style.display === "none" || n.style.display === "")
        n.style.display = t ? i[s] || "" : "none";
    }

    return e;
  }

  function Zt(e, t, n) {
    var r = Rt.exec(t);
    return r ? Math.max(0, r[1] - (n || 0)) + (r[2] || "px") : t;
  }

  function en(e, t, n, r) {
    var i = n === (r ? "border" : "content") ? 4 : t === "width" ? 1 : 0,
      s = 0;

    for (; i < 4; i += 2) {
      n === "margin" && (s += v.css(e, n + $t[i], !0)),
        r
          ? (n === "content" &&
              (s -= parseFloat(Dt(e, "padding" + $t[i])) || 0),
            n !== "margin" &&
              (s -= parseFloat(Dt(e, "border" + $t[i] + "Width")) || 0))
          : ((s += parseFloat(Dt(e, "padding" + $t[i])) || 0),
            n !== "padding" &&
              (s += parseFloat(Dt(e, "border" + $t[i] + "Width")) || 0));
    }

    return s;
  }

  function tn(e, t, n) {
    var r = t === "width" ? e.offsetWidth : e.offsetHeight,
      i = !0,
      s = v.support.boxSizing && v.css(e, "boxSizing") === "border-box";

    if (r <= 0 || r == null) {
      r = Dt(e, t);
      if (r < 0 || r == null) r = e.style[t];
      if (Ut.test(r)) return r;
      (i = s && (v.support.boxSizingReliable || r === e.style[t])),
        (r = parseFloat(r) || 0);
    }

    return r + en(e, t, n || (s ? "border" : "content"), i) + "px";
  }

  function nn(e) {
    if (Wt[e]) return Wt[e];
    var t = v("<" + e + ">").appendTo(i.body),
      n = t.css("display");
    t.remove();

    if (n === "none" || n === "") {
      Pt = i.body.appendChild(
        Pt ||
          v.extend(i.createElement("iframe"), {
            frameBorder: 0,
            width: 0,
            height: 0,
          })
      );
      if (!Ht || !Pt.createElement)
        (Ht = (Pt.contentWindow || Pt.contentDocument).document),
          Ht.write("<!doctype html><html><body>"),
          Ht.close();
      (t = Ht.body.appendChild(Ht.createElement(e))),
        (n = Dt(t, "display")),
        i.body.removeChild(Pt);
    }

    return (Wt[e] = n), n;
  }

  function fn(e, t, n, r) {
    var i;
    if (v.isArray(t))
      v.each(t, function (t, i) {
        n || sn.test(e)
          ? r(e, i)
          : fn(e + "[" + (_typeof(i) == "object" ? t : "") + "]", i, n, r);
      });
    else if (!n && v.type(t) === "object")
      for (i in t) {
        fn(e + "[" + i + "]", t[i], n, r);
      }
    else r(e, t);
  }

  function Cn(e) {
    return function (t, n) {
      typeof t != "string" && ((n = t), (t = "*"));
      var r,
        i,
        s,
        o = t.toLowerCase().split(y),
        u = 0,
        a = o.length;
      if (v.isFunction(n))
        for (; u < a; u++) {
          (r = o[u]),
            (s = /^\+/.test(r)),
            s && (r = r.substr(1) || "*"),
            (i = e[r] = e[r] || []),
            i[s ? "unshift" : "push"](n);
        }
    };
  }

  function kn(e, n, r, i, s, o) {
    (s = s || n.dataTypes[0]), (o = o || {}), (o[s] = !0);
    var u,
      a = e[s],
      f = 0,
      l = a ? a.length : 0,
      c = e === Sn;

    for (; f < l && (c || !u); f++) {
      (u = a[f](n, r, i)),
        typeof u == "string" &&
          (!c || o[u]
            ? (u = t)
            : (n.dataTypes.unshift(u), (u = kn(e, n, r, i, u, o))));
    }

    return (c || !u) && !o["*"] && (u = kn(e, n, r, i, "*", o)), u;
  }

  function Ln(e, n) {
    var r,
      i,
      s = v.ajaxSettings.flatOptions || {};

    for (r in n) {
      n[r] !== t && ((s[r] ? e : i || (i = {}))[r] = n[r]);
    }

    i && v.extend(!0, e, i);
  }

  function An(e, n, r) {
    var i,
      s,
      o,
      u,
      a = e.contents,
      f = e.dataTypes,
      l = e.responseFields;

    for (s in l) {
      s in r && (n[l[s]] = r[s]);
    }

    while (f[0] === "*") {
      f.shift(),
        i === t && (i = e.mimeType || n.getResponseHeader("content-type"));
    }

    if (i)
      for (s in a) {
        if (a[s] && a[s].test(i)) {
          f.unshift(s);
          break;
        }
      }
    if (f[0] in r) o = f[0];
    else {
      for (s in r) {
        if (!f[0] || e.converters[s + " " + f[0]]) {
          o = s;
          break;
        }

        u || (u = s);
      }

      o = o || u;
    }
    if (o) return o !== f[0] && f.unshift(o), r[o];
  }

  function On(e, t) {
    var n,
      r,
      i,
      s,
      o = e.dataTypes.slice(),
      u = o[0],
      a = {},
      f = 0;
    e.dataFilter && (t = e.dataFilter(t, e.dataType));
    if (o[1])
      for (n in e.converters) {
        a[n.toLowerCase()] = e.converters[n];
      }

    for (; (i = o[++f]); ) {
      if (i !== "*") {
        if (u !== "*" && u !== i) {
          n = a[u + " " + i] || a["* " + i];
          if (!n)
            for (r in a) {
              s = r.split(" ");

              if (s[1] === i) {
                n = a[u + " " + s[0]] || a["* " + s[0]];

                if (n) {
                  n === !0
                    ? (n = a[r])
                    : a[r] !== !0 && ((i = s[0]), o.splice(f--, 0, i));
                  break;
                }
              }
            }
          if (n !== !0)
            if (n && e["throws"]) t = n(t);
            else
              try {
                t = n(t);
              } catch (l) {
                return {
                  state: "parsererror",
                  error: n ? l : "No conversion from " + u + " to " + i,
                };
              }
        }

        u = i;
      }
    }

    return {
      state: "success",
      data: t,
    };
  }

  function Fn() {
    try {
      return new e.XMLHttpRequest();
    } catch (t) {}
  }

  function In() {
    try {
      return new e.ActiveXObject("Microsoft.XMLHTTP");
    } catch (t) {}
  }

  function $n() {
    return (
      setTimeout(function () {
        qn = t;
      }, 0),
      (qn = v.now())
    );
  }

  function Jn(e, t) {
    v.each(t, function (t, n) {
      var r = (Vn[t] || []).concat(Vn["*"]),
        i = 0,
        s = r.length;

      for (; i < s; i++) {
        if (r[i].call(e, t, n)) return;
      }
    });
  }

  function Kn(e, t, n) {
    var r,
      i = 0,
      s = 0,
      o = Xn.length,
      u = v.Deferred().always(function () {
        delete a.elem;
      }),
      a = function a() {
        var t = qn || $n(),
          n = Math.max(0, f.startTime + f.duration - t),
          r = n / f.duration || 0,
          i = 1 - r,
          s = 0,
          o = f.tweens.length;

        for (; s < o; s++) {
          f.tweens[s].run(i);
        }

        return (
          u.notifyWith(e, [f, i, n]),
          i < 1 && o ? n : (u.resolveWith(e, [f]), !1)
        );
      },
      f = u.promise({
        elem: e,
        props: v.extend({}, t),
        opts: v.extend(
          !0,
          {
            specialEasing: {},
          },
          n
        ),
        originalProperties: t,
        originalOptions: n,
        startTime: qn || $n(),
        duration: n.duration,
        tweens: [],
        createTween: function createTween(t, n, r) {
          var i = v.Tween(
            e,
            f.opts,
            t,
            n,
            f.opts.specialEasing[t] || f.opts.easing
          );
          return f.tweens.push(i), i;
        },
        stop: function stop(t) {
          var n = 0,
            r = t ? f.tweens.length : 0;

          for (; n < r; n++) {
            f.tweens[n].run(1);
          }

          return t ? u.resolveWith(e, [f, t]) : u.rejectWith(e, [f, t]), this;
        },
      }),
      l = f.props;

    Qn(l, f.opts.specialEasing);

    for (; i < o; i++) {
      r = Xn[i].call(f, e, l, f.opts);
      if (r) return r;
    }

    return (
      Jn(f, l),
      v.isFunction(f.opts.start) && f.opts.start.call(e, f),
      v.fx.timer(
        v.extend(a, {
          anim: f,
          queue: f.opts.queue,
          elem: e,
        })
      ),
      f
        .progress(f.opts.progress)
        .done(f.opts.done, f.opts.complete)
        .fail(f.opts.fail)
        .always(f.opts.always)
    );
  }

  function Qn(e, t) {
    var n, r, i, s, o;

    for (n in e) {
      (r = v.camelCase(n)),
        (i = t[r]),
        (s = e[n]),
        v.isArray(s) && ((i = s[1]), (s = e[n] = s[0])),
        n !== r && ((e[r] = s), delete e[n]),
        (o = v.cssHooks[r]);

      if (o && "expand" in o) {
        (s = o.expand(s)), delete e[r];

        for (n in s) {
          n in e || ((e[n] = s[n]), (t[n] = i));
        }
      } else t[r] = i;
    }
  }

  function Gn(e, t, n) {
    var r,
      i,
      s,
      o,
      u,
      a,
      f,
      l,
      c,
      h = this,
      p = e.style,
      d = {},
      m = [],
      g = e.nodeType && Gt(e);
    n.queue ||
      ((l = v._queueHooks(e, "fx")),
      l.unqueued == null &&
        ((l.unqueued = 0),
        (c = l.empty.fire),
        (l.empty.fire = function () {
          l.unqueued || c();
        })),
      l.unqueued++,
      h.always(function () {
        h.always(function () {
          l.unqueued--, v.queue(e, "fx").length || l.empty.fire();
        });
      })),
      e.nodeType === 1 &&
        ("height" in t || "width" in t) &&
        ((n.overflow = [p.overflow, p.overflowX, p.overflowY]),
        v.css(e, "display") === "inline" &&
          v.css(e, "float") === "none" &&
          (!v.support.inlineBlockNeedsLayout || nn(e.nodeName) === "inline"
            ? (p.display = "inline-block")
            : (p.zoom = 1))),
      n.overflow &&
        ((p.overflow = "hidden"),
        v.support.shrinkWrapBlocks ||
          h.done(function () {
            (p.overflow = n.overflow[0]),
              (p.overflowX = n.overflow[1]),
              (p.overflowY = n.overflow[2]);
          }));

    for (r in t) {
      s = t[r];

      if (Un.exec(s)) {
        delete t[r], (a = a || s === "toggle");
        if (s === (g ? "hide" : "show")) continue;
        m.push(r);
      }
    }

    o = m.length;

    if (o) {
      (u = v._data(e, "fxshow") || v._data(e, "fxshow", {})),
        "hidden" in u && (g = u.hidden),
        a && (u.hidden = !g),
        g
          ? v(e).show()
          : h.done(function () {
              v(e).hide();
            }),
        h.done(function () {
          var t;
          v.removeData(e, "fxshow", !0);

          for (t in d) {
            v.style(e, t, d[t]);
          }
        });

      for (r = 0; r < o; r++) {
        (i = m[r]),
          (f = h.createTween(i, g ? u[i] : 0)),
          (d[i] = u[i] || v.style(e, i)),
          i in u ||
            ((u[i] = f.start),
            g &&
              ((f.end = f.start),
              (f.start = i === "width" || i === "height" ? 1 : 0)));
      }
    }
  }

  function Yn(e, t, n, r, i) {
    return new Yn.prototype.init(e, t, n, r, i);
  }

  function Zn(e, t) {
    var n,
      r = {
        height: e,
      },
      i = 0;
    t = t ? 1 : 0;

    for (; i < 4; i += 2 - t) {
      (n = $t[i]), (r["margin" + n] = r["padding" + n] = e);
    }

    return t && (r.opacity = r.width = e), r;
  }

  function tr(e) {
    return v.isWindow(e)
      ? e
      : e.nodeType === 9
      ? e.defaultView || e.parentWindow
      : !1;
  }

  var n,
    r,
    i = e.document,
    s = e.location,
    o = e.navigator,
    u = e.jQuery,
    a = e.$,
    f = Array.prototype.push,
    l = Array.prototype.slice,
    c = Array.prototype.indexOf,
    h = Object.prototype.toString,
    p = Object.prototype.hasOwnProperty,
    d = String.prototype.trim,
    v = function v(e, t) {
      return new v.fn.init(e, t, n);
    },
    m = /[\-+]?(?:\d*\.|)\d+(?:[eE][\-+]?\d+|)/.source,
    g = /\S/,
    y = /\s+/,
    b = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
    w = /^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/,
    E = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
    S = /^[\],:{}\s]*$/,
    x = /(?:^|:|,)(?:\s*\[)+/g,
    T = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,
    N = /"[^"\\\r\n]*"|true|false|null|-?(?:\d\d*\.|)\d+(?:[eE][\-+]?\d+|)/g,
    C = /^-ms-/,
    k = /-([\da-z])/gi,
    L = function L(e, t) {
      return (t + "").toUpperCase();
    },
    A = function A() {
      i.addEventListener
        ? (i.removeEventListener("DOMContentLoaded", A, !1), v.ready())
        : i.readyState === "complete" &&
          (i.detachEvent("onreadystatechange", A), v.ready());
    },
    O = {};

  (v.fn = v.prototype = {
    constructor: v,
    init: function init(e, n, r) {
      var s, o, u, a;
      if (!e) return this;
      if (e.nodeType)
        return (this.context = this[0] = e), (this.length = 1), this;

      if (typeof e == "string") {
        e.charAt(0) === "<" && e.charAt(e.length - 1) === ">" && e.length >= 3
          ? (s = [null, e, null])
          : (s = w.exec(e));

        if (s && (s[1] || !n)) {
          if (s[1])
            return (
              (n = n instanceof v ? n[0] : n),
              (a = n && n.nodeType ? n.ownerDocument || n : i),
              (e = v.parseHTML(s[1], a, !0)),
              E.test(s[1]) && v.isPlainObject(n) && this.attr.call(e, n, !0),
              v.merge(this, e)
            );
          o = i.getElementById(s[2]);

          if (o && o.parentNode) {
            if (o.id !== s[2]) return r.find(e);
            (this.length = 1), (this[0] = o);
          }

          return (this.context = i), (this.selector = e), this;
        }

        return !n || n.jquery ? (n || r).find(e) : this.constructor(n).find(e);
      }

      return v.isFunction(e)
        ? r.ready(e)
        : (e.selector !== t &&
            ((this.selector = e.selector), (this.context = e.context)),
          v.makeArray(e, this));
    },
    selector: "",
    jquery: "1.8.3",
    length: 0,
    size: function size() {
      return this.length;
    },
    toArray: function toArray() {
      return l.call(this);
    },
    get: function get(e) {
      return e == null
        ? this.toArray()
        : e < 0
        ? this[this.length + e]
        : this[e];
    },
    pushStack: function pushStack(e, t, n) {
      var r = v.merge(this.constructor(), e);
      return (
        (r.prevObject = this),
        (r.context = this.context),
        t === "find"
          ? (r.selector = this.selector + (this.selector ? " " : "") + n)
          : t && (r.selector = this.selector + "." + t + "(" + n + ")"),
        r
      );
    },
    each: function each(e, t) {
      return v.each(this, e, t);
    },
    ready: function ready(e) {
      return v.ready.promise().done(e), this;
    },
    eq: function eq(e) {
      return (e = +e), e === -1 ? this.slice(e) : this.slice(e, e + 1);
    },
    first: function first() {
      return this.eq(0);
    },
    last: function last() {
      return this.eq(-1);
    },
    slice: function slice() {
      return this.pushStack(
        l.apply(this, arguments),
        "slice",
        l.call(arguments).join(",")
      );
    },
    map: function map(e) {
      return this.pushStack(
        v.map(this, function (t, n) {
          return e.call(t, n, t);
        })
      );
    },
    end: function end() {
      return this.prevObject || this.constructor(null);
    },
    push: f,
    sort: [].sort,
    splice: [].splice,
  }),
    (v.fn.init.prototype = v.fn),
    (v.extend = v.fn.extend = function () {
      var e,
        n,
        r,
        i,
        s,
        o,
        u = arguments[0] || {},
        a = 1,
        f = arguments.length,
        l = !1;
      typeof u == "boolean" && ((l = u), (u = arguments[1] || {}), (a = 2)),
        _typeof(u) != "object" && !v.isFunction(u) && (u = {}),
        f === a && ((u = this), --a);

      for (; a < f; a++) {
        if ((e = arguments[a]) != null)
          for (n in e) {
            (r = u[n]), (i = e[n]);
            if (u === i) continue;
            l && i && (v.isPlainObject(i) || (s = v.isArray(i)))
              ? (s
                  ? ((s = !1), (o = r && v.isArray(r) ? r : []))
                  : (o = r && v.isPlainObject(r) ? r : {}),
                (u[n] = v.extend(l, o, i)))
              : i !== t && (u[n] = i);
          }
      }

      return u;
    }),
    v.extend({
      noConflict: function noConflict(t) {
        return e.$ === v && (e.$ = a), t && e.jQuery === v && (e.jQuery = u), v;
      },
      isReady: !1,
      readyWait: 1,
      holdReady: function holdReady(e) {
        e ? v.readyWait++ : v.ready(!0);
      },
      ready: function ready(e) {
        if (e === !0 ? --v.readyWait : v.isReady) return;
        if (!i.body) return setTimeout(v.ready, 1);
        v.isReady = !0;
        if (e !== !0 && --v.readyWait > 0) return;
        r.resolveWith(i, [v]),
          v.fn.trigger && v(i).trigger("ready").off("ready");
      },
      isFunction: function isFunction(e) {
        return v.type(e) === "function";
      },
      isArray:
        Array.isArray ||
        function (e) {
          return v.type(e) === "array";
        },
      isWindow: function isWindow(e) {
        return e != null && e == e.window;
      },
      isNumeric: function isNumeric(e) {
        return !isNaN(parseFloat(e)) && isFinite(e);
      },
      type: function type(e) {
        return e == null ? String(e) : O[h.call(e)] || "object";
      },
      isPlainObject: function isPlainObject(e) {
        if (!e || v.type(e) !== "object" || e.nodeType || v.isWindow(e))
          return !1;

        try {
          if (
            e.constructor &&
            !p.call(e, "constructor") &&
            !p.call(e.constructor.prototype, "isPrototypeOf")
          )
            return !1;
        } catch (n) {
          return !1;
        }

        var r;

        for (r in e) {
        }

        return r === t || p.call(e, r);
      },
      isEmptyObject: function isEmptyObject(e) {
        var t;

        for (t in e) {
          return !1;
        }

        return !0;
      },
      error: function error(e) {
        throw new Error(e);
      },
      parseHTML: function parseHTML(e, t, n) {
        var r;
        return !e || typeof e != "string"
          ? null
          : (typeof t == "boolean" && ((n = t), (t = 0)),
            (t = t || i),
            (r = E.exec(e))
              ? [t.createElement(r[1])]
              : ((r = v.buildFragment([e], t, n ? null : [])),
                v.merge(
                  [],
                  (r.cacheable ? v.clone(r.fragment) : r.fragment).childNodes
                )));
      },
      parseJSON: function parseJSON(t) {
        if (!t || typeof t != "string") return null;
        t = v.trim(t);
        if (e.JSON && e.JSON.parse) return e.JSON.parse(t);
        if (S.test(t.replace(T, "@").replace(N, "]").replace(x, "")))
          return new Function("return " + t)();
        v.error("Invalid JSON: " + t);
      },
      parseXML: function parseXML(n) {
        var r, i;
        if (!n || typeof n != "string") return null;

        try {
          e.DOMParser
            ? ((i = new DOMParser()), (r = i.parseFromString(n, "text/xml")))
            : ((r = new ActiveXObject("Microsoft.XMLDOM")),
              (r.async = "false"),
              r.loadXML(n));
        } catch (s) {
          r = t;
        }

        return (
          (!r ||
            !r.documentElement ||
            r.getElementsByTagName("parsererror").length) &&
            v.error("Invalid XML: " + n),
          r
        );
      },
      noop: function noop() {},
      globalEval: function globalEval(t) {
        t &&
          g.test(t) &&
          (
            e.execScript ||
            function (t) {
              e.eval.call(e, t);
            }
          )(t);
      },
      camelCase: function camelCase(e) {
        return e.replace(C, "ms-").replace(k, L);
      },
      nodeName: function nodeName(e, t) {
        return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase();
      },
      each: function each(e, n, r) {
        var i,
          s = 0,
          o = e.length,
          u = o === t || v.isFunction(e);

        if (r) {
          if (u) {
            for (i in e) {
              if (n.apply(e[i], r) === !1) break;
            }
          } else
            for (; s < o; ) {
              if (n.apply(e[s++], r) === !1) break;
            }
        } else if (u) {
          for (i in e) {
            if (n.call(e[i], i, e[i]) === !1) break;
          }
        } else
          for (; s < o; ) {
            if (n.call(e[s], s, e[s++]) === !1) break;
          }

        return e;
      },
      trim:
        d && !d.call("\uFEFF\xA0")
          ? function (e) {
              return e == null ? "" : d.call(e);
            }
          : function (e) {
              return e == null ? "" : (e + "").replace(b, "");
            },
      makeArray: function makeArray(e, t) {
        var n,
          r = t || [];
        return (
          e != null &&
            ((n = v.type(e)),
            e.length == null ||
            n === "string" ||
            n === "function" ||
            n === "regexp" ||
            v.isWindow(e)
              ? f.call(r, e)
              : v.merge(r, e)),
          r
        );
      },
      inArray: function inArray(e, t, n) {
        var r;

        if (t) {
          if (c) return c.call(t, e, n);
          (r = t.length), (n = n ? (n < 0 ? Math.max(0, r + n) : n) : 0);

          for (; n < r; n++) {
            if (n in t && t[n] === e) return n;
          }
        }

        return -1;
      },
      merge: function merge(e, n) {
        var r = n.length,
          i = e.length,
          s = 0;
        if (typeof r == "number")
          for (; s < r; s++) {
            e[i++] = n[s];
          }
        else
          while (n[s] !== t) {
            e[i++] = n[s++];
          }
        return (e.length = i), e;
      },
      grep: function grep(e, t, n) {
        var r,
          i = [],
          s = 0,
          o = e.length;
        n = !!n;

        for (; s < o; s++) {
          (r = !!t(e[s], s)), n !== r && i.push(e[s]);
        }

        return i;
      },
      map: function map(e, n, r) {
        var i,
          s,
          o = [],
          u = 0,
          a = e.length,
          f =
            e instanceof v ||
            (a !== t &&
              typeof a == "number" &&
              ((a > 0 && e[0] && e[a - 1]) || a === 0 || v.isArray(e)));
        if (f)
          for (; u < a; u++) {
            (i = n(e[u], u, r)), i != null && (o[o.length] = i);
          }
        else
          for (s in e) {
            (i = n(e[s], s, r)), i != null && (o[o.length] = i);
          }
        return o.concat.apply([], o);
      },
      guid: 1,
      proxy: function proxy(e, n) {
        var r, i, s;
        return (
          typeof n == "string" && ((r = e[n]), (n = e), (e = r)),
          v.isFunction(e)
            ? ((i = l.call(arguments, 2)),
              (s = function s() {
                return e.apply(n, i.concat(l.call(arguments)));
              }),
              (s.guid = e.guid = e.guid || v.guid++),
              s)
            : t
        );
      },
      access: function access(e, n, r, i, s, o, u) {
        var a,
          f = r == null,
          l = 0,
          c = e.length;

        if (r && _typeof(r) == "object") {
          for (l in r) {
            v.access(e, n, l, r[l], 1, o, i);
          }

          s = 1;
        } else if (i !== t) {
          (a = u === t && v.isFunction(i)),
            f &&
              (a
                ? ((a = n),
                  (n = function n(e, t, _n2) {
                    return a.call(v(e), _n2);
                  }))
                : (n.call(e, i), (n = null)));
          if (n)
            for (; l < c; l++) {
              n(e[l], r, a ? i.call(e[l], l, n(e[l], r)) : i, u);
            }
          s = 1;
        }

        return s ? e : f ? n.call(e) : c ? n(e[0], r) : o;
      },
      now: function now() {
        return new Date().getTime();
      },
    }),
    (v.ready.promise = function (t) {
      if (!r) {
        r = v.Deferred();
        if (i.readyState === "complete") setTimeout(v.ready, 1);
        else if (i.addEventListener)
          i.addEventListener("DOMContentLoaded", A, !1),
            e.addEventListener("load", v.ready, !1);
        else {
          i.attachEvent("onreadystatechange", A),
            e.attachEvent("onload", v.ready);
          var n = !1;

          try {
            n = e.frameElement == null && i.documentElement;
          } catch (s) {}

          n &&
            n.doScroll &&
            (function o() {
              if (!v.isReady) {
                try {
                  n.doScroll("left");
                } catch (e) {
                  return setTimeout(o, 50);
                }

                v.ready();
              }
            })();
        }
      }

      return r.promise(t);
    }),
    v.each(
      "Boolean Number String Function Array Date RegExp Object".split(" "),
      function (e, t) {
        O["[object " + t + "]"] = t.toLowerCase();
      }
    ),
    (n = v(i));
  var M = {};
  (v.Callbacks = function (e) {
    e = typeof e == "string" ? M[e] || _(e) : v.extend({}, e);

    var n,
      r,
      i,
      s,
      o,
      u,
      a = [],
      f = !e.once && [],
      l = function l(t) {
        (n = e.memory && t),
          (r = !0),
          (u = s || 0),
          (s = 0),
          (o = a.length),
          (i = !0);

        for (; a && u < o; u++) {
          if (a[u].apply(t[0], t[1]) === !1 && e.stopOnFalse) {
            n = !1;
            break;
          }
        }

        (i = !1),
          a && (f ? f.length && l(f.shift()) : n ? (a = []) : c.disable());
      },
      c = {
        add: function add() {
          if (a) {
            var t = a.length;
            (function r(t) {
              v.each(t, function (t, n) {
                var i = v.type(n);
                i === "function"
                  ? (!e.unique || !c.has(n)) && a.push(n)
                  : n && n.length && i !== "string" && r(n);
              });
            })(arguments),
              i ? (o = a.length) : n && ((s = t), l(n));
          }

          return this;
        },
        remove: function remove() {
          return (
            a &&
              v.each(arguments, function (e, t) {
                var n;

                while ((n = v.inArray(t, a, n)) > -1) {
                  a.splice(n, 1), i && (n <= o && o--, n <= u && u--);
                }
              }),
            this
          );
        },
        has: function has(e) {
          return v.inArray(e, a) > -1;
        },
        empty: function empty() {
          return (a = []), this;
        },
        disable: function disable() {
          return (a = f = n = t), this;
        },
        disabled: function disabled() {
          return !a;
        },
        lock: function lock() {
          return (f = t), n || c.disable(), this;
        },
        locked: function locked() {
          return !f;
        },
        fireWith: function fireWith(e, t) {
          return (
            (t = t || []),
            (t = [e, t.slice ? t.slice() : t]),
            a && (!r || f) && (i ? f.push(t) : l(t)),
            this
          );
        },
        fire: function fire() {
          return c.fireWith(this, arguments), this;
        },
        fired: function fired() {
          return !!r;
        },
      };

    return c;
  }),
    v.extend({
      Deferred: function Deferred(e) {
        var t = [
            ["resolve", "done", v.Callbacks("once memory"), "resolved"],
            ["reject", "fail", v.Callbacks("once memory"), "rejected"],
            ["notify", "progress", v.Callbacks("memory")],
          ],
          n = "pending",
          r = {
            state: function state() {
              return n;
            },
            always: function always() {
              return i.done(arguments).fail(arguments), this;
            },
            then: function then() {
              var e = arguments;
              return v
                .Deferred(function (n) {
                  v.each(t, function (t, r) {
                    var s = r[0],
                      o = e[t];
                    i[r[1]](
                      v.isFunction(o)
                        ? function () {
                            var e = o.apply(this, arguments);
                            e && v.isFunction(e.promise)
                              ? e
                                  .promise()
                                  .done(n.resolve)
                                  .fail(n.reject)
                                  .progress(n.notify)
                              : n[s + "With"](this === i ? n : this, [e]);
                          }
                        : n[s]
                    );
                  }),
                    (e = null);
                })
                .promise();
            },
            promise: function promise(e) {
              return e != null ? v.extend(e, r) : r;
            },
          },
          i = {};
        return (
          (r.pipe = r.then),
          v.each(t, function (e, s) {
            var o = s[2],
              u = s[3];
            (r[s[1]] = o.add),
              u &&
                o.add(
                  function () {
                    n = u;
                  },
                  t[e ^ 1][2].disable,
                  t[2][2].lock
                ),
              (i[s[0]] = o.fire),
              (i[s[0] + "With"] = o.fireWith);
          }),
          r.promise(i),
          e && e.call(i, i),
          i
        );
      },
      when: function when(e) {
        var t = 0,
          n = l.call(arguments),
          r = n.length,
          i = r !== 1 || (e && v.isFunction(e.promise)) ? r : 0,
          s = i === 1 ? e : v.Deferred(),
          o = function o(e, t, n) {
            return function (r) {
              (t[e] = this),
                (n[e] = arguments.length > 1 ? l.call(arguments) : r),
                n === u ? s.notifyWith(t, n) : --i || s.resolveWith(t, n);
            };
          },
          u,
          a,
          f;

        if (r > 1) {
          (u = new Array(r)), (a = new Array(r)), (f = new Array(r));

          for (; t < r; t++) {
            n[t] && v.isFunction(n[t].promise)
              ? n[t]
                  .promise()
                  .done(o(t, f, n))
                  .fail(s.reject)
                  .progress(o(t, a, u))
              : --i;
          }
        }

        return i || s.resolveWith(f, n), s.promise();
      },
    }),
    (v.support = (function () {
      var t,
        n,
        r,
        s,
        o,
        u,
        a,
        f,
        l,
        c,
        h,
        p = i.createElement("div");
      p.setAttribute("className", "t"),
        (p.innerHTML =
          "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>"),
        (n = p.getElementsByTagName("*")),
        (r = p.getElementsByTagName("a")[0]);
      if (!n || !r || !n.length) return {};
      (s = i.createElement("select")),
        (o = s.appendChild(i.createElement("option"))),
        (u = p.getElementsByTagName("input")[0]),
        (r.style.cssText = "top:1px;float:left;opacity:.5"),
        (t = {
          leadingWhitespace: p.firstChild.nodeType === 3,
          tbody: !p.getElementsByTagName("tbody").length,
          htmlSerialize: !!p.getElementsByTagName("link").length,
          style: /top/.test(r.getAttribute("style")),
          hrefNormalized: r.getAttribute("href") === "/a",
          opacity: /^0.5/.test(r.style.opacity),
          cssFloat: !!r.style.cssFloat,
          checkOn: u.value === "on",
          optSelected: o.selected,
          getSetAttribute: p.className !== "t",
          enctype: !!i.createElement("form").enctype,
          html5Clone:
            i.createElement("nav").cloneNode(!0).outerHTML !== "<:nav></:nav>",
          boxModel: i.compatMode === "CSS1Compat",
          submitBubbles: !0,
          changeBubbles: !0,
          focusinBubbles: !1,
          deleteExpando: !0,
          noCloneEvent: !0,
          inlineBlockNeedsLayout: !1,
          shrinkWrapBlocks: !1,
          reliableMarginRight: !0,
          boxSizingReliable: !0,
          pixelPosition: !1,
        }),
        (u.checked = !0),
        (t.noCloneChecked = u.cloneNode(!0).checked),
        (s.disabled = !0),
        (t.optDisabled = !o.disabled);

      try {
        delete p.test;
      } catch (d) {
        t.deleteExpando = !1;
      }

      !p.addEventListener &&
        p.attachEvent &&
        p.fireEvent &&
        (p.attachEvent(
          "onclick",
          (h = function h() {
            t.noCloneEvent = !1;
          })
        ),
        p.cloneNode(!0).fireEvent("onclick"),
        p.detachEvent("onclick", h)),
        (u = i.createElement("input")),
        (u.value = "t"),
        u.setAttribute("type", "radio"),
        (t.radioValue = u.value === "t"),
        u.setAttribute("checked", "checked"),
        u.setAttribute("name", "t"),
        p.appendChild(u),
        (a = i.createDocumentFragment()),
        a.appendChild(p.lastChild),
        (t.checkClone = a.cloneNode(!0).cloneNode(!0).lastChild.checked),
        (t.appendChecked = u.checked),
        a.removeChild(u),
        a.appendChild(p);
      if (p.attachEvent)
        for (l in {
          submit: !0,
          change: !0,
          focusin: !0,
        }) {
          (f = "on" + l),
            (c = f in p),
            c ||
              (p.setAttribute(f, "return;"), (c = typeof p[f] == "function")),
            (t[l + "Bubbles"] = c);
        }
      return (
        v(function () {
          var n,
            r,
            s,
            o,
            u = "padding:0;margin:0;border:0;display:block;overflow:hidden;",
            a = i.getElementsByTagName("body")[0];
          if (!a) return;
          (n = i.createElement("div")),
            (n.style.cssText =
              "visibility:hidden;border:0;width:0;height:0;position:static;top:0;margin-top:1px"),
            a.insertBefore(n, a.firstChild),
            (r = i.createElement("div")),
            n.appendChild(r),
            (r.innerHTML = "<table><tr><td></td><td>t</td></tr></table>"),
            (s = r.getElementsByTagName("td")),
            (s[0].style.cssText = "padding:0;margin:0;border:0;display:none"),
            (c = s[0].offsetHeight === 0),
            (s[0].style.display = ""),
            (s[1].style.display = "none"),
            (t.reliableHiddenOffsets = c && s[0].offsetHeight === 0),
            (r.innerHTML = ""),
            (r.style.cssText =
              "box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;"),
            (t.boxSizing = r.offsetWidth === 4),
            (t.doesNotIncludeMarginInBodyOffset = a.offsetTop !== 1),
            e.getComputedStyle &&
              ((t.pixelPosition =
                (e.getComputedStyle(r, null) || {}).top !== "1%"),
              (t.boxSizingReliable =
                (
                  e.getComputedStyle(r, null) || {
                    width: "4px",
                  }
                ).width === "4px"),
              (o = i.createElement("div")),
              (o.style.cssText = r.style.cssText = u),
              (o.style.marginRight = o.style.width = "0"),
              (r.style.width = "1px"),
              r.appendChild(o),
              (t.reliableMarginRight = !parseFloat(
                (e.getComputedStyle(o, null) || {}).marginRight
              ))),
            typeof r.style.zoom != "undefined" &&
              ((r.innerHTML = ""),
              (r.style.cssText =
                u + "width:1px;padding:1px;display:inline;zoom:1"),
              (t.inlineBlockNeedsLayout = r.offsetWidth === 3),
              (r.style.display = "block"),
              (r.style.overflow = "visible"),
              (r.innerHTML = "<div></div>"),
              (r.firstChild.style.width = "5px"),
              (t.shrinkWrapBlocks = r.offsetWidth !== 3),
              (n.style.zoom = 1)),
            a.removeChild(n),
            (n = r = s = o = null);
        }),
        a.removeChild(p),
        (n = r = s = o = u = a = p = null),
        t
      );
    })());
  var D = /(?:\{[\s\S]*\}|\[[\s\S]*\])$/,
    P = /([A-Z])/g;
  v.extend({
    cache: {},
    deletedIds: [],
    uuid: 0,
    expando: "jQuery" + (v.fn.jquery + Math.random()).replace(/\D/g, ""),
    noData: {
      embed: !0,
      object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
      applet: !0,
    },
    hasData: function hasData(e) {
      return (
        (e = e.nodeType ? v.cache[e[v.expando]] : e[v.expando]), !!e && !B(e)
      );
    },
    data: function data(e, n, r, i) {
      if (!v.acceptData(e)) return;
      var s,
        o,
        u = v.expando,
        a = typeof n == "string",
        f = e.nodeType,
        l = f ? v.cache : e,
        c = f ? e[u] : e[u] && u;
      if ((!c || !l[c] || (!i && !l[c].data)) && a && r === t) return;
      c || (f ? (e[u] = c = v.deletedIds.pop() || v.guid++) : (c = u)),
        l[c] || ((l[c] = {}), f || (l[c].toJSON = v.noop));
      if (_typeof(n) == "object" || typeof n == "function")
        i ? (l[c] = v.extend(l[c], n)) : (l[c].data = v.extend(l[c].data, n));
      return (
        (s = l[c]),
        i || (s.data || (s.data = {}), (s = s.data)),
        r !== t && (s[v.camelCase(n)] = r),
        a ? ((o = s[n]), o == null && (o = s[v.camelCase(n)])) : (o = s),
        o
      );
    },
    removeData: function removeData(e, t, n) {
      if (!v.acceptData(e)) return;
      var r,
        i,
        s,
        o = e.nodeType,
        u = o ? v.cache : e,
        a = o ? e[v.expando] : v.expando;
      if (!u[a]) return;

      if (t) {
        r = n ? u[a] : u[a].data;

        if (r) {
          v.isArray(t) ||
            (t in r
              ? (t = [t])
              : ((t = v.camelCase(t)),
                t in r ? (t = [t]) : (t = t.split(" "))));

          for (i = 0, s = t.length; i < s; i++) {
            delete r[t[i]];
          }

          if (!(n ? B : v.isEmptyObject)(r)) return;
        }
      }

      if (!n) {
        delete u[a].data;
        if (!B(u[a])) return;
      }

      o
        ? v.cleanData([e], !0)
        : v.support.deleteExpando || u != u.window
        ? delete u[a]
        : (u[a] = null);
    },
    _data: function _data(e, t, n) {
      return v.data(e, t, n, !0);
    },
    acceptData: function acceptData(e) {
      var t = e.nodeName && v.noData[e.nodeName.toLowerCase()];
      return !t || (t !== !0 && e.getAttribute("classid") === t);
    },
  }),
    v.fn.extend({
      data: function data(e, n) {
        var r,
          i,
          s,
          o,
          u,
          a = this[0],
          f = 0,
          l = null;

        if (e === t) {
          if (this.length) {
            l = v.data(a);

            if (a.nodeType === 1 && !v._data(a, "parsedAttrs")) {
              s = a.attributes;

              for (u = s.length; f < u; f++) {
                (o = s[f].name),
                  o.indexOf("data-") ||
                    ((o = v.camelCase(o.substring(5))), H(a, o, l[o]));
              }

              v._data(a, "parsedAttrs", !0);
            }
          }

          return l;
        }

        return _typeof(e) == "object"
          ? this.each(function () {
              v.data(this, e);
            })
          : ((r = e.split(".", 2)),
            (r[1] = r[1] ? "." + r[1] : ""),
            (i = r[1] + "!"),
            v.access(
              this,
              function (n) {
                if (n === t)
                  return (
                    (l = this.triggerHandler("getData" + i, [r[0]])),
                    l === t && a && ((l = v.data(a, e)), (l = H(a, e, l))),
                    l === t && r[1] ? this.data(r[0]) : l
                  );
                (r[1] = n),
                  this.each(function () {
                    var t = v(this);
                    t.triggerHandler("setData" + i, r),
                      v.data(this, e, n),
                      t.triggerHandler("changeData" + i, r);
                  });
              },
              null,
              n,
              arguments.length > 1,
              null,
              !1
            ));
      },
      removeData: function removeData(e) {
        return this.each(function () {
          v.removeData(this, e);
        });
      },
    }),
    v.extend({
      queue: function queue(e, t, n) {
        var r;
        if (e)
          return (
            (t = (t || "fx") + "queue"),
            (r = v._data(e, t)),
            n &&
              (!r || v.isArray(n)
                ? (r = v._data(e, t, v.makeArray(n)))
                : r.push(n)),
            r || []
          );
      },
      dequeue: function dequeue(e, t) {
        t = t || "fx";

        var n = v.queue(e, t),
          r = n.length,
          i = n.shift(),
          s = v._queueHooks(e, t),
          o = function o() {
            v.dequeue(e, t);
          };

        i === "inprogress" && ((i = n.shift()), r--),
          i &&
            (t === "fx" && n.unshift("inprogress"),
            delete s.stop,
            i.call(e, o, s)),
          !r && s && s.empty.fire();
      },
      _queueHooks: function _queueHooks(e, t) {
        var n = t + "queueHooks";
        return (
          v._data(e, n) ||
          v._data(e, n, {
            empty: v.Callbacks("once memory").add(function () {
              v.removeData(e, t + "queue", !0), v.removeData(e, n, !0);
            }),
          })
        );
      },
    }),
    v.fn.extend({
      queue: function queue(e, n) {
        var r = 2;
        return (
          typeof e != "string" && ((n = e), (e = "fx"), r--),
          arguments.length < r
            ? v.queue(this[0], e)
            : n === t
            ? this
            : this.each(function () {
                var t = v.queue(this, e, n);
                v._queueHooks(this, e),
                  e === "fx" && t[0] !== "inprogress" && v.dequeue(this, e);
              })
        );
      },
      dequeue: function dequeue(e) {
        return this.each(function () {
          v.dequeue(this, e);
        });
      },
      delay: function delay(e, t) {
        return (
          (e = v.fx ? v.fx.speeds[e] || e : e),
          (t = t || "fx"),
          this.queue(t, function (t, n) {
            var r = setTimeout(t, e);

            n.stop = function () {
              clearTimeout(r);
            };
          })
        );
      },
      clearQueue: function clearQueue(e) {
        return this.queue(e || "fx", []);
      },
      promise: function promise(e, n) {
        var r,
          i = 1,
          s = v.Deferred(),
          o = this,
          u = this.length,
          a = function a() {
            --i || s.resolveWith(o, [o]);
          };

        typeof e != "string" && ((n = e), (e = t)), (e = e || "fx");

        while (u--) {
          (r = v._data(o[u], e + "queueHooks")),
            r && r.empty && (i++, r.empty.add(a));
        }

        return a(), s.promise(n);
      },
    });
  var j,
    F,
    I,
    q = /[\t\r\n]/g,
    R = /\r/g,
    U = /^(?:button|input)$/i,
    z = /^(?:button|input|object|select|textarea)$/i,
    W = /^a(?:rea|)$/i,
    X = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
    V = v.support.getSetAttribute;
  v.fn.extend({
    attr: function attr(e, t) {
      return v.access(this, v.attr, e, t, arguments.length > 1);
    },
    removeAttr: function removeAttr(e) {
      return this.each(function () {
        v.removeAttr(this, e);
      });
    },
    prop: function prop(e, t) {
      return v.access(this, v.prop, e, t, arguments.length > 1);
    },
    removeProp: function removeProp(e) {
      return (
        (e = v.propFix[e] || e),
        this.each(function () {
          try {
            (this[e] = t), delete this[e];
          } catch (n) {}
        })
      );
    },
    addClass: function addClass(e) {
      var t, n, r, i, s, o, u;
      if (v.isFunction(e))
        return this.each(function (t) {
          v(this).addClass(e.call(this, t, this.className));
        });

      if (e && typeof e == "string") {
        t = e.split(y);

        for (n = 0, r = this.length; n < r; n++) {
          i = this[n];
          if (i.nodeType === 1)
            if (!i.className && t.length === 1) i.className = e;
            else {
              s = " " + i.className + " ";

              for (o = 0, u = t.length; o < u; o++) {
                s.indexOf(" " + t[o] + " ") < 0 && (s += t[o] + " ");
              }

              i.className = v.trim(s);
            }
        }
      }

      return this;
    },
    removeClass: function removeClass(e) {
      var n, r, i, s, o, u, a;
      if (v.isFunction(e))
        return this.each(function (t) {
          v(this).removeClass(e.call(this, t, this.className));
        });

      if ((e && typeof e == "string") || e === t) {
        n = (e || "").split(y);

        for (u = 0, a = this.length; u < a; u++) {
          i = this[u];

          if (i.nodeType === 1 && i.className) {
            r = (" " + i.className + " ").replace(q, " ");

            for (s = 0, o = n.length; s < o; s++) {
              while (r.indexOf(" " + n[s] + " ") >= 0) {
                r = r.replace(" " + n[s] + " ", " ");
              }
            }

            i.className = e ? v.trim(r) : "";
          }
        }
      }

      return this;
    },
    toggleClass: function toggleClass(e, t) {
      var n = _typeof(e),
        r = typeof t == "boolean";

      return v.isFunction(e)
        ? this.each(function (n) {
            v(this).toggleClass(e.call(this, n, this.className, t), t);
          })
        : this.each(function () {
            if (n === "string") {
              var i,
                s = 0,
                o = v(this),
                u = t,
                a = e.split(y);

              while ((i = a[s++])) {
                (u = r ? u : !o.hasClass(i)),
                  o[u ? "addClass" : "removeClass"](i);
              }
            } else if (n === "undefined" || n === "boolean") this.className && v._data(this, "__className__", this.className), (this.className = this.className || e === !1 ? "" : v._data(this, "__className__") || "");
          });
    },
    hasClass: function hasClass(e) {
      var t = " " + e + " ",
        n = 0,
        r = this.length;

      for (; n < r; n++) {
        if (
          this[n].nodeType === 1 &&
          (" " + this[n].className + " ").replace(q, " ").indexOf(t) >= 0
        )
          return !0;
      }

      return !1;
    },
    val: function val(e) {
      var n,
        r,
        i,
        s = this[0];

      if (!arguments.length) {
        if (s)
          return (
            (n = v.valHooks[s.type] || v.valHooks[s.nodeName.toLowerCase()]),
            n && "get" in n && (r = n.get(s, "value")) !== t
              ? r
              : ((r = s.value),
                typeof r == "string" ? r.replace(R, "") : r == null ? "" : r)
          );
        return;
      }

      return (
        (i = v.isFunction(e)),
        this.each(function (r) {
          var s,
            o = v(this);
          if (this.nodeType !== 1) return;
          i ? (s = e.call(this, r, o.val())) : (s = e),
            s == null
              ? (s = "")
              : typeof s == "number"
              ? (s += "")
              : v.isArray(s) &&
                (s = v.map(s, function (e) {
                  return e == null ? "" : e + "";
                })),
            (n =
              v.valHooks[this.type] || v.valHooks[this.nodeName.toLowerCase()]);
          if (!n || !("set" in n) || n.set(this, s, "value") === t)
            this.value = s;
        })
      );
    },
  }),
    v.extend({
      valHooks: {
        option: {
          get: function get(e) {
            var t = e.attributes.value;
            return !t || t.specified ? e.value : e.text;
          },
        },
        select: {
          get: function get(e) {
            var t,
              n,
              r = e.options,
              i = e.selectedIndex,
              s = e.type === "select-one" || i < 0,
              o = s ? null : [],
              u = s ? i + 1 : r.length,
              a = i < 0 ? u : s ? i : 0;

            for (; a < u; a++) {
              n = r[a];

              if (
                (n.selected || a === i) &&
                (v.support.optDisabled
                  ? !n.disabled
                  : n.getAttribute("disabled") === null) &&
                (!n.parentNode.disabled ||
                  !v.nodeName(n.parentNode, "optgroup"))
              ) {
                t = v(n).val();
                if (s) return t;
                o.push(t);
              }
            }

            return o;
          },
          set: function set(e, t) {
            var n = v.makeArray(t);
            return (
              v(e)
                .find("option")
                .each(function () {
                  this.selected = v.inArray(v(this).val(), n) >= 0;
                }),
              n.length || (e.selectedIndex = -1),
              n
            );
          },
        },
      },
      attrFn: {},
      attr: function attr(e, n, r, i) {
        var s,
          o,
          u,
          a = e.nodeType;
        if (!e || a === 3 || a === 8 || a === 2) return;
        if (i && v.isFunction(v.fn[n])) return v(e)[n](r);
        if (typeof e.getAttribute == "undefined") return v.prop(e, n, r);
        (u = a !== 1 || !v.isXMLDoc(e)),
          u &&
            ((n = n.toLowerCase()),
            (o = v.attrHooks[n] || (X.test(n) ? F : j)));

        if (r !== t) {
          if (r === null) {
            v.removeAttr(e, n);
            return;
          }

          return o && "set" in o && u && (s = o.set(e, r, n)) !== t
            ? s
            : (e.setAttribute(n, r + ""), r);
        }

        return o && "get" in o && u && (s = o.get(e, n)) !== null
          ? s
          : ((s = e.getAttribute(n)), s === null ? t : s);
      },
      removeAttr: function removeAttr(e, t) {
        var n,
          r,
          i,
          s,
          o = 0;

        if (t && e.nodeType === 1) {
          r = t.split(y);

          for (; o < r.length; o++) {
            (i = r[o]),
              i &&
                ((n = v.propFix[i] || i),
                (s = X.test(i)),
                s || v.attr(e, i, ""),
                e.removeAttribute(V ? i : n),
                s && n in e && (e[n] = !1));
          }
        }
      },
      attrHooks: {
        type: {
          set: function set(e, t) {
            if (U.test(e.nodeName) && e.parentNode)
              v.error("type property can't be changed");
            else if (
              !v.support.radioValue &&
              t === "radio" &&
              v.nodeName(e, "input")
            ) {
              var n = e.value;
              return e.setAttribute("type", t), n && (e.value = n), t;
            }
          },
        },
        value: {
          get: function get(e, t) {
            return j && v.nodeName(e, "button")
              ? j.get(e, t)
              : t in e
              ? e.value
              : null;
          },
          set: function set(e, t, n) {
            if (j && v.nodeName(e, "button")) return j.set(e, t, n);
            e.value = t;
          },
        },
      },
      propFix: {
        tabindex: "tabIndex",
        readonly: "readOnly",
        for: "htmlFor",
        class: "className",
        maxlength: "maxLength",
        cellspacing: "cellSpacing",
        cellpadding: "cellPadding",
        rowspan: "rowSpan",
        colspan: "colSpan",
        usemap: "useMap",
        frameborder: "frameBorder",
        contenteditable: "contentEditable",
      },
      prop: function prop(e, n, r) {
        var i,
          s,
          o,
          u = e.nodeType;
        if (!e || u === 3 || u === 8 || u === 2) return;
        return (
          (o = u !== 1 || !v.isXMLDoc(e)),
          o && ((n = v.propFix[n] || n), (s = v.propHooks[n])),
          r !== t
            ? s && "set" in s && (i = s.set(e, r, n)) !== t
              ? i
              : (e[n] = r)
            : s && "get" in s && (i = s.get(e, n)) !== null
            ? i
            : e[n]
        );
      },
      propHooks: {
        tabIndex: {
          get: function get(e) {
            var n = e.getAttributeNode("tabindex");
            return n && n.specified
              ? parseInt(n.value, 10)
              : z.test(e.nodeName) || (W.test(e.nodeName) && e.href)
              ? 0
              : t;
          },
        },
      },
    }),
    (F = {
      get: function get(e, n) {
        var r,
          i = v.prop(e, n);
        return i === !0 ||
          (typeof i != "boolean" &&
            (r = e.getAttributeNode(n)) &&
            r.nodeValue !== !1)
          ? n.toLowerCase()
          : t;
      },
      set: function set(e, t, n) {
        var r;
        return (
          t === !1
            ? v.removeAttr(e, n)
            : ((r = v.propFix[n] || n),
              r in e && (e[r] = !0),
              e.setAttribute(n, n.toLowerCase())),
          n
        );
      },
    }),
    V ||
      ((I = {
        name: !0,
        id: !0,
        coords: !0,
      }),
      (j = v.valHooks.button = {
        get: function get(e, n) {
          var r;
          return (
            (r = e.getAttributeNode(n)),
            r && (I[n] ? r.value !== "" : r.specified) ? r.value : t
          );
        },
        set: function set(e, t, n) {
          var r = e.getAttributeNode(n);
          return (
            r || ((r = i.createAttribute(n)), e.setAttributeNode(r)),
            (r.value = t + "")
          );
        },
      }),
      v.each(["width", "height"], function (e, t) {
        v.attrHooks[t] = v.extend(v.attrHooks[t], {
          set: function set(e, n) {
            if (n === "") return e.setAttribute(t, "auto"), n;
          },
        });
      }),
      (v.attrHooks.contenteditable = {
        get: j.get,
        set: function set(e, t, n) {
          t === "" && (t = "false"), j.set(e, t, n);
        },
      })),
    v.support.hrefNormalized ||
      v.each(["href", "src", "width", "height"], function (e, n) {
        v.attrHooks[n] = v.extend(v.attrHooks[n], {
          get: function get(e) {
            var r = e.getAttribute(n, 2);
            return r === null ? t : r;
          },
        });
      }),
    v.support.style ||
      (v.attrHooks.style = {
        get: function get(e) {
          return e.style.cssText.toLowerCase() || t;
        },
        set: function set(e, t) {
          return (e.style.cssText = t + "");
        },
      }),
    v.support.optSelected ||
      (v.propHooks.selected = v.extend(v.propHooks.selected, {
        get: function get(e) {
          var t = e.parentNode;
          return (
            t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex),
            null
          );
        },
      })),
    v.support.enctype || (v.propFix.enctype = "encoding"),
    v.support.checkOn ||
      v.each(["radio", "checkbox"], function () {
        v.valHooks[this] = {
          get: function get(e) {
            return e.getAttribute("value") === null ? "on" : e.value;
          },
        };
      }),
    v.each(["radio", "checkbox"], function () {
      v.valHooks[this] = v.extend(v.valHooks[this], {
        set: function set(e, t) {
          if (v.isArray(t)) return (e.checked = v.inArray(v(e).val(), t) >= 0);
        },
      });
    });

  var $ = /^(?:textarea|input|select)$/i,
    J = /^([^\.]*|)(?:\.(.+)|)$/,
    K = /(?:^|\s)hover(\.\S+|)\b/,
    Q = /^key/,
    G = /^(?:mouse|contextmenu)|click/,
    Y = /^(?:focusinfocus|focusoutblur)$/,
    Z = function Z(e) {
      return v.event.special.hover
        ? e
        : e.replace(K, "mouseenter$1 mouseleave$1");
    };

  (v.event = {
    add: function add(e, n, r, i, s) {
      var o, _u, a, f, l, c, h, p, d, m, g;

      if (e.nodeType === 3 || e.nodeType === 8 || !n || !r || !(o = v._data(e)))
        return;
      r.handler && ((d = r), (r = d.handler), (s = d.selector)),
        r.guid || (r.guid = v.guid++),
        (a = o.events),
        a || (o.events = a = {}),
        (_u = o.handle),
        _u ||
          ((o.handle = _u = function u(e) {
            return typeof v == "undefined" ||
              (!!e && v.event.triggered === e.type)
              ? t
              : v.event.dispatch.apply(_u.elem, arguments);
          }),
          (_u.elem = e)),
        (n = v.trim(Z(n)).split(" "));

      for (f = 0; f < n.length; f++) {
        (l = J.exec(n[f]) || []),
          (c = l[1]),
          (h = (l[2] || "").split(".").sort()),
          (g = v.event.special[c] || {}),
          (c = (s ? g.delegateType : g.bindType) || c),
          (g = v.event.special[c] || {}),
          (p = v.extend(
            {
              type: c,
              origType: l[1],
              data: i,
              handler: r,
              guid: r.guid,
              selector: s,
              needsContext: s && v.expr.match.needsContext.test(s),
              namespace: h.join("."),
            },
            d
          )),
          (m = a[c]);

        if (!m) {
          (m = a[c] = []), (m.delegateCount = 0);
          if (!g.setup || g.setup.call(e, i, h, _u) === !1)
            e.addEventListener
              ? e.addEventListener(c, _u, !1)
              : e.attachEvent && e.attachEvent("on" + c, _u);
        }

        g.add &&
          (g.add.call(e, p), p.handler.guid || (p.handler.guid = r.guid)),
          s ? m.splice(m.delegateCount++, 0, p) : m.push(p),
          (v.event.global[c] = !0);
      }

      e = null;
    },
    global: {},
    remove: function remove(e, t, n, r, i) {
      var s,
        o,
        u,
        a,
        f,
        l,
        c,
        h,
        p,
        d,
        m,
        g = v.hasData(e) && v._data(e);

      if (!g || !(h = g.events)) return;
      t = v.trim(Z(t || "")).split(" ");

      for (s = 0; s < t.length; s++) {
        (o = J.exec(t[s]) || []), (u = a = o[1]), (f = o[2]);

        if (!u) {
          for (u in h) {
            v.event.remove(e, u + t[s], n, r, !0);
          }

          continue;
        }

        (p = v.event.special[u] || {}),
          (u = (r ? p.delegateType : p.bindType) || u),
          (d = h[u] || []),
          (l = d.length),
          (f = f
            ? new RegExp(
                "(^|\\.)" +
                  f.split(".").sort().join("\\.(?:.*\\.|)") +
                  "(\\.|$)"
              )
            : null);

        for (c = 0; c < d.length; c++) {
          (m = d[c]),
            (i || a === m.origType) &&
              (!n || n.guid === m.guid) &&
              (!f || f.test(m.namespace)) &&
              (!r || r === m.selector || (r === "**" && m.selector)) &&
              (d.splice(c--, 1),
              m.selector && d.delegateCount--,
              p.remove && p.remove.call(e, m));
        }

        d.length === 0 &&
          l !== d.length &&
          ((!p.teardown || p.teardown.call(e, f, g.handle) === !1) &&
            v.removeEvent(e, u, g.handle),
          delete h[u]);
      }

      v.isEmptyObject(h) && (delete g.handle, v.removeData(e, "events", !0));
    },
    customEvent: {
      getData: !0,
      setData: !0,
      changeData: !0,
    },
    trigger: function trigger(n, r, s, o) {
      if (!s || (s.nodeType !== 3 && s.nodeType !== 8)) {
        var u,
          a,
          f,
          l,
          c,
          h,
          p,
          d,
          m,
          g,
          y = n.type || n,
          b = [];
        if (Y.test(y + v.event.triggered)) return;
        y.indexOf("!") >= 0 && ((y = y.slice(0, -1)), (a = !0)),
          y.indexOf(".") >= 0 &&
            ((b = y.split(".")), (y = b.shift()), b.sort());
        if ((!s || v.event.customEvent[y]) && !v.event.global[y]) return;
        (n =
          _typeof(n) == "object"
            ? n[v.expando]
              ? n
              : new v.Event(y, n)
            : new v.Event(y)),
          (n.type = y),
          (n.isTrigger = !0),
          (n.exclusive = a),
          (n.namespace = b.join(".")),
          (n.namespace_re = n.namespace
            ? new RegExp("(^|\\.)" + b.join("\\.(?:.*\\.|)") + "(\\.|$)")
            : null),
          (h = y.indexOf(":") < 0 ? "on" + y : "");

        if (!s) {
          u = v.cache;

          for (f in u) {
            u[f].events &&
              u[f].events[y] &&
              v.event.trigger(n, r, u[f].handle.elem, !0);
          }

          return;
        }

        (n.result = t),
          n.target || (n.target = s),
          (r = r != null ? v.makeArray(r) : []),
          r.unshift(n),
          (p = v.event.special[y] || {});
        if (p.trigger && p.trigger.apply(s, r) === !1) return;
        m = [[s, p.bindType || y]];

        if (!o && !p.noBubble && !v.isWindow(s)) {
          (g = p.delegateType || y), (l = Y.test(g + y) ? s : s.parentNode);

          for (c = s; l; l = l.parentNode) {
            m.push([l, g]), (c = l);
          }

          c === (s.ownerDocument || i) &&
            m.push([c.defaultView || c.parentWindow || e, g]);
        }

        for (f = 0; f < m.length && !n.isPropagationStopped(); f++) {
          (l = m[f][0]),
            (n.type = m[f][1]),
            (d = (v._data(l, "events") || {})[n.type] && v._data(l, "handle")),
            d && d.apply(l, r),
            (d = h && l[h]),
            d &&
              v.acceptData(l) &&
              d.apply &&
              d.apply(l, r) === !1 &&
              n.preventDefault();
        }

        return (
          (n.type = y),
          !o &&
            !n.isDefaultPrevented() &&
            (!p._default || p._default.apply(s.ownerDocument, r) === !1) &&
            (y !== "click" || !v.nodeName(s, "a")) &&
            v.acceptData(s) &&
            h &&
            s[y] &&
            ((y !== "focus" && y !== "blur") || n.target.offsetWidth !== 0) &&
            !v.isWindow(s) &&
            ((c = s[h]),
            c && (s[h] = null),
            (v.event.triggered = y),
            s[y](),
            (v.event.triggered = t),
            c && (s[h] = c)),
          n.result
        );
      }

      return;
    },
    dispatch: function dispatch(n) {
      n = v.event.fix(n || e.event);
      var r,
        i,
        s,
        o,
        u,
        a,
        f,
        c,
        h,
        p,
        d = (v._data(this, "events") || {})[n.type] || [],
        m = d.delegateCount,
        g = l.call(arguments),
        y = !n.exclusive && !n.namespace,
        b = v.event.special[n.type] || {},
        w = [];
      (g[0] = n), (n.delegateTarget = this);
      if (b.preDispatch && b.preDispatch.call(this, n) === !1) return;
      if (m && (!n.button || n.type !== "click"))
        for (s = n.target; s != this; s = s.parentNode || this) {
          if (s.disabled !== !0 || n.type !== "click") {
            (u = {}), (f = []);

            for (r = 0; r < m; r++) {
              (c = d[r]),
                (h = c.selector),
                u[h] === t &&
                  (u[h] = c.needsContext
                    ? v(h, this).index(s) >= 0
                    : v.find(h, this, null, [s]).length),
                u[h] && f.push(c);
            }

            f.length &&
              w.push({
                elem: s,
                matches: f,
              });
          }
        }
      d.length > m &&
        w.push({
          elem: this,
          matches: d.slice(m),
        });

      for (r = 0; r < w.length && !n.isPropagationStopped(); r++) {
        (a = w[r]), (n.currentTarget = a.elem);

        for (
          i = 0;
          i < a.matches.length && !n.isImmediatePropagationStopped();
          i++
        ) {
          c = a.matches[i];
          if (
            y ||
            (!n.namespace && !c.namespace) ||
            (n.namespace_re && n.namespace_re.test(c.namespace))
          )
            (n.data = c.data),
              (n.handleObj = c),
              (o = (
                (v.event.special[c.origType] || {}).handle || c.handler
              ).apply(a.elem, g)),
              o !== t &&
                ((n.result = o),
                o === !1 && (n.preventDefault(), n.stopPropagation()));
        }
      }

      return b.postDispatch && b.postDispatch.call(this, n), n.result;
    },
    props: "attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(
      " "
    ),
    fixHooks: {},
    keyHooks: {
      props: "char charCode key keyCode".split(" "),
      filter: function filter(e, t) {
        return (
          e.which == null &&
            (e.which = t.charCode != null ? t.charCode : t.keyCode),
          e
        );
      },
    },
    mouseHooks: {
      props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(
        " "
      ),
      filter: function filter(e, n) {
        var r,
          s,
          o,
          u = n.button,
          a = n.fromElement;
        return (
          e.pageX == null &&
            n.clientX != null &&
            ((r = e.target.ownerDocument || i),
            (s = r.documentElement),
            (o = r.body),
            (e.pageX =
              n.clientX +
              ((s && s.scrollLeft) || (o && o.scrollLeft) || 0) -
              ((s && s.clientLeft) || (o && o.clientLeft) || 0)),
            (e.pageY =
              n.clientY +
              ((s && s.scrollTop) || (o && o.scrollTop) || 0) -
              ((s && s.clientTop) || (o && o.clientTop) || 0))),
          !e.relatedTarget &&
            a &&
            (e.relatedTarget = a === e.target ? n.toElement : a),
          !e.which &&
            u !== t &&
            (e.which = u & 1 ? 1 : u & 2 ? 3 : u & 4 ? 2 : 0),
          e
        );
      },
    },
    fix: function fix(e) {
      if (e[v.expando]) return e;
      var t,
        n,
        r = e,
        s = v.event.fixHooks[e.type] || {},
        o = s.props ? this.props.concat(s.props) : this.props;
      e = v.Event(r);

      for (t = o.length; t; ) {
        (n = o[--t]), (e[n] = r[n]);
      }

      return (
        e.target || (e.target = r.srcElement || i),
        e.target.nodeType === 3 && (e.target = e.target.parentNode),
        (e.metaKey = !!e.metaKey),
        s.filter ? s.filter(e, r) : e
      );
    },
    special: {
      load: {
        noBubble: !0,
      },
      focus: {
        delegateType: "focusin",
      },
      blur: {
        delegateType: "focusout",
      },
      beforeunload: {
        setup: function setup(e, t, n) {
          v.isWindow(this) && (this.onbeforeunload = n);
        },
        teardown: function teardown(e, t) {
          this.onbeforeunload === t && (this.onbeforeunload = null);
        },
      },
    },
    simulate: function simulate(e, t, n, r) {
      var i = v.extend(new v.Event(), n, {
        type: e,
        isSimulated: !0,
        originalEvent: {},
      });
      r ? v.event.trigger(i, null, t) : v.event.dispatch.call(t, i),
        i.isDefaultPrevented() && n.preventDefault();
    },
  }),
    (v.event.handle = v.event.dispatch),
    (v.removeEvent = i.removeEventListener
      ? function (e, t, n) {
          e.removeEventListener && e.removeEventListener(t, n, !1);
        }
      : function (e, t, n) {
          var r = "on" + t;
          e.detachEvent &&
            (typeof e[r] == "undefined" && (e[r] = null), e.detachEvent(r, n));
        }),
    (v.Event = function (e, t) {
      if (!(this instanceof v.Event)) return new v.Event(e, t);
      e && e.type
        ? ((this.originalEvent = e),
          (this.type = e.type),
          (this.isDefaultPrevented =
            e.defaultPrevented ||
            e.returnValue === !1 ||
            (e.getPreventDefault && e.getPreventDefault())
              ? tt
              : et))
        : (this.type = e),
        t && v.extend(this, t),
        (this.timeStamp = (e && e.timeStamp) || v.now()),
        (this[v.expando] = !0);
    }),
    (v.Event.prototype = {
      preventDefault: function preventDefault() {
        this.isDefaultPrevented = tt;
        var e = this.originalEvent;
        if (!e) return;
        e.preventDefault ? e.preventDefault() : (e.returnValue = !1);
      },
      stopPropagation: function stopPropagation() {
        this.isPropagationStopped = tt;
        var e = this.originalEvent;
        if (!e) return;
        e.stopPropagation && e.stopPropagation(), (e.cancelBubble = !0);
      },
      stopImmediatePropagation: function stopImmediatePropagation() {
        (this.isImmediatePropagationStopped = tt), this.stopPropagation();
      },
      isDefaultPrevented: et,
      isPropagationStopped: et,
      isImmediatePropagationStopped: et,
    }),
    v.each(
      {
        mouseenter: "mouseover",
        mouseleave: "mouseout",
      },
      function (e, t) {
        v.event.special[e] = {
          delegateType: t,
          bindType: t,
          handle: function handle(e) {
            var n,
              r = this,
              i = e.relatedTarget,
              s = e.handleObj,
              o = s.selector;
            if (!i || (i !== r && !v.contains(r, i)))
              (e.type = s.origType),
                (n = s.handler.apply(this, arguments)),
                (e.type = t);
            return n;
          },
        };
      }
    ),
    v.support.submitBubbles ||
      (v.event.special.submit = {
        setup: function setup() {
          if (v.nodeName(this, "form")) return !1;
          v.event.add(this, "click._submit keypress._submit", function (e) {
            var n = e.target,
              r =
                v.nodeName(n, "input") || v.nodeName(n, "button") ? n.form : t;
            r &&
              !v._data(r, "_submit_attached") &&
              (v.event.add(r, "submit._submit", function (e) {
                e._submit_bubble = !0;
              }),
              v._data(r, "_submit_attached", !0));
          });
        },
        postDispatch: function postDispatch(e) {
          e._submit_bubble &&
            (delete e._submit_bubble,
            this.parentNode &&
              !e.isTrigger &&
              v.event.simulate("submit", this.parentNode, e, !0));
        },
        teardown: function teardown() {
          if (v.nodeName(this, "form")) return !1;
          v.event.remove(this, "._submit");
        },
      }),
    v.support.changeBubbles ||
      (v.event.special.change = {
        setup: function setup() {
          if ($.test(this.nodeName)) {
            if (this.type === "checkbox" || this.type === "radio")
              v.event.add(this, "propertychange._change", function (e) {
                e.originalEvent.propertyName === "checked" &&
                  (this._just_changed = !0);
              }),
                v.event.add(this, "click._change", function (e) {
                  this._just_changed &&
                    !e.isTrigger &&
                    (this._just_changed = !1),
                    v.event.simulate("change", this, e, !0);
                });
            return !1;
          }

          v.event.add(this, "beforeactivate._change", function (e) {
            var t = e.target;
            $.test(t.nodeName) &&
              !v._data(t, "_change_attached") &&
              (v.event.add(t, "change._change", function (e) {
                this.parentNode &&
                  !e.isSimulated &&
                  !e.isTrigger &&
                  v.event.simulate("change", this.parentNode, e, !0);
              }),
              v._data(t, "_change_attached", !0));
          });
        },
        handle: function handle(e) {
          var t = e.target;
          if (
            this !== t ||
            e.isSimulated ||
            e.isTrigger ||
            (t.type !== "radio" && t.type !== "checkbox")
          )
            return e.handleObj.handler.apply(this, arguments);
        },
        teardown: function teardown() {
          return v.event.remove(this, "._change"), !$.test(this.nodeName);
        },
      }),
    v.support.focusinBubbles ||
      v.each(
        {
          focus: "focusin",
          blur: "focusout",
        },
        function (e, t) {
          var n = 0,
            r = function r(e) {
              v.event.simulate(t, e.target, v.event.fix(e), !0);
            };

          v.event.special[t] = {
            setup: function setup() {
              n++ === 0 && i.addEventListener(e, r, !0);
            },
            teardown: function teardown() {
              --n === 0 && i.removeEventListener(e, r, !0);
            },
          };
        }
      ),
    v.fn.extend({
      on: function on(e, n, r, i, s) {
        var o, u;

        if (_typeof(e) == "object") {
          typeof n != "string" && ((r = r || n), (n = t));

          for (u in e) {
            this.on(u, n, r, e[u], s);
          }

          return this;
        }

        r == null && i == null
          ? ((i = n), (r = n = t))
          : i == null &&
            (typeof n == "string"
              ? ((i = r), (r = t))
              : ((i = r), (r = n), (n = t)));
        if (i === !1) i = et;
        else if (!i) return this;
        return (
          s === 1 &&
            ((o = i),
            (i = function i(e) {
              return v().off(e), o.apply(this, arguments);
            }),
            (i.guid = o.guid || (o.guid = v.guid++))),
          this.each(function () {
            v.event.add(this, e, i, r, n);
          })
        );
      },
      one: function one(e, t, n, r) {
        return this.on(e, t, n, r, 1);
      },
      off: function off(e, n, r) {
        var i, s;
        if (e && e.preventDefault && e.handleObj)
          return (
            (i = e.handleObj),
            v(e.delegateTarget).off(
              i.namespace ? i.origType + "." + i.namespace : i.origType,
              i.selector,
              i.handler
            ),
            this
          );

        if (_typeof(e) == "object") {
          for (s in e) {
            this.off(s, n, e[s]);
          }

          return this;
        }

        if (n === !1 || typeof n == "function") (r = n), (n = t);
        return (
          r === !1 && (r = et),
          this.each(function () {
            v.event.remove(this, e, r, n);
          })
        );
      },
      bind: function bind(e, t, n) {
        return this.on(e, null, t, n);
      },
      unbind: function unbind(e, t) {
        return this.off(e, null, t);
      },
      live: function live(e, t, n) {
        return v(this.context).on(e, this.selector, t, n), this;
      },
      die: function die(e, t) {
        return v(this.context).off(e, this.selector || "**", t), this;
      },
      delegate: function delegate(e, t, n, r) {
        return this.on(t, e, n, r);
      },
      undelegate: function undelegate(e, t, n) {
        return arguments.length === 1
          ? this.off(e, "**")
          : this.off(t, e || "**", n);
      },
      trigger: function trigger(e, t) {
        return this.each(function () {
          v.event.trigger(e, t, this);
        });
      },
      triggerHandler: function triggerHandler(e, t) {
        if (this[0]) return v.event.trigger(e, t, this[0], !0);
      },
      toggle: function toggle(e) {
        var t = arguments,
          n = e.guid || v.guid++,
          r = 0,
          i = function i(n) {
            var i = (v._data(this, "lastToggle" + e.guid) || 0) % r;
            return (
              v._data(this, "lastToggle" + e.guid, i + 1),
              n.preventDefault(),
              t[i].apply(this, arguments) || !1
            );
          };

        i.guid = n;

        while (r < t.length) {
          t[r++].guid = n;
        }

        return this.click(i);
      },
      hover: function hover(e, t) {
        return this.mouseenter(e).mouseleave(t || e);
      },
    }),
    v.each(
      "blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(
        " "
      ),
      function (e, t) {
        (v.fn[t] = function (e, n) {
          return (
            n == null && ((n = e), (e = null)),
            arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
          );
        }),
          Q.test(t) && (v.event.fixHooks[t] = v.event.keyHooks),
          G.test(t) && (v.event.fixHooks[t] = v.event.mouseHooks);
      }
    ),
    (function (e, t) {
      function nt(e, t, n, r) {
        (n = n || []), (t = t || g);
        var i,
          s,
          a,
          f,
          l = t.nodeType;
        if (!e || typeof e != "string") return n;
        if (l !== 1 && l !== 9) return [];
        a = o(t);
        if (!a && !r)
          if ((i = R.exec(e)))
            if ((f = i[1])) {
              if (l === 9) {
                s = t.getElementById(f);
                if (!s || !s.parentNode) return n;
                if (s.id === f) return n.push(s), n;
              } else if (
                t.ownerDocument &&
                (s = t.ownerDocument.getElementById(f)) &&
                u(t, s) &&
                s.id === f
              )
                return n.push(s), n;
            } else {
              if (i[2])
                return S.apply(n, x.call(t.getElementsByTagName(e), 0)), n;
              if ((f = i[3]) && Z && t.getElementsByClassName)
                return S.apply(n, x.call(t.getElementsByClassName(f), 0)), n;
            }
        return vt(e.replace(j, "$1"), t, n, r, a);
      }

      function rt(e) {
        return function (t) {
          var n = t.nodeName.toLowerCase();
          return n === "input" && t.type === e;
        };
      }

      function it(e) {
        return function (t) {
          var n = t.nodeName.toLowerCase();
          return (n === "input" || n === "button") && t.type === e;
        };
      }

      function st(e) {
        return N(function (t) {
          return (
            (t = +t),
            N(function (n, r) {
              var i,
                s = e([], n.length, t),
                o = s.length;

              while (o--) {
                n[(i = s[o])] && (n[i] = !(r[i] = n[i]));
              }
            })
          );
        });
      }

      function ot(e, t, n) {
        if (e === t) return n;
        var r = e.nextSibling;

        while (r) {
          if (r === t) return -1;
          r = r.nextSibling;
        }

        return 1;
      }

      function ut(e, t) {
        var n,
          r,
          s,
          o,
          u,
          a,
          f,
          l = L[d][e + " "];
        if (l) return t ? 0 : l.slice(0);
        (u = e), (a = []), (f = i.preFilter);

        while (u) {
          if (!n || (r = F.exec(u)))
            r && (u = u.slice(r[0].length) || u), a.push((s = []));
          n = !1;
          if ((r = I.exec(u)))
            s.push((n = new m(r.shift()))),
              (u = u.slice(n.length)),
              (n.type = r[0].replace(j, " "));

          for (o in i.filter) {
            (r = J[o].exec(u)) &&
              (!f[o] || (r = f[o](r))) &&
              (s.push((n = new m(r.shift()))),
              (u = u.slice(n.length)),
              (n.type = o),
              (n.matches = r));
          }

          if (!n) break;
        }

        return t ? u.length : u ? nt.error(e) : L(e, a).slice(0);
      }

      function at(e, t, r) {
        var i = t.dir,
          s = r && t.dir === "parentNode",
          o = w++;
        return t.first
          ? function (t, n, r) {
              while ((t = t[i])) {
                if (s || t.nodeType === 1) return e(t, n, r);
              }
            }
          : function (t, r, u) {
              if (!u) {
                var a,
                  f = b + " " + o + " ",
                  l = f + n;

                while ((t = t[i])) {
                  if (s || t.nodeType === 1) {
                    if ((a = t[d]) === l) return t.sizset;

                    if (typeof a == "string" && a.indexOf(f) === 0) {
                      if (t.sizset) return t;
                    } else {
                      t[d] = l;
                      if (e(t, r, u)) return (t.sizset = !0), t;
                      t.sizset = !1;
                    }
                  }
                }
              } else
                while ((t = t[i])) {
                  if (s || t.nodeType === 1) if (e(t, r, u)) return t;
                }
            };
      }

      function ft(e) {
        return e.length > 1
          ? function (t, n, r) {
              var i = e.length;

              while (i--) {
                if (!e[i](t, n, r)) return !1;
              }

              return !0;
            }
          : e[0];
      }

      function lt(e, t, n, r, i) {
        var s,
          o = [],
          u = 0,
          a = e.length,
          f = t != null;

        for (; u < a; u++) {
          if ((s = e[u])) if (!n || n(s, r, i)) o.push(s), f && t.push(u);
        }

        return o;
      }

      function ct(e, t, n, r, i, s) {
        return (
          r && !r[d] && (r = ct(r)),
          i && !i[d] && (i = ct(i, s)),
          N(function (s, o, u, a) {
            var f,
              l,
              c,
              h = [],
              p = [],
              d = o.length,
              v = s || dt(t || "*", u.nodeType ? [u] : u, []),
              m = e && (s || !t) ? lt(v, h, e, u, a) : v,
              g = n ? (i || (s ? e : d || r) ? [] : o) : m;
            n && n(m, g, u, a);

            if (r) {
              (f = lt(g, p)), r(f, [], u, a), (l = f.length);

              while (l--) {
                if ((c = f[l])) g[p[l]] = !(m[p[l]] = c);
              }
            }

            if (s) {
              if (i || e) {
                if (i) {
                  (f = []), (l = g.length);

                  while (l--) {
                    (c = g[l]) && f.push((m[l] = c));
                  }

                  i(null, (g = []), f, a);
                }

                l = g.length;

                while (l--) {
                  (c = g[l]) &&
                    (f = i ? T.call(s, c) : h[l]) > -1 &&
                    (s[f] = !(o[f] = c));
                }
              }
            } else (g = lt(g === o ? g.splice(d, g.length) : g)), i ? i(null, o, g, a) : S.apply(o, g);
          })
        );
      }

      function ht(e) {
        var t,
          n,
          r,
          s = e.length,
          o = i.relative[e[0].type],
          u = o || i.relative[" "],
          a = o ? 1 : 0,
          f = at(
            function (e) {
              return e === t;
            },
            u,
            !0
          ),
          l = at(
            function (e) {
              return T.call(t, e) > -1;
            },
            u,
            !0
          ),
          h = [
            function (e, n, r) {
              return (
                (!o && (r || n !== c)) ||
                ((t = n).nodeType ? f(e, n, r) : l(e, n, r))
              );
            },
          ];

        for (; a < s; a++) {
          if ((n = i.relative[e[a].type])) h = [at(ft(h), n)];
          else {
            n = i.filter[e[a].type].apply(null, e[a].matches);

            if (n[d]) {
              r = ++a;

              for (; r < s; r++) {
                if (i.relative[e[r].type]) break;
              }

              return ct(
                a > 1 && ft(h),
                a > 1 &&
                  e
                    .slice(0, a - 1)
                    .join("")
                    .replace(j, "$1"),
                n,
                a < r && ht(e.slice(a, r)),
                r < s && ht((e = e.slice(r))),
                r < s && e.join("")
              );
            }

            h.push(n);
          }
        }

        return ft(h);
      }

      function pt(e, t) {
        var r = t.length > 0,
          s = e.length > 0,
          o = function o(u, a, f, l, h) {
            var p,
              d,
              v,
              m = [],
              y = 0,
              w = "0",
              x = u && [],
              T = h != null,
              N = c,
              C = u || (s && i.find.TAG("*", (h && a.parentNode) || a)),
              k = (b += N == null ? 1 : Math.E);
            T && ((c = a !== g && a), (n = o.el));

            for (; (p = C[w]) != null; w++) {
              if (s && p) {
                for (d = 0; (v = e[d]); d++) {
                  if (v(p, a, f)) {
                    l.push(p);
                    break;
                  }
                }

                T && ((b = k), (n = ++o.el));
              }

              r && ((p = !v && p) && y--, u && x.push(p));
            }

            y += w;

            if (r && w !== y) {
              for (d = 0; (v = t[d]); d++) {
                v(x, m, a, f);
              }

              if (u) {
                if (y > 0)
                  while (w--) {
                    !x[w] && !m[w] && (m[w] = E.call(l));
                  }
                m = lt(m);
              }

              S.apply(l, m),
                T && !u && m.length > 0 && y + t.length > 1 && nt.uniqueSort(l);
            }

            return T && ((b = k), (c = N)), x;
          };

        return (o.el = 0), r ? N(o) : o;
      }

      function dt(e, t, n) {
        var r = 0,
          i = t.length;

        for (; r < i; r++) {
          nt(e, t[r], n);
        }

        return n;
      }

      function vt(e, t, n, r, s) {
        var o,
          u,
          f,
          l,
          c,
          h = ut(e),
          p = h.length;

        if (!r && h.length === 1) {
          u = h[0] = h[0].slice(0);

          if (
            u.length > 2 &&
            (f = u[0]).type === "ID" &&
            t.nodeType === 9 &&
            !s &&
            i.relative[u[1].type]
          ) {
            t = i.find.ID(f.matches[0].replace($, ""), t, s)[0];
            if (!t) return n;
            e = e.slice(u.shift().length);
          }

          for (o = J.POS.test(e) ? -1 : u.length - 1; o >= 0; o--) {
            f = u[o];
            if (i.relative[(l = f.type)]) break;
            if ((c = i.find[l]))
              if (
                (r = c(
                  f.matches[0].replace($, ""),
                  (z.test(u[0].type) && t.parentNode) || t,
                  s
                ))
              ) {
                u.splice(o, 1), (e = r.length && u.join(""));
                if (!e) return S.apply(n, x.call(r, 0)), n;
                break;
              }
          }
        }

        return a(e, h)(r, t, s, n, z.test(e)), n;
      }

      function mt() {}

      var n,
        r,
        i,
        s,
        o,
        u,
        a,
        f,
        l,
        c,
        h = !0,
        p = "undefined",
        d = ("sizcache" + Math.random()).replace(".", ""),
        m = String,
        g = e.document,
        y = g.documentElement,
        b = 0,
        w = 0,
        E = [].pop,
        S = [].push,
        x = [].slice,
        T =
          [].indexOf ||
          function (e) {
            var t = 0,
              n = this.length;

            for (; t < n; t++) {
              if (this[t] === e) return t;
            }

            return -1;
          },
        N = function N(e, t) {
          return (e[d] = t == null || t), e;
        },
        C = function C() {
          var e = {},
            t = [];
          return N(function (n, r) {
            return (
              t.push(n) > i.cacheLength && delete e[t.shift()], (e[n + " "] = r)
            );
          }, e);
        },
        k = C(),
        L = C(),
        A = C(),
        O = "[\\x20\\t\\r\\n\\f]",
        M = "(?:\\\\.|[-\\w]|[^\\x00-\\xa0])+",
        _ = M.replace("w", "w#"),
        D = "([*^$|!~]?=)",
        P =
          "\\[" +
          O +
          "*(" +
          M +
          ")" +
          O +
          "*(?:" +
          D +
          O +
          "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" +
          _ +
          ")|)|)" +
          O +
          "*\\]",
        H =
          ":(" +
          M +
          ")(?:\\((?:(['\"])((?:\\\\.|[^\\\\])*?)\\2|([^()[\\]]*|(?:(?:" +
          P +
          ")|[^:]|\\\\.)*|.*))\\)|)",
        B =
          ":(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
          O +
          "*((?:-\\d)?\\d*)" +
          O +
          "*\\)|)(?=[^-]|$)",
        j = new RegExp("^" + O + "+|((?:^|[^\\\\])(?:\\\\.)*)" + O + "+$", "g"),
        F = new RegExp("^" + O + "*," + O + "*"),
        I = new RegExp("^" + O + "*([\\x20\\t\\r\\n\\f>+~])" + O + "*"),
        q = new RegExp(H),
        R = /^(?:#([\w\-]+)|(\w+)|\.([\w\-]+))$/,
        U = /^:not/,
        z = /[\x20\t\r\n\f]*[+~]/,
        W = /:not\($/,
        X = /h\d/i,
        V = /input|select|textarea|button/i,
        $ = /\\(?!\\)/g,
        J = {
          ID: new RegExp("^#(" + M + ")"),
          CLASS: new RegExp("^\\.(" + M + ")"),
          NAME: new RegExp("^\\[name=['\"]?(" + M + ")['\"]?\\]"),
          TAG: new RegExp("^(" + M.replace("w", "w*") + ")"),
          ATTR: new RegExp("^" + P),
          PSEUDO: new RegExp("^" + H),
          POS: new RegExp(B, "i"),
          CHILD: new RegExp(
            "^:(only|nth|first|last)-child(?:\\(" +
              O +
              "*(even|odd|(([+-]|)(\\d*)n|)" +
              O +
              "*(?:([+-]|)" +
              O +
              "*(\\d+)|))" +
              O +
              "*\\)|)",
            "i"
          ),
          needsContext: new RegExp("^" + O + "*[>+~]|" + B, "i"),
        },
        K = function K(e) {
          var t = g.createElement("div");

          try {
            return e(t);
          } catch (n) {
            return !1;
          } finally {
            t = null;
          }
        },
        Q = K(function (e) {
          return (
            e.appendChild(g.createComment("")),
            !e.getElementsByTagName("*").length
          );
        }),
        G = K(function (e) {
          return (
            (e.innerHTML = "<a href='#'></a>"),
            e.firstChild &&
              _typeof(e.firstChild.getAttribute) !== p &&
              e.firstChild.getAttribute("href") === "#"
          );
        }),
        Y = K(function (e) {
          e.innerHTML = "<select></select>";

          var t = _typeof(e.lastChild.getAttribute("multiple"));

          return t !== "boolean" && t !== "string";
        }),
        Z = K(function (e) {
          return (
            (e.innerHTML =
              "<div class='hidden e'></div><div class='hidden'></div>"),
            !e.getElementsByClassName || !e.getElementsByClassName("e").length
              ? !1
              : ((e.lastChild.className = "e"),
                e.getElementsByClassName("e").length === 2)
          );
        }),
        et = K(function (e) {
          (e.id = d + 0),
            (e.innerHTML =
              "<a name='" + d + "'></a><div name='" + d + "'></div>"),
            y.insertBefore(e, y.firstChild);
          var t =
            g.getElementsByName &&
            g.getElementsByName(d).length ===
              2 + g.getElementsByName(d + 0).length;
          return (r = !g.getElementById(d)), y.removeChild(e), t;
        });

      try {
        x.call(y.childNodes, 0)[0].nodeType;
      } catch (tt) {
        x = function x(e) {
          var t,
            n = [];

          for (; (t = this[e]); e++) {
            n.push(t);
          }

          return n;
        };
      }

      (nt.matches = function (e, t) {
        return nt(e, null, null, t);
      }),
        (nt.matchesSelector = function (e, t) {
          return nt(t, null, null, [e]).length > 0;
        }),
        (s = nt.getText = function (e) {
          var t,
            n = "",
            r = 0,
            i = e.nodeType;

          if (i) {
            if (i === 1 || i === 9 || i === 11) {
              if (typeof e.textContent == "string") return e.textContent;

              for (e = e.firstChild; e; e = e.nextSibling) {
                n += s(e);
              }
            } else if (i === 3 || i === 4) return e.nodeValue;
          } else
            for (; (t = e[r]); r++) {
              n += s(t);
            }

          return n;
        }),
        (o = nt.isXML = function (e) {
          var t = e && (e.ownerDocument || e).documentElement;
          return t ? t.nodeName !== "HTML" : !1;
        }),
        (u = nt.contains = y.contains
          ? function (e, t) {
              var n = e.nodeType === 9 ? e.documentElement : e,
                r = t && t.parentNode;
              return (
                e === r ||
                !!(r && r.nodeType === 1 && n.contains && n.contains(r))
              );
            }
          : y.compareDocumentPosition
          ? function (e, t) {
              return t && !!(e.compareDocumentPosition(t) & 16);
            }
          : function (e, t) {
              while ((t = t.parentNode)) {
                if (t === e) return !0;
              }

              return !1;
            }),
        (nt.attr = function (e, t) {
          var n,
            r = o(e);
          return (
            r || (t = t.toLowerCase()),
            (n = i.attrHandle[t])
              ? n(e)
              : r || Y
              ? e.getAttribute(t)
              : ((n = e.getAttributeNode(t)),
                n
                  ? typeof e[t] == "boolean"
                    ? e[t]
                      ? t
                      : null
                    : n.specified
                    ? n.value
                    : null
                  : null)
          );
        }),
        (i = nt.selectors = {
          cacheLength: 50,
          createPseudo: N,
          match: J,
          attrHandle: G
            ? {}
            : {
                href: function href(e) {
                  return e.getAttribute("href", 2);
                },
                type: function type(e) {
                  return e.getAttribute("type");
                },
              },
          find: {
            ID: r
              ? function (e, t, n) {
                  if (_typeof(t.getElementById) !== p && !n) {
                    var r = t.getElementById(e);
                    return r && r.parentNode ? [r] : [];
                  }
                }
              : function (e, n, r) {
                  if (_typeof(n.getElementById) !== p && !r) {
                    var i = n.getElementById(e);
                    return i
                      ? i.id === e ||
                        (_typeof(i.getAttributeNode) !== p &&
                          i.getAttributeNode("id").value === e)
                        ? [i]
                        : t
                      : [];
                  }
                },
            TAG: Q
              ? function (e, t) {
                  if (_typeof(t.getElementsByTagName) !== p)
                    return t.getElementsByTagName(e);
                }
              : function (e, t) {
                  var n = t.getElementsByTagName(e);

                  if (e === "*") {
                    var r,
                      i = [],
                      s = 0;

                    for (; (r = n[s]); s++) {
                      r.nodeType === 1 && i.push(r);
                    }

                    return i;
                  }

                  return n;
                },
            NAME:
              et &&
              function (e, t) {
                if (_typeof(t.getElementsByName) !== p)
                  return t.getElementsByName(name);
              },
            CLASS:
              Z &&
              function (e, t, n) {
                if (_typeof(t.getElementsByClassName) !== p && !n)
                  return t.getElementsByClassName(e);
              },
          },
          relative: {
            ">": {
              dir: "parentNode",
              first: !0,
            },
            " ": {
              dir: "parentNode",
            },
            "+": {
              dir: "previousSibling",
              first: !0,
            },
            "~": {
              dir: "previousSibling",
            },
          },
          preFilter: {
            ATTR: function ATTR(e) {
              return (
                (e[1] = e[1].replace($, "")),
                (e[3] = (e[4] || e[5] || "").replace($, "")),
                e[2] === "~=" && (e[3] = " " + e[3] + " "),
                e.slice(0, 4)
              );
            },
            CHILD: function CHILD(e) {
              return (
                (e[1] = e[1].toLowerCase()),
                e[1] === "nth"
                  ? (e[2] || nt.error(e[0]),
                    (e[3] = +(e[3]
                      ? e[4] + (e[5] || 1)
                      : 2 * (e[2] === "even" || e[2] === "odd"))),
                    (e[4] = +(e[6] + e[7] || e[2] === "odd")))
                  : e[2] && nt.error(e[0]),
                e
              );
            },
            PSEUDO: function PSEUDO(e) {
              var t, n;
              if (J.CHILD.test(e[0])) return null;
              if (e[3]) e[2] = e[3];
              else if ((t = e[4]))
                q.test(t) &&
                  (n = ut(t, !0)) &&
                  (n = t.indexOf(")", t.length - n) - t.length) &&
                  ((t = t.slice(0, n)), (e[0] = e[0].slice(0, n))),
                  (e[2] = t);
              return e.slice(0, 3);
            },
          },
          filter: {
            ID: r
              ? function (e) {
                  return (
                    (e = e.replace($, "")),
                    function (t) {
                      return t.getAttribute("id") === e;
                    }
                  );
                }
              : function (e) {
                  return (
                    (e = e.replace($, "")),
                    function (t) {
                      var n =
                        _typeof(t.getAttributeNode) !== p &&
                        t.getAttributeNode("id");
                      return n && n.value === e;
                    }
                  );
                },
            TAG: function TAG(e) {
              return e === "*"
                ? function () {
                    return !0;
                  }
                : ((e = e.replace($, "").toLowerCase()),
                  function (t) {
                    return t.nodeName && t.nodeName.toLowerCase() === e;
                  });
            },
            CLASS: function CLASS(e) {
              var t = k[d][e + " "];
              return (
                t ||
                ((t = new RegExp("(^|" + O + ")" + e + "(" + O + "|$)")) &&
                  k(e, function (e) {
                    return t.test(
                      e.className ||
                        (_typeof(e.getAttribute) !== p &&
                          e.getAttribute("class")) ||
                        ""
                    );
                  }))
              );
            },
            ATTR: function ATTR(e, t, n) {
              return function (r, i) {
                var s = nt.attr(r, e);
                return s == null
                  ? t === "!="
                  : t
                  ? ((s += ""),
                    t === "="
                      ? s === n
                      : t === "!="
                      ? s !== n
                      : t === "^="
                      ? n && s.indexOf(n) === 0
                      : t === "*="
                      ? n && s.indexOf(n) > -1
                      : t === "$="
                      ? n && s.substr(s.length - n.length) === n
                      : t === "~="
                      ? (" " + s + " ").indexOf(n) > -1
                      : t === "|="
                      ? s === n || s.substr(0, n.length + 1) === n + "-"
                      : !1)
                  : !0;
              };
            },
            CHILD: function CHILD(e, t, n, r) {
              return e === "nth"
                ? function (e) {
                    var t,
                      i,
                      s = e.parentNode;
                    if (n === 1 && r === 0) return !0;

                    if (s) {
                      i = 0;

                      for (t = s.firstChild; t; t = t.nextSibling) {
                        if (t.nodeType === 1) {
                          i++;
                          if (e === t) break;
                        }
                      }
                    }

                    return (i -= r), i === n || (i % n === 0 && i / n >= 0);
                  }
                : function (t) {
                    var n = t;

                    switch (e) {
                      case "only":
                      case "first":
                        while ((n = n.previousSibling)) {
                          if (n.nodeType === 1) return !1;
                        }

                        if (e === "first") return !0;
                        n = t;

                      case "last":
                        while ((n = n.nextSibling)) {
                          if (n.nodeType === 1) return !1;
                        }

                        return !0;
                    }
                  };
            },
            PSEUDO: function PSEUDO(e, t) {
              var n,
                r =
                  i.pseudos[e] ||
                  i.setFilters[e.toLowerCase()] ||
                  nt.error("unsupported pseudo: " + e);
              return r[d]
                ? r(t)
                : r.length > 1
                ? ((n = [e, e, "", t]),
                  i.setFilters.hasOwnProperty(e.toLowerCase())
                    ? N(function (e, n) {
                        var i,
                          s = r(e, t),
                          o = s.length;

                        while (o--) {
                          (i = T.call(e, s[o])), (e[i] = !(n[i] = s[o]));
                        }
                      })
                    : function (e) {
                        return r(e, 0, n);
                      })
                : r;
            },
          },
          pseudos: {
            not: N(function (e) {
              var t = [],
                n = [],
                r = a(e.replace(j, "$1"));
              return r[d]
                ? N(function (e, t, n, i) {
                    var s,
                      o = r(e, null, i, []),
                      u = e.length;

                    while (u--) {
                      if ((s = o[u])) e[u] = !(t[u] = s);
                    }
                  })
                : function (e, i, s) {
                    return (t[0] = e), r(t, null, s, n), !n.pop();
                  };
            }),
            has: N(function (e) {
              return function (t) {
                return nt(e, t).length > 0;
              };
            }),
            contains: N(function (e) {
              return function (t) {
                return (t.textContent || t.innerText || s(t)).indexOf(e) > -1;
              };
            }),
            enabled: function enabled(e) {
              return e.disabled === !1;
            },
            disabled: function disabled(e) {
              return e.disabled === !0;
            },
            checked: function checked(e) {
              var t = e.nodeName.toLowerCase();
              return (
                (t === "input" && !!e.checked) ||
                (t === "option" && !!e.selected)
              );
            },
            selected: function selected(e) {
              return (
                e.parentNode && e.parentNode.selectedIndex, e.selected === !0
              );
            },
            parent: function parent(e) {
              return !i.pseudos.empty(e);
            },
            empty: function empty(e) {
              var t;
              e = e.firstChild;

              while (e) {
                if (e.nodeName > "@" || (t = e.nodeType) === 3 || t === 4)
                  return !1;
                e = e.nextSibling;
              }

              return !0;
            },
            header: function header(e) {
              return X.test(e.nodeName);
            },
            text: function text(e) {
              var t, n;
              return (
                e.nodeName.toLowerCase() === "input" &&
                (t = e.type) === "text" &&
                ((n = e.getAttribute("type")) == null || n.toLowerCase() === t)
              );
            },
            radio: rt("radio"),
            checkbox: rt("checkbox"),
            file: rt("file"),
            password: rt("password"),
            image: rt("image"),
            submit: it("submit"),
            reset: it("reset"),
            button: function button(e) {
              var t = e.nodeName.toLowerCase();
              return (t === "input" && e.type === "button") || t === "button";
            },
            input: function input(e) {
              return V.test(e.nodeName);
            },
            focus: function focus(e) {
              var t = e.ownerDocument;
              return (
                e === t.activeElement &&
                (!t.hasFocus || t.hasFocus()) &&
                !!(e.type || e.href || ~e.tabIndex)
              );
            },
            active: function active(e) {
              return e === e.ownerDocument.activeElement;
            },
            first: st(function () {
              return [0];
            }),
            last: st(function (e, t) {
              return [t - 1];
            }),
            eq: st(function (e, t, n) {
              return [n < 0 ? n + t : n];
            }),
            even: st(function (e, t) {
              for (var n = 0; n < t; n += 2) {
                e.push(n);
              }

              return e;
            }),
            odd: st(function (e, t) {
              for (var n = 1; n < t; n += 2) {
                e.push(n);
              }

              return e;
            }),
            lt: st(function (e, t, n) {
              for (var r = n < 0 ? n + t : n; --r >= 0; ) {
                e.push(r);
              }

              return e;
            }),
            gt: st(function (e, t, n) {
              for (var r = n < 0 ? n + t : n; ++r < t; ) {
                e.push(r);
              }

              return e;
            }),
          },
        }),
        (f = y.compareDocumentPosition
          ? function (e, t) {
              return e === t
                ? ((l = !0), 0)
                : (
                    !e.compareDocumentPosition || !t.compareDocumentPosition
                      ? e.compareDocumentPosition
                      : e.compareDocumentPosition(t) & 4
                  )
                ? -1
                : 1;
            }
          : function (e, t) {
              if (e === t) return (l = !0), 0;
              if (e.sourceIndex && t.sourceIndex)
                return e.sourceIndex - t.sourceIndex;
              var n,
                r,
                i = [],
                s = [],
                o = e.parentNode,
                u = t.parentNode,
                a = o;
              if (o === u) return ot(e, t);
              if (!o) return -1;
              if (!u) return 1;

              while (a) {
                i.unshift(a), (a = a.parentNode);
              }

              a = u;

              while (a) {
                s.unshift(a), (a = a.parentNode);
              }

              (n = i.length), (r = s.length);

              for (var f = 0; f < n && f < r; f++) {
                if (i[f] !== s[f]) return ot(i[f], s[f]);
              }

              return f === n ? ot(e, s[f], -1) : ot(i[f], t, 1);
            }),
        [0, 0].sort(f),
        (h = !l),
        (nt.uniqueSort = function (e) {
          var t,
            n = [],
            r = 1,
            i = 0;
          (l = h), e.sort(f);

          if (l) {
            for (; (t = e[r]); r++) {
              t === e[r - 1] && (i = n.push(r));
            }

            while (i--) {
              e.splice(n[i], 1);
            }
          }

          return e;
        }),
        (nt.error = function (e) {
          throw new Error("Syntax error, unrecognized expression: " + e);
        }),
        (a = nt.compile = function (e, t) {
          var n,
            r = [],
            i = [],
            s = A[d][e + " "];

          if (!s) {
            t || (t = ut(e)), (n = t.length);

            while (n--) {
              (s = ht(t[n])), s[d] ? r.push(s) : i.push(s);
            }

            s = A(e, pt(i, r));
          }

          return s;
        }),
        g.querySelectorAll &&
          (function () {
            var e,
              t = vt,
              n = /'|\\/g,
              r = /\=[\x20\t\r\n\f]*([^'"\]]*)[\x20\t\r\n\f]*\]/g,
              i = [":focus"],
              s = [":active"],
              u =
                y.matchesSelector ||
                y.mozMatchesSelector ||
                y.webkitMatchesSelector ||
                y.oMatchesSelector ||
                y.msMatchesSelector;
            K(function (e) {
              (e.innerHTML = "<select><option selected=''></option></select>"),
                e.querySelectorAll("[selected]").length ||
                  i.push(
                    "\\[" +
                      O +
                      "*(?:checked|disabled|ismap|multiple|readonly|selected|value)"
                  ),
                e.querySelectorAll(":checked").length || i.push(":checked");
            }),
              K(function (e) {
                (e.innerHTML = "<p test=''></p>"),
                  e.querySelectorAll("[test^='']").length &&
                    i.push("[*^$]=" + O + "*(?:\"\"|'')"),
                  (e.innerHTML = "<input type='hidden'/>"),
                  e.querySelectorAll(":enabled").length ||
                    i.push(":enabled", ":disabled");
              }),
              (i = new RegExp(i.join("|"))),
              (vt = function vt(e, r, s, o, u) {
                if (!o && !u && !i.test(e)) {
                  var a,
                    f,
                    l = !0,
                    c = d,
                    h = r,
                    p = r.nodeType === 9 && e;

                  if (
                    r.nodeType === 1 &&
                    r.nodeName.toLowerCase() !== "object"
                  ) {
                    (a = ut(e)),
                      (l = r.getAttribute("id"))
                        ? (c = l.replace(n, "\\$&"))
                        : r.setAttribute("id", c),
                      (c = "[id='" + c + "'] "),
                      (f = a.length);

                    while (f--) {
                      a[f] = c + a[f].join("");
                    }

                    (h = (z.test(e) && r.parentNode) || r), (p = a.join(","));
                  }

                  if (p)
                    try {
                      return S.apply(s, x.call(h.querySelectorAll(p), 0)), s;
                    } catch (v) {
                    } finally {
                      l || r.removeAttribute("id");
                    }
                }

                return t(e, r, s, o, u);
              }),
              u &&
                (K(function (t) {
                  e = u.call(t, "div");

                  try {
                    u.call(t, "[test!='']:sizzle"), s.push("!=", H);
                  } catch (n) {}
                }),
                (s = new RegExp(s.join("|"))),
                (nt.matchesSelector = function (t, n) {
                  n = n.replace(r, "='$1']");
                  if (!o(t) && !s.test(n) && !i.test(n))
                    try {
                      var a = u.call(t, n);
                      if (a || e || (t.document && t.document.nodeType !== 11))
                        return a;
                    } catch (f) {}
                  return nt(n, null, null, [t]).length > 0;
                }));
          })(),
        (i.pseudos.nth = i.pseudos.eq),
        (i.filters = mt.prototype = i.pseudos),
        (i.setFilters = new mt()),
        (nt.attr = v.attr),
        (v.find = nt),
        (v.expr = nt.selectors),
        (v.expr[":"] = v.expr.pseudos),
        (v.unique = nt.uniqueSort),
        (v.text = nt.getText),
        (v.isXMLDoc = nt.isXML),
        (v.contains = nt.contains);
    })(e);
  var nt = /Until$/,
    rt = /^(?:parents|prev(?:Until|All))/,
    it = /^.[^:#\[\.,]*$/,
    st = v.expr.match.needsContext,
    ot = {
      children: !0,
      contents: !0,
      next: !0,
      prev: !0,
    };
  v.fn.extend({
    find: function find(e) {
      var t,
        n,
        r,
        i,
        s,
        o,
        u = this;
      if (typeof e != "string")
        return v(e).filter(function () {
          for (t = 0, n = u.length; t < n; t++) {
            if (v.contains(u[t], this)) return !0;
          }
        });
      o = this.pushStack("", "find", e);

      for (t = 0, n = this.length; t < n; t++) {
        (r = o.length), v.find(e, this[t], o);
        if (t > 0)
          for (i = r; i < o.length; i++) {
            for (s = 0; s < r; s++) {
              if (o[s] === o[i]) {
                o.splice(i--, 1);
                break;
              }
            }
          }
      }

      return o;
    },
    has: function has(e) {
      var t,
        n = v(e, this),
        r = n.length;
      return this.filter(function () {
        for (t = 0; t < r; t++) {
          if (v.contains(this, n[t])) return !0;
        }
      });
    },
    not: function not(e) {
      return this.pushStack(ft(this, e, !1), "not", e);
    },
    filter: function filter(e) {
      return this.pushStack(ft(this, e, !0), "filter", e);
    },
    is: function is(e) {
      return (
        !!e &&
        (typeof e == "string"
          ? st.test(e)
            ? v(e, this.context).index(this[0]) >= 0
            : v.filter(e, this).length > 0
          : this.filter(e).length > 0)
      );
    },
    closest: function closest(e, t) {
      var n,
        r = 0,
        i = this.length,
        s = [],
        o = st.test(e) || typeof e != "string" ? v(e, t || this.context) : 0;

      for (; r < i; r++) {
        n = this[r];

        while (n && n.ownerDocument && n !== t && n.nodeType !== 11) {
          if (o ? o.index(n) > -1 : v.find.matchesSelector(n, e)) {
            s.push(n);
            break;
          }

          n = n.parentNode;
        }
      }

      return (
        (s = s.length > 1 ? v.unique(s) : s), this.pushStack(s, "closest", e)
      );
    },
    index: function index(e) {
      return e
        ? typeof e == "string"
          ? v.inArray(this[0], v(e))
          : v.inArray(e.jquery ? e[0] : e, this)
        : this[0] && this[0].parentNode
        ? this.prevAll().length
        : -1;
    },
    add: function add(e, t) {
      var n =
          typeof e == "string"
            ? v(e, t)
            : v.makeArray(e && e.nodeType ? [e] : e),
        r = v.merge(this.get(), n);
      return this.pushStack(ut(n[0]) || ut(r[0]) ? r : v.unique(r));
    },
    addBack: function addBack(e) {
      return this.add(e == null ? this.prevObject : this.prevObject.filter(e));
    },
  }),
    (v.fn.andSelf = v.fn.addBack),
    v.each(
      {
        parent: function parent(e) {
          var t = e.parentNode;
          return t && t.nodeType !== 11 ? t : null;
        },
        parents: function parents(e) {
          return v.dir(e, "parentNode");
        },
        parentsUntil: function parentsUntil(e, t, n) {
          return v.dir(e, "parentNode", n);
        },
        next: function next(e) {
          return at(e, "nextSibling");
        },
        prev: function prev(e) {
          return at(e, "previousSibling");
        },
        nextAll: function nextAll(e) {
          return v.dir(e, "nextSibling");
        },
        prevAll: function prevAll(e) {
          return v.dir(e, "previousSibling");
        },
        nextUntil: function nextUntil(e, t, n) {
          return v.dir(e, "nextSibling", n);
        },
        prevUntil: function prevUntil(e, t, n) {
          return v.dir(e, "previousSibling", n);
        },
        siblings: function siblings(e) {
          return v.sibling((e.parentNode || {}).firstChild, e);
        },
        children: function children(e) {
          return v.sibling(e.firstChild);
        },
        contents: function contents(e) {
          return v.nodeName(e, "iframe")
            ? e.contentDocument || e.contentWindow.document
            : v.merge([], e.childNodes);
        },
      },
      function (e, t) {
        v.fn[e] = function (n, r) {
          var i = v.map(this, t, n);
          return (
            nt.test(e) || (r = n),
            r && typeof r == "string" && (i = v.filter(r, i)),
            (i = this.length > 1 && !ot[e] ? v.unique(i) : i),
            this.length > 1 && rt.test(e) && (i = i.reverse()),
            this.pushStack(i, e, l.call(arguments).join(","))
          );
        };
      }
    ),
    v.extend({
      filter: function filter(e, t, n) {
        return (
          n && (e = ":not(" + e + ")"),
          t.length === 1
            ? v.find.matchesSelector(t[0], e)
              ? [t[0]]
              : []
            : v.find.matches(e, t)
        );
      },
      dir: function dir(e, n, r) {
        var i = [],
          s = e[n];

        while (
          s &&
          s.nodeType !== 9 &&
          (r === t || s.nodeType !== 1 || !v(s).is(r))
        ) {
          s.nodeType === 1 && i.push(s), (s = s[n]);
        }

        return i;
      },
      sibling: function sibling(e, t) {
        var n = [];

        for (; e; e = e.nextSibling) {
          e.nodeType === 1 && e !== t && n.push(e);
        }

        return n;
      },
    });
  var ct =
      "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
    ht = / jQuery\d+="(?:null|\d+)"/g,
    pt = /^\s+/,
    dt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
    vt = /<([\w:]+)/,
    mt = /<tbody/i,
    gt = /<|&#?\w+;/,
    yt = /<(?:script|style|link)/i,
    bt = /<(?:script|object|embed|option|style)/i,
    wt = new RegExp("<(?:" + ct + ")[\\s/>]", "i"),
    Et = /^(?:checkbox|radio)$/,
    St = /checked\s*(?:[^=]|=\s*.checked.)/i,
    xt = /\/(java|ecma)script/i,
    Tt = /^\s*<!(?:\[CDATA\[|\-\-)|[\]\-]{2}>\s*$/g,
    Nt = {
      option: [1, "<select multiple='multiple'>", "</select>"],
      legend: [1, "<fieldset>", "</fieldset>"],
      thead: [1, "<table>", "</table>"],
      tr: [2, "<table><tbody>", "</tbody></table>"],
      td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
      col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
      area: [1, "<map>", "</map>"],
      _default: [0, "", ""],
    },
    Ct = lt(i),
    kt = Ct.appendChild(i.createElement("div"));
  (Nt.optgroup = Nt.option),
    (Nt.tbody = Nt.tfoot = Nt.colgroup = Nt.caption = Nt.thead),
    (Nt.th = Nt.td),
    v.support.htmlSerialize || (Nt._default = [1, "X<div>", "</div>"]),
    v.fn.extend({
      text: function text(e) {
        return v.access(
          this,
          function (e) {
            return e === t
              ? v.text(this)
              : this.empty().append(
                  ((this[0] && this[0].ownerDocument) || i).createTextNode(e)
                );
          },
          null,
          e,
          arguments.length
        );
      },
      wrapAll: function wrapAll(e) {
        if (v.isFunction(e))
          return this.each(function (t) {
            v(this).wrapAll(e.call(this, t));
          });

        if (this[0]) {
          var t = v(e, this[0].ownerDocument).eq(0).clone(!0);
          this[0].parentNode && t.insertBefore(this[0]),
            t
              .map(function () {
                var e = this;

                while (e.firstChild && e.firstChild.nodeType === 1) {
                  e = e.firstChild;
                }

                return e;
              })
              .append(this);
        }

        return this;
      },
      wrapInner: function wrapInner(e) {
        return v.isFunction(e)
          ? this.each(function (t) {
              v(this).wrapInner(e.call(this, t));
            })
          : this.each(function () {
              var t = v(this),
                n = t.contents();
              n.length ? n.wrapAll(e) : t.append(e);
            });
      },
      wrap: function wrap(e) {
        var t = v.isFunction(e);
        return this.each(function (n) {
          v(this).wrapAll(t ? e.call(this, n) : e);
        });
      },
      unwrap: function unwrap() {
        return this.parent()
          .each(function () {
            v.nodeName(this, "body") || v(this).replaceWith(this.childNodes);
          })
          .end();
      },
      append: function append() {
        return this.domManip(arguments, !0, function (e) {
          (this.nodeType === 1 || this.nodeType === 11) && this.appendChild(e);
        });
      },
      prepend: function prepend() {
        return this.domManip(arguments, !0, function (e) {
          (this.nodeType === 1 || this.nodeType === 11) &&
            this.insertBefore(e, this.firstChild);
        });
      },
      before: function before() {
        if (!ut(this[0]))
          return this.domManip(arguments, !1, function (e) {
            this.parentNode.insertBefore(e, this);
          });

        if (arguments.length) {
          var e = v.clean(arguments);
          return this.pushStack(v.merge(e, this), "before", this.selector);
        }
      },
      after: function after() {
        if (!ut(this[0]))
          return this.domManip(arguments, !1, function (e) {
            this.parentNode.insertBefore(e, this.nextSibling);
          });

        if (arguments.length) {
          var e = v.clean(arguments);
          return this.pushStack(v.merge(this, e), "after", this.selector);
        }
      },
      remove: function remove(e, t) {
        var n,
          r = 0;

        for (; (n = this[r]) != null; r++) {
          if (!e || v.filter(e, [n]).length)
            !t &&
              n.nodeType === 1 &&
              (v.cleanData(n.getElementsByTagName("*")), v.cleanData([n])),
              n.parentNode && n.parentNode.removeChild(n);
        }

        return this;
      },
      empty: function empty() {
        var e,
          t = 0;

        for (; (e = this[t]) != null; t++) {
          e.nodeType === 1 && v.cleanData(e.getElementsByTagName("*"));

          while (e.firstChild) {
            e.removeChild(e.firstChild);
          }
        }

        return this;
      },
      clone: function clone(e, t) {
        return (
          (e = e == null ? !1 : e),
          (t = t == null ? e : t),
          this.map(function () {
            return v.clone(this, e, t);
          })
        );
      },
      html: function html(e) {
        return v.access(
          this,
          function (e) {
            var n = this[0] || {},
              r = 0,
              i = this.length;
            if (e === t)
              return n.nodeType === 1 ? n.innerHTML.replace(ht, "") : t;

            if (
              typeof e == "string" &&
              !yt.test(e) &&
              (v.support.htmlSerialize || !wt.test(e)) &&
              (v.support.leadingWhitespace || !pt.test(e)) &&
              !Nt[(vt.exec(e) || ["", ""])[1].toLowerCase()]
            ) {
              e = e.replace(dt, "<$1></$2>");

              try {
                for (; r < i; r++) {
                  (n = this[r] || {}),
                    n.nodeType === 1 &&
                      (v.cleanData(n.getElementsByTagName("*")),
                      (n.innerHTML = e));
                }

                n = 0;
              } catch (s) {}
            }

            n && this.empty().append(e);
          },
          null,
          e,
          arguments.length
        );
      },
      replaceWith: function replaceWith(e) {
        return ut(this[0])
          ? this.length
            ? this.pushStack(v(v.isFunction(e) ? e() : e), "replaceWith", e)
            : this
          : v.isFunction(e)
          ? this.each(function (t) {
              var n = v(this),
                r = n.html();
              n.replaceWith(e.call(this, t, r));
            })
          : (typeof e != "string" && (e = v(e).detach()),
            this.each(function () {
              var t = this.nextSibling,
                n = this.parentNode;
              v(this).remove(), t ? v(t).before(e) : v(n).append(e);
            }));
      },
      detach: function detach(e) {
        return this.remove(e, !0);
      },
      domManip: function domManip(e, n, r) {
        e = [].concat.apply([], e);
        var i,
          s,
          o,
          u,
          a = 0,
          f = e[0],
          l = [],
          c = this.length;
        if (
          !v.support.checkClone &&
          c > 1 &&
          typeof f == "string" &&
          St.test(f)
        )
          return this.each(function () {
            v(this).domManip(e, n, r);
          });
        if (v.isFunction(f))
          return this.each(function (i) {
            var s = v(this);
            (e[0] = f.call(this, i, n ? s.html() : t)), s.domManip(e, n, r);
          });

        if (this[0]) {
          (i = v.buildFragment(e, this, l)),
            (o = i.fragment),
            (s = o.firstChild),
            o.childNodes.length === 1 && (o = s);

          if (s) {
            n = n && v.nodeName(s, "tr");

            for (u = i.cacheable || c - 1; a < c; a++) {
              r.call(
                n && v.nodeName(this[a], "table")
                  ? Lt(this[a], "tbody")
                  : this[a],
                a === u ? o : v.clone(o, !0, !0)
              );
            }
          }

          (o = s = null),
            l.length &&
              v.each(l, function (e, t) {
                t.src
                  ? v.ajax
                    ? v.ajax({
                        url: t.src,
                        type: "GET",
                        dataType: "script",
                        async: !1,
                        global: !1,
                        throws: !0,
                      })
                    : v.error("no ajax")
                  : v.globalEval(
                      (t.text || t.textContent || t.innerHTML || "").replace(
                        Tt,
                        ""
                      )
                    ),
                  t.parentNode && t.parentNode.removeChild(t);
              });
        }

        return this;
      },
    }),
    (v.buildFragment = function (e, n, r) {
      var s,
        o,
        u,
        a = e[0];
      return (
        (n = n || i),
        (n = (!n.nodeType && n[0]) || n),
        (n = n.ownerDocument || n),
        e.length === 1 &&
          typeof a == "string" &&
          a.length < 512 &&
          n === i &&
          a.charAt(0) === "<" &&
          !bt.test(a) &&
          (v.support.checkClone || !St.test(a)) &&
          (v.support.html5Clone || !wt.test(a)) &&
          ((o = !0), (s = v.fragments[a]), (u = s !== t)),
        s ||
          ((s = n.createDocumentFragment()),
          v.clean(e, n, s, r),
          o && (v.fragments[a] = u && s)),
        {
          fragment: s,
          cacheable: o,
        }
      );
    }),
    (v.fragments = {}),
    v.each(
      {
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith",
      },
      function (e, t) {
        v.fn[e] = function (n) {
          var r,
            i = 0,
            s = [],
            o = v(n),
            u = o.length,
            a = this.length === 1 && this[0].parentNode;
          if (
            (a == null ||
              (a && a.nodeType === 11 && a.childNodes.length === 1)) &&
            u === 1
          )
            return o[t](this[0]), this;

          for (; i < u; i++) {
            (r = (i > 0 ? this.clone(!0) : this).get()),
              v(o[i])[t](r),
              (s = s.concat(r));
          }

          return this.pushStack(s, e, o.selector);
        };
      }
    ),
    v.extend({
      clone: function clone(e, t, n) {
        var r, i, s, o;
        v.support.html5Clone ||
        v.isXMLDoc(e) ||
        !wt.test("<" + e.nodeName + ">")
          ? (o = e.cloneNode(!0))
          : ((kt.innerHTML = e.outerHTML), kt.removeChild((o = kt.firstChild)));

        if (
          (!v.support.noCloneEvent || !v.support.noCloneChecked) &&
          (e.nodeType === 1 || e.nodeType === 11) &&
          !v.isXMLDoc(e)
        ) {
          Ot(e, o), (r = Mt(e)), (i = Mt(o));

          for (s = 0; r[s]; ++s) {
            i[s] && Ot(r[s], i[s]);
          }
        }

        if (t) {
          At(e, o);

          if (n) {
            (r = Mt(e)), (i = Mt(o));

            for (s = 0; r[s]; ++s) {
              At(r[s], i[s]);
            }
          }
        }

        return (r = i = null), o;
      },
      clean: function clean(e, t, n, r) {
        var s,
          o,
          u,
          a,
          f,
          l,
          c,
          h,
          p,
          d,
          m,
          g,
          y = t === i && Ct,
          b = [];
        if (!t || typeof t.createDocumentFragment == "undefined") t = i;

        for (s = 0; (u = e[s]) != null; s++) {
          typeof u == "number" && (u += "");
          if (!u) continue;
          if (typeof u == "string")
            if (!gt.test(u)) u = t.createTextNode(u);
            else {
              (y = y || lt(t)),
                (c = t.createElement("div")),
                y.appendChild(c),
                (u = u.replace(dt, "<$1></$2>")),
                (a = (vt.exec(u) || ["", ""])[1].toLowerCase()),
                (f = Nt[a] || Nt._default),
                (l = f[0]),
                (c.innerHTML = f[1] + u + f[2]);

              while (l--) {
                c = c.lastChild;
              }

              if (!v.support.tbody) {
                (h = mt.test(u)),
                  (p =
                    a === "table" && !h
                      ? c.firstChild && c.firstChild.childNodes
                      : f[1] === "<table>" && !h
                      ? c.childNodes
                      : []);

                for (o = p.length - 1; o >= 0; --o) {
                  v.nodeName(p[o], "tbody") &&
                    !p[o].childNodes.length &&
                    p[o].parentNode.removeChild(p[o]);
                }
              }

              !v.support.leadingWhitespace &&
                pt.test(u) &&
                c.insertBefore(t.createTextNode(pt.exec(u)[0]), c.firstChild),
                (u = c.childNodes),
                c.parentNode.removeChild(c);
            }
          u.nodeType ? b.push(u) : v.merge(b, u);
        }

        c && (u = c = y = null);
        if (!v.support.appendChecked)
          for (s = 0; (u = b[s]) != null; s++) {
            v.nodeName(u, "input")
              ? _t(u)
              : typeof u.getElementsByTagName != "undefined" &&
                v.grep(u.getElementsByTagName("input"), _t);
          }

        if (n) {
          m = function m(e) {
            if (!e.type || xt.test(e.type))
              return r
                ? r.push(e.parentNode ? e.parentNode.removeChild(e) : e)
                : n.appendChild(e);
          };

          for (s = 0; (u = b[s]) != null; s++) {
            if (!v.nodeName(u, "script") || !m(u))
              n.appendChild(u),
                typeof u.getElementsByTagName != "undefined" &&
                  ((g = v.grep(
                    v.merge([], u.getElementsByTagName("script")),
                    m
                  )),
                  b.splice.apply(b, [s + 1, 0].concat(g)),
                  (s += g.length));
          }
        }

        return b;
      },
      cleanData: function cleanData(e, t) {
        var n,
          r,
          i,
          s,
          o = 0,
          u = v.expando,
          a = v.cache,
          f = v.support.deleteExpando,
          l = v.event.special;

        for (; (i = e[o]) != null; o++) {
          if (t || v.acceptData(i)) {
            (r = i[u]), (n = r && a[r]);

            if (n) {
              if (n.events)
                for (s in n.events) {
                  l[s] ? v.event.remove(i, s) : v.removeEvent(i, s, n.handle);
                }
              a[r] &&
                (delete a[r],
                f
                  ? delete i[u]
                  : i.removeAttribute
                  ? i.removeAttribute(u)
                  : (i[u] = null),
                v.deletedIds.push(r));
            }
          }
        }
      },
    }),
    (function () {
      var e, t;
      (v.uaMatch = function (e) {
        e = e.toLowerCase();
        var t =
          /(chrome)[ \/]([\w.]+)/.exec(e) ||
          /(webkit)[ \/]([\w.]+)/.exec(e) ||
          /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(e) ||
          /(msie) ([\w.]+)/.exec(e) ||
          (e.indexOf("compatible") < 0 &&
            /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(e)) ||
          [];
        return {
          browser: t[1] || "",
          version: t[2] || "0",
        };
      }),
        (e = v.uaMatch(o.userAgent)),
        (t = {}),
        e.browser && ((t[e.browser] = !0), (t.version = e.version)),
        t.chrome ? (t.webkit = !0) : t.webkit && (t.safari = !0),
        (v.browser = t),
        (v.sub = function () {
          function e(t, n) {
            return new e.fn.init(t, n);
          }

          v.extend(!0, e, this),
            (e.superclass = this),
            (e.fn = e.prototype = this()),
            (e.fn.constructor = e),
            (e.sub = this.sub),
            (e.fn.init = function (r, i) {
              return (
                i && i instanceof v && !(i instanceof e) && (i = e(i)),
                v.fn.init.call(this, r, i, t)
              );
            }),
            (e.fn.init.prototype = e.fn);
          var t = e(i);
          return e;
        });
    })();
  var Dt,
    Pt,
    Ht,
    Bt = /alpha\([^)]*\)/i,
    jt = /opacity=([^)]*)/,
    Ft = /^(top|right|bottom|left)$/,
    It = /^(none|table(?!-c[ea]).+)/,
    qt = /^margin/,
    Rt = new RegExp("^(" + m + ")(.*)$", "i"),
    Ut = new RegExp("^(" + m + ")(?!px)[a-z%]+$", "i"),
    zt = new RegExp("^([-+])=(" + m + ")", "i"),
    Wt = {
      BODY: "block",
    },
    Xt = {
      position: "absolute",
      visibility: "hidden",
      display: "block",
    },
    Vt = {
      letterSpacing: 0,
      fontWeight: 400,
    },
    $t = ["Top", "Right", "Bottom", "Left"],
    Jt = ["Webkit", "O", "Moz", "ms"],
    Kt = v.fn.toggle;
  v.fn.extend({
    css: function css(e, n) {
      return v.access(
        this,
        function (e, n, r) {
          return r !== t ? v.style(e, n, r) : v.css(e, n);
        },
        e,
        n,
        arguments.length > 1
      );
    },
    show: function show() {
      return Yt(this, !0);
    },
    hide: function hide() {
      return Yt(this);
    },
    toggle: function toggle(e, t) {
      var n = typeof e == "boolean";
      return v.isFunction(e) && v.isFunction(t)
        ? Kt.apply(this, arguments)
        : this.each(function () {
            (n ? e : Gt(this)) ? v(this).show() : v(this).hide();
          });
    },
  }),
    v.extend({
      cssHooks: {
        opacity: {
          get: function get(e, t) {
            if (t) {
              var n = Dt(e, "opacity");
              return n === "" ? "1" : n;
            }
          },
        },
      },
      cssNumber: {
        fillOpacity: !0,
        fontWeight: !0,
        lineHeight: !0,
        opacity: !0,
        orphans: !0,
        widows: !0,
        zIndex: !0,
        zoom: !0,
      },
      cssProps: {
        float: v.support.cssFloat ? "cssFloat" : "styleFloat",
      },
      style: function style(e, n, r, i) {
        if (!e || e.nodeType === 3 || e.nodeType === 8 || !e.style) return;
        var s,
          o,
          u,
          a = v.camelCase(n),
          f = e.style;
        (n = v.cssProps[a] || (v.cssProps[a] = Qt(f, a))),
          (u = v.cssHooks[n] || v.cssHooks[a]);
        if (r === t)
          return u && "get" in u && (s = u.get(e, !1, i)) !== t ? s : f[n];
        (o = _typeof(r)),
          o === "string" &&
            (s = zt.exec(r)) &&
            ((r = (s[1] + 1) * s[2] + parseFloat(v.css(e, n))), (o = "number"));
        if (r == null || (o === "number" && isNaN(r))) return;
        o === "number" && !v.cssNumber[a] && (r += "px");
        if (!u || !("set" in u) || (r = u.set(e, r, i)) !== t)
          try {
            f[n] = r;
          } catch (l) {}
      },
      css: function css(e, n, r, i) {
        var s,
          o,
          u,
          a = v.camelCase(n);
        return (
          (n = v.cssProps[a] || (v.cssProps[a] = Qt(e.style, a))),
          (u = v.cssHooks[n] || v.cssHooks[a]),
          u && "get" in u && (s = u.get(e, !0, i)),
          s === t && (s = Dt(e, n)),
          s === "normal" && n in Vt && (s = Vt[n]),
          r || i !== t
            ? ((o = parseFloat(s)), r || v.isNumeric(o) ? o || 0 : s)
            : s
        );
      },
      swap: function swap(e, t, n) {
        var r,
          i,
          s = {};

        for (i in t) {
          (s[i] = e.style[i]), (e.style[i] = t[i]);
        }

        r = n.call(e);

        for (i in t) {
          e.style[i] = s[i];
        }

        return r;
      },
    }),
    e.getComputedStyle
      ? (Dt = function Dt(t, n) {
          var r,
            i,
            s,
            o,
            u = e.getComputedStyle(t, null),
            a = t.style;
          return (
            u &&
              ((r = u.getPropertyValue(n) || u[n]),
              r === "" &&
                !v.contains(t.ownerDocument, t) &&
                (r = v.style(t, n)),
              Ut.test(r) &&
                qt.test(n) &&
                ((i = a.width),
                (s = a.minWidth),
                (o = a.maxWidth),
                (a.minWidth = a.maxWidth = a.width = r),
                (r = u.width),
                (a.width = i),
                (a.minWidth = s),
                (a.maxWidth = o))),
            r
          );
        })
      : i.documentElement.currentStyle &&
        (Dt = function Dt(e, t) {
          var n,
            r,
            i = e.currentStyle && e.currentStyle[t],
            s = e.style;
          return (
            i == null && s && s[t] && (i = s[t]),
            Ut.test(i) &&
              !Ft.test(t) &&
              ((n = s.left),
              (r = e.runtimeStyle && e.runtimeStyle.left),
              r && (e.runtimeStyle.left = e.currentStyle.left),
              (s.left = t === "fontSize" ? "1em" : i),
              (i = s.pixelLeft + "px"),
              (s.left = n),
              r && (e.runtimeStyle.left = r)),
            i === "" ? "auto" : i
          );
        }),
    v.each(["height", "width"], function (e, t) {
      v.cssHooks[t] = {
        get: function get(e, n, r) {
          if (n)
            return e.offsetWidth === 0 && It.test(Dt(e, "display"))
              ? v.swap(e, Xt, function () {
                  return tn(e, t, r);
                })
              : tn(e, t, r);
        },
        set: function set(e, n, r) {
          return Zt(
            e,
            n,
            r
              ? en(
                  e,
                  t,
                  r,
                  v.support.boxSizing && v.css(e, "boxSizing") === "border-box"
                )
              : 0
          );
        },
      };
    }),
    v.support.opacity ||
      (v.cssHooks.opacity = {
        get: function get(e, t) {
          return jt.test(
            (t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || ""
          )
            ? 0.01 * parseFloat(RegExp.$1) + ""
            : t
            ? "1"
            : "";
        },
        set: function set(e, t) {
          var n = e.style,
            r = e.currentStyle,
            i = v.isNumeric(t) ? "alpha(opacity=" + t * 100 + ")" : "",
            s = (r && r.filter) || n.filter || "";
          n.zoom = 1;

          if (t >= 1 && v.trim(s.replace(Bt, "")) === "" && n.removeAttribute) {
            n.removeAttribute("filter");
            if (r && !r.filter) return;
          }

          n.filter = Bt.test(s) ? s.replace(Bt, i) : s + " " + i;
        },
      }),
    v(function () {
      v.support.reliableMarginRight ||
        (v.cssHooks.marginRight = {
          get: function get(e, t) {
            return v.swap(
              e,
              {
                display: "inline-block",
              },
              function () {
                if (t) return Dt(e, "marginRight");
              }
            );
          },
        }),
        !v.support.pixelPosition &&
          v.fn.position &&
          v.each(["top", "left"], function (e, t) {
            v.cssHooks[t] = {
              get: function get(e, n) {
                if (n) {
                  var r = Dt(e, t);
                  return Ut.test(r) ? v(e).position()[t] + "px" : r;
                }
              },
            };
          });
    }),
    v.expr &&
      v.expr.filters &&
      ((v.expr.filters.hidden = function (e) {
        return (
          (e.offsetWidth === 0 && e.offsetHeight === 0) ||
          (!v.support.reliableHiddenOffsets &&
            ((e.style && e.style.display) || Dt(e, "display")) === "none")
        );
      }),
      (v.expr.filters.visible = function (e) {
        return !v.expr.filters.hidden(e);
      })),
    v.each(
      {
        margin: "",
        padding: "",
        border: "Width",
      },
      function (e, t) {
        (v.cssHooks[e + t] = {
          expand: function expand(n) {
            var r,
              i = typeof n == "string" ? n.split(" ") : [n],
              s = {};

            for (r = 0; r < 4; r++) {
              s[e + $t[r] + t] = i[r] || i[r - 2] || i[0];
            }

            return s;
          },
        }),
          qt.test(e) || (v.cssHooks[e + t].set = Zt);
      }
    );
  var rn = /%20/g,
    sn = /\[\]$/,
    on = /\r?\n/g,
    un = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,
    an = /^(?:select|textarea)/i;
  v.fn.extend({
    serialize: function serialize() {
      return v.param(this.serializeArray());
    },
    serializeArray: function serializeArray() {
      return this.map(function () {
        return this.elements ? v.makeArray(this.elements) : this;
      })
        .filter(function () {
          return (
            this.name &&
            !this.disabled &&
            (this.checked || an.test(this.nodeName) || un.test(this.type))
          );
        })
        .map(function (e, t) {
          var n = v(this).val();
          return n == null
            ? null
            : v.isArray(n)
            ? v.map(n, function (e, n) {
                return {
                  name: t.name,
                  value: e.replace(on, "\r\n"),
                };
              })
            : {
                name: t.name,
                value: n.replace(on, "\r\n"),
              };
        })
        .get();
    },
  }),
    (v.param = function (e, n) {
      var r,
        i = [],
        s = function s(e, t) {
          (t = v.isFunction(t) ? t() : t == null ? "" : t),
            (i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t));
        };

      n === t && (n = v.ajaxSettings && v.ajaxSettings.traditional);
      if (v.isArray(e) || (e.jquery && !v.isPlainObject(e)))
        v.each(e, function () {
          s(this.name, this.value);
        });
      else
        for (r in e) {
          fn(r, e[r], n, s);
        }
      return i.join("&").replace(rn, "+");
    });
  var ln,
    cn,
    hn = /#.*$/,
    pn = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
    dn = /^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/,
    vn = /^(?:GET|HEAD)$/,
    mn = /^\/\//,
    gn = /\?/,
    yn = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
    bn = /([?&])_=[^&]*/,
    wn = /^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,
    En = v.fn.load,
    Sn = {},
    xn = {},
    Tn = ["*/"] + ["*"];

  try {
    cn = s.href;
  } catch (Nn) {
    (cn = i.createElement("a")), (cn.href = ""), (cn = cn.href);
  }

  (ln = wn.exec(cn.toLowerCase()) || []),
    (v.fn.load = function (e, n, r) {
      if (typeof e != "string" && En) return En.apply(this, arguments);
      if (!this.length) return this;
      var i,
        s,
        o,
        u = this,
        a = e.indexOf(" ");
      return (
        a >= 0 && ((i = e.slice(a, e.length)), (e = e.slice(0, a))),
        v.isFunction(n)
          ? ((r = n), (n = t))
          : n && _typeof(n) == "object" && (s = "POST"),
        v
          .ajax({
            url: e,
            type: s,
            dataType: "html",
            data: n,
            complete: function complete(e, t) {
              r && u.each(r, o || [e.responseText, t, e]);
            },
          })
          .done(function (e) {
            (o = arguments),
              u.html(i ? v("<div>").append(e.replace(yn, "")).find(i) : e);
          }),
        this
      );
    }),
    v.each(
      "ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(
        " "
      ),
      function (e, t) {
        v.fn[t] = function (e) {
          return this.on(t, e);
        };
      }
    ),
    v.each(["get", "post"], function (e, n) {
      v[n] = function (e, r, i, s) {
        return (
          v.isFunction(r) && ((s = s || i), (i = r), (r = t)),
          v.ajax({
            type: n,
            url: e,
            data: r,
            success: i,
            dataType: s,
          })
        );
      };
    }),
    v.extend({
      getScript: function getScript(e, n) {
        return v.get(e, t, n, "script");
      },
      getJSON: function getJSON(e, t, n) {
        return v.get(e, t, n, "json");
      },
      ajaxSetup: function ajaxSetup(e, t) {
        return (
          t ? Ln(e, v.ajaxSettings) : ((t = e), (e = v.ajaxSettings)),
          Ln(e, t),
          e
        );
      },
      ajaxSettings: {
        url: cn,
        isLocal: dn.test(ln[1]),
        global: !0,
        type: "GET",
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        processData: !0,
        async: !0,
        accepts: {
          xml: "application/xml, text/xml",
          html: "text/html",
          text: "text/plain",
          json: "application/json, text/javascript",
          "*": Tn,
        },
        contents: {
          xml: /xml/,
          html: /html/,
          json: /json/,
        },
        responseFields: {
          xml: "responseXML",
          text: "responseText",
        },
        converters: {
          "* text": e.String,
          "text html": !0,
          "text json": v.parseJSON,
          "text xml": v.parseXML,
        },
        flatOptions: {
          context: !0,
          url: !0,
        },
      },
      ajaxPrefilter: Cn(Sn),
      ajaxTransport: Cn(xn),
      ajax: function ajax(e, n) {
        function T(e, n, s, a) {
          var l,
            y,
            b,
            w,
            S,
            T = n;
          if (E === 2) return;
          (E = 2),
            u && clearTimeout(u),
            (o = t),
            (i = a || ""),
            (x.readyState = e > 0 ? 4 : 0),
            s && (w = An(c, x, s));
          if ((e >= 200 && e < 300) || e === 304)
            c.ifModified &&
              ((S = x.getResponseHeader("Last-Modified")),
              S && (v.lastModified[r] = S),
              (S = x.getResponseHeader("Etag")),
              S && (v.etag[r] = S)),
              e === 304
                ? ((T = "notmodified"), (l = !0))
                : ((l = On(c, w)),
                  (T = l.state),
                  (y = l.data),
                  (b = l.error),
                  (l = !b));
          else {
            b = T;
            if (!T || e) (T = "error"), e < 0 && (e = 0);
          }
          (x.status = e),
            (x.statusText = (n || T) + ""),
            l ? d.resolveWith(h, [y, T, x]) : d.rejectWith(h, [x, T, b]),
            x.statusCode(g),
            (g = t),
            f &&
              p.trigger("ajax" + (l ? "Success" : "Error"), [x, c, l ? y : b]),
            m.fireWith(h, [x, T]),
            f &&
              (p.trigger("ajaxComplete", [x, c]),
              --v.active || v.event.trigger("ajaxStop"));
        }

        _typeof(e) == "object" && ((n = e), (e = t)), (n = n || {});
        var r,
          i,
          s,
          o,
          u,
          a,
          f,
          l,
          c = v.ajaxSetup({}, n),
          h = c.context || c,
          p = h !== c && (h.nodeType || h instanceof v) ? v(h) : v.event,
          d = v.Deferred(),
          m = v.Callbacks("once memory"),
          g = c.statusCode || {},
          b = {},
          w = {},
          E = 0,
          S = "canceled",
          x = {
            readyState: 0,
            setRequestHeader: function setRequestHeader(e, t) {
              if (!E) {
                var n = e.toLowerCase();
                (e = w[n] = w[n] || e), (b[e] = t);
              }

              return this;
            },
            getAllResponseHeaders: function getAllResponseHeaders() {
              return E === 2 ? i : null;
            },
            getResponseHeader: function getResponseHeader(e) {
              var n;

              if (E === 2) {
                if (!s) {
                  s = {};

                  while ((n = pn.exec(i))) {
                    s[n[1].toLowerCase()] = n[2];
                  }
                }

                n = s[e.toLowerCase()];
              }

              return n === t ? null : n;
            },
            overrideMimeType: function overrideMimeType(e) {
              return E || (c.mimeType = e), this;
            },
            abort: function abort(e) {
              return (e = e || S), o && o.abort(e), T(0, e), this;
            },
          };
        d.promise(x),
          (x.success = x.done),
          (x.error = x.fail),
          (x.complete = m.add),
          (x.statusCode = function (e) {
            if (e) {
              var t;
              if (E < 2)
                for (t in e) {
                  g[t] = [g[t], e[t]];
                }
              else (t = e[x.status]), x.always(t);
            }

            return this;
          }),
          (c.url = ((e || c.url) + "")
            .replace(hn, "")
            .replace(mn, ln[1] + "//")),
          (c.dataTypes = v
            .trim(c.dataType || "*")
            .toLowerCase()
            .split(y)),
          c.crossDomain == null &&
            ((a = wn.exec(c.url.toLowerCase())),
            (c.crossDomain = !(
              !a ||
              (a[1] === ln[1] &&
                a[2] === ln[2] &&
                (a[3] || (a[1] === "http:" ? 80 : 443)) ==
                  (ln[3] || (ln[1] === "http:" ? 80 : 443)))
            ))),
          c.data &&
            c.processData &&
            typeof c.data != "string" &&
            (c.data = v.param(c.data, c.traditional)),
          kn(Sn, c, n, x);
        if (E === 2) return x;
        (f = c.global),
          (c.type = c.type.toUpperCase()),
          (c.hasContent = !vn.test(c.type)),
          f && v.active++ === 0 && v.event.trigger("ajaxStart");

        if (!c.hasContent) {
          c.data &&
            ((c.url += (gn.test(c.url) ? "&" : "?") + c.data), delete c.data),
            (r = c.url);

          if (c.cache === !1) {
            var N = v.now(),
              C = c.url.replace(bn, "$1_=" + N);
            c.url =
              C + (C === c.url ? (gn.test(c.url) ? "&" : "?") + "_=" + N : "");
          }
        }

        ((c.data && c.hasContent && c.contentType !== !1) || n.contentType) &&
          x.setRequestHeader("Content-Type", c.contentType),
          c.ifModified &&
            ((r = r || c.url),
            v.lastModified[r] &&
              x.setRequestHeader("If-Modified-Since", v.lastModified[r]),
            v.etag[r] && x.setRequestHeader("If-None-Match", v.etag[r])),
          x.setRequestHeader(
            "Accept",
            c.dataTypes[0] && c.accepts[c.dataTypes[0]]
              ? c.accepts[c.dataTypes[0]] +
                  (c.dataTypes[0] !== "*" ? ", " + Tn + "; q=0.01" : "")
              : c.accepts["*"]
          );

        for (l in c.headers) {
          x.setRequestHeader(l, c.headers[l]);
        }

        if (!c.beforeSend || (c.beforeSend.call(h, x, c) !== !1 && E !== 2)) {
          S = "abort";

          for (l in {
            success: 1,
            error: 1,
            complete: 1,
          }) {
            x[l](c[l]);
          }

          o = kn(xn, c, n, x);
          if (!o) T(-1, "No Transport");
          else {
            (x.readyState = 1),
              f && p.trigger("ajaxSend", [x, c]),
              c.async &&
                c.timeout > 0 &&
                (u = setTimeout(function () {
                  x.abort("timeout");
                }, c.timeout));

            try {
              (E = 1), o.send(b, T);
            } catch (k) {
              if (!(E < 2)) throw k;
              T(-1, k);
            }
          }
          return x;
        }

        return x.abort();
      },
      active: 0,
      lastModified: {},
      etag: {},
    });
  var Mn = [],
    _n = /\?/,
    Dn = /(=)\?(?=&|$)|\?\?/,
    Pn = v.now();
  v.ajaxSetup({
    jsonp: "callback",
    jsonpCallback: function jsonpCallback() {
      var e = Mn.pop() || v.expando + "_" + Pn++;
      return (this[e] = !0), e;
    },
  }),
    v.ajaxPrefilter("json jsonp", function (n, r, i) {
      var s,
        o,
        u,
        a = n.data,
        f = n.url,
        l = n.jsonp !== !1,
        c = l && Dn.test(f),
        h =
          l &&
          !c &&
          typeof a == "string" &&
          !(n.contentType || "").indexOf("application/x-www-form-urlencoded") &&
          Dn.test(a);
      if (n.dataTypes[0] === "jsonp" || c || h)
        return (
          (s = n.jsonpCallback = v.isFunction(n.jsonpCallback)
            ? n.jsonpCallback()
            : n.jsonpCallback),
          (o = e[s]),
          c
            ? (n.url = f.replace(Dn, "$1" + s))
            : h
            ? (n.data = a.replace(Dn, "$1" + s))
            : l && (n.url += (_n.test(f) ? "&" : "?") + n.jsonp + "=" + s),
          (n.converters["script json"] = function () {
            return u || v.error(s + " was not called"), u[0];
          }),
          (n.dataTypes[0] = "json"),
          (e[s] = function () {
            u = arguments;
          }),
          i.always(function () {
            (e[s] = o),
              n[s] && ((n.jsonpCallback = r.jsonpCallback), Mn.push(s)),
              u && v.isFunction(o) && o(u[0]),
              (u = o = t);
          }),
          "script"
        );
    }),
    v.ajaxSetup({
      accepts: {
        script:
          "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript",
      },
      contents: {
        script: /javascript|ecmascript/,
      },
      converters: {
        "text script": function textScript(e) {
          return v.globalEval(e), e;
        },
      },
    }),
    v.ajaxPrefilter("script", function (e) {
      e.cache === t && (e.cache = !1),
        e.crossDomain && ((e.type = "GET"), (e.global = !1));
    }),
    v.ajaxTransport("script", function (e) {
      if (e.crossDomain) {
        var n,
          r = i.head || i.getElementsByTagName("head")[0] || i.documentElement;
        return {
          send: function send(s, o) {
            (n = i.createElement("script")),
              (n.async = "async"),
              e.scriptCharset && (n.charset = e.scriptCharset),
              (n.src = e.url),
              (n.onload = n.onreadystatechange = function (e, i) {
                if (i || !n.readyState || /loaded|complete/.test(n.readyState))
                  (n.onload = n.onreadystatechange = null),
                    r && n.parentNode && r.removeChild(n),
                    (n = t),
                    i || o(200, "success");
              }),
              r.insertBefore(n, r.firstChild);
          },
          abort: function abort() {
            n && n.onload(0, 1);
          },
        };
      }
    });
  var Hn,
    Bn = e.ActiveXObject
      ? function () {
          for (var e in Hn) {
            Hn[e](0, 1);
          }
        }
      : !1,
    jn = 0;
  (v.ajaxSettings.xhr = e.ActiveXObject
    ? function () {
        return (!this.isLocal && Fn()) || In();
      }
    : Fn),
    (function (e) {
      v.extend(v.support, {
        ajax: !!e,
        cors: !!e && "withCredentials" in e,
      });
    })(v.ajaxSettings.xhr()),
    v.support.ajax &&
      v.ajaxTransport(function (n) {
        if (!n.crossDomain || v.support.cors) {
          var _r;

          return {
            send: function send(i, s) {
              var o,
                u,
                a = n.xhr();
              n.username
                ? a.open(n.type, n.url, n.async, n.username, n.password)
                : a.open(n.type, n.url, n.async);
              if (n.xhrFields)
                for (u in n.xhrFields) {
                  a[u] = n.xhrFields[u];
                }
              n.mimeType &&
                a.overrideMimeType &&
                a.overrideMimeType(n.mimeType),
                !n.crossDomain &&
                  !i["X-Requested-With"] &&
                  (i["X-Requested-With"] = "XMLHttpRequest");

              try {
                for (u in i) {
                  a.setRequestHeader(u, i[u]);
                }
              } catch (f) {}

              a.send((n.hasContent && n.data) || null),
                (_r = function r(e, i) {
                  var u, f, l, c, h;

                  try {
                    if (_r && (i || a.readyState === 4)) {
                      (_r = t),
                        o &&
                          ((a.onreadystatechange = v.noop), Bn && delete Hn[o]);
                      if (i) a.readyState !== 4 && a.abort();
                      else {
                        (u = a.status),
                          (l = a.getAllResponseHeaders()),
                          (c = {}),
                          (h = a.responseXML),
                          h && h.documentElement && (c.xml = h);

                        try {
                          c.text = a.responseText;
                        } catch (p) {}

                        try {
                          f = a.statusText;
                        } catch (p) {
                          f = "";
                        }

                        !u && n.isLocal && !n.crossDomain
                          ? (u = c.text ? 200 : 404)
                          : u === 1223 && (u = 204);
                      }
                    }
                  } catch (d) {
                    i || s(-1, d);
                  }

                  c && s(u, f, c, l);
                }),
                n.async
                  ? a.readyState === 4
                    ? setTimeout(_r, 0)
                    : ((o = ++jn),
                      Bn && (Hn || ((Hn = {}), v(e).unload(Bn)), (Hn[o] = _r)),
                      (a.onreadystatechange = _r))
                  : _r();
            },
            abort: function abort() {
              _r && _r(0, 1);
            },
          };
        }
      });
  var qn,
    Rn,
    Un = /^(?:toggle|show|hide)$/,
    zn = new RegExp("^(?:([-+])=|)(" + m + ")([a-z%]*)$", "i"),
    Wn = /queueHooks$/,
    Xn = [Gn],
    Vn = {
      "*": [
        function (e, t) {
          var n,
            r,
            i = this.createTween(e, t),
            s = zn.exec(t),
            o = i.cur(),
            u = +o || 0,
            a = 1,
            f = 20;

          if (s) {
            (n = +s[2]), (r = s[3] || (v.cssNumber[e] ? "" : "px"));

            if (r !== "px" && u) {
              u = v.css(i.elem, e, !0) || n || 1;

              do {
                (a = a || ".5"), (u /= a), v.style(i.elem, e, u + r);
              } while (a !== (a = i.cur() / o) && a !== 1 && --f);
            }

            (i.unit = r),
              (i.start = u),
              (i.end = s[1] ? u + (s[1] + 1) * n : n);
          }

          return i;
        },
      ],
    };
  (v.Animation = v.extend(Kn, {
    tweener: function tweener(e, t) {
      v.isFunction(e) ? ((t = e), (e = ["*"])) : (e = e.split(" "));
      var n,
        r = 0,
        i = e.length;

      for (; r < i; r++) {
        (n = e[r]), (Vn[n] = Vn[n] || []), Vn[n].unshift(t);
      }
    },
    prefilter: function prefilter(e, t) {
      t ? Xn.unshift(e) : Xn.push(e);
    },
  })),
    (v.Tween = Yn),
    (Yn.prototype = {
      constructor: Yn,
      init: function init(e, t, n, r, i, s) {
        (this.elem = e),
          (this.prop = n),
          (this.easing = i || "swing"),
          (this.options = t),
          (this.start = this.now = this.cur()),
          (this.end = r),
          (this.unit = s || (v.cssNumber[n] ? "" : "px"));
      },
      cur: function cur() {
        var e = Yn.propHooks[this.prop];
        return e && e.get ? e.get(this) : Yn.propHooks._default.get(this);
      },
      run: function run(e) {
        var t,
          n = Yn.propHooks[this.prop];
        return (
          this.options.duration
            ? (this.pos = t = v.easing[this.easing](
                e,
                this.options.duration * e,
                0,
                1,
                this.options.duration
              ))
            : (this.pos = t = e),
          (this.now = (this.end - this.start) * t + this.start),
          this.options.step &&
            this.options.step.call(this.elem, this.now, this),
          n && n.set ? n.set(this) : Yn.propHooks._default.set(this),
          this
        );
      },
    }),
    (Yn.prototype.init.prototype = Yn.prototype),
    (Yn.propHooks = {
      _default: {
        get: function get(e) {
          var t;
          return e.elem[e.prop] == null ||
            (!!e.elem.style && e.elem.style[e.prop] != null)
            ? ((t = v.css(e.elem, e.prop, !1, "")), !t || t === "auto" ? 0 : t)
            : e.elem[e.prop];
        },
        set: function set(e) {
          v.fx.step[e.prop]
            ? v.fx.step[e.prop](e)
            : e.elem.style &&
              (e.elem.style[v.cssProps[e.prop]] != null || v.cssHooks[e.prop])
            ? v.style(e.elem, e.prop, e.now + e.unit)
            : (e.elem[e.prop] = e.now);
        },
      },
    }),
    (Yn.propHooks.scrollTop = Yn.propHooks.scrollLeft = {
      set: function set(e) {
        e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now);
      },
    }),
    v.each(["toggle", "show", "hide"], function (e, t) {
      var n = v.fn[t];

      v.fn[t] = function (r, i, s) {
        return r == null ||
          typeof r == "boolean" ||
          (!e && v.isFunction(r) && v.isFunction(i))
          ? n.apply(this, arguments)
          : this.animate(Zn(t, !0), r, i, s);
      };
    }),
    v.fn.extend({
      fadeTo: function fadeTo(e, t, n, r) {
        return this.filter(Gt).css("opacity", 0).show().end().animate(
          {
            opacity: t,
          },
          e,
          n,
          r
        );
      },
      animate: function animate(e, t, n, r) {
        var i = v.isEmptyObject(e),
          s = v.speed(t, n, r),
          o = function o() {
            var t = Kn(this, v.extend({}, e), s);
            i && t.stop(!0);
          };

        return i || s.queue === !1 ? this.each(o) : this.queue(s.queue, o);
      },
      stop: function stop(e, n, r) {
        var i = function i(e) {
          var t = e.stop;
          delete e.stop, t(r);
        };

        return (
          typeof e != "string" && ((r = n), (n = e), (e = t)),
          n && e !== !1 && this.queue(e || "fx", []),
          this.each(function () {
            var t = !0,
              n = e != null && e + "queueHooks",
              s = v.timers,
              o = v._data(this);

            if (n) o[n] && o[n].stop && i(o[n]);
            else
              for (n in o) {
                o[n] && o[n].stop && Wn.test(n) && i(o[n]);
              }

            for (n = s.length; n--; ) {
              s[n].elem === this &&
                (e == null || s[n].queue === e) &&
                (s[n].anim.stop(r), (t = !1), s.splice(n, 1));
            }

            (t || !r) && v.dequeue(this, e);
          })
        );
      },
    }),
    v.each(
      {
        slideDown: Zn("show"),
        slideUp: Zn("hide"),
        slideToggle: Zn("toggle"),
        fadeIn: {
          opacity: "show",
        },
        fadeOut: {
          opacity: "hide",
        },
        fadeToggle: {
          opacity: "toggle",
        },
      },
      function (e, t) {
        v.fn[e] = function (e, n, r) {
          return this.animate(t, e, n, r);
        };
      }
    ),
    (v.speed = function (e, t, n) {
      var r =
        e && _typeof(e) == "object"
          ? v.extend({}, e)
          : {
              complete: n || (!n && t) || (v.isFunction(e) && e),
              duration: e,
              easing: (n && t) || (t && !v.isFunction(t) && t),
            };
      r.duration = v.fx.off
        ? 0
        : typeof r.duration == "number"
        ? r.duration
        : r.duration in v.fx.speeds
        ? v.fx.speeds[r.duration]
        : v.fx.speeds._default;
      if (r.queue == null || r.queue === !0) r.queue = "fx";
      return (
        (r.old = r.complete),
        (r.complete = function () {
          v.isFunction(r.old) && r.old.call(this),
            r.queue && v.dequeue(this, r.queue);
        }),
        r
      );
    }),
    (v.easing = {
      linear: function linear(e) {
        return e;
      },
      swing: function swing(e) {
        return 0.5 - Math.cos(e * Math.PI) / 2;
      },
    }),
    (v.timers = []),
    (v.fx = Yn.prototype.init),
    (v.fx.tick = function () {
      var e,
        n = v.timers,
        r = 0;
      qn = v.now();

      for (; r < n.length; r++) {
        (e = n[r]), !e() && n[r] === e && n.splice(r--, 1);
      }

      n.length || v.fx.stop(), (qn = t);
    }),
    (v.fx.timer = function (e) {
      e() &&
        v.timers.push(e) &&
        !Rn &&
        (Rn = setInterval(v.fx.tick, v.fx.interval));
    }),
    (v.fx.interval = 13),
    (v.fx.stop = function () {
      clearInterval(Rn), (Rn = null);
    }),
    (v.fx.speeds = {
      slow: 600,
      fast: 200,
      _default: 400,
    }),
    (v.fx.step = {}),
    v.expr &&
      v.expr.filters &&
      (v.expr.filters.animated = function (e) {
        return v.grep(v.timers, function (t) {
          return e === t.elem;
        }).length;
      });
  var er = /^(?:body|html)$/i;
  (v.fn.offset = function (e) {
    if (arguments.length)
      return e === t
        ? this
        : this.each(function (t) {
            v.offset.setOffset(this, e, t);
          });
    var n,
      r,
      i,
      s,
      o,
      u,
      a,
      f = {
        top: 0,
        left: 0,
      },
      l = this[0],
      c = l && l.ownerDocument;
    if (!c) return;
    return (r = c.body) === l
      ? v.offset.bodyOffset(l)
      : ((n = c.documentElement),
        v.contains(n, l)
          ? (typeof l.getBoundingClientRect != "undefined" &&
              (f = l.getBoundingClientRect()),
            (i = tr(c)),
            (s = n.clientTop || r.clientTop || 0),
            (o = n.clientLeft || r.clientLeft || 0),
            (u = i.pageYOffset || n.scrollTop),
            (a = i.pageXOffset || n.scrollLeft),
            {
              top: f.top + u - s,
              left: f.left + a - o,
            })
          : f);
  }),
    (v.offset = {
      bodyOffset: function bodyOffset(e) {
        var t = e.offsetTop,
          n = e.offsetLeft;
        return (
          v.support.doesNotIncludeMarginInBodyOffset &&
            ((t += parseFloat(v.css(e, "marginTop")) || 0),
            (n += parseFloat(v.css(e, "marginLeft")) || 0)),
          {
            top: t,
            left: n,
          }
        );
      },
      setOffset: function setOffset(e, t, n) {
        var r = v.css(e, "position");
        r === "static" && (e.style.position = "relative");
        var i = v(e),
          s = i.offset(),
          o = v.css(e, "top"),
          u = v.css(e, "left"),
          a =
            (r === "absolute" || r === "fixed") &&
            v.inArray("auto", [o, u]) > -1,
          f = {},
          l = {},
          c,
          h;
        a
          ? ((l = i.position()), (c = l.top), (h = l.left))
          : ((c = parseFloat(o) || 0), (h = parseFloat(u) || 0)),
          v.isFunction(t) && (t = t.call(e, n, s)),
          t.top != null && (f.top = t.top - s.top + c),
          t.left != null && (f.left = t.left - s.left + h),
          "using" in t ? t.using.call(e, f) : i.css(f);
      },
    }),
    v.fn.extend({
      position: function position() {
        if (!this[0]) return;
        var e = this[0],
          t = this.offsetParent(),
          n = this.offset(),
          r = er.test(t[0].nodeName)
            ? {
                top: 0,
                left: 0,
              }
            : t.offset();
        return (
          (n.top -= parseFloat(v.css(e, "marginTop")) || 0),
          (n.left -= parseFloat(v.css(e, "marginLeft")) || 0),
          (r.top += parseFloat(v.css(t[0], "borderTopWidth")) || 0),
          (r.left += parseFloat(v.css(t[0], "borderLeftWidth")) || 0),
          {
            top: n.top - r.top,
            left: n.left - r.left,
          }
        );
      },
      offsetParent: function offsetParent() {
        return this.map(function () {
          var e = this.offsetParent || i.body;

          while (
            e &&
            !er.test(e.nodeName) &&
            v.css(e, "position") === "static"
          ) {
            e = e.offsetParent;
          }

          return e || i.body;
        });
      },
    }),
    v.each(
      {
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset",
      },
      function (e, n) {
        var r = /Y/.test(n);

        v.fn[e] = function (i) {
          return v.access(
            this,
            function (e, i, s) {
              var o = tr(e);
              if (s === t)
                return o
                  ? n in o
                    ? o[n]
                    : o.document.documentElement[i]
                  : e[i];
              o
                ? o.scrollTo(
                    r ? v(o).scrollLeft() : s,
                    r ? s : v(o).scrollTop()
                  )
                : (e[i] = s);
            },
            e,
            i,
            arguments.length,
            null
          );
        };
      }
    ),
    v.each(
      {
        Height: "height",
        Width: "width",
      },
      function (e, n) {
        v.each(
          {
            padding: "inner" + e,
            content: n,
            "": "outer" + e,
          },
          function (r, i) {
            v.fn[i] = function (i, s) {
              var o = arguments.length && (r || typeof i != "boolean"),
                u = r || (i === !0 || s === !0 ? "margin" : "border");
              return v.access(
                this,
                function (n, r, i) {
                  var s;
                  return v.isWindow(n)
                    ? n.document.documentElement["client" + e]
                    : n.nodeType === 9
                    ? ((s = n.documentElement),
                      Math.max(
                        n.body["scroll" + e],
                        s["scroll" + e],
                        n.body["offset" + e],
                        s["offset" + e],
                        s["client" + e]
                      ))
                    : i === t
                    ? v.css(n, r, i, u)
                    : v.style(n, r, i, u);
                },
                n,
                o ? i : t,
                o,
                null
              );
            };
          }
        );
      }
    ),
    (e.jQuery = e.$ = v),
    typeof define == "function" &&
      define.amd &&
      define.amd.jQuery &&
      define("jquery", [], function () {
        return v;
      });
})(window);
/**
 * Swiper 6.1.1
 * Most modern mobile touch slider and framework with hardware accelerated transitions
 * http://swiperjs.com
 *
 * Copyright 2014-2020 Vladimir Kharlampidi
 *
 * Released under the MIT License
 *
 * Released on: July 31, 2020
 */

!(function (e, t) {
  "object" ==
    (typeof exports === "undefined" ? "undefined" : _typeof(exports)) &&
  "undefined" != typeof module
    ? (module.exports = t())
    : "function" == typeof define && define.amd
    ? define(t)
    : ((e = e || self).Swiper = t());
})(void 0, function () {
  "use strict";

  function e(e, t) {
    for (var i = 0; i < t.length; i++) {
      var s = t[i];
      (s.enumerable = s.enumerable || !1),
        (s.configurable = !0),
        "value" in s && (s.writable = !0),
        Object.defineProperty(e, s.key, s);
    }
  }

  function t() {
    return (t =
      Object.assign ||
      function (e) {
        for (var t = 1; t < arguments.length; t++) {
          var i = arguments[t];

          for (var s in i) {
            Object.prototype.hasOwnProperty.call(i, s) && (e[s] = i[s]);
          }
        }

        return e;
      }).apply(this, arguments);
  }

  function i(e) {
    return (
      null !== e &&
      "object" == _typeof(e) &&
      "constructor" in e &&
      e.constructor === Object
    );
  }

  function s(e, t) {
    void 0 === e && (e = {}),
      void 0 === t && (t = {}),
      Object.keys(t).forEach(function (a) {
        void 0 === e[a]
          ? (e[a] = t[a])
          : i(t[a]) && i(e[a]) && Object.keys(t[a]).length > 0 && s(e[a], t[a]);
      });
  }

  var a = {
    body: {},
    addEventListener: function addEventListener() {},
    removeEventListener: function removeEventListener() {},
    activeElement: {
      blur: function blur() {},
      nodeName: "",
    },
    querySelector: function querySelector() {
      return null;
    },
    querySelectorAll: function querySelectorAll() {
      return [];
    },
    getElementById: function getElementById() {
      return null;
    },
    createEvent: function createEvent() {
      return {
        initEvent: function initEvent() {},
      };
    },
    createElement: function createElement() {
      return {
        children: [],
        childNodes: [],
        style: {},
        setAttribute: function setAttribute() {},
        getElementsByTagName: function getElementsByTagName() {
          return [];
        },
      };
    },
    createElementNS: function createElementNS() {
      return {};
    },
    importNode: function importNode() {
      return null;
    },
    location: {
      hash: "",
      host: "",
      hostname: "",
      href: "",
      origin: "",
      pathname: "",
      protocol: "",
      search: "",
    },
  };

  function r() {
    var e = "undefined" != typeof document ? document : {};
    return s(e, a), e;
  }

  var n = {
    document: a,
    navigator: {
      userAgent: "",
    },
    location: {
      hash: "",
      host: "",
      hostname: "",
      href: "",
      origin: "",
      pathname: "",
      protocol: "",
      search: "",
    },
    history: {
      replaceState: function replaceState() {},
      pushState: function pushState() {},
      go: function go() {},
      back: function back() {},
    },
    CustomEvent: function CustomEvent() {
      return this;
    },
    addEventListener: function addEventListener() {},
    removeEventListener: function removeEventListener() {},
    getComputedStyle: function getComputedStyle() {
      return {
        getPropertyValue: function getPropertyValue() {
          return "";
        },
      };
    },
    Image: function Image() {},
    Date: function Date() {},
    screen: {},
    setTimeout: function setTimeout() {},
    clearTimeout: function clearTimeout() {},
    matchMedia: function matchMedia() {
      return {};
    },
    requestAnimationFrame: function requestAnimationFrame(e) {
      return "undefined" == typeof setTimeout ? (e(), null) : setTimeout(e, 0);
    },
    cancelAnimationFrame: function cancelAnimationFrame(e) {
      "undefined" != typeof setTimeout && clearTimeout(e);
    },
  };

  function l() {
    var e = "undefined" != typeof window ? window : {};
    return s(e, n), e;
  }

  function o(e) {
    return (o = Object.setPrototypeOf
      ? Object.getPrototypeOf
      : function (e) {
          return e.__proto__ || Object.getPrototypeOf(e);
        })(e);
  }

  function d(e, t) {
    return (d =
      Object.setPrototypeOf ||
      function (e, t) {
        return (e.__proto__ = t), e;
      })(e, t);
  }

  function h() {
    if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
    if (Reflect.construct.sham) return !1;
    if ("function" == typeof Proxy) return !0;

    try {
      return (
        Date.prototype.toString.call(
          Reflect.construct(Date, [], function () {})
        ),
        !0
      );
    } catch (e) {
      return !1;
    }
  }

  function p(e, t, i) {
    return (p = h()
      ? Reflect.construct
      : function (e, t, i) {
          var s = [null];
          s.push.apply(s, t);
          var a = new (Function.bind.apply(e, s))();
          return i && d(a, i.prototype), a;
        }).apply(null, arguments);
  }

  function u(e) {
    var t = "function" == typeof Map ? new Map() : void 0;
    return (u = function u(e) {
      if (
        null === e ||
        ((i = e), -1 === Function.toString.call(i).indexOf("[native code]"))
      )
        return e;
      var i;
      if ("function" != typeof e)
        throw new TypeError(
          "Super expression must either be null or a function"
        );

      if (void 0 !== t) {
        if (t.has(e)) return t.get(e);
        t.set(e, s);
      }

      function s() {
        return p(e, arguments, o(this).constructor);
      }

      return (
        (s.prototype = Object.create(e.prototype, {
          constructor: {
            value: s,
            enumerable: !1,
            writable: !0,
            configurable: !0,
          },
        })),
        d(s, e)
      );
    })(e);
  }

  var c = (function (e) {
    var t, i;

    function s(t) {
      var i, s, a;
      return (
        (i = e.call.apply(e, [this].concat(t)) || this),
        (s = (function (e) {
          if (void 0 === e)
            throw new ReferenceError(
              "this hasn't been initialised - super() hasn't been called"
            );
          return e;
        })(i)),
        (a = s.__proto__),
        Object.defineProperty(s, "__proto__", {
          get: function get() {
            return a;
          },
          set: function set(e) {
            a.__proto__ = e;
          },
        }),
        i
      );
    }

    return (
      (i = e),
      ((t = s).prototype = Object.create(i.prototype)),
      (t.prototype.constructor = t),
      (t.__proto__ = i),
      s
    );
  })(u(Array));

  function v(e) {
    void 0 === e && (e = []);
    var t = [];
    return (
      e.forEach(function (e) {
        Array.isArray(e) ? t.push.apply(t, v(e)) : t.push(e);
      }),
      t
    );
  }

  function f(e, t) {
    return Array.prototype.filter.call(e, t);
  }

  function m(e, t) {
    var i = l(),
      s = r(),
      a = [];
    if (!t && e instanceof c) return e;
    if (!e) return new c(a);

    if ("string" == typeof e) {
      var n = e.trim();

      if (n.indexOf("<") >= 0 && n.indexOf(">") >= 0) {
        var o = "div";
        0 === n.indexOf("<li") && (o = "ul"),
          0 === n.indexOf("<tr") && (o = "tbody"),
          (0 !== n.indexOf("<td") && 0 !== n.indexOf("<th")) || (o = "tr"),
          0 === n.indexOf("<tbody") && (o = "table"),
          0 === n.indexOf("<option") && (o = "select");
        var d = s.createElement(o);
        d.innerHTML = n;

        for (var h = 0; h < d.childNodes.length; h += 1) {
          a.push(d.childNodes[h]);
        }
      } else
        a = (function (e, t) {
          if ("string" != typeof e) return [e];

          for (
            var i = [], s = t.querySelectorAll(e), a = 0;
            a < s.length;
            a += 1
          ) {
            i.push(s[a]);
          }

          return i;
        })(e.trim(), t || s);
    } else if (e.nodeType || e === i || e === s) a.push(e);
    else if (Array.isArray(e)) {
      if (e instanceof c) return e;
      a = e;
    }

    return new c(
      (function (e) {
        for (var t = [], i = 0; i < e.length; i += 1) {
          -1 === t.indexOf(e[i]) && t.push(e[i]);
        }

        return t;
      })(a)
    );
  }

  m.fn = c.prototype;
  var g,
    w,
    b,
    y = {
      addClass: function addClass() {
        for (var e = arguments.length, t = new Array(e), i = 0; i < e; i++) {
          t[i] = arguments[i];
        }

        var s = v(
          t.map(function (e) {
            return e.split(" ");
          })
        );
        return (
          this.forEach(function (e) {
            var t;
            (t = e.classList).add.apply(t, s);
          }),
          this
        );
      },
      removeClass: function removeClass() {
        for (var e = arguments.length, t = new Array(e), i = 0; i < e; i++) {
          t[i] = arguments[i];
        }

        var s = v(
          t.map(function (e) {
            return e.split(" ");
          })
        );
        return (
          this.forEach(function (e) {
            var t;
            (t = e.classList).remove.apply(t, s);
          }),
          this
        );
      },
      hasClass: function hasClass() {
        for (var e = arguments.length, t = new Array(e), i = 0; i < e; i++) {
          t[i] = arguments[i];
        }

        var s = v(
          t.map(function (e) {
            return e.split(" ");
          })
        );
        return (
          f(this, function (e) {
            return (
              s.filter(function (t) {
                return e.classList.contains(t);
              }).length > 0
            );
          }).length > 0
        );
      },
      toggleClass: function toggleClass() {
        for (var e = arguments.length, t = new Array(e), i = 0; i < e; i++) {
          t[i] = arguments[i];
        }

        var s = v(
          t.map(function (e) {
            return e.split(" ");
          })
        );
        this.forEach(function (e) {
          s.forEach(function (t) {
            e.classList.toggle(t);
          });
        });
      },
      attr: function attr(e, t) {
        if (1 === arguments.length && "string" == typeof e)
          return this[0] ? this[0].getAttribute(e) : void 0;

        for (var i = 0; i < this.length; i += 1) {
          if (2 === arguments.length) this[i].setAttribute(e, t);
          else
            for (var s in e) {
              (this[i][s] = e[s]), this[i].setAttribute(s, e[s]);
            }
        }

        return this;
      },
      removeAttr: function removeAttr(e) {
        for (var t = 0; t < this.length; t += 1) {
          this[t].removeAttribute(e);
        }

        return this;
      },
      transform: function transform(e) {
        for (var t = 0; t < this.length; t += 1) {
          this[t].style.transform = e;
        }

        return this;
      },
      transition: function transition(e) {
        for (var t = 0; t < this.length; t += 1) {
          this[t].style.transition = "string" != typeof e ? e + "ms" : e;
        }

        return this;
      },
      on: function on() {
        for (var e = arguments.length, t = new Array(e), i = 0; i < e; i++) {
          t[i] = arguments[i];
        }

        var s = t[0],
          a = t[1],
          r = t[2],
          n = t[3];

        function l(e) {
          var t = e.target;

          if (t) {
            var i = e.target.dom7EventData || [];
            if ((i.indexOf(e) < 0 && i.unshift(e), m(t).is(a))) r.apply(t, i);
            else
              for (var s = m(t).parents(), n = 0; n < s.length; n += 1) {
                m(s[n]).is(a) && r.apply(s[n], i);
              }
          }
        }

        function o(e) {
          var t = (e && e.target && e.target.dom7EventData) || [];
          t.indexOf(e) < 0 && t.unshift(e), r.apply(this, t);
        }

        "function" == typeof t[1] &&
          ((s = t[0]), (r = t[1]), (n = t[2]), (a = void 0)),
          n || (n = !1);

        for (var d, h = s.split(" "), p = 0; p < this.length; p += 1) {
          var u = this[p];
          if (a)
            for (d = 0; d < h.length; d += 1) {
              var c = h[d];
              u.dom7LiveListeners || (u.dom7LiveListeners = {}),
                u.dom7LiveListeners[c] || (u.dom7LiveListeners[c] = []),
                u.dom7LiveListeners[c].push({
                  listener: r,
                  proxyListener: l,
                }),
                u.addEventListener(c, l, n);
            }
          else
            for (d = 0; d < h.length; d += 1) {
              var v = h[d];
              u.dom7Listeners || (u.dom7Listeners = {}),
                u.dom7Listeners[v] || (u.dom7Listeners[v] = []),
                u.dom7Listeners[v].push({
                  listener: r,
                  proxyListener: o,
                }),
                u.addEventListener(v, o, n);
            }
        }

        return this;
      },
      off: function off() {
        for (var e = arguments.length, t = new Array(e), i = 0; i < e; i++) {
          t[i] = arguments[i];
        }

        var s = t[0],
          a = t[1],
          r = t[2],
          n = t[3];
        "function" == typeof t[1] &&
          ((s = t[0]), (r = t[1]), (n = t[2]), (a = void 0)),
          n || (n = !1);

        for (var l = s.split(" "), o = 0; o < l.length; o += 1) {
          for (var d = l[o], h = 0; h < this.length; h += 1) {
            var p = this[h],
              u = void 0;
            if (
              (!a && p.dom7Listeners
                ? (u = p.dom7Listeners[d])
                : a && p.dom7LiveListeners && (u = p.dom7LiveListeners[d]),
              u && u.length)
            )
              for (var c = u.length - 1; c >= 0; c -= 1) {
                var v = u[c];
                (r && v.listener === r) ||
                (r &&
                  v.listener &&
                  v.listener.dom7proxy &&
                  v.listener.dom7proxy === r)
                  ? (p.removeEventListener(d, v.proxyListener, n),
                    u.splice(c, 1))
                  : r ||
                    (p.removeEventListener(d, v.proxyListener, n),
                    u.splice(c, 1));
              }
          }
        }

        return this;
      },
      trigger: function trigger() {
        for (
          var e = l(), t = arguments.length, i = new Array(t), s = 0;
          s < t;
          s++
        ) {
          i[s] = arguments[s];
        }

        for (var a = i[0].split(" "), r = i[1], n = 0; n < a.length; n += 1) {
          for (var o = a[n], d = 0; d < this.length; d += 1) {
            var h = this[d];

            if (e.CustomEvent) {
              var p = new e.CustomEvent(o, {
                detail: r,
                bubbles: !0,
                cancelable: !0,
              });
              (h.dom7EventData = i.filter(function (e, t) {
                return t > 0;
              })),
                h.dispatchEvent(p),
                (h.dom7EventData = []),
                delete h.dom7EventData;
            }
          }
        }

        return this;
      },
      transitionEnd: function transitionEnd(e) {
        var t = this;
        return (
          e &&
            t.on("transitionend", function i(s) {
              s.target === this && (e.call(this, s), t.off("transitionend", i));
            }),
          this
        );
      },
      outerWidth: function outerWidth(e) {
        if (this.length > 0) {
          if (e) {
            var t = this.styles();
            return (
              this[0].offsetWidth +
              parseFloat(t.getPropertyValue("margin-right")) +
              parseFloat(t.getPropertyValue("margin-left"))
            );
          }

          return this[0].offsetWidth;
        }

        return null;
      },
      outerHeight: function outerHeight(e) {
        if (this.length > 0) {
          if (e) {
            var t = this.styles();
            return (
              this[0].offsetHeight +
              parseFloat(t.getPropertyValue("margin-top")) +
              parseFloat(t.getPropertyValue("margin-bottom"))
            );
          }

          return this[0].offsetHeight;
        }

        return null;
      },
      styles: function styles() {
        var e = l();
        return this[0] ? e.getComputedStyle(this[0], null) : {};
      },
      offset: function offset() {
        if (this.length > 0) {
          var e = l(),
            t = r(),
            i = this[0],
            s = i.getBoundingClientRect(),
            a = t.body,
            n = i.clientTop || a.clientTop || 0,
            o = i.clientLeft || a.clientLeft || 0,
            d = i === e ? e.scrollY : i.scrollTop,
            h = i === e ? e.scrollX : i.scrollLeft;
          return {
            top: s.top + d - n,
            left: s.left + h - o,
          };
        }

        return null;
      },
      css: function css(e, t) {
        var i,
          s = l();

        if (1 === arguments.length) {
          if ("string" != typeof e) {
            for (i = 0; i < this.length; i += 1) {
              for (var a in e) {
                this[i].style[a] = e[a];
              }
            }

            return this;
          }

          if (this[0])
            return s.getComputedStyle(this[0], null).getPropertyValue(e);
        }

        if (2 === arguments.length && "string" == typeof e) {
          for (i = 0; i < this.length; i += 1) {
            this[i].style[e] = t;
          }

          return this;
        }

        return this;
      },
      each: function each(e) {
        return e
          ? (this.forEach(function (t, i) {
              e.apply(t, [t, i]);
            }),
            this)
          : this;
      },
      html: function html(e) {
        if (void 0 === e) return this[0] ? this[0].innerHTML : null;

        for (var t = 0; t < this.length; t += 1) {
          this[t].innerHTML = e;
        }

        return this;
      },
      text: function text(e) {
        if (void 0 === e) return this[0] ? this[0].textContent.trim() : null;

        for (var t = 0; t < this.length; t += 1) {
          this[t].textContent = e;
        }

        return this;
      },
      is: function is(e) {
        var t,
          i,
          s = l(),
          a = r(),
          n = this[0];
        if (!n || void 0 === e) return !1;

        if ("string" == typeof e) {
          if (n.matches) return n.matches(e);
          if (n.webkitMatchesSelector) return n.webkitMatchesSelector(e);
          if (n.msMatchesSelector) return n.msMatchesSelector(e);

          for (t = m(e), i = 0; i < t.length; i += 1) {
            if (t[i] === n) return !0;
          }

          return !1;
        }

        if (e === a) return n === a;
        if (e === s) return n === s;

        if (e.nodeType || e instanceof c) {
          for (t = e.nodeType ? [e] : e, i = 0; i < t.length; i += 1) {
            if (t[i] === n) return !0;
          }

          return !1;
        }

        return !1;
      },
      index: function index() {
        var e,
          t = this[0];

        if (t) {
          for (e = 0; null !== (t = t.previousSibling); ) {
            1 === t.nodeType && (e += 1);
          }

          return e;
        }
      },
      eq: function eq(e) {
        if (void 0 === e) return this;
        var t = this.length;
        if (e > t - 1) return m([]);

        if (e < 0) {
          var i = t + e;
          return m(i < 0 ? [] : [this[i]]);
        }

        return m([this[e]]);
      },
      append: function append() {
        for (var e, t = r(), i = 0; i < arguments.length; i += 1) {
          e = i < 0 || arguments.length <= i ? void 0 : arguments[i];

          for (var s = 0; s < this.length; s += 1) {
            if ("string" == typeof e) {
              var a = t.createElement("div");

              for (a.innerHTML = e; a.firstChild; ) {
                this[s].appendChild(a.firstChild);
              }
            } else if (e instanceof c)
              for (var n = 0; n < e.length; n += 1) {
                this[s].appendChild(e[n]);
              }
            else this[s].appendChild(e);
          }
        }

        return this;
      },
      prepend: function prepend(e) {
        var t,
          i,
          s = r();

        for (t = 0; t < this.length; t += 1) {
          if ("string" == typeof e) {
            var a = s.createElement("div");

            for (a.innerHTML = e, i = a.childNodes.length - 1; i >= 0; i -= 1) {
              this[t].insertBefore(a.childNodes[i], this[t].childNodes[0]);
            }
          } else if (e instanceof c)
            for (i = 0; i < e.length; i += 1) {
              this[t].insertBefore(e[i], this[t].childNodes[0]);
            }
          else this[t].insertBefore(e, this[t].childNodes[0]);
        }

        return this;
      },
      next: function next(e) {
        return this.length > 0
          ? e
            ? this[0].nextElementSibling && m(this[0].nextElementSibling).is(e)
              ? m([this[0].nextElementSibling])
              : m([])
            : this[0].nextElementSibling
            ? m([this[0].nextElementSibling])
            : m([])
          : m([]);
      },
      nextAll: function nextAll(e) {
        var t = [],
          i = this[0];
        if (!i) return m([]);

        for (; i.nextElementSibling; ) {
          var s = i.nextElementSibling;
          e ? m(s).is(e) && t.push(s) : t.push(s), (i = s);
        }

        return m(t);
      },
      prev: function prev(e) {
        if (this.length > 0) {
          var t = this[0];
          return e
            ? t.previousElementSibling && m(t.previousElementSibling).is(e)
              ? m([t.previousElementSibling])
              : m([])
            : t.previousElementSibling
            ? m([t.previousElementSibling])
            : m([]);
        }

        return m([]);
      },
      prevAll: function prevAll(e) {
        var t = [],
          i = this[0];
        if (!i) return m([]);

        for (; i.previousElementSibling; ) {
          var s = i.previousElementSibling;
          e ? m(s).is(e) && t.push(s) : t.push(s), (i = s);
        }

        return m(t);
      },
      parent: function parent(e) {
        for (var t = [], i = 0; i < this.length; i += 1) {
          null !== this[i].parentNode &&
            (e
              ? m(this[i].parentNode).is(e) && t.push(this[i].parentNode)
              : t.push(this[i].parentNode));
        }

        return m(t);
      },
      parents: function parents(e) {
        for (var t = [], i = 0; i < this.length; i += 1) {
          for (var s = this[i].parentNode; s; ) {
            e ? m(s).is(e) && t.push(s) : t.push(s), (s = s.parentNode);
          }
        }

        return m(t);
      },
      closest: function closest(e) {
        var t = this;
        return void 0 === e ? m([]) : (t.is(e) || (t = t.parents(e).eq(0)), t);
      },
      find: function find(e) {
        for (var t = [], i = 0; i < this.length; i += 1) {
          for (
            var s = this[i].querySelectorAll(e), a = 0;
            a < s.length;
            a += 1
          ) {
            t.push(s[a]);
          }
        }

        return m(t);
      },
      children: function children(e) {
        for (var t = [], i = 0; i < this.length; i += 1) {
          for (var s = this[i].children, a = 0; a < s.length; a += 1) {
            (e && !m(s[a]).is(e)) || t.push(s[a]);
          }
        }

        return m(t);
      },
      filter: function filter(e) {
        return m(f(this, e));
      },
      remove: function remove() {
        for (var e = 0; e < this.length; e += 1) {
          this[e].parentNode && this[e].parentNode.removeChild(this[e]);
        }

        return this;
      },
    };

  function E(e, t) {
    return void 0 === t && (t = 0), setTimeout(e, t);
  }

  function x() {
    return Date.now();
  }

  function T(e, t) {
    void 0 === t && (t = "x");
    var i,
      s,
      a,
      r = l(),
      n = r.getComputedStyle(e, null);
    return (
      r.WebKitCSSMatrix
        ? ((s = n.transform || n.webkitTransform).split(",").length > 6 &&
            (s = s
              .split(", ")
              .map(function (e) {
                return e.replace(",", ".");
              })
              .join(", ")),
          (a = new r.WebKitCSSMatrix("none" === s ? "" : s)))
        : (i = (a =
            n.MozTransform ||
            n.OTransform ||
            n.MsTransform ||
            n.msTransform ||
            n.transform ||
            n
              .getPropertyValue("transform")
              .replace("translate(", "matrix(1, 0, 0, 1,"))
            .toString()
            .split(",")),
      "x" === t &&
        (s = r.WebKitCSSMatrix
          ? a.m41
          : 16 === i.length
          ? parseFloat(i[12])
          : parseFloat(i[4])),
      "y" === t &&
        (s = r.WebKitCSSMatrix
          ? a.m42
          : 16 === i.length
          ? parseFloat(i[13])
          : parseFloat(i[5])),
      s || 0
    );
  }

  function C(e) {
    return (
      "object" == _typeof(e) &&
      null !== e &&
      e.constructor &&
      e.constructor === Object
    );
  }

  function S() {
    for (
      var e = Object(arguments.length <= 0 ? void 0 : arguments[0]), t = 1;
      t < arguments.length;
      t += 1
    ) {
      var i = t < 0 || arguments.length <= t ? void 0 : arguments[t];
      if (null != i)
        for (
          var s = Object.keys(Object(i)), a = 0, r = s.length;
          a < r;
          a += 1
        ) {
          var n = s[a],
            l = Object.getOwnPropertyDescriptor(i, n);
          void 0 !== l &&
            l.enumerable &&
            (C(e[n]) && C(i[n])
              ? S(e[n], i[n])
              : !C(e[n]) && C(i[n])
              ? ((e[n] = {}), S(e[n], i[n]))
              : (e[n] = i[n]));
        }
    }

    return e;
  }

  function M(e, t) {
    Object.keys(t).forEach(function (i) {
      C(t[i]) &&
        Object.keys(t[i]).forEach(function (s) {
          "function" == typeof t[i][s] && (t[i][s] = t[i][s].bind(e));
        }),
        (e[i] = t[i]);
    });
  }

  function z() {
    return (
      g ||
        (g = (function () {
          var e = l(),
            t = r();
          return {
            touch: !!(
              "ontouchstart" in e ||
              (e.DocumentTouch && t instanceof e.DocumentTouch)
            ),
            pointerEvents:
              !!e.PointerEvent &&
              "maxTouchPoints" in e.navigator &&
              e.navigator.maxTouchPoints >= 0,
            observer: "MutationObserver" in e || "WebkitMutationObserver" in e,
            passiveListener: (function () {
              var t = !1;

              try {
                var i = Object.defineProperty({}, "passive", {
                  get: function get() {
                    t = !0;
                  },
                });
                e.addEventListener("testPassiveListener", null, i);
              } catch (e) {}

              return t;
            })(),
            gestures: "ongesturestart" in e,
          };
        })()),
      g
    );
  }

  function P(e) {
    return (
      void 0 === e && (e = {}),
      w ||
        (w = (function (e) {
          var t = (void 0 === e ? {} : e).userAgent,
            i = z(),
            s = l(),
            a = s.navigator.platform,
            r = t || s.navigator.userAgent,
            n = {
              ios: !1,
              android: !1,
            },
            o = s.screen.width,
            d = s.screen.height,
            h = r.match(/(Android);?[\s\/]+([\d.]+)?/),
            p = r.match(/(iPad).*OS\s([\d_]+)/),
            u = r.match(/(iPod)(.*OS\s([\d_]+))?/),
            c = !p && r.match(/(iPhone\sOS|iOS)\s([\d_]+)/),
            v = "Win32" === a,
            f = "MacIntel" === a;
          return (
            !p &&
              f &&
              i.touch &&
              [
                "1024x1366",
                "1366x1024",
                "834x1194",
                "1194x834",
                "834x1112",
                "1112x834",
                "768x1024",
                "1024x768",
              ].indexOf(o + "x" + d) >= 0 &&
              ((p = r.match(/(Version)\/([\d.]+)/)) || (p = [0, 1, "13_0_0"]),
              (f = !1)),
            h && !v && ((n.os = "android"), (n.android = !0)),
            (p || c || u) && ((n.os = "ios"), (n.ios = !0)),
            n
          );
        })(e)),
      w
    );
  }

  function k() {
    return (
      b ||
        (b = (function () {
          var e,
            t = l();
          return {
            isEdge: !!t.navigator.userAgent.match(/Edge/g),
            isSafari:
              ((e = t.navigator.userAgent.toLowerCase()),
              e.indexOf("safari") >= 0 &&
                e.indexOf("chrome") < 0 &&
                e.indexOf("android") < 0),
            isWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(
              t.navigator.userAgent
            ),
          };
        })()),
      b
    );
  }

  Object.keys(y).forEach(function (e) {
    m.fn[e] = y[e];
  });
  var $ = {
      name: "resize",
      create: function create() {
        var e = this;
        S(e, {
          resize: {
            resizeHandler: function resizeHandler() {
              e &&
                !e.destroyed &&
                e.initialized &&
                (e.emit("beforeResize"), e.emit("resize"));
            },
            orientationChangeHandler: function orientationChangeHandler() {
              e && !e.destroyed && e.initialized && e.emit("orientationchange");
            },
          },
        });
      },
      on: {
        init: function init(e) {
          var t = l();
          t.addEventListener("resize", e.resize.resizeHandler),
            t.addEventListener(
              "orientationchange",
              e.resize.orientationChangeHandler
            );
        },
        destroy: function destroy(e) {
          var t = l();
          t.removeEventListener("resize", e.resize.resizeHandler),
            t.removeEventListener(
              "orientationchange",
              e.resize.orientationChangeHandler
            );
        },
      },
    },
    L = {
      attach: function attach(e, t) {
        void 0 === t && (t = {});
        var i = l(),
          s = this,
          a = new (i.MutationObserver || i.WebkitMutationObserver)(function (
            e
          ) {
            if (1 !== e.length) {
              var t = function t() {
                s.emit("observerUpdate", e[0]);
              };

              i.requestAnimationFrame
                ? i.requestAnimationFrame(t)
                : i.setTimeout(t, 0);
            } else s.emit("observerUpdate", e[0]);
          });
        a.observe(e, {
          attributes: void 0 === t.attributes || t.attributes,
          childList: void 0 === t.childList || t.childList,
          characterData: void 0 === t.characterData || t.characterData,
        }),
          s.observer.observers.push(a);
      },
      init: function init() {
        if (this.support.observer && this.params.observer) {
          if (this.params.observeParents)
            for (var e = this.$el.parents(), t = 0; t < e.length; t += 1) {
              this.observer.attach(e[t]);
            }
          this.observer.attach(this.$el[0], {
            childList: this.params.observeSlideChildren,
          }),
            this.observer.attach(this.$wrapperEl[0], {
              attributes: !1,
            });
        }
      },
      destroy: function destroy() {
        this.observer.observers.forEach(function (e) {
          e.disconnect();
        }),
          (this.observer.observers = []);
      },
    },
    I = {
      name: "observer",
      params: {
        observer: !1,
        observeParents: !1,
        observeSlideChildren: !1,
      },
      create: function create() {
        M(this, {
          observer: t(
            t({}, L),
            {},
            {
              observers: [],
            }
          ),
        });
      },
      on: {
        init: function init(e) {
          e.observer.init();
        },
        destroy: function destroy(e) {
          e.observer.destroy();
        },
      },
    };

  function O(e) {
    var t = r(),
      i = l(),
      s = this.touchEventsData,
      a = this.params,
      n = this.touches;

    if (!this.animating || !a.preventInteractionOnTransition) {
      var o = e;
      o.originalEvent && (o = o.originalEvent);
      var d = m(o.target);
      if (
        ("wrapper" !== a.touchEventsTarget ||
          d.closest(this.wrapperEl).length) &&
        ((s.isTouchEvent = "touchstart" === o.type),
        (s.isTouchEvent || !("which" in o) || 3 !== o.which) &&
          !(
            (!s.isTouchEvent && "button" in o && o.button > 0) ||
            (s.isTouched && s.isMoved)
          ))
      )
        if (
          a.noSwiping &&
          d.closest(
            a.noSwipingSelector ? a.noSwipingSelector : "." + a.noSwipingClass
          )[0]
        )
          this.allowClick = !0;
        else if (!a.swipeHandler || d.closest(a.swipeHandler)[0]) {
          (n.currentX =
            "touchstart" === o.type ? o.targetTouches[0].pageX : o.pageX),
            (n.currentY =
              "touchstart" === o.type ? o.targetTouches[0].pageY : o.pageY);
          var h = n.currentX,
            p = n.currentY,
            u = a.edgeSwipeDetection || a.iOSEdgeSwipeDetection,
            c = a.edgeSwipeThreshold || a.iOSEdgeSwipeThreshold;

          if (!u || !(h <= c || h >= i.screen.width - c)) {
            if (
              (S(s, {
                isTouched: !0,
                isMoved: !1,
                allowTouchCallbacks: !0,
                isScrolling: void 0,
                startMoving: void 0,
              }),
              (n.startX = h),
              (n.startY = p),
              (s.touchStartTime = x()),
              (this.allowClick = !0),
              this.updateSize(),
              (this.swipeDirection = void 0),
              a.threshold > 0 && (s.allowThresholdMove = !1),
              "touchstart" !== o.type)
            ) {
              var v = !0;
              d.is(s.formElements) && (v = !1),
                t.activeElement &&
                  m(t.activeElement).is(s.formElements) &&
                  t.activeElement !== d[0] &&
                  t.activeElement.blur();
              var f = v && this.allowTouchMove && a.touchStartPreventDefault;
              (a.touchStartForcePreventDefault || f) && o.preventDefault();
            }

            this.emit("touchStart", o);
          }
        }
    }
  }

  function A(e) {
    var t = r(),
      i = this.touchEventsData,
      s = this.params,
      a = this.touches,
      n = this.rtlTranslate,
      l = e;

    if ((l.originalEvent && (l = l.originalEvent), i.isTouched)) {
      if (!i.isTouchEvent || "touchmove" === l.type) {
        var o =
            "touchmove" === l.type &&
            l.targetTouches &&
            (l.targetTouches[0] || l.changedTouches[0]),
          d = "touchmove" === l.type ? o.pageX : l.pageX,
          h = "touchmove" === l.type ? o.pageY : l.pageY;
        if (l.preventedByNestedSwiper)
          return (a.startX = d), void (a.startY = h);
        if (!this.allowTouchMove)
          return (
            (this.allowClick = !1),
            void (
              i.isTouched &&
              (S(a, {
                startX: d,
                startY: h,
                currentX: d,
                currentY: h,
              }),
              (i.touchStartTime = x()))
            )
          );
        if (i.isTouchEvent && s.touchReleaseOnEdges && !s.loop)
          if (this.isVertical()) {
            if (
              (h < a.startY && this.translate <= this.maxTranslate()) ||
              (h > a.startY && this.translate >= this.minTranslate())
            )
              return (i.isTouched = !1), void (i.isMoved = !1);
          } else if (
            (d < a.startX && this.translate <= this.maxTranslate()) ||
            (d > a.startX && this.translate >= this.minTranslate())
          )
            return;
        if (
          i.isTouchEvent &&
          t.activeElement &&
          l.target === t.activeElement &&
          m(l.target).is(i.formElements)
        )
          return (i.isMoved = !0), void (this.allowClick = !1);

        if (
          (i.allowTouchCallbacks && this.emit("touchMove", l),
          !(l.targetTouches && l.targetTouches.length > 1))
        ) {
          (a.currentX = d), (a.currentY = h);
          var p = a.currentX - a.startX,
            u = a.currentY - a.startY;

          if (
            !(
              this.params.threshold &&
              Math.sqrt(Math.pow(p, 2) + Math.pow(u, 2)) < this.params.threshold
            )
          ) {
            var c;
            if (void 0 === i.isScrolling)
              (this.isHorizontal() && a.currentY === a.startY) ||
              (this.isVertical() && a.currentX === a.startX)
                ? (i.isScrolling = !1)
                : p * p + u * u >= 25 &&
                  ((c = (180 * Math.atan2(Math.abs(u), Math.abs(p))) / Math.PI),
                  (i.isScrolling = this.isHorizontal()
                    ? c > s.touchAngle
                    : 90 - c > s.touchAngle));
            if (
              (i.isScrolling && this.emit("touchMoveOpposite", l),
              void 0 === i.startMoving &&
                ((a.currentX === a.startX && a.currentY === a.startY) ||
                  (i.startMoving = !0)),
              i.isScrolling)
            )
              i.isTouched = !1;
            else if (i.startMoving) {
              (this.allowClick = !1),
                !s.cssMode && l.cancelable && l.preventDefault(),
                s.touchMoveStopPropagation && !s.nested && l.stopPropagation(),
                i.isMoved ||
                  (s.loop && this.loopFix(),
                  (i.startTranslate = this.getTranslate()),
                  this.setTransition(0),
                  this.animating &&
                    this.$wrapperEl.trigger(
                      "webkitTransitionEnd transitionend"
                    ),
                  (i.allowMomentumBounce = !1),
                  !s.grabCursor ||
                    (!0 !== this.allowSlideNext &&
                      !0 !== this.allowSlidePrev) ||
                    this.setGrabCursor(!0),
                  this.emit("sliderFirstMove", l)),
                this.emit("sliderMove", l),
                (i.isMoved = !0);
              var v = this.isHorizontal() ? p : u;
              (a.diff = v),
                (v *= s.touchRatio),
                n && (v = -v),
                (this.swipeDirection = v > 0 ? "prev" : "next"),
                (i.currentTranslate = v + i.startTranslate);
              var f = !0,
                g = s.resistanceRatio;

              if (
                (s.touchReleaseOnEdges && (g = 0),
                v > 0 && i.currentTranslate > this.minTranslate()
                  ? ((f = !1),
                    s.resistance &&
                      (i.currentTranslate =
                        this.minTranslate() -
                        1 +
                        Math.pow(
                          -this.minTranslate() + i.startTranslate + v,
                          g
                        )))
                  : v < 0 &&
                    i.currentTranslate < this.maxTranslate() &&
                    ((f = !1),
                    s.resistance &&
                      (i.currentTranslate =
                        this.maxTranslate() +
                        1 -
                        Math.pow(
                          this.maxTranslate() - i.startTranslate - v,
                          g
                        ))),
                f && (l.preventedByNestedSwiper = !0),
                !this.allowSlideNext &&
                  "next" === this.swipeDirection &&
                  i.currentTranslate < i.startTranslate &&
                  (i.currentTranslate = i.startTranslate),
                !this.allowSlidePrev &&
                  "prev" === this.swipeDirection &&
                  i.currentTranslate > i.startTranslate &&
                  (i.currentTranslate = i.startTranslate),
                s.threshold > 0)
              ) {
                if (!(Math.abs(v) > s.threshold || i.allowThresholdMove))
                  return void (i.currentTranslate = i.startTranslate);
                if (!i.allowThresholdMove)
                  return (
                    (i.allowThresholdMove = !0),
                    (a.startX = a.currentX),
                    (a.startY = a.currentY),
                    (i.currentTranslate = i.startTranslate),
                    void (a.diff = this.isHorizontal()
                      ? a.currentX - a.startX
                      : a.currentY - a.startY)
                  );
              }

              s.followFinger &&
                !s.cssMode &&
                ((s.freeMode ||
                  s.watchSlidesProgress ||
                  s.watchSlidesVisibility) &&
                  (this.updateActiveIndex(), this.updateSlidesClasses()),
                s.freeMode &&
                  (0 === i.velocities.length &&
                    i.velocities.push({
                      position: a[this.isHorizontal() ? "startX" : "startY"],
                      time: i.touchStartTime,
                    }),
                  i.velocities.push({
                    position: a[this.isHorizontal() ? "currentX" : "currentY"],
                    time: x(),
                  })),
                this.updateProgress(i.currentTranslate),
                this.setTranslate(i.currentTranslate));
            }
          }
        }
      }
    } else i.startMoving && i.isScrolling && this.emit("touchMoveOpposite", l);
  }

  function D(e) {
    var t = this,
      i = t.touchEventsData,
      s = t.params,
      a = t.touches,
      r = t.rtlTranslate,
      n = t.$wrapperEl,
      l = t.slidesGrid,
      o = t.snapGrid,
      d = e;
    if (
      (d.originalEvent && (d = d.originalEvent),
      i.allowTouchCallbacks && t.emit("touchEnd", d),
      (i.allowTouchCallbacks = !1),
      !i.isTouched)
    )
      return (
        i.isMoved && s.grabCursor && t.setGrabCursor(!1),
        (i.isMoved = !1),
        void (i.startMoving = !1)
      );
    s.grabCursor &&
      i.isMoved &&
      i.isTouched &&
      (!0 === t.allowSlideNext || !0 === t.allowSlidePrev) &&
      t.setGrabCursor(!1);
    var h,
      p = x(),
      u = p - i.touchStartTime;
    if (
      (t.allowClick &&
        (t.updateClickedSlide(d),
        t.emit("tap click", d),
        u < 300 &&
          p - i.lastClickTime < 300 &&
          t.emit("doubleTap doubleClick", d)),
      (i.lastClickTime = x()),
      E(function () {
        t.destroyed || (t.allowClick = !0);
      }),
      !i.isTouched ||
        !i.isMoved ||
        !t.swipeDirection ||
        0 === a.diff ||
        i.currentTranslate === i.startTranslate)
    )
      return (i.isTouched = !1), (i.isMoved = !1), void (i.startMoving = !1);
    if (
      ((i.isTouched = !1),
      (i.isMoved = !1),
      (i.startMoving = !1),
      (h = s.followFinger
        ? r
          ? t.translate
          : -t.translate
        : -i.currentTranslate),
      !s.cssMode)
    )
      if (s.freeMode) {
        if (h < -t.minTranslate()) return void t.slideTo(t.activeIndex);
        if (h > -t.maxTranslate())
          return void (t.slides.length < o.length
            ? t.slideTo(o.length - 1)
            : t.slideTo(t.slides.length - 1));

        if (s.freeModeMomentum) {
          if (i.velocities.length > 1) {
            var c = i.velocities.pop(),
              v = i.velocities.pop(),
              f = c.position - v.position,
              m = c.time - v.time;
            (t.velocity = f / m),
              (t.velocity /= 2),
              Math.abs(t.velocity) < s.freeModeMinimumVelocity &&
                (t.velocity = 0),
              (m > 150 || x() - c.time > 300) && (t.velocity = 0);
          } else t.velocity = 0;

          (t.velocity *= s.freeModeMomentumVelocityRatio),
            (i.velocities.length = 0);
          var g = 1e3 * s.freeModeMomentumRatio,
            w = t.velocity * g,
            b = t.translate + w;
          r && (b = -b);
          var y,
            T,
            C = !1,
            S = 20 * Math.abs(t.velocity) * s.freeModeMomentumBounceRatio;
          if (b < t.maxTranslate())
            s.freeModeMomentumBounce
              ? (b + t.maxTranslate() < -S && (b = t.maxTranslate() - S),
                (y = t.maxTranslate()),
                (C = !0),
                (i.allowMomentumBounce = !0))
              : (b = t.maxTranslate()),
              s.loop && s.centeredSlides && (T = !0);
          else if (b > t.minTranslate())
            s.freeModeMomentumBounce
              ? (b - t.minTranslate() > S && (b = t.minTranslate() + S),
                (y = t.minTranslate()),
                (C = !0),
                (i.allowMomentumBounce = !0))
              : (b = t.minTranslate()),
              s.loop && s.centeredSlides && (T = !0);
          else if (s.freeModeSticky) {
            for (var M, z = 0; z < o.length; z += 1) {
              if (o[z] > -b) {
                M = z;
                break;
              }
            }

            b = -(b =
              Math.abs(o[M] - b) < Math.abs(o[M - 1] - b) ||
              "next" === t.swipeDirection
                ? o[M]
                : o[M - 1]);
          }

          if (
            (T &&
              t.once("transitionEnd", function () {
                t.loopFix();
              }),
            0 !== t.velocity)
          ) {
            if (
              ((g = r
                ? Math.abs((-b - t.translate) / t.velocity)
                : Math.abs((b - t.translate) / t.velocity)),
              s.freeModeSticky)
            ) {
              var P = Math.abs((r ? -b : b) - t.translate),
                k = t.slidesSizesGrid[t.activeIndex];
              g = P < k ? s.speed : P < 2 * k ? 1.5 * s.speed : 2.5 * s.speed;
            }
          } else if (s.freeModeSticky) return void t.slideToClosest();

          s.freeModeMomentumBounce && C
            ? (t.updateProgress(y),
              t.setTransition(g),
              t.setTranslate(b),
              t.transitionStart(!0, t.swipeDirection),
              (t.animating = !0),
              n.transitionEnd(function () {
                t &&
                  !t.destroyed &&
                  i.allowMomentumBounce &&
                  (t.emit("momentumBounce"),
                  t.setTransition(s.speed),
                  setTimeout(function () {
                    t.setTranslate(y),
                      n.transitionEnd(function () {
                        t && !t.destroyed && t.transitionEnd();
                      });
                  }, 0));
              }))
            : t.velocity
            ? (t.updateProgress(b),
              t.setTransition(g),
              t.setTranslate(b),
              t.transitionStart(!0, t.swipeDirection),
              t.animating ||
                ((t.animating = !0),
                n.transitionEnd(function () {
                  t && !t.destroyed && t.transitionEnd();
                })))
            : t.updateProgress(b),
            t.updateActiveIndex(),
            t.updateSlidesClasses();
        } else if (s.freeModeSticky) return void t.slideToClosest();

        (!s.freeModeMomentum || u >= s.longSwipesMs) &&
          (t.updateProgress(), t.updateActiveIndex(), t.updateSlidesClasses());
      } else {
        for (
          var $ = 0, L = t.slidesSizesGrid[0], I = 0;
          I < l.length;
          I += I < s.slidesPerGroupSkip ? 1 : s.slidesPerGroup
        ) {
          var O = I < s.slidesPerGroupSkip - 1 ? 1 : s.slidesPerGroup;
          void 0 !== l[I + O]
            ? h >= l[I] && h < l[I + O] && (($ = I), (L = l[I + O] - l[I]))
            : h >= l[I] && (($ = I), (L = l[l.length - 1] - l[l.length - 2]));
        }

        var A = (h - l[$]) / L,
          D = $ < s.slidesPerGroupSkip - 1 ? 1 : s.slidesPerGroup;

        if (u > s.longSwipesMs) {
          if (!s.longSwipes) return void t.slideTo(t.activeIndex);
          "next" === t.swipeDirection &&
            (A >= s.longSwipesRatio ? t.slideTo($ + D) : t.slideTo($)),
            "prev" === t.swipeDirection &&
              (A > 1 - s.longSwipesRatio ? t.slideTo($ + D) : t.slideTo($));
        } else {
          if (!s.shortSwipes) return void t.slideTo(t.activeIndex);
          t.navigation &&
          (d.target === t.navigation.nextEl || d.target === t.navigation.prevEl)
            ? d.target === t.navigation.nextEl
              ? t.slideTo($ + D)
              : t.slideTo($)
            : ("next" === t.swipeDirection && t.slideTo($ + D),
              "prev" === t.swipeDirection && t.slideTo($));
        }
      }
  }

  function G() {
    var e = this.params,
      t = this.el;

    if (!t || 0 !== t.offsetWidth) {
      e.breakpoints && this.setBreakpoint();
      var i = this.allowSlideNext,
        s = this.allowSlidePrev,
        a = this.snapGrid;
      (this.allowSlideNext = !0),
        (this.allowSlidePrev = !0),
        this.updateSize(),
        this.updateSlides(),
        this.updateSlidesClasses(),
        ("auto" === e.slidesPerView || e.slidesPerView > 1) &&
        this.isEnd &&
        !this.isBeginning &&
        !this.params.centeredSlides
          ? this.slideTo(this.slides.length - 1, 0, !1, !0)
          : this.slideTo(this.activeIndex, 0, !1, !0),
        this.autoplay &&
          this.autoplay.running &&
          this.autoplay.paused &&
          this.autoplay.run(),
        (this.allowSlidePrev = s),
        (this.allowSlideNext = i),
        this.params.watchOverflow &&
          a !== this.snapGrid &&
          this.checkOverflow();
    }
  }

  function N(e) {
    this.allowClick ||
      (this.params.preventClicks && e.preventDefault(),
      this.params.preventClicksPropagation &&
        this.animating &&
        (e.stopPropagation(), e.stopImmediatePropagation()));
  }

  function B() {
    var e = this.wrapperEl,
      t = this.rtlTranslate;
    (this.previousTranslate = this.translate),
      this.isHorizontal()
        ? (this.translate = t
            ? e.scrollWidth - e.offsetWidth - e.scrollLeft
            : -e.scrollLeft)
        : (this.translate = -e.scrollTop),
      -0 === this.translate && (this.translate = 0),
      this.updateActiveIndex(),
      this.updateSlidesClasses();
    var i = this.maxTranslate() - this.minTranslate();
    (0 === i ? 0 : (this.translate - this.minTranslate()) / i) !==
      this.progress &&
      this.updateProgress(t ? -this.translate : this.translate),
      this.emit("setTranslate", this.translate, !1);
  }

  var H = !1;

  function X() {}

  var Y = {
      init: !0,
      direction: "horizontal",
      touchEventsTarget: "container",
      initialSlide: 0,
      speed: 300,
      cssMode: !1,
      updateOnWindowResize: !0,
      width: null,
      height: null,
      preventInteractionOnTransition: !1,
      userAgent: null,
      url: null,
      edgeSwipeDetection: !1,
      edgeSwipeThreshold: 20,
      freeMode: !1,
      freeModeMomentum: !0,
      freeModeMomentumRatio: 1,
      freeModeMomentumBounce: !0,
      freeModeMomentumBounceRatio: 1,
      freeModeMomentumVelocityRatio: 1,
      freeModeSticky: !1,
      freeModeMinimumVelocity: 0.02,
      autoHeight: !1,
      setWrapperSize: !1,
      virtualTranslate: !1,
      effect: "slide",
      breakpoints: void 0,
      spaceBetween: 0,
      slidesPerView: 1,
      slidesPerColumn: 1,
      slidesPerColumnFill: "column",
      slidesPerGroup: 1,
      slidesPerGroupSkip: 0,
      centeredSlides: !1,
      centeredSlidesBounds: !1,
      slidesOffsetBefore: 0,
      slidesOffsetAfter: 0,
      normalizeSlideIndex: !0,
      centerInsufficientSlides: !1,
      watchOverflow: !1,
      roundLengths: !1,
      touchRatio: 1,
      touchAngle: 45,
      simulateTouch: !0,
      shortSwipes: !0,
      longSwipes: !0,
      longSwipesRatio: 0.5,
      longSwipesMs: 300,
      followFinger: !0,
      allowTouchMove: !0,
      threshold: 0,
      touchMoveStopPropagation: !1,
      touchStartPreventDefault: !0,
      touchStartForcePreventDefault: !1,
      touchReleaseOnEdges: !1,
      uniqueNavElements: !0,
      resistance: !0,
      resistanceRatio: 0.85,
      watchSlidesProgress: !1,
      watchSlidesVisibility: !1,
      grabCursor: !1,
      preventClicks: !0,
      preventClicksPropagation: !0,
      slideToClickedSlide: !1,
      preloadImages: !0,
      updateOnImagesReady: !0,
      loop: !1,
      loopAdditionalSlides: 0,
      loopedSlides: null,
      loopFillGroupWithBlank: !1,
      loopPreventsSlide: !0,
      allowSlidePrev: !0,
      allowSlideNext: !0,
      swipeHandler: null,
      noSwiping: !0,
      noSwipingClass: "swiper-no-swiping",
      noSwipingSelector: null,
      passiveListeners: !0,
      containerModifierClass: "swiper-container-",
      slideClass: "swiper-slide",
      slideBlankClass: "swiper-slide-invisible-blank",
      slideActiveClass: "swiper-slide-active",
      slideDuplicateActiveClass: "swiper-slide-duplicate-active",
      slideVisibleClass: "swiper-slide-visible",
      slideDuplicateClass: "swiper-slide-duplicate",
      slideNextClass: "swiper-slide-next",
      slideDuplicateNextClass: "swiper-slide-duplicate-next",
      slidePrevClass: "swiper-slide-prev",
      slideDuplicatePrevClass: "swiper-slide-duplicate-prev",
      wrapperClass: "swiper-wrapper",
      runCallbacksOnInit: !0,
      _emitClasses: !1,
    },
    V = {
      modular: {
        useParams: function useParams(e) {
          var t = this;
          t.modules &&
            Object.keys(t.modules).forEach(function (i) {
              var s = t.modules[i];
              s.params && S(e, s.params);
            });
        },
        useModules: function useModules(e) {
          void 0 === e && (e = {});
          var t = this;
          t.modules &&
            Object.keys(t.modules).forEach(function (i) {
              var s = t.modules[i],
                a = e[i] || {};
              s.on &&
                t.on &&
                Object.keys(s.on).forEach(function (e) {
                  t.on(e, s.on[e]);
                }),
                s.create && s.create.bind(t)(a);
            });
        },
      },
      eventsEmitter: {
        on: function on(e, t, i) {
          var s = this;
          if ("function" != typeof t) return s;
          var a = i ? "unshift" : "push";
          return (
            e.split(" ").forEach(function (e) {
              s.eventsListeners[e] || (s.eventsListeners[e] = []),
                s.eventsListeners[e][a](t);
            }),
            s
          );
        },
        once: function once(e, t, i) {
          var s = this;
          if ("function" != typeof t) return s;

          function a() {
            s.off(e, a), a.__emitterProxy && delete a.__emitterProxy;

            for (
              var i = arguments.length, r = new Array(i), n = 0;
              n < i;
              n++
            ) {
              r[n] = arguments[n];
            }

            t.apply(s, r);
          }

          return (a.__emitterProxy = t), s.on(e, a, i);
        },
        onAny: function onAny(e, t) {
          if ("function" != typeof e) return this;
          var i = t ? "unshift" : "push";
          return (
            this.eventsAnyListeners.indexOf(e) < 0 &&
              this.eventsAnyListeners[i](e),
            this
          );
        },
        offAny: function offAny(e) {
          if (!this.eventsAnyListeners) return this;
          var t = this.eventsAnyListeners.indexOf(e);
          return t >= 0 && this.eventsAnyListeners.splice(t, 1), this;
        },
        off: function off(e, t) {
          var i = this;
          return i.eventsListeners
            ? (e.split(" ").forEach(function (e) {
                void 0 === t
                  ? (i.eventsListeners[e] = [])
                  : i.eventsListeners[e] &&
                    i.eventsListeners[e].forEach(function (s, a) {
                      (s === t ||
                        (s.__emitterProxy && s.__emitterProxy === t)) &&
                        i.eventsListeners[e].splice(a, 1);
                    });
              }),
              i)
            : i;
        },
        emit: function emit() {
          var e,
            t,
            i,
            s = this;
          if (!s.eventsListeners) return s;

          for (var a = arguments.length, r = new Array(a), n = 0; n < a; n++) {
            r[n] = arguments[n];
          }

          "string" == typeof r[0] || Array.isArray(r[0])
            ? ((e = r[0]), (t = r.slice(1, r.length)), (i = s))
            : ((e = r[0].events), (t = r[0].data), (i = r[0].context || s)),
            t.unshift(i);
          var l = Array.isArray(e) ? e : e.split(" ");
          return (
            l.forEach(function (e) {
              if (s.eventsListeners && s.eventsListeners[e]) {
                var a = [];
                s.eventsListeners[e].forEach(function (e) {
                  a.push(e);
                }),
                  a.forEach(function (e) {
                    e.apply(i, t);
                  });
              }
            }),
            s
          );
        },
      },
      update: {
        updateSize: function updateSize() {
          var e,
            t,
            i = this.$el;
          (e =
            void 0 !== this.params.width && null !== this.params.width
              ? this.params.width
              : i[0].clientWidth),
            (t =
              void 0 !== this.params.height && null !== this.params.width
                ? this.params.height
                : i[0].clientHeight),
            (0 === e && this.isHorizontal()) ||
              (0 === t && this.isVertical()) ||
              ((e =
                e -
                parseInt(i.css("padding-left") || 0, 10) -
                parseInt(i.css("padding-right") || 0, 10)),
              (t =
                t -
                parseInt(i.css("padding-top") || 0, 10) -
                parseInt(i.css("padding-bottom") || 0, 10)),
              Number.isNaN(e) && (e = 0),
              Number.isNaN(t) && (t = 0),
              S(this, {
                width: e,
                height: t,
                size: this.isHorizontal() ? e : t,
              }));
        },
        updateSlides: function updateSlides() {
          var e = l(),
            t = this.params,
            i = this.$wrapperEl,
            s = this.size,
            a = this.rtlTranslate,
            r = this.wrongRTL,
            n = this.virtual && t.virtual.enabled,
            o = n ? this.virtual.slides.length : this.slides.length,
            d = i.children("." + this.params.slideClass),
            h = n ? this.virtual.slides.length : d.length,
            p = [],
            u = [],
            c = [];

          function v(e, i) {
            return !t.cssMode || i !== d.length - 1;
          }

          var f = t.slidesOffsetBefore;
          "function" == typeof f && (f = t.slidesOffsetBefore.call(this));
          var m = t.slidesOffsetAfter;
          "function" == typeof m && (m = t.slidesOffsetAfter.call(this));
          var g = this.snapGrid.length,
            w = this.snapGrid.length,
            b = t.spaceBetween,
            y = -f,
            E = 0,
            x = 0;

          if (void 0 !== s) {
            var T, C;
            "string" == typeof b &&
              b.indexOf("%") >= 0 &&
              (b = (parseFloat(b.replace("%", "")) / 100) * s),
              (this.virtualSize = -b),
              a
                ? d.css({
                    marginLeft: "",
                    marginTop: "",
                  })
                : d.css({
                    marginRight: "",
                    marginBottom: "",
                  }),
              t.slidesPerColumn > 1 &&
                ((T =
                  Math.floor(h / t.slidesPerColumn) ===
                  h / this.params.slidesPerColumn
                    ? h
                    : Math.ceil(h / t.slidesPerColumn) * t.slidesPerColumn),
                "auto" !== t.slidesPerView &&
                  "row" === t.slidesPerColumnFill &&
                  (T = Math.max(T, t.slidesPerView * t.slidesPerColumn)));

            for (
              var M,
                z = t.slidesPerColumn,
                P = T / z,
                k = Math.floor(h / t.slidesPerColumn),
                $ = 0;
              $ < h;
              $ += 1
            ) {
              C = 0;
              var L = d.eq($);

              if (t.slidesPerColumn > 1) {
                var I = void 0,
                  O = void 0,
                  A = void 0;

                if ("row" === t.slidesPerColumnFill && t.slidesPerGroup > 1) {
                  var D = Math.floor(
                      $ / (t.slidesPerGroup * t.slidesPerColumn)
                    ),
                    G = $ - t.slidesPerColumn * t.slidesPerGroup * D,
                    N =
                      0 === D
                        ? t.slidesPerGroup
                        : Math.min(
                            Math.ceil((h - D * z * t.slidesPerGroup) / z),
                            t.slidesPerGroup
                          );
                  (I =
                    (O =
                      G - (A = Math.floor(G / N)) * N + D * t.slidesPerGroup) +
                    (A * T) / z),
                    L.css({
                      "-webkit-box-ordinal-group": I,
                      "-moz-box-ordinal-group": I,
                      "-ms-flex-order": I,
                      "-webkit-order": I,
                      order: I,
                    });
                } else
                  "column" === t.slidesPerColumnFill
                    ? ((A = $ - (O = Math.floor($ / z)) * z),
                      (O > k || (O === k && A === z - 1)) &&
                        (A += 1) >= z &&
                        ((A = 0), (O += 1)))
                    : (O = $ - (A = Math.floor($ / P)) * P);

                L.css(
                  "margin-" + (this.isHorizontal() ? "top" : "left"),
                  0 !== A && t.spaceBetween && t.spaceBetween + "px"
                );
              }

              if ("none" !== L.css("display")) {
                if ("auto" === t.slidesPerView) {
                  var B = e.getComputedStyle(L[0], null),
                    H = L[0].style.transform,
                    X = L[0].style.webkitTransform;
                  if (
                    (H && (L[0].style.transform = "none"),
                    X && (L[0].style.webkitTransform = "none"),
                    t.roundLengths)
                  )
                    C = this.isHorizontal()
                      ? L.outerWidth(!0)
                      : L.outerHeight(!0);
                  else if (this.isHorizontal()) {
                    var Y = parseFloat(B.getPropertyValue("width") || 0),
                      V = parseFloat(B.getPropertyValue("padding-left") || 0),
                      F = parseFloat(B.getPropertyValue("padding-right") || 0),
                      W = parseFloat(B.getPropertyValue("margin-left") || 0),
                      R = parseFloat(B.getPropertyValue("margin-right") || 0),
                      q = B.getPropertyValue("box-sizing");
                    C = q && "border-box" === q ? Y + W + R : Y + V + F + W + R;
                  } else {
                    var j = parseFloat(B.getPropertyValue("height") || 0),
                      _ = parseFloat(B.getPropertyValue("padding-top") || 0),
                      U = parseFloat(B.getPropertyValue("padding-bottom") || 0),
                      K = parseFloat(B.getPropertyValue("margin-top") || 0),
                      Z = parseFloat(B.getPropertyValue("margin-bottom") || 0),
                      J = B.getPropertyValue("box-sizing");

                    C = J && "border-box" === J ? j + K + Z : j + _ + U + K + Z;
                  }
                  H && (L[0].style.transform = H),
                    X && (L[0].style.webkitTransform = X),
                    t.roundLengths && (C = Math.floor(C));
                } else
                  (C = (s - (t.slidesPerView - 1) * b) / t.slidesPerView),
                    t.roundLengths && (C = Math.floor(C)),
                    d[$] &&
                      (this.isHorizontal()
                        ? (d[$].style.width = C + "px")
                        : (d[$].style.height = C + "px"));

                d[$] && (d[$].swiperSlideSize = C),
                  c.push(C),
                  t.centeredSlides
                    ? ((y = y + C / 2 + E / 2 + b),
                      0 === E && 0 !== $ && (y = y - s / 2 - b),
                      0 === $ && (y = y - s / 2 - b),
                      Math.abs(y) < 0.001 && (y = 0),
                      t.roundLengths && (y = Math.floor(y)),
                      x % t.slidesPerGroup == 0 && p.push(y),
                      u.push(y))
                    : (t.roundLengths && (y = Math.floor(y)),
                      (x - Math.min(this.params.slidesPerGroupSkip, x)) %
                        this.params.slidesPerGroup ==
                        0 && p.push(y),
                      u.push(y),
                      (y = y + C + b)),
                  (this.virtualSize += C + b),
                  (E = C),
                  (x += 1);
              }
            }

            if (
              ((this.virtualSize = Math.max(this.virtualSize, s) + m),
              a &&
                r &&
                ("slide" === t.effect || "coverflow" === t.effect) &&
                i.css({
                  width: this.virtualSize + t.spaceBetween + "px",
                }),
              t.setWrapperSize &&
                (this.isHorizontal()
                  ? i.css({
                      width: this.virtualSize + t.spaceBetween + "px",
                    })
                  : i.css({
                      height: this.virtualSize + t.spaceBetween + "px",
                    })),
              t.slidesPerColumn > 1 &&
                ((this.virtualSize = (C + t.spaceBetween) * T),
                (this.virtualSize =
                  Math.ceil(this.virtualSize / t.slidesPerColumn) -
                  t.spaceBetween),
                this.isHorizontal()
                  ? i.css({
                      width: this.virtualSize + t.spaceBetween + "px",
                    })
                  : i.css({
                      height: this.virtualSize + t.spaceBetween + "px",
                    }),
                t.centeredSlides))
            ) {
              M = [];

              for (var Q = 0; Q < p.length; Q += 1) {
                var ee = p[Q];
                t.roundLengths && (ee = Math.floor(ee)),
                  p[Q] < this.virtualSize + p[0] && M.push(ee);
              }

              p = M;
            }

            if (!t.centeredSlides) {
              M = [];

              for (var te = 0; te < p.length; te += 1) {
                var ie = p[te];
                t.roundLengths && (ie = Math.floor(ie)),
                  p[te] <= this.virtualSize - s && M.push(ie);
              }

              (p = M),
                Math.floor(this.virtualSize - s) - Math.floor(p[p.length - 1]) >
                  1 && p.push(this.virtualSize - s);
            }

            if (
              (0 === p.length && (p = [0]),
              0 !== t.spaceBetween &&
                (this.isHorizontal()
                  ? a
                    ? d.filter(v).css({
                        marginLeft: b + "px",
                      })
                    : d.filter(v).css({
                        marginRight: b + "px",
                      })
                  : d.filter(v).css({
                      marginBottom: b + "px",
                    })),
              t.centeredSlides && t.centeredSlidesBounds)
            ) {
              var se = 0;
              c.forEach(function (e) {
                se += e + (t.spaceBetween ? t.spaceBetween : 0);
              });
              var ae = (se -= t.spaceBetween) - s;
              p = p.map(function (e) {
                return e < 0 ? -f : e > ae ? ae + m : e;
              });
            }

            if (t.centerInsufficientSlides) {
              var re = 0;

              if (
                (c.forEach(function (e) {
                  re += e + (t.spaceBetween ? t.spaceBetween : 0);
                }),
                (re -= t.spaceBetween) < s)
              ) {
                var ne = (s - re) / 2;
                p.forEach(function (e, t) {
                  p[t] = e - ne;
                }),
                  u.forEach(function (e, t) {
                    u[t] = e + ne;
                  });
              }
            }

            S(this, {
              slides: d,
              snapGrid: p,
              slidesGrid: u,
              slidesSizesGrid: c,
            }),
              h !== o && this.emit("slidesLengthChange"),
              p.length !== g &&
                (this.params.watchOverflow && this.checkOverflow(),
                this.emit("snapGridLengthChange")),
              u.length !== w && this.emit("slidesGridLengthChange"),
              (t.watchSlidesProgress || t.watchSlidesVisibility) &&
                this.updateSlidesOffset();
          }
        },
        updateAutoHeight: function updateAutoHeight(e) {
          var t,
            i = [],
            s = 0;
          if (
            ("number" == typeof e
              ? this.setTransition(e)
              : !0 === e && this.setTransition(this.params.speed),
            "auto" !== this.params.slidesPerView &&
              this.params.slidesPerView > 1)
          ) {
            if (this.params.centeredSlides)
              this.visibleSlides.each(function (e) {
                i.push(e);
              });
            else
              for (t = 0; t < Math.ceil(this.params.slidesPerView); t += 1) {
                var a = this.activeIndex + t;
                if (a > this.slides.length) break;
                i.push(this.slides.eq(a)[0]);
              }
          } else i.push(this.slides.eq(this.activeIndex)[0]);

          for (t = 0; t < i.length; t += 1) {
            if (void 0 !== i[t]) {
              var r = i[t].offsetHeight;
              s = r > s ? r : s;
            }
          }

          s && this.$wrapperEl.css("height", s + "px");
        },
        updateSlidesOffset: function updateSlidesOffset() {
          for (var e = this.slides, t = 0; t < e.length; t += 1) {
            e[t].swiperSlideOffset = this.isHorizontal()
              ? e[t].offsetLeft
              : e[t].offsetTop;
          }
        },
        updateSlidesProgress: function updateSlidesProgress(e) {
          void 0 === e && (e = (this && this.translate) || 0);
          var t = this.params,
            i = this.slides,
            s = this.rtlTranslate;

          if (0 !== i.length) {
            void 0 === i[0].swiperSlideOffset && this.updateSlidesOffset();
            var a = -e;
            s && (a = e),
              i.removeClass(t.slideVisibleClass),
              (this.visibleSlidesIndexes = []),
              (this.visibleSlides = []);

            for (var r = 0; r < i.length; r += 1) {
              var n = i[r],
                l =
                  (a +
                    (t.centeredSlides ? this.minTranslate() : 0) -
                    n.swiperSlideOffset) /
                  (n.swiperSlideSize + t.spaceBetween);

              if (
                t.watchSlidesVisibility ||
                (t.centeredSlides && t.autoHeight)
              ) {
                var o = -(a - n.swiperSlideOffset),
                  d = o + this.slidesSizesGrid[r];
                ((o >= 0 && o < this.size - 1) ||
                  (d > 1 && d <= this.size) ||
                  (o <= 0 && d >= this.size)) &&
                  (this.visibleSlides.push(n),
                  this.visibleSlidesIndexes.push(r),
                  i.eq(r).addClass(t.slideVisibleClass));
              }

              n.progress = s ? -l : l;
            }

            this.visibleSlides = m(this.visibleSlides);
          }
        },
        updateProgress: function updateProgress(e) {
          if (void 0 === e) {
            var t = this.rtlTranslate ? -1 : 1;
            e = (this && this.translate && this.translate * t) || 0;
          }

          var i = this.params,
            s = this.maxTranslate() - this.minTranslate(),
            a = this.progress,
            r = this.isBeginning,
            n = this.isEnd,
            l = r,
            o = n;
          0 === s
            ? ((a = 0), (r = !0), (n = !0))
            : ((r = (a = (e - this.minTranslate()) / s) <= 0), (n = a >= 1)),
            S(this, {
              progress: a,
              isBeginning: r,
              isEnd: n,
            }),
            (i.watchSlidesProgress ||
              i.watchSlidesVisibility ||
              (i.centeredSlides && i.autoHeight)) &&
              this.updateSlidesProgress(e),
            r && !l && this.emit("reachBeginning toEdge"),
            n && !o && this.emit("reachEnd toEdge"),
            ((l && !r) || (o && !n)) && this.emit("fromEdge"),
            this.emit("progress", a);
        },
        updateSlidesClasses: function updateSlidesClasses() {
          var e,
            t = this.slides,
            i = this.params,
            s = this.$wrapperEl,
            a = this.activeIndex,
            r = this.realIndex,
            n = this.virtual && i.virtual.enabled;
          t.removeClass(
            i.slideActiveClass +
              " " +
              i.slideNextClass +
              " " +
              i.slidePrevClass +
              " " +
              i.slideDuplicateActiveClass +
              " " +
              i.slideDuplicateNextClass +
              " " +
              i.slideDuplicatePrevClass
          ),
            (e = n
              ? this.$wrapperEl.find(
                  "." + i.slideClass + '[data-swiper-slide-index="' + a + '"]'
                )
              : t.eq(a)).addClass(i.slideActiveClass),
            i.loop &&
              (e.hasClass(i.slideDuplicateClass)
                ? s
                    .children(
                      "." +
                        i.slideClass +
                        ":not(." +
                        i.slideDuplicateClass +
                        ')[data-swiper-slide-index="' +
                        r +
                        '"]'
                    )
                    .addClass(i.slideDuplicateActiveClass)
                : s
                    .children(
                      "." +
                        i.slideClass +
                        "." +
                        i.slideDuplicateClass +
                        '[data-swiper-slide-index="' +
                        r +
                        '"]'
                    )
                    .addClass(i.slideDuplicateActiveClass));
          var l = e
            .nextAll("." + i.slideClass)
            .eq(0)
            .addClass(i.slideNextClass);
          i.loop && 0 === l.length && (l = t.eq(0)).addClass(i.slideNextClass);
          var o = e
            .prevAll("." + i.slideClass)
            .eq(0)
            .addClass(i.slidePrevClass);
          i.loop && 0 === o.length && (o = t.eq(-1)).addClass(i.slidePrevClass),
            i.loop &&
              (l.hasClass(i.slideDuplicateClass)
                ? s
                    .children(
                      "." +
                        i.slideClass +
                        ":not(." +
                        i.slideDuplicateClass +
                        ')[data-swiper-slide-index="' +
                        l.attr("data-swiper-slide-index") +
                        '"]'
                    )
                    .addClass(i.slideDuplicateNextClass)
                : s
                    .children(
                      "." +
                        i.slideClass +
                        "." +
                        i.slideDuplicateClass +
                        '[data-swiper-slide-index="' +
                        l.attr("data-swiper-slide-index") +
                        '"]'
                    )
                    .addClass(i.slideDuplicateNextClass),
              o.hasClass(i.slideDuplicateClass)
                ? s
                    .children(
                      "." +
                        i.slideClass +
                        ":not(." +
                        i.slideDuplicateClass +
                        ')[data-swiper-slide-index="' +
                        o.attr("data-swiper-slide-index") +
                        '"]'
                    )
                    .addClass(i.slideDuplicatePrevClass)
                : s
                    .children(
                      "." +
                        i.slideClass +
                        "." +
                        i.slideDuplicateClass +
                        '[data-swiper-slide-index="' +
                        o.attr("data-swiper-slide-index") +
                        '"]'
                    )
                    .addClass(i.slideDuplicatePrevClass)),
            this.emitSlidesClasses();
        },
        updateActiveIndex: function updateActiveIndex(e) {
          var t,
            i = this.rtlTranslate ? this.translate : -this.translate,
            s = this.slidesGrid,
            a = this.snapGrid,
            r = this.params,
            n = this.activeIndex,
            l = this.realIndex,
            o = this.snapIndex,
            d = e;

          if (void 0 === d) {
            for (var h = 0; h < s.length; h += 1) {
              void 0 !== s[h + 1]
                ? i >= s[h] && i < s[h + 1] - (s[h + 1] - s[h]) / 2
                  ? (d = h)
                  : i >= s[h] && i < s[h + 1] && (d = h + 1)
                : i >= s[h] && (d = h);
            }

            r.normalizeSlideIndex && (d < 0 || void 0 === d) && (d = 0);
          }

          if (a.indexOf(i) >= 0) t = a.indexOf(i);
          else {
            var p = Math.min(r.slidesPerGroupSkip, d);
            t = p + Math.floor((d - p) / r.slidesPerGroup);
          }

          if ((t >= a.length && (t = a.length - 1), d !== n)) {
            var u = parseInt(
              this.slides.eq(d).attr("data-swiper-slide-index") || d,
              10
            );
            S(this, {
              snapIndex: t,
              realIndex: u,
              previousIndex: n,
              activeIndex: d,
            }),
              this.emit("activeIndexChange"),
              this.emit("snapIndexChange"),
              l !== u && this.emit("realIndexChange"),
              (this.initialized || this.params.runCallbacksOnInit) &&
                this.emit("slideChange");
          } else
            t !== o && ((this.snapIndex = t), this.emit("snapIndexChange"));
        },
        updateClickedSlide: function updateClickedSlide(e) {
          var t = this.params,
            i = m(e.target).closest("." + t.slideClass)[0],
            s = !1;
          if (i)
            for (var a = 0; a < this.slides.length; a += 1) {
              this.slides[a] === i && (s = !0);
            }
          if (!i || !s)
            return (
              (this.clickedSlide = void 0), void (this.clickedIndex = void 0)
            );
          (this.clickedSlide = i),
            this.virtual && this.params.virtual.enabled
              ? (this.clickedIndex = parseInt(
                  m(i).attr("data-swiper-slide-index"),
                  10
                ))
              : (this.clickedIndex = m(i).index()),
            t.slideToClickedSlide &&
              void 0 !== this.clickedIndex &&
              this.clickedIndex !== this.activeIndex &&
              this.slideToClickedSlide();
        },
      },
      translate: {
        getTranslate: function getTranslate(e) {
          void 0 === e && (e = this.isHorizontal() ? "x" : "y");
          var t = this.params,
            i = this.rtlTranslate,
            s = this.translate,
            a = this.$wrapperEl;
          if (t.virtualTranslate) return i ? -s : s;
          if (t.cssMode) return s;
          var r = T(a[0], e);
          return i && (r = -r), r || 0;
        },
        setTranslate: function setTranslate(e, t) {
          var i = this.rtlTranslate,
            s = this.params,
            a = this.$wrapperEl,
            r = this.wrapperEl,
            n = this.progress,
            l = 0,
            o = 0;
          this.isHorizontal() ? (l = i ? -e : e) : (o = e),
            s.roundLengths && ((l = Math.floor(l)), (o = Math.floor(o))),
            s.cssMode
              ? (r[
                  this.isHorizontal() ? "scrollLeft" : "scrollTop"
                ] = this.isHorizontal() ? -l : -o)
              : s.virtualTranslate ||
                a.transform("translate3d(" + l + "px, " + o + "px, 0px)"),
            (this.previousTranslate = this.translate),
            (this.translate = this.isHorizontal() ? l : o);
          var d = this.maxTranslate() - this.minTranslate();
          (0 === d ? 0 : (e - this.minTranslate()) / d) !== n &&
            this.updateProgress(e),
            this.emit("setTranslate", this.translate, t);
        },
        minTranslate: function minTranslate() {
          return -this.snapGrid[0];
        },
        maxTranslate: function maxTranslate() {
          return -this.snapGrid[this.snapGrid.length - 1];
        },
        translateTo: function translateTo(e, t, i, s, a) {
          void 0 === e && (e = 0),
            void 0 === t && (t = this.params.speed),
            void 0 === i && (i = !0),
            void 0 === s && (s = !0);
          var r = this,
            n = r.params,
            l = r.wrapperEl;
          if (r.animating && n.preventInteractionOnTransition) return !1;
          var o,
            d = r.minTranslate(),
            h = r.maxTranslate();

          if (
            ((o = s && e > d ? d : s && e < h ? h : e),
            r.updateProgress(o),
            n.cssMode)
          ) {
            var p,
              u = r.isHorizontal();
            if (0 === t) l[u ? "scrollLeft" : "scrollTop"] = -o;
            else if (l.scrollTo)
              l.scrollTo(
                (((p = {})[u ? "left" : "top"] = -o),
                (p.behavior = "smooth"),
                p)
              );
            else l[u ? "scrollLeft" : "scrollTop"] = -o;
            return !0;
          }

          return (
            0 === t
              ? (r.setTransition(0),
                r.setTranslate(o),
                i &&
                  (r.emit("beforeTransitionStart", t, a),
                  r.emit("transitionEnd")))
              : (r.setTransition(t),
                r.setTranslate(o),
                i &&
                  (r.emit("beforeTransitionStart", t, a),
                  r.emit("transitionStart")),
                r.animating ||
                  ((r.animating = !0),
                  r.onTranslateToWrapperTransitionEnd ||
                    (r.onTranslateToWrapperTransitionEnd = function (e) {
                      r &&
                        !r.destroyed &&
                        e.target === this &&
                        (r.$wrapperEl[0].removeEventListener(
                          "transitionend",
                          r.onTranslateToWrapperTransitionEnd
                        ),
                        r.$wrapperEl[0].removeEventListener(
                          "webkitTransitionEnd",
                          r.onTranslateToWrapperTransitionEnd
                        ),
                        (r.onTranslateToWrapperTransitionEnd = null),
                        delete r.onTranslateToWrapperTransitionEnd,
                        i && r.emit("transitionEnd"));
                    }),
                  r.$wrapperEl[0].addEventListener(
                    "transitionend",
                    r.onTranslateToWrapperTransitionEnd
                  ),
                  r.$wrapperEl[0].addEventListener(
                    "webkitTransitionEnd",
                    r.onTranslateToWrapperTransitionEnd
                  ))),
            !0
          );
        },
      },
      transition: {
        setTransition: function setTransition(e, t) {
          this.params.cssMode || this.$wrapperEl.transition(e),
            this.emit("setTransition", e, t);
        },
        transitionStart: function transitionStart(e, t) {
          void 0 === e && (e = !0);
          var i = this.activeIndex,
            s = this.params,
            a = this.previousIndex;

          if (!s.cssMode) {
            s.autoHeight && this.updateAutoHeight();
            var r = t;

            if (
              (r || (r = i > a ? "next" : i < a ? "prev" : "reset"),
              this.emit("transitionStart"),
              e && i !== a)
            ) {
              if ("reset" === r)
                return void this.emit("slideResetTransitionStart");
              this.emit("slideChangeTransitionStart"),
                "next" === r
                  ? this.emit("slideNextTransitionStart")
                  : this.emit("slidePrevTransitionStart");
            }
          }
        },
        transitionEnd: function transitionEnd(e, t) {
          void 0 === e && (e = !0);
          var i = this.activeIndex,
            s = this.previousIndex,
            a = this.params;

          if (((this.animating = !1), !a.cssMode)) {
            this.setTransition(0);
            var r = t;

            if (
              (r || (r = i > s ? "next" : i < s ? "prev" : "reset"),
              this.emit("transitionEnd"),
              e && i !== s)
            ) {
              if ("reset" === r)
                return void this.emit("slideResetTransitionEnd");
              this.emit("slideChangeTransitionEnd"),
                "next" === r
                  ? this.emit("slideNextTransitionEnd")
                  : this.emit("slidePrevTransitionEnd");
            }
          }
        },
      },
      slide: {
        slideTo: function slideTo(e, t, i, s) {
          void 0 === e && (e = 0),
            void 0 === t && (t = this.params.speed),
            void 0 === i && (i = !0);
          var a = this,
            r = e;
          r < 0 && (r = 0);
          var n = a.params,
            l = a.snapGrid,
            o = a.slidesGrid,
            d = a.previousIndex,
            h = a.activeIndex,
            p = a.rtlTranslate,
            u = a.wrapperEl;
          if (a.animating && n.preventInteractionOnTransition) return !1;
          var c = Math.min(a.params.slidesPerGroupSkip, r),
            v = c + Math.floor((r - c) / a.params.slidesPerGroup);
          v >= l.length && (v = l.length - 1),
            (h || n.initialSlide || 0) === (d || 0) &&
              i &&
              a.emit("beforeSlideChangeStart");
          var f,
            m = -l[v];
          if ((a.updateProgress(m), n.normalizeSlideIndex))
            for (var g = 0; g < o.length; g += 1) {
              -Math.floor(100 * m) >= Math.floor(100 * o[g]) && (r = g);
            }

          if (a.initialized && r !== h) {
            if (!a.allowSlideNext && m < a.translate && m < a.minTranslate())
              return !1;
            if (
              !a.allowSlidePrev &&
              m > a.translate &&
              m > a.maxTranslate() &&
              (h || 0) !== r
            )
              return !1;
          }

          if (
            ((f = r > h ? "next" : r < h ? "prev" : "reset"),
            (p && -m === a.translate) || (!p && m === a.translate))
          )
            return (
              a.updateActiveIndex(r),
              n.autoHeight && a.updateAutoHeight(),
              a.updateSlidesClasses(),
              "slide" !== n.effect && a.setTranslate(m),
              "reset" !== f && (a.transitionStart(i, f), a.transitionEnd(i, f)),
              !1
            );

          if (n.cssMode) {
            var w,
              b = a.isHorizontal(),
              y = -m;
            if ((p && (y = u.scrollWidth - u.offsetWidth - y), 0 === t))
              u[b ? "scrollLeft" : "scrollTop"] = y;
            else if (u.scrollTo)
              u.scrollTo(
                (((w = {})[b ? "left" : "top"] = y), (w.behavior = "smooth"), w)
              );
            else u[b ? "scrollLeft" : "scrollTop"] = y;
            return !0;
          }

          return (
            0 === t
              ? (a.setTransition(0),
                a.setTranslate(m),
                a.updateActiveIndex(r),
                a.updateSlidesClasses(),
                a.emit("beforeTransitionStart", t, s),
                a.transitionStart(i, f),
                a.transitionEnd(i, f))
              : (a.setTransition(t),
                a.setTranslate(m),
                a.updateActiveIndex(r),
                a.updateSlidesClasses(),
                a.emit("beforeTransitionStart", t, s),
                a.transitionStart(i, f),
                a.animating ||
                  ((a.animating = !0),
                  a.onSlideToWrapperTransitionEnd ||
                    (a.onSlideToWrapperTransitionEnd = function (e) {
                      a &&
                        !a.destroyed &&
                        e.target === this &&
                        (a.$wrapperEl[0].removeEventListener(
                          "transitionend",
                          a.onSlideToWrapperTransitionEnd
                        ),
                        a.$wrapperEl[0].removeEventListener(
                          "webkitTransitionEnd",
                          a.onSlideToWrapperTransitionEnd
                        ),
                        (a.onSlideToWrapperTransitionEnd = null),
                        delete a.onSlideToWrapperTransitionEnd,
                        a.transitionEnd(i, f));
                    }),
                  a.$wrapperEl[0].addEventListener(
                    "transitionend",
                    a.onSlideToWrapperTransitionEnd
                  ),
                  a.$wrapperEl[0].addEventListener(
                    "webkitTransitionEnd",
                    a.onSlideToWrapperTransitionEnd
                  ))),
            !0
          );
        },
        slideToLoop: function slideToLoop(e, t, i, s) {
          void 0 === e && (e = 0),
            void 0 === t && (t = this.params.speed),
            void 0 === i && (i = !0);
          var a = e;
          return (
            this.params.loop && (a += this.loopedSlides),
            this.slideTo(a, t, i, s)
          );
        },
        slideNext: function slideNext(e, t, i) {
          void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
          var s = this.params,
            a = this.animating,
            r = this.activeIndex < s.slidesPerGroupSkip ? 1 : s.slidesPerGroup;

          if (s.loop) {
            if (a && s.loopPreventsSlide) return !1;
            this.loopFix(), (this._clientLeft = this.$wrapperEl[0].clientLeft);
          }

          return this.slideTo(this.activeIndex + r, e, t, i);
        },
        slidePrev: function slidePrev(e, t, i) {
          void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
          var s = this.params,
            a = this.animating,
            r = this.snapGrid,
            n = this.slidesGrid,
            l = this.rtlTranslate;

          if (s.loop) {
            if (a && s.loopPreventsSlide) return !1;
            this.loopFix(), (this._clientLeft = this.$wrapperEl[0].clientLeft);
          }

          function o(e) {
            return e < 0 ? -Math.floor(Math.abs(e)) : Math.floor(e);
          }

          var d,
            h = o(l ? this.translate : -this.translate),
            p = r.map(function (e) {
              return o(e);
            }),
            u = (r[p.indexOf(h)], r[p.indexOf(h) - 1]);
          return (
            void 0 === u &&
              s.cssMode &&
              r.forEach(function (e) {
                !u && h >= e && (u = e);
              }),
            void 0 !== u &&
              (d = n.indexOf(u)) < 0 &&
              (d = this.activeIndex - 1),
            this.slideTo(d, e, t, i)
          );
        },
        slideReset: function slideReset(e, t, i) {
          return (
            void 0 === e && (e = this.params.speed),
            void 0 === t && (t = !0),
            this.slideTo(this.activeIndex, e, t, i)
          );
        },
        slideToClosest: function slideToClosest(e, t, i, s) {
          void 0 === e && (e = this.params.speed),
            void 0 === t && (t = !0),
            void 0 === s && (s = 0.5);
          var a = this.activeIndex,
            r = Math.min(this.params.slidesPerGroupSkip, a),
            n = r + Math.floor((a - r) / this.params.slidesPerGroup),
            l = this.rtlTranslate ? this.translate : -this.translate;

          if (l >= this.snapGrid[n]) {
            var o = this.snapGrid[n];
            l - o > (this.snapGrid[n + 1] - o) * s &&
              (a += this.params.slidesPerGroup);
          } else {
            var d = this.snapGrid[n - 1];
            l - d <= (this.snapGrid[n] - d) * s &&
              (a -= this.params.slidesPerGroup);
          }

          return (
            (a = Math.max(a, 0)),
            (a = Math.min(a, this.slidesGrid.length - 1)),
            this.slideTo(a, e, t, i)
          );
        },
        slideToClickedSlide: function slideToClickedSlide() {
          var e,
            t = this,
            i = t.params,
            s = t.$wrapperEl,
            a =
              "auto" === i.slidesPerView
                ? t.slidesPerViewDynamic()
                : i.slidesPerView,
            r = t.clickedIndex;

          if (i.loop) {
            if (t.animating) return;
            (e = parseInt(
              m(t.clickedSlide).attr("data-swiper-slide-index"),
              10
            )),
              i.centeredSlides
                ? r < t.loopedSlides - a / 2 ||
                  r > t.slides.length - t.loopedSlides + a / 2
                  ? (t.loopFix(),
                    (r = s
                      .children(
                        "." +
                          i.slideClass +
                          '[data-swiper-slide-index="' +
                          e +
                          '"]:not(.' +
                          i.slideDuplicateClass +
                          ")"
                      )
                      .eq(0)
                      .index()),
                    E(function () {
                      t.slideTo(r);
                    }))
                  : t.slideTo(r)
                : r > t.slides.length - a
                ? (t.loopFix(),
                  (r = s
                    .children(
                      "." +
                        i.slideClass +
                        '[data-swiper-slide-index="' +
                        e +
                        '"]:not(.' +
                        i.slideDuplicateClass +
                        ")"
                    )
                    .eq(0)
                    .index()),
                  E(function () {
                    t.slideTo(r);
                  }))
                : t.slideTo(r);
          } else t.slideTo(r);
        },
      },
      loop: {
        loopCreate: function loopCreate() {
          var e = this,
            t = r(),
            i = e.params,
            s = e.$wrapperEl;
          s.children("." + i.slideClass + "." + i.slideDuplicateClass).remove();
          var a = s.children("." + i.slideClass);

          if (i.loopFillGroupWithBlank) {
            var n = i.slidesPerGroup - (a.length % i.slidesPerGroup);

            if (n !== i.slidesPerGroup) {
              for (var l = 0; l < n; l += 1) {
                var o = m(t.createElement("div")).addClass(
                  i.slideClass + " " + i.slideBlankClass
                );
                s.append(o);
              }

              a = s.children("." + i.slideClass);
            }
          }

          "auto" !== i.slidesPerView ||
            i.loopedSlides ||
            (i.loopedSlides = a.length),
            (e.loopedSlides = Math.ceil(
              parseFloat(i.loopedSlides || i.slidesPerView, 10)
            )),
            (e.loopedSlides += i.loopAdditionalSlides),
            e.loopedSlides > a.length && (e.loopedSlides = a.length);
          var d = [],
            h = [];
          a.each(function (t, i) {
            var s = m(t);
            i < e.loopedSlides && h.push(t),
              i < a.length && i >= a.length - e.loopedSlides && d.push(t),
              s.attr("data-swiper-slide-index", i);
          });

          for (var p = 0; p < h.length; p += 1) {
            s.append(m(h[p].cloneNode(!0)).addClass(i.slideDuplicateClass));
          }

          for (var u = d.length - 1; u >= 0; u -= 1) {
            s.prepend(m(d[u].cloneNode(!0)).addClass(i.slideDuplicateClass));
          }
        },
        loopFix: function loopFix() {
          this.emit("beforeLoopFix");
          var e,
            t = this.activeIndex,
            i = this.slides,
            s = this.loopedSlides,
            a = this.allowSlidePrev,
            r = this.allowSlideNext,
            n = this.snapGrid,
            l = this.rtlTranslate;
          (this.allowSlidePrev = !0), (this.allowSlideNext = !0);
          var o = -n[t] - this.getTranslate();
          if (t < s)
            (e = i.length - 3 * s + t),
              (e += s),
              this.slideTo(e, 0, !1, !0) &&
                0 !== o &&
                this.setTranslate((l ? -this.translate : this.translate) - o);
          else if (t >= i.length - s) {
            (e = -i.length + t + s),
              (e += s),
              this.slideTo(e, 0, !1, !0) &&
                0 !== o &&
                this.setTranslate((l ? -this.translate : this.translate) - o);
          }
          (this.allowSlidePrev = a),
            (this.allowSlideNext = r),
            this.emit("loopFix");
        },
        loopDestroy: function loopDestroy() {
          var e = this.$wrapperEl,
            t = this.params,
            i = this.slides;
          e
            .children(
              "." +
                t.slideClass +
                "." +
                t.slideDuplicateClass +
                ",." +
                t.slideClass +
                "." +
                t.slideBlankClass
            )
            .remove(),
            i.removeAttr("data-swiper-slide-index");
        },
      },
      grabCursor: {
        setGrabCursor: function setGrabCursor(e) {
          if (
            !(
              this.support.touch ||
              !this.params.simulateTouch ||
              (this.params.watchOverflow && this.isLocked) ||
              this.params.cssMode
            )
          ) {
            var t = this.el;
            (t.style.cursor = "move"),
              (t.style.cursor = e ? "-webkit-grabbing" : "-webkit-grab"),
              (t.style.cursor = e ? "-moz-grabbin" : "-moz-grab"),
              (t.style.cursor = e ? "grabbing" : "grab");
          }
        },
        unsetGrabCursor: function unsetGrabCursor() {
          this.support.touch ||
            (this.params.watchOverflow && this.isLocked) ||
            this.params.cssMode ||
            (this.el.style.cursor = "");
        },
      },
      manipulation: {
        appendSlide: function appendSlide(e) {
          var t = this.$wrapperEl,
            i = this.params;
          if (
            (i.loop && this.loopDestroy(),
            "object" == _typeof(e) && "length" in e)
          )
            for (var s = 0; s < e.length; s += 1) {
              e[s] && t.append(e[s]);
            }
          else t.append(e);
          i.loop && this.loopCreate(),
            (i.observer && this.support.observer) || this.update();
        },
        prependSlide: function prependSlide(e) {
          var t = this.params,
            i = this.$wrapperEl,
            s = this.activeIndex;
          t.loop && this.loopDestroy();
          var a = s + 1;

          if ("object" == _typeof(e) && "length" in e) {
            for (var r = 0; r < e.length; r += 1) {
              e[r] && i.prepend(e[r]);
            }

            a = s + e.length;
          } else i.prepend(e);

          t.loop && this.loopCreate(),
            (t.observer && this.support.observer) || this.update(),
            this.slideTo(a, 0, !1);
        },
        addSlide: function addSlide(e, t) {
          var i = this.$wrapperEl,
            s = this.params,
            a = this.activeIndex;
          s.loop &&
            ((a -= this.loopedSlides),
            this.loopDestroy(),
            (this.slides = i.children("." + s.slideClass)));
          var r = this.slides.length;
          if (e <= 0) this.prependSlide(t);
          else if (e >= r) this.appendSlide(t);
          else {
            for (var n = a > e ? a + 1 : a, l = [], o = r - 1; o >= e; o -= 1) {
              var d = this.slides.eq(o);
              d.remove(), l.unshift(d);
            }

            if ("object" == _typeof(t) && "length" in t) {
              for (var h = 0; h < t.length; h += 1) {
                t[h] && i.append(t[h]);
              }

              n = a > e ? a + t.length : a;
            } else i.append(t);

            for (var p = 0; p < l.length; p += 1) {
              i.append(l[p]);
            }

            s.loop && this.loopCreate(),
              (s.observer && this.support.observer) || this.update(),
              s.loop
                ? this.slideTo(n + this.loopedSlides, 0, !1)
                : this.slideTo(n, 0, !1);
          }
        },
        removeSlide: function removeSlide(e) {
          var t = this.params,
            i = this.$wrapperEl,
            s = this.activeIndex;
          t.loop &&
            ((s -= this.loopedSlides),
            this.loopDestroy(),
            (this.slides = i.children("." + t.slideClass)));
          var a,
            r = s;

          if ("object" == _typeof(e) && "length" in e) {
            for (var n = 0; n < e.length; n += 1) {
              (a = e[n]),
                this.slides[a] && this.slides.eq(a).remove(),
                a < r && (r -= 1);
            }

            r = Math.max(r, 0);
          } else
            (a = e),
              this.slides[a] && this.slides.eq(a).remove(),
              a < r && (r -= 1),
              (r = Math.max(r, 0));

          t.loop && this.loopCreate(),
            (t.observer && this.support.observer) || this.update(),
            t.loop
              ? this.slideTo(r + this.loopedSlides, 0, !1)
              : this.slideTo(r, 0, !1);
        },
        removeAllSlides: function removeAllSlides() {
          for (var e = [], t = 0; t < this.slides.length; t += 1) {
            e.push(t);
          }

          this.removeSlide(e);
        },
      },
      events: {
        attachEvents: function attachEvents() {
          var e = r(),
            t = this.params,
            i = this.touchEvents,
            s = this.el,
            a = this.wrapperEl,
            n = this.device,
            l = this.support;
          (this.onTouchStart = O.bind(this)),
            (this.onTouchMove = A.bind(this)),
            (this.onTouchEnd = D.bind(this)),
            t.cssMode && (this.onScroll = B.bind(this)),
            (this.onClick = N.bind(this));
          var o = !!t.nested;
          if (!l.touch && l.pointerEvents)
            s.addEventListener(i.start, this.onTouchStart, !1),
              e.addEventListener(i.move, this.onTouchMove, o),
              e.addEventListener(i.end, this.onTouchEnd, !1);
          else {
            if (l.touch) {
              var d = !(
                "touchstart" !== i.start ||
                !l.passiveListener ||
                !t.passiveListeners
              ) && {
                passive: !0,
                capture: !1,
              };
              s.addEventListener(i.start, this.onTouchStart, d),
                s.addEventListener(
                  i.move,
                  this.onTouchMove,
                  l.passiveListener
                    ? {
                        passive: !1,
                        capture: o,
                      }
                    : o
                ),
                s.addEventListener(i.end, this.onTouchEnd, d),
                i.cancel && s.addEventListener(i.cancel, this.onTouchEnd, d),
                H || (e.addEventListener("touchstart", X), (H = !0));
            }

            ((t.simulateTouch && !n.ios && !n.android) ||
              (t.simulateTouch && !l.touch && n.ios)) &&
              (s.addEventListener("mousedown", this.onTouchStart, !1),
              e.addEventListener("mousemove", this.onTouchMove, o),
              e.addEventListener("mouseup", this.onTouchEnd, !1));
          }
          (t.preventClicks || t.preventClicksPropagation) &&
            s.addEventListener("click", this.onClick, !0),
            t.cssMode && a.addEventListener("scroll", this.onScroll),
            t.updateOnWindowResize
              ? this.on(
                  n.ios || n.android
                    ? "resize orientationchange observerUpdate"
                    : "resize observerUpdate",
                  G,
                  !0
                )
              : this.on("observerUpdate", G, !0);
        },
        detachEvents: function detachEvents() {
          var e = r(),
            t = this.params,
            i = this.touchEvents,
            s = this.el,
            a = this.wrapperEl,
            n = this.device,
            l = this.support,
            o = !!t.nested;
          if (!l.touch && l.pointerEvents)
            s.removeEventListener(i.start, this.onTouchStart, !1),
              e.removeEventListener(i.move, this.onTouchMove, o),
              e.removeEventListener(i.end, this.onTouchEnd, !1);
          else {
            if (l.touch) {
              var d = !(
                "onTouchStart" !== i.start ||
                !l.passiveListener ||
                !t.passiveListeners
              ) && {
                passive: !0,
                capture: !1,
              };
              s.removeEventListener(i.start, this.onTouchStart, d),
                s.removeEventListener(i.move, this.onTouchMove, o),
                s.removeEventListener(i.end, this.onTouchEnd, d),
                i.cancel && s.removeEventListener(i.cancel, this.onTouchEnd, d);
            }

            ((t.simulateTouch && !n.ios && !n.android) ||
              (t.simulateTouch && !l.touch && n.ios)) &&
              (s.removeEventListener("mousedown", this.onTouchStart, !1),
              e.removeEventListener("mousemove", this.onTouchMove, o),
              e.removeEventListener("mouseup", this.onTouchEnd, !1));
          }
          (t.preventClicks || t.preventClicksPropagation) &&
            s.removeEventListener("click", this.onClick, !0),
            t.cssMode && a.removeEventListener("scroll", this.onScroll),
            this.off(
              n.ios || n.android
                ? "resize orientationchange observerUpdate"
                : "resize observerUpdate",
              G
            );
        },
      },
      breakpoints: {
        setBreakpoint: function setBreakpoint() {
          var e = this.activeIndex,
            t = this.initialized,
            i = this.loopedSlides,
            s = void 0 === i ? 0 : i,
            a = this.params,
            r = this.$el,
            n = a.breakpoints;

          if (n && (!n || 0 !== Object.keys(n).length)) {
            var l = this.getBreakpoint(n);

            if (l && this.currentBreakpoint !== l) {
              var o = l in n ? n[l] : void 0;
              o &&
                [
                  "slidesPerView",
                  "spaceBetween",
                  "slidesPerGroup",
                  "slidesPerGroupSkip",
                  "slidesPerColumn",
                ].forEach(function (e) {
                  var t = o[e];
                  void 0 !== t &&
                    (o[e] =
                      "slidesPerView" !== e || ("AUTO" !== t && "auto" !== t)
                        ? "slidesPerView" === e
                          ? parseFloat(t)
                          : parseInt(t, 10)
                        : "auto");
                });
              var d = o || this.originalParams,
                h = a.slidesPerColumn > 1,
                p = d.slidesPerColumn > 1;
              h && !p
                ? (r.removeClass(
                    a.containerModifierClass +
                      "multirow " +
                      a.containerModifierClass +
                      "multirow-column"
                  ),
                  this.emitContainerClasses())
                : !h &&
                  p &&
                  (r.addClass(a.containerModifierClass + "multirow"),
                  "column" === d.slidesPerColumnFill &&
                    r.addClass(a.containerModifierClass + "multirow-column"),
                  this.emitContainerClasses());
              var u = d.direction && d.direction !== a.direction,
                c = a.loop && (d.slidesPerView !== a.slidesPerView || u);
              u && t && this.changeDirection(),
                S(this.params, d),
                S(this, {
                  allowTouchMove: this.params.allowTouchMove,
                  allowSlideNext: this.params.allowSlideNext,
                  allowSlidePrev: this.params.allowSlidePrev,
                }),
                (this.currentBreakpoint = l),
                c &&
                  t &&
                  (this.loopDestroy(),
                  this.loopCreate(),
                  this.updateSlides(),
                  this.slideTo(e - s + this.loopedSlides, 0, !1)),
                this.emit("breakpoint", d);
            }
          }
        },
        getBreakpoint: function getBreakpoint(e) {
          var t = l();

          if (e) {
            var i = !1,
              s = Object.keys(e).map(function (e) {
                if ("string" == typeof e && 0 === e.indexOf("@")) {
                  var i = parseFloat(e.substr(1));
                  return {
                    value: t.innerHeight * i,
                    point: e,
                  };
                }

                return {
                  value: e,
                  point: e,
                };
              });
            s.sort(function (e, t) {
              return parseInt(e.value, 10) - parseInt(t.value, 10);
            });

            for (var a = 0; a < s.length; a += 1) {
              var r = s[a],
                n = r.point;
              r.value <= t.innerWidth && (i = n);
            }

            return i || "max";
          }
        },
      },
      checkOverflow: {
        checkOverflow: function checkOverflow() {
          var e = this.params,
            t = this.isLocked,
            i =
              this.slides.length > 0 &&
              e.slidesOffsetBefore +
                e.spaceBetween * (this.slides.length - 1) +
                this.slides[0].offsetWidth * this.slides.length;
          e.slidesOffsetBefore && e.slidesOffsetAfter && i
            ? (this.isLocked = i <= this.size)
            : (this.isLocked = 1 === this.snapGrid.length),
            (this.allowSlideNext = !this.isLocked),
            (this.allowSlidePrev = !this.isLocked),
            t !== this.isLocked && this.emit(this.isLocked ? "lock" : "unlock"),
            t &&
              t !== this.isLocked &&
              ((this.isEnd = !1), this.navigation && this.navigation.update());
        },
      },
      classes: {
        addClasses: function addClasses() {
          var e = this.classNames,
            t = this.params,
            i = this.rtl,
            s = this.$el,
            a = this.device,
            r = [];
          r.push("initialized"),
            r.push(t.direction),
            t.freeMode && r.push("free-mode"),
            t.autoHeight && r.push("autoheight"),
            i && r.push("rtl"),
            t.slidesPerColumn > 1 &&
              (r.push("multirow"),
              "column" === t.slidesPerColumnFill && r.push("multirow-column")),
            a.android && r.push("android"),
            a.ios && r.push("ios"),
            t.cssMode && r.push("css-mode"),
            r.forEach(function (i) {
              e.push(t.containerModifierClass + i);
            }),
            s.addClass(e.join(" ")),
            this.emitContainerClasses();
        },
        removeClasses: function removeClasses() {
          var e = this.$el,
            t = this.classNames;
          e.removeClass(t.join(" ")), this.emitContainerClasses();
        },
      },
      images: {
        loadImage: function loadImage(e, t, i, s, a, r) {
          var n,
            o = l();

          function d() {
            r && r();
          }

          m(e).parent("picture")[0] || (e.complete && a)
            ? d()
            : t
            ? (((n = new o.Image()).onload = d),
              (n.onerror = d),
              s && (n.sizes = s),
              i && (n.srcset = i),
              t && (n.src = t))
            : d();
        },
        preloadImages: function preloadImages() {
          var e = this;

          function t() {
            null != e &&
              e &&
              !e.destroyed &&
              (void 0 !== e.imagesLoaded && (e.imagesLoaded += 1),
              e.imagesLoaded === e.imagesToLoad.length &&
                (e.params.updateOnImagesReady && e.update(),
                e.emit("imagesReady")));
          }

          e.imagesToLoad = e.$el.find("img");

          for (var i = 0; i < e.imagesToLoad.length; i += 1) {
            var s = e.imagesToLoad[i];
            e.loadImage(
              s,
              s.currentSrc || s.getAttribute("src"),
              s.srcset || s.getAttribute("srcset"),
              s.sizes || s.getAttribute("sizes"),
              !0,
              t
            );
          }
        },
      },
    },
    F = {},
    W = (function () {
      function t() {
        for (
          var e, i, s = arguments.length, a = new Array(s), r = 0;
          r < s;
          r++
        ) {
          a[r] = arguments[r];
        }

        1 === a.length && a[0].constructor && a[0].constructor === Object
          ? (i = a[0])
          : ((e = a[0]), (i = a[1])),
          i || (i = {}),
          (i = S({}, i)),
          e && !i.el && (i.el = e);
        var n = this;
        (n.support = z()),
          (n.device = P({
            userAgent: i.userAgent,
          })),
          (n.browser = k()),
          (n.eventsListeners = {}),
          (n.eventsAnyListeners = []),
          Object.keys(V).forEach(function (e) {
            Object.keys(V[e]).forEach(function (i) {
              t.prototype[i] || (t.prototype[i] = V[e][i]);
            });
          }),
          void 0 === n.modules && (n.modules = {}),
          Object.keys(n.modules).forEach(function (e) {
            var t = n.modules[e];

            if (t.params) {
              var s = Object.keys(t.params)[0],
                a = t.params[s];
              if ("object" != _typeof(a) || null === a) return;
              if (!(s in i) || !("enabled" in a)) return;
              !0 === i[s] &&
                (i[s] = {
                  enabled: !0,
                }),
                "object" != _typeof(i[s]) ||
                  "enabled" in i[s] ||
                  (i[s].enabled = !0),
                i[s] ||
                  (i[s] = {
                    enabled: !1,
                  });
            }
          });
        var l = S({}, Y);
        n.useParams(l),
          (n.params = S({}, l, F, i)),
          (n.originalParams = S({}, n.params)),
          (n.passedParams = S({}, i)),
          n.params &&
            n.params.on &&
            Object.keys(n.params.on).forEach(function (e) {
              n.on(e, n.params.on[e]);
            }),
          (n.$ = m);
        var o = m(n.params.el);

        if ((e = o[0])) {
          if (o.length > 1) {
            var d = [];
            return (
              o.each(function (e) {
                var s = S({}, i, {
                  el: e,
                });
                d.push(new t(s));
              }),
              d
            );
          }

          var h, p, u;
          return (
            (e.swiper = n),
            e && e.shadowRoot && e.shadowRoot.querySelector
              ? ((h = m(
                  e.shadowRoot.querySelector("." + n.params.wrapperClass)
                )).children = function (e) {
                  return o.children(e);
                })
              : (h = o.children("." + n.params.wrapperClass)),
            S(n, {
              $el: o,
              el: e,
              $wrapperEl: h,
              wrapperEl: h[0],
              classNames: [],
              slides: m(),
              slidesGrid: [],
              snapGrid: [],
              slidesSizesGrid: [],
              isHorizontal: function isHorizontal() {
                return "horizontal" === n.params.direction;
              },
              isVertical: function isVertical() {
                return "vertical" === n.params.direction;
              },
              rtl:
                "rtl" === e.dir.toLowerCase() || "rtl" === o.css("direction"),
              rtlTranslate:
                "horizontal" === n.params.direction &&
                ("rtl" === e.dir.toLowerCase() || "rtl" === o.css("direction")),
              wrongRTL: "-webkit-box" === h.css("display"),
              activeIndex: 0,
              realIndex: 0,
              isBeginning: !0,
              isEnd: !1,
              translate: 0,
              previousTranslate: 0,
              progress: 0,
              velocity: 0,
              animating: !1,
              allowSlideNext: n.params.allowSlideNext,
              allowSlidePrev: n.params.allowSlidePrev,
              touchEvents:
                ((p = ["touchstart", "touchmove", "touchend", "touchcancel"]),
                (u = ["mousedown", "mousemove", "mouseup"]),
                n.support.pointerEvents &&
                  (u = ["pointerdown", "pointermove", "pointerup"]),
                (n.touchEventsTouch = {
                  start: p[0],
                  move: p[1],
                  end: p[2],
                  cancel: p[3],
                }),
                (n.touchEventsDesktop = {
                  start: u[0],
                  move: u[1],
                  end: u[2],
                }),
                n.support.touch || !n.params.simulateTouch
                  ? n.touchEventsTouch
                  : n.touchEventsDesktop),
              touchEventsData: {
                isTouched: void 0,
                isMoved: void 0,
                allowTouchCallbacks: void 0,
                touchStartTime: void 0,
                isScrolling: void 0,
                currentTranslate: void 0,
                startTranslate: void 0,
                allowThresholdMove: void 0,
                formElements:
                  "input, select, option, textarea, button, video, label",
                lastClickTime: x(),
                clickTimeout: void 0,
                velocities: [],
                allowMomentumBounce: void 0,
                isTouchEvent: void 0,
                startMoving: void 0,
              },
              allowClick: !0,
              allowTouchMove: n.params.allowTouchMove,
              touches: {
                startX: 0,
                startY: 0,
                currentX: 0,
                currentY: 0,
                diff: 0,
              },
              imagesToLoad: [],
              imagesLoaded: 0,
            }),
            n.useModules(),
            n.emit("_swiper"),
            n.params.init && n.init(),
            n
          );
        }
      }

      var i,
        s,
        a,
        r = t.prototype;
      return (
        (r.emitContainerClasses = function () {
          var e = this;

          if (e.params._emitClasses && e.el) {
            var t = e.el.className.split(" ").filter(function (t) {
              return (
                0 === t.indexOf("swiper-container") ||
                0 === t.indexOf(e.params.containerModifierClass)
              );
            });
            e.emit("_containerClasses", t.join(" "));
          }
        }),
        (r.emitSlidesClasses = function () {
          var e = this;
          e.params._emitClasses &&
            e.el &&
            e.slides.each(function (t) {
              var i = t.className.split(" ").filter(function (t) {
                return (
                  0 === t.indexOf("swiper-slide") ||
                  0 === t.indexOf(e.params.slideClass)
                );
              });
              e.emit("_slideClass", t, i.join(" "));
            });
        }),
        (r.slidesPerViewDynamic = function () {
          var e = this.params,
            t = this.slides,
            i = this.slidesGrid,
            s = this.size,
            a = this.activeIndex,
            r = 1;

          if (e.centeredSlides) {
            for (
              var n, l = t[a].swiperSlideSize, o = a + 1;
              o < t.length;
              o += 1
            ) {
              t[o] &&
                !n &&
                ((r += 1), (l += t[o].swiperSlideSize) > s && (n = !0));
            }

            for (var d = a - 1; d >= 0; d -= 1) {
              t[d] &&
                !n &&
                ((r += 1), (l += t[d].swiperSlideSize) > s && (n = !0));
            }
          } else
            for (var h = a + 1; h < t.length; h += 1) {
              i[h] - i[a] < s && (r += 1);
            }

          return r;
        }),
        (r.update = function () {
          var e = this;

          if (e && !e.destroyed) {
            var t = e.snapGrid,
              i = e.params;
            i.breakpoints && e.setBreakpoint(),
              e.updateSize(),
              e.updateSlides(),
              e.updateProgress(),
              e.updateSlidesClasses(),
              e.params.freeMode
                ? (s(), e.params.autoHeight && e.updateAutoHeight())
                : (("auto" === e.params.slidesPerView ||
                    e.params.slidesPerView > 1) &&
                  e.isEnd &&
                  !e.params.centeredSlides
                    ? e.slideTo(e.slides.length - 1, 0, !1, !0)
                    : e.slideTo(e.activeIndex, 0, !1, !0)) || s(),
              i.watchOverflow && t !== e.snapGrid && e.checkOverflow(),
              e.emit("update");
          }

          function s() {
            var t = e.rtlTranslate ? -1 * e.translate : e.translate,
              i = Math.min(Math.max(t, e.maxTranslate()), e.minTranslate());
            e.setTranslate(i), e.updateActiveIndex(), e.updateSlidesClasses();
          }
        }),
        (r.changeDirection = function (e, t) {
          void 0 === t && (t = !0);
          var i = this.params.direction;
          return (
            e || (e = "horizontal" === i ? "vertical" : "horizontal"),
            e === i ||
              ("horizontal" !== e && "vertical" !== e) ||
              (this.$el
                .removeClass("" + this.params.containerModifierClass + i)
                .addClass("" + this.params.containerModifierClass + e),
              this.emitContainerClasses(),
              (this.params.direction = e),
              this.slides.each(function (t) {
                "vertical" === e ? (t.style.width = "") : (t.style.height = "");
              }),
              this.emit("changeDirection"),
              t && this.update()),
            this
          );
        }),
        (r.init = function () {
          this.initialized ||
            (this.emit("beforeInit"),
            this.params.breakpoints && this.setBreakpoint(),
            this.addClasses(),
            this.params.loop && this.loopCreate(),
            this.updateSize(),
            this.updateSlides(),
            this.params.watchOverflow && this.checkOverflow(),
            this.params.grabCursor && this.setGrabCursor(),
            this.params.preloadImages && this.preloadImages(),
            this.params.loop
              ? this.slideTo(
                  this.params.initialSlide + this.loopedSlides,
                  0,
                  this.params.runCallbacksOnInit
                )
              : this.slideTo(
                  this.params.initialSlide,
                  0,
                  this.params.runCallbacksOnInit
                ),
            this.attachEvents(),
            (this.initialized = !0),
            this.emit("init"));
        }),
        (r.destroy = function (e, t) {
          void 0 === e && (e = !0), void 0 === t && (t = !0);
          var i,
            s = this,
            a = s.params,
            r = s.$el,
            n = s.$wrapperEl,
            l = s.slides;
          return (
            void 0 === s.params ||
              s.destroyed ||
              (s.emit("beforeDestroy"),
              (s.initialized = !1),
              s.detachEvents(),
              a.loop && s.loopDestroy(),
              t &&
                (s.removeClasses(),
                r.removeAttr("style"),
                n.removeAttr("style"),
                l &&
                  l.length &&
                  l
                    .removeClass(
                      [
                        a.slideVisibleClass,
                        a.slideActiveClass,
                        a.slideNextClass,
                        a.slidePrevClass,
                      ].join(" ")
                    )
                    .removeAttr("style")
                    .removeAttr("data-swiper-slide-index")),
              s.emit("destroy"),
              Object.keys(s.eventsListeners).forEach(function (e) {
                s.off(e);
              }),
              !1 !== e &&
                ((s.$el[0].swiper = null),
                (i = s),
                Object.keys(i).forEach(function (e) {
                  try {
                    i[e] = null;
                  } catch (e) {}

                  try {
                    delete i[e];
                  } catch (e) {}
                })),
              (s.destroyed = !0)),
            null
          );
        }),
        (t.extendDefaults = function (e) {
          S(F, e);
        }),
        (t.installModule = function (e) {
          t.prototype.modules || (t.prototype.modules = {});
          var i = e.name || Object.keys(t.prototype.modules).length + "_" + x();
          t.prototype.modules[i] = e;
        }),
        (t.use = function (e) {
          return Array.isArray(e)
            ? (e.forEach(function (e) {
                return t.installModule(e);
              }),
              t)
            : (t.installModule(e), t);
        }),
        (i = t),
        (a = [
          {
            key: "extendedDefaults",
            get: function get() {
              return F;
            },
          },
          {
            key: "defaults",
            get: function get() {
              return Y;
            },
          },
        ]),
        (s = null) && e(i.prototype, s),
        a && e(i, a),
        t
      );
    })();

  W.use([$, I]);
  var R = {
      update: function update(e) {
        var t = this,
          i = t.params,
          s = i.slidesPerView,
          a = i.slidesPerGroup,
          r = i.centeredSlides,
          n = t.params.virtual,
          l = n.addSlidesBefore,
          o = n.addSlidesAfter,
          d = t.virtual,
          h = d.from,
          p = d.to,
          u = d.slides,
          c = d.slidesGrid,
          v = d.renderSlide,
          f = d.offset;
        t.updateActiveIndex();
        var m,
          g,
          w,
          b = t.activeIndex || 0;
        (m = t.rtlTranslate ? "right" : t.isHorizontal() ? "left" : "top"),
          r
            ? ((g = Math.floor(s / 2) + a + o), (w = Math.floor(s / 2) + a + l))
            : ((g = s + (a - 1) + o), (w = a + l));
        var y = Math.max((b || 0) - w, 0),
          E = Math.min((b || 0) + g, u.length - 1),
          x = (t.slidesGrid[y] || 0) - (t.slidesGrid[0] || 0);

        function T() {
          t.updateSlides(),
            t.updateProgress(),
            t.updateSlidesClasses(),
            t.lazy && t.params.lazy.enabled && t.lazy.load();
        }

        if (
          (S(t.virtual, {
            from: y,
            to: E,
            offset: x,
            slidesGrid: t.slidesGrid,
          }),
          h === y && p === E && !e)
        )
          return (
            t.slidesGrid !== c && x !== f && t.slides.css(m, x + "px"),
            void t.updateProgress()
          );
        if (t.params.virtual.renderExternal)
          return (
            t.params.virtual.renderExternal.call(t, {
              offset: x,
              from: y,
              to: E,
              slides: (function () {
                for (var e = [], t = y; t <= E; t += 1) {
                  e.push(u[t]);
                }

                return e;
              })(),
            }),
            void (t.params.virtual.renderExternalUpdate && T())
          );
        var C = [],
          M = [];
        if (e) t.$wrapperEl.find("." + t.params.slideClass).remove();
        else
          for (var z = h; z <= p; z += 1) {
            (z < y || z > E) &&
              t.$wrapperEl
                .find(
                  "." +
                    t.params.slideClass +
                    '[data-swiper-slide-index="' +
                    z +
                    '"]'
                )
                .remove();
          }

        for (var P = 0; P < u.length; P += 1) {
          P >= y &&
            P <= E &&
            (void 0 === p || e
              ? M.push(P)
              : (P > p && M.push(P), P < h && C.push(P)));
        }

        M.forEach(function (e) {
          t.$wrapperEl.append(v(u[e], e));
        }),
          C.sort(function (e, t) {
            return t - e;
          }).forEach(function (e) {
            t.$wrapperEl.prepend(v(u[e], e));
          }),
          t.$wrapperEl.children(".swiper-slide").css(m, x + "px"),
          T();
      },
      renderSlide: function renderSlide(e, t) {
        var i = this.params.virtual;
        if (i.cache && this.virtual.cache[t]) return this.virtual.cache[t];
        var s = i.renderSlide
          ? m(i.renderSlide.call(this, e, t))
          : m(
              '<div class="' +
                this.params.slideClass +
                '" data-swiper-slide-index="' +
                t +
                '">' +
                e +
                "</div>"
            );
        return (
          s.attr("data-swiper-slide-index") ||
            s.attr("data-swiper-slide-index", t),
          i.cache && (this.virtual.cache[t] = s),
          s
        );
      },
      appendSlide: function appendSlide(e) {
        if ("object" == _typeof(e) && "length" in e)
          for (var t = 0; t < e.length; t += 1) {
            e[t] && this.virtual.slides.push(e[t]);
          }
        else this.virtual.slides.push(e);
        this.virtual.update(!0);
      },
      prependSlide: function prependSlide(e) {
        var t = this.activeIndex,
          i = t + 1,
          s = 1;

        if (Array.isArray(e)) {
          for (var a = 0; a < e.length; a += 1) {
            e[a] && this.virtual.slides.unshift(e[a]);
          }

          (i = t + e.length), (s = e.length);
        } else this.virtual.slides.unshift(e);

        if (this.params.virtual.cache) {
          var r = this.virtual.cache,
            n = {};
          Object.keys(r).forEach(function (e) {
            var t = r[e],
              i = t.attr("data-swiper-slide-index");
            i && t.attr("data-swiper-slide-index", parseInt(i, 10) + 1),
              (n[parseInt(e, 10) + s] = t);
          }),
            (this.virtual.cache = n);
        }

        this.virtual.update(!0), this.slideTo(i, 0);
      },
      removeSlide: function removeSlide(e) {
        if (null != e) {
          var t = this.activeIndex;
          if (Array.isArray(e))
            for (var i = e.length - 1; i >= 0; i -= 1) {
              this.virtual.slides.splice(e[i], 1),
                this.params.virtual.cache && delete this.virtual.cache[e[i]],
                e[i] < t && (t -= 1),
                (t = Math.max(t, 0));
            }
          else
            this.virtual.slides.splice(e, 1),
              this.params.virtual.cache && delete this.virtual.cache[e],
              e < t && (t -= 1),
              (t = Math.max(t, 0));
          this.virtual.update(!0), this.slideTo(t, 0);
        }
      },
      removeAllSlides: function removeAllSlides() {
        (this.virtual.slides = []),
          this.params.virtual.cache && (this.virtual.cache = {}),
          this.virtual.update(!0),
          this.slideTo(0, 0);
      },
    },
    q = {
      name: "virtual",
      params: {
        virtual: {
          enabled: !1,
          slides: [],
          cache: !0,
          renderSlide: null,
          renderExternal: null,
          renderExternalUpdate: !0,
          addSlidesBefore: 0,
          addSlidesAfter: 0,
        },
      },
      create: function create() {
        M(this, {
          virtual: t(
            t({}, R),
            {},
            {
              slides: this.params.virtual.slides,
              cache: {},
            }
          ),
        });
      },
      on: {
        beforeInit: function beforeInit(e) {
          if (e.params.virtual.enabled) {
            e.classNames.push(e.params.containerModifierClass + "virtual");
            var t = {
              watchSlidesProgress: !0,
            };
            S(e.params, t),
              S(e.originalParams, t),
              e.params.initialSlide || e.virtual.update();
          }
        },
        setTranslate: function setTranslate(e) {
          e.params.virtual.enabled && e.virtual.update();
        },
      },
    },
    j = {
      handle: function handle(e) {
        var t = l(),
          i = r(),
          s = this.rtlTranslate,
          a = e;
        a.originalEvent && (a = a.originalEvent);
        var n = a.keyCode || a.charCode,
          o = this.params.keyboard.pageUpDown,
          d = o && 33 === n,
          h = o && 34 === n,
          p = 37 === n,
          u = 39 === n,
          c = 38 === n,
          v = 40 === n;
        if (
          !this.allowSlideNext &&
          ((this.isHorizontal() && u) || (this.isVertical() && v) || h)
        )
          return !1;
        if (
          !this.allowSlidePrev &&
          ((this.isHorizontal() && p) || (this.isVertical() && c) || d)
        )
          return !1;

        if (
          !(
            a.shiftKey ||
            a.altKey ||
            a.ctrlKey ||
            a.metaKey ||
            (i.activeElement &&
              i.activeElement.nodeName &&
              ("input" === i.activeElement.nodeName.toLowerCase() ||
                "textarea" === i.activeElement.nodeName.toLowerCase()))
          )
        ) {
          if (
            this.params.keyboard.onlyInViewport &&
            (d || h || p || u || c || v)
          ) {
            var f = !1;
            if (
              this.$el.parents("." + this.params.slideClass).length > 0 &&
              0 === this.$el.parents("." + this.params.slideActiveClass).length
            )
              return;
            var m = t.innerWidth,
              g = t.innerHeight,
              w = this.$el.offset();
            s && (w.left -= this.$el[0].scrollLeft);

            for (
              var b = [
                  [w.left, w.top],
                  [w.left + this.width, w.top],
                  [w.left, w.top + this.height],
                  [w.left + this.width, w.top + this.height],
                ],
                y = 0;
              y < b.length;
              y += 1
            ) {
              var E = b[y];
              E[0] >= 0 && E[0] <= m && E[1] >= 0 && E[1] <= g && (f = !0);
            }

            if (!f) return;
          }

          this.isHorizontal()
            ? ((d || h || p || u) &&
                (a.preventDefault ? a.preventDefault() : (a.returnValue = !1)),
              (((h || u) && !s) || ((d || p) && s)) && this.slideNext(),
              (((d || p) && !s) || ((h || u) && s)) && this.slidePrev())
            : ((d || h || c || v) &&
                (a.preventDefault ? a.preventDefault() : (a.returnValue = !1)),
              (h || v) && this.slideNext(),
              (d || c) && this.slidePrev()),
            this.emit("keyPress", n);
        }
      },
      enable: function enable() {
        var e = r();
        this.keyboard.enabled ||
          (m(e).on("keydown", this.keyboard.handle),
          (this.keyboard.enabled = !0));
      },
      disable: function disable() {
        var e = r();
        this.keyboard.enabled &&
          (m(e).off("keydown", this.keyboard.handle),
          (this.keyboard.enabled = !1));
      },
    },
    _ = {
      name: "keyboard",
      params: {
        keyboard: {
          enabled: !1,
          onlyInViewport: !0,
          pageUpDown: !0,
        },
      },
      create: function create() {
        M(this, {
          keyboard: t(
            {
              enabled: !1,
            },
            j
          ),
        });
      },
      on: {
        init: function init(e) {
          e.params.keyboard.enabled && e.keyboard.enable();
        },
        destroy: function destroy(e) {
          e.keyboard.enabled && e.keyboard.disable();
        },
      },
    };
  var U = {
      lastScrollTime: x(),
      lastEventBeforeSnap: void 0,
      recentWheelEvents: [],
      event: function event() {
        return l().navigator.userAgent.indexOf("firefox") > -1
          ? "DOMMouseScroll"
          : (function () {
              var e = r(),
                t = "onwheel" in e;

              if (!t) {
                var i = e.createElement("div");
                i.setAttribute("onwheel", "return;"),
                  (t = "function" == typeof i.onwheel);
              }

              return (
                !t &&
                  e.implementation &&
                  e.implementation.hasFeature &&
                  !0 !== e.implementation.hasFeature("", "") &&
                  (t = e.implementation.hasFeature("Events.wheel", "3.0")),
                t
              );
            })()
          ? "wheel"
          : "mousewheel";
      },
      normalize: function normalize(e) {
        var t = 0,
          i = 0,
          s = 0,
          a = 0;
        return (
          "detail" in e && (i = e.detail),
          "wheelDelta" in e && (i = -e.wheelDelta / 120),
          "wheelDeltaY" in e && (i = -e.wheelDeltaY / 120),
          "wheelDeltaX" in e && (t = -e.wheelDeltaX / 120),
          "axis" in e && e.axis === e.HORIZONTAL_AXIS && ((t = i), (i = 0)),
          (s = 10 * t),
          (a = 10 * i),
          "deltaY" in e && (a = e.deltaY),
          "deltaX" in e && (s = e.deltaX),
          e.shiftKey && !s && ((s = a), (a = 0)),
          (s || a) &&
            e.deltaMode &&
            (1 === e.deltaMode
              ? ((s *= 40), (a *= 40))
              : ((s *= 800), (a *= 800))),
          s && !t && (t = s < 1 ? -1 : 1),
          a && !i && (i = a < 1 ? -1 : 1),
          {
            spinX: t,
            spinY: i,
            pixelX: s,
            pixelY: a,
          }
        );
      },
      handleMouseEnter: function handleMouseEnter() {
        this.mouseEntered = !0;
      },
      handleMouseLeave: function handleMouseLeave() {
        this.mouseEntered = !1;
      },
      handle: function handle(e) {
        var t = e,
          i = this,
          s = i.params.mousewheel;
        i.params.cssMode && t.preventDefault();
        var a = i.$el;
        if (
          ("container" !== i.params.mousewheel.eventsTarget &&
            (a = m(i.params.mousewheel.eventsTarget)),
          !i.mouseEntered && !a[0].contains(t.target) && !s.releaseOnEdges)
        )
          return !0;
        t.originalEvent && (t = t.originalEvent);
        var r = 0,
          n = i.rtlTranslate ? -1 : 1,
          l = U.normalize(t);
        if (s.forceToAxis) {
          if (i.isHorizontal()) {
            if (!(Math.abs(l.pixelX) > Math.abs(l.pixelY))) return !0;
            r = -l.pixelX * n;
          } else {
            if (!(Math.abs(l.pixelY) > Math.abs(l.pixelX))) return !0;
            r = -l.pixelY;
          }
        } else
          r =
            Math.abs(l.pixelX) > Math.abs(l.pixelY) ? -l.pixelX * n : -l.pixelY;
        if (0 === r) return !0;

        if ((s.invert && (r = -r), i.params.freeMode)) {
          var o = {
              time: x(),
              delta: Math.abs(r),
              direction: Math.sign(r),
            },
            d = i.mousewheel.lastEventBeforeSnap,
            h =
              d &&
              o.time < d.time + 500 &&
              o.delta <= d.delta &&
              o.direction === d.direction;

          if (!h) {
            (i.mousewheel.lastEventBeforeSnap = void 0),
              i.params.loop && i.loopFix();
            var p = i.getTranslate() + r * s.sensitivity,
              u = i.isBeginning,
              c = i.isEnd;

            if (
              (p >= i.minTranslate() && (p = i.minTranslate()),
              p <= i.maxTranslate() && (p = i.maxTranslate()),
              i.setTransition(0),
              i.setTranslate(p),
              i.updateProgress(),
              i.updateActiveIndex(),
              i.updateSlidesClasses(),
              ((!u && i.isBeginning) || (!c && i.isEnd)) &&
                i.updateSlidesClasses(),
              i.params.freeModeSticky)
            ) {
              clearTimeout(i.mousewheel.timeout),
                (i.mousewheel.timeout = void 0);
              var v = i.mousewheel.recentWheelEvents;
              v.length >= 15 && v.shift();
              var f = v.length ? v[v.length - 1] : void 0,
                g = v[0];
              if (
                (v.push(o),
                f && (o.delta > f.delta || o.direction !== f.direction))
              )
                v.splice(0);
              else if (
                v.length >= 15 &&
                o.time - g.time < 500 &&
                g.delta - o.delta >= 1 &&
                o.delta <= 6
              ) {
                var w = r > 0 ? 0.8 : 0.2;
                (i.mousewheel.lastEventBeforeSnap = o),
                  v.splice(0),
                  (i.mousewheel.timeout = E(function () {
                    i.slideToClosest(i.params.speed, !0, void 0, w);
                  }, 0));
              }
              i.mousewheel.timeout ||
                (i.mousewheel.timeout = E(function () {
                  (i.mousewheel.lastEventBeforeSnap = o),
                    v.splice(0),
                    i.slideToClosest(i.params.speed, !0, void 0, 0.5);
                }, 500));
            }

            if (
              (h || i.emit("scroll", t),
              i.params.autoplay &&
                i.params.autoplayDisableOnInteraction &&
                i.autoplay.stop(),
              p === i.minTranslate() || p === i.maxTranslate())
            )
              return !0;
          }
        } else {
          var b = {
              time: x(),
              delta: Math.abs(r),
              direction: Math.sign(r),
              raw: e,
            },
            y = i.mousewheel.recentWheelEvents;
          y.length >= 2 && y.shift();
          var T = y.length ? y[y.length - 1] : void 0;
          if (
            (y.push(b),
            T
              ? (b.direction !== T.direction ||
                  b.delta > T.delta ||
                  b.time > T.time + 150) &&
                i.mousewheel.animateSlider(b)
              : i.mousewheel.animateSlider(b),
            i.mousewheel.releaseScroll(b))
          )
            return !0;
        }

        return t.preventDefault ? t.preventDefault() : (t.returnValue = !1), !1;
      },
      animateSlider: function animateSlider(e) {
        var t = l();
        return (
          !(
            this.params.mousewheel.thresholdDelta &&
            e.delta < this.params.mousewheel.thresholdDelta
          ) &&
          !(
            this.params.mousewheel.thresholdTime &&
            x() - this.mousewheel.lastScrollTime <
              this.params.mousewheel.thresholdTime
          ) &&
          ((e.delta >= 6 && x() - this.mousewheel.lastScrollTime < 60) ||
            (e.direction < 0
              ? (this.isEnd && !this.params.loop) ||
                this.animating ||
                (this.slideNext(), this.emit("scroll", e.raw))
              : (this.isBeginning && !this.params.loop) ||
                this.animating ||
                (this.slidePrev(), this.emit("scroll", e.raw)),
            (this.mousewheel.lastScrollTime = new t.Date().getTime()),
            !1))
        );
      },
      releaseScroll: function releaseScroll(e) {
        var t = this.params.mousewheel;

        if (e.direction < 0) {
          if (this.isEnd && !this.params.loop && t.releaseOnEdges) return !0;
        } else if (this.isBeginning && !this.params.loop && t.releaseOnEdges)
          return !0;

        return !1;
      },
      enable: function enable() {
        var e = U.event();
        if (this.params.cssMode)
          return (
            this.wrapperEl.removeEventListener(e, this.mousewheel.handle), !0
          );
        if (!e) return !1;
        if (this.mousewheel.enabled) return !1;
        var t = this.$el;
        return (
          "container" !== this.params.mousewheel.eventsTarget &&
            (t = m(this.params.mousewheel.eventsTarget)),
          t.on("mouseenter", this.mousewheel.handleMouseEnter),
          t.on("mouseleave", this.mousewheel.handleMouseLeave),
          t.on(e, this.mousewheel.handle),
          (this.mousewheel.enabled = !0),
          !0
        );
      },
      disable: function disable() {
        var e = U.event();
        if (this.params.cssMode)
          return this.wrapperEl.addEventListener(e, this.mousewheel.handle), !0;
        if (!e) return !1;
        if (!this.mousewheel.enabled) return !1;
        var t = this.$el;
        return (
          "container" !== this.params.mousewheel.eventsTarget &&
            (t = m(this.params.mousewheel.eventsTarget)),
          t.off(e, this.mousewheel.handle),
          (this.mousewheel.enabled = !1),
          !0
        );
      },
    },
    K = {
      update: function update() {
        var e = this.params.navigation;

        if (!this.params.loop) {
          var t = this.navigation,
            i = t.$nextEl,
            s = t.$prevEl;
          s &&
            s.length > 0 &&
            (this.isBeginning
              ? s.addClass(e.disabledClass)
              : s.removeClass(e.disabledClass),
            s[
              this.params.watchOverflow && this.isLocked
                ? "addClass"
                : "removeClass"
            ](e.lockClass)),
            i &&
              i.length > 0 &&
              (this.isEnd
                ? i.addClass(e.disabledClass)
                : i.removeClass(e.disabledClass),
              i[
                this.params.watchOverflow && this.isLocked
                  ? "addClass"
                  : "removeClass"
              ](e.lockClass));
        }
      },
      onPrevClick: function onPrevClick(e) {
        e.preventDefault(),
          (this.isBeginning && !this.params.loop) || this.slidePrev();
      },
      onNextClick: function onNextClick(e) {
        e.preventDefault(),
          (this.isEnd && !this.params.loop) || this.slideNext();
      },
      init: function init() {
        var e,
          t,
          i = this.params.navigation;
        (i.nextEl || i.prevEl) &&
          (i.nextEl &&
            ((e = m(i.nextEl)),
            this.params.uniqueNavElements &&
              "string" == typeof i.nextEl &&
              e.length > 1 &&
              1 === this.$el.find(i.nextEl).length &&
              (e = this.$el.find(i.nextEl))),
          i.prevEl &&
            ((t = m(i.prevEl)),
            this.params.uniqueNavElements &&
              "string" == typeof i.prevEl &&
              t.length > 1 &&
              1 === this.$el.find(i.prevEl).length &&
              (t = this.$el.find(i.prevEl))),
          e && e.length > 0 && e.on("click", this.navigation.onNextClick),
          t && t.length > 0 && t.on("click", this.navigation.onPrevClick),
          S(this.navigation, {
            $nextEl: e,
            nextEl: e && e[0],
            $prevEl: t,
            prevEl: t && t[0],
          }));
      },
      destroy: function destroy() {
        var e = this.navigation,
          t = e.$nextEl,
          i = e.$prevEl;
        t &&
          t.length &&
          (t.off("click", this.navigation.onNextClick),
          t.removeClass(this.params.navigation.disabledClass)),
          i &&
            i.length &&
            (i.off("click", this.navigation.onPrevClick),
            i.removeClass(this.params.navigation.disabledClass));
      },
    },
    Z = {
      update: function update() {
        var e = this.rtl,
          t = this.params.pagination;

        if (
          t.el &&
          this.pagination.el &&
          this.pagination.$el &&
          0 !== this.pagination.$el.length
        ) {
          var i,
            s =
              this.virtual && this.params.virtual.enabled
                ? this.virtual.slides.length
                : this.slides.length,
            a = this.pagination.$el,
            r = this.params.loop
              ? Math.ceil(
                  (s - 2 * this.loopedSlides) / this.params.slidesPerGroup
                )
              : this.snapGrid.length;

          if (
            (this.params.loop
              ? ((i = Math.ceil(
                  (this.activeIndex - this.loopedSlides) /
                    this.params.slidesPerGroup
                )) >
                  s - 1 - 2 * this.loopedSlides &&
                  (i -= s - 2 * this.loopedSlides),
                i > r - 1 && (i -= r),
                i < 0 &&
                  "bullets" !== this.params.paginationType &&
                  (i = r + i))
              : (i =
                  void 0 !== this.snapIndex
                    ? this.snapIndex
                    : this.activeIndex || 0),
            "bullets" === t.type &&
              this.pagination.bullets &&
              this.pagination.bullets.length > 0)
          ) {
            var n,
              l,
              o,
              d = this.pagination.bullets;
            if (
              (t.dynamicBullets &&
                ((this.pagination.bulletSize = d
                  .eq(0)
                  [this.isHorizontal() ? "outerWidth" : "outerHeight"](!0)),
                a.css(
                  this.isHorizontal() ? "width" : "height",
                  this.pagination.bulletSize * (t.dynamicMainBullets + 4) + "px"
                ),
                t.dynamicMainBullets > 1 &&
                  void 0 !== this.previousIndex &&
                  ((this.pagination.dynamicBulletIndex +=
                    i - this.previousIndex),
                  this.pagination.dynamicBulletIndex > t.dynamicMainBullets - 1
                    ? (this.pagination.dynamicBulletIndex =
                        t.dynamicMainBullets - 1)
                    : this.pagination.dynamicBulletIndex < 0 &&
                      (this.pagination.dynamicBulletIndex = 0)),
                (n = i - this.pagination.dynamicBulletIndex),
                (o =
                  ((l = n + (Math.min(d.length, t.dynamicMainBullets) - 1)) +
                    n) /
                  2)),
              d.removeClass(
                t.bulletActiveClass +
                  " " +
                  t.bulletActiveClass +
                  "-next " +
                  t.bulletActiveClass +
                  "-next-next " +
                  t.bulletActiveClass +
                  "-prev " +
                  t.bulletActiveClass +
                  "-prev-prev " +
                  t.bulletActiveClass +
                  "-main"
              ),
              a.length > 1)
            )
              d.each(function (e) {
                var s = m(e),
                  a = s.index();
                a === i && s.addClass(t.bulletActiveClass),
                  t.dynamicBullets &&
                    (a >= n &&
                      a <= l &&
                      s.addClass(t.bulletActiveClass + "-main"),
                    a === n &&
                      s
                        .prev()
                        .addClass(t.bulletActiveClass + "-prev")
                        .prev()
                        .addClass(t.bulletActiveClass + "-prev-prev"),
                    a === l &&
                      s
                        .next()
                        .addClass(t.bulletActiveClass + "-next")
                        .next()
                        .addClass(t.bulletActiveClass + "-next-next"));
              });
            else {
              var h = d.eq(i),
                p = h.index();

              if ((h.addClass(t.bulletActiveClass), t.dynamicBullets)) {
                for (var u = d.eq(n), c = d.eq(l), v = n; v <= l; v += 1) {
                  d.eq(v).addClass(t.bulletActiveClass + "-main");
                }

                if (this.params.loop) {
                  if (p >= d.length - t.dynamicMainBullets) {
                    for (var f = t.dynamicMainBullets; f >= 0; f -= 1) {
                      d.eq(d.length - f).addClass(
                        t.bulletActiveClass + "-main"
                      );
                    }

                    d.eq(d.length - t.dynamicMainBullets - 1).addClass(
                      t.bulletActiveClass + "-prev"
                    );
                  } else
                    u
                      .prev()
                      .addClass(t.bulletActiveClass + "-prev")
                      .prev()
                      .addClass(t.bulletActiveClass + "-prev-prev"),
                      c
                        .next()
                        .addClass(t.bulletActiveClass + "-next")
                        .next()
                        .addClass(t.bulletActiveClass + "-next-next");
                } else
                  u
                    .prev()
                    .addClass(t.bulletActiveClass + "-prev")
                    .prev()
                    .addClass(t.bulletActiveClass + "-prev-prev"),
                    c
                      .next()
                      .addClass(t.bulletActiveClass + "-next")
                      .next()
                      .addClass(t.bulletActiveClass + "-next-next");
              }
            }

            if (t.dynamicBullets) {
              var g = Math.min(d.length, t.dynamicMainBullets + 4),
                w =
                  (this.pagination.bulletSize * g -
                    this.pagination.bulletSize) /
                    2 -
                  o * this.pagination.bulletSize,
                b = e ? "right" : "left";
              d.css(this.isHorizontal() ? b : "top", w + "px");
            }
          }

          if (
            ("fraction" === t.type &&
              (a
                .find("." + t.currentClass)
                .text(t.formatFractionCurrent(i + 1)),
              a.find("." + t.totalClass).text(t.formatFractionTotal(r))),
            "progressbar" === t.type)
          ) {
            var y;
            y = t.progressbarOpposite
              ? this.isHorizontal()
                ? "vertical"
                : "horizontal"
              : this.isHorizontal()
              ? "horizontal"
              : "vertical";
            var E = (i + 1) / r,
              x = 1,
              T = 1;
            "horizontal" === y ? (x = E) : (T = E),
              a
                .find("." + t.progressbarFillClass)
                .transform(
                  "translate3d(0,0,0) scaleX(" + x + ") scaleY(" + T + ")"
                )
                .transition(this.params.speed);
          }

          "custom" === t.type && t.renderCustom
            ? (a.html(t.renderCustom(this, i + 1, r)),
              this.emit("paginationRender", a[0]))
            : this.emit("paginationUpdate", a[0]),
            a[
              this.params.watchOverflow && this.isLocked
                ? "addClass"
                : "removeClass"
            ](t.lockClass);
        }
      },
      render: function render() {
        var e = this.params.pagination;

        if (
          e.el &&
          this.pagination.el &&
          this.pagination.$el &&
          0 !== this.pagination.$el.length
        ) {
          var t =
              this.virtual && this.params.virtual.enabled
                ? this.virtual.slides.length
                : this.slides.length,
            i = this.pagination.$el,
            s = "";

          if ("bullets" === e.type) {
            for (
              var a = this.params.loop
                  ? Math.ceil(
                      (t - 2 * this.loopedSlides) / this.params.slidesPerGroup
                    )
                  : this.snapGrid.length,
                r = 0;
              r < a;
              r += 1
            ) {
              e.renderBullet
                ? (s += e.renderBullet.call(this, r, e.bulletClass))
                : (s +=
                    "<" +
                    e.bulletElement +
                    ' class="' +
                    e.bulletClass +
                    '"></' +
                    e.bulletElement +
                    ">");
            }

            i.html(s), (this.pagination.bullets = i.find("." + e.bulletClass));
          }

          "fraction" === e.type &&
            ((s = e.renderFraction
              ? e.renderFraction.call(this, e.currentClass, e.totalClass)
              : '<span class="' +
                e.currentClass +
                '"></span> / <span class="' +
                e.totalClass +
                '"></span>'),
            i.html(s)),
            "progressbar" === e.type &&
              ((s = e.renderProgressbar
                ? e.renderProgressbar.call(this, e.progressbarFillClass)
                : '<span class="' + e.progressbarFillClass + '"></span>'),
              i.html(s)),
            "custom" !== e.type &&
              this.emit("paginationRender", this.pagination.$el[0]);
        }
      },
      init: function init() {
        var e = this,
          t = e.params.pagination;

        if (t.el) {
          var i = m(t.el);
          0 !== i.length &&
            (e.params.uniqueNavElements &&
              "string" == typeof t.el &&
              i.length > 1 &&
              (i = e.$el.find(t.el)),
            "bullets" === t.type && t.clickable && i.addClass(t.clickableClass),
            i.addClass(t.modifierClass + t.type),
            "bullets" === t.type &&
              t.dynamicBullets &&
              (i.addClass("" + t.modifierClass + t.type + "-dynamic"),
              (e.pagination.dynamicBulletIndex = 0),
              t.dynamicMainBullets < 1 && (t.dynamicMainBullets = 1)),
            "progressbar" === t.type &&
              t.progressbarOpposite &&
              i.addClass(t.progressbarOppositeClass),
            t.clickable &&
              i.on("click", "." + t.bulletClass, function (t) {
                t.preventDefault();
                var i = m(this).index() * e.params.slidesPerGroup;
                e.params.loop && (i += e.loopedSlides), e.slideTo(i);
              }),
            S(e.pagination, {
              $el: i,
              el: i[0],
            }));
        }
      },
      destroy: function destroy() {
        var e = this.params.pagination;

        if (
          e.el &&
          this.pagination.el &&
          this.pagination.$el &&
          0 !== this.pagination.$el.length
        ) {
          var t = this.pagination.$el;
          t.removeClass(e.hiddenClass),
            t.removeClass(e.modifierClass + e.type),
            this.pagination.bullets &&
              this.pagination.bullets.removeClass(e.bulletActiveClass),
            e.clickable && t.off("click", "." + e.bulletClass);
        }
      },
    },
    J = {
      setTranslate: function setTranslate() {
        if (this.params.scrollbar.el && this.scrollbar.el) {
          var e = this.scrollbar,
            t = this.rtlTranslate,
            i = this.progress,
            s = e.dragSize,
            a = e.trackSize,
            r = e.$dragEl,
            n = e.$el,
            l = this.params.scrollbar,
            o = s,
            d = (a - s) * i;
          t
            ? (d = -d) > 0
              ? ((o = s - d), (d = 0))
              : -d + s > a && (o = a + d)
            : d < 0
            ? ((o = s + d), (d = 0))
            : d + s > a && (o = a - d),
            this.isHorizontal()
              ? (r.transform("translate3d(" + d + "px, 0, 0)"),
                (r[0].style.width = o + "px"))
              : (r.transform("translate3d(0px, " + d + "px, 0)"),
                (r[0].style.height = o + "px")),
            l.hide &&
              (clearTimeout(this.scrollbar.timeout),
              (n[0].style.opacity = 1),
              (this.scrollbar.timeout = setTimeout(function () {
                (n[0].style.opacity = 0), n.transition(400);
              }, 1e3)));
        }
      },
      setTransition: function setTransition(e) {
        this.params.scrollbar.el &&
          this.scrollbar.el &&
          this.scrollbar.$dragEl.transition(e);
      },
      updateSize: function updateSize() {
        if (this.params.scrollbar.el && this.scrollbar.el) {
          var e = this.scrollbar,
            t = e.$dragEl,
            i = e.$el;
          (t[0].style.width = ""), (t[0].style.height = "");
          var s,
            a = this.isHorizontal() ? i[0].offsetWidth : i[0].offsetHeight,
            r = this.size / this.virtualSize,
            n = r * (a / this.size);
          (s =
            "auto" === this.params.scrollbar.dragSize
              ? a * r
              : parseInt(this.params.scrollbar.dragSize, 10)),
            this.isHorizontal()
              ? (t[0].style.width = s + "px")
              : (t[0].style.height = s + "px"),
            (i[0].style.display = r >= 1 ? "none" : ""),
            this.params.scrollbar.hide && (i[0].style.opacity = 0),
            S(e, {
              trackSize: a,
              divider: r,
              moveDivider: n,
              dragSize: s,
            }),
            e.$el[
              this.params.watchOverflow && this.isLocked
                ? "addClass"
                : "removeClass"
            ](this.params.scrollbar.lockClass);
        }
      },
      getPointerPosition: function getPointerPosition(e) {
        return this.isHorizontal()
          ? "touchstart" === e.type || "touchmove" === e.type
            ? e.targetTouches[0].clientX
            : e.clientX
          : "touchstart" === e.type || "touchmove" === e.type
          ? e.targetTouches[0].clientY
          : e.clientY;
      },
      setDragPosition: function setDragPosition(e) {
        var t,
          i = this.scrollbar,
          s = this.rtlTranslate,
          a = i.$el,
          r = i.dragSize,
          n = i.trackSize,
          l = i.dragStartPos;
        (t =
          (i.getPointerPosition(e) -
            a.offset()[this.isHorizontal() ? "left" : "top"] -
            (null !== l ? l : r / 2)) /
          (n - r)),
          (t = Math.max(Math.min(t, 1), 0)),
          s && (t = 1 - t);
        var o =
          this.minTranslate() + (this.maxTranslate() - this.minTranslate()) * t;
        this.updateProgress(o),
          this.setTranslate(o),
          this.updateActiveIndex(),
          this.updateSlidesClasses();
      },
      onDragStart: function onDragStart(e) {
        var t = this.params.scrollbar,
          i = this.scrollbar,
          s = this.$wrapperEl,
          a = i.$el,
          r = i.$dragEl;
        (this.scrollbar.isTouched = !0),
          (this.scrollbar.dragStartPos =
            e.target === r[0] || e.target === r
              ? i.getPointerPosition(e) -
                e.target.getBoundingClientRect()[
                  this.isHorizontal() ? "left" : "top"
                ]
              : null),
          e.preventDefault(),
          e.stopPropagation(),
          s.transition(100),
          r.transition(100),
          i.setDragPosition(e),
          clearTimeout(this.scrollbar.dragTimeout),
          a.transition(0),
          t.hide && a.css("opacity", 1),
          this.params.cssMode &&
            this.$wrapperEl.css("scroll-snap-type", "none"),
          this.emit("scrollbarDragStart", e);
      },
      onDragMove: function onDragMove(e) {
        var t = this.scrollbar,
          i = this.$wrapperEl,
          s = t.$el,
          a = t.$dragEl;
        this.scrollbar.isTouched &&
          (e.preventDefault ? e.preventDefault() : (e.returnValue = !1),
          t.setDragPosition(e),
          i.transition(0),
          s.transition(0),
          a.transition(0),
          this.emit("scrollbarDragMove", e));
      },
      onDragEnd: function onDragEnd(e) {
        var t = this.params.scrollbar,
          i = this.scrollbar,
          s = this.$wrapperEl,
          a = i.$el;
        this.scrollbar.isTouched &&
          ((this.scrollbar.isTouched = !1),
          this.params.cssMode &&
            (this.$wrapperEl.css("scroll-snap-type", ""), s.transition("")),
          t.hide &&
            (clearTimeout(this.scrollbar.dragTimeout),
            (this.scrollbar.dragTimeout = E(function () {
              a.css("opacity", 0), a.transition(400);
            }, 1e3))),
          this.emit("scrollbarDragEnd", e),
          t.snapOnRelease && this.slideToClosest());
      },
      enableDraggable: function enableDraggable() {
        if (this.params.scrollbar.el) {
          var e = r(),
            t = this.scrollbar,
            i = this.touchEventsTouch,
            s = this.touchEventsDesktop,
            a = this.params,
            n = this.support,
            l = t.$el[0],
            o = !(!n.passiveListener || !a.passiveListeners) && {
              passive: !1,
              capture: !1,
            },
            d = !(!n.passiveListener || !a.passiveListeners) && {
              passive: !0,
              capture: !1,
            };
          n.touch
            ? (l.addEventListener(i.start, this.scrollbar.onDragStart, o),
              l.addEventListener(i.move, this.scrollbar.onDragMove, o),
              l.addEventListener(i.end, this.scrollbar.onDragEnd, d))
            : (l.addEventListener(s.start, this.scrollbar.onDragStart, o),
              e.addEventListener(s.move, this.scrollbar.onDragMove, o),
              e.addEventListener(s.end, this.scrollbar.onDragEnd, d));
        }
      },
      disableDraggable: function disableDraggable() {
        if (this.params.scrollbar.el) {
          var e = r(),
            t = this.scrollbar,
            i = this.touchEventsTouch,
            s = this.touchEventsDesktop,
            a = this.params,
            n = this.support,
            l = t.$el[0],
            o = !(!n.passiveListener || !a.passiveListeners) && {
              passive: !1,
              capture: !1,
            },
            d = !(!n.passiveListener || !a.passiveListeners) && {
              passive: !0,
              capture: !1,
            };
          n.touch
            ? (l.removeEventListener(i.start, this.scrollbar.onDragStart, o),
              l.removeEventListener(i.move, this.scrollbar.onDragMove, o),
              l.removeEventListener(i.end, this.scrollbar.onDragEnd, d))
            : (l.removeEventListener(s.start, this.scrollbar.onDragStart, o),
              e.removeEventListener(s.move, this.scrollbar.onDragMove, o),
              e.removeEventListener(s.end, this.scrollbar.onDragEnd, d));
        }
      },
      init: function init() {
        if (this.params.scrollbar.el) {
          var e = this.scrollbar,
            t = this.$el,
            i = this.params.scrollbar,
            s = m(i.el);
          this.params.uniqueNavElements &&
            "string" == typeof i.el &&
            s.length > 1 &&
            1 === t.find(i.el).length &&
            (s = t.find(i.el));
          var a = s.find("." + this.params.scrollbar.dragClass);
          0 === a.length &&
            ((a = m(
              '<div class="' + this.params.scrollbar.dragClass + '"></div>'
            )),
            s.append(a)),
            S(e, {
              $el: s,
              el: s[0],
              $dragEl: a,
              dragEl: a[0],
            }),
            i.draggable && e.enableDraggable();
        }
      },
      destroy: function destroy() {
        this.scrollbar.disableDraggable();
      },
    },
    Q = {
      setTransform: function setTransform(e, t) {
        var i = this.rtl,
          s = m(e),
          a = i ? -1 : 1,
          r = s.attr("data-swiper-parallax") || "0",
          n = s.attr("data-swiper-parallax-x"),
          l = s.attr("data-swiper-parallax-y"),
          o = s.attr("data-swiper-parallax-scale"),
          d = s.attr("data-swiper-parallax-opacity");

        if (
          (n || l
            ? ((n = n || "0"), (l = l || "0"))
            : this.isHorizontal()
            ? ((n = r), (l = "0"))
            : ((l = r), (n = "0")),
          (n =
            n.indexOf("%") >= 0
              ? parseInt(n, 10) * t * a + "%"
              : n * t * a + "px"),
          (l = l.indexOf("%") >= 0 ? parseInt(l, 10) * t + "%" : l * t + "px"),
          null != d)
        ) {
          var h = d - (d - 1) * (1 - Math.abs(t));
          s[0].style.opacity = h;
        }

        if (null == o) s.transform("translate3d(" + n + ", " + l + ", 0px)");
        else {
          var p = o - (o - 1) * (1 - Math.abs(t));
          s.transform(
            "translate3d(" + n + ", " + l + ", 0px) scale(" + p + ")"
          );
        }
      },
      setTranslate: function setTranslate() {
        var e = this,
          t = e.$el,
          i = e.slides,
          s = e.progress,
          a = e.snapGrid;
        t
          .children(
            "[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]"
          )
          .each(function (t) {
            e.parallax.setTransform(t, s);
          }),
          i.each(function (t, i) {
            var r = t.progress;
            e.params.slidesPerGroup > 1 &&
              "auto" !== e.params.slidesPerView &&
              (r += Math.ceil(i / 2) - s * (a.length - 1)),
              (r = Math.min(Math.max(r, -1), 1)),
              m(t)
                .find(
                  "[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]"
                )
                .each(function (t) {
                  e.parallax.setTransform(t, r);
                });
          });
      },
      setTransition: function setTransition(e) {
        void 0 === e && (e = this.params.speed);
        this.$el
          .find(
            "[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]"
          )
          .each(function (t) {
            var i = m(t),
              s = parseInt(i.attr("data-swiper-parallax-duration"), 10) || e;
            0 === e && (s = 0), i.transition(s);
          });
      },
    },
    ee = {
      getDistanceBetweenTouches: function getDistanceBetweenTouches(e) {
        if (e.targetTouches.length < 2) return 1;
        var t = e.targetTouches[0].pageX,
          i = e.targetTouches[0].pageY,
          s = e.targetTouches[1].pageX,
          a = e.targetTouches[1].pageY;
        return Math.sqrt(Math.pow(s - t, 2) + Math.pow(a - i, 2));
      },
      onGestureStart: function onGestureStart(e) {
        var t = this.support,
          i = this.params.zoom,
          s = this.zoom,
          a = s.gesture;

        if (
          ((s.fakeGestureTouched = !1), (s.fakeGestureMoved = !1), !t.gestures)
        ) {
          if (
            "touchstart" !== e.type ||
            ("touchstart" === e.type && e.targetTouches.length < 2)
          )
            return;
          (s.fakeGestureTouched = !0),
            (a.scaleStart = ee.getDistanceBetweenTouches(e));
        }

        (a.$slideEl && a.$slideEl.length) ||
        ((a.$slideEl = m(e.target).closest("." + this.params.slideClass)),
        0 === a.$slideEl.length &&
          (a.$slideEl = this.slides.eq(this.activeIndex)),
        (a.$imageEl = a.$slideEl.find(
          "img, svg, canvas, picture, .swiper-zoom-target"
        )),
        (a.$imageWrapEl = a.$imageEl.parent("." + i.containerClass)),
        (a.maxRatio = a.$imageWrapEl.attr("data-swiper-zoom") || i.maxRatio),
        0 !== a.$imageWrapEl.length)
          ? (a.$imageEl && a.$imageEl.transition(0), (this.zoom.isScaling = !0))
          : (a.$imageEl = void 0);
      },
      onGestureChange: function onGestureChange(e) {
        var t = this.support,
          i = this.params.zoom,
          s = this.zoom,
          a = s.gesture;

        if (!t.gestures) {
          if (
            "touchmove" !== e.type ||
            ("touchmove" === e.type && e.targetTouches.length < 2)
          )
            return;
          (s.fakeGestureMoved = !0),
            (a.scaleMove = ee.getDistanceBetweenTouches(e));
        }

        a.$imageEl && 0 !== a.$imageEl.length
          ? (t.gestures
              ? (s.scale = e.scale * s.currentScale)
              : (s.scale = (a.scaleMove / a.scaleStart) * s.currentScale),
            s.scale > a.maxRatio &&
              (s.scale =
                a.maxRatio - 1 + Math.pow(s.scale - a.maxRatio + 1, 0.5)),
            s.scale < i.minRatio &&
              (s.scale =
                i.minRatio + 1 - Math.pow(i.minRatio - s.scale + 1, 0.5)),
            a.$imageEl.transform("translate3d(0,0,0) scale(" + s.scale + ")"))
          : "gesturechange" === e.type && s.onGestureStart(e);
      },
      onGestureEnd: function onGestureEnd(e) {
        var t = this.device,
          i = this.support,
          s = this.params.zoom,
          a = this.zoom,
          r = a.gesture;

        if (!i.gestures) {
          if (!a.fakeGestureTouched || !a.fakeGestureMoved) return;
          if (
            "touchend" !== e.type ||
            ("touchend" === e.type && e.changedTouches.length < 2 && !t.android)
          )
            return;
          (a.fakeGestureTouched = !1), (a.fakeGestureMoved = !1);
        }

        r.$imageEl &&
          0 !== r.$imageEl.length &&
          ((a.scale = Math.max(Math.min(a.scale, r.maxRatio), s.minRatio)),
          r.$imageEl
            .transition(this.params.speed)
            .transform("translate3d(0,0,0) scale(" + a.scale + ")"),
          (a.currentScale = a.scale),
          (a.isScaling = !1),
          1 === a.scale && (r.$slideEl = void 0));
      },
      onTouchStart: function onTouchStart(e) {
        var t = this.device,
          i = this.zoom,
          s = i.gesture,
          a = i.image;
        s.$imageEl &&
          0 !== s.$imageEl.length &&
          (a.isTouched ||
            (t.android && e.cancelable && e.preventDefault(),
            (a.isTouched = !0),
            (a.touchesStart.x =
              "touchstart" === e.type ? e.targetTouches[0].pageX : e.pageX),
            (a.touchesStart.y =
              "touchstart" === e.type ? e.targetTouches[0].pageY : e.pageY)));
      },
      onTouchMove: function onTouchMove(e) {
        var t = this.zoom,
          i = t.gesture,
          s = t.image,
          a = t.velocity;

        if (
          i.$imageEl &&
          0 !== i.$imageEl.length &&
          ((this.allowClick = !1), s.isTouched && i.$slideEl)
        ) {
          s.isMoved ||
            ((s.width = i.$imageEl[0].offsetWidth),
            (s.height = i.$imageEl[0].offsetHeight),
            (s.startX = T(i.$imageWrapEl[0], "x") || 0),
            (s.startY = T(i.$imageWrapEl[0], "y") || 0),
            (i.slideWidth = i.$slideEl[0].offsetWidth),
            (i.slideHeight = i.$slideEl[0].offsetHeight),
            i.$imageWrapEl.transition(0),
            this.rtl && ((s.startX = -s.startX), (s.startY = -s.startY)));
          var r = s.width * t.scale,
            n = s.height * t.scale;

          if (!(r < i.slideWidth && n < i.slideHeight)) {
            if (
              ((s.minX = Math.min(i.slideWidth / 2 - r / 2, 0)),
              (s.maxX = -s.minX),
              (s.minY = Math.min(i.slideHeight / 2 - n / 2, 0)),
              (s.maxY = -s.minY),
              (s.touchesCurrent.x =
                "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX),
              (s.touchesCurrent.y =
                "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY),
              !s.isMoved && !t.isScaling)
            ) {
              if (
                this.isHorizontal() &&
                ((Math.floor(s.minX) === Math.floor(s.startX) &&
                  s.touchesCurrent.x < s.touchesStart.x) ||
                  (Math.floor(s.maxX) === Math.floor(s.startX) &&
                    s.touchesCurrent.x > s.touchesStart.x))
              )
                return void (s.isTouched = !1);
              if (
                !this.isHorizontal() &&
                ((Math.floor(s.minY) === Math.floor(s.startY) &&
                  s.touchesCurrent.y < s.touchesStart.y) ||
                  (Math.floor(s.maxY) === Math.floor(s.startY) &&
                    s.touchesCurrent.y > s.touchesStart.y))
              )
                return void (s.isTouched = !1);
            }

            e.cancelable && e.preventDefault(),
              e.stopPropagation(),
              (s.isMoved = !0),
              (s.currentX = s.touchesCurrent.x - s.touchesStart.x + s.startX),
              (s.currentY = s.touchesCurrent.y - s.touchesStart.y + s.startY),
              s.currentX < s.minX &&
                (s.currentX =
                  s.minX + 1 - Math.pow(s.minX - s.currentX + 1, 0.8)),
              s.currentX > s.maxX &&
                (s.currentX =
                  s.maxX - 1 + Math.pow(s.currentX - s.maxX + 1, 0.8)),
              s.currentY < s.minY &&
                (s.currentY =
                  s.minY + 1 - Math.pow(s.minY - s.currentY + 1, 0.8)),
              s.currentY > s.maxY &&
                (s.currentY =
                  s.maxY - 1 + Math.pow(s.currentY - s.maxY + 1, 0.8)),
              a.prevPositionX || (a.prevPositionX = s.touchesCurrent.x),
              a.prevPositionY || (a.prevPositionY = s.touchesCurrent.y),
              a.prevTime || (a.prevTime = Date.now()),
              (a.x =
                (s.touchesCurrent.x - a.prevPositionX) /
                (Date.now() - a.prevTime) /
                2),
              (a.y =
                (s.touchesCurrent.y - a.prevPositionY) /
                (Date.now() - a.prevTime) /
                2),
              Math.abs(s.touchesCurrent.x - a.prevPositionX) < 2 && (a.x = 0),
              Math.abs(s.touchesCurrent.y - a.prevPositionY) < 2 && (a.y = 0),
              (a.prevPositionX = s.touchesCurrent.x),
              (a.prevPositionY = s.touchesCurrent.y),
              (a.prevTime = Date.now()),
              i.$imageWrapEl.transform(
                "translate3d(" + s.currentX + "px, " + s.currentY + "px,0)"
              );
          }
        }
      },
      onTouchEnd: function onTouchEnd() {
        var e = this.zoom,
          t = e.gesture,
          i = e.image,
          s = e.velocity;

        if (t.$imageEl && 0 !== t.$imageEl.length) {
          if (!i.isTouched || !i.isMoved)
            return (i.isTouched = !1), void (i.isMoved = !1);
          (i.isTouched = !1), (i.isMoved = !1);
          var a = 300,
            r = 300,
            n = s.x * a,
            l = i.currentX + n,
            o = s.y * r,
            d = i.currentY + o;
          0 !== s.x && (a = Math.abs((l - i.currentX) / s.x)),
            0 !== s.y && (r = Math.abs((d - i.currentY) / s.y));
          var h = Math.max(a, r);
          (i.currentX = l), (i.currentY = d);
          var p = i.width * e.scale,
            u = i.height * e.scale;
          (i.minX = Math.min(t.slideWidth / 2 - p / 2, 0)),
            (i.maxX = -i.minX),
            (i.minY = Math.min(t.slideHeight / 2 - u / 2, 0)),
            (i.maxY = -i.minY),
            (i.currentX = Math.max(Math.min(i.currentX, i.maxX), i.minX)),
            (i.currentY = Math.max(Math.min(i.currentY, i.maxY), i.minY)),
            t.$imageWrapEl
              .transition(h)
              .transform(
                "translate3d(" + i.currentX + "px, " + i.currentY + "px,0)"
              );
        }
      },
      onTransitionEnd: function onTransitionEnd() {
        var e = this.zoom,
          t = e.gesture;
        t.$slideEl &&
          this.previousIndex !== this.activeIndex &&
          (t.$imageEl && t.$imageEl.transform("translate3d(0,0,0) scale(1)"),
          t.$imageWrapEl && t.$imageWrapEl.transform("translate3d(0,0,0)"),
          (e.scale = 1),
          (e.currentScale = 1),
          (t.$slideEl = void 0),
          (t.$imageEl = void 0),
          (t.$imageWrapEl = void 0));
      },
      toggle: function toggle(e) {
        var t = this.zoom;
        t.scale && 1 !== t.scale ? t.out() : t["in"](e);
      },
      in: function _in(e) {
        var t,
          i,
          s,
          a,
          r,
          n,
          l,
          o,
          d,
          h,
          p,
          u,
          c,
          v,
          f,
          m,
          g = this.zoom,
          w = this.params.zoom,
          b = g.gesture,
          y = g.image;
        (b.$slideEl ||
          (this.params.virtual && this.params.virtual.enabled && this.virtual
            ? (b.$slideEl = this.$wrapperEl.children(
                "." + this.params.slideActiveClass
              ))
            : (b.$slideEl = this.slides.eq(this.activeIndex)),
          (b.$imageEl = b.$slideEl.find(
            "img, svg, canvas, picture, .swiper-zoom-target"
          )),
          (b.$imageWrapEl = b.$imageEl.parent("." + w.containerClass))),
        b.$imageEl && 0 !== b.$imageEl.length) &&
          (b.$slideEl.addClass("" + w.zoomedSlideClass),
          void 0 === y.touchesStart.x && e
            ? ((t =
                "touchend" === e.type ? e.changedTouches[0].pageX : e.pageX),
              (i = "touchend" === e.type ? e.changedTouches[0].pageY : e.pageY))
            : ((t = y.touchesStart.x), (i = y.touchesStart.y)),
          (g.scale = b.$imageWrapEl.attr("data-swiper-zoom") || w.maxRatio),
          (g.currentScale =
            b.$imageWrapEl.attr("data-swiper-zoom") || w.maxRatio),
          e
            ? ((f = b.$slideEl[0].offsetWidth),
              (m = b.$slideEl[0].offsetHeight),
              (s = b.$slideEl.offset().left + f / 2 - t),
              (a = b.$slideEl.offset().top + m / 2 - i),
              (l = b.$imageEl[0].offsetWidth),
              (o = b.$imageEl[0].offsetHeight),
              (d = l * g.scale),
              (h = o * g.scale),
              (c = -(p = Math.min(f / 2 - d / 2, 0))),
              (v = -(u = Math.min(m / 2 - h / 2, 0))),
              (r = s * g.scale) < p && (r = p),
              r > c && (r = c),
              (n = a * g.scale) < u && (n = u),
              n > v && (n = v))
            : ((r = 0), (n = 0)),
          b.$imageWrapEl
            .transition(300)
            .transform("translate3d(" + r + "px, " + n + "px,0)"),
          b.$imageEl
            .transition(300)
            .transform("translate3d(0,0,0) scale(" + g.scale + ")"));
      },
      out: function out() {
        var e = this.zoom,
          t = this.params.zoom,
          i = e.gesture;
        i.$slideEl ||
          (this.params.virtual && this.params.virtual.enabled && this.virtual
            ? (i.$slideEl = this.$wrapperEl.children(
                "." + this.params.slideActiveClass
              ))
            : (i.$slideEl = this.slides.eq(this.activeIndex)),
          (i.$imageEl = i.$slideEl.find(
            "img, svg, canvas, picture, .swiper-zoom-target"
          )),
          (i.$imageWrapEl = i.$imageEl.parent("." + t.containerClass))),
          i.$imageEl &&
            0 !== i.$imageEl.length &&
            ((e.scale = 1),
            (e.currentScale = 1),
            i.$imageWrapEl.transition(300).transform("translate3d(0,0,0)"),
            i.$imageEl.transition(300).transform("translate3d(0,0,0) scale(1)"),
            i.$slideEl.removeClass("" + t.zoomedSlideClass),
            (i.$slideEl = void 0));
      },
      toggleGestures: function toggleGestures(e) {
        var t = this.zoom,
          i = t.slideSelector,
          s = t.passiveListener;
        this.$wrapperEl[e]("gesturestart", i, t.onGestureStart, s),
          this.$wrapperEl[e]("gesturechange", i, t.onGestureChange, s),
          this.$wrapperEl[e]("gestureend", i, t.onGestureEnd, s);
      },
      enableGestures: function enableGestures() {
        this.zoom.gesturesEnabled ||
          ((this.zoom.gesturesEnabled = !0), this.zoom.toggleGestures("on"));
      },
      disableGestures: function disableGestures() {
        this.zoom.gesturesEnabled &&
          ((this.zoom.gesturesEnabled = !1), this.zoom.toggleGestures("off"));
      },
      enable: function enable() {
        var e = this.support,
          t = this.zoom;

        if (!t.enabled) {
          t.enabled = !0;
          var i = !(
              "touchstart" !== this.touchEvents.start ||
              !e.passiveListener ||
              !this.params.passiveListeners
            ) && {
              passive: !0,
              capture: !1,
            },
            s = !e.passiveListener || {
              passive: !1,
              capture: !0,
            },
            a = "." + this.params.slideClass;
          (this.zoom.passiveListener = i),
            (this.zoom.slideSelector = a),
            e.gestures
              ? (this.$wrapperEl.on(
                  this.touchEvents.start,
                  this.zoom.enableGestures,
                  i
                ),
                this.$wrapperEl.on(
                  this.touchEvents.end,
                  this.zoom.disableGestures,
                  i
                ))
              : "touchstart" === this.touchEvents.start &&
                (this.$wrapperEl.on(
                  this.touchEvents.start,
                  a,
                  t.onGestureStart,
                  i
                ),
                this.$wrapperEl.on(
                  this.touchEvents.move,
                  a,
                  t.onGestureChange,
                  s
                ),
                this.$wrapperEl.on(this.touchEvents.end, a, t.onGestureEnd, i),
                this.touchEvents.cancel &&
                  this.$wrapperEl.on(
                    this.touchEvents.cancel,
                    a,
                    t.onGestureEnd,
                    i
                  )),
            this.$wrapperEl.on(
              this.touchEvents.move,
              "." + this.params.zoom.containerClass,
              t.onTouchMove,
              s
            );
        }
      },
      disable: function disable() {
        var e = this.zoom;

        if (e.enabled) {
          var t = this.support;
          this.zoom.enabled = !1;
          var i = !(
              "touchstart" !== this.touchEvents.start ||
              !t.passiveListener ||
              !this.params.passiveListeners
            ) && {
              passive: !0,
              capture: !1,
            },
            s = !t.passiveListener || {
              passive: !1,
              capture: !0,
            },
            a = "." + this.params.slideClass;
          t.gestures
            ? (this.$wrapperEl.off(
                this.touchEvents.start,
                this.zoom.enableGestures,
                i
              ),
              this.$wrapperEl.off(
                this.touchEvents.end,
                this.zoom.disableGestures,
                i
              ))
            : "touchstart" === this.touchEvents.start &&
              (this.$wrapperEl.off(
                this.touchEvents.start,
                a,
                e.onGestureStart,
                i
              ),
              this.$wrapperEl.off(
                this.touchEvents.move,
                a,
                e.onGestureChange,
                s
              ),
              this.$wrapperEl.off(this.touchEvents.end, a, e.onGestureEnd, i),
              this.touchEvents.cancel &&
                this.$wrapperEl.off(
                  this.touchEvents.cancel,
                  a,
                  e.onGestureEnd,
                  i
                )),
            this.$wrapperEl.off(
              this.touchEvents.move,
              "." + this.params.zoom.containerClass,
              e.onTouchMove,
              s
            );
        }
      },
    },
    te = {
      loadInSlide: function loadInSlide(e, t) {
        void 0 === t && (t = !0);
        var i = this,
          s = i.params.lazy;

        if (void 0 !== e && 0 !== i.slides.length) {
          var a =
              i.virtual && i.params.virtual.enabled
                ? i.$wrapperEl.children(
                    "." +
                      i.params.slideClass +
                      '[data-swiper-slide-index="' +
                      e +
                      '"]'
                  )
                : i.slides.eq(e),
            r = a.find(
              "." +
                s.elementClass +
                ":not(." +
                s.loadedClass +
                "):not(." +
                s.loadingClass +
                ")"
            );
          !a.hasClass(s.elementClass) ||
            a.hasClass(s.loadedClass) ||
            a.hasClass(s.loadingClass) ||
            r.push(a[0]),
            0 !== r.length &&
              r.each(function (e) {
                var r = m(e);
                r.addClass(s.loadingClass);
                var n = r.attr("data-background"),
                  l = r.attr("data-src"),
                  o = r.attr("data-srcset"),
                  d = r.attr("data-sizes"),
                  h = r.parent("picture");
                i.loadImage(r[0], l || n, o, d, !1, function () {
                  if (null != i && i && (!i || i.params) && !i.destroyed) {
                    if (
                      (n
                        ? (r.css("background-image", 'url("' + n + '")'),
                          r.removeAttr("data-background"))
                        : (o &&
                            (r.attr("srcset", o), r.removeAttr("data-srcset")),
                          d && (r.attr("sizes", d), r.removeAttr("data-sizes")),
                          h.length &&
                            h.children("source").each(function (e) {
                              var t = m(e);
                              t.attr("data-srcset") &&
                                (t.attr("srcset", t.attr("data-srcset")),
                                t.removeAttr("data-srcset"));
                            }),
                          l && (r.attr("src", l), r.removeAttr("data-src"))),
                      r.addClass(s.loadedClass).removeClass(s.loadingClass),
                      a.find("." + s.preloaderClass).remove(),
                      i.params.loop && t)
                    ) {
                      var e = a.attr("data-swiper-slide-index");

                      if (a.hasClass(i.params.slideDuplicateClass)) {
                        var p = i.$wrapperEl.children(
                          '[data-swiper-slide-index="' +
                            e +
                            '"]:not(.' +
                            i.params.slideDuplicateClass +
                            ")"
                        );
                        i.lazy.loadInSlide(p.index(), !1);
                      } else {
                        var u = i.$wrapperEl.children(
                          "." +
                            i.params.slideDuplicateClass +
                            '[data-swiper-slide-index="' +
                            e +
                            '"]'
                        );
                        i.lazy.loadInSlide(u.index(), !1);
                      }
                    }

                    i.emit("lazyImageReady", a[0], r[0]),
                      i.params.autoHeight && i.updateAutoHeight();
                  }
                }),
                  i.emit("lazyImageLoad", a[0], r[0]);
              });
        }
      },
      load: function load() {
        var e = this,
          t = e.$wrapperEl,
          i = e.params,
          s = e.slides,
          a = e.activeIndex,
          r = e.virtual && i.virtual.enabled,
          n = i.lazy,
          l = i.slidesPerView;

        function o(e) {
          if (r) {
            if (
              t.children(
                "." + i.slideClass + '[data-swiper-slide-index="' + e + '"]'
              ).length
            )
              return !0;
          } else if (s[e]) return !0;

          return !1;
        }

        function d(e) {
          return r ? m(e).attr("data-swiper-slide-index") : m(e).index();
        }

        if (
          ("auto" === l && (l = 0),
          e.lazy.initialImageLoaded || (e.lazy.initialImageLoaded = !0),
          e.params.watchSlidesVisibility)
        )
          t.children("." + i.slideVisibleClass).each(function (t) {
            var i = r ? m(t).attr("data-swiper-slide-index") : m(t).index();
            e.lazy.loadInSlide(i);
          });
        else if (l > 1)
          for (var h = a; h < a + l; h += 1) {
            o(h) && e.lazy.loadInSlide(h);
          }
        else e.lazy.loadInSlide(a);
        if (n.loadPrevNext)
          if (l > 1 || (n.loadPrevNextAmount && n.loadPrevNextAmount > 1)) {
            for (
              var p = n.loadPrevNextAmount,
                u = l,
                c = Math.min(a + u + Math.max(p, u), s.length),
                v = Math.max(a - Math.max(u, p), 0),
                f = a + l;
              f < c;
              f += 1
            ) {
              o(f) && e.lazy.loadInSlide(f);
            }

            for (var g = v; g < a; g += 1) {
              o(g) && e.lazy.loadInSlide(g);
            }
          } else {
            var w = t.children("." + i.slideNextClass);
            w.length > 0 && e.lazy.loadInSlide(d(w));
            var b = t.children("." + i.slidePrevClass);
            b.length > 0 && e.lazy.loadInSlide(d(b));
          }
      },
    },
    ie = {
      LinearSpline: function LinearSpline(e, t) {
        var i,
          s,
          a,
          r,
          n,
          l = function l(e, t) {
            for (s = -1, i = e.length; i - s > 1; ) {
              e[(a = (i + s) >> 1)] <= t ? (s = a) : (i = a);
            }

            return i;
          };

        return (
          (this.x = e),
          (this.y = t),
          (this.lastIndex = e.length - 1),
          (this.interpolate = function (e) {
            return e
              ? ((n = l(this.x, e)),
                (r = n - 1),
                ((e - this.x[r]) * (this.y[n] - this.y[r])) /
                  (this.x[n] - this.x[r]) +
                  this.y[r])
              : 0;
          }),
          this
        );
      },
      getInterpolateFunction: function getInterpolateFunction(e) {
        this.controller.spline ||
          (this.controller.spline = this.params.loop
            ? new ie.LinearSpline(this.slidesGrid, e.slidesGrid)
            : new ie.LinearSpline(this.snapGrid, e.snapGrid));
      },
      setTranslate: function setTranslate(e, t) {
        var i,
          s,
          a = this,
          r = a.controller.control,
          n = a.constructor;

        function l(e) {
          var t = a.rtlTranslate ? -a.translate : a.translate;
          "slide" === a.params.controller.by &&
            (a.controller.getInterpolateFunction(e),
            (s = -a.controller.spline.interpolate(-t))),
            (s && "container" !== a.params.controller.by) ||
              ((i =
                (e.maxTranslate() - e.minTranslate()) /
                (a.maxTranslate() - a.minTranslate())),
              (s = (t - a.minTranslate()) * i + e.minTranslate())),
            a.params.controller.inverse && (s = e.maxTranslate() - s),
            e.updateProgress(s),
            e.setTranslate(s, a),
            e.updateActiveIndex(),
            e.updateSlidesClasses();
        }

        if (Array.isArray(r))
          for (var o = 0; o < r.length; o += 1) {
            r[o] !== t && r[o] instanceof n && l(r[o]);
          }
        else r instanceof n && t !== r && l(r);
      },
      setTransition: function setTransition(e, t) {
        var i,
          s = this,
          a = s.constructor,
          r = s.controller.control;

        function n(t) {
          t.setTransition(e, s),
            0 !== e &&
              (t.transitionStart(),
              t.params.autoHeight &&
                E(function () {
                  t.updateAutoHeight();
                }),
              t.$wrapperEl.transitionEnd(function () {
                r &&
                  (t.params.loop &&
                    "slide" === s.params.controller.by &&
                    t.loopFix(),
                  t.transitionEnd());
              }));
        }

        if (Array.isArray(r))
          for (i = 0; i < r.length; i += 1) {
            r[i] !== t && r[i] instanceof a && n(r[i]);
          }
        else r instanceof a && t !== r && n(r);
      },
    },
    se = {
      makeElFocusable: function makeElFocusable(e) {
        return e.attr("tabIndex", "0"), e;
      },
      makeElNotFocusable: function makeElNotFocusable(e) {
        return e.attr("tabIndex", "-1"), e;
      },
      addElRole: function addElRole(e, t) {
        return e.attr("role", t), e;
      },
      addElLabel: function addElLabel(e, t) {
        return e.attr("aria-label", t), e;
      },
      disableEl: function disableEl(e) {
        return e.attr("aria-disabled", !0), e;
      },
      enableEl: function enableEl(e) {
        return e.attr("aria-disabled", !1), e;
      },
      onEnterKey: function onEnterKey(e) {
        var t = this.params.a11y;

        if (13 === e.keyCode) {
          var i = m(e.target);
          this.navigation &&
            this.navigation.$nextEl &&
            i.is(this.navigation.$nextEl) &&
            ((this.isEnd && !this.params.loop) || this.slideNext(),
            this.isEnd
              ? this.a11y.notify(t.lastSlideMessage)
              : this.a11y.notify(t.nextSlideMessage)),
            this.navigation &&
              this.navigation.$prevEl &&
              i.is(this.navigation.$prevEl) &&
              ((this.isBeginning && !this.params.loop) || this.slidePrev(),
              this.isBeginning
                ? this.a11y.notify(t.firstSlideMessage)
                : this.a11y.notify(t.prevSlideMessage)),
            this.pagination &&
              i.is("." + this.params.pagination.bulletClass) &&
              i[0].click();
        }
      },
      notify: function notify(e) {
        var t = this.a11y.liveRegion;
        0 !== t.length && (t.html(""), t.html(e));
      },
      updateNavigation: function updateNavigation() {
        if (!this.params.loop && this.navigation) {
          var e = this.navigation,
            t = e.$nextEl,
            i = e.$prevEl;
          i &&
            i.length > 0 &&
            (this.isBeginning
              ? (this.a11y.disableEl(i), this.a11y.makeElNotFocusable(i))
              : (this.a11y.enableEl(i), this.a11y.makeElFocusable(i))),
            t &&
              t.length > 0 &&
              (this.isEnd
                ? (this.a11y.disableEl(t), this.a11y.makeElNotFocusable(t))
                : (this.a11y.enableEl(t), this.a11y.makeElFocusable(t)));
        }
      },
      updatePagination: function updatePagination() {
        var e = this,
          t = e.params.a11y;
        e.pagination &&
          e.params.pagination.clickable &&
          e.pagination.bullets &&
          e.pagination.bullets.length &&
          e.pagination.bullets.each(function (i) {
            var s = m(i);
            e.a11y.makeElFocusable(s),
              e.a11y.addElRole(s, "button"),
              e.a11y.addElLabel(
                s,
                t.paginationBulletMessage.replace(
                  /\{\{index\}\}/,
                  s.index() + 1
                )
              );
          });
      },
      init: function init() {
        this.$el.append(this.a11y.liveRegion);
        var e,
          t,
          i = this.params.a11y;
        this.navigation &&
          this.navigation.$nextEl &&
          (e = this.navigation.$nextEl),
          this.navigation &&
            this.navigation.$prevEl &&
            (t = this.navigation.$prevEl),
          e &&
            (this.a11y.makeElFocusable(e),
            this.a11y.addElRole(e, "button"),
            this.a11y.addElLabel(e, i.nextSlideMessage),
            e.on("keydown", this.a11y.onEnterKey)),
          t &&
            (this.a11y.makeElFocusable(t),
            this.a11y.addElRole(t, "button"),
            this.a11y.addElLabel(t, i.prevSlideMessage),
            t.on("keydown", this.a11y.onEnterKey)),
          this.pagination &&
            this.params.pagination.clickable &&
            this.pagination.bullets &&
            this.pagination.bullets.length &&
            this.pagination.$el.on(
              "keydown",
              "." + this.params.pagination.bulletClass,
              this.a11y.onEnterKey
            );
      },
      destroy: function destroy() {
        var e, t;
        this.a11y.liveRegion &&
          this.a11y.liveRegion.length > 0 &&
          this.a11y.liveRegion.remove(),
          this.navigation &&
            this.navigation.$nextEl &&
            (e = this.navigation.$nextEl),
          this.navigation &&
            this.navigation.$prevEl &&
            (t = this.navigation.$prevEl),
          e && e.off("keydown", this.a11y.onEnterKey),
          t && t.off("keydown", this.a11y.onEnterKey),
          this.pagination &&
            this.params.pagination.clickable &&
            this.pagination.bullets &&
            this.pagination.bullets.length &&
            this.pagination.$el.off(
              "keydown",
              "." + this.params.pagination.bulletClass,
              this.a11y.onEnterKey
            );
      },
    },
    ae = {
      init: function init() {
        var e = l();

        if (this.params.history) {
          if (!e.history || !e.history.pushState)
            return (
              (this.params.history.enabled = !1),
              void (this.params.hashNavigation.enabled = !0)
            );
          var t = this.history;
          (t.initialized = !0),
            (t.paths = ae.getPathValues(this.params.url)),
            (t.paths.key || t.paths.value) &&
              (t.scrollToSlide(
                0,
                t.paths.value,
                this.params.runCallbacksOnInit
              ),
              this.params.history.replaceState ||
                e.addEventListener(
                  "popstate",
                  this.history.setHistoryPopState
                ));
        }
      },
      destroy: function destroy() {
        var e = l();
        this.params.history.replaceState ||
          e.removeEventListener("popstate", this.history.setHistoryPopState);
      },
      setHistoryPopState: function setHistoryPopState() {
        (this.history.paths = ae.getPathValues(this.params.url)),
          this.history.scrollToSlide(
            this.params.speed,
            this.history.paths.value,
            !1
          );
      },
      getPathValues: function getPathValues(e) {
        var t = l(),
          i = (e ? new URL(e) : t.location).pathname
            .slice(1)
            .split("/")
            .filter(function (e) {
              return "" !== e;
            }),
          s = i.length;
        return {
          key: i[s - 2],
          value: i[s - 1],
        };
      },
      setHistory: function setHistory(e, t) {
        var i = l();

        if (this.history.initialized && this.params.history.enabled) {
          var s;
          s = this.params.url ? new URL(this.params.url) : i.location;
          var a = this.slides.eq(t),
            r = ae.slugify(a.attr("data-history"));
          s.pathname.includes(e) || (r = e + "/" + r);
          var n = i.history.state;
          (n && n.value === r) ||
            (this.params.history.replaceState
              ? i.history.replaceState(
                  {
                    value: r,
                  },
                  null,
                  r
                )
              : i.history.pushState(
                  {
                    value: r,
                  },
                  null,
                  r
                ));
        }
      },
      slugify: function slugify(e) {
        return e
          .toString()
          .replace(/\s+/g, "-")
          .replace(/[^\w-]+/g, "")
          .replace(/--+/g, "-")
          .replace(/^-+/, "")
          .replace(/-+$/, "");
      },
      scrollToSlide: function scrollToSlide(e, t, i) {
        if (t)
          for (var s = 0, a = this.slides.length; s < a; s += 1) {
            var r = this.slides.eq(s);

            if (
              ae.slugify(r.attr("data-history")) === t &&
              !r.hasClass(this.params.slideDuplicateClass)
            ) {
              var n = r.index();
              this.slideTo(n, e, i);
            }
          }
        else this.slideTo(0, e, i);
      },
    },
    re = {
      onHashCange: function onHashCange() {
        var e = r();
        this.emit("hashChange");
        var t = e.location.hash.replace("#", "");

        if (t !== this.slides.eq(this.activeIndex).attr("data-hash")) {
          var i = this.$wrapperEl
            .children("." + this.params.slideClass + '[data-hash="' + t + '"]')
            .index();
          if (void 0 === i) return;
          this.slideTo(i);
        }
      },
      setHash: function setHash() {
        var e = l(),
          t = r();
        if (
          this.hashNavigation.initialized &&
          this.params.hashNavigation.enabled
        )
          if (
            this.params.hashNavigation.replaceState &&
            e.history &&
            e.history.replaceState
          )
            e.history.replaceState(
              null,
              null,
              "#" + this.slides.eq(this.activeIndex).attr("data-hash") || ""
            ),
              this.emit("hashSet");
          else {
            var i = this.slides.eq(this.activeIndex),
              s = i.attr("data-hash") || i.attr("data-history");
            (t.location.hash = s || ""), this.emit("hashSet");
          }
      },
      init: function init() {
        var e = r(),
          t = l();

        if (
          !(
            !this.params.hashNavigation.enabled ||
            (this.params.history && this.params.history.enabled)
          )
        ) {
          this.hashNavigation.initialized = !0;
          var i = e.location.hash.replace("#", "");
          if (i)
            for (var s = 0, a = this.slides.length; s < a; s += 1) {
              var n = this.slides.eq(s);

              if (
                (n.attr("data-hash") || n.attr("data-history")) === i &&
                !n.hasClass(this.params.slideDuplicateClass)
              ) {
                var o = n.index();
                this.slideTo(o, 0, this.params.runCallbacksOnInit, !0);
              }
            }
          this.params.hashNavigation.watchState &&
            m(t).on("hashchange", this.hashNavigation.onHashCange);
        }
      },
      destroy: function destroy() {
        var e = l();
        this.params.hashNavigation.watchState &&
          m(e).off("hashchange", this.hashNavigation.onHashCange);
      },
    },
    ne = {
      run: function run() {
        var e = this,
          t = e.slides.eq(e.activeIndex),
          i = e.params.autoplay.delay;
        t.attr("data-swiper-autoplay") &&
          (i = t.attr("data-swiper-autoplay") || e.params.autoplay.delay),
          clearTimeout(e.autoplay.timeout),
          (e.autoplay.timeout = E(function () {
            e.params.autoplay.reverseDirection
              ? e.params.loop
                ? (e.loopFix(),
                  e.slidePrev(e.params.speed, !0, !0),
                  e.emit("autoplay"))
                : e.isBeginning
                ? e.params.autoplay.stopOnLastSlide
                  ? e.autoplay.stop()
                  : (e.slideTo(e.slides.length - 1, e.params.speed, !0, !0),
                    e.emit("autoplay"))
                : (e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay"))
              : e.params.loop
              ? (e.loopFix(),
                e.slideNext(e.params.speed, !0, !0),
                e.emit("autoplay"))
              : e.isEnd
              ? e.params.autoplay.stopOnLastSlide
                ? e.autoplay.stop()
                : (e.slideTo(0, e.params.speed, !0, !0), e.emit("autoplay"))
              : (e.slideNext(e.params.speed, !0, !0), e.emit("autoplay")),
              e.params.cssMode && e.autoplay.running && e.autoplay.run();
          }, i));
      },
      start: function start() {
        return (
          void 0 === this.autoplay.timeout &&
          !this.autoplay.running &&
          ((this.autoplay.running = !0),
          this.emit("autoplayStart"),
          this.autoplay.run(),
          !0)
        );
      },
      stop: function stop() {
        return (
          !!this.autoplay.running &&
          void 0 !== this.autoplay.timeout &&
          (this.autoplay.timeout &&
            (clearTimeout(this.autoplay.timeout),
            (this.autoplay.timeout = void 0)),
          (this.autoplay.running = !1),
          this.emit("autoplayStop"),
          !0)
        );
      },
      pause: function pause(e) {
        this.autoplay.running &&
          (this.autoplay.paused ||
            (this.autoplay.timeout && clearTimeout(this.autoplay.timeout),
            (this.autoplay.paused = !0),
            0 !== e && this.params.autoplay.waitForTransition
              ? (this.$wrapperEl[0].addEventListener(
                  "transitionend",
                  this.autoplay.onTransitionEnd
                ),
                this.$wrapperEl[0].addEventListener(
                  "webkitTransitionEnd",
                  this.autoplay.onTransitionEnd
                ))
              : ((this.autoplay.paused = !1), this.autoplay.run())));
      },
      onVisibilityChange: function onVisibilityChange() {
        var e = r();
        "hidden" === e.visibilityState &&
          this.autoplay.running &&
          this.autoplay.pause(),
          "visible" === e.visibilityState &&
            this.autoplay.paused &&
            (this.autoplay.run(), (this.autoplay.paused = !1));
      },
      onTransitionEnd: function onTransitionEnd(e) {
        this &&
          !this.destroyed &&
          this.$wrapperEl &&
          e.target === this.$wrapperEl[0] &&
          (this.$wrapperEl[0].removeEventListener(
            "transitionend",
            this.autoplay.onTransitionEnd
          ),
          this.$wrapperEl[0].removeEventListener(
            "webkitTransitionEnd",
            this.autoplay.onTransitionEnd
          ),
          (this.autoplay.paused = !1),
          this.autoplay.running ? this.autoplay.run() : this.autoplay.stop());
      },
    },
    le = {
      setTranslate: function setTranslate() {
        for (var e = this.slides, t = 0; t < e.length; t += 1) {
          var i = this.slides.eq(t),
            s = -i[0].swiperSlideOffset;
          this.params.virtualTranslate || (s -= this.translate);
          var a = 0;
          this.isHorizontal() || ((a = s), (s = 0));
          var r = this.params.fadeEffect.crossFade
            ? Math.max(1 - Math.abs(i[0].progress), 0)
            : 1 + Math.min(Math.max(i[0].progress, -1), 0);
          i.css({
            opacity: r,
          }).transform("translate3d(" + s + "px, " + a + "px, 0px)");
        }
      },
      setTransition: function setTransition(e) {
        var t = this,
          i = t.slides,
          s = t.$wrapperEl;

        if ((i.transition(e), t.params.virtualTranslate && 0 !== e)) {
          var a = !1;
          i.transitionEnd(function () {
            if (!a && t && !t.destroyed) {
              (a = !0), (t.animating = !1);

              for (
                var e = ["webkitTransitionEnd", "transitionend"], i = 0;
                i < e.length;
                i += 1
              ) {
                s.trigger(e[i]);
              }
            }
          });
        }
      },
    },
    oe = {
      setTranslate: function setTranslate() {
        var e,
          t = this.$el,
          i = this.$wrapperEl,
          s = this.slides,
          a = this.width,
          r = this.height,
          n = this.rtlTranslate,
          l = this.size,
          o = this.browser,
          d = this.params.cubeEffect,
          h = this.isHorizontal(),
          p = this.virtual && this.params.virtual.enabled,
          u = 0;
        d.shadow &&
          (h
            ? (0 === (e = i.find(".swiper-cube-shadow")).length &&
                ((e = m('<div class="swiper-cube-shadow"></div>')),
                i.append(e)),
              e.css({
                height: a + "px",
              }))
            : 0 === (e = t.find(".swiper-cube-shadow")).length &&
              ((e = m('<div class="swiper-cube-shadow"></div>')), t.append(e)));

        for (var c = 0; c < s.length; c += 1) {
          var v = s.eq(c),
            f = c;
          p && (f = parseInt(v.attr("data-swiper-slide-index"), 10));
          var g = 90 * f,
            w = Math.floor(g / 360);
          n && ((g = -g), (w = Math.floor(-g / 360)));
          var b = Math.max(Math.min(v[0].progress, 1), -1),
            y = 0,
            E = 0,
            x = 0;
          f % 4 == 0
            ? ((y = 4 * -w * l), (x = 0))
            : (f - 1) % 4 == 0
            ? ((y = 0), (x = 4 * -w * l))
            : (f - 2) % 4 == 0
            ? ((y = l + 4 * w * l), (x = l))
            : (f - 3) % 4 == 0 && ((y = -l), (x = 3 * l + 4 * l * w)),
            n && (y = -y),
            h || ((E = y), (y = 0));
          var T =
            "rotateX(" +
            (h ? 0 : -g) +
            "deg) rotateY(" +
            (h ? g : 0) +
            "deg) translate3d(" +
            y +
            "px, " +
            E +
            "px, " +
            x +
            "px)";

          if (
            (b <= 1 &&
              b > -1 &&
              ((u = 90 * f + 90 * b), n && (u = 90 * -f - 90 * b)),
            v.transform(T),
            d.slideShadows)
          ) {
            var C = h
                ? v.find(".swiper-slide-shadow-left")
                : v.find(".swiper-slide-shadow-top"),
              S = h
                ? v.find(".swiper-slide-shadow-right")
                : v.find(".swiper-slide-shadow-bottom");
            0 === C.length &&
              ((C = m(
                '<div class="swiper-slide-shadow-' +
                  (h ? "left" : "top") +
                  '"></div>'
              )),
              v.append(C)),
              0 === S.length &&
                ((S = m(
                  '<div class="swiper-slide-shadow-' +
                    (h ? "right" : "bottom") +
                    '"></div>'
                )),
                v.append(S)),
              C.length && (C[0].style.opacity = Math.max(-b, 0)),
              S.length && (S[0].style.opacity = Math.max(b, 0));
          }
        }

        if (
          (i.css({
            "-webkit-transform-origin": "50% 50% -" + l / 2 + "px",
            "-moz-transform-origin": "50% 50% -" + l / 2 + "px",
            "-ms-transform-origin": "50% 50% -" + l / 2 + "px",
            "transform-origin": "50% 50% -" + l / 2 + "px",
          }),
          d.shadow)
        )
          if (h)
            e.transform(
              "translate3d(0px, " +
                (a / 2 + d.shadowOffset) +
                "px, " +
                -a / 2 +
                "px) rotateX(90deg) rotateZ(0deg) scale(" +
                d.shadowScale +
                ")"
            );
          else {
            var M = Math.abs(u) - 90 * Math.floor(Math.abs(u) / 90),
              z =
                1.5 -
                (Math.sin((2 * M * Math.PI) / 360) / 2 +
                  Math.cos((2 * M * Math.PI) / 360) / 2),
              P = d.shadowScale,
              k = d.shadowScale / z,
              $ = d.shadowOffset;
            e.transform(
              "scale3d(" +
                P +
                ", 1, " +
                k +
                ") translate3d(0px, " +
                (r / 2 + $) +
                "px, " +
                -r / 2 / k +
                "px) rotateX(-90deg)"
            );
          }
        var L = o.isSafari || o.isWebView ? -l / 2 : 0;
        i.transform(
          "translate3d(0px,0," +
            L +
            "px) rotateX(" +
            (this.isHorizontal() ? 0 : u) +
            "deg) rotateY(" +
            (this.isHorizontal() ? -u : 0) +
            "deg)"
        );
      },
      setTransition: function setTransition(e) {
        var t = this.$el;
        this.slides
          .transition(e)
          .find(
            ".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left"
          )
          .transition(e),
          this.params.cubeEffect.shadow &&
            !this.isHorizontal() &&
            t.find(".swiper-cube-shadow").transition(e);
      },
    },
    de = {
      setTranslate: function setTranslate() {
        for (
          var e = this.slides, t = this.rtlTranslate, i = 0;
          i < e.length;
          i += 1
        ) {
          var s = e.eq(i),
            a = s[0].progress;
          this.params.flipEffect.limitRotation &&
            (a = Math.max(Math.min(s[0].progress, 1), -1));
          var r = -180 * a,
            n = 0,
            l = -s[0].swiperSlideOffset,
            o = 0;

          if (
            (this.isHorizontal()
              ? t && (r = -r)
              : ((o = l), (l = 0), (n = -r), (r = 0)),
            (s[0].style.zIndex = -Math.abs(Math.round(a)) + e.length),
            this.params.flipEffect.slideShadows)
          ) {
            var d = this.isHorizontal()
                ? s.find(".swiper-slide-shadow-left")
                : s.find(".swiper-slide-shadow-top"),
              h = this.isHorizontal()
                ? s.find(".swiper-slide-shadow-right")
                : s.find(".swiper-slide-shadow-bottom");
            0 === d.length &&
              ((d = m(
                '<div class="swiper-slide-shadow-' +
                  (this.isHorizontal() ? "left" : "top") +
                  '"></div>'
              )),
              s.append(d)),
              0 === h.length &&
                ((h = m(
                  '<div class="swiper-slide-shadow-' +
                    (this.isHorizontal() ? "right" : "bottom") +
                    '"></div>'
                )),
                s.append(h)),
              d.length && (d[0].style.opacity = Math.max(-a, 0)),
              h.length && (h[0].style.opacity = Math.max(a, 0));
          }

          s.transform(
            "translate3d(" +
              l +
              "px, " +
              o +
              "px, 0px) rotateX(" +
              n +
              "deg) rotateY(" +
              r +
              "deg)"
          );
        }
      },
      setTransition: function setTransition(e) {
        var t = this,
          i = t.slides,
          s = t.activeIndex,
          a = t.$wrapperEl;

        if (
          (i
            .transition(e)
            .find(
              ".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left"
            )
            .transition(e),
          t.params.virtualTranslate && 0 !== e)
        ) {
          var r = !1;
          i.eq(s).transitionEnd(function () {
            if (!r && t && !t.destroyed) {
              (r = !0), (t.animating = !1);

              for (
                var e = ["webkitTransitionEnd", "transitionend"], i = 0;
                i < e.length;
                i += 1
              ) {
                a.trigger(e[i]);
              }
            }
          });
        }
      },
    },
    he = {
      setTranslate: function setTranslate() {
        for (
          var e = this.width,
            t = this.height,
            i = this.slides,
            s = this.slidesSizesGrid,
            a = this.params.coverflowEffect,
            r = this.isHorizontal(),
            n = this.translate,
            l = r ? e / 2 - n : t / 2 - n,
            o = r ? a.rotate : -a.rotate,
            d = a.depth,
            h = 0,
            p = i.length;
          h < p;
          h += 1
        ) {
          var u = i.eq(h),
            c = s[h],
            v = ((l - u[0].swiperSlideOffset - c / 2) / c) * a.modifier,
            f = r ? o * v : 0,
            g = r ? 0 : o * v,
            w = -d * Math.abs(v),
            b = a.stretch;
          "string" == typeof b &&
            -1 !== b.indexOf("%") &&
            (b = (parseFloat(a.stretch) / 100) * c);
          var y = r ? 0 : b * v,
            E = r ? b * v : 0,
            x = 1 - (1 - a.scale) * Math.abs(v);
          Math.abs(E) < 0.001 && (E = 0),
            Math.abs(y) < 0.001 && (y = 0),
            Math.abs(w) < 0.001 && (w = 0),
            Math.abs(f) < 0.001 && (f = 0),
            Math.abs(g) < 0.001 && (g = 0),
            Math.abs(x) < 0.001 && (x = 0);
          var T =
            "translate3d(" +
            E +
            "px," +
            y +
            "px," +
            w +
            "px)  rotateX(" +
            g +
            "deg) rotateY(" +
            f +
            "deg) scale(" +
            x +
            ")";

          if (
            (u.transform(T),
            (u[0].style.zIndex = 1 - Math.abs(Math.round(v))),
            a.slideShadows)
          ) {
            var C = r
                ? u.find(".swiper-slide-shadow-left")
                : u.find(".swiper-slide-shadow-top"),
              S = r
                ? u.find(".swiper-slide-shadow-right")
                : u.find(".swiper-slide-shadow-bottom");
            0 === C.length &&
              ((C = m(
                '<div class="swiper-slide-shadow-' +
                  (r ? "left" : "top") +
                  '"></div>'
              )),
              u.append(C)),
              0 === S.length &&
                ((S = m(
                  '<div class="swiper-slide-shadow-' +
                    (r ? "right" : "bottom") +
                    '"></div>'
                )),
                u.append(S)),
              C.length && (C[0].style.opacity = v > 0 ? v : 0),
              S.length && (S[0].style.opacity = -v > 0 ? -v : 0);
          }
        }
      },
      setTransition: function setTransition(e) {
        this.slides
          .transition(e)
          .find(
            ".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left"
          )
          .transition(e);
      },
    },
    pe = {
      init: function init() {
        var e = this.params.thumbs;
        if (this.thumbs.initialized) return !1;
        this.thumbs.initialized = !0;
        var t = this.constructor;
        return (
          e.swiper instanceof t
            ? ((this.thumbs.swiper = e.swiper),
              S(this.thumbs.swiper.originalParams, {
                watchSlidesProgress: !0,
                slideToClickedSlide: !1,
              }),
              S(this.thumbs.swiper.params, {
                watchSlidesProgress: !0,
                slideToClickedSlide: !1,
              }))
            : C(e.swiper) &&
              ((this.thumbs.swiper = new t(
                S({}, e.swiper, {
                  watchSlidesVisibility: !0,
                  watchSlidesProgress: !0,
                  slideToClickedSlide: !1,
                })
              )),
              (this.thumbs.swiperCreated = !0)),
          this.thumbs.swiper.$el.addClass(
            this.params.thumbs.thumbsContainerClass
          ),
          this.thumbs.swiper.on("tap", this.thumbs.onThumbClick),
          !0
        );
      },
      onThumbClick: function onThumbClick() {
        var e = this.thumbs.swiper;

        if (e) {
          var t = e.clickedIndex,
            i = e.clickedSlide;

          if (
            !(
              (i && m(i).hasClass(this.params.thumbs.slideThumbActiveClass)) ||
              null == t
            )
          ) {
            var s;

            if (
              ((s = e.params.loop
                ? parseInt(
                    m(e.clickedSlide).attr("data-swiper-slide-index"),
                    10
                  )
                : t),
              this.params.loop)
            ) {
              var a = this.activeIndex;
              this.slides.eq(a).hasClass(this.params.slideDuplicateClass) &&
                (this.loopFix(),
                (this._clientLeft = this.$wrapperEl[0].clientLeft),
                (a = this.activeIndex));
              var r = this.slides
                  .eq(a)
                  .prevAll('[data-swiper-slide-index="' + s + '"]')
                  .eq(0)
                  .index(),
                n = this.slides
                  .eq(a)
                  .nextAll('[data-swiper-slide-index="' + s + '"]')
                  .eq(0)
                  .index();
              s = void 0 === r ? n : void 0 === n ? r : n - a < a - r ? n : r;
            }

            this.slideTo(s);
          }
        }
      },
      update: function update(e) {
        var t = this.thumbs.swiper;

        if (t) {
          var i =
              "auto" === t.params.slidesPerView
                ? t.slidesPerViewDynamic()
                : t.params.slidesPerView,
            s = this.params.thumbs.autoScrollOffset,
            a = s && !t.params.loop;

          if (this.realIndex !== t.realIndex || a) {
            var r,
              n,
              l = t.activeIndex;

            if (t.params.loop) {
              t.slides.eq(l).hasClass(t.params.slideDuplicateClass) &&
                (t.loopFix(),
                (t._clientLeft = t.$wrapperEl[0].clientLeft),
                (l = t.activeIndex));
              var o = t.slides
                  .eq(l)
                  .prevAll('[data-swiper-slide-index="' + this.realIndex + '"]')
                  .eq(0)
                  .index(),
                d = t.slides
                  .eq(l)
                  .nextAll('[data-swiper-slide-index="' + this.realIndex + '"]')
                  .eq(0)
                  .index();
              (r =
                void 0 === o
                  ? d
                  : void 0 === d
                  ? o
                  : d - l == l - o
                  ? l
                  : d - l < l - o
                  ? d
                  : o),
                (n = this.activeIndex > this.previousIndex ? "next" : "prev");
            } else
              n = (r = this.realIndex) > this.previousIndex ? "next" : "prev";

            a && (r += "next" === n ? s : -1 * s),
              t.visibleSlidesIndexes &&
                t.visibleSlidesIndexes.indexOf(r) < 0 &&
                (t.params.centeredSlides
                  ? (r =
                      r > l
                        ? r - Math.floor(i / 2) + 1
                        : r + Math.floor(i / 2) - 1)
                  : r > l && (r = r - i + 1),
                t.slideTo(r, e ? 0 : void 0));
          }

          var h = 1,
            p = this.params.thumbs.slideThumbActiveClass;
          if (
            (this.params.slidesPerView > 1 &&
              !this.params.centeredSlides &&
              (h = this.params.slidesPerView),
            this.params.thumbs.multipleActiveThumbs || (h = 1),
            (h = Math.floor(h)),
            t.slides.removeClass(p),
            t.params.loop || (t.params.virtual && t.params.virtual.enabled))
          )
            for (var u = 0; u < h; u += 1) {
              t.$wrapperEl
                .children(
                  '[data-swiper-slide-index="' + (this.realIndex + u) + '"]'
                )
                .addClass(p);
            }
          else
            for (var c = 0; c < h; c += 1) {
              t.slides.eq(this.realIndex + c).addClass(p);
            }
        }
      },
    },
    ue = [
      q,
      _,
      {
        name: "mousewheel",
        params: {
          mousewheel: {
            enabled: !1,
            releaseOnEdges: !1,
            invert: !1,
            forceToAxis: !1,
            sensitivity: 1,
            eventsTarget: "container",
            thresholdDelta: null,
            thresholdTime: null,
          },
        },
        create: function create() {
          M(this, {
            mousewheel: {
              enabled: !1,
              lastScrollTime: x(),
              lastEventBeforeSnap: void 0,
              recentWheelEvents: [],
              enable: U.enable,
              disable: U.disable,
              handle: U.handle,
              handleMouseEnter: U.handleMouseEnter,
              handleMouseLeave: U.handleMouseLeave,
              animateSlider: U.animateSlider,
              releaseScroll: U.releaseScroll,
            },
          });
        },
        on: {
          init: function init(e) {
            !e.params.mousewheel.enabled &&
              e.params.cssMode &&
              e.mousewheel.disable(),
              e.params.mousewheel.enabled && e.mousewheel.enable();
          },
          destroy: function destroy(e) {
            e.params.cssMode && e.mousewheel.enable(),
              e.mousewheel.enabled && e.mousewheel.disable();
          },
        },
      },
      {
        name: "navigation",
        params: {
          navigation: {
            nextEl: null,
            prevEl: null,
            hideOnClick: !1,
            disabledClass: "swiper-button-disabled",
            hiddenClass: "swiper-button-hidden",
            lockClass: "swiper-button-lock",
          },
        },
        create: function create() {
          M(this, {
            navigation: t({}, K),
          });
        },
        on: {
          init: function init(e) {
            e.navigation.init(), e.navigation.update();
          },
          toEdge: function toEdge(e) {
            e.navigation.update();
          },
          fromEdge: function fromEdge(e) {
            e.navigation.update();
          },
          destroy: function destroy(e) {
            e.navigation.destroy();
          },
          click: function click(e, t) {
            var i,
              s = e.navigation,
              a = s.$nextEl,
              r = s.$prevEl;
            !e.params.navigation.hideOnClick ||
              m(t.target).is(r) ||
              m(t.target).is(a) ||
              (a
                ? (i = a.hasClass(e.params.navigation.hiddenClass))
                : r && (i = r.hasClass(e.params.navigation.hiddenClass)),
              !0 === i ? e.emit("navigationShow") : e.emit("navigationHide"),
              a && a.toggleClass(e.params.navigation.hiddenClass),
              r && r.toggleClass(e.params.navigation.hiddenClass));
          },
        },
      },
      {
        name: "pagination",
        params: {
          pagination: {
            el: null,
            bulletElement: "span",
            clickable: !1,
            hideOnClick: !1,
            renderBullet: null,
            renderProgressbar: null,
            renderFraction: null,
            renderCustom: null,
            progressbarOpposite: !1,
            type: "bullets",
            dynamicBullets: !1,
            dynamicMainBullets: 1,
            formatFractionCurrent: function formatFractionCurrent(e) {
              return e;
            },
            formatFractionTotal: function formatFractionTotal(e) {
              return e;
            },
            bulletClass: "swiper-pagination-bullet",
            bulletActiveClass: "swiper-pagination-bullet-active",
            modifierClass: "swiper-pagination-",
            currentClass: "swiper-pagination-current",
            totalClass: "swiper-pagination-total",
            hiddenClass: "swiper-pagination-hidden",
            progressbarFillClass: "swiper-pagination-progressbar-fill",
            progressbarOppositeClass: "swiper-pagination-progressbar-opposite",
            clickableClass: "swiper-pagination-clickable",
            lockClass: "swiper-pagination-lock",
          },
        },
        create: function create() {
          M(this, {
            pagination: t(
              {
                dynamicBulletIndex: 0,
              },
              Z
            ),
          });
        },
        on: {
          init: function init(e) {
            e.pagination.init(), e.pagination.render(), e.pagination.update();
          },
          activeIndexChange: function activeIndexChange(e) {
            (e.params.loop || void 0 === e.snapIndex) && e.pagination.update();
          },
          snapIndexChange: function snapIndexChange(e) {
            e.params.loop || e.pagination.update();
          },
          slidesLengthChange: function slidesLengthChange(e) {
            e.params.loop && (e.pagination.render(), e.pagination.update());
          },
          snapGridLengthChange: function snapGridLengthChange(e) {
            e.params.loop || (e.pagination.render(), e.pagination.update());
          },
          destroy: function destroy(e) {
            e.pagination.destroy();
          },
          click: function click(e, t) {
            e.params.pagination.el &&
              e.params.pagination.hideOnClick &&
              e.pagination.$el.length > 0 &&
              !m(t.target).hasClass(e.params.pagination.bulletClass) &&
              (!0 === e.pagination.$el.hasClass(e.params.pagination.hiddenClass)
                ? e.emit("paginationShow")
                : e.emit("paginationHide"),
              e.pagination.$el.toggleClass(e.params.pagination.hiddenClass));
          },
        },
      },
      {
        name: "scrollbar",
        params: {
          scrollbar: {
            el: null,
            dragSize: "auto",
            hide: !1,
            draggable: !1,
            snapOnRelease: !0,
            lockClass: "swiper-scrollbar-lock",
            dragClass: "swiper-scrollbar-drag",
          },
        },
        create: function create() {
          M(this, {
            scrollbar: t(
              {
                isTouched: !1,
                timeout: null,
                dragTimeout: null,
              },
              J
            ),
          });
        },
        on: {
          init: function init(e) {
            e.scrollbar.init(),
              e.scrollbar.updateSize(),
              e.scrollbar.setTranslate();
          },
          update: function update(e) {
            e.scrollbar.updateSize();
          },
          resize: function resize(e) {
            e.scrollbar.updateSize();
          },
          observerUpdate: function observerUpdate(e) {
            e.scrollbar.updateSize();
          },
          setTranslate: function setTranslate(e) {
            e.scrollbar.setTranslate();
          },
          setTransition: function setTransition(e, t) {
            e.scrollbar.setTransition(t);
          },
          destroy: function destroy(e) {
            e.scrollbar.destroy();
          },
        },
      },
      {
        name: "parallax",
        params: {
          parallax: {
            enabled: !1,
          },
        },
        create: function create() {
          M(this, {
            parallax: t({}, Q),
          });
        },
        on: {
          beforeInit: function beforeInit(e) {
            e.params.parallax.enabled &&
              ((e.params.watchSlidesProgress = !0),
              (e.originalParams.watchSlidesProgress = !0));
          },
          init: function init(e) {
            e.params.parallax.enabled && e.parallax.setTranslate();
          },
          setTranslate: function setTranslate(e) {
            e.params.parallax.enabled && e.parallax.setTranslate();
          },
          setTransition: function setTransition(e, t) {
            e.params.parallax.enabled && e.parallax.setTransition(t);
          },
        },
      },
      {
        name: "zoom",
        params: {
          zoom: {
            enabled: !1,
            maxRatio: 3,
            minRatio: 1,
            toggle: !0,
            containerClass: "swiper-zoom-container",
            zoomedSlideClass: "swiper-slide-zoomed",
          },
        },
        create: function create() {
          var e = this;
          M(e, {
            zoom: t(
              {
                enabled: !1,
                scale: 1,
                currentScale: 1,
                isScaling: !1,
                gesture: {
                  $slideEl: void 0,
                  slideWidth: void 0,
                  slideHeight: void 0,
                  $imageEl: void 0,
                  $imageWrapEl: void 0,
                  maxRatio: 3,
                },
                image: {
                  isTouched: void 0,
                  isMoved: void 0,
                  currentX: void 0,
                  currentY: void 0,
                  minX: void 0,
                  minY: void 0,
                  maxX: void 0,
                  maxY: void 0,
                  width: void 0,
                  height: void 0,
                  startX: void 0,
                  startY: void 0,
                  touchesStart: {},
                  touchesCurrent: {},
                },
                velocity: {
                  x: void 0,
                  y: void 0,
                  prevPositionX: void 0,
                  prevPositionY: void 0,
                  prevTime: void 0,
                },
              },
              ee
            ),
          });
          var i = 1;
          Object.defineProperty(e.zoom, "scale", {
            get: function get() {
              return i;
            },
            set: function set(t) {
              if (i !== t) {
                var s = e.zoom.gesture.$imageEl
                    ? e.zoom.gesture.$imageEl[0]
                    : void 0,
                  a = e.zoom.gesture.$slideEl
                    ? e.zoom.gesture.$slideEl[0]
                    : void 0;
                e.emit("zoomChange", t, s, a);
              }

              i = t;
            },
          });
        },
        on: {
          init: function init(e) {
            e.params.zoom.enabled && e.zoom.enable();
          },
          destroy: function destroy(e) {
            e.zoom.disable();
          },
          touchStart: function touchStart(e, t) {
            e.zoom.enabled && e.zoom.onTouchStart(t);
          },
          touchEnd: function touchEnd(e, t) {
            e.zoom.enabled && e.zoom.onTouchEnd(t);
          },
          doubleTap: function doubleTap(e, t) {
            e.params.zoom.enabled &&
              e.zoom.enabled &&
              e.params.zoom.toggle &&
              e.zoom.toggle(t);
          },
          transitionEnd: function transitionEnd(e) {
            e.zoom.enabled && e.params.zoom.enabled && e.zoom.onTransitionEnd();
          },
          slideChange: function slideChange(e) {
            e.zoom.enabled &&
              e.params.zoom.enabled &&
              e.params.cssMode &&
              e.zoom.onTransitionEnd();
          },
        },
      },
      {
        name: "lazy",
        params: {
          lazy: {
            enabled: !1,
            loadPrevNext: !1,
            loadPrevNextAmount: 1,
            loadOnTransitionStart: !1,
            elementClass: "swiper-lazy",
            loadingClass: "swiper-lazy-loading",
            loadedClass: "swiper-lazy-loaded",
            preloaderClass: "swiper-lazy-preloader",
          },
        },
        create: function create() {
          M(this, {
            lazy: t(
              {
                initialImageLoaded: !1,
              },
              te
            ),
          });
        },
        on: {
          beforeInit: function beforeInit(e) {
            e.params.lazy.enabled &&
              e.params.preloadImages &&
              (e.params.preloadImages = !1);
          },
          init: function init(e) {
            e.params.lazy.enabled &&
              !e.params.loop &&
              0 === e.params.initialSlide &&
              e.lazy.load();
          },
          scroll: function scroll(e) {
            e.params.freeMode && !e.params.freeModeSticky && e.lazy.load();
          },
          resize: function resize(e) {
            e.params.lazy.enabled && e.lazy.load();
          },
          scrollbarDragMove: function scrollbarDragMove(e) {
            e.params.lazy.enabled && e.lazy.load();
          },
          transitionStart: function transitionStart(e) {
            e.params.lazy.enabled &&
              (e.params.lazy.loadOnTransitionStart ||
                (!e.params.lazy.loadOnTransitionStart &&
                  !e.lazy.initialImageLoaded)) &&
              e.lazy.load();
          },
          transitionEnd: function transitionEnd(e) {
            e.params.lazy.enabled &&
              !e.params.lazy.loadOnTransitionStart &&
              e.lazy.load();
          },
          slideChange: function slideChange(e) {
            e.params.lazy.enabled && e.params.cssMode && e.lazy.load();
          },
        },
      },
      {
        name: "controller",
        params: {
          controller: {
            control: void 0,
            inverse: !1,
            by: "slide",
          },
        },
        create: function create() {
          M(this, {
            controller: t(
              {
                control: this.params.controller.control,
              },
              ie
            ),
          });
        },
        on: {
          update: function update(e) {
            e.controller.control &&
              e.controller.spline &&
              ((e.controller.spline = void 0), delete e.controller.spline);
          },
          resize: function resize(e) {
            e.controller.control &&
              e.controller.spline &&
              ((e.controller.spline = void 0), delete e.controller.spline);
          },
          observerUpdate: function observerUpdate(e) {
            e.controller.control &&
              e.controller.spline &&
              ((e.controller.spline = void 0), delete e.controller.spline);
          },
          setTranslate: function setTranslate(e, t, i) {
            e.controller.control && e.controller.setTranslate(t, i);
          },
          setTransition: function setTransition(e, t, i) {
            e.controller.control && e.controller.setTransition(t, i);
          },
        },
      },
      {
        name: "a11y",
        params: {
          a11y: {
            enabled: !0,
            notificationClass: "swiper-notification",
            prevSlideMessage: "Previous slide",
            nextSlideMessage: "Next slide",
            firstSlideMessage: "This is the first slide",
            lastSlideMessage: "This is the last slide",
            paginationBulletMessage: "Go to slide {{index}}",
          },
        },
        create: function create() {
          M(this, {
            a11y: t(
              t({}, se),
              {},
              {
                liveRegion: m(
                  '<span class="' +
                    this.params.a11y.notificationClass +
                    '" aria-live="assertive" aria-atomic="true"></span>'
                ),
              }
            ),
          });
        },
        on: {
          init: function init(e) {
            e.params.a11y.enabled && (e.a11y.init(), e.a11y.updateNavigation());
          },
          toEdge: function toEdge(e) {
            e.params.a11y.enabled && e.a11y.updateNavigation();
          },
          fromEdge: function fromEdge(e) {
            e.params.a11y.enabled && e.a11y.updateNavigation();
          },
          paginationUpdate: function paginationUpdate(e) {
            e.params.a11y.enabled && e.a11y.updatePagination();
          },
          destroy: function destroy(e) {
            e.params.a11y.enabled && e.a11y.destroy();
          },
        },
      },
      {
        name: "history",
        params: {
          history: {
            enabled: !1,
            replaceState: !1,
            key: "slides",
          },
        },
        create: function create() {
          M(this, {
            history: t({}, ae),
          });
        },
        on: {
          init: function init(e) {
            e.params.history.enabled && e.history.init();
          },
          destroy: function destroy(e) {
            e.params.history.enabled && e.history.destroy();
          },
          transitionEnd: function transitionEnd(e) {
            e.history.initialized &&
              e.history.setHistory(e.params.history.key, e.activeIndex);
          },
          slideChange: function slideChange(e) {
            e.history.initialized &&
              e.params.cssMode &&
              e.history.setHistory(e.params.history.key, e.activeIndex);
          },
        },
      },
      {
        name: "hash-navigation",
        params: {
          hashNavigation: {
            enabled: !1,
            replaceState: !1,
            watchState: !1,
          },
        },
        create: function create() {
          M(this, {
            hashNavigation: t(
              {
                initialized: !1,
              },
              re
            ),
          });
        },
        on: {
          init: function init(e) {
            e.params.hashNavigation.enabled && e.hashNavigation.init();
          },
          destroy: function destroy(e) {
            e.params.hashNavigation.enabled && e.hashNavigation.destroy();
          },
          transitionEnd: function transitionEnd(e) {
            e.hashNavigation.initialized && e.hashNavigation.setHash();
          },
          slideChange: function slideChange(e) {
            e.hashNavigation.initialized &&
              e.params.cssMode &&
              e.hashNavigation.setHash();
          },
        },
      },
      {
        name: "autoplay",
        params: {
          autoplay: {
            enabled: !1,
            delay: 3e3,
            waitForTransition: !0,
            disableOnInteraction: !0,
            stopOnLastSlide: !1,
            reverseDirection: !1,
          },
        },
        create: function create() {
          M(this, {
            autoplay: t(
              t({}, ne),
              {},
              {
                running: !1,
                paused: !1,
              }
            ),
          });
        },
        on: {
          init: function init(e) {
            e.params.autoplay.enabled &&
              (e.autoplay.start(),
              r().addEventListener(
                "visibilitychange",
                e.autoplay.onVisibilityChange
              ));
          },
          beforeTransitionStart: function beforeTransitionStart(e, t, i) {
            e.autoplay.running &&
              (i || !e.params.autoplay.disableOnInteraction
                ? e.autoplay.pause(t)
                : e.autoplay.stop());
          },
          sliderFirstMove: function sliderFirstMove(e) {
            e.autoplay.running &&
              (e.params.autoplay.disableOnInteraction
                ? e.autoplay.stop()
                : e.autoplay.pause());
          },
          touchEnd: function touchEnd(e) {
            e.params.cssMode &&
              e.autoplay.paused &&
              !e.params.autoplay.disableOnInteraction &&
              e.autoplay.run();
          },
          destroy: function destroy(e) {
            e.autoplay.running && e.autoplay.stop(),
              r().removeEventListener(
                "visibilitychange",
                e.autoplay.onVisibilityChange
              );
          },
        },
      },
      {
        name: "effect-fade",
        params: {
          fadeEffect: {
            crossFade: !1,
          },
        },
        create: function create() {
          M(this, {
            fadeEffect: t({}, le),
          });
        },
        on: {
          beforeInit: function beforeInit(e) {
            if ("fade" === e.params.effect) {
              e.classNames.push(e.params.containerModifierClass + "fade");
              var t = {
                slidesPerView: 1,
                slidesPerColumn: 1,
                slidesPerGroup: 1,
                watchSlidesProgress: !0,
                spaceBetween: 0,
                virtualTranslate: !0,
              };
              S(e.params, t), S(e.originalParams, t);
            }
          },
          setTranslate: function setTranslate(e) {
            "fade" === e.params.effect && e.fadeEffect.setTranslate();
          },
          setTransition: function setTransition(e, t) {
            "fade" === e.params.effect && e.fadeEffect.setTransition(t);
          },
        },
      },
      {
        name: "effect-cube",
        params: {
          cubeEffect: {
            slideShadows: !0,
            shadow: !0,
            shadowOffset: 20,
            shadowScale: 0.94,
          },
        },
        create: function create() {
          M(this, {
            cubeEffect: t({}, oe),
          });
        },
        on: {
          beforeInit: function beforeInit(e) {
            if ("cube" === e.params.effect) {
              e.classNames.push(e.params.containerModifierClass + "cube"),
                e.classNames.push(e.params.containerModifierClass + "3d");
              var t = {
                slidesPerView: 1,
                slidesPerColumn: 1,
                slidesPerGroup: 1,
                watchSlidesProgress: !0,
                resistanceRatio: 0,
                spaceBetween: 0,
                centeredSlides: !1,
                virtualTranslate: !0,
              };
              S(e.params, t), S(e.originalParams, t);
            }
          },
          setTranslate: function setTranslate(e) {
            "cube" === e.params.effect && e.cubeEffect.setTranslate();
          },
          setTransition: function setTransition(e, t) {
            "cube" === e.params.effect && e.cubeEffect.setTransition(t);
          },
        },
      },
      {
        name: "effect-flip",
        params: {
          flipEffect: {
            slideShadows: !0,
            limitRotation: !0,
          },
        },
        create: function create() {
          M(this, {
            flipEffect: t({}, de),
          });
        },
        on: {
          beforeInit: function beforeInit(e) {
            if ("flip" === e.params.effect) {
              e.classNames.push(e.params.containerModifierClass + "flip"),
                e.classNames.push(e.params.containerModifierClass + "3d");
              var t = {
                slidesPerView: 1,
                slidesPerColumn: 1,
                slidesPerGroup: 1,
                watchSlidesProgress: !0,
                spaceBetween: 0,
                virtualTranslate: !0,
              };
              S(e.params, t), S(e.originalParams, t);
            }
          },
          setTranslate: function setTranslate(e) {
            "flip" === e.params.effect && e.flipEffect.setTranslate();
          },
          setTransition: function setTransition(e, t) {
            "flip" === e.params.effect && e.flipEffect.setTransition(t);
          },
        },
      },
      {
        name: "effect-coverflow",
        params: {
          coverflowEffect: {
            rotate: 50,
            stretch: 0,
            depth: 100,
            scale: 1,
            modifier: 1,
            slideShadows: !0,
          },
        },
        create: function create() {
          M(this, {
            coverflowEffect: t({}, he),
          });
        },
        on: {
          beforeInit: function beforeInit(e) {
            "coverflow" === e.params.effect &&
              (e.classNames.push(e.params.containerModifierClass + "coverflow"),
              e.classNames.push(e.params.containerModifierClass + "3d"),
              (e.params.watchSlidesProgress = !0),
              (e.originalParams.watchSlidesProgress = !0));
          },
          setTranslate: function setTranslate(e) {
            "coverflow" === e.params.effect && e.coverflowEffect.setTranslate();
          },
          setTransition: function setTransition(e, t) {
            "coverflow" === e.params.effect &&
              e.coverflowEffect.setTransition(t);
          },
        },
      },
      {
        name: "thumbs",
        params: {
          thumbs: {
            swiper: null,
            multipleActiveThumbs: !0,
            autoScrollOffset: 0,
            slideThumbActiveClass: "swiper-slide-thumb-active",
            thumbsContainerClass: "swiper-container-thumbs",
          },
        },
        create: function create() {
          M(this, {
            thumbs: t(
              {
                swiper: null,
                initialized: !1,
              },
              pe
            ),
          });
        },
        on: {
          beforeInit: function beforeInit(e) {
            var t = e.params.thumbs;
            t && t.swiper && (e.thumbs.init(), e.thumbs.update(!0));
          },
          slideChange: function slideChange(e) {
            e.thumbs.swiper && e.thumbs.update();
          },
          update: function update(e) {
            e.thumbs.swiper && e.thumbs.update();
          },
          resize: function resize(e) {
            e.thumbs.swiper && e.thumbs.update();
          },
          observerUpdate: function observerUpdate(e) {
            e.thumbs.swiper && e.thumbs.update();
          },
          setTransition: function setTransition(e, t) {
            var i = e.thumbs.swiper;
            i && i.setTransition(t);
          },
          beforeDestroy: function beforeDestroy(e) {
            var t = e.thumbs.swiper;
            t && e.thumbs.swiperCreated && t && t.destroy();
          },
        },
      },
    ];
  return W.use(ue), W;
});
// abctesting

var _ref,
  _ref2,
  _ref3,
  _ref4,
  _ref5,
  _ref6,
  _ref7,
  _ref8,
  _ref9,
  _ref10,
  _ref11,
  _ref12,
  _ref13,
  _ref14,
  _ref15,
  _ref16,
  _ref17,
  _ref18,
  _ref19,
  _ref20,
  _ref21,
  _ref22,
  _ref23,
  _ref24,
  _ref25,
  _ref26,
  _ref27,
  _ref28,
  _ref29,
  _ref30,
  _ref31,
  _ref32,
  _ref33,
  _ref34,
  _ref35,
  _ref36,
  _ref37,
  _ref38,
  _ref39,
  _ref40,
  _ref41,
  _ref42,
  _ref43,
  _ref44,
  _ref45,
  _ref46,
  _ref47,
  _ref48,
  _ref49,
  _ref50,
  _ref51,
  _ref52,
  _ref53,
  _ref54,
  _ref55,
  _ref56,
  _ref57,
  _ref58,
  _ref59;

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true,
    });
  } else {
    obj[key] = value;
  }
  return obj;
}

const tagA = (url, text = false) => {
  return `<a href="${url}" target="_blank" >${text ? text : url}</a>`;
};

let modalx = [
  {
    product: 1,
    category: 1,
    model: 1,
  },
  {
    product: 2,
    category: 2,
    model: 2,
  },
  {
    product: 3,
    category: 3,
    model: 3,
  },
  {
    product: 4,
    category: 4,
    model: 4,
  },
  {
    product: 5,
    category: 5,
    model: 5,
  },
  {
    product: 6,
    category: 6,
    model: 6,
  },
  {
    product: 7,
    category: 7,
    model: 7,
  },
  {
    product: 8,
    category: 8,
    model: 8,
  },
  {
    product: 9,
    category: 9,
    model: 9,
  },
  {
    product: 10,
    category: 10,
    model: 10,
  },
  {
    product: 11,
    category: 11,
    model: 11,
  },
  {
    product: 12,
    category: 12,
    model: 12,
  },
  {
    product: 13,
    category: 13,
    model: 13,
  },
  {
    product: 14,
    category: 14,
    model: 14,
  },
  {
    product: 15,
    category: 15,
    model: 15,
  },
  {
    product: 16,
    category: 16,
    model: 16,
  },
  {
    product: 17,
    category: 17,
    model: 17,
  },
  {
    product: 18,
    category: 18,
    model: 18,
  },
  {
    product: 19,
    category: 19,
    model: 19,
  },
  {
    product: 20,
    category: 20,
    model: 20,
  },
  {
    product: 21,
    category: 21,
    model: 21,
  },
  {
    product: 22,
    category: 22,
    model: 22,
  },
  {
    product: 23,
    category: 23,
    model: 23,
  },
  {
    product: 24,
    category: 24,
    model: 24,
  },
  {
    product: 25,
    category: 25,
    model: 25,
  },
  {
    product: 26,
    category: 26,
    model: 26,
  },
  {
    product: 27,
    category: 27,
    model: 27,
  },
  {
    product: 28,
    category: 28,
    model: 28,
  },
  {
    product: 29,
    category: 29,
    model: 29,
  },
  {
    product: 30,
    category: 30,
    model: 30,
  },
  {
    product: 31,
    category: 31,
    model: 31,
  },
  {
    product: 32,
    category: 32,
    model: 32,
  },
  {
    product: 33,
    category: 33,
    model: 33,
  },
  {
    product: 34,
    category: 34,
    model: 34,
  },
  {
    product: 35,
    category: 35,
    model: 35,
  },
  {
    product: 36,
    category: 36,
    model: 36,
  },
  {
    product: 37,
    category: 37,
    model: 37,
  },
  {
    product: 38,
    category: 38,
    model: 38,
  },
  {
    product: 39,
    category: 39,
    model: 39,
  },
];

let modalx2 = {}

let modalItems = [
  { modalName: "modalx", modal: modalx },
  { modalName: "modalx-2", modal: modalx2 },
];

let products = [
  {
    key: 1,
    name: "ทีวี",
  },
  {
    key: 2,
    name: "ทีวี",
  },
  {
    key: 3,
    name: "ทีวี",
  },
  {
    key: 4,
    name: "ทีวี",
  },
  {
    key: 5,
    name: "ทีวี",
  },
  {
    key: 6,
    name: "ทีวี",
  },
  {
    key: 7,
    name: "ทีวี",
  },
  {
    key: 8,
    name: "ทีวี",
  },
  {
    key: 9,
    name: "ทีวี",
  },
  {
    key: 10,
    name: "ทีวี",
  },
  {
    key: 11,
    name: "ทีวี",
  },
  {
    key: 12,
    name: "ทีวี",
  },
  {
    key: 13,
    name: "ทีวี",
  },
  {
    key: 14,
    name: "ทีวี",
  },
  {
    key: 15,
    name: "ทีวี",
  },
  {
    key: 16,
    name: "ทีวี",
  },
  {
    key: 17,
    name: "ทีวี",
  },
  {
    key: 18,
    name: "ทีวี",
  },
  {
    key: 19,
    name: "ทีวี",
  },
  {
    key: 20,
    name: "ทีวี",
  },
  {
    key: 21,
    name: "ทีวี",
  },
  {
    key: 22,
    name: "ทีวี",
  },
  {
    key: 23,
    name: "ทีวี",
  },
  {
    key: 24,
    name: "ทีวี",
  },
  {
    key: 25,
    name: "ทีวี",
  },
  {
    key: 26,
    name: "ทีวี",
  },
  {
    key: 27,
    name: "ทีวี",
  },
  {
    key: 28,
    name: "ทีวี",
  },
  {
    key: 29,
    name: "ทีวี",
  },
  {
    key: 30,
    name: "ทีวี",
  },
  {
    key: 31,
    name: "ทีวี",
  },
  {
    key: 32,
    name: "ทีวี",
  },
  {
    key: 33,
    name: "ทีวี",
  },
  {
    key: 34,
    name: "ทีวี",
  },
  {
    key: 35,
    name: "ทีวี",
  },
  {
    key: 36,
    name: "ทีวี",
  },
  {
    key: 37,
    name: "ตู้เย็น",
  },
  {
    key: 38,
    name: "ตู้เย็น",
  },
  {
    key: 39,
    name: "เครื่องทำความสะอาดและรีดผ้า",
  },
];

let categories = [
  {
    key: 1,
    name: "Q950TS QLED 8K (2020)",
  },
  {
    key: 2,
    name: "Q950TS QLED 8K (2020)",
  },
  {
    key: 3,
    name: "Q950TS QLED 8K (2020)",
  },
  {
    key: 4,
    name: "Q800T QLED 8K (2020)",
  },
  {
    key: 5,
    name: "Q800T QLED 8K (2020)",
  },
  {
    key: 6,
    name: "Q800T QLED 8K (2020)",
  },
  {
    key: 7,
    name: "Q900R QLED 8K (2019)",
  },
  {
    key: 8,
    name: "Q900R QLED 8K (2019)",
  },
  {
    key: 9,
    name: "Q900R QLED 8K (2019)",
  },
  {
    key: 10,
    name: "Q900R QLED 8K (2019)",
  },
  {
    key: 11,
    name: "Q900R QLED 8K (2019)",
  },
  {
    key: 12,
    name: "Q80T QLED 4K (2020)",
  },
  {
    key: 13,
    name: "Q80T QLED 4K (2020)",
  },
  {
    key: 14,
    name: "Q80T QLED 4K (2020)",
  },
  {
    key: 15,
    name: "Q80T QLED 4K (2020)",
  },
  {
    key: 16,
    name: "Q95T QLED 4K (2020)",
  },
  {
    key: 17,
    name: "Q95T QLED 4K (2020)",
  },
  {
    key: 18,
    name: "Q70T QLED 4K (2020)",
  },
  {
    key: 19,
    name: "Q70T QLED 4K (2020)",
  },
  {
    key: 20,
    name: "Q70T QLED 4K (2020)",
  },
  {
    key: 21,
    name: "Q70T QLED 4K (2020)",
  },
  {
    key: 22,
    name: "Q65T QLED 4K (2020)",
  },
  {
    key: 23,
    name: "Q65T QLED 4K (2020)",
  },
  {
    key: 24,
    name: "Q60T QLED 4K (2020)",
  },
  {
    key: 25,
    name: "Q60T QLED 4K (2020)",
  },
  {
    key: 26,
    name: "Q60T QLED 4K (2020)",
  },
  {
    key: 27,
    name: "Q60T QLED 4K (2020)",
  },
  {
    key: 28,
    name: "Q60T QLED 4K (2020)",
  },
  {
    key: 29,
    name: "Q60T QLED 4K (2020)",
  },
  {
    key: 30,
    name: "Q60T QLED 4K (2020)",
  },
  {
    key: 31,
    name: "The Frame Smart 4K (2020)",
  },
  {
    key: 32,
    name: "The Frame Smart 4K (2020)",
  },
  {
    key: 33,
    name: "The Frame Smart 4K (2020)",
  },
  {
    key: 34,
    name: "The Serif 4K Smart TV (2020)",
  },
  {
    key: 35,
    name: "The Serif 4K Smart TV (2020)",
  },
  {
    key: 36,
    name: "The Serif 4K Smart TV (2020)",
  },
  {
    key: 37,
    name: "Family Hub",
  },
  {
    key: 38,
    name: "Family Hub",
  },
  {
    key: 38,
    name: "AirDresser",
  },
];

let models = [
  {
    key: 1,
    name: "QA65Q950TSKXXT",
  },
  {
    key: 2,
    name: "QA75Q950TSKXXT",
  },
  {
    key: 3,
    name: "QA85Q950TSKXXT",
  },
  {
    key: 4,
    name: "QA65Q800TAKXXT",
  },
  {
    key: 5,
    name: "QA75Q800TAKXXT",
  },
  {
    key: 6,
    name: "QA82Q800TAKXXT",
  },
  {
    key: 7,
    name: "QA55Q900RBKXXT",
  },
  {
    key: 8,
    name: "QA65Q900RBKXXT",
  },
  {
    key: 9,
    name: "QA75Q900RBKXXT",
  },
  {
    key: 10,
    name: "QA82Q900RBKXXT",
  },
  {
    key: 11,
    name: "QA98Q900RBKXXT",
  },
  {
    key: 12,
    name: "QA55Q80TAKXXT",
  },
  {
    key: 13,
    name: "QA65Q80TAKXXT",
  },
  {
    key: 14,
    name: "QA75Q80TAKXXT",
  },
  {
    key: 15,
    name: "QA85Q80TAKXXT",
  },
  {
    key: 16,
    name: "QA65Q95TAKXXT",
  },
  {
    key: 17,
    name: "QA75Q95TAKXXT",
  },
  {
    key: 18,
    name: "QA55Q70TAKXXT",
  },
  {
    key: 19,
    name: "QA65Q70TAKXXT",
  },
  {
    key: 20,
    name: "QA75Q70TAKXXT",
  },
  {
    key: 21,
    name: "QA85Q70TAKXXT",
  },
  {
    key: 22,
    name: "QA55Q65TAKXXT",
  },
  {
    key: 23,
    name: "QA65Q65TAKXXT",
  },
  {
    key: 24,
    name: "QA43Q60TAKXXT",
  },
  {
    key: 25,
    name: "QA50Q60TAKXXT",
  },
  {
    key: 26,
    name: "QA55Q60TAKXXT",
  },
  {
    key: 27,
    name: "QA58Q60TAKXXT",
  },
  {
    key: 28,
    name: "QA65Q60TAKXXT",
  },
  {
    key: 29,
    name: "QA75Q60TAKXXT",
  },
  {
    key: 30,
    name: "QA85Q60TAKXXT",
  },
  {
    key: 31,
    name: "QA32LS03TBKXXT",
  },
  {
    key: 32,
    name: "QA55LS03TAKXXT",
  },
  {
    key: 33,
    name: "QA65LS03TAKXXT",
  },
  {
    key: 34,
    name: "QA43LS01TAKXXT",
  },
  {
    key: 35,
    name: "QA55LS01TAKXXT",
  },
  {
    key: 36,
    name: "QA43LS05TAKXXT",
  },
  {
    key: 37,
    name: "RF56N9740SG/ST ",
  },
  {
    key: 38,
    name: "RS64T5F01B4/ST",
  },
  {
    key: 39,
    name: "DF60R8600CG/ST",
  },
];
let items = {
  mobile: {
    Unbox: [
      {
        no: "01",
        head: "-",
        text: "-",
        image: "unbox-01_01",
        image2: "unbox-mb-01_01",
        title: "แกะกล่องเครื่องใหม่",
      },
      {
        no: "02",
        head: "-",
        text: "-",
        image: "unbox-01_02",
        image2: "unbox-mb-01_02",
        title: "At Your Service",
        mapName: "at-service",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง At Your Service เพื่อเป็นข้อมูล ประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "serviceat",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "03",
        head: "-",
        text: "-",
        image: "unbox-01_03",
        image2: "unbox-mb-01_03",
        title: "Samsung Members",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      },
      // {
      //   no: "04",
      //   head: "-",
      //   text: "-",
      //   image: "unbox-01_04",
      //   image2: "unbox-mb-01_04",
      //   url: "serice-btn",
      //   title: "Extended Warranty",
      //   condition:
      //     "1. ลูกค้าต้องลงทะเบียนผ่านเว็บไซต์ SSTHWARRANTY  เพื่อเอาประกันเท่านั้น <br> 2. ระยะเวลาที่ขยายการรับประกันสินค้า ขึ้นอยู่กับรุ่นของสินค้าและโปรโมชั่นขณะนั้น <br> 3. การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      // },
    ],
    Inquiry: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-02_01",
        image2: "unbox-mb-02_01",
        title: "เครื่องใหม่ ยังไม่ชิน / ต้องการแก้ไขปัญหาเริ่มต้น? ",
      },
      {
        no: "05",
        head: "-",
        text: "-",

        image: "unbox-02_02",
        image2: "unbox-mb-02_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",

        image: "unbox-02_03",
        image2: "unbox-mb-02_03",
        title: "Live Chat",
        mapName: "livechat",
        imageLinks: [
          {
            name: "livechat",
            url:
              "https://livechat.support.samsung.com/Customer_mys/CustomerChat_mys.aspx?roomNo=29352510&amp;encrypt=DA7538FE760DCE56801E33CA5AC8840F",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",

        image: "unbox-02_04_mb",
        image2: "unbox-mb-02_04_mb",
        title: "Remote Service",
      },
      // {
      //   no: "05",
      //   head: "-",
      //   text: "-",

      //   image: "unbox-02_05",
      //   image2: "unbox-mb-02_05",
      //   title: "Visual Support",
      // },
      // {
      //   no: "05",
      //   head: "-",
      //   text: "-",
      //   image: "unbox-02_06",
      //   image2: "unbox-mb-02_05",
      //   title: "Private Demo Service",
      //   condition:
      //     "1. ลูกค้าสามารถติดต่อขอใช้บริการได้ภายใน 90 วัน นับจากวันที่ลูกค้าซื้อสินค้า โดยอ้างอิงจากใบเสร็จ <br> 2.ลูกค้าต้องเตรียมหลักฐานประกอบการรับบริการ เช่น ใบเสร็จของสินค้าหรือใบรับของจากร้านค้าที่ซื้อมา โดยมีรายละเอียดของผู้ซื้อ ผู้ขายรวมถึงรุ่นของสินค้าที่ซื้อมาอย่างชัดเจน <br> 3.บริการสาธิตการใช้งานผลิตภัณฑ์สำหรับตู้เย็นชนิด Family Hub และ AirDresser ในรุ่นที่กำหนด ให้บริการเฉพาะพื้นที่กรุงเทพฯ และปริมณฑลเท่านั้น",
      //   modalName: "md-1",
      //   modalTitle: "Test by arm",
      //   modalType: "search",
      //   modal: [
      //     {
      //       product: 1,
      //       category: 1,
      //       model: 1,
      //     },
      //     {
      //       product: 2,
      //       category: 1,
      //       model: 1,
      //     },
      //     {
      //       product: 1,
      //       category: 1,
      //       model: 1,
      //     },
      //   ],
      // },
      // {
      //   no: "05",
      //   head: "-",
      //   text: "-",

      //   image: "unbox-02_07",
      //   image2: "unbox-mb-02_05",
      //   title: "e-Demo Service",
      // },
    ],
    Repair: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-03_01",
        image2: "unbox-mb-03_01",
        title: "พบช่องทางที่หลากหลายในการแจ้งซ่อมผลิตภัณฑ์",
      },
      {
        no: "05",
        head: "-",
        text: "-",

        image: "unbox-03_02",
        image2: "unbox-mb-03_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",

        image: "unbox-03_03",
        image2: "unbox-mb-03_03",
        title: "At Your Service",
        mapName: "atservice",
        imageLinks: [
          {
            name: "atservice",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",

        image: "unbox-03_04",
        image2: "unbox-mb-03_04",
        title: "Samsung Members",
        condition: "แอปพลิเคชันนี้รองรับเฉพาะโทรศัพท์ซัมซุงเท่านั้น",
      },
      {
        no: "05",
        head: "-",
        text: "-",

        image: "unbox-03_05",
        image2: "unbox-mb-03_05",
        title: "Live Chat",
      },
      // {
      //   no: "05",
      //   head: "-",
      //   text: "-",

      //   image: "unbox-03_06",
      //   image2: "unbox-mb-02_06",
      //   title: "For Home appliance",
      //   condition:
      //     "บริการนี้สำหรับเครื่องใช้ไฟฟ้าประเภทตู้เย็น เครื่องซักผ้า เครื่องปรับอากาศ และทีวีขนาด 32 นิ้วขึ้นไปเท่านั้น ",
      // },
    ],
    Extra: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-04_02",
        image2: "unbox-mb-04_02",
        title: "Extra benefit ",
        condition: `ตรวจสอบเงื่อนไขเพิ่มเติมที่ ${tagA(
          "https://www.samsung.com/th/butler"
        )} `,
        mapName: "extra",
        imageLinks: [
          {
            name: "extra",
            url: "https://www.samsung.com/th/butler/",
          },
        ],
      },
    ],
  },
  tv: {
    Unbox: [
      {
        no: "01",
        head: "-",
        text: "-",
        image: "unbox-01_01",
        image2: "unbox-mb-01_01",
        title: "แกะกล่องเครื่องใหม่",
      },
      {
        no: "02",
        head: "-",
        text: "-",
        image: "unbox-01_02",
        image2: "unbox-mb-01_02",
        title: "At Your Service",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง At Your Service เพื่อเป็นข้อมูล ประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "serviceat",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "03",
        head: "-",
        text: "-",
        image: "unbox-01_03",
        image2: "unbox-mb-01_03",
        title: "Samsung Members",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      },
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-01_04",
        image2: "unbox-mb-01_04",
        url: "serice-btn",
        title: "Extended Warranty",
        mapName: "warranty",
        condition:
          "1. ลูกค้าต้องลงทะเบียนผ่านเว็บไซต์ SSTHWARRANTY  เพื่อเอาประกันเท่านั้น <br> 2. ระยะเวลาที่ขยายการรับประกันสินค้า ขึ้นอยู่กับรุ่นของสินค้าและโปรโมชั่นขณะนั้น <br> 3. การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "warranty",
            url: "https://www.ssthwarranty.com/warranty",
          },
        ],
      },
    ],
    Inquiry: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-02_01",
        image2: "unbox-mb-02_01",
        title: "เครื่องใหม่ ยังไม่ชิน / ต้องการแก้ไขปัญหาเริ่มต้น? ",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_02",
        image2: "unbox-mb-02_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_03",
        image2: "unbox-mb-02_03",
        title: "Live Chat",
        mapName: "livechat",
        imageLinks: [
          {
            name: "livechat",
            url:
              "https://livechat.support.samsung.com/Customer_mys/CustomerChat_mys.aspx?roomNo=29352510&amp;encrypt=DA7538FE760DCE56801E33CA5AC8840F",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_04",
        image2: "unbox-mb-02_04",
        title: "Remote Service",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_05",
        image2: "unbox-mb-02_visual",
        title: "Visual Support",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_06",
        image2: "unbox-mb-02_05",
        title: "Private Demo Service",

        condition:
          "1. ลูกค้าสามารถติดต่อขอใช้บริการได้ภายใน 90 วัน นับจากวันที่ลูกค้าซื้อสินค้า โดยอ้างอิงจากใบเสร็จ <br> 2.ลูกค้าต้องเตรียมหลักฐานประกอบการรับบริการ เช่น ใบเสร็จของสินค้าหรือใบรับของจากร้านค้าที่ซื้อมา โดยมีรายละเอียดของผู้ซื้อ ผู้ขายรวมถึงรุ่นของสินค้าที่ซื้อมาอย่างชัดเจน <br> 3.บริการสาธิตการใช้งานผลิตภัณฑ์สำหรับตู้เย็นชนิด Family Hub และ AirDresser ในรุ่นที่กำหนด ให้บริการเฉพาะพื้นที่กรุงเทพฯ และปริมณฑลเท่านั้น",
        modalType: "search",
        modalName: "private-service",
        // modal: modalx,
        imageLinks: [
          {
            name: "modalversion1",
            modal: "modalx",
          },
        ],
      },
      // {
      //   no: "05",
      //   head: "-",
      //   text: "-",
      //   image: "unbox-02_07",
      //   image2: "unbox-mb-02_05",
      //   title: "e-Demo Service",
      // },
    ],
    Repair: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-03_01",
        image2: "unbox-mb-03_01",
        title: "พบช่องทางที่หลากหลายในการแจ้งซ่อมผลิตภัณฑ์",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_02",
        image2: "unbox-mb-03_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_03",
        image2: "unbox-mb-03_03",
        title: "At Your Service",
        mapName: "atservice",
        imageLinks: [
          {
            name: "atservice",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_04",
        image2: "unbox-mb-03_04",
        title: "Samsung Members",
        condition: "แอปพลิเคชันนี้รองรับเฉพาะโทรศัพท์ซัมซุงเท่านั้น",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_05",
        image2: "unbox-mb-03_05",
        title: "Live Chat",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_06",
        image2: "unbox-mb-03_06",
        title: "For Home appliance",
        condition:
          "บริการนี้สำหรับเครื่องใช้ไฟฟ้าประเภทตู้เย็น เครื่องซักผ้า เครื่องปรับอากาศ และทีวีขนาด 32 นิ้วขึ้นไปเท่านั้น ",
      },
    ],
    Extra: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-04_01",
        image2: "unbox-mb-04_01",
        title: " ",
        title: "Extra benefit ",
        condition:
          "เมื่อซื้อ Samsung Smart TV รุ่นปี 2020 และลงทะเบียนรับสิทธิ์ระหว่างวันที่ 1 ส.ค. 63 – 31 ต.ค. 63",
      },
    ],
  },
  wm: {
    Unbox: [
      {
        no: "01",
        head: "-",
        text: "-",
        image: "unbox-01_01",
        image2: "unbox-mb-01_01",
        title: "แกะกล่องเครื่องใหม่",
      },
      {
        no: "02",
        head: "-",
        text: "-",
        image: "unbox-01_02",
        image2: "unbox-mb-01_02",
        title: "At Your Service",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง At Your Service เพื่อเป็นข้อมูล ประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "serviceat",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "03",
        head: "-",
        text: "-",
        image: "unbox-01_03",
        image2: "unbox-mb-01_03",
        title: "Samsung Members",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      },
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-01_04",
        image2: "unbox-mb-01_04",
        url: "serice-btn",
        title: "Extended Warranty",
        mapName: "warranty",
        condition:
          "1. ลูกค้าต้องลงทะเบียนผ่านเว็บไซต์ SSTHWARRANTY  เพื่อเอาประกันเท่านั้น <br> 2. ระยะเวลาที่ขยายการรับประกันสินค้า ขึ้นอยู่กับรุ่นของสินค้าและโปรโมชั่นขณะนั้น <br> 3. การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "warranty",
            url: "https://www.ssthwarranty.com/warranty",
          },
        ],
      },
    ],
    Inquiry: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-02_01",
        image2: "unbox-mb-02_01",
        title: "เครื่องใหม่ ยังไม่ชิน / ต้องการแก้ไขปัญหาเริ่มต้น? ",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_02",
        image2: "unbox-mb-02_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_03",
        image2: "unbox-mb-02_03",
        title: "Live Chat",
        mapName: "livechat",
        imageLinks: [
          {
            name: "livechat",
            url:
              "https://livechat.support.samsung.com/Customer_mys/CustomerChat_mys.aspx?roomNo=29352510&amp;encrypt=DA7538FE760DCE56801E33CA5AC8840F",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_05",
        image2: "unbox-mb-02_visual",
        title: "Visual Support",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "unbox-02_07",
        image2: "unbox-mb-02_06",
        title: "e-Demo Service",
        mapName: "digital-e-domo-service",
        imageLinks: [
          {
            name: "modalversion",
            modal: "modalx",
          },
          {
            name: "modalversion2",
            modalImage:
              "images/back-popup",
          },
        ],

        imageLinks2: [
          {
            name: "serviceat2",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
    ],
    Repair: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-03_01",
        image2: "unbox-mb-03_01",
        title: "พบช่องทางที่หลากหลายในการแจ้งซ่อมผลิตภัณฑ์",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_02",
        image2: "unbox-mb-03_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_03",
        image2: "unbox-mb-03_03",
        title: "At Your Service",
        mapName: "atservice",
        imageLinks: [
          {
            name: "atservice",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_04",
        image2: "unbox-mb-03_04",
        title: "Samsung Members",
        condition: "แอปพลิเคชันนี้รองรับเฉพาะโทรศัพท์ซัมซุงเท่านั้น",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_05",
        image2: "unbox-mb-03_05",
        title: "Live Chat",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_06",
        image2: "unbox-mb-03_06",
        title: "For Home appliance",
        condition:
          "บริการนี้สำหรับเครื่องใช้ไฟฟ้าประเภทตู้เย็น เครื่องซักผ้า เครื่องปรับอากาศ และทีวีขนาด 32 นิ้วขึ้นไปเท่านั้น ",
      },
    ],
  },
  cloth: {
    Unbox: [
      {
        no: "01",
        head: "-",
        text: "-",
        image: "unbox-01_01",
        image2: "unbox-mb-01_01",
        title: "แกะกล่องเครื่องใหม่",
      },
      {
        no: "02",
        head: "-",
        text: "-",
        image: "unbox-01_02",
        image2: "unbox-mb-01_02",
        title: "At Your Service",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง At Your Service เพื่อเป็นข้อมูล ประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "serviceat",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "03",
        head: "-",
        text: "-",
        image: "unbox-01_03",
        image2: "unbox-mb-01_03",
        title: "Samsung Members",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      },
      // {
      //   no: "04",
      //   head: "-",
      //   text: "-",
      //   image: "unbox-01_04",
      //   image2: "unbox-mb-01_04",
      //   url: "serice-btn",
      //   title: "Extended Warranty",
      //   condition:
      //     "1. ลูกค้าต้องลงทะเบียนผ่านเว็บไซต์ SSTHWARRANTY  เพื่อเอาประกันเท่านั้น <br> 2. ระยะเวลาที่ขยายการรับประกันสินค้า ขึ้นอยู่กับรุ่นของสินค้าและโปรโมชั่นขณะนั้น <br> 3. การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      // },
    ],
    Inquiry: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-02_01",
        image2: "unbox-mb-02_01",
        title: "เครื่องใหม่ ยังไม่ชิน / ต้องการแก้ไขปัญหาเริ่มต้น? ",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_02",
        image2: "unbox-mb-02_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_03",
        image2: "unbox-mb-02_03",
        title: "Live Chat",
        mapName: "livechat",
        imageLinks: [
          {
            name: "livechat",
            url:
              "https://livechat.support.samsung.com/Customer_mys/CustomerChat_mys.aspx?roomNo=29352510&amp;encrypt=DA7538FE760DCE56801E33CA5AC8840F",
          },
        ],
      },

      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_05",
        image2: "unbox-mb-02_visual",
        title: "Visual Support",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_06",
        image2: "unbox-mb-02_05",
        title: "Private Demo Service",
        modalName: "e-demo",
        condition:
          "1. ลูกค้าสามารถติดต่อขอใช้บริการได้ภายใน 90 วัน นับจากวันที่ลูกค้าซื้อสินค้า โดยอ้างอิงจากใบเสร็จ <br> 2.ลูกค้าต้องเตรียมหลักฐานประกอบการรับบริการ เช่น ใบเสร็จของสินค้าหรือใบรับของจากร้านค้าที่ซื้อมา โดยมีรายละเอียดของผู้ซื้อ ผู้ขายรวมถึงรุ่นของสินค้าที่ซื้อมาอย่างชัดเจน <br> 3.บริการสาธิตการใช้งานผลิตภัณฑ์สำหรับตู้เย็นชนิด Family Hub และ AirDresser ในรุ่นที่กำหนด ให้บริการเฉพาะพื้นที่กรุงเทพฯ และปริมณฑลเท่านั้น",
        modalType: "search",
        imageLinks: [
          {
            name: "modalversion1",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "unbox-02_07",
        image2: "unbox-mb-02_06",
        title: "e-Demo Service",
        imageLinks: [
          {
            name: "modalversion",
            modal: "modalx",
          },
          {
            name: "modalversion2",
            modalImage: "modalx2",
          },
        ],

        imageLinks2: [
          {
            name: "serviceat2",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
    ],
    Repair: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-03_01",
        image2: "unbox-mb-03_01",
        title: "พบช่องทางที่หลากหลายในการแจ้งซ่อมผลิตภัณฑ์",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_02",
        image2: "unbox-mb-03_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_03",
        image2: "unbox-mb-03_03",
        title: "At Your Service",
        mapName: "atservice",
        imageLinks: [
          {
            name: "atservice",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_04",
        image2: "unbox-mb-03_04",
        title: "Samsung Members",
        condition: "แอปพลิเคชันนี้รองรับเฉพาะโทรศัพท์ซัมซุงเท่านั้น",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_05",
        image2: "unbox-mb-03_05",
        title: "Live Chat",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_06",
        image2: "unbox-mb-03_06",
        title: "For Home appliance",
        condition:
          "บริการนี้สำหรับเครื่องใช้ไฟฟ้าประเภทตู้เย็น เครื่องซักผ้า เครื่องปรับอากาศ และทีวีขนาด 32 นิ้วขึ้นไปเท่านั้น ",
      },
    ],
  },
  refrig: {
    Unbox: [
      {
        no: "01",
        head: "-",
        text: "-",
        image: "unbox-01_01",
        image2: "unbox-mb-01_01",
        title: "แกะกล่องเครื่องใหม่",
      },
      {
        no: "02",
        head: "-",
        text: "-",
        image: "unbox-01_02",
        image2: "unbox-mb-01_02",
        title: "At Your Service",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง At Your Service เพื่อเป็นข้อมูล ประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "serviceat",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "03",
        head: "-",
        text: "-",
        image: "unbox-01_03",
        image2: "unbox-mb-01_03",
        title: "Samsung Members",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      },
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-01_04",
        image2: "unbox-mb-01_04",
        url: "serice-btn",
        title: "Extended Warranty",
        mapName: "warranty",
        condition:
          "1. ลูกค้าต้องลงทะเบียนผ่านเว็บไซต์ SSTHWARRANTY  เพื่อเอาประกันเท่านั้น <br> 2. ระยะเวลาที่ขยายการรับประกันสินค้า ขึ้นอยู่กับรุ่นของสินค้าและโปรโมชั่นขณะนั้น <br> 3. การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "warranty",
            url: "https://www.ssthwarranty.com/warranty",
          },
        ],
      },
    ],
    Inquiry: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-02_01",
        image2: "unbox-mb-02_01",
        title: "เครื่องใหม่ ยังไม่ชิน / ต้องการแก้ไขปัญหาเริ่มต้น? ",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_02",
        image2: "unbox-mb-02_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_03",
        image2: "unbox-mb-02_03",
        title: "Live Chat",
        mapName: "livechat",
        imageLinks: [
          {
            name: "livechat",
            url:
              "https://livechat.support.samsung.com/Customer_mys/CustomerChat_mys.aspx?roomNo=29352510&amp;encrypt=DA7538FE760DCE56801E33CA5AC8840F",
          },
        ],
      },

      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_05",
        image2: "unbox-mb-02_visual",
        title: "Visual Support",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_06",
        image2: "unbox-mb-02_05",
        title: "Private Demo Service",
        condition:
          "1. ลูกค้าสามารถติดต่อขอใช้บริการได้ภายใน 90 วัน นับจากวันที่ลูกค้าซื้อสินค้า โดยอ้างอิงจากใบเสร็จ <br> 2.ลูกค้าต้องเตรียมหลักฐานประกอบการรับบริการ เช่น ใบเสร็จของสินค้าหรือใบรับของจากร้านค้าที่ซื้อมา โดยมีรายละเอียดของผู้ซื้อ ผู้ขายรวมถึงรุ่นของสินค้าที่ซื้อมาอย่างชัดเจน <br> 3.บริการสาธิตการใช้งานผลิตภัณฑ์สำหรับตู้เย็นชนิด Family Hub และ AirDresser ในรุ่นที่กำหนด ให้บริการเฉพาะพื้นที่กรุงเทพฯ และปริมณฑลเท่านั้น",
        modalType: "search",
        imageLinks: [
          {
            name: "modalversion1",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "unbox-02_07",
        image2: "unbox-mb-02_06",
        title: "e-Demo Service",
        imageLinks: [
          {
            name: "modalversion",
            modal: "modalx",
          },
          {
            name: "modalversion2",
            modalImage: "modalx2",
          },
        ],

        imageLinks2: [
          {
            name: "serviceat2",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
    ],
    Repair: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-03_01",
        image2: "unbox-mb-03_01",
        title: "พบช่องทางที่หลากหลายในการแจ้งซ่อมผลิตภัณฑ์",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_02",
        image2: "unbox-mb-03_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_03",
        image2: "unbox-mb-03_03",
        title: "At Your Service",
        mapName: "atservice",
        imageLinks: [
          {
            name: "atservice",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_04",
        image2: "unbox-mb-03_04",
        title: "Samsung Members",
        condition: "แอปพลิเคชันนี้รองรับเฉพาะโทรศัพท์ซัมซุงเท่านั้น",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_05",
        image2: "unbox-mb-03_05",
        title: "Live Chat",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_06",
        image2: "unbox-mb-03_06",
        title: "For Home appliance",
        condition:
          "บริการนี้สำหรับเครื่องใช้ไฟฟ้าประเภทตู้เย็น เครื่องซักผ้า เครื่องปรับอากาศ และทีวีขนาด 32 นิ้วขึ้นไปเท่านั้น ",
      },
    ],
  },
  ac: {
    Unbox: [
      {
        no: "01",
        head: "-",
        text: "-",
        image: "unbox-01_01",
        image2: "unbox-mb-01_01",
        title: "แกะกล่องเครื่องใหม่",
      },
      {
        no: "02",
        head: "-",
        text: "-",
        image: "unbox-01_02",
        image2: "unbox-mb-01_02",
        title: "At Your Service",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง At Your Service เพื่อเป็นข้อมูล ประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "serviceat",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "03",
        head: "-",
        text: "-",
        image: "unbox-01_03",
        image2: "unbox-mb-01_03",
        title: "Samsung Members",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      },
      // {
      //   no: "04",
      //   head: "-",
      //   text: "-",
      //   image: "unbox-01_04",
      //   image2: "unbox-mb-01_04",
      //   url: "serice-btn",
      //   title: "Extended Warranty",
      //   condition:
      //     "1. ลูกค้าต้องลงทะเบียนผ่านเว็บไซต์ SSTHWARRANTY  เพื่อเอาประกันเท่านั้น <br> 2. ระยะเวลาที่ขยายการรับประกันสินค้า ขึ้นอยู่กับรุ่นของสินค้าและโปรโมชั่นขณะนั้น <br> 3. การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      // },
    ],
    Inquiry: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-02_01",
        image2: "unbox-mb-02_01",
        title: "เครื่องใหม่ ยังไม่ชิน / ต้องการแก้ไขปัญหาเริ่มต้น? ",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_02",
        image2: "unbox-mb-02_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_03",
        image2: "unbox-mb-02_03",
        title: "Live Chat",
        mapName: "livechat",
        imageLinks: [
          {
            name: "livechat",
            url:
              "https://livechat.support.samsung.com/Customer_mys/CustomerChat_mys.aspx?roomNo=29352510&amp;encrypt=DA7538FE760DCE56801E33CA5AC8840F",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_05",
        image2: "unbox-mb-02_visual",
        title: "Visual Support",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "unbox-02_07",
        image2: "unbox-mb-02_06",
        title: "e-Demo Service",
        imageLinks: [
          {
            name: "modalversion",
            modal: "modalx",
          },
          {
            name: "modalversion2",
            modalImage: "modalx2",
          },
        ],

        imageLinks2: [
          {
            name: "serviceat2",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
    ],
    Repair: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-03_01",
        image2: "unbox-mb-03_01",
        title: "พบช่องทางที่หลากหลายในการแจ้งซ่อมผลิตภัณฑ์",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_02",
        image2: "unbox-mb-03_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_03",
        image2: "unbox-mb-03_03",
        title: "At Your Service",
        mapName: "atservice",
        imageLinks: [
          {
            name: "atservice",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_04",
        image2: "unbox-mb-03_04",
        title: "Samsung Members",
        condition: "แอปพลิเคชันนี้รองรับเฉพาะโทรศัพท์ซัมซุงเท่านั้น",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_05",
        image2: "unbox-mb-03_05",
        title: "Live Chat",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_06",
        image2: "unbox-mb-03_06",
        title: "For Home appliance",
        condition:
          "บริการนี้สำหรับเครื่องใช้ไฟฟ้าประเภทตู้เย็น เครื่องซักผ้า เครื่องปรับอากาศ และทีวีขนาด 32 นิ้วขึ้นไปเท่านั้น ",
      },
    ],
  },
  mc: {
    Unbox: [
      {
        no: "01",
        head: "-",
        text: "-",
        image: "unbox-01_01",
        image2: "unbox-mb-01_01",
        title: "แกะกล่องเครื่องใหม่",
      },
      {
        no: "02",
        head: "-",
        text: "-",
        image: "unbox-01_02",
        image2: "unbox-mb-01_02",
        title: "At Your Service",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง At Your Service เพื่อเป็นข้อมูล ประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "serviceat",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "03",
        head: "-",
        text: "-",
        image: "unbox-01_03",
        image2: "unbox-mb-01_03",
        title: "Samsung Members",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      },
      // {
      //   no: "04",
      //   head: "-",
      //   text: "-",
      //   image: "unbox-01_04",
      //   image2: "unbox-mb-01_04",
      //   url: "serice-btn",
      //   title: "Extended Warranty",
      //   condition:
      //     "1. ลูกค้าต้องลงทะเบียนผ่านเว็บไซต์ SSTHWARRANTY  เพื่อเอาประกันเท่านั้น <br> 2. ระยะเวลาที่ขยายการรับประกันสินค้า ขึ้นอยู่กับรุ่นของสินค้าและโปรโมชั่นขณะนั้น <br> 3. การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      // },
    ],
    Inquiry: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-02_01",
        image2: "unbox-mb-02_01",
        title: "เครื่องใหม่ ยังไม่ชิน / ต้องการแก้ไขปัญหาเริ่มต้น? ",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_02",
        image2: "unbox-mb-02_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_03",
        image2: "unbox-mb-02_03",
        title: "Live Chat",
        mapName: "livechat",
        imageLinks: [
          {
            name: "livechat",
            url:
              "https://livechat.support.samsung.com/Customer_mys/CustomerChat_mys.aspx?roomNo=29352510&amp;encrypt=DA7538FE760DCE56801E33CA5AC8840F",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_05",
        image2: "unbox-mb-02_visual",
        title: "Visual Support",
      },
    ],
    Repair: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-03_01",
        image2: "unbox-mb-03_01",
        title: "พบช่องทางที่หลากหลายในการแจ้งซ่อมผลิตภัณฑ์",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_02",
        image2: "unbox-mb-03_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_03",
        image2: "unbox-mb-03_03",
        title: "At Your Service",
        mapName: "atservice",
        imageLinks: [
          {
            name: "atservice",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_04",
        image2: "unbox-mb-03_04",
        title: "Samsung Members",
        condition: "แอปพลิเคชันนี้รองรับเฉพาะโทรศัพท์ซัมซุงเท่านั้น",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_05",
        image2: "unbox-mb-03_05",
        title: "Live Chat",
      },
    ],
  },
  robot: {
    Unbox: [
      {
        no: "01",
        head: "-",
        text: "-",
        image: "unbox-01_01",
        image2: "unbox-mb-01_01",
        title: "แกะกล่องเครื่องใหม่",
      },
      {
        no: "02",
        head: "-",
        text: "-",
        image: "unbox-01_02",
        image2: "unbox-mb-01_02",
        title: "At Your Service",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง At Your Service เพื่อเป็นข้อมูล ประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "serviceat",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "03",
        head: "-",
        text: "-",
        image: "unbox-01_03",
        image2: "unbox-mb-01_03",
        title: "Samsung Members",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      },
      // {
      //   no: "04",
      //   head: "-",
      //   text: "-",
      //   image: "unbox-01_04",
      //   image2: "unbox-mb-01_04",
      //   url: "serice-btn",
      //   title: "Extended Warranty",
      //   condition:
      //     "1. ลูกค้าต้องลงทะเบียนผ่านเว็บไซต์ SSTHWARRANTY  เพื่อเอาประกันเท่านั้น <br> 2. ระยะเวลาที่ขยายการรับประกันสินค้า ขึ้นอยู่กับรุ่นของสินค้าและโปรโมชั่นขณะนั้น <br> 3. การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      // },
    ],
    Inquiry: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-02_01",
        image2: "unbox-mb-02_01",
        title: "เครื่องใหม่ ยังไม่ชิน / ต้องการแก้ไขปัญหาเริ่มต้น? ",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_02",
        image2: "unbox-mb-02_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_03",
        image2: "unbox-mb-02_03",
        title: "Live Chat",
        mapName: "livechat",
        imageLinks: [
          {
            name: "livechat",
            url:
              "https://livechat.support.samsung.com/Customer_mys/CustomerChat_mys.aspx?roomNo=29352510&amp;encrypt=DA7538FE760DCE56801E33CA5AC8840F",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-02_05",
        image2: "unbox-mb-02_visual",
        title: "Visual Support",
      },
    ],
    Repair: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-03_01",
        image2: "unbox-mb-03_01",
        title: "พบช่องทางที่หลากหลายในการแจ้งซ่อมผลิตภัณฑ์",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_02",
        image2: "unbox-mb-03_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_03",
        image2: "unbox-mb-03_03",
        title: "At Your Service",
        mapName: "atservice",
        imageLinks: [
          {
            name: "atservice",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_04",
        image2: "unbox-mb-03_04",
        title: "Samsung Members",
        condition: "แอปพลิเคชันนี้รองรับเฉพาะโทรศัพท์ซัมซุงเท่านั้น",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
        image: "unbox-03_05",
        image2: "unbox-mb-03_05",
        title: "Live Chat",
      },
    ],
  },
};

let mainActive = "mobile";
let subActive = "Unbox";
let No = 1;
const eventSlider = (current) => {
  var totalSlides = items[mainActive][subActive].length;

  let btnNext = ".swiper-button-next";
  let btnPrev = ".swiper-button-prev";
  No = current.activeIndex + 1;
  if (No >= totalSlides) {
    $(btnNext).hide();
  } else {
    $(btnNext).show();
  }

  if (No == 1) {
    $(btnPrev).hide();
  } else {
    $(btnPrev).show();
  }

  $(`#${subActive} #currentslide`).text(No);
  let find = items[mainActive][subActive][current.activeIndex];
  let title = find.title ? find.title : "no-title";
  let condition = find.condition ? find.condition : false;
  if (condition) {
    $(`.tool-tip-ex .tooltiptext`).html(condition);
    $(`.tool-tip-ex`).show();
  } else {
    $(`.tool-tip-ex`).hide();
  }

  $(`#${subActive} #totalslide`).html(
    `${totalSlides} <span class="title-page">${title} </span>`
  );
};
const callSlider = () => {
  var swiper2 = new Swiper(".s2", {
    slidesPerView: "auto",
    // spaceBetween: 30,
    loop: false,
    // width: 1166,
    // setWrapperSize: true,
    // mousewheel: {
    //   invert: true,
    // },
    simulateTouch: false,
    centeredSlides: true,
    autoHeight: true,
    pagination: {
      el: ".swiper-pagination,.swiper-paginations",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    on: {
      init: function (current) {
        console.log(`#${subActive} #currentslide`);
        eventSlider(current);
      },
      slideChange: function (current) {
        // console.log("current", current.activeIndex - 1);
        console.log("swiper change");
        eventSlider(current);
      },
    },
    breakpoints: {
      768: {
        slidesPerView: "auto",
        // spaceBetween: 100,
        autoHeight: true,
      },
    },
  });
};

const renderAImageLink = (links) => {
  let html = "";

  if (links && links.length) {
    links.forEach((el) => {
      let textModalAttr = "";

      if (el.modal) {
        textModalAttr = `data-modal="${el.modal}"`;
      }
        if (el.modalImage) {
        textModalAttr = `data-modal-image="${el.modalImage}"`;
      }
      html += `<a class="link-ss--${el.name}" ${textModalAttr} href="${
        !el.url ? "javascript:void(0)" : el.url
      }" ${!el.url ? "" : 'target = "_blank"'}></a>`;
    });
  }

  return html;
};
const renderSlider = (name) => {
  let templateHtml = (data) => {
    let textModalAttr = "";
    if (data.modal) {
      textModalAttr = `data-modal="${data.modalName}"`;
    }
 
    // if (subActive == 'Unbox' || subActive == 'Inquiry' ){
    return ` 
        <div class="swiper-slide slide--01" >
           <div class="slide--item">
           
             <div class="image-service">
              <button class="outline" ${textModalAttr}>
               <img src="images/${data.image}.png" usemap='#${
      data.mapName
    }' width="100%" class="dt-show"/>
               
               <img src="images/${data.image2}.png" usemap='#${
      data.mapName
    }-m' alt="" width="100%" class="mb-show"/>
               </button>
               ${renderAImageLink(data.imageLinks)}
               ${renderAImageLink(data.imageLinks2)}
               ${renderAImageLink(data.imageLinksmodal2)}
              </div>
            </div>
        </div>
        `;
    // }
    // else if (subActive == "Extra") {
    //   return `
    //   <div class="swiper-slide slide--01">
    //      <div class="slide--item">
    //          <img src="images/${data.image}.png" alt=""  width="100%" class="dt-show"/>
    //          <img src="images/${data.image2}.png" alt=""  width="100%" class="mb-show"/>
    //       </div>
    //   </div>

    //   `;
    // }
  };
  let render = "";
  items[name][subActive].forEach((el) => {
    render += templateHtml(el);
  });
  return render;
};

let modalActive = "";

let setModalLength = (n) => {
  $("#modal-html .m--result span").text(n);
  $("#modal-html .m--result span").show();
  $("#modal-html .modal-title").show();
  $("#modal-html .addinput").show();
  $("#modal-html .m--result").show();

};
const init = {
  menuClick() {
    $(".button-menu--click").click(function () {
      let dataSlider = $(this).data("slider");
      mainActive = dataSlider;
      //$(".wrap-s2 .swiper-wrapper").empty();
      let content = $(`#${subActive} .swiper-wrapper`);
      let activeArray = items[mainActive];
      $(".tablinks").hide();

      for (var key in activeArray) {
        $(`[data-tabname='${key}']`).show();
      }
      content.html(renderSlider(mainActive));

      //  let content2 = $(`#${subActive} .titleslide`);
      //  content2.html(rendertitle(mainActive));
      // console.log(renderSlider(mainActive));
      // console.log(dataSlider);
      // console.log(subActive);

      callSlider();
    });
  },
  tabClick() {
    $(".service--tab .tablinks").click(function () {
      let dataTabname = $(this).data("tabname");
      subActive = dataTabname;
      let content = $(`#${subActive} .swiper-wrapper`);
      content.html(renderSlider(mainActive));
      console.log(subActive);
      // let content2 = $(`#${subActive} .titleslide`);
      // content2.html(rendertitle(mainActive));

      callSlider();
    });
  },

  modal: {
    click() {
      $("[data-modal]").live("click", function () {
        modalActive = $(this).data("modal");

        let itemActive = items[mainActive][subActive];
        let findModalActiveByName = _.find(
          modalItems,
          (el) => el.modalName == modalActive
        );
        let datas = findModalActiveByName.modal;
        // let modalTitle = findModalActiveByName.modalTitle

        let resultData = _.map(datas, (el) => {
          let product_text = _.find(products, (p) => p.key == el.product);
          let category_text = _.find(categories, (p) => p.key == el.category);
          let model_text = _.find(models, (p) => p.key == el.model);

          return {
            product: product_text ? product_text : el.product,
            category: category_text ? category_text : el.category,
            model: model_text ? model_text : el.model,
          };
        });

        console.log("map", { resultData, itemActive, modalActive });

        $(`#s-modal .modal-content`).css("display", "block");
        $(`#s-modal`).css("display", "block");

        let renderModal = renderModalSearch(resultData);

        $(`#s-modal .modal-content #modal-html .table-list`).html(renderModal);
        // $(`#image-modal .modal-content #modal-html-2 .table-list`).html(renderModal);
        // $(`#s-modal .modal-content #modal-html h1.modal-title`).html(modalTitle)
      });
    },
     imageClick() {
      $("[data-modal-image]").live("click", function () {
        let modalUrl = $(this).data("modal-image");

        $(`#image-modal .modal-content`).css("display", "block");
        $(`#image-modal`).css("display", "block");

        let renderModal = renderModalImage(modalUrl);

        
        $(`#image-modal .modal-content #modal-html .table-list2`).html(
          renderModal
        );
        // $(`#s-modal .modal-content #modal-html h1.modal-title`).html(modalTitle)
      });
    },
    
    
    close() {
      $(".close").live("click", function () {
        $(`#s-modal`).css("display", "none");
        $(`#image-modal`).css("display", "none");
      });
     
    },
    search() {
      $(".myInputModal").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function () {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });
        let countSearch = $("#myTable").children("tr:visible").length;
        setModalLength(countSearch);
      });
    },
  },
};

$(window).ready(() => {
  init.menuClick();
  init.tabClick();
  init.modal.click();
    init.modal.imageClick();

  init.modal.close();
  init.modal.search();
});

const renderTable = (header, bodyCol, bodyDatas) => {
  let head = `
  <thead class="head-d">
  <tr>
  $COLUMN
  </tr>
  </thead>
  `;

  let headColumn = "";

  header.forEach((el) => {
    headColumn += `<td>${el}</td>`;
  });

  head = head.replace("$COLUMN", headColumn);

  let body = `
  <tbody id="myTable">
  $COLUMN
  </tbody>
  `;

  let bodyColumn = "";

  bodyDatas.forEach((el) => {
    bodyColumn += "<tr>";
    bodyCol.forEach((ibody) => {
      let x = el[ibody];
      bodyColumn += `<td>${x.name}</td>`;
    });
    bodyColumn += "</tr>";
  });

  body = body.replace("$COLUMN", bodyColumn);

  let html = `
  <table >
  ${head}
  ${body}
  </table>
  `;

  return html;
};

const renderModalSearch = (resultData) => {
  let table = renderTable(
    ["Products", "Category", "Models"],
    ["product", "category", "model"],
    resultData
  );

  setModalLength(resultData.length);
  let html = `
  ${table}
  `;

  return html;
};







const renderModalImage = (image) => {
  $("#modal-html .m--result span").hide();
  $("#modal-html .modal-title").hide();
  $("#modal-html .addinput").hide();
  $("#modal-html .m--result").hide();

  let html = `
  <img src="${image}.png" class="modal-dt-image"/>
    <img src="${image}-m.png" class="modal-mb-image"/>

  `;

  return html;
};

