"use strict";

/*! jQuery v1.8.3 jquery.com | jquery.org/license */


function _typeof(obj) {
  "@babel/helpers - typeof";
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj &&
        typeof Symbol === "function" &&
        obj.constructor === Symbol &&
        obj !== Symbol.prototype
        ? "symbol"
        : typeof obj;
    };
  }
  return _typeof(obj);
}

(function (e, t) {
  function _(e) {
    var t = (M[e] = {});
    return (
      v.each(e.split(y), function (e, n) {
        t[n] = !0;
      }),
      t
    );
  }

  function H(e, n, r) {
    if (r === t && e.nodeType === 1) {
      var i = "data-" + n.replace(P, "-$1").toLowerCase();
      r = e.getAttribute(i);

      if (typeof r == "string") {
        try {
          r =
            r === "true"
              ? !0
              : r === "false"
              ? !1
              : r === "null"
              ? null
              : +r + "" === r
              ? +r
              : D.test(r)
              ? v.parseJSON(r)
              : r;
        } catch (s) {}

        v.data(e, n, r);
      } else r = t;
    }

    return r;
  }

  function B(e) {
    var t;

    for (t in e) {
      if (t === "data" && v.isEmptyObject(e[t])) continue;
      if (t !== "toJSON") return !1;
    }

    return !0;
  }

  function et() {
    return !1;
  }

  function tt() {
    return !0;
  }

  function ut(e) {
    return !e || !e.parentNode || e.parentNode.nodeType === 11;
  }

  function at(e, t) {
    do {
      e = e[t];
    } while (e && e.nodeType !== 1);

    return e;
  }

  function ft(e, t, n) {
    t = t || 0;
    if (v.isFunction(t))
      return v.grep(e, function (e, r) {
        var i = !!t.call(e, r, e);
        return i === n;
      });
    if (t.nodeType)
      return v.grep(e, function (e, r) {
        return (e === t) === n;
      });

    if (typeof t == "string") {
      var r = v.grep(e, function (e) {
        return e.nodeType === 1;
      });
      if (it.test(t)) return v.filter(t, r, !n);
      t = v.filter(t, r);
    }

    return v.grep(e, function (e, r) {
      return v.inArray(e, t) >= 0 === n;
    });
  }

  function lt(e) {
    var t = ct.split("|"),
      n = e.createDocumentFragment();
    if (n.createElement)
      while (t.length) {
        n.createElement(t.pop());
      }
    return n;
  }

  function Lt(e, t) {
    return (
      e.getElementsByTagName(t)[0] ||
      e.appendChild(e.ownerDocument.createElement(t))
    );
  }

  function At(e, t) {
    if (t.nodeType !== 1 || !v.hasData(e)) return;

    var n,
      r,
      i,
      s = v._data(e),
      o = v._data(t, s),
      u = s.events;

    if (u) {
      delete o.handle, (o.events = {});

      for (n in u) {
        for (r = 0, i = u[n].length; r < i; r++) {
          v.event.add(t, n, u[n][r]);
        }
      }
    }

    o.data && (o.data = v.extend({}, o.data));
  }

  function Ot(e, t) {
    var n;
    if (t.nodeType !== 1) return;
    t.clearAttributes && t.clearAttributes(),
      t.mergeAttributes && t.mergeAttributes(e),
      (n = t.nodeName.toLowerCase()),
      n === "object"
        ? (t.parentNode && (t.outerHTML = e.outerHTML),
          v.support.html5Clone &&
            e.innerHTML &&
            !v.trim(t.innerHTML) &&
            (t.innerHTML = e.innerHTML))
        : n === "input" && Et.test(e.type)
        ? ((t.defaultChecked = t.checked = e.checked),
          t.value !== e.value && (t.value = e.value))
        : n === "option"
        ? (t.selected = e.defaultSelected)
        : n === "input" || n === "textarea"
        ? (t.defaultValue = e.defaultValue)
        : n === "script" && t.text !== e.text && (t.text = e.text),
      t.removeAttribute(v.expando);
  }

  function Mt(e) {
    return typeof e.getElementsByTagName != "undefined"
      ? e.getElementsByTagName("*")
      : typeof e.querySelectorAll != "undefined"
      ? e.querySelectorAll("*")
      : [];
  }

  function _t(e) {
    Et.test(e.type) && (e.defaultChecked = e.checked);
  }

  function Qt(e, t) {
    if (t in e) return t;
    var n = t.charAt(0).toUpperCase() + t.slice(1),
      r = t,
      i = Jt.length;

    while (i--) {
      t = Jt[i] + n;
      if (t in e) return t;
    }

    return r;
  }

  function Gt(e, t) {
    return (
      (e = t || e),
      v.css(e, "display") === "none" || !v.contains(e.ownerDocument, e)
    );
  }

  function Yt(e, t) {
    var n,
      r,
      i = [],
      s = 0,
      o = e.length;

    for (; s < o; s++) {
      n = e[s];
      if (!n.style) continue;
      (i[s] = v._data(n, "olddisplay")),
        t
          ? (!i[s] && n.style.display === "none" && (n.style.display = ""),
            n.style.display === "" &&
              Gt(n) &&
              (i[s] = v._data(n, "olddisplay", nn(n.nodeName))))
          : ((r = Dt(n, "display")),
            !i[s] && r !== "none" && v._data(n, "olddisplay", r));
    }

    for (s = 0; s < o; s++) {
      n = e[s];
      if (!n.style) continue;
      if (!t || n.style.display === "none" || n.style.display === "")
        n.style.display = t ? i[s] || "" : "none";
    }

    return e;
  }

  function Zt(e, t, n) {
    var r = Rt.exec(t);
    return r ? Math.max(0, r[1] - (n || 0)) + (r[2] || "px") : t;
  }

  function en(e, t, n, r) {
    var i = n === (r ? "border" : "content") ? 4 : t === "width" ? 1 : 0,
      s = 0;

    for (; i < 4; i += 2) {
      n === "margin" && (s += v.css(e, n + $t[i], !0)),
        r
          ? (n === "content" &&
              (s -= parseFloat(Dt(e, "padding" + $t[i])) || 0),
            n !== "margin" &&
              (s -= parseFloat(Dt(e, "border" + $t[i] + "Width")) || 0))
          : ((s += parseFloat(Dt(e, "padding" + $t[i])) || 0),
            n !== "padding" &&
              (s += parseFloat(Dt(e, "border" + $t[i] + "Width")) || 0));
    }

    return s;
  }

  function tn(e, t, n) {
    var r = t === "width" ? e.offsetWidth : e.offsetHeight,
      i = !0,
      s = v.support.boxSizing && v.css(e, "boxSizing") === "border-box";

    if (r <= 0 || r == null) {
      r = Dt(e, t);
      if (r < 0 || r == null) r = e.style[t];
      if (Ut.test(r)) return r;
      (i = s && (v.support.boxSizingReliable || r === e.style[t])),
        (r = parseFloat(r) || 0);
    }

    return r + en(e, t, n || (s ? "border" : "content"), i) + "px";
  }

  function nn(e) {
    if (Wt[e]) return Wt[e];
    var t = v("<" + e + ">").appendTo(i.body),
      n = t.css("display");
    t.remove();

    if (n === "none" || n === "") {
      Pt = i.body.appendChild(
        Pt ||
          v.extend(i.createElement("iframe"), {
            frameBorder: 0,
            width: 0,
            height: 0
          })
      );
      if (!Ht || !Pt.createElement)
        (Ht = (Pt.contentWindow || Pt.contentDocument).document),
          Ht.write("<!doctype html><html><body>"),
          Ht.close();
      (t = Ht.body.appendChild(Ht.createElement(e))),
        (n = Dt(t, "display")),
        i.body.removeChild(Pt);
    }

    return (Wt[e] = n), n;
  }

  function fn(e, t, n, r) {
    var i;
    if (v.isArray(t))
      v.each(t, function (t, i) {
        n || sn.test(e)
          ? r(e, i)
          : fn(e + "[" + (_typeof(i) == "object" ? t : "") + "]", i, n, r);
      });
    else if (!n && v.type(t) === "object")
      for (i in t) {
        fn(e + "[" + i + "]", t[i], n, r);
      }
    else r(e, t);
  }

  function Cn(e) {
    return function (t, n) {
      typeof t != "string" && ((n = t), (t = "*"));
      var r,
        i,
        s,
        o = t.toLowerCase().split(y),
        u = 0,
        a = o.length;
      if (v.isFunction(n))
        for (; u < a; u++) {
          (r = o[u]),
            (s = /^\+/.test(r)),
            s && (r = r.substr(1) || "*"),
            (i = e[r] = e[r] || []),
            i[s ? "unshift" : "push"](n);
        }
    };
  }

  function kn(e, n, r, i, s, o) {
    (s = s || n.dataTypes[0]), (o = o || {}), (o[s] = !0);
    var u,
      a = e[s],
      f = 0,
      l = a ? a.length : 0,
      c = e === Sn;

    for (; f < l && (c || !u); f++) {
      (u = a[f](n, r, i)),
        typeof u == "string" &&
          (!c || o[u]
            ? (u = t)
            : (n.dataTypes.unshift(u), (u = kn(e, n, r, i, u, o))));
    }

    return (c || !u) && !o["*"] && (u = kn(e, n, r, i, "*", o)), u;
  }

  function Ln(e, n) {
    var r,
      i,
      s = v.ajaxSettings.flatOptions || {};

    for (r in n) {
      n[r] !== t && ((s[r] ? e : i || (i = {}))[r] = n[r]);
    }

    i && v.extend(!0, e, i);
  }

  function An(e, n, r) {
    var i,
      s,
      o,
      u,
      a = e.contents,
      f = e.dataTypes,
      l = e.responseFields;

    for (s in l) {
      s in r && (n[l[s]] = r[s]);
    }

    while (f[0] === "*") {
      f.shift(),
        i === t && (i = e.mimeType || n.getResponseHeader("content-type"));
    }

    if (i)
      for (s in a) {
        if (a[s] && a[s].test(i)) {
          f.unshift(s);
          break;
        }
      }
    if (f[0] in r) o = f[0];
    else {
      for (s in r) {
        if (!f[0] || e.converters[s + " " + f[0]]) {
          o = s;
          break;
        }

        u || (u = s);
      }

      o = o || u;
    }
    if (o) return o !== f[0] && f.unshift(o), r[o];
  }

  function On(e, t) {
    var n,
      r,
      i,
      s,
      o = e.dataTypes.slice(),
      u = o[0],
      a = {},
      f = 0;
    e.dataFilter && (t = e.dataFilter(t, e.dataType));
    if (o[1])
      for (n in e.converters) {
        a[n.toLowerCase()] = e.converters[n];
      }

    for (; (i = o[++f]); ) {
      if (i !== "*") {
        if (u !== "*" && u !== i) {
          n = a[u + " " + i] || a["* " + i];
          if (!n)
            for (r in a) {
              s = r.split(" ");

              if (s[1] === i) {
                n = a[u + " " + s[0]] || a["* " + s[0]];

                if (n) {
                  n === !0
                    ? (n = a[r])
                    : a[r] !== !0 && ((i = s[0]), o.splice(f--, 0, i));
                  break;
                }
              }
            }
          if (n !== !0)
            if (n && e["throws"]) t = n(t);
            else
              try {
                t = n(t);
              } catch (l) {
                return {
                  state: "parsererror",
                  error: n ? l : "No conversion from " + u + " to " + i
                };
              }
        }

        u = i;
      }
    }

    return {
      state: "success",
      data: t
    };
  }

  function Fn() {
    try {
      return new e.XMLHttpRequest();
    } catch (t) {}
  }

  function In() {
    try {
      return new e.ActiveXObject("Microsoft.XMLHTTP");
    } catch (t) {}
  }

  function $n() {
    return (
      setTimeout(function () {
        qn = t;
      }, 0),
      (qn = v.now())
    );
  }

  function Jn(e, t) {
    v.each(t, function (t, n) {
      var r = (Vn[t] || []).concat(Vn["*"]),
        i = 0,
        s = r.length;

      for (; i < s; i++) {
        if (r[i].call(e, t, n)) return;
      }
    });
  }

  function Kn(e, t, n) {
    var r,
      i = 0,
      s = 0,
      o = Xn.length,
      u = v.Deferred().always(function () {
        delete a.elem;
      }),
      a = function a() {
        var t = qn || $n(),
          n = Math.max(0, f.startTime + f.duration - t),
          r = n / f.duration || 0,
          i = 1 - r,
          s = 0,
          o = f.tweens.length;

        for (; s < o; s++) {
          f.tweens[s].run(i);
        }

        return (
          u.notifyWith(e, [f, i, n]),
          i < 1 && o ? n : (u.resolveWith(e, [f]), !1)
        );
      },
      f = u.promise({
        elem: e,
        props: v.extend({}, t),
        opts: v.extend(
          !0,
          {
            specialEasing: {}
          },
          n
        ),
        originalProperties: t,
        originalOptions: n,
        startTime: qn || $n(),
        duration: n.duration,
        tweens: [],
        createTween: function createTween(t, n, r) {
          var i = v.Tween(
            e,
            f.opts,
            t,
            n,
            f.opts.specialEasing[t] || f.opts.easing
          );
          return f.tweens.push(i), i;
        },
        stop: function stop(t) {
          var n = 0,
            r = t ? f.tweens.length : 0;

          for (; n < r; n++) {
            f.tweens[n].run(1);
          }

          return t ? u.resolveWith(e, [f, t]) : u.rejectWith(e, [f, t]), this;
        }
      }),
      l = f.props;

    Qn(l, f.opts.specialEasing);

    for (; i < o; i++) {
      r = Xn[i].call(f, e, l, f.opts);
      if (r) return r;
    }

    return (
      Jn(f, l),
      v.isFunction(f.opts.start) && f.opts.start.call(e, f),
      v.fx.timer(
        v.extend(a, {
          anim: f,
          queue: f.opts.queue,
          elem: e
        })
      ),
      f
        .progress(f.opts.progress)
        .done(f.opts.done, f.opts.complete)
        .fail(f.opts.fail)
        .always(f.opts.always)
    );
  }

  function Qn(e, t) {
    var n, r, i, s, o;

    for (n in e) {
      (r = v.camelCase(n)),
        (i = t[r]),
        (s = e[n]),
        v.isArray(s) && ((i = s[1]), (s = e[n] = s[0])),
        n !== r && ((e[r] = s), delete e[n]),
        (o = v.cssHooks[r]);

      if (o && "expand" in o) {
        (s = o.expand(s)), delete e[r];

        for (n in s) {
          n in e || ((e[n] = s[n]), (t[n] = i));
        }
      } else t[r] = i;
    }
  }

  function Gn(e, t, n) {
    var r,
      i,
      s,
      o,
      u,
      a,
      f,
      l,
      c,
      h = this,
      p = e.style,
      d = {},
      m = [],
      g = e.nodeType && Gt(e);
    n.queue ||
      ((l = v._queueHooks(e, "fx")),
      l.unqueued == null &&
        ((l.unqueued = 0),
        (c = l.empty.fire),
        (l.empty.fire = function () {
          l.unqueued || c();
        })),
      l.unqueued++,
      h.always(function () {
        h.always(function () {
          l.unqueued--, v.queue(e, "fx").length || l.empty.fire();
        });
      })),
      e.nodeType === 1 &&
        ("height" in t || "width" in t) &&
        ((n.overflow = [p.overflow, p.overflowX, p.overflowY]),
        v.css(e, "display") === "inline" &&
          v.css(e, "float") === "none" &&
          (!v.support.inlineBlockNeedsLayout || nn(e.nodeName) === "inline"
            ? (p.display = "inline-block")
            : (p.zoom = 1))),
      n.overflow &&
        ((p.overflow = "hidden"),
        v.support.shrinkWrapBlocks ||
          h.done(function () {
            (p.overflow = n.overflow[0]),
              (p.overflowX = n.overflow[1]),
              (p.overflowY = n.overflow[2]);
          }));

    for (r in t) {
      s = t[r];

      if (Un.exec(s)) {
        delete t[r], (a = a || s === "toggle");
        if (s === (g ? "hide" : "show")) continue;
        m.push(r);
      }
    }

    o = m.length;

    if (o) {
      (u = v._data(e, "fxshow") || v._data(e, "fxshow", {})),
        "hidden" in u && (g = u.hidden),
        a && (u.hidden = !g),
        g
          ? v(e).show()
          : h.done(function () {
              v(e).hide();
            }),
        h.done(function () {
          var t;
          v.removeData(e, "fxshow", !0);

          for (t in d) {
            v.style(e, t, d[t]);
          }
        });

      for (r = 0; r < o; r++) {
        (i = m[r]),
          (f = h.createTween(i, g ? u[i] : 0)),
          (d[i] = u[i] || v.style(e, i)),
          i in u ||
            ((u[i] = f.start),
            g &&
              ((f.end = f.start),
              (f.start = i === "width" || i === "height" ? 1 : 0)));
      }
    }
  }

  function Yn(e, t, n, r, i) {
    return new Yn.prototype.init(e, t, n, r, i);
  }

  function Zn(e, t) {
    var n,
      r = {
        height: e
      },
      i = 0;
    t = t ? 1 : 0;

    for (; i < 4; i += 2 - t) {
      (n = $t[i]), (r["margin" + n] = r["padding" + n] = e);
    }

    return t && (r.opacity = r.width = e), r;
  }

  function tr(e) {
    return v.isWindow(e)
      ? e
      : e.nodeType === 9
      ? e.defaultView || e.parentWindow
      : !1;
  }

  var n,
    r,
    i = e.document,
    s = e.location,
    o = e.navigator,
    u = e.jQuery,
    a = e.$,
    f = Array.prototype.push,
    l = Array.prototype.slice,
    c = Array.prototype.indexOf,
    h = Object.prototype.toString,
    p = Object.prototype.hasOwnProperty,
    d = String.prototype.trim,
    v = function v(e, t) {
      return new v.fn.init(e, t, n);
    },
    m = /[\-+]?(?:\d*\.|)\d+(?:[eE][\-+]?\d+|)/.source,
    g = /\S/,
    y = /\s+/,
    b = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
    w = /^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/,
    E = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
    S = /^[\],:{}\s]*$/,
    x = /(?:^|:|,)(?:\s*\[)+/g,
    T = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,
    N = /"[^"\\\r\n]*"|true|false|null|-?(?:\d\d*\.|)\d+(?:[eE][\-+]?\d+|)/g,
    C = /^-ms-/,
    k = /-([\da-z])/gi,
    L = function L(e, t) {
      return (t + "").toUpperCase();
    },
    A = function A() {
      i.addEventListener
        ? (i.removeEventListener("DOMContentLoaded", A, !1), v.ready())
        : i.readyState === "complete" &&
          (i.detachEvent("onreadystatechange", A), v.ready());
    },
    O = {};

  (v.fn = v.prototype = {
    constructor: v,
    init: function init(e, n, r) {
      var s, o, u, a;
      if (!e) return this;
      if (e.nodeType)
        return (this.context = this[0] = e), (this.length = 1), this;

      if (typeof e == "string") {
        e.charAt(0) === "<" && e.charAt(e.length - 1) === ">" && e.length >= 3
          ? (s = [null, e, null])
          : (s = w.exec(e));

        if (s && (s[1] || !n)) {
          if (s[1])
            return (
              (n = n instanceof v ? n[0] : n),
              (a = n && n.nodeType ? n.ownerDocument || n : i),
              (e = v.parseHTML(s[1], a, !0)),
              E.test(s[1]) && v.isPlainObject(n) && this.attr.call(e, n, !0),
              v.merge(this, e)
            );
          o = i.getElementById(s[2]);

          if (o && o.parentNode) {
            if (o.id !== s[2]) return r.find(e);
            (this.length = 1), (this[0] = o);
          }

          return (this.context = i), (this.selector = e), this;
        }

        return !n || n.jquery ? (n || r).find(e) : this.constructor(n).find(e);
      }

      return v.isFunction(e)
        ? r.ready(e)
        : (e.selector !== t &&
            ((this.selector = e.selector), (this.context = e.context)),
          v.makeArray(e, this));
    },
    selector: "",
    jquery: "1.8.3",
    length: 0,
    size: function size() {
      return this.length;
    },
    toArray: function toArray() {
      return l.call(this);
    },
    get: function get(e) {
      return e == null
        ? this.toArray()
        : e < 0
        ? this[this.length + e]
        : this[e];
    },
    pushStack: function pushStack(e, t, n) {
      var r = v.merge(this.constructor(), e);
      return (
        (r.prevObject = this),
        (r.context = this.context),
        t === "find"
          ? (r.selector = this.selector + (this.selector ? " " : "") + n)
          : t && (r.selector = this.selector + "." + t + "(" + n + ")"),
        r
      );
    },
    each: function each(e, t) {
      return v.each(this, e, t);
    },
    ready: function ready(e) {
      return v.ready.promise().done(e), this;
    },
    eq: function eq(e) {
      return (e = +e), e === -1 ? this.slice(e) : this.slice(e, e + 1);
    },
    first: function first() {
      return this.eq(0);
    },
    last: function last() {
      return this.eq(-1);
    },
    slice: function slice() {
      return this.pushStack(
        l.apply(this, arguments),
        "slice",
        l.call(arguments).join(",")
      );
    },
    map: function map(e) {
      return this.pushStack(
        v.map(this, function (t, n) {
          return e.call(t, n, t);
        })
      );
    },
    end: function end() {
      return this.prevObject || this.constructor(null);
    },
    push: f,
    sort: [].sort,
    splice: [].splice
  }),
    (v.fn.init.prototype = v.fn),
    (v.extend = v.fn.extend = function () {
      var e,
        n,
        r,
        i,
        s,
        o,
        u = arguments[0] || {},
        a = 1,
        f = arguments.length,
        l = !1;
      typeof u == "boolean" && ((l = u), (u = arguments[1] || {}), (a = 2)),
        _typeof(u) != "object" && !v.isFunction(u) && (u = {}),
        f === a && ((u = this), --a);

      for (; a < f; a++) {
        if ((e = arguments[a]) != null)
          for (n in e) {
            (r = u[n]), (i = e[n]);
            if (u === i) continue;
            l && i && (v.isPlainObject(i) || (s = v.isArray(i)))
              ? (s
                  ? ((s = !1), (o = r && v.isArray(r) ? r : []))
                  : (o = r && v.isPlainObject(r) ? r : {}),
                (u[n] = v.extend(l, o, i)))
              : i !== t && (u[n] = i);
          }
      }

      return u;
    }),
    v.extend({
      noConflict: function noConflict(t) {
        return e.$ === v && (e.$ = a), t && e.jQuery === v && (e.jQuery = u), v;
      },
      isReady: !1,
      readyWait: 1,
      holdReady: function holdReady(e) {
        e ? v.readyWait++ : v.ready(!0);
      },
      ready: function ready(e) {
        if (e === !0 ? --v.readyWait : v.isReady) return;
        if (!i.body) return setTimeout(v.ready, 1);
        v.isReady = !0;
        if (e !== !0 && --v.readyWait > 0) return;
        r.resolveWith(i, [v]),
          v.fn.trigger && v(i).trigger("ready").off("ready");
      },
      isFunction: function isFunction(e) {
        return v.type(e) === "function";
      },
      isArray:
        Array.isArray ||
        function (e) {
          return v.type(e) === "array";
        },
      isWindow: function isWindow(e) {
        return e != null && e == e.window;
      },
      isNumeric: function isNumeric(e) {
        return !isNaN(parseFloat(e)) && isFinite(e);
      },
      type: function type(e) {
        return e == null ? String(e) : O[h.call(e)] || "object";
      },
      isPlainObject: function isPlainObject(e) {
        if (!e || v.type(e) !== "object" || e.nodeType || v.isWindow(e))
          return !1;

        try {
          if (
            e.constructor &&
            !p.call(e, "constructor") &&
            !p.call(e.constructor.prototype, "isPrototypeOf")
          )
            return !1;
        } catch (n) {
          return !1;
        }

        var r;

        for (r in e) {
        }

        return r === t || p.call(e, r);
      },
      isEmptyObject: function isEmptyObject(e) {
        var t;

        for (t in e) {
          return !1;
        }

        return !0;
      },
      error: function error(e) {
        throw new Error(e);
      },
      parseHTML: function parseHTML(e, t, n) {
        var r;
        return !e || typeof e != "string"
          ? null
          : (typeof t == "boolean" && ((n = t), (t = 0)),
            (t = t || i),
            (r = E.exec(e))
              ? [t.createElement(r[1])]
              : ((r = v.buildFragment([e], t, n ? null : [])),
                v.merge(
                  [],
                  (r.cacheable ? v.clone(r.fragment) : r.fragment).childNodes
                )));
      },
      parseJSON: function parseJSON(t) {
        if (!t || typeof t != "string") return null;
        t = v.trim(t);
        if (e.JSON && e.JSON.parse) return e.JSON.parse(t);
        if (S.test(t.replace(T, "@").replace(N, "]").replace(x, "")))
          return new Function("return " + t)();
        v.error("Invalid JSON: " + t);
      },
      parseXML: function parseXML(n) {
        var r, i;
        if (!n || typeof n != "string") return null;

        try {
          e.DOMParser
            ? ((i = new DOMParser()), (r = i.parseFromString(n, "text/xml")))
            : ((r = new ActiveXObject("Microsoft.XMLDOM")),
              (r.async = "false"),
              r.loadXML(n));
        } catch (s) {
          r = t;
        }

        return (
          (!r ||
            !r.documentElement ||
            r.getElementsByTagName("parsererror").length) &&
            v.error("Invalid XML: " + n),
          r
        );
      },
      noop: function noop() {},
      globalEval: function globalEval(t) {
        t &&
          g.test(t) &&
          (
            e.execScript ||
            function (t) {
              e.eval.call(e, t);
            }
          )(t);
      },
      camelCase: function camelCase(e) {
        return e.replace(C, "ms-").replace(k, L);
      },
      nodeName: function nodeName(e, t) {
        return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase();
      },
      each: function each(e, n, r) {
        var i,
          s = 0,
          o = e.length,
          u = o === t || v.isFunction(e);

        if (r) {
          if (u) {
            for (i in e) {
              if (n.apply(e[i], r) === !1) break;
            }
          } else
            for (; s < o; ) {
              if (n.apply(e[s++], r) === !1) break;
            }
        } else if (u) {
          for (i in e) {
            if (n.call(e[i], i, e[i]) === !1) break;
          }
        } else
          for (; s < o; ) {
            if (n.call(e[s], s, e[s++]) === !1) break;
          }

        return e;
      },
      trim:
        d && !d.call("\uFEFF\xA0")
          ? function (e) {
              return e == null ? "" : d.call(e);
            }
          : function (e) {
              return e == null ? "" : (e + "").replace(b, "");
            },
      makeArray: function makeArray(e, t) {
        var n,
          r = t || [];
        return (
          e != null &&
            ((n = v.type(e)),
            e.length == null ||
            n === "string" ||
            n === "function" ||
            n === "regexp" ||
            v.isWindow(e)
              ? f.call(r, e)
              : v.merge(r, e)),
          r
        );
      },
      inArray: function inArray(e, t, n) {
        var r;

        if (t) {
          if (c) return c.call(t, e, n);
          (r = t.length), (n = n ? (n < 0 ? Math.max(0, r + n) : n) : 0);

          for (; n < r; n++) {
            if (n in t && t[n] === e) return n;
          }
        }

        return -1;
      },
      merge: function merge(e, n) {
        var r = n.length,
          i = e.length,
          s = 0;
        if (typeof r == "number")
          for (; s < r; s++) {
            e[i++] = n[s];
          }
        else
          while (n[s] !== t) {
            e[i++] = n[s++];
          }
        return (e.length = i), e;
      },
      grep: function grep(e, t, n) {
        var r,
          i = [],
          s = 0,
          o = e.length;
        n = !!n;

        for (; s < o; s++) {
          (r = !!t(e[s], s)), n !== r && i.push(e[s]);
        }

        return i;
      },
      map: function map(e, n, r) {
        var i,
          s,
          o = [],
          u = 0,
          a = e.length,
          f =
            e instanceof v ||
            (a !== t &&
              typeof a == "number" &&
              ((a > 0 && e[0] && e[a - 1]) || a === 0 || v.isArray(e)));
        if (f)
          for (; u < a; u++) {
            (i = n(e[u], u, r)), i != null && (o[o.length] = i);
          }
        else
          for (s in e) {
            (i = n(e[s], s, r)), i != null && (o[o.length] = i);
          }
        return o.concat.apply([], o);
      },
      guid: 1,
      proxy: function proxy(e, n) {
        var r, i, s;
        return (
          typeof n == "string" && ((r = e[n]), (n = e), (e = r)),
          v.isFunction(e)
            ? ((i = l.call(arguments, 2)),
              (s = function s() {
                return e.apply(n, i.concat(l.call(arguments)));
              }),
              (s.guid = e.guid = e.guid || v.guid++),
              s)
            : t
        );
      },
      access: function access(e, n, r, i, s, o, u) {
        var a,
          f = r == null,
          l = 0,
          c = e.length;

        if (r && _typeof(r) == "object") {
          for (l in r) {
            v.access(e, n, l, r[l], 1, o, i);
          }

          s = 1;
        } else if (i !== t) {
          (a = u === t && v.isFunction(i)),
            f &&
              (a
                ? ((a = n),
                  (n = function n(e, t, _n2) {
                    return a.call(v(e), _n2);
                  }))
                : (n.call(e, i), (n = null)));
          if (n)
            for (; l < c; l++) {
              n(e[l], r, a ? i.call(e[l], l, n(e[l], r)) : i, u);
            }
          s = 1;
        }

        return s ? e : f ? n.call(e) : c ? n(e[0], r) : o;
      },
      now: function now() {
        return new Date().getTime();
      }
    }),
    (v.ready.promise = function (t) {
      if (!r) {
        r = v.Deferred();
        if (i.readyState === "complete") setTimeout(v.ready, 1);
        else if (i.addEventListener)
          i.addEventListener("DOMContentLoaded", A, !1),
            e.addEventListener("load", v.ready, !1);
        else {
          i.attachEvent("onreadystatechange", A),
            e.attachEvent("onload", v.ready);
          var n = !1;

          try {
            n = e.frameElement == null && i.documentElement;
          } catch (s) {}

          n &&
            n.doScroll &&
            (function o() {
              if (!v.isReady) {
                try {
                  n.doScroll("left");
                } catch (e) {
                  return setTimeout(o, 50);
                }

                v.ready();
              }
            })();
        }
      }

      return r.promise(t);
    }),
    v.each(
      "Boolean Number String Function Array Date RegExp Object".split(" "),
      function (e, t) {
        O["[object " + t + "]"] = t.toLowerCase();
      }
    ),
    (n = v(i));
  var M = {};
  (v.Callbacks = function (e) {
    e = typeof e == "string" ? M[e] || _(e) : v.extend({}, e);

    var n,
      r,
      i,
      s,
      o,
      u,
      a = [],
      f = !e.once && [],
      l = function l(t) {
        (n = e.memory && t),
          (r = !0),
          (u = s || 0),
          (s = 0),
          (o = a.length),
          (i = !0);

        for (; a && u < o; u++) {
          if (a[u].apply(t[0], t[1]) === !1 && e.stopOnFalse) {
            n = !1;
            break;
          }
        }

        (i = !1),
          a && (f ? f.length && l(f.shift()) : n ? (a = []) : c.disable());
      },
      c = {
        add: function add() {
          if (a) {
            var t = a.length;
            (function r(t) {
              v.each(t, function (t, n) {
                var i = v.type(n);
                i === "function"
                  ? (!e.unique || !c.has(n)) && a.push(n)
                  : n && n.length && i !== "string" && r(n);
              });
            })(arguments),
              i ? (o = a.length) : n && ((s = t), l(n));
          }

          return this;
        },
        remove: function remove() {
          return (
            a &&
              v.each(arguments, function (e, t) {
                var n;

                while ((n = v.inArray(t, a, n)) > -1) {
                  a.splice(n, 1), i && (n <= o && o--, n <= u && u--);
                }
              }),
            this
          );
        },
        has: function has(e) {
          return v.inArray(e, a) > -1;
        },
        empty: function empty() {
          return (a = []), this;
        },
        disable: function disable() {
          return (a = f = n = t), this;
        },
        disabled: function disabled() {
          return !a;
        },
        lock: function lock() {
          return (f = t), n || c.disable(), this;
        },
        locked: function locked() {
          return !f;
        },
        fireWith: function fireWith(e, t) {
          return (
            (t = t || []),
            (t = [e, t.slice ? t.slice() : t]),
            a && (!r || f) && (i ? f.push(t) : l(t)),
            this
          );
        },
        fire: function fire() {
          return c.fireWith(this, arguments), this;
        },
        fired: function fired() {
          return !!r;
        }
      };

    return c;
  }),
    v.extend({
      Deferred: function Deferred(e) {
        var t = [
            ["resolve", "done", v.Callbacks("once memory"), "resolved"],
            ["reject", "fail", v.Callbacks("once memory"), "rejected"],
            ["notify", "progress", v.Callbacks("memory")]
          ],
          n = "pending",
          r = {
            state: function state() {
              return n;
            },
            always: function always() {
              return i.done(arguments).fail(arguments), this;
            },
            then: function then() {
              var e = arguments;
              return v
                .Deferred(function (n) {
                  v.each(t, function (t, r) {
                    var s = r[0],
                      o = e[t];
                    i[r[1]](
                      v.isFunction(o)
                        ? function () {
                            var e = o.apply(this, arguments);
                            e && v.isFunction(e.promise)
                              ? e
                                  .promise()
                                  .done(n.resolve)
                                  .fail(n.reject)
                                  .progress(n.notify)
                              : n[s + "With"](this === i ? n : this, [e]);
                          }
                        : n[s]
                    );
                  }),
                    (e = null);
                })
                .promise();
            },
            promise: function promise(e) {
              return e != null ? v.extend(e, r) : r;
            }
          },
          i = {};
        return (
          (r.pipe = r.then),
          v.each(t, function (e, s) {
            var o = s[2],
              u = s[3];
            (r[s[1]] = o.add),
              u &&
                o.add(
                  function () {
                    n = u;
                  },
                  t[e ^ 1][2].disable,
                  t[2][2].lock
                ),
              (i[s[0]] = o.fire),
              (i[s[0] + "With"] = o.fireWith);
          }),
          r.promise(i),
          e && e.call(i, i),
          i
        );
      },
      when: function when(e) {
        var t = 0,
          n = l.call(arguments),
          r = n.length,
          i = r !== 1 || (e && v.isFunction(e.promise)) ? r : 0,
          s = i === 1 ? e : v.Deferred(),
          o = function o(e, t, n) {
            return function (r) {
              (t[e] = this),
                (n[e] = arguments.length > 1 ? l.call(arguments) : r),
                n === u ? s.notifyWith(t, n) : --i || s.resolveWith(t, n);
            };
          },
          u,
          a,
          f;

        if (r > 1) {
          (u = new Array(r)), (a = new Array(r)), (f = new Array(r));

          for (; t < r; t++) {
            n[t] && v.isFunction(n[t].promise)
              ? n[t]
                  .promise()
                  .done(o(t, f, n))
                  .fail(s.reject)
                  .progress(o(t, a, u))
              : --i;
          }
        }

        return i || s.resolveWith(f, n), s.promise();
      }
    }),
    (v.support = (function () {
      var t,
        n,
        r,
        s,
        o,
        u,
        a,
        f,
        l,
        c,
        h,
        p = i.createElement("div");
      p.setAttribute("className", "t"),
        (p.innerHTML =
          "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>"),
        (n = p.getElementsByTagName("*")),
        (r = p.getElementsByTagName("a")[0]);
      if (!n || !r || !n.length) return {};
      (s = i.createElement("select")),
        (o = s.appendChild(i.createElement("option"))),
        (u = p.getElementsByTagName("input")[0]),
        (r.style.cssText = "top:1px;float:left;opacity:.5"),
        (t = {
          leadingWhitespace: p.firstChild.nodeType === 3,
          tbody: !p.getElementsByTagName("tbody").length,
          htmlSerialize: !!p.getElementsByTagName("link").length,
          style: /top/.test(r.getAttribute("style")),
          hrefNormalized: r.getAttribute("href") === "/a",
          opacity: /^0.5/.test(r.style.opacity),
          cssFloat: !!r.style.cssFloat,
          checkOn: u.value === "on",
          optSelected: o.selected,
          getSetAttribute: p.className !== "t",
          enctype: !!i.createElement("form").enctype,
          html5Clone:
            i.createElement("nav").cloneNode(!0).outerHTML !== "<:nav></:nav>",
          boxModel: i.compatMode === "CSS1Compat",
          submitBubbles: !0,
          changeBubbles: !0,
          focusinBubbles: !1,
          deleteExpando: !0,
          noCloneEvent: !0,
          inlineBlockNeedsLayout: !1,
          shrinkWrapBlocks: !1,
          reliableMarginRight: !0,
          boxSizingReliable: !0,
          pixelPosition: !1
        }),
        (u.checked = !0),
        (t.noCloneChecked = u.cloneNode(!0).checked),
        (s.disabled = !0),
        (t.optDisabled = !o.disabled);

      try {
        delete p.test;
      } catch (d) {
        t.deleteExpando = !1;
      }

      !p.addEventListener &&
        p.attachEvent &&
        p.fireEvent &&
        (p.attachEvent(
          "onclick",
          (h = function h() {
            t.noCloneEvent = !1;
          })
        ),
        p.cloneNode(!0).fireEvent("onclick"),
        p.detachEvent("onclick", h)),
        (u = i.createElement("input")),
        (u.value = "t"),
        u.setAttribute("type", "radio"),
        (t.radioValue = u.value === "t"),
        u.setAttribute("checked", "checked"),
        u.setAttribute("name", "t"),
        p.appendChild(u),
        (a = i.createDocumentFragment()),
        a.appendChild(p.lastChild),
        (t.checkClone = a.cloneNode(!0).cloneNode(!0).lastChild.checked),
        (t.appendChecked = u.checked),
        a.removeChild(u),
        a.appendChild(p);
      if (p.attachEvent)
        for (l in {
          submit: !0,
          change: !0,
          focusin: !0
        }) {
          (f = "on" + l),
            (c = f in p),
            c ||
              (p.setAttribute(f, "return;"), (c = typeof p[f] == "function")),
            (t[l + "Bubbles"] = c);
        }
      return (
        v(function () {
          var n,
            r,
            s,
            o,
            u = "padding:0;margin:0;border:0;display:block;overflow:hidden;",
            a = i.getElementsByTagName("body")[0];
          if (!a) return;
          (n = i.createElement("div")),
            (n.style.cssText =
              "visibility:hidden;border:0;width:0;height:0;position:static;top:0;margin-top:1px"),
            a.insertBefore(n, a.firstChild),
            (r = i.createElement("div")),
            n.appendChild(r),
            (r.innerHTML = "<table><tr><td></td><td>t</td></tr></table>"),
            (s = r.getElementsByTagName("td")),
            (s[0].style.cssText = "padding:0;margin:0;border:0;display:none"),
            (c = s[0].offsetHeight === 0),
            (s[0].style.display = ""),
            (s[1].style.display = "none"),
            (t.reliableHiddenOffsets = c && s[0].offsetHeight === 0),
            (r.innerHTML = ""),
            (r.style.cssText =
              "box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;"),
            (t.boxSizing = r.offsetWidth === 4),
            (t.doesNotIncludeMarginInBodyOffset = a.offsetTop !== 1),
            e.getComputedStyle &&
              ((t.pixelPosition =
                (e.getComputedStyle(r, null) || {}).top !== "1%"),
              (t.boxSizingReliable =
                (
                  e.getComputedStyle(r, null) || {
                    width: "4px"
                  }
                ).width === "4px"),
              (o = i.createElement("div")),
              (o.style.cssText = r.style.cssText = u),
              (o.style.marginRight = o.style.width = "0"),
              (r.style.width = "1px"),
              r.appendChild(o),
              (t.reliableMarginRight = !parseFloat(
                (e.getComputedStyle(o, null) || {}).marginRight
              ))),
            typeof r.style.zoom != "undefined" &&
              ((r.innerHTML = ""),
              (r.style.cssText =
                u + "width:1px;padding:1px;display:inline;zoom:1"),
              (t.inlineBlockNeedsLayout = r.offsetWidth === 3),
              (r.style.display = "block"),
              (r.style.overflow = "visible"),
              (r.innerHTML = "<div></div>"),
              (r.firstChild.style.width = "5px"),
              (t.shrinkWrapBlocks = r.offsetWidth !== 3),
              (n.style.zoom = 1)),
            a.removeChild(n),
            (n = r = s = o = null);
        }),
        a.removeChild(p),
        (n = r = s = o = u = a = p = null),
        t
      );
    })());
  var D = /(?:\{[\s\S]*\}|\[[\s\S]*\])$/,
    P = /([A-Z])/g;
  v.extend({
    cache: {},
    deletedIds: [],
    uuid: 0,
    expando: "jQuery" + (v.fn.jquery + Math.random()).replace(/\D/g, ""),
    noData: {
      embed: !0,
      object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
      applet: !0
    },
    hasData: function hasData(e) {
      return (
        (e = e.nodeType ? v.cache[e[v.expando]] : e[v.expando]), !!e && !B(e)
      );
    },
    data: function data(e, n, r, i) {
      if (!v.acceptData(e)) return;
      var s,
        o,
        u = v.expando,
        a = typeof n == "string",
        f = e.nodeType,
        l = f ? v.cache : e,
        c = f ? e[u] : e[u] && u;
      if ((!c || !l[c] || (!i && !l[c].data)) && a && r === t) return;
      c || (f ? (e[u] = c = v.deletedIds.pop() || v.guid++) : (c = u)),
        l[c] || ((l[c] = {}), f || (l[c].toJSON = v.noop));
      if (_typeof(n) == "object" || typeof n == "function")
        i ? (l[c] = v.extend(l[c], n)) : (l[c].data = v.extend(l[c].data, n));
      return (
        (s = l[c]),
        i || (s.data || (s.data = {}), (s = s.data)),
        r !== t && (s[v.camelCase(n)] = r),
        a ? ((o = s[n]), o == null && (o = s[v.camelCase(n)])) : (o = s),
        o
      );
    },
    removeData: function removeData(e, t, n) {
      if (!v.acceptData(e)) return;
      var r,
        i,
        s,
        o = e.nodeType,
        u = o ? v.cache : e,
        a = o ? e[v.expando] : v.expando;
      if (!u[a]) return;

      if (t) {
        r = n ? u[a] : u[a].data;

        if (r) {
          v.isArray(t) ||
            (t in r
              ? (t = [t])
              : ((t = v.camelCase(t)),
                t in r ? (t = [t]) : (t = t.split(" "))));

          for (i = 0, s = t.length; i < s; i++) {
            delete r[t[i]];
          }

          if (!(n ? B : v.isEmptyObject)(r)) return;
        }
      }

      if (!n) {
        delete u[a].data;
        if (!B(u[a])) return;
      }

      o
        ? v.cleanData([e], !0)
        : v.support.deleteExpando || u != u.window
        ? delete u[a]
        : (u[a] = null);
    },
    _data: function _data(e, t, n) {
      return v.data(e, t, n, !0);
    },
    acceptData: function acceptData(e) {
      var t = e.nodeName && v.noData[e.nodeName.toLowerCase()];
      return !t || (t !== !0 && e.getAttribute("classid") === t);
    }
  }),
    v.fn.extend({
      data: function data(e, n) {
        var r,
          i,
          s,
          o,
          u,
          a = this[0],
          f = 0,
          l = null;

        if (e === t) {
          if (this.length) {
            l = v.data(a);

            if (a.nodeType === 1 && !v._data(a, "parsedAttrs")) {
              s = a.attributes;

              for (u = s.length; f < u; f++) {
                (o = s[f].name),
                  o.indexOf("data-") ||
                    ((o = v.camelCase(o.substring(5))), H(a, o, l[o]));
              }

              v._data(a, "parsedAttrs", !0);
            }
          }

          return l;
        }

        return _typeof(e) == "object"
          ? this.each(function () {
              v.data(this, e);
            })
          : ((r = e.split(".", 2)),
            (r[1] = r[1] ? "." + r[1] : ""),
            (i = r[1] + "!"),
            v.access(
              this,
              function (n) {
                if (n === t)
                  return (
                    (l = this.triggerHandler("getData" + i, [r[0]])),
                    l === t && a && ((l = v.data(a, e)), (l = H(a, e, l))),
                    l === t && r[1] ? this.data(r[0]) : l
                  );
                (r[1] = n),
                  this.each(function () {
                    var t = v(this);
                    t.triggerHandler("setData" + i, r),
                      v.data(this, e, n),
                      t.triggerHandler("changeData" + i, r);
                  });
              },
              null,
              n,
              arguments.length > 1,
              null,
              !1
            ));
      },
      removeData: function removeData(e) {
        return this.each(function () {
          v.removeData(this, e);
        });
      }
    }),
    v.extend({
      queue: function queue(e, t, n) {
        var r;
        if (e)
          return (
            (t = (t || "fx") + "queue"),
            (r = v._data(e, t)),
            n &&
              (!r || v.isArray(n)
                ? (r = v._data(e, t, v.makeArray(n)))
                : r.push(n)),
            r || []
          );
      },
      dequeue: function dequeue(e, t) {
        t = t || "fx";

        var n = v.queue(e, t),
          r = n.length,
          i = n.shift(),
          s = v._queueHooks(e, t),
          o = function o() {
            v.dequeue(e, t);
          };

        i === "inprogress" && ((i = n.shift()), r--),
          i &&
            (t === "fx" && n.unshift("inprogress"),
            delete s.stop,
            i.call(e, o, s)),
          !r && s && s.empty.fire();
      },
      _queueHooks: function _queueHooks(e, t) {
        var n = t + "queueHooks";
        return (
          v._data(e, n) ||
          v._data(e, n, {
            empty: v.Callbacks("once memory").add(function () {
              v.removeData(e, t + "queue", !0), v.removeData(e, n, !0);
            })
          })
        );
      }
    }),
    v.fn.extend({
      queue: function queue(e, n) {
        var r = 2;
        return (
          typeof e != "string" && ((n = e), (e = "fx"), r--),
          arguments.length < r
            ? v.queue(this[0], e)
            : n === t
            ? this
            : this.each(function () {
                var t = v.queue(this, e, n);
                v._queueHooks(this, e),
                  e === "fx" && t[0] !== "inprogress" && v.dequeue(this, e);
              })
        );
      },
      dequeue: function dequeue(e) {
        return this.each(function () {
          v.dequeue(this, e);
        });
      },
      delay: function delay(e, t) {
        return (
          (e = v.fx ? v.fx.speeds[e] || e : e),
          (t = t || "fx"),
          this.queue(t, function (t, n) {
            var r = setTimeout(t, e);

            n.stop = function () {
              clearTimeout(r);
            };
          })
        );
      },
      clearQueue: function clearQueue(e) {
        return this.queue(e || "fx", []);
      },
      promise: function promise(e, n) {
        var r,
          i = 1,
          s = v.Deferred(),
          o = this,
          u = this.length,
          a = function a() {
            --i || s.resolveWith(o, [o]);
          };

        typeof e != "string" && ((n = e), (e = t)), (e = e || "fx");

        while (u--) {
          (r = v._data(o[u], e + "queueHooks")),
            r && r.empty && (i++, r.empty.add(a));
        }

        return a(), s.promise(n);
      }
    });
  var j,
    F,
    I,
    q = /[\t\r\n]/g,
    R = /\r/g,
    U = /^(?:button|input)$/i,
    z = /^(?:button|input|object|select|textarea)$/i,
    W = /^a(?:rea|)$/i,
    X = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
    V = v.support.getSetAttribute;
  v.fn.extend({
    attr: function attr(e, t) {
      return v.access(this, v.attr, e, t, arguments.length > 1);
    },
    removeAttr: function removeAttr(e) {
      return this.each(function () {
        v.removeAttr(this, e);
      });
    },
    prop: function prop(e, t) {
      return v.access(this, v.prop, e, t, arguments.length > 1);
    },
    removeProp: function removeProp(e) {
      return (
        (e = v.propFix[e] || e),
        this.each(function () {
          try {
            (this[e] = t), delete this[e];
          } catch (n) {}
        })
      );
    },
    addClass: function addClass(e) {
      var t, n, r, i, s, o, u;
      if (v.isFunction(e))
        return this.each(function (t) {
          v(this).addClass(e.call(this, t, this.className));
        });

      if (e && typeof e == "string") {
        t = e.split(y);

        for (n = 0, r = this.length; n < r; n++) {
          i = this[n];
          if (i.nodeType === 1)
            if (!i.className && t.length === 1) i.className = e;
            else {
              s = " " + i.className + " ";

              for (o = 0, u = t.length; o < u; o++) {
                s.indexOf(" " + t[o] + " ") < 0 && (s += t[o] + " ");
              }

              i.className = v.trim(s);
            }
        }
      }

      return this;
    },
    removeClass: function removeClass(e) {
      var n, r, i, s, o, u, a;
      if (v.isFunction(e))
        return this.each(function (t) {
          v(this).removeClass(e.call(this, t, this.className));
        });

      if ((e && typeof e == "string") || e === t) {
        n = (e || "").split(y);

        for (u = 0, a = this.length; u < a; u++) {
          i = this[u];

          if (i.nodeType === 1 && i.className) {
            r = (" " + i.className + " ").replace(q, " ");

            for (s = 0, o = n.length; s < o; s++) {
              while (r.indexOf(" " + n[s] + " ") >= 0) {
                r = r.replace(" " + n[s] + " ", " ");
              }
            }

            i.className = e ? v.trim(r) : "";
          }
        }
      }

      return this;
    },
    toggleClass: function toggleClass(e, t) {
      var n = _typeof(e),
        r = typeof t == "boolean";

      return v.isFunction(e)
        ? this.each(function (n) {
            v(this).toggleClass(e.call(this, n, this.className, t), t);
          })
        : this.each(function () {
            if (n === "string") {
              var i,
                s = 0,
                o = v(this),
                u = t,
                a = e.split(y);

              while ((i = a[s++])) {
                (u = r ? u : !o.hasClass(i)),
                  o[u ? "addClass" : "removeClass"](i);
              }
            } else if (n === "undefined" || n === "boolean") this.className && v._data(this, "__className__", this.className), (this.className = this.className || e === !1 ? "" : v._data(this, "__className__") || "");
          });
    },
    hasClass: function hasClass(e) {
      var t = " " + e + " ",
        n = 0,
        r = this.length;

      for (; n < r; n++) {
        if (
          this[n].nodeType === 1 &&
          (" " + this[n].className + " ").replace(q, " ").indexOf(t) >= 0
        )
          return !0;
      }

      return !1;
    },
    val: function val(e) {
      var n,
        r,
        i,
        s = this[0];

      if (!arguments.length) {
        if (s)
          return (
            (n = v.valHooks[s.type] || v.valHooks[s.nodeName.toLowerCase()]),
            n && "get" in n && (r = n.get(s, "value")) !== t
              ? r
              : ((r = s.value),
                typeof r == "string" ? r.replace(R, "") : r == null ? "" : r)
          );
        return;
      }

      return (
        (i = v.isFunction(e)),
        this.each(function (r) {
          var s,
            o = v(this);
          if (this.nodeType !== 1) return;
          i ? (s = e.call(this, r, o.val())) : (s = e),
            s == null
              ? (s = "")
              : typeof s == "number"
              ? (s += "")
              : v.isArray(s) &&
                (s = v.map(s, function (e) {
                  return e == null ? "" : e + "";
                })),
            (n =
              v.valHooks[this.type] || v.valHooks[this.nodeName.toLowerCase()]);
          if (!n || !("set" in n) || n.set(this, s, "value") === t)
            this.value = s;
        })
      );
    }
  }),
    v.extend({
      valHooks: {
        option: {
          get: function get(e) {
            var t = e.attributes.value;
            return !t || t.specified ? e.value : e.text;
          }
        },
        select: {
          get: function get(e) {
            var t,
              n,
              r = e.options,
              i = e.selectedIndex,
              s = e.type === "select-one" || i < 0,
              o = s ? null : [],
              u = s ? i + 1 : r.length,
              a = i < 0 ? u : s ? i : 0;

            for (; a < u; a++) {
              n = r[a];

              if (
                (n.selected || a === i) &&
                (v.support.optDisabled
                  ? !n.disabled
                  : n.getAttribute("disabled") === null) &&
                (!n.parentNode.disabled ||
                  !v.nodeName(n.parentNode, "optgroup"))
              ) {
                t = v(n).val();
                if (s) return t;
                o.push(t);
              }
            }

            return o;
          },
          set: function set(e, t) {
            var n = v.makeArray(t);
            return (
              v(e)
                .find("option")
                .each(function () {
                  this.selected = v.inArray(v(this).val(), n) >= 0;
                }),
              n.length || (e.selectedIndex = -1),
              n
            );
          }
        }
      },
      attrFn: {},
      attr: function attr(e, n, r, i) {
        var s,
          o,
          u,
          a = e.nodeType;
        if (!e || a === 3 || a === 8 || a === 2) return;
        if (i && v.isFunction(v.fn[n])) return v(e)[n](r);
        if (typeof e.getAttribute == "undefined") return v.prop(e, n, r);
        (u = a !== 1 || !v.isXMLDoc(e)),
          u &&
            ((n = n.toLowerCase()),
            (o = v.attrHooks[n] || (X.test(n) ? F : j)));

        if (r !== t) {
          if (r === null) {
            v.removeAttr(e, n);
            return;
          }

          return o && "set" in o && u && (s = o.set(e, r, n)) !== t
            ? s
            : (e.setAttribute(n, r + ""), r);
        }

        return o && "get" in o && u && (s = o.get(e, n)) !== null
          ? s
          : ((s = e.getAttribute(n)), s === null ? t : s);
      },
      removeAttr: function removeAttr(e, t) {
        var n,
          r,
          i,
          s,
          o = 0;

        if (t && e.nodeType === 1) {
          r = t.split(y);

          for (; o < r.length; o++) {
            (i = r[o]),
              i &&
                ((n = v.propFix[i] || i),
                (s = X.test(i)),
                s || v.attr(e, i, ""),
                e.removeAttribute(V ? i : n),
                s && n in e && (e[n] = !1));
          }
        }
      },
      attrHooks: {
        type: {
          set: function set(e, t) {
            if (U.test(e.nodeName) && e.parentNode)
              v.error("type property can't be changed");
            else if (
              !v.support.radioValue &&
              t === "radio" &&
              v.nodeName(e, "input")
            ) {
              var n = e.value;
              return e.setAttribute("type", t), n && (e.value = n), t;
            }
          }
        },
        value: {
          get: function get(e, t) {
            return j && v.nodeName(e, "button")
              ? j.get(e, t)
              : t in e
              ? e.value
              : null;
          },
          set: function set(e, t, n) {
            if (j && v.nodeName(e, "button")) return j.set(e, t, n);
            e.value = t;
          }
        }
      },
      propFix: {
        tabindex: "tabIndex",
        readonly: "readOnly",
        for: "htmlFor",
        class: "className",
        maxlength: "maxLength",
        cellspacing: "cellSpacing",
        cellpadding: "cellPadding",
        rowspan: "rowSpan",
        colspan: "colSpan",
        usemap: "useMap",
        frameborder: "frameBorder",
        contenteditable: "contentEditable"
      },
      prop: function prop(e, n, r) {
        var i,
          s,
          o,
          u = e.nodeType;
        if (!e || u === 3 || u === 8 || u === 2) return;
        return (
          (o = u !== 1 || !v.isXMLDoc(e)),
          o && ((n = v.propFix[n] || n), (s = v.propHooks[n])),
          r !== t
            ? s && "set" in s && (i = s.set(e, r, n)) !== t
              ? i
              : (e[n] = r)
            : s && "get" in s && (i = s.get(e, n)) !== null
            ? i
            : e[n]
        );
      },
      propHooks: {
        tabIndex: {
          get: function get(e) {
            var n = e.getAttributeNode("tabindex");
            return n && n.specified
              ? parseInt(n.value, 10)
              : z.test(e.nodeName) || (W.test(e.nodeName) && e.href)
              ? 0
              : t;
          }
        }
      }
    }),
    (F = {
      get: function get(e, n) {
        var r,
          i = v.prop(e, n);
        return i === !0 ||
          (typeof i != "boolean" &&
            (r = e.getAttributeNode(n)) &&
            r.nodeValue !== !1)
          ? n.toLowerCase()
          : t;
      },
      set: function set(e, t, n) {
        var r;
        return (
          t === !1
            ? v.removeAttr(e, n)
            : ((r = v.propFix[n] || n),
              r in e && (e[r] = !0),
              e.setAttribute(n, n.toLowerCase())),
          n
        );
      }
    }),
    V ||
      ((I = {
        name: !0,
        id: !0,
        coords: !0
      }),
      (j = v.valHooks.button = {
        get: function get(e, n) {
          var r;
          return (
            (r = e.getAttributeNode(n)),
            r && (I[n] ? r.value !== "" : r.specified) ? r.value : t
          );
        },
        set: function set(e, t, n) {
          var r = e.getAttributeNode(n);
          return (
            r || ((r = i.createAttribute(n)), e.setAttributeNode(r)),
            (r.value = t + "")
          );
        }
      }),
      v.each(["width", "height"], function (e, t) {
        v.attrHooks[t] = v.extend(v.attrHooks[t], {
          set: function set(e, n) {
            if (n === "") return e.setAttribute(t, "auto"), n;
          }
        });
      }),
      (v.attrHooks.contenteditable = {
        get: j.get,
        set: function set(e, t, n) {
          t === "" && (t = "false"), j.set(e, t, n);
        }
      })),
    v.support.hrefNormalized ||
      v.each(["href", "src", "width", "height"], function (e, n) {
        v.attrHooks[n] = v.extend(v.attrHooks[n], {
          get: function get(e) {
            var r = e.getAttribute(n, 2);
            return r === null ? t : r;
          }
        });
      }),
    v.support.style ||
      (v.attrHooks.style = {
        get: function get(e) {
          return e.style.cssText.toLowerCase() || t;
        },
        set: function set(e, t) {
          return (e.style.cssText = t + "");
        }
      }),
    v.support.optSelected ||
      (v.propHooks.selected = v.extend(v.propHooks.selected, {
        get: function get(e) {
          var t = e.parentNode;
          return (
            t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex),
            null
          );
        }
      })),
    v.support.enctype || (v.propFix.enctype = "encoding"),
    v.support.checkOn ||
      v.each(["radio", "checkbox"], function () {
        v.valHooks[this] = {
          get: function get(e) {
            return e.getAttribute("value") === null ? "on" : e.value;
          }
        };
      }),
    v.each(["radio", "checkbox"], function () {
      v.valHooks[this] = v.extend(v.valHooks[this], {
        set: function set(e, t) {
          if (v.isArray(t)) return (e.checked = v.inArray(v(e).val(), t) >= 0);
        }
      });
    });

  var $ = /^(?:textarea|input|select)$/i,
    J = /^([^\.]*|)(?:\.(.+)|)$/,
    K = /(?:^|\s)hover(\.\S+|)\b/,
    Q = /^key/,
    G = /^(?:mouse|contextmenu)|click/,
    Y = /^(?:focusinfocus|focusoutblur)$/,
    Z = function Z(e) {
      return v.event.special.hover
        ? e
        : e.replace(K, "mouseenter$1 mouseleave$1");
    };

  (v.event = {
    add: function add(e, n, r, i, s) {
      var o, _u, a, f, l, c, h, p, d, m, g;

      if (e.nodeType === 3 || e.nodeType === 8 || !n || !r || !(o = v._data(e)))
        return;
      r.handler && ((d = r), (r = d.handler), (s = d.selector)),
        r.guid || (r.guid = v.guid++),
        (a = o.events),
        a || (o.events = a = {}),
        (_u = o.handle),
        _u ||
          ((o.handle = _u = function u(e) {
            return typeof v == "undefined" ||
              (!!e && v.event.triggered === e.type)
              ? t
              : v.event.dispatch.apply(_u.elem, arguments);
          }),
          (_u.elem = e)),
        (n = v.trim(Z(n)).split(" "));

      for (f = 0; f < n.length; f++) {
        (l = J.exec(n[f]) || []),
          (c = l[1]),
          (h = (l[2] || "").split(".").sort()),
          (g = v.event.special[c] || {}),
          (c = (s ? g.delegateType : g.bindType) || c),
          (g = v.event.special[c] || {}),
          (p = v.extend(
            {
              type: c,
              origType: l[1],
              data: i,
              handler: r,
              guid: r.guid,
              selector: s,
              needsContext: s && v.expr.match.needsContext.test(s),
              namespace: h.join(".")
            },
            d
          )),
          (m = a[c]);

        if (!m) {
          (m = a[c] = []), (m.delegateCount = 0);
          if (!g.setup || g.setup.call(e, i, h, _u) === !1)
            e.addEventListener
              ? e.addEventListener(c, _u, !1)
              : e.attachEvent && e.attachEvent("on" + c, _u);
        }

        g.add &&
          (g.add.call(e, p), p.handler.guid || (p.handler.guid = r.guid)),
          s ? m.splice(m.delegateCount++, 0, p) : m.push(p),
          (v.event.global[c] = !0);
      }

      e = null;
    },
    global: {},
    remove: function remove(e, t, n, r, i) {
      var s,
        o,
        u,
        a,
        f,
        l,
        c,
        h,
        p,
        d,
        m,
        g = v.hasData(e) && v._data(e);

      if (!g || !(h = g.events)) return;
      t = v.trim(Z(t || "")).split(" ");

      for (s = 0; s < t.length; s++) {
        (o = J.exec(t[s]) || []), (u = a = o[1]), (f = o[2]);

        if (!u) {
          for (u in h) {
            v.event.remove(e, u + t[s], n, r, !0);
          }

          continue;
        }

        (p = v.event.special[u] || {}),
          (u = (r ? p.delegateType : p.bindType) || u),
          (d = h[u] || []),
          (l = d.length),
          (f = f
            ? new RegExp(
                "(^|\\.)" +
                  f.split(".").sort().join("\\.(?:.*\\.|)") +
                  "(\\.|$)"
              )
            : null);

        for (c = 0; c < d.length; c++) {
          (m = d[c]),
            (i || a === m.origType) &&
              (!n || n.guid === m.guid) &&
              (!f || f.test(m.namespace)) &&
              (!r || r === m.selector || (r === "**" && m.selector)) &&
              (d.splice(c--, 1),
              m.selector && d.delegateCount--,
              p.remove && p.remove.call(e, m));
        }

        d.length === 0 &&
          l !== d.length &&
          ((!p.teardown || p.teardown.call(e, f, g.handle) === !1) &&
            v.removeEvent(e, u, g.handle),
          delete h[u]);
      }

      v.isEmptyObject(h) && (delete g.handle, v.removeData(e, "events", !0));
    },
    customEvent: {
      getData: !0,
      setData: !0,
      changeData: !0
    },
    trigger: function trigger(n, r, s, o) {
      if (!s || (s.nodeType !== 3 && s.nodeType !== 8)) {
        var u,
          a,
          f,
          l,
          c,
          h,
          p,
          d,
          m,
          g,
          y = n.type || n,
          b = [];
        if (Y.test(y + v.event.triggered)) return;
        y.indexOf("!") >= 0 && ((y = y.slice(0, -1)), (a = !0)),
          y.indexOf(".") >= 0 &&
            ((b = y.split(".")), (y = b.shift()), b.sort());
        if ((!s || v.event.customEvent[y]) && !v.event.global[y]) return;
        (n =
          _typeof(n) == "object"
            ? n[v.expando]
              ? n
              : new v.Event(y, n)
            : new v.Event(y)),
          (n.type = y),
          (n.isTrigger = !0),
          (n.exclusive = a),
          (n.namespace = b.join(".")),
          (n.namespace_re = n.namespace
            ? new RegExp("(^|\\.)" + b.join("\\.(?:.*\\.|)") + "(\\.|$)")
            : null),
          (h = y.indexOf(":") < 0 ? "on" + y : "");

        if (!s) {
          u = v.cache;

          for (f in u) {
            u[f].events &&
              u[f].events[y] &&
              v.event.trigger(n, r, u[f].handle.elem, !0);
          }

          return;
        }

        (n.result = t),
          n.target || (n.target = s),
          (r = r != null ? v.makeArray(r) : []),
          r.unshift(n),
          (p = v.event.special[y] || {});
        if (p.trigger && p.trigger.apply(s, r) === !1) return;
        m = [[s, p.bindType || y]];

        if (!o && !p.noBubble && !v.isWindow(s)) {
          (g = p.delegateType || y), (l = Y.test(g + y) ? s : s.parentNode);

          for (c = s; l; l = l.parentNode) {
            m.push([l, g]), (c = l);
          }

          c === (s.ownerDocument || i) &&
            m.push([c.defaultView || c.parentWindow || e, g]);
        }

        for (f = 0; f < m.length && !n.isPropagationStopped(); f++) {
          (l = m[f][0]),
            (n.type = m[f][1]),
            (d = (v._data(l, "events") || {})[n.type] && v._data(l, "handle")),
            d && d.apply(l, r),
            (d = h && l[h]),
            d &&
              v.acceptData(l) &&
              d.apply &&
              d.apply(l, r) === !1 &&
              n.preventDefault();
        }

        return (
          (n.type = y),
          !o &&
            !n.isDefaultPrevented() &&
            (!p._default || p._default.apply(s.ownerDocument, r) === !1) &&
            (y !== "click" || !v.nodeName(s, "a")) &&
            v.acceptData(s) &&
            h &&
            s[y] &&
            ((y !== "focus" && y !== "blur") || n.target.offsetWidth !== 0) &&
            !v.isWindow(s) &&
            ((c = s[h]),
            c && (s[h] = null),
            (v.event.triggered = y),
            s[y](),
            (v.event.triggered = t),
            c && (s[h] = c)),
          n.result
        );
      }

      return;
    },
    dispatch: function dispatch(n) {
      n = v.event.fix(n || e.event);
      var r,
        i,
        s,
        o,
        u,
        a,
        f,
        c,
        h,
        p,
        d = (v._data(this, "events") || {})[n.type] || [],
        m = d.delegateCount,
        g = l.call(arguments),
        y = !n.exclusive && !n.namespace,
        b = v.event.special[n.type] || {},
        w = [];
      (g[0] = n), (n.delegateTarget = this);
      if (b.preDispatch && b.preDispatch.call(this, n) === !1) return;
      if (m && (!n.button || n.type !== "click"))
        for (s = n.target; s != this; s = s.parentNode || this) {
          if (s.disabled !== !0 || n.type !== "click") {
            (u = {}), (f = []);

            for (r = 0; r < m; r++) {
              (c = d[r]),
                (h = c.selector),
                u[h] === t &&
                  (u[h] = c.needsContext
                    ? v(h, this).index(s) >= 0
                    : v.find(h, this, null, [s]).length),
                u[h] && f.push(c);
            }

            f.length &&
              w.push({
                elem: s,
                matches: f
              });
          }
        }
      d.length > m &&
        w.push({
          elem: this,
          matches: d.slice(m)
        });

      for (r = 0; r < w.length && !n.isPropagationStopped(); r++) {
        (a = w[r]), (n.currentTarget = a.elem);

        for (
          i = 0;
          i < a.matches.length && !n.isImmediatePropagationStopped();
          i++
        ) {
          c = a.matches[i];
          if (
            y ||
            (!n.namespace && !c.namespace) ||
            (n.namespace_re && n.namespace_re.test(c.namespace))
          )
            (n.data = c.data),
              (n.handleObj = c),
              (o = (
                (v.event.special[c.origType] || {}).handle || c.handler
              ).apply(a.elem, g)),
              o !== t &&
                ((n.result = o),
                o === !1 && (n.preventDefault(), n.stopPropagation()));
        }
      }

      return b.postDispatch && b.postDispatch.call(this, n), n.result;
    },
    props: "attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(
      " "
    ),
    fixHooks: {},
    keyHooks: {
      props: "char charCode key keyCode".split(" "),
      filter: function filter(e, t) {
        return (
          e.which == null &&
            (e.which = t.charCode != null ? t.charCode : t.keyCode),
          e
        );
      }
    },
    mouseHooks: {
      props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(
        " "
      ),
      filter: function filter(e, n) {
        var r,
          s,
          o,
          u = n.button,
          a = n.fromElement;
        return (
          e.pageX == null &&
            n.clientX != null &&
            ((r = e.target.ownerDocument || i),
            (s = r.documentElement),
            (o = r.body),
            (e.pageX =
              n.clientX +
              ((s && s.scrollLeft) || (o && o.scrollLeft) || 0) -
              ((s && s.clientLeft) || (o && o.clientLeft) || 0)),
            (e.pageY =
              n.clientY +
              ((s && s.scrollTop) || (o && o.scrollTop) || 0) -
              ((s && s.clientTop) || (o && o.clientTop) || 0))),
          !e.relatedTarget &&
            a &&
            (e.relatedTarget = a === e.target ? n.toElement : a),
          !e.which &&
            u !== t &&
            (e.which = u & 1 ? 1 : u & 2 ? 3 : u & 4 ? 2 : 0),
          e
        );
      }
    },
    fix: function fix(e) {
      if (e[v.expando]) return e;
      var t,
        n,
        r = e,
        s = v.event.fixHooks[e.type] || {},
        o = s.props ? this.props.concat(s.props) : this.props;
      e = v.Event(r);

      for (t = o.length; t; ) {
        (n = o[--t]), (e[n] = r[n]);
      }

      return (
        e.target || (e.target = r.srcElement || i),
        e.target.nodeType === 3 && (e.target = e.target.parentNode),
        (e.metaKey = !!e.metaKey),
        s.filter ? s.filter(e, r) : e
      );
    },
    special: {
      load: {
        noBubble: !0
      },
      focus: {
        delegateType: "focusin"
      },
      blur: {
        delegateType: "focusout"
      },
      beforeunload: {
        setup: function setup(e, t, n) {
          v.isWindow(this) && (this.onbeforeunload = n);
        },
        teardown: function teardown(e, t) {
          this.onbeforeunload === t && (this.onbeforeunload = null);
        }
      }
    },
    simulate: function simulate(e, t, n, r) {
      var i = v.extend(new v.Event(), n, {
        type: e,
        isSimulated: !0,
        originalEvent: {}
      });
      r ? v.event.trigger(i, null, t) : v.event.dispatch.call(t, i),
        i.isDefaultPrevented() && n.preventDefault();
    }
  }),
    (v.event.handle = v.event.dispatch),
    (v.removeEvent = i.removeEventListener
      ? function (e, t, n) {
          e.removeEventListener && e.removeEventListener(t, n, !1);
        }
      : function (e, t, n) {
          var r = "on" + t;
          e.detachEvent &&
            (typeof e[r] == "undefined" && (e[r] = null), e.detachEvent(r, n));
        }),
    (v.Event = function (e, t) {
      if (!(this instanceof v.Event)) return new v.Event(e, t);
      e && e.type
        ? ((this.originalEvent = e),
          (this.type = e.type),
          (this.isDefaultPrevented =
            e.defaultPrevented ||
            e.returnValue === !1 ||
            (e.getPreventDefault && e.getPreventDefault())
              ? tt
              : et))
        : (this.type = e),
        t && v.extend(this, t),
        (this.timeStamp = (e && e.timeStamp) || v.now()),
        (this[v.expando] = !0);
    }),
    (v.Event.prototype = {
      preventDefault: function preventDefault() {
        this.isDefaultPrevented = tt;
        var e = this.originalEvent;
        if (!e) return;
        e.preventDefault ? e.preventDefault() : (e.returnValue = !1);
      },
      stopPropagation: function stopPropagation() {
        this.isPropagationStopped = tt;
        var e = this.originalEvent;
        if (!e) return;
        e.stopPropagation && e.stopPropagation(), (e.cancelBubble = !0);
      },
      stopImmediatePropagation: function stopImmediatePropagation() {
        (this.isImmediatePropagationStopped = tt), this.stopPropagation();
      },
      isDefaultPrevented: et,
      isPropagationStopped: et,
      isImmediatePropagationStopped: et
    }),
    v.each(
      {
        mouseenter: "mouseover",
        mouseleave: "mouseout"
      },
      function (e, t) {
        v.event.special[e] = {
          delegateType: t,
          bindType: t,
          handle: function handle(e) {
            var n,
              r = this,
              i = e.relatedTarget,
              s = e.handleObj,
              o = s.selector;
            if (!i || (i !== r && !v.contains(r, i)))
              (e.type = s.origType),
                (n = s.handler.apply(this, arguments)),
                (e.type = t);
            return n;
          }
        };
      }
    ),
    v.support.submitBubbles ||
      (v.event.special.submit = {
        setup: function setup() {
          if (v.nodeName(this, "form")) return !1;
          v.event.add(this, "click._submit keypress._submit", function (e) {
            var n = e.target,
              r =
                v.nodeName(n, "input") || v.nodeName(n, "button") ? n.form : t;
            r &&
              !v._data(r, "_submit_attached") &&
              (v.event.add(r, "submit._submit", function (e) {
                e._submit_bubble = !0;
              }),
              v._data(r, "_submit_attached", !0));
          });
        },
        postDispatch: function postDispatch(e) {
          e._submit_bubble &&
            (delete e._submit_bubble,
            this.parentNode &&
              !e.isTrigger &&
              v.event.simulate("submit", this.parentNode, e, !0));
        },
        teardown: function teardown() {
          if (v.nodeName(this, "form")) return !1;
          v.event.remove(this, "._submit");
        }
      }),
    v.support.changeBubbles ||
      (v.event.special.change = {
        setup: function setup() {
          if ($.test(this.nodeName)) {
            if (this.type === "checkbox" || this.type === "radio")
              v.event.add(this, "propertychange._change", function (e) {
                e.originalEvent.propertyName === "checked" &&
                  (this._just_changed = !0);
              }),
                v.event.add(this, "click._change", function (e) {
                  this._just_changed &&
                    !e.isTrigger &&
                    (this._just_changed = !1),
                    v.event.simulate("change", this, e, !0);
                });
            return !1;
          }

          v.event.add(this, "beforeactivate._change", function (e) {
            var t = e.target;
            $.test(t.nodeName) &&
              !v._data(t, "_change_attached") &&
              (v.event.add(t, "change._change", function (e) {
                this.parentNode &&
                  !e.isSimulated &&
                  !e.isTrigger &&
                  v.event.simulate("change", this.parentNode, e, !0);
              }),
              v._data(t, "_change_attached", !0));
          });
        },
        handle: function handle(e) {
          var t = e.target;
          if (
            this !== t ||
            e.isSimulated ||
            e.isTrigger ||
            (t.type !== "radio" && t.type !== "checkbox")
          )
            return e.handleObj.handler.apply(this, arguments);
        },
        teardown: function teardown() {
          return v.event.remove(this, "._change"), !$.test(this.nodeName);
        }
      }),
    v.support.focusinBubbles ||
      v.each(
        {
          focus: "focusin",
          blur: "focusout"
        },
        function (e, t) {
          var n = 0,
            r = function r(e) {
              v.event.simulate(t, e.target, v.event.fix(e), !0);
            };

          v.event.special[t] = {
            setup: function setup() {
              n++ === 0 && i.addEventListener(e, r, !0);
            },
            teardown: function teardown() {
              --n === 0 && i.removeEventListener(e, r, !0);
            }
          };
        }
      ),
    v.fn.extend({
      on: function on(e, n, r, i, s) {
        var o, u;

        if (_typeof(e) == "object") {
          typeof n != "string" && ((r = r || n), (n = t));

          for (u in e) {
            this.on(u, n, r, e[u], s);
          }

          return this;
        }

        r == null && i == null
          ? ((i = n), (r = n = t))
          : i == null &&
            (typeof n == "string"
              ? ((i = r), (r = t))
              : ((i = r), (r = n), (n = t)));
        if (i === !1) i = et;
        else if (!i) return this;
        return (
          s === 1 &&
            ((o = i),
            (i = function i(e) {
              return v().off(e), o.apply(this, arguments);
            }),
            (i.guid = o.guid || (o.guid = v.guid++))),
          this.each(function () {
            v.event.add(this, e, i, r, n);
          })
        );
      },
      one: function one(e, t, n, r) {
        return this.on(e, t, n, r, 1);
      },
      off: function off(e, n, r) {
        var i, s;
        if (e && e.preventDefault && e.handleObj)
          return (
            (i = e.handleObj),
            v(e.delegateTarget).off(
              i.namespace ? i.origType + "." + i.namespace : i.origType,
              i.selector,
              i.handler
            ),
            this
          );

        if (_typeof(e) == "object") {
          for (s in e) {
            this.off(s, n, e[s]);
          }

          return this;
        }

        if (n === !1 || typeof n == "function") (r = n), (n = t);
        return (
          r === !1 && (r = et),
          this.each(function () {
            v.event.remove(this, e, r, n);
          })
        );
      },
      bind: function bind(e, t, n) {
        return this.on(e, null, t, n);
      },
      unbind: function unbind(e, t) {
        return this.off(e, null, t);
      },
      live: function live(e, t, n) {
        return v(this.context).on(e, this.selector, t, n), this;
      },
      die: function die(e, t) {
        return v(this.context).off(e, this.selector || "**", t), this;
      },
      delegate: function delegate(e, t, n, r) {
        return this.on(t, e, n, r);
      },
      undelegate: function undelegate(e, t, n) {
        return arguments.length === 1
          ? this.off(e, "**")
          : this.off(t, e || "**", n);
      },
      trigger: function trigger(e, t) {
        return this.each(function () {
          v.event.trigger(e, t, this);
        });
      },
      triggerHandler: function triggerHandler(e, t) {
        if (this[0]) return v.event.trigger(e, t, this[0], !0);
      },
      toggle: function toggle(e) {
        var t = arguments,
          n = e.guid || v.guid++,
          r = 0,
          i = function i(n) {
            var i = (v._data(this, "lastToggle" + e.guid) || 0) % r;
            return (
              v._data(this, "lastToggle" + e.guid, i + 1),
              n.preventDefault(),
              t[i].apply(this, arguments) || !1
            );
          };

        i.guid = n;

        while (r < t.length) {
          t[r++].guid = n;
        }

        return this.click(i);
      },
      hover: function hover(e, t) {
        return this.mouseenter(e).mouseleave(t || e);
      }
    }),
    v.each(
      "blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(
        " "
      ),
      function (e, t) {
        (v.fn[t] = function (e, n) {
          return (
            n == null && ((n = e), (e = null)),
            arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
          );
        }),
          Q.test(t) && (v.event.fixHooks[t] = v.event.keyHooks),
          G.test(t) && (v.event.fixHooks[t] = v.event.mouseHooks);
      }
    ),
    (function (e, t) {
      function nt(e, t, n, r) {
        (n = n || []), (t = t || g);
        var i,
          s,
          a,
          f,
          l = t.nodeType;
        if (!e || typeof e != "string") return n;
        if (l !== 1 && l !== 9) return [];
        a = o(t);
        if (!a && !r)
          if ((i = R.exec(e)))
            if ((f = i[1])) {
              if (l === 9) {
                s = t.getElementById(f);
                if (!s || !s.parentNode) return n;
                if (s.id === f) return n.push(s), n;
              } else if (
                t.ownerDocument &&
                (s = t.ownerDocument.getElementById(f)) &&
                u(t, s) &&
                s.id === f
              )
                return n.push(s), n;
            } else {
              if (i[2])
                return S.apply(n, x.call(t.getElementsByTagName(e), 0)), n;
              if ((f = i[3]) && Z && t.getElementsByClassName)
                return S.apply(n, x.call(t.getElementsByClassName(f), 0)), n;
            }
        return vt(e.replace(j, "$1"), t, n, r, a);
      }

      function rt(e) {
        return function (t) {
          var n = t.nodeName.toLowerCase();
          return n === "input" && t.type === e;
        };
      }

      function it(e) {
        return function (t) {
          var n = t.nodeName.toLowerCase();
          return (n === "input" || n === "button") && t.type === e;
        };
      }

      function st(e) {
        return N(function (t) {
          return (
            (t = +t),
            N(function (n, r) {
              var i,
                s = e([], n.length, t),
                o = s.length;

              while (o--) {
                n[(i = s[o])] && (n[i] = !(r[i] = n[i]));
              }
            })
          );
        });
      }

      function ot(e, t, n) {
        if (e === t) return n;
        var r = e.nextSibling;

        while (r) {
          if (r === t) return -1;
          r = r.nextSibling;
        }

        return 1;
      }

      function ut(e, t) {
        var n,
          r,
          s,
          o,
          u,
          a,
          f,
          l = L[d][e + " "];
        if (l) return t ? 0 : l.slice(0);
        (u = e), (a = []), (f = i.preFilter);

        while (u) {
          if (!n || (r = F.exec(u)))
            r && (u = u.slice(r[0].length) || u), a.push((s = []));
          n = !1;
          if ((r = I.exec(u)))
            s.push((n = new m(r.shift()))),
              (u = u.slice(n.length)),
              (n.type = r[0].replace(j, " "));

          for (o in i.filter) {
            (r = J[o].exec(u)) &&
              (!f[o] || (r = f[o](r))) &&
              (s.push((n = new m(r.shift()))),
              (u = u.slice(n.length)),
              (n.type = o),
              (n.matches = r));
          }

          if (!n) break;
        }

        return t ? u.length : u ? nt.error(e) : L(e, a).slice(0);
      }

      function at(e, t, r) {
        var i = t.dir,
          s = r && t.dir === "parentNode",
          o = w++;
        return t.first
          ? function (t, n, r) {
              while ((t = t[i])) {
                if (s || t.nodeType === 1) return e(t, n, r);
              }
            }
          : function (t, r, u) {
              if (!u) {
                var a,
                  f = b + " " + o + " ",
                  l = f + n;

                while ((t = t[i])) {
                  if (s || t.nodeType === 1) {
                    if ((a = t[d]) === l) return t.sizset;

                    if (typeof a == "string" && a.indexOf(f) === 0) {
                      if (t.sizset) return t;
                    } else {
                      t[d] = l;
                      if (e(t, r, u)) return (t.sizset = !0), t;
                      t.sizset = !1;
                    }
                  }
                }
              } else
                while ((t = t[i])) {
                  if (s || t.nodeType === 1) if (e(t, r, u)) return t;
                }
            };
      }

      function ft(e) {
        return e.length > 1
          ? function (t, n, r) {
              var i = e.length;

              while (i--) {
                if (!e[i](t, n, r)) return !1;
              }

              return !0;
            }
          : e[0];
      }

      function lt(e, t, n, r, i) {
        var s,
          o = [],
          u = 0,
          a = e.length,
          f = t != null;

        for (; u < a; u++) {
          if ((s = e[u])) if (!n || n(s, r, i)) o.push(s), f && t.push(u);
        }

        return o;
      }

      function ct(e, t, n, r, i, s) {
        return (
          r && !r[d] && (r = ct(r)),
          i && !i[d] && (i = ct(i, s)),
          N(function (s, o, u, a) {
            var f,
              l,
              c,
              h = [],
              p = [],
              d = o.length,
              v = s || dt(t || "*", u.nodeType ? [u] : u, []),
              m = e && (s || !t) ? lt(v, h, e, u, a) : v,
              g = n ? (i || (s ? e : d || r) ? [] : o) : m;
            n && n(m, g, u, a);

            if (r) {
              (f = lt(g, p)), r(f, [], u, a), (l = f.length);

              while (l--) {
                if ((c = f[l])) g[p[l]] = !(m[p[l]] = c);
              }
            }

            if (s) {
              if (i || e) {
                if (i) {
                  (f = []), (l = g.length);

                  while (l--) {
                    (c = g[l]) && f.push((m[l] = c));
                  }

                  i(null, (g = []), f, a);
                }

                l = g.length;

                while (l--) {
                  (c = g[l]) &&
                    (f = i ? T.call(s, c) : h[l]) > -1 &&
                    (s[f] = !(o[f] = c));
                }
              }
            } else (g = lt(g === o ? g.splice(d, g.length) : g)), i ? i(null, o, g, a) : S.apply(o, g);
          })
        );
      }

      function ht(e) {
        var t,
          n,
          r,
          s = e.length,
          o = i.relative[e[0].type],
          u = o || i.relative[" "],
          a = o ? 1 : 0,
          f = at(
            function (e) {
              return e === t;
            },
            u,
            !0
          ),
          l = at(
            function (e) {
              return T.call(t, e) > -1;
            },
            u,
            !0
          ),
          h = [
            function (e, n, r) {
              return (
                (!o && (r || n !== c)) ||
                ((t = n).nodeType ? f(e, n, r) : l(e, n, r))
              );
            }
          ];

        for (; a < s; a++) {
          if ((n = i.relative[e[a].type])) h = [at(ft(h), n)];
          else {
            n = i.filter[e[a].type].apply(null, e[a].matches);

            if (n[d]) {
              r = ++a;

              for (; r < s; r++) {
                if (i.relative[e[r].type]) break;
              }

              return ct(
                a > 1 && ft(h),
                a > 1 &&
                  e
                    .slice(0, a - 1)
                    .join("")
                    .replace(j, "$1"),
                n,
                a < r && ht(e.slice(a, r)),
                r < s && ht((e = e.slice(r))),
                r < s && e.join("")
              );
            }

            h.push(n);
          }
        }

        return ft(h);
      }

      function pt(e, t) {
        var r = t.length > 0,
          s = e.length > 0,
          o = function o(u, a, f, l, h) {
            var p,
              d,
              v,
              m = [],
              y = 0,
              w = "0",
              x = u && [],
              T = h != null,
              N = c,
              C = u || (s && i.find.TAG("*", (h && a.parentNode) || a)),
              k = (b += N == null ? 1 : Math.E);
            T && ((c = a !== g && a), (n = o.el));

            for (; (p = C[w]) != null; w++) {
              if (s && p) {
                for (d = 0; (v = e[d]); d++) {
                  if (v(p, a, f)) {
                    l.push(p);
                    break;
                  }
                }

                T && ((b = k), (n = ++o.el));
              }

              r && ((p = !v && p) && y--, u && x.push(p));
            }

            y += w;

            if (r && w !== y) {
              for (d = 0; (v = t[d]); d++) {
                v(x, m, a, f);
              }

              if (u) {
                if (y > 0)
                  while (w--) {
                    !x[w] && !m[w] && (m[w] = E.call(l));
                  }
                m = lt(m);
              }

              S.apply(l, m),
                T && !u && m.length > 0 && y + t.length > 1 && nt.uniqueSort(l);
            }

            return T && ((b = k), (c = N)), x;
          };

        return (o.el = 0), r ? N(o) : o;
      }

      function dt(e, t, n) {
        var r = 0,
          i = t.length;

        for (; r < i; r++) {
          nt(e, t[r], n);
        }

        return n;
      }

      function vt(e, t, n, r, s) {
        var o,
          u,
          f,
          l,
          c,
          h = ut(e),
          p = h.length;

        if (!r && h.length === 1) {
          u = h[0] = h[0].slice(0);

          if (
            u.length > 2 &&
            (f = u[0]).type === "ID" &&
            t.nodeType === 9 &&
            !s &&
            i.relative[u[1].type]
          ) {
            t = i.find.ID(f.matches[0].replace($, ""), t, s)[0];
            if (!t) return n;
            e = e.slice(u.shift().length);
          }

          for (o = J.POS.test(e) ? -1 : u.length - 1; o >= 0; o--) {
            f = u[o];
            if (i.relative[(l = f.type)]) break;
            if ((c = i.find[l]))
              if (
                (r = c(
                  f.matches[0].replace($, ""),
                  (z.test(u[0].type) && t.parentNode) || t,
                  s
                ))
              ) {
                u.splice(o, 1), (e = r.length && u.join(""));
                if (!e) return S.apply(n, x.call(r, 0)), n;
                break;
              }
          }
        }

        return a(e, h)(r, t, s, n, z.test(e)), n;
      }

      function mt() {}

      var n,
        r,
        i,
        s,
        o,
        u,
        a,
        f,
        l,
        c,
        h = !0,
        p = "undefined",
        d = ("sizcache" + Math.random()).replace(".", ""),
        m = String,
        g = e.document,
        y = g.documentElement,
        b = 0,
        w = 0,
        E = [].pop,
        S = [].push,
        x = [].slice,
        T =
          [].indexOf ||
          function (e) {
            var t = 0,
              n = this.length;

            for (; t < n; t++) {
              if (this[t] === e) return t;
            }

            return -1;
          },
        N = function N(e, t) {
          return (e[d] = t == null || t), e;
        },
        C = function C() {
          var e = {},
            t = [];
          return N(function (n, r) {
            return (
              t.push(n) > i.cacheLength && delete e[t.shift()], (e[n + " "] = r)
            );
          }, e);
        },
        k = C(),
        L = C(),
        A = C(),
        O = "[\\x20\\t\\r\\n\\f]",
        M = "(?:\\\\.|[-\\w]|[^\\x00-\\xa0])+",
        _ = M.replace("w", "w#"),
        D = "([*^$|!~]?=)",
        P =
          "\\[" +
          O +
          "*(" +
          M +
          ")" +
          O +
          "*(?:" +
          D +
          O +
          "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" +
          _ +
          ")|)|)" +
          O +
          "*\\]",
        H =
          ":(" +
          M +
          ")(?:\\((?:(['\"])((?:\\\\.|[^\\\\])*?)\\2|([^()[\\]]*|(?:(?:" +
          P +
          ")|[^:]|\\\\.)*|.*))\\)|)",
        B =
          ":(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
          O +
          "*((?:-\\d)?\\d*)" +
          O +
          "*\\)|)(?=[^-]|$)",
        j = new RegExp("^" + O + "+|((?:^|[^\\\\])(?:\\\\.)*)" + O + "+$", "g"),
        F = new RegExp("^" + O + "*," + O + "*"),
        I = new RegExp("^" + O + "*([\\x20\\t\\r\\n\\f>+~])" + O + "*"),
        q = new RegExp(H),
        R = /^(?:#([\w\-]+)|(\w+)|\.([\w\-]+))$/,
        U = /^:not/,
        z = /[\x20\t\r\n\f]*[+~]/,
        W = /:not\($/,
        X = /h\d/i,
        V = /input|select|textarea|button/i,
        $ = /\\(?!\\)/g,
        J = {
          ID: new RegExp("^#(" + M + ")"),
          CLASS: new RegExp("^\\.(" + M + ")"),
          NAME: new RegExp("^\\[name=['\"]?(" + M + ")['\"]?\\]"),
          TAG: new RegExp("^(" + M.replace("w", "w*") + ")"),
          ATTR: new RegExp("^" + P),
          PSEUDO: new RegExp("^" + H),
          POS: new RegExp(B, "i"),
          CHILD: new RegExp(
            "^:(only|nth|first|last)-child(?:\\(" +
              O +
              "*(even|odd|(([+-]|)(\\d*)n|)" +
              O +
              "*(?:([+-]|)" +
              O +
              "*(\\d+)|))" +
              O +
              "*\\)|)",
            "i"
          ),
          needsContext: new RegExp("^" + O + "*[>+~]|" + B, "i")
        },
        K = function K(e) {
          var t = g.createElement("div");

          try {
            return e(t);
          } catch (n) {
            return !1;
          } finally {
            t = null;
          }
        },
        Q = K(function (e) {
          return (
            e.appendChild(g.createComment("")),
            !e.getElementsByTagName("*").length
          );
        }),
        G = K(function (e) {
          return (
            (e.innerHTML = "<a href='#'></a>"),
            e.firstChild &&
              _typeof(e.firstChild.getAttribute) !== p &&
              e.firstChild.getAttribute("href") === "#"
          );
        }),
        Y = K(function (e) {
          e.innerHTML = "<select></select>";

          var t = _typeof(e.lastChild.getAttribute("multiple"));

          return t !== "boolean" && t !== "string";
        }),
        Z = K(function (e) {
          return (
            (e.innerHTML =
              "<div class='hidden e'></div><div class='hidden'></div>"),
            !e.getElementsByClassName || !e.getElementsByClassName("e").length
              ? !1
              : ((e.lastChild.className = "e"),
                e.getElementsByClassName("e").length === 2)
          );
        }),
        et = K(function (e) {
          (e.id = d + 0),
            (e.innerHTML =
              "<a name='" + d + "'></a><div name='" + d + "'></div>"),
            y.insertBefore(e, y.firstChild);
          var t =
            g.getElementsByName &&
            g.getElementsByName(d).length ===
              2 + g.getElementsByName(d + 0).length;
          return (r = !g.getElementById(d)), y.removeChild(e), t;
        });

      try {
        x.call(y.childNodes, 0)[0].nodeType;
      } catch (tt) {
        x = function x(e) {
          var t,
            n = [];

          for (; (t = this[e]); e++) {
            n.push(t);
          }

          return n;
        };
      }

      (nt.matches = function (e, t) {
        return nt(e, null, null, t);
      }),
        (nt.matchesSelector = function (e, t) {
          return nt(t, null, null, [e]).length > 0;
        }),
        (s = nt.getText = function (e) {
          var t,
            n = "",
            r = 0,
            i = e.nodeType;

          if (i) {
            if (i === 1 || i === 9 || i === 11) {
              if (typeof e.textContent == "string") return e.textContent;

              for (e = e.firstChild; e; e = e.nextSibling) {
                n += s(e);
              }
            } else if (i === 3 || i === 4) return e.nodeValue;
          } else
            for (; (t = e[r]); r++) {
              n += s(t);
            }

          return n;
        }),
        (o = nt.isXML = function (e) {
          var t = e && (e.ownerDocument || e).documentElement;
          return t ? t.nodeName !== "HTML" : !1;
        }),
        (u = nt.contains = y.contains
          ? function (e, t) {
              var n = e.nodeType === 9 ? e.documentElement : e,
                r = t && t.parentNode;
              return (
                e === r ||
                !!(r && r.nodeType === 1 && n.contains && n.contains(r))
              );
            }
          : y.compareDocumentPosition
          ? function (e, t) {
              return t && !!(e.compareDocumentPosition(t) & 16);
            }
          : function (e, t) {
              while ((t = t.parentNode)) {
                if (t === e) return !0;
              }

              return !1;
            }),
        (nt.attr = function (e, t) {
          var n,
            r = o(e);
          return (
            r || (t = t.toLowerCase()),
            (n = i.attrHandle[t])
              ? n(e)
              : r || Y
              ? e.getAttribute(t)
              : ((n = e.getAttributeNode(t)),
                n
                  ? typeof e[t] == "boolean"
                    ? e[t]
                      ? t
                      : null
                    : n.specified
                    ? n.value
                    : null
                  : null)
          );
        }),
        (i = nt.selectors = {
          cacheLength: 50,
          createPseudo: N,
          match: J,
          attrHandle: G
            ? {}
            : {
                href: function href(e) {
                  return e.getAttribute("href", 2);
                },
                type: function type(e) {
                  return e.getAttribute("type");
                }
              },
          find: {
            ID: r
              ? function (e, t, n) {
                  if (_typeof(t.getElementById) !== p && !n) {
                    var r = t.getElementById(e);
                    return r && r.parentNode ? [r] : [];
                  }
                }
              : function (e, n, r) {
                  if (_typeof(n.getElementById) !== p && !r) {
                    var i = n.getElementById(e);
                    return i
                      ? i.id === e ||
                        (_typeof(i.getAttributeNode) !== p &&
                          i.getAttributeNode("id").value === e)
                        ? [i]
                        : t
                      : [];
                  }
                },
            TAG: Q
              ? function (e, t) {
                  if (_typeof(t.getElementsByTagName) !== p)
                    return t.getElementsByTagName(e);
                }
              : function (e, t) {
                  var n = t.getElementsByTagName(e);

                  if (e === "*") {
                    var r,
                      i = [],
                      s = 0;

                    for (; (r = n[s]); s++) {
                      r.nodeType === 1 && i.push(r);
                    }

                    return i;
                  }

                  return n;
                },
            NAME:
              et &&
              function (e, t) {
                if (_typeof(t.getElementsByName) !== p)
                  return t.getElementsByName(name);
              },
            CLASS:
              Z &&
              function (e, t, n) {
                if (_typeof(t.getElementsByClassName) !== p && !n)
                  return t.getElementsByClassName(e);
              }
          },
          relative: {
            ">": {
              dir: "parentNode",
              first: !0
            },
            " ": {
              dir: "parentNode"
            },
            "+": {
              dir: "previousSibling",
              first: !0
            },
            "~": {
              dir: "previousSibling"
            }
          },
          preFilter: {
            ATTR: function ATTR(e) {
              return (
                (e[1] = e[1].replace($, "")),
                (e[3] = (e[4] || e[5] || "").replace($, "")),
                e[2] === "~=" && (e[3] = " " + e[3] + " "),
                e.slice(0, 4)
              );
            },
            CHILD: function CHILD(e) {
              return (
                (e[1] = e[1].toLowerCase()),
                e[1] === "nth"
                  ? (e[2] || nt.error(e[0]),
                    (e[3] = +(e[3]
                      ? e[4] + (e[5] || 1)
                      : 2 * (e[2] === "even" || e[2] === "odd"))),
                    (e[4] = +(e[6] + e[7] || e[2] === "odd")))
                  : e[2] && nt.error(e[0]),
                e
              );
            },
            PSEUDO: function PSEUDO(e) {
              var t, n;
              if (J.CHILD.test(e[0])) return null;
              if (e[3]) e[2] = e[3];
              else if ((t = e[4]))
                q.test(t) &&
                  (n = ut(t, !0)) &&
                  (n = t.indexOf(")", t.length - n) - t.length) &&
                  ((t = t.slice(0, n)), (e[0] = e[0].slice(0, n))),
                  (e[2] = t);
              return e.slice(0, 3);
            }
          },
          filter: {
            ID: r
              ? function (e) {
                  return (
                    (e = e.replace($, "")),
                    function (t) {
                      return t.getAttribute("id") === e;
                    }
                  );
                }
              : function (e) {
                  return (
                    (e = e.replace($, "")),
                    function (t) {
                      var n =
                        _typeof(t.getAttributeNode) !== p &&
                        t.getAttributeNode("id");
                      return n && n.value === e;
                    }
                  );
                },
            TAG: function TAG(e) {
              return e === "*"
                ? function () {
                    return !0;
                  }
                : ((e = e.replace($, "").toLowerCase()),
                  function (t) {
                    return t.nodeName && t.nodeName.toLowerCase() === e;
                  });
            },
            CLASS: function CLASS(e) {
              var t = k[d][e + " "];
              return (
                t ||
                ((t = new RegExp("(^|" + O + ")" + e + "(" + O + "|$)")) &&
                  k(e, function (e) {
                    return t.test(
                      e.className ||
                        (_typeof(e.getAttribute) !== p &&
                          e.getAttribute("class")) ||
                        ""
                    );
                  }))
              );
            },
            ATTR: function ATTR(e, t, n) {
              return function (r, i) {
                var s = nt.attr(r, e);
                return s == null
                  ? t === "!="
                  : t
                  ? ((s += ""),
                    t === "="
                      ? s === n
                      : t === "!="
                      ? s !== n
                      : t === "^="
                      ? n && s.indexOf(n) === 0
                      : t === "*="
                      ? n && s.indexOf(n) > -1
                      : t === "$="
                      ? n && s.substr(s.length - n.length) === n
                      : t === "~="
                      ? (" " + s + " ").indexOf(n) > -1
                      : t === "|="
                      ? s === n || s.substr(0, n.length + 1) === n + "-"
                      : !1)
                  : !0;
              };
            },
            CHILD: function CHILD(e, t, n, r) {
              return e === "nth"
                ? function (e) {
                    var t,
                      i,
                      s = e.parentNode;
                    if (n === 1 && r === 0) return !0;

                    if (s) {
                      i = 0;

                      for (t = s.firstChild; t; t = t.nextSibling) {
                        if (t.nodeType === 1) {
                          i++;
                          if (e === t) break;
                        }
                      }
                    }

                    return (i -= r), i === n || (i % n === 0 && i / n >= 0);
                  }
                : function (t) {
                    var n = t;

                    switch (e) {
                      case "only":
                      case "first":
                        while ((n = n.previousSibling)) {
                          if (n.nodeType === 1) return !1;
                        }

                        if (e === "first") return !0;
                        n = t;

                      case "last":
                        while ((n = n.nextSibling)) {
                          if (n.nodeType === 1) return !1;
                        }

                        return !0;
                    }
                  };
            },
            PSEUDO: function PSEUDO(e, t) {
              var n,
                r =
                  i.pseudos[e] ||
                  i.setFilters[e.toLowerCase()] ||
                  nt.error("unsupported pseudo: " + e);
              return r[d]
                ? r(t)
                : r.length > 1
                ? ((n = [e, e, "", t]),
                  i.setFilters.hasOwnProperty(e.toLowerCase())
                    ? N(function (e, n) {
                        var i,
                          s = r(e, t),
                          o = s.length;

                        while (o--) {
                          (i = T.call(e, s[o])), (e[i] = !(n[i] = s[o]));
                        }
                      })
                    : function (e) {
                        return r(e, 0, n);
                      })
                : r;
            }
          },
          pseudos: {
            not: N(function (e) {
              var t = [],
                n = [],
                r = a(e.replace(j, "$1"));
              return r[d]
                ? N(function (e, t, n, i) {
                    var s,
                      o = r(e, null, i, []),
                      u = e.length;

                    while (u--) {
                      if ((s = o[u])) e[u] = !(t[u] = s);
                    }
                  })
                : function (e, i, s) {
                    return (t[0] = e), r(t, null, s, n), !n.pop();
                  };
            }),
            has: N(function (e) {
              return function (t) {
                return nt(e, t).length > 0;
              };
            }),
            contains: N(function (e) {
              return function (t) {
                return (t.textContent || t.innerText || s(t)).indexOf(e) > -1;
              };
            }),
            enabled: function enabled(e) {
              return e.disabled === !1;
            },
            disabled: function disabled(e) {
              return e.disabled === !0;
            },
            checked: function checked(e) {
              var t = e.nodeName.toLowerCase();
              return (
                (t === "input" && !!e.checked) ||
                (t === "option" && !!e.selected)
              );
            },
            selected: function selected(e) {
              return (
                e.parentNode && e.parentNode.selectedIndex, e.selected === !0
              );
            },
            parent: function parent(e) {
              return !i.pseudos.empty(e);
            },
            empty: function empty(e) {
              var t;
              e = e.firstChild;

              while (e) {
                if (e.nodeName > "@" || (t = e.nodeType) === 3 || t === 4)
                  return !1;
                e = e.nextSibling;
              }

              return !0;
            },
            header: function header(e) {
              return X.test(e.nodeName);
            },
            text: function text(e) {
              var t, n;
              return (
                e.nodeName.toLowerCase() === "input" &&
                (t = e.type) === "text" &&
                ((n = e.getAttribute("type")) == null || n.toLowerCase() === t)
              );
            },
            radio: rt("radio"),
            checkbox: rt("checkbox"),
            file: rt("file"),
            password: rt("password"),
            image: rt("image"),
            submit: it("submit"),
            reset: it("reset"),
            button: function button(e) {
              var t = e.nodeName.toLowerCase();
              return (t === "input" && e.type === "button") || t === "button";
            },
            input: function input(e) {
              return V.test(e.nodeName);
            },
            focus: function focus(e) {
              var t = e.ownerDocument;
              return (
                e === t.activeElement &&
                (!t.hasFocus || t.hasFocus()) &&
                !!(e.type || e.href || ~e.tabIndex)
              );
            },
            active: function active(e) {
              return e === e.ownerDocument.activeElement;
            },
            first: st(function () {
              return [0];
            }),
            last: st(function (e, t) {
              return [t - 1];
            }),
            eq: st(function (e, t, n) {
              return [n < 0 ? n + t : n];
            }),
            even: st(function (e, t) {
              for (var n = 0; n < t; n += 2) {
                e.push(n);
              }

              return e;
            }),
            odd: st(function (e, t) {
              for (var n = 1; n < t; n += 2) {
                e.push(n);
              }

              return e;
            }),
            lt: st(function (e, t, n) {
              for (var r = n < 0 ? n + t : n; --r >= 0; ) {
                e.push(r);
              }

              return e;
            }),
            gt: st(function (e, t, n) {
              for (var r = n < 0 ? n + t : n; ++r < t; ) {
                e.push(r);
              }

              return e;
            })
          }
        }),
        (f = y.compareDocumentPosition
          ? function (e, t) {
              return e === t
                ? ((l = !0), 0)
                : (
                    !e.compareDocumentPosition || !t.compareDocumentPosition
                      ? e.compareDocumentPosition
                      : e.compareDocumentPosition(t) & 4
                  )
                ? -1
                : 1;
            }
          : function (e, t) {
              if (e === t) return (l = !0), 0;
              if (e.sourceIndex && t.sourceIndex)
                return e.sourceIndex - t.sourceIndex;
              var n,
                r,
                i = [],
                s = [],
                o = e.parentNode,
                u = t.parentNode,
                a = o;
              if (o === u) return ot(e, t);
              if (!o) return -1;
              if (!u) return 1;

              while (a) {
                i.unshift(a), (a = a.parentNode);
              }

              a = u;

              while (a) {
                s.unshift(a), (a = a.parentNode);
              }

              (n = i.length), (r = s.length);

              for (var f = 0; f < n && f < r; f++) {
                if (i[f] !== s[f]) return ot(i[f], s[f]);
              }

              return f === n ? ot(e, s[f], -1) : ot(i[f], t, 1);
            }),
        [0, 0].sort(f),
        (h = !l),
        (nt.uniqueSort = function (e) {
          var t,
            n = [],
            r = 1,
            i = 0;
          (l = h), e.sort(f);

          if (l) {
            for (; (t = e[r]); r++) {
              t === e[r - 1] && (i = n.push(r));
            }

            while (i--) {
              e.splice(n[i], 1);
            }
          }

          return e;
        }),
        (nt.error = function (e) {
          throw new Error("Syntax error, unrecognized expression: " + e);
        }),
        (a = nt.compile = function (e, t) {
          var n,
            r = [],
            i = [],
            s = A[d][e + " "];

          if (!s) {
            t || (t = ut(e)), (n = t.length);

            while (n--) {
              (s = ht(t[n])), s[d] ? r.push(s) : i.push(s);
            }

            s = A(e, pt(i, r));
          }

          return s;
        }),
        g.querySelectorAll &&
          (function () {
            var e,
              t = vt,
              n = /'|\\/g,
              r = /\=[\x20\t\r\n\f]*([^'"\]]*)[\x20\t\r\n\f]*\]/g,
              i = [":focus"],
              s = [":active"],
              u =
                y.matchesSelector ||
                y.mozMatchesSelector ||
                y.webkitMatchesSelector ||
                y.oMatchesSelector ||
                y.msMatchesSelector;
            K(function (e) {
              (e.innerHTML = "<select><option selected=''></option></select>"),
                e.querySelectorAll("[selected]").length ||
                  i.push(
                    "\\[" +
                      O +
                      "*(?:checked|disabled|ismap|multiple|readonly|selected|value)"
                  ),
                e.querySelectorAll(":checked").length || i.push(":checked");
            }),
              K(function (e) {
                (e.innerHTML = "<p test=''></p>"),
                  e.querySelectorAll("[test^='']").length &&
                    i.push("[*^$]=" + O + "*(?:\"\"|'')"),
                  (e.innerHTML = "<input type='hidden'/>"),
                  e.querySelectorAll(":enabled").length ||
                    i.push(":enabled", ":disabled");
              }),
              (i = new RegExp(i.join("|"))),
              (vt = function vt(e, r, s, o, u) {
                if (!o && !u && !i.test(e)) {
                  var a,
                    f,
                    l = !0,
                    c = d,
                    h = r,
                    p = r.nodeType === 9 && e;

                  if (
                    r.nodeType === 1 &&
                    r.nodeName.toLowerCase() !== "object"
                  ) {
                    (a = ut(e)),
                      (l = r.getAttribute("id"))
                        ? (c = l.replace(n, "\\$&"))
                        : r.setAttribute("id", c),
                      (c = "[id='" + c + "'] "),
                      (f = a.length);

                    while (f--) {
                      a[f] = c + a[f].join("");
                    }

                    (h = (z.test(e) && r.parentNode) || r), (p = a.join(","));
                  }

                  if (p)
                    try {
                      return S.apply(s, x.call(h.querySelectorAll(p), 0)), s;
                    } catch (v) {
                    } finally {
                      l || r.removeAttribute("id");
                    }
                }

                return t(e, r, s, o, u);
              }),
              u &&
                (K(function (t) {
                  e = u.call(t, "div");

                  try {
                    u.call(t, "[test!='']:sizzle"), s.push("!=", H);
                  } catch (n) {}
                }),
                (s = new RegExp(s.join("|"))),
                (nt.matchesSelector = function (t, n) {
                  n = n.replace(r, "='$1']");
                  if (!o(t) && !s.test(n) && !i.test(n))
                    try {
                      var a = u.call(t, n);
                      if (a || e || (t.document && t.document.nodeType !== 11))
                        return a;
                    } catch (f) {}
                  return nt(n, null, null, [t]).length > 0;
                }));
          })(),
        (i.pseudos.nth = i.pseudos.eq),
        (i.filters = mt.prototype = i.pseudos),
        (i.setFilters = new mt()),
        (nt.attr = v.attr),
        (v.find = nt),
        (v.expr = nt.selectors),
        (v.expr[":"] = v.expr.pseudos),
        (v.unique = nt.uniqueSort),
        (v.text = nt.getText),
        (v.isXMLDoc = nt.isXML),
        (v.contains = nt.contains);
    })(e);
  var nt = /Until$/,
    rt = /^(?:parents|prev(?:Until|All))/,
    it = /^.[^:#\[\.,]*$/,
    st = v.expr.match.needsContext,
    ot = {
      children: !0,
      contents: !0,
      next: !0,
      prev: !0
    };
  v.fn.extend({
    find: function find(e) {
      var t,
        n,
        r,
        i,
        s,
        o,
        u = this;
      if (typeof e != "string")
        return v(e).filter(function () {
          for (t = 0, n = u.length; t < n; t++) {
            if (v.contains(u[t], this)) return !0;
          }
        });
      o = this.pushStack("", "find", e);

      for (t = 0, n = this.length; t < n; t++) {
        (r = o.length), v.find(e, this[t], o);
        if (t > 0)
          for (i = r; i < o.length; i++) {
            for (s = 0; s < r; s++) {
              if (o[s] === o[i]) {
                o.splice(i--, 1);
                break;
              }
            }
          }
      }

      return o;
    },
    has: function has(e) {
      var t,
        n = v(e, this),
        r = n.length;
      return this.filter(function () {
        for (t = 0; t < r; t++) {
          if (v.contains(this, n[t])) return !0;
        }
      });
    },
    not: function not(e) {
      return this.pushStack(ft(this, e, !1), "not", e);
    },
    filter: function filter(e) {
      return this.pushStack(ft(this, e, !0), "filter", e);
    },
    is: function is(e) {
      return (
        !!e &&
        (typeof e == "string"
          ? st.test(e)
            ? v(e, this.context).index(this[0]) >= 0
            : v.filter(e, this).length > 0
          : this.filter(e).length > 0)
      );
    },
    closest: function closest(e, t) {
      var n,
        r = 0,
        i = this.length,
        s = [],
        o = st.test(e) || typeof e != "string" ? v(e, t || this.context) : 0;

      for (; r < i; r++) {
        n = this[r];

        while (n && n.ownerDocument && n !== t && n.nodeType !== 11) {
          if (o ? o.index(n) > -1 : v.find.matchesSelector(n, e)) {
            s.push(n);
            break;
          }

          n = n.parentNode;
        }
      }

      return (
        (s = s.length > 1 ? v.unique(s) : s), this.pushStack(s, "closest", e)
      );
    },
    index: function index(e) {
      return e
        ? typeof e == "string"
          ? v.inArray(this[0], v(e))
          : v.inArray(e.jquery ? e[0] : e, this)
        : this[0] && this[0].parentNode
        ? this.prevAll().length
        : -1;
    },
    add: function add(e, t) {
      var n =
          typeof e == "string"
            ? v(e, t)
            : v.makeArray(e && e.nodeType ? [e] : e),
        r = v.merge(this.get(), n);
      return this.pushStack(ut(n[0]) || ut(r[0]) ? r : v.unique(r));
    },
    addBack: function addBack(e) {
      return this.add(e == null ? this.prevObject : this.prevObject.filter(e));
    }
  }),
    (v.fn.andSelf = v.fn.addBack),
    v.each(
      {
        parent: function parent(e) {
          var t = e.parentNode;
          return t && t.nodeType !== 11 ? t : null;
        },
        parents: function parents(e) {
          return v.dir(e, "parentNode");
        },
        parentsUntil: function parentsUntil(e, t, n) {
          return v.dir(e, "parentNode", n);
        },
        next: function next(e) {
          return at(e, "nextSibling");
        },
        prev: function prev(e) {
          return at(e, "previousSibling");
        },
        nextAll: function nextAll(e) {
          return v.dir(e, "nextSibling");
        },
        prevAll: function prevAll(e) {
          return v.dir(e, "previousSibling");
        },
        nextUntil: function nextUntil(e, t, n) {
          return v.dir(e, "nextSibling", n);
        },
        prevUntil: function prevUntil(e, t, n) {
          return v.dir(e, "previousSibling", n);
        },
        siblings: function siblings(e) {
          return v.sibling((e.parentNode || {}).firstChild, e);
        },
        children: function children(e) {
          return v.sibling(e.firstChild);
        },
        contents: function contents(e) {
          return v.nodeName(e, "iframe")
            ? e.contentDocument || e.contentWindow.document
            : v.merge([], e.childNodes);
        }
      },
      function (e, t) {
        v.fn[e] = function (n, r) {
          var i = v.map(this, t, n);
          return (
            nt.test(e) || (r = n),
            r && typeof r == "string" && (i = v.filter(r, i)),
            (i = this.length > 1 && !ot[e] ? v.unique(i) : i),
            this.length > 1 && rt.test(e) && (i = i.reverse()),
            this.pushStack(i, e, l.call(arguments).join(","))
          );
        };
      }
    ),
    v.extend({
      filter: function filter(e, t, n) {
        return (
          n && (e = ":not(" + e + ")"),
          t.length === 1
            ? v.find.matchesSelector(t[0], e)
              ? [t[0]]
              : []
            : v.find.matches(e, t)
        );
      },
      dir: function dir(e, n, r) {
        var i = [],
          s = e[n];

        while (
          s &&
          s.nodeType !== 9 &&
          (r === t || s.nodeType !== 1 || !v(s).is(r))
        ) {
          s.nodeType === 1 && i.push(s), (s = s[n]);
        }

        return i;
      },
      sibling: function sibling(e, t) {
        var n = [];

        for (; e; e = e.nextSibling) {
          e.nodeType === 1 && e !== t && n.push(e);
        }

        return n;
      }
    });
  var ct =
      "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
    ht = / jQuery\d+="(?:null|\d+)"/g,
    pt = /^\s+/,
    dt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
    vt = /<([\w:]+)/,
    mt = /<tbody/i,
    gt = /<|&#?\w+;/,
    yt = /<(?:script|style|link)/i,
    bt = /<(?:script|object|embed|option|style)/i,
    wt = new RegExp("<(?:" + ct + ")[\\s/>]", "i"),
    Et = /^(?:checkbox|radio)$/,
    St = /checked\s*(?:[^=]|=\s*.checked.)/i,
    xt = /\/(java|ecma)script/i,
    Tt = /^\s*<!(?:\[CDATA\[|\-\-)|[\]\-]{2}>\s*$/g,
    Nt = {
      option: [1, "<select multiple='multiple'>", "</select>"],
      legend: [1, "<fieldset>", "</fieldset>"],
      thead: [1, "<table>", "</table>"],
      tr: [2, "<table><tbody>", "</tbody></table>"],
      td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
      col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
      area: [1, "<map>", "</map>"],
      _default: [0, "", ""]
    },
    Ct = lt(i),
    kt = Ct.appendChild(i.createElement("div"));
  (Nt.optgroup = Nt.option),
    (Nt.tbody = Nt.tfoot = Nt.colgroup = Nt.caption = Nt.thead),
    (Nt.th = Nt.td),
    v.support.htmlSerialize || (Nt._default = [1, "X<div>", "</div>"]),
    v.fn.extend({
      text: function text(e) {
        return v.access(
          this,
          function (e) {
            return e === t
              ? v.text(this)
              : this.empty().append(
                  ((this[0] && this[0].ownerDocument) || i).createTextNode(e)
                );
          },
          null,
          e,
          arguments.length
        );
      },
      wrapAll: function wrapAll(e) {
        if (v.isFunction(e))
          return this.each(function (t) {
            v(this).wrapAll(e.call(this, t));
          });

        if (this[0]) {
          var t = v(e, this[0].ownerDocument).eq(0).clone(!0);
          this[0].parentNode && t.insertBefore(this[0]),
            t
              .map(function () {
                var e = this;

                while (e.firstChild && e.firstChild.nodeType === 1) {
                  e = e.firstChild;
                }

                return e;
              })
              .append(this);
        }

        return this;
      },
      wrapInner: function wrapInner(e) {
        return v.isFunction(e)
          ? this.each(function (t) {
              v(this).wrapInner(e.call(this, t));
            })
          : this.each(function () {
              var t = v(this),
                n = t.contents();
              n.length ? n.wrapAll(e) : t.append(e);
            });
      },
      wrap: function wrap(e) {
        var t = v.isFunction(e);
        return this.each(function (n) {
          v(this).wrapAll(t ? e.call(this, n) : e);
        });
      },
      unwrap: function unwrap() {
        return this.parent()
          .each(function () {
            v.nodeName(this, "body") || v(this).replaceWith(this.childNodes);
          })
          .end();
      },
      append: function append() {
        return this.domManip(arguments, !0, function (e) {
          (this.nodeType === 1 || this.nodeType === 11) && this.appendChild(e);
        });
      },
      prepend: function prepend() {
        return this.domManip(arguments, !0, function (e) {
          (this.nodeType === 1 || this.nodeType === 11) &&
            this.insertBefore(e, this.firstChild);
        });
      },
      before: function before() {
        if (!ut(this[0]))
          return this.domManip(arguments, !1, function (e) {
            this.parentNode.insertBefore(e, this);
          });

        if (arguments.length) {
          var e = v.clean(arguments);
          return this.pushStack(v.merge(e, this), "before", this.selector);
        }
      },
      after: function after() {
        if (!ut(this[0]))
          return this.domManip(arguments, !1, function (e) {
            this.parentNode.insertBefore(e, this.nextSibling);
          });

        if (arguments.length) {
          var e = v.clean(arguments);
          return this.pushStack(v.merge(this, e), "after", this.selector);
        }
      },
      remove: function remove(e, t) {
        var n,
          r = 0;

        for (; (n = this[r]) != null; r++) {
          if (!e || v.filter(e, [n]).length)
            !t &&
              n.nodeType === 1 &&
              (v.cleanData(n.getElementsByTagName("*")), v.cleanData([n])),
              n.parentNode && n.parentNode.removeChild(n);
        }

        return this;
      },
      empty: function empty() {
        var e,
          t = 0;

        for (; (e = this[t]) != null; t++) {
          e.nodeType === 1 && v.cleanData(e.getElementsByTagName("*"));

          while (e.firstChild) {
            e.removeChild(e.firstChild);
          }
        }

        return this;
      },
      clone: function clone(e, t) {
        return (
          (e = e == null ? !1 : e),
          (t = t == null ? e : t),
          this.map(function () {
            return v.clone(this, e, t);
          })
        );
      },
      html: function html(e) {
        return v.access(
          this,
          function (e) {
            var n = this[0] || {},
              r = 0,
              i = this.length;
            if (e === t)
              return n.nodeType === 1 ? n.innerHTML.replace(ht, "") : t;

            if (
              typeof e == "string" &&
              !yt.test(e) &&
              (v.support.htmlSerialize || !wt.test(e)) &&
              (v.support.leadingWhitespace || !pt.test(e)) &&
              !Nt[(vt.exec(e) || ["", ""])[1].toLowerCase()]
            ) {
              e = e.replace(dt, "<$1></$2>");

              try {
                for (; r < i; r++) {
                  (n = this[r] || {}),
                    n.nodeType === 1 &&
                      (v.cleanData(n.getElementsByTagName("*")),
                      (n.innerHTML = e));
                }

                n = 0;
              } catch (s) {}
            }

            n && this.empty().append(e);
          },
          null,
          e,
          arguments.length
        );
      },
      replaceWith: function replaceWith(e) {
        return ut(this[0])
          ? this.length
            ? this.pushStack(v(v.isFunction(e) ? e() : e), "replaceWith", e)
            : this
          : v.isFunction(e)
          ? this.each(function (t) {
              var n = v(this),
                r = n.html();
              n.replaceWith(e.call(this, t, r));
            })
          : (typeof e != "string" && (e = v(e).detach()),
            this.each(function () {
              var t = this.nextSibling,
                n = this.parentNode;
              v(this).remove(), t ? v(t).before(e) : v(n).append(e);
            }));
      },
      detach: function detach(e) {
        return this.remove(e, !0);
      },
      domManip: function domManip(e, n, r) {
        e = [].concat.apply([], e);
        var i,
          s,
          o,
          u,
          a = 0,
          f = e[0],
          l = [],
          c = this.length;
        if (
          !v.support.checkClone &&
          c > 1 &&
          typeof f == "string" &&
          St.test(f)
        )
          return this.each(function () {
            v(this).domManip(e, n, r);
          });
        if (v.isFunction(f))
          return this.each(function (i) {
            var s = v(this);
            (e[0] = f.call(this, i, n ? s.html() : t)), s.domManip(e, n, r);
          });

        if (this[0]) {
          (i = v.buildFragment(e, this, l)),
            (o = i.fragment),
            (s = o.firstChild),
            o.childNodes.length === 1 && (o = s);

          if (s) {
            n = n && v.nodeName(s, "tr");

            for (u = i.cacheable || c - 1; a < c; a++) {
              r.call(
                n && v.nodeName(this[a], "table")
                  ? Lt(this[a], "tbody")
                  : this[a],
                a === u ? o : v.clone(o, !0, !0)
              );
            }
          }

          (o = s = null),
            l.length &&
              v.each(l, function (e, t) {
                t.src
                  ? v.ajax
                    ? v.ajax({
                        url: t.src,
                        type: "GET",
                        dataType: "script",
                        async: !1,
                        global: !1,
                        throws: !0
                      })
                    : v.error("no ajax")
                  : v.globalEval(
                      (t.text || t.textContent || t.innerHTML || "").replace(
                        Tt,
                        ""
                      )
                    ),
                  t.parentNode && t.parentNode.removeChild(t);
              });
        }

        return this;
      }
    }),
    (v.buildFragment = function (e, n, r) {
      var s,
        o,
        u,
        a = e[0];
      return (
        (n = n || i),
        (n = (!n.nodeType && n[0]) || n),
        (n = n.ownerDocument || n),
        e.length === 1 &&
          typeof a == "string" &&
          a.length < 512 &&
          n === i &&
          a.charAt(0) === "<" &&
          !bt.test(a) &&
          (v.support.checkClone || !St.test(a)) &&
          (v.support.html5Clone || !wt.test(a)) &&
          ((o = !0), (s = v.fragments[a]), (u = s !== t)),
        s ||
          ((s = n.createDocumentFragment()),
          v.clean(e, n, s, r),
          o && (v.fragments[a] = u && s)),
        {
          fragment: s,
          cacheable: o
        }
      );
    }),
    (v.fragments = {}),
    v.each(
      {
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
      },
      function (e, t) {
        v.fn[e] = function (n) {
          var r,
            i = 0,
            s = [],
            o = v(n),
            u = o.length,
            a = this.length === 1 && this[0].parentNode;
          if (
            (a == null ||
              (a && a.nodeType === 11 && a.childNodes.length === 1)) &&
            u === 1
          )
            return o[t](this[0]), this;

          for (; i < u; i++) {
            (r = (i > 0 ? this.clone(!0) : this).get()),
              v(o[i])[t](r),
              (s = s.concat(r));
          }

          return this.pushStack(s, e, o.selector);
        };
      }
    ),
    v.extend({
      clone: function clone(e, t, n) {
        var r, i, s, o;
        v.support.html5Clone ||
        v.isXMLDoc(e) ||
        !wt.test("<" + e.nodeName + ">")
          ? (o = e.cloneNode(!0))
          : ((kt.innerHTML = e.outerHTML), kt.removeChild((o = kt.firstChild)));

        if (
          (!v.support.noCloneEvent || !v.support.noCloneChecked) &&
          (e.nodeType === 1 || e.nodeType === 11) &&
          !v.isXMLDoc(e)
        ) {
          Ot(e, o), (r = Mt(e)), (i = Mt(o));

          for (s = 0; r[s]; ++s) {
            i[s] && Ot(r[s], i[s]);
          }
        }

        if (t) {
          At(e, o);

          if (n) {
            (r = Mt(e)), (i = Mt(o));

            for (s = 0; r[s]; ++s) {
              At(r[s], i[s]);
            }
          }
        }

        return (r = i = null), o;
      },
      clean: function clean(e, t, n, r) {
        var s,
          o,
          u,
          a,
          f,
          l,
          c,
          h,
          p,
          d,
          m,
          g,
          y = t === i && Ct,
          b = [];
        if (!t || typeof t.createDocumentFragment == "undefined") t = i;

        for (s = 0; (u = e[s]) != null; s++) {
          typeof u == "number" && (u += "");
          if (!u) continue;
          if (typeof u == "string")
            if (!gt.test(u)) u = t.createTextNode(u);
            else {
              (y = y || lt(t)),
                (c = t.createElement("div")),
                y.appendChild(c),
                (u = u.replace(dt, "<$1></$2>")),
                (a = (vt.exec(u) || ["", ""])[1].toLowerCase()),
                (f = Nt[a] || Nt._default),
                (l = f[0]),
                (c.innerHTML = f[1] + u + f[2]);

              while (l--) {
                c = c.lastChild;
              }

              if (!v.support.tbody) {
                (h = mt.test(u)),
                  (p =
                    a === "table" && !h
                      ? c.firstChild && c.firstChild.childNodes
                      : f[1] === "<table>" && !h
                      ? c.childNodes
                      : []);

                for (o = p.length - 1; o >= 0; --o) {
                  v.nodeName(p[o], "tbody") &&
                    !p[o].childNodes.length &&
                    p[o].parentNode.removeChild(p[o]);
                }
              }

              !v.support.leadingWhitespace &&
                pt.test(u) &&
                c.insertBefore(t.createTextNode(pt.exec(u)[0]), c.firstChild),
                (u = c.childNodes),
                c.parentNode.removeChild(c);
            }
          u.nodeType ? b.push(u) : v.merge(b, u);
        }

        c && (u = c = y = null);
        if (!v.support.appendChecked)
          for (s = 0; (u = b[s]) != null; s++) {
            v.nodeName(u, "input")
              ? _t(u)
              : typeof u.getElementsByTagName != "undefined" &&
                v.grep(u.getElementsByTagName("input"), _t);
          }

        if (n) {
          m = function m(e) {
            if (!e.type || xt.test(e.type))
              return r
                ? r.push(e.parentNode ? e.parentNode.removeChild(e) : e)
                : n.appendChild(e);
          };

          for (s = 0; (u = b[s]) != null; s++) {
            if (!v.nodeName(u, "script") || !m(u))
              n.appendChild(u),
                typeof u.getElementsByTagName != "undefined" &&
                  ((g = v.grep(
                    v.merge([], u.getElementsByTagName("script")),
                    m
                  )),
                  b.splice.apply(b, [s + 1, 0].concat(g)),
                  (s += g.length));
          }
        }

        return b;
      },
      cleanData: function cleanData(e, t) {
        var n,
          r,
          i,
          s,
          o = 0,
          u = v.expando,
          a = v.cache,
          f = v.support.deleteExpando,
          l = v.event.special;

        for (; (i = e[o]) != null; o++) {
          if (t || v.acceptData(i)) {
            (r = i[u]), (n = r && a[r]);

            if (n) {
              if (n.events)
                for (s in n.events) {
                  l[s] ? v.event.remove(i, s) : v.removeEvent(i, s, n.handle);
                }
              a[r] &&
                (delete a[r],
                f
                  ? delete i[u]
                  : i.removeAttribute
                  ? i.removeAttribute(u)
                  : (i[u] = null),
                v.deletedIds.push(r));
            }
          }
        }
      }
    }),
    (function () {
      var e, t;
      (v.uaMatch = function (e) {
        e = e.toLowerCase();
        var t =
          /(chrome)[ \/]([\w.]+)/.exec(e) ||
          /(webkit)[ \/]([\w.]+)/.exec(e) ||
          /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(e) ||
          /(msie) ([\w.]+)/.exec(e) ||
          (e.indexOf("compatible") < 0 &&
            /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(e)) ||
          [];
        return {
          browser: t[1] || "",
          version: t[2] || "0"
        };
      }),
        (e = v.uaMatch(o.userAgent)),
        (t = {}),
        e.browser && ((t[e.browser] = !0), (t.version = e.version)),
        t.chrome ? (t.webkit = !0) : t.webkit && (t.safari = !0),
        (v.browser = t),
        (v.sub = function () {
          function e(t, n) {
            return new e.fn.init(t, n);
          }

          v.extend(!0, e, this),
            (e.superclass = this),
            (e.fn = e.prototype = this()),
            (e.fn.constructor = e),
            (e.sub = this.sub),
            (e.fn.init = function (r, i) {
              return (
                i && i instanceof v && !(i instanceof e) && (i = e(i)),
                v.fn.init.call(this, r, i, t)
              );
            }),
            (e.fn.init.prototype = e.fn);
          var t = e(i);
          return e;
        });
    })();
  var Dt,
    Pt,
    Ht,
    Bt = /alpha\([^)]*\)/i,
    jt = /opacity=([^)]*)/,
    Ft = /^(top|right|bottom|left)$/,
    It = /^(none|table(?!-c[ea]).+)/,
    qt = /^margin/,
    Rt = new RegExp("^(" + m + ")(.*)$", "i"),
    Ut = new RegExp("^(" + m + ")(?!px)[a-z%]+$", "i"),
    zt = new RegExp("^([-+])=(" + m + ")", "i"),
    Wt = {
      BODY: "block"
    },
    Xt = {
      position: "absolute",
      visibility: "hidden",
      display: "block"
    },
    Vt = {
      letterSpacing: 0,
      fontWeight: 400
    },
    $t = ["Top", "Right", "Bottom", "Left"],
    Jt = ["Webkit", "O", "Moz", "ms"],
    Kt = v.fn.toggle;
  v.fn.extend({
    css: function css(e, n) {
      return v.access(
        this,
        function (e, n, r) {
          return r !== t ? v.style(e, n, r) : v.css(e, n);
        },
        e,
        n,
        arguments.length > 1
      );
    },
    show: function show() {
      return Yt(this, !0);
    },
    hide: function hide() {
      return Yt(this);
    },
    toggle: function toggle(e, t) {
      var n = typeof e == "boolean";
      return v.isFunction(e) && v.isFunction(t)
        ? Kt.apply(this, arguments)
        : this.each(function () {
            (n ? e : Gt(this)) ? v(this).show() : v(this).hide();
          });
    }
  }),
    v.extend({
      cssHooks: {
        opacity: {
          get: function get(e, t) {
            if (t) {
              var n = Dt(e, "opacity");
              return n === "" ? "1" : n;
            }
          }
        }
      },
      cssNumber: {
        fillOpacity: !0,
        fontWeight: !0,
        lineHeight: !0,
        opacity: !0,
        orphans: !0,
        widows: !0,
        zIndex: !0,
        zoom: !0
      },
      cssProps: {
        float: v.support.cssFloat ? "cssFloat" : "styleFloat"
      },
      style: function style(e, n, r, i) {
        if (!e || e.nodeType === 3 || e.nodeType === 8 || !e.style) return;
        var s,
          o,
          u,
          a = v.camelCase(n),
          f = e.style;
        (n = v.cssProps[a] || (v.cssProps[a] = Qt(f, a))),
          (u = v.cssHooks[n] || v.cssHooks[a]);
        if (r === t)
          return u && "get" in u && (s = u.get(e, !1, i)) !== t ? s : f[n];
        (o = _typeof(r)),
          o === "string" &&
            (s = zt.exec(r)) &&
            ((r = (s[1] + 1) * s[2] + parseFloat(v.css(e, n))), (o = "number"));
        if (r == null || (o === "number" && isNaN(r))) return;
        o === "number" && !v.cssNumber[a] && (r += "px");
        if (!u || !("set" in u) || (r = u.set(e, r, i)) !== t)
          try {
            f[n] = r;
          } catch (l) {}
      },
      css: function css(e, n, r, i) {
        var s,
          o,
          u,
          a = v.camelCase(n);
        return (
          (n = v.cssProps[a] || (v.cssProps[a] = Qt(e.style, a))),
          (u = v.cssHooks[n] || v.cssHooks[a]),
          u && "get" in u && (s = u.get(e, !0, i)),
          s === t && (s = Dt(e, n)),
          s === "normal" && n in Vt && (s = Vt[n]),
          r || i !== t
            ? ((o = parseFloat(s)), r || v.isNumeric(o) ? o || 0 : s)
            : s
        );
      },
      swap: function swap(e, t, n) {
        var r,
          i,
          s = {};

        for (i in t) {
          (s[i] = e.style[i]), (e.style[i] = t[i]);
        }

        r = n.call(e);

        for (i in t) {
          e.style[i] = s[i];
        }

        return r;
      }
    }),
    e.getComputedStyle
      ? (Dt = function Dt(t, n) {
          var r,
            i,
            s,
            o,
            u = e.getComputedStyle(t, null),
            a = t.style;
          return (
            u &&
              ((r = u.getPropertyValue(n) || u[n]),
              r === "" &&
                !v.contains(t.ownerDocument, t) &&
                (r = v.style(t, n)),
              Ut.test(r) &&
                qt.test(n) &&
                ((i = a.width),
                (s = a.minWidth),
                (o = a.maxWidth),
                (a.minWidth = a.maxWidth = a.width = r),
                (r = u.width),
                (a.width = i),
                (a.minWidth = s),
                (a.maxWidth = o))),
            r
          );
        })
      : i.documentElement.currentStyle &&
        (Dt = function Dt(e, t) {
          var n,
            r,
            i = e.currentStyle && e.currentStyle[t],
            s = e.style;
          return (
            i == null && s && s[t] && (i = s[t]),
            Ut.test(i) &&
              !Ft.test(t) &&
              ((n = s.left),
              (r = e.runtimeStyle && e.runtimeStyle.left),
              r && (e.runtimeStyle.left = e.currentStyle.left),
              (s.left = t === "fontSize" ? "1em" : i),
              (i = s.pixelLeft + "px"),
              (s.left = n),
              r && (e.runtimeStyle.left = r)),
            i === "" ? "auto" : i
          );
        }),
    v.each(["height", "width"], function (e, t) {
      v.cssHooks[t] = {
        get: function get(e, n, r) {
          if (n)
            return e.offsetWidth === 0 && It.test(Dt(e, "display"))
              ? v.swap(e, Xt, function () {
                  return tn(e, t, r);
                })
              : tn(e, t, r);
        },
        set: function set(e, n, r) {
          return Zt(
            e,
            n,
            r
              ? en(
                  e,
                  t,
                  r,
                  v.support.boxSizing && v.css(e, "boxSizing") === "border-box"
                )
              : 0
          );
        }
      };
    }),
    v.support.opacity ||
      (v.cssHooks.opacity = {
        get: function get(e, t) {
          return jt.test(
            (t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || ""
          )
            ? 0.01 * parseFloat(RegExp.$1) + ""
            : t
            ? "1"
            : "";
        },
        set: function set(e, t) {
          var n = e.style,
            r = e.currentStyle,
            i = v.isNumeric(t) ? "alpha(opacity=" + t * 100 + ")" : "",
            s = (r && r.filter) || n.filter || "";
          n.zoom = 1;

          if (t >= 1 && v.trim(s.replace(Bt, "")) === "" && n.removeAttribute) {
            n.removeAttribute("filter");
            if (r && !r.filter) return;
          }

          n.filter = Bt.test(s) ? s.replace(Bt, i) : s + " " + i;
        }
      }),
    v(function () {
      v.support.reliableMarginRight ||
        (v.cssHooks.marginRight = {
          get: function get(e, t) {
            return v.swap(
              e,
              {
                display: "inline-block"
              },
              function () {
                if (t) return Dt(e, "marginRight");
              }
            );
          }
        }),
        !v.support.pixelPosition &&
          v.fn.position &&
          v.each(["top", "left"], function (e, t) {
            v.cssHooks[t] = {
              get: function get(e, n) {
                if (n) {
                  var r = Dt(e, t);
                  return Ut.test(r) ? v(e).position()[t] + "px" : r;
                }
              }
            };
          });
    }),
    v.expr &&
      v.expr.filters &&
      ((v.expr.filters.hidden = function (e) {
        return (
          (e.offsetWidth === 0 && e.offsetHeight === 0) ||
          (!v.support.reliableHiddenOffsets &&
            ((e.style && e.style.display) || Dt(e, "display")) === "none")
        );
      }),
      (v.expr.filters.visible = function (e) {
        return !v.expr.filters.hidden(e);
      })),
    v.each(
      {
        margin: "",
        padding: "",
        border: "Width"
      },
      function (e, t) {
        (v.cssHooks[e + t] = {
          expand: function expand(n) {
            var r,
              i = typeof n == "string" ? n.split(" ") : [n],
              s = {};

            for (r = 0; r < 4; r++) {
              s[e + $t[r] + t] = i[r] || i[r - 2] || i[0];
            }

            return s;
          }
        }),
          qt.test(e) || (v.cssHooks[e + t].set = Zt);
      }
    );
  var rn = /%20/g,
    sn = /\[\]$/,
    on = /\r?\n/g,
    un = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,
    an = /^(?:select|textarea)/i;
  v.fn.extend({
    serialize: function serialize() {
      return v.param(this.serializeArray());
    },
    serializeArray: function serializeArray() {
      return this.map(function () {
        return this.elements ? v.makeArray(this.elements) : this;
      })
        .filter(function () {
          return (
            this.name &&
            !this.disabled &&
            (this.checked || an.test(this.nodeName) || un.test(this.type))
          );
        })
        .map(function (e, t) {
          var n = v(this).val();
          return n == null
            ? null
            : v.isArray(n)
            ? v.map(n, function (e, n) {
                return {
                  name: t.name,
                  value: e.replace(on, "\r\n")
                };
              })
            : {
                name: t.name,
                value: n.replace(on, "\r\n")
              };
        })
        .get();
    }
  }),
    (v.param = function (e, n) {
      var r,
        i = [],
        s = function s(e, t) {
          (t = v.isFunction(t) ? t() : t == null ? "" : t),
            (i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t));
        };

      n === t && (n = v.ajaxSettings && v.ajaxSettings.traditional);
      if (v.isArray(e) || (e.jquery && !v.isPlainObject(e)))
        v.each(e, function () {
          s(this.name, this.value);
        });
      else
        for (r in e) {
          fn(r, e[r], n, s);
        }
      return i.join("&").replace(rn, "+");
    });
  var ln,
    cn,
    hn = /#.*$/,
    pn = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
    dn = /^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/,
    vn = /^(?:GET|HEAD)$/,
    mn = /^\/\//,
    gn = /\?/,
    yn = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
    bn = /([?&])_=[^&]*/,
    wn = /^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,
    En = v.fn.load,
    Sn = {},
    xn = {},
    Tn = ["*/"] + ["*"];

  try {
    cn = s.href;
  } catch (Nn) {
    (cn = i.createElement("a")), (cn.href = ""), (cn = cn.href);
  }

  (ln = wn.exec(cn.toLowerCase()) || []),
    (v.fn.load = function (e, n, r) {
      if (typeof e != "string" && En) return En.apply(this, arguments);
      if (!this.length) return this;
      var i,
        s,
        o,
        u = this,
        a = e.indexOf(" ");
      return (
        a >= 0 && ((i = e.slice(a, e.length)), (e = e.slice(0, a))),
        v.isFunction(n)
          ? ((r = n), (n = t))
          : n && _typeof(n) == "object" && (s = "POST"),
        v
          .ajax({
            url: e,
            type: s,
            dataType: "html",
            data: n,
            complete: function complete(e, t) {
              r && u.each(r, o || [e.responseText, t, e]);
            }
          })
          .done(function (e) {
            (o = arguments),
              u.html(i ? v("<div>").append(e.replace(yn, "")).find(i) : e);
          }),
        this
      );
    }),
    v.each(
      "ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(
        " "
      ),
      function (e, t) {
        v.fn[t] = function (e) {
          return this.on(t, e);
        };
      }
    ),
    v.each(["get", "post"], function (e, n) {
      v[n] = function (e, r, i, s) {
        return (
          v.isFunction(r) && ((s = s || i), (i = r), (r = t)),
          v.ajax({
            type: n,
            url: e,
            data: r,
            success: i,
            dataType: s
          })
        );
      };
    }),
    v.extend({
      getScript: function getScript(e, n) {
        return v.get(e, t, n, "script");
      },
      getJSON: function getJSON(e, t, n) {
        return v.get(e, t, n, "json");
      },
      ajaxSetup: function ajaxSetup(e, t) {
        return (
          t ? Ln(e, v.ajaxSettings) : ((t = e), (e = v.ajaxSettings)),
          Ln(e, t),
          e
        );
      },
      ajaxSettings: {
        url: cn,
        isLocal: dn.test(ln[1]),
        global: !0,
        type: "GET",
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        processData: !0,
        async: !0,
        accepts: {
          xml: "application/xml, text/xml",
          html: "text/html",
          text: "text/plain",
          json: "application/json, text/javascript",
          "*": Tn
        },
        contents: {
          xml: /xml/,
          html: /html/,
          json: /json/
        },
        responseFields: {
          xml: "responseXML",
          text: "responseText"
        },
        converters: {
          "* text": e.String,
          "text html": !0,
          "text json": v.parseJSON,
          "text xml": v.parseXML
        },
        flatOptions: {
          context: !0,
          url: !0
        }
      },
      ajaxPrefilter: Cn(Sn),
      ajaxTransport: Cn(xn),
      ajax: function ajax(e, n) {
        function T(e, n, s, a) {
          var l,
            y,
            b,
            w,
            S,
            T = n;
          if (E === 2) return;
          (E = 2),
            u && clearTimeout(u),
            (o = t),
            (i = a || ""),
            (x.readyState = e > 0 ? 4 : 0),
            s && (w = An(c, x, s));
          if ((e >= 200 && e < 300) || e === 304)
            c.ifModified &&
              ((S = x.getResponseHeader("Last-Modified")),
              S && (v.lastModified[r] = S),
              (S = x.getResponseHeader("Etag")),
              S && (v.etag[r] = S)),
              e === 304
                ? ((T = "notmodified"), (l = !0))
                : ((l = On(c, w)),
                  (T = l.state),
                  (y = l.data),
                  (b = l.error),
                  (l = !b));
          else {
            b = T;
            if (!T || e) (T = "error"), e < 0 && (e = 0);
          }
          (x.status = e),
            (x.statusText = (n || T) + ""),
            l ? d.resolveWith(h, [y, T, x]) : d.rejectWith(h, [x, T, b]),
            x.statusCode(g),
            (g = t),
            f &&
              p.trigger("ajax" + (l ? "Success" : "Error"), [x, c, l ? y : b]),
            m.fireWith(h, [x, T]),
            f &&
              (p.trigger("ajaxComplete", [x, c]),
              --v.active || v.event.trigger("ajaxStop"));
        }

        _typeof(e) == "object" && ((n = e), (e = t)), (n = n || {});
        var r,
          i,
          s,
          o,
          u,
          a,
          f,
          l,
          c = v.ajaxSetup({}, n),
          h = c.context || c,
          p = h !== c && (h.nodeType || h instanceof v) ? v(h) : v.event,
          d = v.Deferred(),
          m = v.Callbacks("once memory"),
          g = c.statusCode || {},
          b = {},
          w = {},
          E = 0,
          S = "canceled",
          x = {
            readyState: 0,
            setRequestHeader: function setRequestHeader(e, t) {
              if (!E) {
                var n = e.toLowerCase();
                (e = w[n] = w[n] || e), (b[e] = t);
              }

              return this;
            },
            getAllResponseHeaders: function getAllResponseHeaders() {
              return E === 2 ? i : null;
            },
            getResponseHeader: function getResponseHeader(e) {
              var n;

              if (E === 2) {
                if (!s) {
                  s = {};

                  while ((n = pn.exec(i))) {
                    s[n[1].toLowerCase()] = n[2];
                  }
                }

                n = s[e.toLowerCase()];
              }

              return n === t ? null : n;
            },
            overrideMimeType: function overrideMimeType(e) {
              return E || (c.mimeType = e), this;
            },
            abort: function abort(e) {
              return (e = e || S), o && o.abort(e), T(0, e), this;
            }
          };
        d.promise(x),
          (x.success = x.done),
          (x.error = x.fail),
          (x.complete = m.add),
          (x.statusCode = function (e) {
            if (e) {
              var t;
              if (E < 2)
                for (t in e) {
                  g[t] = [g[t], e[t]];
                }
              else (t = e[x.status]), x.always(t);
            }

            return this;
          }),
          (c.url = ((e || c.url) + "")
            .replace(hn, "")
            .replace(mn, ln[1] + "//")),
          (c.dataTypes = v
            .trim(c.dataType || "*")
            .toLowerCase()
            .split(y)),
          c.crossDomain == null &&
            ((a = wn.exec(c.url.toLowerCase())),
            (c.crossDomain = !(
              !a ||
              (a[1] === ln[1] &&
                a[2] === ln[2] &&
                (a[3] || (a[1] === "http:" ? 80 : 443)) ==
                  (ln[3] || (ln[1] === "http:" ? 80 : 443)))
            ))),
          c.data &&
            c.processData &&
            typeof c.data != "string" &&
            (c.data = v.param(c.data, c.traditional)),
          kn(Sn, c, n, x);
        if (E === 2) return x;
        (f = c.global),
          (c.type = c.type.toUpperCase()),
          (c.hasContent = !vn.test(c.type)),
          f && v.active++ === 0 && v.event.trigger("ajaxStart");

        if (!c.hasContent) {
          c.data &&
            ((c.url += (gn.test(c.url) ? "&" : "?") + c.data), delete c.data),
            (r = c.url);

          if (c.cache === !1) {
            var N = v.now(),
              C = c.url.replace(bn, "$1_=" + N);
            c.url =
              C + (C === c.url ? (gn.test(c.url) ? "&" : "?") + "_=" + N : "");
          }
        }

        ((c.data && c.hasContent && c.contentType !== !1) || n.contentType) &&
          x.setRequestHeader("Content-Type", c.contentType),
          c.ifModified &&
            ((r = r || c.url),
            v.lastModified[r] &&
              x.setRequestHeader("If-Modified-Since", v.lastModified[r]),
            v.etag[r] && x.setRequestHeader("If-None-Match", v.etag[r])),
          x.setRequestHeader(
            "Accept",
            c.dataTypes[0] && c.accepts[c.dataTypes[0]]
              ? c.accepts[c.dataTypes[0]] +
                  (c.dataTypes[0] !== "*" ? ", " + Tn + "; q=0.01" : "")
              : c.accepts["*"]
          );

        for (l in c.headers) {
          x.setRequestHeader(l, c.headers[l]);
        }

        if (!c.beforeSend || (c.beforeSend.call(h, x, c) !== !1 && E !== 2)) {
          S = "abort";

          for (l in {
            success: 1,
            error: 1,
            complete: 1
          }) {
            x[l](c[l]);
          }

          o = kn(xn, c, n, x);
          if (!o) T(-1, "No Transport");
          else {
            (x.readyState = 1),
              f && p.trigger("ajaxSend", [x, c]),
              c.async &&
                c.timeout > 0 &&
                (u = setTimeout(function () {
                  x.abort("timeout");
                }, c.timeout));

            try {
              (E = 1), o.send(b, T);
            } catch (k) {
              if (!(E < 2)) throw k;
              T(-1, k);
            }
          }
          return x;
        }

        return x.abort();
      },
      active: 0,
      lastModified: {},
      etag: {}
    });
  var Mn = [],
    _n = /\?/,
    Dn = /(=)\?(?=&|$)|\?\?/,
    Pn = v.now();
  v.ajaxSetup({
    jsonp: "callback",
    jsonpCallback: function jsonpCallback() {
      var e = Mn.pop() || v.expando + "_" + Pn++;
      return (this[e] = !0), e;
    }
  }),
    v.ajaxPrefilter("json jsonp", function (n, r, i) {
      var s,
        o,
        u,
        a = n.data,
        f = n.url,
        l = n.jsonp !== !1,
        c = l && Dn.test(f),
        h =
          l &&
          !c &&
          typeof a == "string" &&
          !(n.contentType || "").indexOf("application/x-www-form-urlencoded") &&
          Dn.test(a);
      if (n.dataTypes[0] === "jsonp" || c || h)
        return (
          (s = n.jsonpCallback = v.isFunction(n.jsonpCallback)
            ? n.jsonpCallback()
            : n.jsonpCallback),
          (o = e[s]),
          c
            ? (n.url = f.replace(Dn, "$1" + s))
            : h
            ? (n.data = a.replace(Dn, "$1" + s))
            : l && (n.url += (_n.test(f) ? "&" : "?") + n.jsonp + "=" + s),
          (n.converters["script json"] = function () {
            return u || v.error(s + " was not called"), u[0];
          }),
          (n.dataTypes[0] = "json"),
          (e[s] = function () {
            u = arguments;
          }),
          i.always(function () {
            (e[s] = o),
              n[s] && ((n.jsonpCallback = r.jsonpCallback), Mn.push(s)),
              u && v.isFunction(o) && o(u[0]),
              (u = o = t);
          }),
          "script"
        );
    }),
    v.ajaxSetup({
      accepts: {
        script:
          "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
      },
      contents: {
        script: /javascript|ecmascript/
      },
      converters: {
        "text script": function textScript(e) {
          return v.globalEval(e), e;
        }
      }
    }),
    v.ajaxPrefilter("script", function (e) {
      e.cache === t && (e.cache = !1),
        e.crossDomain && ((e.type = "GET"), (e.global = !1));
    }),
    v.ajaxTransport("script", function (e) {
      if (e.crossDomain) {
        var n,
          r = i.head || i.getElementsByTagName("head")[0] || i.documentElement;
        return {
          send: function send(s, o) {
            (n = i.createElement("script")),
              (n.async = "async"),
              e.scriptCharset && (n.charset = e.scriptCharset),
              (n.src = e.url),
              (n.onload = n.onreadystatechange = function (e, i) {
                if (i || !n.readyState || /loaded|complete/.test(n.readyState))
                  (n.onload = n.onreadystatechange = null),
                    r && n.parentNode && r.removeChild(n),
                    (n = t),
                    i || o(200, "success");
              }),
              r.insertBefore(n, r.firstChild);
          },
          abort: function abort() {
            n && n.onload(0, 1);
          }
        };
      }
    });
  var Hn,
    Bn = e.ActiveXObject
      ? function () {
          for (var e in Hn) {
            Hn[e](0, 1);
          }
        }
      : !1,
    jn = 0;
  (v.ajaxSettings.xhr = e.ActiveXObject
    ? function () {
        return (!this.isLocal && Fn()) || In();
      }
    : Fn),
    (function (e) {
      v.extend(v.support, {
        ajax: !!e,
        cors: !!e && "withCredentials" in e
      });
    })(v.ajaxSettings.xhr()),
    v.support.ajax &&
      v.ajaxTransport(function (n) {
        if (!n.crossDomain || v.support.cors) {
          var _r;

          return {
            send: function send(i, s) {
              var o,
                u,
                a = n.xhr();
              n.username
                ? a.open(n.type, n.url, n.async, n.username, n.password)
                : a.open(n.type, n.url, n.async);
              if (n.xhrFields)
                for (u in n.xhrFields) {
                  a[u] = n.xhrFields[u];
                }
              n.mimeType &&
                a.overrideMimeType &&
                a.overrideMimeType(n.mimeType),
                !n.crossDomain &&
                  !i["X-Requested-With"] &&
                  (i["X-Requested-With"] = "XMLHttpRequest");

              try {
                for (u in i) {
                  a.setRequestHeader(u, i[u]);
                }
              } catch (f) {}

              a.send((n.hasContent && n.data) || null),
                (_r = function r(e, i) {
                  var u, f, l, c, h;

                  try {
                    if (_r && (i || a.readyState === 4)) {
                      (_r = t),
                        o &&
                          ((a.onreadystatechange = v.noop), Bn && delete Hn[o]);
                      if (i) a.readyState !== 4 && a.abort();
                      else {
                        (u = a.status),
                          (l = a.getAllResponseHeaders()),
                          (c = {}),
                          (h = a.responseXML),
                          h && h.documentElement && (c.xml = h);

                        try {
                          c.text = a.responseText;
                        } catch (p) {}

                        try {
                          f = a.statusText;
                        } catch (p) {
                          f = "";
                        }

                        !u && n.isLocal && !n.crossDomain
                          ? (u = c.text ? 200 : 404)
                          : u === 1223 && (u = 204);
                      }
                    }
                  } catch (d) {
                    i || s(-1, d);
                  }

                  c && s(u, f, c, l);
                }),
                n.async
                  ? a.readyState === 4
                    ? setTimeout(_r, 0)
                    : ((o = ++jn),
                      Bn && (Hn || ((Hn = {}), v(e).unload(Bn)), (Hn[o] = _r)),
                      (a.onreadystatechange = _r))
                  : _r();
            },
            abort: function abort() {
              _r && _r(0, 1);
            }
          };
        }
      });
  var qn,
    Rn,
    Un = /^(?:toggle|show|hide)$/,
    zn = new RegExp("^(?:([-+])=|)(" + m + ")([a-z%]*)$", "i"),
    Wn = /queueHooks$/,
    Xn = [Gn],
    Vn = {
      "*": [
        function (e, t) {
          var n,
            r,
            i = this.createTween(e, t),
            s = zn.exec(t),
            o = i.cur(),
            u = +o || 0,
            a = 1,
            f = 20;

          if (s) {
            (n = +s[2]), (r = s[3] || (v.cssNumber[e] ? "" : "px"));

            if (r !== "px" && u) {
              u = v.css(i.elem, e, !0) || n || 1;

              do {
                (a = a || ".5"), (u /= a), v.style(i.elem, e, u + r);
              } while (a !== (a = i.cur() / o) && a !== 1 && --f);
            }

            (i.unit = r),
              (i.start = u),
              (i.end = s[1] ? u + (s[1] + 1) * n : n);
          }

          return i;
        }
      ]
    };
  (v.Animation = v.extend(Kn, {
    tweener: function tweener(e, t) {
      v.isFunction(e) ? ((t = e), (e = ["*"])) : (e = e.split(" "));
      var n,
        r = 0,
        i = e.length;

      for (; r < i; r++) {
        (n = e[r]), (Vn[n] = Vn[n] || []), Vn[n].unshift(t);
      }
    },
    prefilter: function prefilter(e, t) {
      t ? Xn.unshift(e) : Xn.push(e);
    }
  })),
    (v.Tween = Yn),
    (Yn.prototype = {
      constructor: Yn,
      init: function init(e, t, n, r, i, s) {
        (this.elem = e),
          (this.prop = n),
          (this.easing = i || "swing"),
          (this.options = t),
          (this.start = this.now = this.cur()),
          (this.end = r),
          (this.unit = s || (v.cssNumber[n] ? "" : "px"));
      },
      cur: function cur() {
        var e = Yn.propHooks[this.prop];
        return e && e.get ? e.get(this) : Yn.propHooks._default.get(this);
      },
      run: function run(e) {
        var t,
          n = Yn.propHooks[this.prop];
        return (
          this.options.duration
            ? (this.pos = t = v.easing[this.easing](
                e,
                this.options.duration * e,
                0,
                1,
                this.options.duration
              ))
            : (this.pos = t = e),
          (this.now = (this.end - this.start) * t + this.start),
          this.options.step &&
            this.options.step.call(this.elem, this.now, this),
          n && n.set ? n.set(this) : Yn.propHooks._default.set(this),
          this
        );
      }
    }),
    (Yn.prototype.init.prototype = Yn.prototype),
    (Yn.propHooks = {
      _default: {
        get: function get(e) {
          var t;
          return e.elem[e.prop] == null ||
            (!!e.elem.style && e.elem.style[e.prop] != null)
            ? ((t = v.css(e.elem, e.prop, !1, "")), !t || t === "auto" ? 0 : t)
            : e.elem[e.prop];
        },
        set: function set(e) {
          v.fx.step[e.prop]
            ? v.fx.step[e.prop](e)
            : e.elem.style &&
              (e.elem.style[v.cssProps[e.prop]] != null || v.cssHooks[e.prop])
            ? v.style(e.elem, e.prop, e.now + e.unit)
            : (e.elem[e.prop] = e.now);
        }
      }
    }),
    (Yn.propHooks.scrollTop = Yn.propHooks.scrollLeft = {
      set: function set(e) {
        e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now);
      }
    }),
    v.each(["toggle", "show", "hide"], function (e, t) {
      var n = v.fn[t];

      v.fn[t] = function (r, i, s) {
        return r == null ||
          typeof r == "boolean" ||
          (!e && v.isFunction(r) && v.isFunction(i))
          ? n.apply(this, arguments)
          : this.animate(Zn(t, !0), r, i, s);
      };
    }),
    v.fn.extend({
      fadeTo: function fadeTo(e, t, n, r) {
        return this.filter(Gt).css("opacity", 0).show().end().animate(
          {
            opacity: t
          },
          e,
          n,
          r
        );
      },
      animate: function animate(e, t, n, r) {
        var i = v.isEmptyObject(e),
          s = v.speed(t, n, r),
          o = function o() {
            var t = Kn(this, v.extend({}, e), s);
            i && t.stop(!0);
          };

        return i || s.queue === !1 ? this.each(o) : this.queue(s.queue, o);
      },
      stop: function stop(e, n, r) {
        var i = function i(e) {
          var t = e.stop;
          delete e.stop, t(r);
        };

        return (
          typeof e != "string" && ((r = n), (n = e), (e = t)),
          n && e !== !1 && this.queue(e || "fx", []),
          this.each(function () {
            var t = !0,
              n = e != null && e + "queueHooks",
              s = v.timers,
              o = v._data(this);

            if (n) o[n] && o[n].stop && i(o[n]);
            else
              for (n in o) {
                o[n] && o[n].stop && Wn.test(n) && i(o[n]);
              }

            for (n = s.length; n--; ) {
              s[n].elem === this &&
                (e == null || s[n].queue === e) &&
                (s[n].anim.stop(r), (t = !1), s.splice(n, 1));
            }

            (t || !r) && v.dequeue(this, e);
          })
        );
      }
    }),
    v.each(
      {
        slideDown: Zn("show"),
        slideUp: Zn("hide"),
        slideToggle: Zn("toggle"),
        fadeIn: {
          opacity: "show"
        },
        fadeOut: {
          opacity: "hide"
        },
        fadeToggle: {
          opacity: "toggle"
        }
      },
      function (e, t) {
        v.fn[e] = function (e, n, r) {
          return this.animate(t, e, n, r);
        };
      }
    ),
    (v.speed = function (e, t, n) {
      var r =
        e && _typeof(e) == "object"
          ? v.extend({}, e)
          : {
              complete: n || (!n && t) || (v.isFunction(e) && e),
              duration: e,
              easing: (n && t) || (t && !v.isFunction(t) && t)
            };
      r.duration = v.fx.off
        ? 0
        : typeof r.duration == "number"
        ? r.duration
        : r.duration in v.fx.speeds
        ? v.fx.speeds[r.duration]
        : v.fx.speeds._default;
      if (r.queue == null || r.queue === !0) r.queue = "fx";
      return (
        (r.old = r.complete),
        (r.complete = function () {
          v.isFunction(r.old) && r.old.call(this),
            r.queue && v.dequeue(this, r.queue);
        }),
        r
      );
    }),
    (v.easing = {
      linear: function linear(e) {
        return e;
      },
      swing: function swing(e) {
        return 0.5 - Math.cos(e * Math.PI) / 2;
      }
    }),
    (v.timers = []),
    (v.fx = Yn.prototype.init),
    (v.fx.tick = function () {
      var e,
        n = v.timers,
        r = 0;
      qn = v.now();

      for (; r < n.length; r++) {
        (e = n[r]), !e() && n[r] === e && n.splice(r--, 1);
      }

      n.length || v.fx.stop(), (qn = t);
    }),
    (v.fx.timer = function (e) {
      e() &&
        v.timers.push(e) &&
        !Rn &&
        (Rn = setInterval(v.fx.tick, v.fx.interval));
    }),
    (v.fx.interval = 13),
    (v.fx.stop = function () {
      clearInterval(Rn), (Rn = null);
    }),
    (v.fx.speeds = {
      slow: 600,
      fast: 200,
      _default: 400
    }),
    (v.fx.step = {}),
    v.expr &&
      v.expr.filters &&
      (v.expr.filters.animated = function (e) {
        return v.grep(v.timers, function (t) {
          return e === t.elem;
        }).length;
      });
  var er = /^(?:body|html)$/i;
  (v.fn.offset = function (e) {
    if (arguments.length)
      return e === t
        ? this
        : this.each(function (t) {
            v.offset.setOffset(this, e, t);
          });
    var n,
      r,
      i,
      s,
      o,
      u,
      a,
      f = {
        top: 0,
        left: 0
      },
      l = this[0],
      c = l && l.ownerDocument;
    if (!c) return;
    return (r = c.body) === l
      ? v.offset.bodyOffset(l)
      : ((n = c.documentElement),
        v.contains(n, l)
          ? (typeof l.getBoundingClientRect != "undefined" &&
              (f = l.getBoundingClientRect()),
            (i = tr(c)),
            (s = n.clientTop || r.clientTop || 0),
            (o = n.clientLeft || r.clientLeft || 0),
            (u = i.pageYOffset || n.scrollTop),
            (a = i.pageXOffset || n.scrollLeft),
            {
              top: f.top + u - s,
              left: f.left + a - o
            })
          : f);
  }),
    (v.offset = {
      bodyOffset: function bodyOffset(e) {
        var t = e.offsetTop,
          n = e.offsetLeft;
        return (
          v.support.doesNotIncludeMarginInBodyOffset &&
            ((t += parseFloat(v.css(e, "marginTop")) || 0),
            (n += parseFloat(v.css(e, "marginLeft")) || 0)),
          {
            top: t,
            left: n
          }
        );
      },
      setOffset: function setOffset(e, t, n) {
        var r = v.css(e, "position");
        r === "static" && (e.style.position = "relative");
        var i = v(e),
          s = i.offset(),
          o = v.css(e, "top"),
          u = v.css(e, "left"),
          a =
            (r === "absolute" || r === "fixed") &&
            v.inArray("auto", [o, u]) > -1,
          f = {},
          l = {},
          c,
          h;
        a
          ? ((l = i.position()), (c = l.top), (h = l.left))
          : ((c = parseFloat(o) || 0), (h = parseFloat(u) || 0)),
          v.isFunction(t) && (t = t.call(e, n, s)),
          t.top != null && (f.top = t.top - s.top + c),
          t.left != null && (f.left = t.left - s.left + h),
          "using" in t ? t.using.call(e, f) : i.css(f);
      }
    }),
    v.fn.extend({
      position: function position() {
        if (!this[0]) return;
        var e = this[0],
          t = this.offsetParent(),
          n = this.offset(),
          r = er.test(t[0].nodeName)
            ? {
                top: 0,
                left: 0
              }
            : t.offset();
        return (
          (n.top -= parseFloat(v.css(e, "marginTop")) || 0),
          (n.left -= parseFloat(v.css(e, "marginLeft")) || 0),
          (r.top += parseFloat(v.css(t[0], "borderTopWidth")) || 0),
          (r.left += parseFloat(v.css(t[0], "borderLeftWidth")) || 0),
          {
            top: n.top - r.top,
            left: n.left - r.left
          }
        );
      },
      offsetParent: function offsetParent() {
        return this.map(function () {
          var e = this.offsetParent || i.body;

          while (
            e &&
            !er.test(e.nodeName) &&
            v.css(e, "position") === "static"
          ) {
            e = e.offsetParent;
          }

          return e || i.body;
        });
      }
    }),
    v.each(
      {
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
      },
      function (e, n) {
        var r = /Y/.test(n);

        v.fn[e] = function (i) {
          return v.access(
            this,
            function (e, i, s) {
              var o = tr(e);
              if (s === t)
                return o
                  ? n in o
                    ? o[n]
                    : o.document.documentElement[i]
                  : e[i];
              o
                ? o.scrollTo(
                    r ? v(o).scrollLeft() : s,
                    r ? s : v(o).scrollTop()
                  )
                : (e[i] = s);
            },
            e,
            i,
            arguments.length,
            null
          );
        };
      }
    ),
    v.each(
      {
        Height: "height",
        Width: "width"
      },
      function (e, n) {
        v.each(
          {
            padding: "inner" + e,
            content: n,
            "": "outer" + e
          },
          function (r, i) {
            v.fn[i] = function (i, s) {
              var o = arguments.length && (r || typeof i != "boolean"),
                u = r || (i === !0 || s === !0 ? "margin" : "border");
              return v.access(
                this,
                function (n, r, i) {
                  var s;
                  return v.isWindow(n)
                    ? n.document.documentElement["client" + e]
                    : n.nodeType === 9
                    ? ((s = n.documentElement),
                      Math.max(
                        n.body["scroll" + e],
                        s["scroll" + e],
                        n.body["offset" + e],
                        s["offset" + e],
                        s["client" + e]
                      ))
                    : i === t
                    ? v.css(n, r, i, u)
                    : v.style(n, r, i, u);
                },
                n,
                o ? i : t,
                o,
                null
              );
            };
          }
        );
      }
    ),
    (e.jQuery = e.$ = v),
    typeof define == "function" &&
      define.amd &&
      define.amd.jQuery &&
      define("jquery", [], function () {
        return v;
      });
})(window);
// abcbtesting


function _typeof(obj) {
  "@babel/helpers - typeof";
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj &&
        typeof Symbol === "function" &&
        obj.constructor === Symbol &&
        obj !== Symbol.prototype
        ? "symbol"
        : typeof obj;
    };
  }
  return _typeof(obj);
}

/**
 * Swiper 4.5.3
 * Most modern mobile touch slider and framework with hardware accelerated transitions
 * http://www.idangero.us/swiper/
 *
 * Copyright 2014-2019 Vladimir Kharlampidi
 *
 * Released under the MIT License
 *
 * Released on: October 16, 2019
 */
(function (global, factory) {
  (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ===
    "object" && typeof module !== "undefined"
    ? (module.exports = factory())
    : typeof define === "function" && define.amd
    ? define(factory)
    : ((global = global || self), (global.Swiper = factory()));
})(void 0, function () {
  "use strict";
  /**
   * SSR Window 1.0.1
   * Better handling for window object in SSR environment
   * https://github.com/nolimits4web/ssr-window
   *
   * Copyright 2018, Vladimir Kharlampidi
   *
   * Licensed under MIT
   *
   * Released on: July 18, 2018
   */

  var doc =
    typeof document === "undefined"
      ? {
          body: {},
          addEventListener: function addEventListener() {},
          removeEventListener: function removeEventListener() {},
          activeElement: {
            blur: function blur() {},
            nodeName: "",
          },
          querySelector: function querySelector() {
            return null;
          },
          querySelectorAll: function querySelectorAll() {
            return [];
          },
          getElementById: function getElementById() {
            return null;
          },
          createEvent: function createEvent() {
            return {
              initEvent: function initEvent() {},
            };
          },
          createElement: function createElement() {
            return {
              children: [],
              childNodes: [],
              style: {},
              setAttribute: function setAttribute() {},
              getElementsByTagName: function getElementsByTagName() {
                return [];
              },
            };
          },
          location: {
            hash: "",
          },
        }
      : document; // eslint-disable-line

  var win =
    typeof window === "undefined"
      ? {
          document: doc,
          navigator: {
            userAgent: "",
          },
          location: {},
          history: {},
          CustomEvent: function CustomEvent() {
            return this;
          },
          addEventListener: function addEventListener() {},
          removeEventListener: function removeEventListener() {},
          getComputedStyle: function getComputedStyle() {
            return {
              getPropertyValue: function getPropertyValue() {
                return "";
              },
            };
          },
          Image: function Image() {},
          Date: function Date() {},
          screen: {},
          setTimeout: function setTimeout() {},
          clearTimeout: function clearTimeout() {},
        }
      : window; // eslint-disable-line

  /**
   * Dom7 2.1.3
   * Minimalistic JavaScript library for DOM manipulation, with a jQuery-compatible API
   * http://framework7.io/docs/dom.html
   *
   * Copyright 2019, Vladimir Kharlampidi
   * The iDangero.us
   * http://www.idangero.us/
   *
   * Licensed under MIT
   *
   * Released on: February 11, 2019
   */

  var Dom7 = function Dom7(arr) {
    var self = this; // Create array-like object

    for (var i = 0; i < arr.length; i += 1) {
      self[i] = arr[i];
    }

    self.length = arr.length; // Return collection with methods

    return this;
  };

  function $(selector, context) {
    var arr = [];
    var i = 0;

    if (selector && !context) {
      if (selector instanceof Dom7) {
        return selector;
      }
    }

    if (selector) {
      // String
      if (typeof selector === "string") {
        var els;
        var tempParent;
        var html = selector.trim();

        if (html.indexOf("<") >= 0 && html.indexOf(">") >= 0) {
          var toCreate = "div";

          if (html.indexOf("<li") === 0) {
            toCreate = "ul";
          }

          if (html.indexOf("<tr") === 0) {
            toCreate = "tbody";
          }

          if (html.indexOf("<td") === 0 || html.indexOf("<th") === 0) {
            toCreate = "tr";
          }

          if (html.indexOf("<tbody") === 0) {
            toCreate = "table";
          }

          if (html.indexOf("<option") === 0) {
            toCreate = "select";
          }

          tempParent = doc.createElement(toCreate);
          tempParent.innerHTML = html;

          for (i = 0; i < tempParent.childNodes.length; i += 1) {
            arr.push(tempParent.childNodes[i]);
          }
        } else {
          if (!context && selector[0] === "#" && !selector.match(/[ .<>:~]/)) {
            // Pure ID selector
            els = [doc.getElementById(selector.trim().split("#")[1])];
          } else {
            // Other selectors
            els = (context || doc).querySelectorAll(selector.trim());
          }

          for (i = 0; i < els.length; i += 1) {
            if (els[i]) {
              arr.push(els[i]);
            }
          }
        }
      } else if (selector.nodeType || selector === win || selector === doc) {
        // Node/element
        arr.push(selector);
      } else if (selector.length > 0 && selector[0].nodeType) {
        // Array of elements or instance of Dom
        for (i = 0; i < selector.length; i += 1) {
          arr.push(selector[i]);
        }
      }
    }

    return new Dom7(arr);
  }

  $.fn = Dom7.prototype;
  $.Class = Dom7;
  $.Dom7 = Dom7;

  function unique(arr) {
    var uniqueArray = [];

    for (var i = 0; i < arr.length; i += 1) {
      if (uniqueArray.indexOf(arr[i]) === -1) {
        uniqueArray.push(arr[i]);
      }
    }

    return uniqueArray;
  } // Classes and attributes

  function addClass(className) {
    if (typeof className === "undefined") {
      return this;
    }

    var classes = className.split(" ");

    for (var i = 0; i < classes.length; i += 1) {
      for (var j = 0; j < this.length; j += 1) {
        if (
          typeof this[j] !== "undefined" &&
          typeof this[j].classList !== "undefined"
        ) {
          this[j].classList.add(classes[i]);
        }
      }
    }

    return this;
  }

  function removeClass(className) {
    var classes = className.split(" ");

    for (var i = 0; i < classes.length; i += 1) {
      for (var j = 0; j < this.length; j += 1) {
        if (
          typeof this[j] !== "undefined" &&
          typeof this[j].classList !== "undefined"
        ) {
          this[j].classList.remove(classes[i]);
        }
      }
    }

    return this;
  }

  function hasClass(className) {
    if (!this[0]) {
      return false;
    }

    return this[0].classList.contains(className);
  }

  function toggleClass(className) {
    var classes = className.split(" ");

    for (var i = 0; i < classes.length; i += 1) {
      for (var j = 0; j < this.length; j += 1) {
        if (
          typeof this[j] !== "undefined" &&
          typeof this[j].classList !== "undefined"
        ) {
          this[j].classList.toggle(classes[i]);
        }
      }
    }

    return this;
  }

  function attr(attrs, value) {
    var arguments$1 = arguments;

    if (arguments.length === 1 && typeof attrs === "string") {
      // Get attr
      if (this[0]) {
        return this[0].getAttribute(attrs);
      }

      return undefined;
    } // Set attrs

    for (var i = 0; i < this.length; i += 1) {
      if (arguments$1.length === 2) {
        // String
        this[i].setAttribute(attrs, value);
      } else {
        // Object
        // eslint-disable-next-line
        for (var attrName in attrs) {
          this[i][attrName] = attrs[attrName];
          this[i].setAttribute(attrName, attrs[attrName]);
        }
      }
    }

    return this;
  } // eslint-disable-next-line

  function removeAttr(attr) {
    for (var i = 0; i < this.length; i += 1) {
      this[i].removeAttribute(attr);
    }

    return this;
  }

  function data(key, value) {
    var el;

    if (typeof value === "undefined") {
      el = this[0]; // Get value

      if (el) {
        if (el.dom7ElementDataStorage && key in el.dom7ElementDataStorage) {
          return el.dom7ElementDataStorage[key];
        }

        var dataKey = el.getAttribute("data-" + key);

        if (dataKey) {
          return dataKey;
        }

        return undefined;
      }

      return undefined;
    } // Set value

    for (var i = 0; i < this.length; i += 1) {
      el = this[i];

      if (!el.dom7ElementDataStorage) {
        el.dom7ElementDataStorage = {};
      }

      el.dom7ElementDataStorage[key] = value;
    }

    return this;
  } // Transforms
  // eslint-disable-next-line

  function transform(transform) {
    for (var i = 0; i < this.length; i += 1) {
      var elStyle = this[i].style;
      elStyle.webkitTransform = transform;
      elStyle.transform = transform;
    }

    return this;
  }

  function transition(duration) {
    if (typeof duration !== "string") {
      duration = duration + "ms"; // eslint-disable-line
    }

    for (var i = 0; i < this.length; i += 1) {
      var elStyle = this[i].style;
      elStyle.webkitTransitionDuration = duration;
      elStyle.transitionDuration = duration;
    }

    return this;
  } // Events

  function on() {
    var assign;
    var args = [],
      len = arguments.length;

    while (len--) {
      args[len] = arguments[len];
    }

    var eventType = args[0];
    var targetSelector = args[1];
    var listener = args[2];
    var capture = args[3];

    if (typeof args[1] === "function") {
      (assign = args),
        (eventType = assign[0]),
        (listener = assign[1]),
        (capture = assign[2]);
      targetSelector = undefined;
    }

    if (!capture) {
      capture = false;
    }

    function handleLiveEvent(e) {
      var target = e.target;

      if (!target) {
        return;
      }

      var eventData = e.target.dom7EventData || [];

      if (eventData.indexOf(e) < 0) {
        eventData.unshift(e);
      }

      if ($(target).is(targetSelector)) {
        listener.apply(target, eventData);
      } else {
        var parents = $(target).parents(); // eslint-disable-line

        for (var k = 0; k < parents.length; k += 1) {
          if ($(parents[k]).is(targetSelector)) {
            listener.apply(parents[k], eventData);
          }
        }
      }
    }

    function handleEvent(e) {
      var eventData = e && e.target ? e.target.dom7EventData || [] : [];

      if (eventData.indexOf(e) < 0) {
        eventData.unshift(e);
      }

      listener.apply(this, eventData);
    }

    var events = eventType.split(" ");
    var j;

    for (var i = 0; i < this.length; i += 1) {
      var el = this[i];

      if (!targetSelector) {
        for (j = 0; j < events.length; j += 1) {
          var event = events[j];

          if (!el.dom7Listeners) {
            el.dom7Listeners = {};
          }

          if (!el.dom7Listeners[event]) {
            el.dom7Listeners[event] = [];
          }

          el.dom7Listeners[event].push({
            listener: listener,
            proxyListener: handleEvent,
          });
          el.addEventListener(event, handleEvent, capture);
        }
      } else {
        // Live events
        for (j = 0; j < events.length; j += 1) {
          var event$1 = events[j];

          if (!el.dom7LiveListeners) {
            el.dom7LiveListeners = {};
          }

          if (!el.dom7LiveListeners[event$1]) {
            el.dom7LiveListeners[event$1] = [];
          }

          el.dom7LiveListeners[event$1].push({
            listener: listener,
            proxyListener: handleLiveEvent,
          });
          el.addEventListener(event$1, handleLiveEvent, capture);
        }
      }
    }

    return this;
  }

  function off() {
    var assign;
    var args = [],
      len = arguments.length;

    while (len--) {
      args[len] = arguments[len];
    }

    var eventType = args[0];
    var targetSelector = args[1];
    var listener = args[2];
    var capture = args[3];

    if (typeof args[1] === "function") {
      (assign = args),
        (eventType = assign[0]),
        (listener = assign[1]),
        (capture = assign[2]);
      targetSelector = undefined;
    }

    if (!capture) {
      capture = false;
    }

    var events = eventType.split(" ");

    for (var i = 0; i < events.length; i += 1) {
      var event = events[i];

      for (var j = 0; j < this.length; j += 1) {
        var el = this[j];
        var handlers = void 0;

        if (!targetSelector && el.dom7Listeners) {
          handlers = el.dom7Listeners[event];
        } else if (targetSelector && el.dom7LiveListeners) {
          handlers = el.dom7LiveListeners[event];
        }

        if (handlers && handlers.length) {
          for (var k = handlers.length - 1; k >= 0; k -= 1) {
            var handler = handlers[k];

            if (listener && handler.listener === listener) {
              el.removeEventListener(event, handler.proxyListener, capture);
              handlers.splice(k, 1);
            } else if (
              listener &&
              handler.listener &&
              handler.listener.dom7proxy &&
              handler.listener.dom7proxy === listener
            ) {
              el.removeEventListener(event, handler.proxyListener, capture);
              handlers.splice(k, 1);
            } else if (!listener) {
              el.removeEventListener(event, handler.proxyListener, capture);
              handlers.splice(k, 1);
            }
          }
        }
      }
    }

    return this;
  }

  function trigger() {
    var args = [],
      len = arguments.length;

    while (len--) {
      args[len] = arguments[len];
    }

    var events = args[0].split(" ");
    var eventData = args[1];

    for (var i = 0; i < events.length; i += 1) {
      var event = events[i];

      for (var j = 0; j < this.length; j += 1) {
        var el = this[j];
        var evt = void 0;

        try {
          evt = new win.CustomEvent(event, {
            detail: eventData,
            bubbles: true,
            cancelable: true,
          });
        } catch (e) {
          evt = doc.createEvent("Event");
          evt.initEvent(event, true, true);
          evt.detail = eventData;
        } // eslint-disable-next-line

        el.dom7EventData = args.filter(function (data, dataIndex) {
          return dataIndex > 0;
        });
        el.dispatchEvent(evt);
        el.dom7EventData = [];
        delete el.dom7EventData;
      }
    }

    return this;
  }

  function transitionEnd(callback) {
    var events = ["webkitTransitionEnd", "transitionend"];
    var dom = this;
    var i;

    function fireCallBack(e) {
      /* jshint validthis:true */
      if (e.target !== this) {
        return;
      }

      callback.call(this, e);

      for (i = 0; i < events.length; i += 1) {
        dom.off(events[i], fireCallBack);
      }
    }

    if (callback) {
      for (i = 0; i < events.length; i += 1) {
        dom.on(events[i], fireCallBack);
      }
    }

    return this;
  }

  function outerWidth(includeMargins) {
    if (this.length > 0) {
      if (includeMargins) {
        // eslint-disable-next-line
        var styles = this.styles();
        return (
          this[0].offsetWidth +
          parseFloat(styles.getPropertyValue("margin-right")) +
          parseFloat(styles.getPropertyValue("margin-left"))
        );
      }

      return this[0].offsetWidth;
    }

    return null;
  }

  function outerHeight(includeMargins) {
    if (this.length > 0) {
      if (includeMargins) {
        // eslint-disable-next-line
        var styles = this.styles();
        return (
          this[0].offsetHeight +
          parseFloat(styles.getPropertyValue("margin-top")) +
          parseFloat(styles.getPropertyValue("margin-bottom"))
        );
      }

      return this[0].offsetHeight;
    }

    return null;
  }

  function offset() {
    if (this.length > 0) {
      var el = this[0];
      var box = el.getBoundingClientRect();
      var body = doc.body;
      var clientTop = el.clientTop || body.clientTop || 0;
      var clientLeft = el.clientLeft || body.clientLeft || 0;
      var scrollTop = el === win ? win.scrollY : el.scrollTop;
      var scrollLeft = el === win ? win.scrollX : el.scrollLeft;
      return {
        top: box.top + scrollTop - clientTop,
        left: box.left + scrollLeft - clientLeft,
      };
    }

    return null;
  }

  function styles() {
    if (this[0]) {
      return win.getComputedStyle(this[0], null);
    }

    return {};
  }

  function css(props, value) {
    var i;

    if (arguments.length === 1) {
      if (typeof props === "string") {
        if (this[0]) {
          return win.getComputedStyle(this[0], null).getPropertyValue(props);
        }
      } else {
        for (i = 0; i < this.length; i += 1) {
          // eslint-disable-next-line
          for (var prop in props) {
            this[i].style[prop] = props[prop];
          }
        }

        return this;
      }
    }

    if (arguments.length === 2 && typeof props === "string") {
      for (i = 0; i < this.length; i += 1) {
        this[i].style[props] = value;
      }

      return this;
    }

    return this;
  } // Iterate over the collection passing elements to `callback`

  function each(callback) {
    // Don't bother continuing without a callback
    if (!callback) {
      return this;
    } // Iterate over the current collection

    for (var i = 0; i < this.length; i += 1) {
      // If the callback returns false
      if (callback.call(this[i], i, this[i]) === false) {
        // End the loop early
        return this;
      }
    } // Return `this` to allow chained DOM operations

    return this;
  } // eslint-disable-next-line

  function html(html) {
    if (typeof html === "undefined") {
      return this[0] ? this[0].innerHTML : undefined;
    }

    for (var i = 0; i < this.length; i += 1) {
      this[i].innerHTML = html;
    }

    return this;
  } // eslint-disable-next-line

  function text(text) {
    if (typeof text === "undefined") {
      if (this[0]) {
        return this[0].textContent.trim();
      }

      return null;
    }

    for (var i = 0; i < this.length; i += 1) {
      this[i].textContent = text;
    }

    return this;
  }

  function is(selector) {
    var el = this[0];
    var compareWith;
    var i;

    if (!el || typeof selector === "undefined") {
      return false;
    }

    if (typeof selector === "string") {
      if (el.matches) {
        return el.matches(selector);
      } else if (el.webkitMatchesSelector) {
        return el.webkitMatchesSelector(selector);
      } else if (el.msMatchesSelector) {
        return el.msMatchesSelector(selector);
      }

      compareWith = $(selector);

      for (i = 0; i < compareWith.length; i += 1) {
        if (compareWith[i] === el) {
          return true;
        }
      }

      return false;
    } else if (selector === doc) {
      return el === doc;
    } else if (selector === win) {
      return el === win;
    }

    if (selector.nodeType || selector instanceof Dom7) {
      compareWith = selector.nodeType ? [selector] : selector;

      for (i = 0; i < compareWith.length; i += 1) {
        if (compareWith[i] === el) {
          return true;
        }
      }

      return false;
    }

    return false;
  }

  function index() {
    var child = this[0];
    var i;

    if (child) {
      i = 0; // eslint-disable-next-line

      while ((child = child.previousSibling) !== null) {
        if (child.nodeType === 1) {
          i += 1;
        }
      }

      return i;
    }

    return undefined;
  } // eslint-disable-next-line

  function eq(index) {
    if (typeof index === "undefined") {
      return this;
    }

    var length = this.length;
    var returnIndex;

    if (index > length - 1) {
      return new Dom7([]);
    }

    if (index < 0) {
      returnIndex = length + index;

      if (returnIndex < 0) {
        return new Dom7([]);
      }

      return new Dom7([this[returnIndex]]);
    }

    return new Dom7([this[index]]);
  }

  function append() {
    var args = [],
      len = arguments.length;

    while (len--) {
      args[len] = arguments[len];
    }

    var newChild;

    for (var k = 0; k < args.length; k += 1) {
      newChild = args[k];

      for (var i = 0; i < this.length; i += 1) {
        if (typeof newChild === "string") {
          var tempDiv = doc.createElement("div");
          tempDiv.innerHTML = newChild;

          while (tempDiv.firstChild) {
            this[i].appendChild(tempDiv.firstChild);
          }
        } else if (newChild instanceof Dom7) {
          for (var j = 0; j < newChild.length; j += 1) {
            this[i].appendChild(newChild[j]);
          }
        } else {
          this[i].appendChild(newChild);
        }
      }
    }

    return this;
  }

  function prepend(newChild) {
    var i;
    var j;

    for (i = 0; i < this.length; i += 1) {
      if (typeof newChild === "string") {
        var tempDiv = doc.createElement("div");
        tempDiv.innerHTML = newChild;

        for (j = tempDiv.childNodes.length - 1; j >= 0; j -= 1) {
          this[i].insertBefore(tempDiv.childNodes[j], this[i].childNodes[0]);
        }
      } else if (newChild instanceof Dom7) {
        for (j = 0; j < newChild.length; j += 1) {
          this[i].insertBefore(newChild[j], this[i].childNodes[0]);
        }
      } else {
        this[i].insertBefore(newChild, this[i].childNodes[0]);
      }
    }

    return this;
  }

  function next(selector) {
    if (this.length > 0) {
      if (selector) {
        if (
          this[0].nextElementSibling &&
          $(this[0].nextElementSibling).is(selector)
        ) {
          return new Dom7([this[0].nextElementSibling]);
        }

        return new Dom7([]);
      }

      if (this[0].nextElementSibling) {
        return new Dom7([this[0].nextElementSibling]);
      }

      return new Dom7([]);
    }

    return new Dom7([]);
  }

  function nextAll(selector) {
    var nextEls = [];
    var el = this[0];

    if (!el) {
      return new Dom7([]);
    }

    while (el.nextElementSibling) {
      var next = el.nextElementSibling; // eslint-disable-line

      if (selector) {
        if ($(next).is(selector)) {
          nextEls.push(next);
        }
      } else {
        nextEls.push(next);
      }

      el = next;
    }

    return new Dom7(nextEls);
  }

  function prev(selector) {
    if (this.length > 0) {
      var el = this[0];

      if (selector) {
        if (
          el.previousElementSibling &&
          $(el.previousElementSibling).is(selector)
        ) {
          return new Dom7([el.previousElementSibling]);
        }

        return new Dom7([]);
      }

      if (el.previousElementSibling) {
        return new Dom7([el.previousElementSibling]);
      }

      return new Dom7([]);
    }

    return new Dom7([]);
  }

  function prevAll(selector) {
    var prevEls = [];
    var el = this[0];

    if (!el) {
      return new Dom7([]);
    }

    while (el.previousElementSibling) {
      var prev = el.previousElementSibling; // eslint-disable-line

      if (selector) {
        if ($(prev).is(selector)) {
          prevEls.push(prev);
        }
      } else {
        prevEls.push(prev);
      }

      el = prev;
    }

    return new Dom7(prevEls);
  }

  function parent(selector) {
    var parents = []; // eslint-disable-line

    for (var i = 0; i < this.length; i += 1) {
      if (this[i].parentNode !== null) {
        if (selector) {
          if ($(this[i].parentNode).is(selector)) {
            parents.push(this[i].parentNode);
          }
        } else {
          parents.push(this[i].parentNode);
        }
      }
    }

    return $(unique(parents));
  }

  function parents(selector) {
    var parents = []; // eslint-disable-line

    for (var i = 0; i < this.length; i += 1) {
      var parent = this[i].parentNode; // eslint-disable-line

      while (parent) {
        if (selector) {
          if ($(parent).is(selector)) {
            parents.push(parent);
          }
        } else {
          parents.push(parent);
        }

        parent = parent.parentNode;
      }
    }

    return $(unique(parents));
  }

  function closest(selector) {
    var closest = this; // eslint-disable-line

    if (typeof selector === "undefined") {
      return new Dom7([]);
    }

    if (!closest.is(selector)) {
      closest = closest.parents(selector).eq(0);
    }

    return closest;
  }

  function find(selector) {
    var foundElements = [];

    for (var i = 0; i < this.length; i += 1) {
      var found = this[i].querySelectorAll(selector);

      for (var j = 0; j < found.length; j += 1) {
        foundElements.push(found[j]);
      }
    }

    return new Dom7(foundElements);
  }

  function children(selector) {
    var children = []; // eslint-disable-line

    for (var i = 0; i < this.length; i += 1) {
      var childNodes = this[i].childNodes;

      for (var j = 0; j < childNodes.length; j += 1) {
        if (!selector) {
          if (childNodes[j].nodeType === 1) {
            children.push(childNodes[j]);
          }
        } else if (
          childNodes[j].nodeType === 1 &&
          $(childNodes[j]).is(selector)
        ) {
          children.push(childNodes[j]);
        }
      }
    }

    return new Dom7(unique(children));
  }

  function remove() {
    for (var i = 0; i < this.length; i += 1) {
      if (this[i].parentNode) {
        this[i].parentNode.removeChild(this[i]);
      }
    }

    return this;
  }

  function add() {
    var args = [],
      len = arguments.length;

    while (len--) {
      args[len] = arguments[len];
    }

    var dom = this;
    var i;
    var j;

    for (i = 0; i < args.length; i += 1) {
      var toAdd = $(args[i]);

      for (j = 0; j < toAdd.length; j += 1) {
        dom[dom.length] = toAdd[j];
        dom.length += 1;
      }
    }

    return dom;
  }

  var Methods = {
    addClass: addClass,
    removeClass: removeClass,
    hasClass: hasClass,
    toggleClass: toggleClass,
    attr: attr,
    removeAttr: removeAttr,
    data: data,
    transform: transform,
    transition: transition,
    on: on,
    off: off,
    trigger: trigger,
    transitionEnd: transitionEnd,
    outerWidth: outerWidth,
    outerHeight: outerHeight,
    offset: offset,
    css: css,
    each: each,
    html: html,
    text: text,
    is: is,
    index: index,
    eq: eq,
    append: append,
    prepend: prepend,
    next: next,
    nextAll: nextAll,
    prev: prev,
    prevAll: prevAll,
    parent: parent,
    parents: parents,
    closest: closest,
    find: find,
    children: children,
    remove: remove,
    add: add,
    styles: styles,
  };
  Object.keys(Methods).forEach(function (methodName) {
    $.fn[methodName] = $.fn[methodName] || Methods[methodName];
  });
  var Utils = {
    deleteProps: function deleteProps(obj) {
      var object = obj;
      Object.keys(object).forEach(function (key) {
        try {
          object[key] = null;
        } catch (e) {
          // no getter for object
        }

        try {
          delete object[key];
        } catch (e) {
          // something got wrong
        }
      });
    },
    nextTick: function nextTick(callback, delay) {
      if (delay === void 0) delay = 0;
      return setTimeout(callback, delay);
    },
    now: function now() {
      return Date.now();
    },
    getTranslate: function getTranslate(el, axis) {
      if (axis === void 0) axis = "x";
      var matrix;
      var curTransform;
      var transformMatrix;
      var curStyle = win.getComputedStyle(el, null);

      if (win.WebKitCSSMatrix) {
        curTransform = curStyle.transform || curStyle.webkitTransform;

        if (curTransform.split(",").length > 6) {
          curTransform = curTransform
            .split(", ")
            .map(function (a) {
              return a.replace(",", ".");
            })
            .join(", ");
        } // Some old versions of Webkit choke when 'none' is passed; pass
        // empty string instead in this case

        transformMatrix = new win.WebKitCSSMatrix(
          curTransform === "none" ? "" : curTransform
        );
      } else {
        transformMatrix =
          curStyle.MozTransform ||
          curStyle.OTransform ||
          curStyle.MsTransform ||
          curStyle.msTransform ||
          curStyle.transform ||
          curStyle
            .getPropertyValue("transform")
            .replace("translate(", "matrix(1, 0, 0, 1,");
        matrix = transformMatrix.toString().split(",");
      }

      if (axis === "x") {
        // Latest Chrome and webkits Fix
        if (win.WebKitCSSMatrix) {
          curTransform = transformMatrix.m41;
        } // Crazy IE10 Matrix
        else if (matrix.length === 16) {
          curTransform = parseFloat(matrix[12]);
        } // Normal Browsers
        else {
          curTransform = parseFloat(matrix[4]);
        }
      }

      if (axis === "y") {
        // Latest Chrome and webkits Fix
        if (win.WebKitCSSMatrix) {
          curTransform = transformMatrix.m42;
        } // Crazy IE10 Matrix
        else if (matrix.length === 16) {
          curTransform = parseFloat(matrix[13]);
        } // Normal Browsers
        else {
          curTransform = parseFloat(matrix[5]);
        }
      }

      return curTransform || 0;
    },
    parseUrlQuery: function parseUrlQuery(url) {
      var query = {};
      var urlToParse = url || win.location.href;
      var i;
      var params;
      var param;
      var length;

      if (typeof urlToParse === "string" && urlToParse.length) {
        urlToParse =
          urlToParse.indexOf("?") > -1 ? urlToParse.replace(/\S*\?/, "") : "";
        params = urlToParse.split("&").filter(function (paramsPart) {
          return paramsPart !== "";
        });
        length = params.length;

        for (i = 0; i < length; i += 1) {
          param = params[i].replace(/#\S+/g, "").split("=");
          query[decodeURIComponent(param[0])] =
            typeof param[1] === "undefined"
              ? undefined
              : decodeURIComponent(param[1]) || "";
        }
      }

      return query;
    },
    isObject: function isObject(o) {
      return (
        _typeof(o) === "object" &&
        o !== null &&
        o.constructor &&
        o.constructor === Object
      );
    },
    extend: function extend() {
      var args = [],
        len$1 = arguments.length;

      while (len$1--) {
        args[len$1] = arguments[len$1];
      }

      var to = Object(args[0]);

      for (var i = 1; i < args.length; i += 1) {
        var nextSource = args[i];

        if (nextSource !== undefined && nextSource !== null) {
          var keysArray = Object.keys(Object(nextSource));

          for (
            var nextIndex = 0, len = keysArray.length;
            nextIndex < len;
            nextIndex += 1
          ) {
            var nextKey = keysArray[nextIndex];
            var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);

            if (desc !== undefined && desc.enumerable) {
              if (
                Utils.isObject(to[nextKey]) &&
                Utils.isObject(nextSource[nextKey])
              ) {
                Utils.extend(to[nextKey], nextSource[nextKey]);
              } else if (
                !Utils.isObject(to[nextKey]) &&
                Utils.isObject(nextSource[nextKey])
              ) {
                to[nextKey] = {};
                Utils.extend(to[nextKey], nextSource[nextKey]);
              } else {
                to[nextKey] = nextSource[nextKey];
              }
            }
          }
        }
      }

      return to;
    },
  };

  var Support = (function Support() {
    var testDiv = doc.createElement("div");
    return {
      touch:
        (win.Modernizr && win.Modernizr.touch === true) ||
        (function checkTouch() {
          return !!(
            win.navigator.maxTouchPoints > 0 ||
            "ontouchstart" in win ||
            (win.DocumentTouch && doc instanceof win.DocumentTouch)
          );
        })(),
      pointerEvents: !!(
        win.navigator.pointerEnabled ||
        win.PointerEvent ||
        ("maxTouchPoints" in win.navigator && win.navigator.maxTouchPoints > 0)
      ),
      prefixedPointerEvents: !!win.navigator.msPointerEnabled,
      transition: (function checkTransition() {
        var style = testDiv.style;
        return (
          "transition" in style ||
          "webkitTransition" in style ||
          "MozTransition" in style
        );
      })(),
      transforms3d:
        (win.Modernizr && win.Modernizr.csstransforms3d === true) ||
        (function checkTransforms3d() {
          var style = testDiv.style;
          return (
            "webkitPerspective" in style ||
            "MozPerspective" in style ||
            "OPerspective" in style ||
            "MsPerspective" in style ||
            "perspective" in style
          );
        })(),
      flexbox: (function checkFlexbox() {
        var style = testDiv.style;
        var styles = "alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(
          " "
        );

        for (var i = 0; i < styles.length; i += 1) {
          if (styles[i] in style) {
            return true;
          }
        }

        return false;
      })(),
      observer: (function checkObserver() {
        return "MutationObserver" in win || "WebkitMutationObserver" in win;
      })(),
      passiveListener: (function checkPassiveListener() {
        var supportsPassive = false;

        try {
          var opts = Object.defineProperty({}, "passive", {
            // eslint-disable-next-line
            get: function get() {
              supportsPassive = true;
            },
          });
          win.addEventListener("testPassiveListener", null, opts);
        } catch (e) {
          // No support
        }

        return supportsPassive;
      })(),
      gestures: (function checkGestures() {
        return "ongesturestart" in win;
      })(),
    };
  })();

  var Browser = (function Browser() {
    function isSafari() {
      var ua = win.navigator.userAgent.toLowerCase();
      return (
        ua.indexOf("safari") >= 0 &&
        ua.indexOf("chrome") < 0 &&
        ua.indexOf("android") < 0
      );
    }

    return {
      isIE:
        !!win.navigator.userAgent.match(/Trident/g) ||
        !!win.navigator.userAgent.match(/MSIE/g),
      isEdge: !!win.navigator.userAgent.match(/Edge/g),
      isSafari: isSafari(),
      isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(
        win.navigator.userAgent
      ),
    };
  })();

  var SwiperClass = function SwiperClass(params) {
    if (params === void 0) params = {};
    var self = this;
    self.params = params; // Events

    self.eventsListeners = {};

    if (self.params && self.params.on) {
      Object.keys(self.params.on).forEach(function (eventName) {
        self.on(eventName, self.params.on[eventName]);
      });
    }
  };

  var staticAccessors = {
    components: {
      configurable: true,
    },
  };

  SwiperClass.prototype.on = function on(events, handler, priority) {
    var self = this;

    if (typeof handler !== "function") {
      return self;
    }

    var method = priority ? "unshift" : "push";
    events.split(" ").forEach(function (event) {
      if (!self.eventsListeners[event]) {
        self.eventsListeners[event] = [];
      }

      self.eventsListeners[event][method](handler);
    });
    return self;
  };

  SwiperClass.prototype.once = function once(events, handler, priority) {
    var self = this;

    if (typeof handler !== "function") {
      return self;
    }

    function onceHandler() {
      var args = [],
        len = arguments.length;

      while (len--) {
        args[len] = arguments[len];
      }

      handler.apply(self, args);
      self.off(events, onceHandler);

      if (onceHandler.f7proxy) {
        delete onceHandler.f7proxy;
      }
    }

    onceHandler.f7proxy = handler;
    return self.on(events, onceHandler, priority);
  };

  SwiperClass.prototype.off = function off(events, handler) {
    var self = this;

    if (!self.eventsListeners) {
      return self;
    }

    events.split(" ").forEach(function (event) {
      if (typeof handler === "undefined") {
        self.eventsListeners[event] = [];
      } else if (
        self.eventsListeners[event] &&
        self.eventsListeners[event].length
      ) {
        self.eventsListeners[event].forEach(function (eventHandler, index) {
          if (
            eventHandler === handler ||
            (eventHandler.f7proxy && eventHandler.f7proxy === handler)
          ) {
            self.eventsListeners[event].splice(index, 1);
          }
        });
      }
    });
    return self;
  };

  SwiperClass.prototype.emit = function emit() {
    var args = [],
      len = arguments.length;

    while (len--) {
      args[len] = arguments[len];
    }

    var self = this;

    if (!self.eventsListeners) {
      return self;
    }

    var events;
    var data;
    var context;

    if (typeof args[0] === "string" || Array.isArray(args[0])) {
      events = args[0];
      data = args.slice(1, args.length);
      context = self;
    } else {
      events = args[0].events;
      data = args[0].data;
      context = args[0].context || self;
    }

    var eventsArray = Array.isArray(events) ? events : events.split(" ");
    eventsArray.forEach(function (event) {
      if (self.eventsListeners && self.eventsListeners[event]) {
        var handlers = [];
        self.eventsListeners[event].forEach(function (eventHandler) {
          handlers.push(eventHandler);
        });
        handlers.forEach(function (eventHandler) {
          eventHandler.apply(context, data);
        });
      }
    });
    return self;
  };

  SwiperClass.prototype.useModulesParams = function useModulesParams(
    instanceParams
  ) {
    var instance = this;

    if (!instance.modules) {
      return;
    }

    Object.keys(instance.modules).forEach(function (moduleName) {
      var module = instance.modules[moduleName]; // Extend params

      if (module.params) {
        Utils.extend(instanceParams, module.params);
      }
    });
  };

  SwiperClass.prototype.useModules = function useModules(modulesParams) {
    if (modulesParams === void 0) modulesParams = {};
    var instance = this;

    if (!instance.modules) {
      return;
    }

    Object.keys(instance.modules).forEach(function (moduleName) {
      var module = instance.modules[moduleName];
      var moduleParams = modulesParams[moduleName] || {}; // Extend instance methods and props

      if (module.instance) {
        Object.keys(module.instance).forEach(function (modulePropName) {
          var moduleProp = module.instance[modulePropName];

          if (typeof moduleProp === "function") {
            instance[modulePropName] = moduleProp.bind(instance);
          } else {
            instance[modulePropName] = moduleProp;
          }
        });
      } // Add event listeners

      if (module.on && instance.on) {
        Object.keys(module.on).forEach(function (moduleEventName) {
          instance.on(moduleEventName, module.on[moduleEventName]);
        });
      } // Module create callback

      if (module.create) {
        module.create.bind(instance)(moduleParams);
      }
    });
  };

  staticAccessors.components.set = function (components) {
    var Class = this;

    if (!Class.use) {
      return;
    }

    Class.use(components);
  };

  SwiperClass.installModule = function installModule(module) {
    var params = [],
      len = arguments.length - 1;

    while (len-- > 0) {
      params[len] = arguments[len + 1];
    }

    var Class = this;

    if (!Class.prototype.modules) {
      Class.prototype.modules = {};
    }

    var name =
      module.name ||
      Object.keys(Class.prototype.modules).length + "_" + Utils.now();
    Class.prototype.modules[name] = module; // Prototype

    if (module.proto) {
      Object.keys(module.proto).forEach(function (key) {
        Class.prototype[key] = module.proto[key];
      });
    } // Class

    if (module["static"]) {
      Object.keys(module["static"]).forEach(function (key) {
        Class[key] = module["static"][key];
      });
    } // Callback

    if (module.install) {
      module.install.apply(Class, params);
    }

    return Class;
  };

  SwiperClass.use = function use(module) {
    var params = [],
      len = arguments.length - 1;

    while (len-- > 0) {
      params[len] = arguments[len + 1];
    }

    var Class = this;

    if (Array.isArray(module)) {
      module.forEach(function (m) {
        return Class.installModule(m);
      });
      return Class;
    }

    return Class.installModule.apply(Class, [module].concat(params));
  };

  Object.defineProperties(SwiperClass, staticAccessors);

  function updateSize() {
    var swiper = this;
    var width;
    var height;
    var $el = swiper.$el;

    if (typeof swiper.params.width !== "undefined") {
      width = swiper.params.width;
    } else {
      width = $el[0].clientWidth;
    }

    if (typeof swiper.params.height !== "undefined") {
      height = swiper.params.height;
    } else {
      height = $el[0].clientHeight;
    }

    if (
      (width === 0 && swiper.isHorizontal()) ||
      (height === 0 && swiper.isVertical())
    ) {
      return;
    } // Subtract paddings

    width =
      width -
      parseInt($el.css("padding-left"), 10) -
      parseInt($el.css("padding-right"), 10);
    height =
      height -
      parseInt($el.css("padding-top"), 10) -
      parseInt($el.css("padding-bottom"), 10);
    Utils.extend(swiper, {
      width: width,
      height: height,
      size: swiper.isHorizontal() ? width : height,
    });
  }

  function updateSlides() {
    var swiper = this;
    var params = swiper.params;
    var $wrapperEl = swiper.$wrapperEl;
    var swiperSize = swiper.size;
    var rtl = swiper.rtlTranslate;
    var wrongRTL = swiper.wrongRTL;
    var isVirtual = swiper.virtual && params.virtual.enabled;
    var previousSlidesLength = isVirtual
      ? swiper.virtual.slides.length
      : swiper.slides.length;
    var slides = $wrapperEl.children("." + swiper.params.slideClass);
    var slidesLength = isVirtual ? swiper.virtual.slides.length : slides.length;
    var snapGrid = [];
    var slidesGrid = [];
    var slidesSizesGrid = [];
    var offsetBefore = params.slidesOffsetBefore;

    if (typeof offsetBefore === "function") {
      offsetBefore = params.slidesOffsetBefore.call(swiper);
    }

    var offsetAfter = params.slidesOffsetAfter;

    if (typeof offsetAfter === "function") {
      offsetAfter = params.slidesOffsetAfter.call(swiper);
    }

    var previousSnapGridLength = swiper.snapGrid.length;
    var previousSlidesGridLength = swiper.snapGrid.length;
    var spaceBetween = params.spaceBetween;
    var slidePosition = -offsetBefore;
    var prevSlideSize = 0;
    var index = 0;

    if (typeof swiperSize === "undefined") {
      return;
    }

    if (typeof spaceBetween === "string" && spaceBetween.indexOf("%") >= 0) {
      spaceBetween =
        (parseFloat(spaceBetween.replace("%", "")) / 100) * swiperSize;
    }

    swiper.virtualSize = -spaceBetween; // reset margins

    if (rtl) {
      slides.css({
        marginLeft: "",
        marginTop: "",
      });
    } else {
      slides.css({
        marginRight: "",
        marginBottom: "",
      });
    }

    var slidesNumberEvenToRows;

    if (params.slidesPerColumn > 1) {
      if (
        Math.floor(slidesLength / params.slidesPerColumn) ===
        slidesLength / swiper.params.slidesPerColumn
      ) {
        slidesNumberEvenToRows = slidesLength;
      } else {
        slidesNumberEvenToRows =
          Math.ceil(slidesLength / params.slidesPerColumn) *
          params.slidesPerColumn;
      }

      if (
        params.slidesPerView !== "auto" &&
        params.slidesPerColumnFill === "row"
      ) {
        slidesNumberEvenToRows = Math.max(
          slidesNumberEvenToRows,
          params.slidesPerView * params.slidesPerColumn
        );
      }
    } // Calc slides

    var slideSize;
    var slidesPerColumn = params.slidesPerColumn;
    var slidesPerRow = slidesNumberEvenToRows / slidesPerColumn;
    var numFullColumns = Math.floor(slidesLength / params.slidesPerColumn);

    for (var i = 0; i < slidesLength; i += 1) {
      slideSize = 0;
      var slide = slides.eq(i);

      if (params.slidesPerColumn > 1) {
        // Set slides order
        var newSlideOrderIndex = void 0;
        var column = void 0;
        var row = void 0;

        if (
          params.slidesPerColumnFill === "column" ||
          (params.slidesPerColumnFill === "row" && params.slidesPerGroup > 1)
        ) {
          if (params.slidesPerColumnFill === "column") {
            column = Math.floor(i / slidesPerColumn);
            row = i - column * slidesPerColumn;

            if (
              column > numFullColumns ||
              (column === numFullColumns && row === slidesPerColumn - 1)
            ) {
              row += 1;

              if (row >= slidesPerColumn) {
                row = 0;
                column += 1;
              }
            }
          } else {
            var groupIndex = Math.floor(i / params.slidesPerGroup);
            row =
              Math.floor(i / params.slidesPerView) -
              groupIndex * params.slidesPerColumn;
            column =
              i -
              row * params.slidesPerView -
              groupIndex * params.slidesPerView;
          }

          newSlideOrderIndex =
            column + (row * slidesNumberEvenToRows) / slidesPerColumn;
          slide.css({
            "-webkit-box-ordinal-group": newSlideOrderIndex,
            "-moz-box-ordinal-group": newSlideOrderIndex,
            "-ms-flex-order": newSlideOrderIndex,
            "-webkit-order": newSlideOrderIndex,
            order: newSlideOrderIndex,
          });
        } else {
          row = Math.floor(i / slidesPerRow);
          column = i - row * slidesPerRow;
        }

        slide
          .css(
            "margin-" + (swiper.isHorizontal() ? "top" : "left"),
            row !== 0 && params.spaceBetween && params.spaceBetween + "px"
          )
          .attr("data-swiper-column", column)
          .attr("data-swiper-row", row);
      }

      if (slide.css("display") === "none") {
        continue;
      } // eslint-disable-line

      if (params.slidesPerView === "auto") {
        var slideStyles = win.getComputedStyle(slide[0], null);
        var currentTransform = slide[0].style.transform;
        var currentWebKitTransform = slide[0].style.webkitTransform;

        if (currentTransform) {
          slide[0].style.transform = "none";
        }

        if (currentWebKitTransform) {
          slide[0].style.webkitTransform = "none";
        }

        if (params.roundLengths) {
          slideSize = swiper.isHorizontal()
            ? slide.outerWidth(true)
            : slide.outerHeight(true);
        } else {
          // eslint-disable-next-line
          if (swiper.isHorizontal()) {
            var width = parseFloat(slideStyles.getPropertyValue("width"));
            var paddingLeft = parseFloat(
              slideStyles.getPropertyValue("padding-left")
            );
            var paddingRight = parseFloat(
              slideStyles.getPropertyValue("padding-right")
            );
            var marginLeft = parseFloat(
              slideStyles.getPropertyValue("margin-left")
            );
            var marginRight = parseFloat(
              slideStyles.getPropertyValue("margin-right")
            );
            var boxSizing = slideStyles.getPropertyValue("box-sizing");

            if (boxSizing && boxSizing === "border-box" && !Browser.isIE) {
              slideSize = width + marginLeft + marginRight;
            } else {
              slideSize =
                width + paddingLeft + paddingRight + marginLeft + marginRight;
            }
          } else {
            var height = parseFloat(slideStyles.getPropertyValue("height"));
            var paddingTop = parseFloat(
              slideStyles.getPropertyValue("padding-top")
            );
            var paddingBottom = parseFloat(
              slideStyles.getPropertyValue("padding-bottom")
            );
            var marginTop = parseFloat(
              slideStyles.getPropertyValue("margin-top")
            );
            var marginBottom = parseFloat(
              slideStyles.getPropertyValue("margin-bottom")
            );
            var boxSizing$1 = slideStyles.getPropertyValue("box-sizing");

            if (boxSizing$1 && boxSizing$1 === "border-box" && !Browser.isIE) {
              slideSize = height + marginTop + marginBottom;
            } else {
              slideSize =
                height + paddingTop + paddingBottom + marginTop + marginBottom;
            }
          }
        }

        if (currentTransform) {
          slide[0].style.transform = currentTransform;
        }

        if (currentWebKitTransform) {
          slide[0].style.webkitTransform = currentWebKitTransform;
        }

        if (params.roundLengths) {
          slideSize = Math.floor(slideSize);
        }
      } else {
        slideSize =
          (swiperSize - (params.slidesPerView - 1) * spaceBetween) /
          params.slidesPerView;

        if (params.roundLengths) {
          slideSize = Math.floor(slideSize);
        }

        if (slides[i]) {
          if (swiper.isHorizontal()) {
            slides[i].style.width = slideSize + "px";
          } else {
            slides[i].style.height = slideSize + "px";
          }
        }
      }

      if (slides[i]) {
        slides[i].swiperSlideSize = slideSize;
      }

      slidesSizesGrid.push(slideSize);

      if (params.centeredSlides) {
        slidePosition =
          slidePosition + slideSize / 2 + prevSlideSize / 2 + spaceBetween;

        if (prevSlideSize === 0 && i !== 0) {
          slidePosition = slidePosition - swiperSize / 2 - spaceBetween;
        }

        if (i === 0) {
          slidePosition = slidePosition - swiperSize / 2 - spaceBetween;
        }

        if (Math.abs(slidePosition) < 1 / 1000) {
          slidePosition = 0;
        }

        if (params.roundLengths) {
          slidePosition = Math.floor(slidePosition);
        }

        if (index % params.slidesPerGroup === 0) {
          snapGrid.push(slidePosition);
        }

        slidesGrid.push(slidePosition);
      } else {
        if (params.roundLengths) {
          slidePosition = Math.floor(slidePosition);
        }

        if (index % params.slidesPerGroup === 0) {
          snapGrid.push(slidePosition);
        }

        slidesGrid.push(slidePosition);
        slidePosition = slidePosition + slideSize + spaceBetween;
      }

      swiper.virtualSize += slideSize + spaceBetween;
      prevSlideSize = slideSize;
      index += 1;
    }

    swiper.virtualSize = Math.max(swiper.virtualSize, swiperSize) + offsetAfter;
    var newSlidesGrid;

    if (
      rtl &&
      wrongRTL &&
      (params.effect === "slide" || params.effect === "coverflow")
    ) {
      $wrapperEl.css({
        width: swiper.virtualSize + params.spaceBetween + "px",
      });
    }

    if (!Support.flexbox || params.setWrapperSize) {
      if (swiper.isHorizontal()) {
        $wrapperEl.css({
          width: swiper.virtualSize + params.spaceBetween + "px",
        });
      } else {
        $wrapperEl.css({
          height: swiper.virtualSize + params.spaceBetween + "px",
        });
      }
    }

    if (params.slidesPerColumn > 1) {
      swiper.virtualSize =
        (slideSize + params.spaceBetween) * slidesNumberEvenToRows;
      swiper.virtualSize =
        Math.ceil(swiper.virtualSize / params.slidesPerColumn) -
        params.spaceBetween;

      if (swiper.isHorizontal()) {
        $wrapperEl.css({
          width: swiper.virtualSize + params.spaceBetween + "px",
        });
      } else {
        $wrapperEl.css({
          height: swiper.virtualSize + params.spaceBetween + "px",
        });
      }

      if (params.centeredSlides) {
        newSlidesGrid = [];

        for (var i$1 = 0; i$1 < snapGrid.length; i$1 += 1) {
          var slidesGridItem = snapGrid[i$1];

          if (params.roundLengths) {
            slidesGridItem = Math.floor(slidesGridItem);
          }

          if (snapGrid[i$1] < swiper.virtualSize + snapGrid[0]) {
            newSlidesGrid.push(slidesGridItem);
          }
        }

        snapGrid = newSlidesGrid;
      }
    } // Remove last grid elements depending on width

    if (!params.centeredSlides) {
      newSlidesGrid = [];

      for (var i$2 = 0; i$2 < snapGrid.length; i$2 += 1) {
        var slidesGridItem$1 = snapGrid[i$2];

        if (params.roundLengths) {
          slidesGridItem$1 = Math.floor(slidesGridItem$1);
        }

        if (snapGrid[i$2] <= swiper.virtualSize - swiperSize) {
          newSlidesGrid.push(slidesGridItem$1);
        }
      }

      snapGrid = newSlidesGrid;

      if (
        Math.floor(swiper.virtualSize - swiperSize) -
          Math.floor(snapGrid[snapGrid.length - 1]) >
        1
      ) {
        snapGrid.push(swiper.virtualSize - swiperSize);
      }
    }

    if (snapGrid.length === 0) {
      snapGrid = [0];
    }

    if (params.spaceBetween !== 0) {
      if (swiper.isHorizontal()) {
        if (rtl) {
          slides.css({
            marginLeft: spaceBetween + "px",
          });
        } else {
          slides.css({
            marginRight: spaceBetween + "px",
          });
        }
      } else {
        slides.css({
          marginBottom: spaceBetween + "px",
        });
      }
    }

    if (params.centerInsufficientSlides) {
      var allSlidesSize = 0;
      slidesSizesGrid.forEach(function (slideSizeValue) {
        allSlidesSize +=
          slideSizeValue + (params.spaceBetween ? params.spaceBetween : 0);
      });
      allSlidesSize -= params.spaceBetween;

      if (allSlidesSize < swiperSize) {
        var allSlidesOffset = (swiperSize - allSlidesSize) / 2;
        snapGrid.forEach(function (snap, snapIndex) {
          snapGrid[snapIndex] = snap - allSlidesOffset;
        });
        slidesGrid.forEach(function (snap, snapIndex) {
          slidesGrid[snapIndex] = snap + allSlidesOffset;
        });
      }
    }

    Utils.extend(swiper, {
      slides: slides,
      snapGrid: snapGrid,
      slidesGrid: slidesGrid,
      slidesSizesGrid: slidesSizesGrid,
    });

    if (slidesLength !== previousSlidesLength) {
      swiper.emit("slidesLengthChange");
    }

    if (snapGrid.length !== previousSnapGridLength) {
      if (swiper.params.watchOverflow) {
        swiper.checkOverflow();
      }

      swiper.emit("snapGridLengthChange");
    }

    if (slidesGrid.length !== previousSlidesGridLength) {
      swiper.emit("slidesGridLengthChange");
    }

    if (params.watchSlidesProgress || params.watchSlidesVisibility) {
      swiper.updateSlidesOffset();
    }
  }

  function updateAutoHeight(speed) {
    var swiper = this;
    var activeSlides = [];
    var newHeight = 0;
    var i;

    if (typeof speed === "number") {
      swiper.setTransition(speed);
    } else if (speed === true) {
      swiper.setTransition(swiper.params.speed);
    } // Find slides currently in view

    if (
      swiper.params.slidesPerView !== "auto" &&
      swiper.params.slidesPerView > 1
    ) {
      for (i = 0; i < Math.ceil(swiper.params.slidesPerView); i += 1) {
        var index = swiper.activeIndex + i;

        if (index > swiper.slides.length) {
          break;
        }

        activeSlides.push(swiper.slides.eq(index)[0]);
      }
    } else {
      activeSlides.push(swiper.slides.eq(swiper.activeIndex)[0]);
    } // Find new height from highest slide in view

    for (i = 0; i < activeSlides.length; i += 1) {
      if (typeof activeSlides[i] !== "undefined") {
        var height = activeSlides[i].offsetHeight;
        newHeight = height > newHeight ? height : newHeight;
      }
    } // Update Height

    if (newHeight) {
      swiper.$wrapperEl.css("height", newHeight + "px");
    }
  }

  function updateSlidesOffset() {
    var swiper = this;
    var slides = swiper.slides;

    for (var i = 0; i < slides.length; i += 1) {
      slides[i].swiperSlideOffset = swiper.isHorizontal()
        ? slides[i].offsetLeft
        : slides[i].offsetTop;
    }
  }

  function updateSlidesProgress(translate) {
    var swiper = this;
    var params = swiper.params;

    if (typeof translate === "undefined") {
      // eslint-disable-next-line
      translate = (swiper && swiper.translate) || 0;
    }

    var slides = swiper.slides;
    var rtl = swiper.rtlTranslate;

    if (slides.length === 0) {
      return;
    }

    if (typeof slides[0].swiperSlideOffset === "undefined") {
      swiper.updateSlidesOffset();
    }

    var offsetCenter = -translate;

    if (rtl) {
      offsetCenter = translate;
    } // Visible Slides

    slides.removeClass(params.slideVisibleClass);
    swiper.visibleSlidesIndexes = [];
    swiper.visibleSlides = [];

    for (var i = 0; i < slides.length; i += 1) {
      var slide = slides[i];
      var slideProgress =
        (offsetCenter +
          (params.centeredSlides ? swiper.minTranslate() : 0) -
          slide.swiperSlideOffset) /
        (slide.swiperSlideSize + params.spaceBetween);

      if (params.watchSlidesVisibility) {
        var slideBefore = -(offsetCenter - slide.swiperSlideOffset);
        var slideAfter = slideBefore + swiper.slidesSizesGrid[i];
        var isVisible =
          (slideBefore >= 0 && slideBefore < swiper.size - 1) ||
          (slideAfter > 1 && slideAfter <= swiper.size) ||
          (slideBefore <= 0 && slideAfter >= swiper.size);

        if (isVisible) {
          swiper.visibleSlides.push(slide);
          swiper.visibleSlidesIndexes.push(i);
          slides.eq(i).addClass(params.slideVisibleClass);
        }
      }

      slide.progress = rtl ? -slideProgress : slideProgress;
    }

    swiper.visibleSlides = $(swiper.visibleSlides);
  }

  function updateProgress(translate) {
    var swiper = this;
    var params = swiper.params;

    if (typeof translate === "undefined") {
      var multiplier = swiper.rtlTranslate ? -1 : 1; // eslint-disable-next-line

      translate =
        (swiper && swiper.translate && swiper.translate * multiplier) || 0;
    }

    var translatesDiff = swiper.maxTranslate() - swiper.minTranslate();
    var progress = swiper.progress;
    var isBeginning = swiper.isBeginning;
    var isEnd = swiper.isEnd;
    var wasBeginning = isBeginning;
    var wasEnd = isEnd;

    if (translatesDiff === 0) {
      progress = 0;
      isBeginning = true;
      isEnd = true;
    } else {
      progress = (translate - swiper.minTranslate()) / translatesDiff;
      isBeginning = progress <= 0;
      isEnd = progress >= 1;
    }

    Utils.extend(swiper, {
      progress: progress,
      isBeginning: isBeginning,
      isEnd: isEnd,
    });

    if (params.watchSlidesProgress || params.watchSlidesVisibility) {
      swiper.updateSlidesProgress(translate);
    }

    if (isBeginning && !wasBeginning) {
      swiper.emit("reachBeginning toEdge");
    }

    if (isEnd && !wasEnd) {
      swiper.emit("reachEnd toEdge");
    }

    if ((wasBeginning && !isBeginning) || (wasEnd && !isEnd)) {
      swiper.emit("fromEdge");
    }

    swiper.emit("progress", progress);
  }

  function updateSlidesClasses() {
    var swiper = this;
    var slides = swiper.slides;
    var params = swiper.params;
    var $wrapperEl = swiper.$wrapperEl;
    var activeIndex = swiper.activeIndex;
    var realIndex = swiper.realIndex;
    var isVirtual = swiper.virtual && params.virtual.enabled;
    slides.removeClass(
      params.slideActiveClass +
        " " +
        params.slideNextClass +
        " " +
        params.slidePrevClass +
        " " +
        params.slideDuplicateActiveClass +
        " " +
        params.slideDuplicateNextClass +
        " " +
        params.slideDuplicatePrevClass
    );
    var activeSlide;

    if (isVirtual) {
      activeSlide = swiper.$wrapperEl.find(
        "." +
          params.slideClass +
          '[data-swiper-slide-index="' +
          activeIndex +
          '"]'
      );
    } else {
      activeSlide = slides.eq(activeIndex);
    } // Active classes

    activeSlide.addClass(params.slideActiveClass);

    if (params.loop) {
      // Duplicate to all looped slides
      if (activeSlide.hasClass(params.slideDuplicateClass)) {
        $wrapperEl
          .children(
            "." +
              params.slideClass +
              ":not(." +
              params.slideDuplicateClass +
              ')[data-swiper-slide-index="' +
              realIndex +
              '"]'
          )
          .addClass(params.slideDuplicateActiveClass);
      } else {
        $wrapperEl
          .children(
            "." +
              params.slideClass +
              "." +
              params.slideDuplicateClass +
              '[data-swiper-slide-index="' +
              realIndex +
              '"]'
          )
          .addClass(params.slideDuplicateActiveClass);
      }
    } // Next Slide

    var nextSlide = activeSlide
      .nextAll("." + params.slideClass)
      .eq(0)
      .addClass(params.slideNextClass);

    if (params.loop && nextSlide.length === 0) {
      nextSlide = slides.eq(0);
      nextSlide.addClass(params.slideNextClass);
    } // Prev Slide

    var prevSlide = activeSlide
      .prevAll("." + params.slideClass)
      .eq(0)
      .addClass(params.slidePrevClass);

    if (params.loop && prevSlide.length === 0) {
      prevSlide = slides.eq(-1);
      prevSlide.addClass(params.slidePrevClass);
    }

    if (params.loop) {
      // Duplicate to all looped slides
      if (nextSlide.hasClass(params.slideDuplicateClass)) {
        $wrapperEl
          .children(
            "." +
              params.slideClass +
              ":not(." +
              params.slideDuplicateClass +
              ')[data-swiper-slide-index="' +
              nextSlide.attr("data-swiper-slide-index") +
              '"]'
          )
          .addClass(params.slideDuplicateNextClass);
      } else {
        $wrapperEl
          .children(
            "." +
              params.slideClass +
              "." +
              params.slideDuplicateClass +
              '[data-swiper-slide-index="' +
              nextSlide.attr("data-swiper-slide-index") +
              '"]'
          )
          .addClass(params.slideDuplicateNextClass);
      }

      if (prevSlide.hasClass(params.slideDuplicateClass)) {
        $wrapperEl
          .children(
            "." +
              params.slideClass +
              ":not(." +
              params.slideDuplicateClass +
              ')[data-swiper-slide-index="' +
              prevSlide.attr("data-swiper-slide-index") +
              '"]'
          )
          .addClass(params.slideDuplicatePrevClass);
      } else {
        $wrapperEl
          .children(
            "." +
              params.slideClass +
              "." +
              params.slideDuplicateClass +
              '[data-swiper-slide-index="' +
              prevSlide.attr("data-swiper-slide-index") +
              '"]'
          )
          .addClass(params.slideDuplicatePrevClass);
      }
    }
  }

  function updateActiveIndex(newActiveIndex) {
    var swiper = this;
    var translate = swiper.rtlTranslate ? swiper.translate : -swiper.translate;
    var slidesGrid = swiper.slidesGrid;
    var snapGrid = swiper.snapGrid;
    var params = swiper.params;
    var previousIndex = swiper.activeIndex;
    var previousRealIndex = swiper.realIndex;
    var previousSnapIndex = swiper.snapIndex;
    var activeIndex = newActiveIndex;
    var snapIndex;

    if (typeof activeIndex === "undefined") {
      for (var i = 0; i < slidesGrid.length; i += 1) {
        if (typeof slidesGrid[i + 1] !== "undefined") {
          if (
            translate >= slidesGrid[i] &&
            translate <
              slidesGrid[i + 1] - (slidesGrid[i + 1] - slidesGrid[i]) / 2
          ) {
            activeIndex = i;
          } else if (
            translate >= slidesGrid[i] &&
            translate < slidesGrid[i + 1]
          ) {
            activeIndex = i + 1;
          }
        } else if (translate >= slidesGrid[i]) {
          activeIndex = i;
        }
      } // Normalize slideIndex

      if (params.normalizeSlideIndex) {
        if (activeIndex < 0 || typeof activeIndex === "undefined") {
          activeIndex = 0;
        }
      }
    }

    if (snapGrid.indexOf(translate) >= 0) {
      snapIndex = snapGrid.indexOf(translate);
    } else {
      snapIndex = Math.floor(activeIndex / params.slidesPerGroup);
    }

    if (snapIndex >= snapGrid.length) {
      snapIndex = snapGrid.length - 1;
    }

    if (activeIndex === previousIndex) {
      if (snapIndex !== previousSnapIndex) {
        swiper.snapIndex = snapIndex;
        swiper.emit("snapIndexChange");
      }

      return;
    } // Get real index

    var realIndex = parseInt(
      swiper.slides.eq(activeIndex).attr("data-swiper-slide-index") ||
        activeIndex,
      10
    );
    Utils.extend(swiper, {
      snapIndex: snapIndex,
      realIndex: realIndex,
      previousIndex: previousIndex,
      activeIndex: activeIndex,
    });
    swiper.emit("activeIndexChange");
    swiper.emit("snapIndexChange");

    if (previousRealIndex !== realIndex) {
      swiper.emit("realIndexChange");
    }

    if (swiper.initialized || swiper.runCallbacksOnInit) {
      swiper.emit("slideChange");
    }
  }

  function updateClickedSlide(e) {
    var swiper = this;
    var params = swiper.params;
    var slide = $(e.target).closest("." + params.slideClass)[0];
    var slideFound = false;

    if (slide) {
      for (var i = 0; i < swiper.slides.length; i += 1) {
        if (swiper.slides[i] === slide) {
          slideFound = true;
        }
      }
    }

    if (slide && slideFound) {
      swiper.clickedSlide = slide;

      if (swiper.virtual && swiper.params.virtual.enabled) {
        swiper.clickedIndex = parseInt(
          $(slide).attr("data-swiper-slide-index"),
          10
        );
      } else {
        swiper.clickedIndex = $(slide).index();
      }
    } else {
      swiper.clickedSlide = undefined;
      swiper.clickedIndex = undefined;
      return;
    }

    if (
      params.slideToClickedSlide &&
      swiper.clickedIndex !== undefined &&
      swiper.clickedIndex !== swiper.activeIndex
    ) {
      swiper.slideToClickedSlide();
    }
  }

  var update = {
    updateSize: updateSize,
    updateSlides: updateSlides,
    updateAutoHeight: updateAutoHeight,
    updateSlidesOffset: updateSlidesOffset,
    updateSlidesProgress: updateSlidesProgress,
    updateProgress: updateProgress,
    updateSlidesClasses: updateSlidesClasses,
    updateActiveIndex: updateActiveIndex,
    updateClickedSlide: updateClickedSlide,
  };

  function getTranslate(axis) {
    if (axis === void 0) axis = this.isHorizontal() ? "x" : "y";
    var swiper = this;
    var params = swiper.params;
    var rtl = swiper.rtlTranslate;
    var translate = swiper.translate;
    var $wrapperEl = swiper.$wrapperEl;

    if (params.virtualTranslate) {
      return rtl ? -translate : translate;
    }

    var currentTranslate = Utils.getTranslate($wrapperEl[0], axis);

    if (rtl) {
      currentTranslate = -currentTranslate;
    }

    return currentTranslate || 0;
  }

  function setTranslate(translate, byController) {
    var swiper = this;
    var rtl = swiper.rtlTranslate;
    var params = swiper.params;
    var $wrapperEl = swiper.$wrapperEl;
    var progress = swiper.progress;
    var x = 0;
    var y = 0;
    var z = 0;

    if (swiper.isHorizontal()) {
      x = rtl ? -translate : translate;
    } else {
      y = translate;
    }

    if (params.roundLengths) {
      x = Math.floor(x);
      y = Math.floor(y);
    }

    if (!params.virtualTranslate) {
      if (Support.transforms3d) {
        $wrapperEl.transform(
          "translate3d(" + x + "px, " + y + "px, " + z + "px)"
        );
      } else {
        $wrapperEl.transform("translate(" + x + "px, " + y + "px)");
      }
    }

    swiper.previousTranslate = swiper.translate;
    swiper.translate = swiper.isHorizontal() ? x : y; // Check if we need to update progress

    var newProgress;
    var translatesDiff = swiper.maxTranslate() - swiper.minTranslate();

    if (translatesDiff === 0) {
      newProgress = 0;
    } else {
      newProgress = (translate - swiper.minTranslate()) / translatesDiff;
    }

    if (newProgress !== progress) {
      swiper.updateProgress(translate);
    }

    swiper.emit("setTranslate", swiper.translate, byController);
  }

  function minTranslate() {
    return -this.snapGrid[0];
  }

  function maxTranslate() {
    return -this.snapGrid[this.snapGrid.length - 1];
  }

  var translate = {
    getTranslate: getTranslate,
    setTranslate: setTranslate,
    minTranslate: minTranslate,
    maxTranslate: maxTranslate,
  };

  function setTransition(duration, byController) {
    var swiper = this;
    swiper.$wrapperEl.transition(duration);
    swiper.emit("setTransition", duration, byController);
  }

  function transitionStart(runCallbacks, direction) {
    if (runCallbacks === void 0) runCallbacks = true;
    var swiper = this;
    var activeIndex = swiper.activeIndex;
    var params = swiper.params;
    var previousIndex = swiper.previousIndex;

    if (params.autoHeight) {
      swiper.updateAutoHeight();
    }

    var dir = direction;

    if (!dir) {
      if (activeIndex > previousIndex) {
        dir = "next";
      } else if (activeIndex < previousIndex) {
        dir = "prev";
      } else {
        dir = "reset";
      }
    }

    swiper.emit("transitionStart");

    if (runCallbacks && activeIndex !== previousIndex) {
      if (dir === "reset") {
        swiper.emit("slideResetTransitionStart");
        return;
      }

      swiper.emit("slideChangeTransitionStart");

      if (dir === "next") {
        swiper.emit("slideNextTransitionStart");
      } else {
        swiper.emit("slidePrevTransitionStart");
      }
    }
  }

  function transitionEnd$1(runCallbacks, direction) {
    if (runCallbacks === void 0) runCallbacks = true;
    var swiper = this;
    var activeIndex = swiper.activeIndex;
    var previousIndex = swiper.previousIndex;
    swiper.animating = false;
    swiper.setTransition(0);
    var dir = direction;

    if (!dir) {
      if (activeIndex > previousIndex) {
        dir = "next";
      } else if (activeIndex < previousIndex) {
        dir = "prev";
      } else {
        dir = "reset";
      }
    }

    swiper.emit("transitionEnd");

    if (runCallbacks && activeIndex !== previousIndex) {
      if (dir === "reset") {
        swiper.emit("slideResetTransitionEnd");
        return;
      }

      swiper.emit("slideChangeTransitionEnd");

      if (dir === "next") {
        swiper.emit("slideNextTransitionEnd");
      } else {
        swiper.emit("slidePrevTransitionEnd");
      }
    }
  }

  var transition$1 = {
    setTransition: setTransition,
    transitionStart: transitionStart,
    transitionEnd: transitionEnd$1,
  };

  function slideTo(index, speed, runCallbacks, internal) {
    if (index === void 0) index = 0;
    if (speed === void 0) speed = this.params.speed;
    if (runCallbacks === void 0) runCallbacks = true;
    var swiper = this;
    var slideIndex = index;

    if (slideIndex < 0) {
      slideIndex = 0;
    }

    var params = swiper.params;
    var snapGrid = swiper.snapGrid;
    var slidesGrid = swiper.slidesGrid;
    var previousIndex = swiper.previousIndex;
    var activeIndex = swiper.activeIndex;
    var rtl = swiper.rtlTranslate;

    if (swiper.animating && params.preventInteractionOnTransition) {
      return false;
    }

    var snapIndex = Math.floor(slideIndex / params.slidesPerGroup);

    if (snapIndex >= snapGrid.length) {
      snapIndex = snapGrid.length - 1;
    }

    if (
      (activeIndex || params.initialSlide || 0) === (previousIndex || 0) &&
      runCallbacks
    ) {
      swiper.emit("beforeSlideChangeStart");
    }

    var translate = -snapGrid[snapIndex]; // Update progress

    swiper.updateProgress(translate); // Normalize slideIndex

    if (params.normalizeSlideIndex) {
      for (var i = 0; i < slidesGrid.length; i += 1) {
        if (-Math.floor(translate * 100) >= Math.floor(slidesGrid[i] * 100)) {
          slideIndex = i;
        }
      }
    } // Directions locks

    if (swiper.initialized && slideIndex !== activeIndex) {
      if (
        !swiper.allowSlideNext &&
        translate < swiper.translate &&
        translate < swiper.minTranslate()
      ) {
        return false;
      }

      if (
        !swiper.allowSlidePrev &&
        translate > swiper.translate &&
        translate > swiper.maxTranslate()
      ) {
        if ((activeIndex || 0) !== slideIndex) {
          return false;
        }
      }
    }

    var direction;

    if (slideIndex > activeIndex) {
      direction = "next";
    } else if (slideIndex < activeIndex) {
      direction = "prev";
    } else {
      direction = "reset";
    } // Update Index

    if (
      (rtl && -translate === swiper.translate) ||
      (!rtl && translate === swiper.translate)
    ) {
      swiper.updateActiveIndex(slideIndex); // Update Height

      if (params.autoHeight) {
        swiper.updateAutoHeight();
      }

      swiper.updateSlidesClasses();

      if (params.effect !== "slide") {
        swiper.setTranslate(translate);
      }

      if (direction !== "reset") {
        swiper.transitionStart(runCallbacks, direction);
        swiper.transitionEnd(runCallbacks, direction);
      }

      return false;
    }

    if (speed === 0 || !Support.transition) {
      swiper.setTransition(0);
      swiper.setTranslate(translate);
      swiper.updateActiveIndex(slideIndex);
      swiper.updateSlidesClasses();
      swiper.emit("beforeTransitionStart", speed, internal);
      swiper.transitionStart(runCallbacks, direction);
      swiper.transitionEnd(runCallbacks, direction);
    } else {
      swiper.setTransition(speed);
      swiper.setTranslate(translate);
      swiper.updateActiveIndex(slideIndex);
      swiper.updateSlidesClasses();
      swiper.emit("beforeTransitionStart", speed, internal);
      swiper.transitionStart(runCallbacks, direction);

      if (!swiper.animating) {
        swiper.animating = true;

        if (!swiper.onSlideToWrapperTransitionEnd) {
          swiper.onSlideToWrapperTransitionEnd = function transitionEnd(e) {
            if (!swiper || swiper.destroyed) {
              return;
            }

            if (e.target !== this) {
              return;
            }

            swiper.$wrapperEl[0].removeEventListener(
              "transitionend",
              swiper.onSlideToWrapperTransitionEnd
            );
            swiper.$wrapperEl[0].removeEventListener(
              "webkitTransitionEnd",
              swiper.onSlideToWrapperTransitionEnd
            );
            swiper.onSlideToWrapperTransitionEnd = null;
            delete swiper.onSlideToWrapperTransitionEnd;
            swiper.transitionEnd(runCallbacks, direction);
          };
        }

        swiper.$wrapperEl[0].addEventListener(
          "transitionend",
          swiper.onSlideToWrapperTransitionEnd
        );
        swiper.$wrapperEl[0].addEventListener(
          "webkitTransitionEnd",
          swiper.onSlideToWrapperTransitionEnd
        );
      }
    }

    return true;
  }

  function slideToLoop(index, speed, runCallbacks, internal) {
    if (index === void 0) index = 0;
    if (speed === void 0) speed = this.params.speed;
    if (runCallbacks === void 0) runCallbacks = true;
    var swiper = this;
    var newIndex = index;

    if (swiper.params.loop) {
      newIndex += swiper.loopedSlides;
    }

    return swiper.slideTo(newIndex, speed, runCallbacks, internal);
  }
  /* eslint no-unused-vars: "off" */

  function slideNext(speed, runCallbacks, internal) {
    if (speed === void 0) speed = this.params.speed;
    if (runCallbacks === void 0) runCallbacks = true;
    var swiper = this;
    var params = swiper.params;
    var animating = swiper.animating;

    if (params.loop) {
      if (animating) {
        return false;
      }

      swiper.loopFix(); // eslint-disable-next-line

      swiper._clientLeft = swiper.$wrapperEl[0].clientLeft;
      return swiper.slideTo(
        swiper.activeIndex + params.slidesPerGroup,
        speed,
        runCallbacks,
        internal
      );
    }

    return swiper.slideTo(
      swiper.activeIndex + params.slidesPerGroup,
      speed,
      runCallbacks,
      internal
    );
  }
  /* eslint no-unused-vars: "off" */

  function slidePrev(speed, runCallbacks, internal) {
    if (speed === void 0) speed = this.params.speed;
    if (runCallbacks === void 0) runCallbacks = true;
    var swiper = this;
    var params = swiper.params;
    var animating = swiper.animating;
    var snapGrid = swiper.snapGrid;
    var slidesGrid = swiper.slidesGrid;
    var rtlTranslate = swiper.rtlTranslate;

    if (params.loop) {
      if (animating) {
        return false;
      }

      swiper.loopFix(); // eslint-disable-next-line

      swiper._clientLeft = swiper.$wrapperEl[0].clientLeft;
    }

    var translate = rtlTranslate ? swiper.translate : -swiper.translate;

    function normalize(val) {
      if (val < 0) {
        return -Math.floor(Math.abs(val));
      }

      return Math.floor(val);
    }

    var normalizedTranslate = normalize(translate);
    var normalizedSnapGrid = snapGrid.map(function (val) {
      return normalize(val);
    });
    var normalizedSlidesGrid = slidesGrid.map(function (val) {
      return normalize(val);
    });
    var currentSnap = snapGrid[normalizedSnapGrid.indexOf(normalizedTranslate)];
    var prevSnap =
      snapGrid[normalizedSnapGrid.indexOf(normalizedTranslate) - 1];
    var prevIndex;

    if (typeof prevSnap !== "undefined") {
      prevIndex = slidesGrid.indexOf(prevSnap);

      if (prevIndex < 0) {
        prevIndex = swiper.activeIndex - 1;
      }
    }

    return swiper.slideTo(prevIndex, speed, runCallbacks, internal);
  }
  /* eslint no-unused-vars: "off" */

  function slideReset(speed, runCallbacks, internal) {
    if (speed === void 0) speed = this.params.speed;
    if (runCallbacks === void 0) runCallbacks = true;
    var swiper = this;
    return swiper.slideTo(swiper.activeIndex, speed, runCallbacks, internal);
  }
  /* eslint no-unused-vars: "off" */

  function slideToClosest(speed, runCallbacks, internal) {
    if (speed === void 0) speed = this.params.speed;
    if (runCallbacks === void 0) runCallbacks = true;
    var swiper = this;
    var index = swiper.activeIndex;
    var snapIndex = Math.floor(index / swiper.params.slidesPerGroup);

    if (snapIndex < swiper.snapGrid.length - 1) {
      var translate = swiper.rtlTranslate
        ? swiper.translate
        : -swiper.translate;
      var currentSnap = swiper.snapGrid[snapIndex];
      var nextSnap = swiper.snapGrid[snapIndex + 1];

      if (translate - currentSnap > (nextSnap - currentSnap) / 2) {
        index = swiper.params.slidesPerGroup;
      }
    }

    return swiper.slideTo(index, speed, runCallbacks, internal);
  }

  function slideToClickedSlide() {
    var swiper = this;
    var params = swiper.params;
    var $wrapperEl = swiper.$wrapperEl;
    var slidesPerView =
      params.slidesPerView === "auto"
        ? swiper.slidesPerViewDynamic()
        : params.slidesPerView;
    var slideToIndex = swiper.clickedIndex;
    var realIndex;

    if (params.loop) {
      if (swiper.animating) {
        return;
      }

      realIndex = parseInt(
        $(swiper.clickedSlide).attr("data-swiper-slide-index"),
        10
      );

      if (params.centeredSlides) {
        if (
          slideToIndex < swiper.loopedSlides - slidesPerView / 2 ||
          slideToIndex >
            swiper.slides.length - swiper.loopedSlides + slidesPerView / 2
        ) {
          swiper.loopFix();
          slideToIndex = $wrapperEl
            .children(
              "." +
                params.slideClass +
                '[data-swiper-slide-index="' +
                realIndex +
                '"]:not(.' +
                params.slideDuplicateClass +
                ")"
            )
            .eq(0)
            .index();
          Utils.nextTick(function () {
            swiper.slideTo(slideToIndex);
          });
        } else {
          swiper.slideTo(slideToIndex);
        }
      } else if (slideToIndex > swiper.slides.length - slidesPerView) {
        swiper.loopFix();
        slideToIndex = $wrapperEl
          .children(
            "." +
              params.slideClass +
              '[data-swiper-slide-index="' +
              realIndex +
              '"]:not(.' +
              params.slideDuplicateClass +
              ")"
          )
          .eq(0)
          .index();
        Utils.nextTick(function () {
          swiper.slideTo(slideToIndex);
        });
      } else {
        swiper.slideTo(slideToIndex);
      }
    } else {
      swiper.slideTo(slideToIndex);
    }
  }

  var slide = {
    slideTo: slideTo,
    slideToLoop: slideToLoop,
    slideNext: slideNext,
    slidePrev: slidePrev,
    slideReset: slideReset,
    slideToClosest: slideToClosest,
    slideToClickedSlide: slideToClickedSlide,
  };

  function loopCreate() {
    var swiper = this;
    var params = swiper.params;
    var $wrapperEl = swiper.$wrapperEl; // Remove duplicated slides

    $wrapperEl
      .children("." + params.slideClass + "." + params.slideDuplicateClass)
      .remove();
    var slides = $wrapperEl.children("." + params.slideClass);

    if (params.loopFillGroupWithBlank) {
      var blankSlidesNum =
        params.slidesPerGroup - (slides.length % params.slidesPerGroup);

      if (blankSlidesNum !== params.slidesPerGroup) {
        for (var i = 0; i < blankSlidesNum; i += 1) {
          var blankNode = $(doc.createElement("div")).addClass(
            params.slideClass + " " + params.slideBlankClass
          );
          $wrapperEl.append(blankNode);
        }

        slides = $wrapperEl.children("." + params.slideClass);
      }
    }

    if (params.slidesPerView === "auto" && !params.loopedSlides) {
      params.loopedSlides = slides.length;
    }

    swiper.loopedSlides = parseInt(
      params.loopedSlides || params.slidesPerView,
      10
    );
    swiper.loopedSlides += params.loopAdditionalSlides;

    if (swiper.loopedSlides > slides.length) {
      swiper.loopedSlides = slides.length;
    }

    var prependSlides = [];
    var appendSlides = [];
    slides.each(function (index, el) {
      var slide = $(el);

      if (index < swiper.loopedSlides) {
        appendSlides.push(el);
      }

      if (
        index < slides.length &&
        index >= slides.length - swiper.loopedSlides
      ) {
        prependSlides.push(el);
      }

      slide.attr("data-swiper-slide-index", index);
    });

    for (var i$1 = 0; i$1 < appendSlides.length; i$1 += 1) {
      $wrapperEl.append(
        $(appendSlides[i$1].cloneNode(true)).addClass(
          params.slideDuplicateClass
        )
      );
    }

    for (var i$2 = prependSlides.length - 1; i$2 >= 0; i$2 -= 1) {
      $wrapperEl.prepend(
        $(prependSlides[i$2].cloneNode(true)).addClass(
          params.slideDuplicateClass
        )
      );
    }
  }

  function loopFix() {
    var swiper = this;
    var params = swiper.params;
    var activeIndex = swiper.activeIndex;
    var slides = swiper.slides;
    var loopedSlides = swiper.loopedSlides;
    var allowSlidePrev = swiper.allowSlidePrev;
    var allowSlideNext = swiper.allowSlideNext;
    var snapGrid = swiper.snapGrid;
    var rtl = swiper.rtlTranslate;
    var newIndex;
    swiper.allowSlidePrev = true;
    swiper.allowSlideNext = true;
    var snapTranslate = -snapGrid[activeIndex];
    var diff = snapTranslate - swiper.getTranslate(); // Fix For Negative Oversliding

    if (activeIndex < loopedSlides) {
      newIndex = slides.length - loopedSlides * 3 + activeIndex;
      newIndex += loopedSlides;
      var slideChanged = swiper.slideTo(newIndex, 0, false, true);

      if (slideChanged && diff !== 0) {
        swiper.setTranslate(
          (rtl ? -swiper.translate : swiper.translate) - diff
        );
      }
    } else if (
      (params.slidesPerView === "auto" && activeIndex >= loopedSlides * 2) ||
      activeIndex >= slides.length - loopedSlides
    ) {
      // Fix For Positive Oversliding
      newIndex = -slides.length + activeIndex + loopedSlides;
      newIndex += loopedSlides;
      var slideChanged$1 = swiper.slideTo(newIndex, 0, false, true);

      if (slideChanged$1 && diff !== 0) {
        swiper.setTranslate(
          (rtl ? -swiper.translate : swiper.translate) - diff
        );
      }
    }

    swiper.allowSlidePrev = allowSlidePrev;
    swiper.allowSlideNext = allowSlideNext;
  }

  function loopDestroy() {
    var swiper = this;
    var $wrapperEl = swiper.$wrapperEl;
    var params = swiper.params;
    var slides = swiper.slides;
    $wrapperEl
      .children(
        "." +
          params.slideClass +
          "." +
          params.slideDuplicateClass +
          ",." +
          params.slideClass +
          "." +
          params.slideBlankClass
      )
      .remove();
    slides.removeAttr("data-swiper-slide-index");
  }

  var loop = {
    loopCreate: loopCreate,
    loopFix: loopFix,
    loopDestroy: loopDestroy,
  };

  function setGrabCursor(moving) {
    var swiper = this;

    if (
      Support.touch ||
      !swiper.params.simulateTouch ||
      (swiper.params.watchOverflow && swiper.isLocked)
    ) {
      return;
    }

    var el = swiper.el;
    el.style.cursor = "move";
    el.style.cursor = moving ? "-webkit-grabbing" : "-webkit-grab";
    el.style.cursor = moving ? "-moz-grabbin" : "-moz-grab";
    el.style.cursor = moving ? "grabbing" : "grab";
  }

  function unsetGrabCursor() {
    var swiper = this;

    if (Support.touch || (swiper.params.watchOverflow && swiper.isLocked)) {
      return;
    }

    swiper.el.style.cursor = "";
  }

  var grabCursor = {
    setGrabCursor: setGrabCursor,
    unsetGrabCursor: unsetGrabCursor,
  };

  function appendSlide(slides) {
    var swiper = this;
    var $wrapperEl = swiper.$wrapperEl;
    var params = swiper.params;

    if (params.loop) {
      swiper.loopDestroy();
    }

    if (_typeof(slides) === "object" && "length" in slides) {
      for (var i = 0; i < slides.length; i += 1) {
        if (slides[i]) {
          $wrapperEl.append(slides[i]);
        }
      }
    } else {
      $wrapperEl.append(slides);
    }

    if (params.loop) {
      swiper.loopCreate();
    }

    if (!(params.observer && Support.observer)) {
      swiper.update();
    }
  }

  function prependSlide(slides) {
    var swiper = this;
    var params = swiper.params;
    var $wrapperEl = swiper.$wrapperEl;
    var activeIndex = swiper.activeIndex;

    if (params.loop) {
      swiper.loopDestroy();
    }

    var newActiveIndex = activeIndex + 1;

    if (_typeof(slides) === "object" && "length" in slides) {
      for (var i = 0; i < slides.length; i += 1) {
        if (slides[i]) {
          $wrapperEl.prepend(slides[i]);
        }
      }

      newActiveIndex = activeIndex + slides.length;
    } else {
      $wrapperEl.prepend(slides);
    }

    if (params.loop) {
      swiper.loopCreate();
    }

    if (!(params.observer && Support.observer)) {
      swiper.update();
    }

    swiper.slideTo(newActiveIndex, 0, false);
  }

  function addSlide(index, slides) {
    var swiper = this;
    var $wrapperEl = swiper.$wrapperEl;
    var params = swiper.params;
    var activeIndex = swiper.activeIndex;
    var activeIndexBuffer = activeIndex;

    if (params.loop) {
      activeIndexBuffer -= swiper.loopedSlides;
      swiper.loopDestroy();
      swiper.slides = $wrapperEl.children("." + params.slideClass);
    }

    var baseLength = swiper.slides.length;

    if (index <= 0) {
      swiper.prependSlide(slides);
      return;
    }

    if (index >= baseLength) {
      swiper.appendSlide(slides);
      return;
    }

    var newActiveIndex =
      activeIndexBuffer > index ? activeIndexBuffer + 1 : activeIndexBuffer;
    var slidesBuffer = [];

    for (var i = baseLength - 1; i >= index; i -= 1) {
      var currentSlide = swiper.slides.eq(i);
      currentSlide.remove();
      slidesBuffer.unshift(currentSlide);
    }

    if (_typeof(slides) === "object" && "length" in slides) {
      for (var i$1 = 0; i$1 < slides.length; i$1 += 1) {
        if (slides[i$1]) {
          $wrapperEl.append(slides[i$1]);
        }
      }

      newActiveIndex =
        activeIndexBuffer > index
          ? activeIndexBuffer + slides.length
          : activeIndexBuffer;
    } else {
      $wrapperEl.append(slides);
    }

    for (var i$2 = 0; i$2 < slidesBuffer.length; i$2 += 1) {
      $wrapperEl.append(slidesBuffer[i$2]);
    }

    if (params.loop) {
      swiper.loopCreate();
    }

    if (!(params.observer && Support.observer)) {
      swiper.update();
    }

    if (params.loop) {
      swiper.slideTo(newActiveIndex + swiper.loopedSlides, 0, false);
    } else {
      swiper.slideTo(newActiveIndex, 0, false);
    }
  }

  function removeSlide(slidesIndexes) {
    var swiper = this;
    var params = swiper.params;
    var $wrapperEl = swiper.$wrapperEl;
    var activeIndex = swiper.activeIndex;
    var activeIndexBuffer = activeIndex;

    if (params.loop) {
      activeIndexBuffer -= swiper.loopedSlides;
      swiper.loopDestroy();
      swiper.slides = $wrapperEl.children("." + params.slideClass);
    }

    var newActiveIndex = activeIndexBuffer;
    var indexToRemove;

    if (_typeof(slidesIndexes) === "object" && "length" in slidesIndexes) {
      for (var i = 0; i < slidesIndexes.length; i += 1) {
        indexToRemove = slidesIndexes[i];

        if (swiper.slides[indexToRemove]) {
          swiper.slides.eq(indexToRemove).remove();
        }

        if (indexToRemove < newActiveIndex) {
          newActiveIndex -= 1;
        }
      }

      newActiveIndex = Math.max(newActiveIndex, 0);
    } else {
      indexToRemove = slidesIndexes;

      if (swiper.slides[indexToRemove]) {
        swiper.slides.eq(indexToRemove).remove();
      }

      if (indexToRemove < newActiveIndex) {
        newActiveIndex -= 1;
      }

      newActiveIndex = Math.max(newActiveIndex, 0);
    }

    if (params.loop) {
      swiper.loopCreate();
    }

    if (!(params.observer && Support.observer)) {
      swiper.update();
    }

    if (params.loop) {
      swiper.slideTo(newActiveIndex + swiper.loopedSlides, 0, false);
    } else {
      swiper.slideTo(newActiveIndex, 0, false);
    }
  }

  function removeAllSlides() {
    var swiper = this;
    var slidesIndexes = [];

    for (var i = 0; i < swiper.slides.length; i += 1) {
      slidesIndexes.push(i);
    }

    swiper.removeSlide(slidesIndexes);
  }

  var manipulation = {
    appendSlide: appendSlide,
    prependSlide: prependSlide,
    addSlide: addSlide,
    removeSlide: removeSlide,
    removeAllSlides: removeAllSlides,
  };

  var Device = (function Device() {
    var ua = win.navigator.userAgent;
    var device = {
      ios: false,
      android: false,
      androidChrome: false,
      desktop: false,
      windows: false,
      iphone: false,
      ipod: false,
      ipad: false,
      cordova: win.cordova || win.phonegap,
      phonegap: win.cordova || win.phonegap,
    };
    var windows = ua.match(/(Windows Phone);?[\s\/]+([\d.]+)?/); // eslint-disable-line

    var android = ua.match(/(Android);?[\s\/]+([\d.]+)?/); // eslint-disable-line

    var ipad = ua.match(/(iPad).*OS\s([\d_]+)/);
    var ipod = ua.match(/(iPod)(.*OS\s([\d_]+))?/);
    var iphone = !ipad && ua.match(/(iPhone\sOS|iOS)\s([\d_]+)/); // Windows

    if (windows) {
      device.os = "windows";
      device.osVersion = windows[2];
      device.windows = true;
    } // Android

    if (android && !windows) {
      device.os = "android";
      device.osVersion = android[2];
      device.android = true;
      device.androidChrome = ua.toLowerCase().indexOf("chrome") >= 0;
    }

    if (ipad || iphone || ipod) {
      device.os = "ios";
      device.ios = true;
    } // iOS

    if (iphone && !ipod) {
      device.osVersion = iphone[2].replace(/_/g, ".");
      device.iphone = true;
    }

    if (ipad) {
      device.osVersion = ipad[2].replace(/_/g, ".");
      device.ipad = true;
    }

    if (ipod) {
      device.osVersion = ipod[3] ? ipod[3].replace(/_/g, ".") : null;
      device.iphone = true;
    } // iOS 8+ changed UA

    if (device.ios && device.osVersion && ua.indexOf("Version/") >= 0) {
      if (device.osVersion.split(".")[0] === "10") {
        device.osVersion = ua.toLowerCase().split("version/")[1].split(" ")[0];
      }
    } // Desktop

    device.desktop = !(device.os || device.android || device.webView); // Webview

    device.webView =
      (iphone || ipad || ipod) && ua.match(/.*AppleWebKit(?!.*Safari)/i); // Minimal UI

    if (device.os && device.os === "ios") {
      var osVersionArr = device.osVersion.split(".");
      var metaViewport = doc.querySelector('meta[name="viewport"]');
      device.minimalUi =
        !device.webView &&
        (ipod || iphone) &&
        (osVersionArr[0] * 1 === 7
          ? osVersionArr[1] * 1 >= 1
          : osVersionArr[0] * 1 > 7) &&
        metaViewport &&
        metaViewport.getAttribute("content").indexOf("minimal-ui") >= 0;
    } // Pixel Ratio

    device.pixelRatio = win.devicePixelRatio || 1; // Export object

    return device;
  })();

  function onTouchStart(event) {
    var swiper = this;
    var data = swiper.touchEventsData;
    var params = swiper.params;
    var touches = swiper.touches;

    if (swiper.animating && params.preventInteractionOnTransition) {
      return;
    }

    var e = event;

    if (e.originalEvent) {
      e = e.originalEvent;
    }

    data.isTouchEvent = e.type === "touchstart";

    if (!data.isTouchEvent && "which" in e && e.which === 3) {
      return;
    }

    if (!data.isTouchEvent && "button" in e && e.button > 0) {
      return;
    }

    if (data.isTouched && data.isMoved) {
      return;
    }

    if (
      params.noSwiping &&
      $(e.target).closest(
        params.noSwipingSelector
          ? params.noSwipingSelector
          : "." + params.noSwipingClass
      )[0]
    ) {
      swiper.allowClick = true;
      return;
    }

    if (params.swipeHandler) {
      if (!$(e).closest(params.swipeHandler)[0]) {
        return;
      }
    }

    touches.currentX =
      e.type === "touchstart" ? e.targetTouches[0].pageX : e.pageX;
    touches.currentY =
      e.type === "touchstart" ? e.targetTouches[0].pageY : e.pageY;
    var startX = touches.currentX;
    var startY = touches.currentY; // Do NOT start if iOS edge swipe is detected. Otherwise iOS app (UIWebView) cannot swipe-to-go-back anymore

    var edgeSwipeDetection =
      params.edgeSwipeDetection || params.iOSEdgeSwipeDetection;
    var edgeSwipeThreshold =
      params.edgeSwipeThreshold || params.iOSEdgeSwipeThreshold;

    if (
      edgeSwipeDetection &&
      (startX <= edgeSwipeThreshold ||
        startX >= win.screen.width - edgeSwipeThreshold)
    ) {
      return;
    }

    Utils.extend(data, {
      isTouched: true,
      isMoved: false,
      allowTouchCallbacks: true,
      isScrolling: undefined,
      startMoving: undefined,
    });
    touches.startX = startX;
    touches.startY = startY;
    data.touchStartTime = Utils.now();
    swiper.allowClick = true;
    swiper.updateSize();
    swiper.swipeDirection = undefined;

    if (params.threshold > 0) {
      data.allowThresholdMove = false;
    }

    if (e.type !== "touchstart") {
      var preventDefault = true;

      if ($(e.target).is(data.formElements)) {
        preventDefault = false;
      }

      if (
        doc.activeElement &&
        $(doc.activeElement).is(data.formElements) &&
        doc.activeElement !== e.target
      ) {
        doc.activeElement.blur();
      }

      var shouldPreventDefault =
        preventDefault &&
        swiper.allowTouchMove &&
        params.touchStartPreventDefault;

      if (params.touchStartForcePreventDefault || shouldPreventDefault) {
        e.preventDefault();
      }
    }

    swiper.emit("touchStart", e);
  }

  function onTouchMove(event) {
    var swiper = this;
    var data = swiper.touchEventsData;
    var params = swiper.params;
    var touches = swiper.touches;
    var rtl = swiper.rtlTranslate;
    var e = event;

    if (e.originalEvent) {
      e = e.originalEvent;
    }

    if (!data.isTouched) {
      if (data.startMoving && data.isScrolling) {
        swiper.emit("touchMoveOpposite", e);
      }

      return;
    }

    if (data.isTouchEvent && e.type === "mousemove") {
      return;
    }

    var targetTouch =
      e.type === "touchmove" &&
      e.targetTouches &&
      (e.targetTouches[0] || e.changedTouches[0]);
    var pageX = e.type === "touchmove" ? targetTouch.pageX : e.pageX;
    var pageY = e.type === "touchmove" ? targetTouch.pageY : e.pageY;

    if (e.preventedByNestedSwiper) {
      touches.startX = pageX;
      touches.startY = pageY;
      return;
    }

    if (!swiper.allowTouchMove) {
      // isMoved = true;
      swiper.allowClick = false;

      if (data.isTouched) {
        Utils.extend(touches, {
          startX: pageX,
          startY: pageY,
          currentX: pageX,
          currentY: pageY,
        });
        data.touchStartTime = Utils.now();
      }

      return;
    }

    if (data.isTouchEvent && params.touchReleaseOnEdges && !params.loop) {
      if (swiper.isVertical()) {
        // Vertical
        if (
          (pageY < touches.startY &&
            swiper.translate <= swiper.maxTranslate()) ||
          (pageY > touches.startY && swiper.translate >= swiper.minTranslate())
        ) {
          data.isTouched = false;
          data.isMoved = false;
          return;
        }
      } else if (
        (pageX < touches.startX && swiper.translate <= swiper.maxTranslate()) ||
        (pageX > touches.startX && swiper.translate >= swiper.minTranslate())
      ) {
        return;
      }
    }

    if (data.isTouchEvent && doc.activeElement) {
      if (e.target === doc.activeElement && $(e.target).is(data.formElements)) {
        data.isMoved = true;
        swiper.allowClick = false;
        return;
      }
    }

    if (data.allowTouchCallbacks) {
      swiper.emit("touchMove", e);
    }

    if (e.targetTouches && e.targetTouches.length > 1) {
      return;
    }

    touches.currentX = pageX;
    touches.currentY = pageY;
    var diffX = touches.currentX - touches.startX;
    var diffY = touches.currentY - touches.startY;

    if (
      swiper.params.threshold &&
      Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2)) <
        swiper.params.threshold
    ) {
      return;
    }

    if (typeof data.isScrolling === "undefined") {
      var touchAngle;

      if (
        (swiper.isHorizontal() && touches.currentY === touches.startY) ||
        (swiper.isVertical() && touches.currentX === touches.startX)
      ) {
        data.isScrolling = false;
      } else {
        // eslint-disable-next-line
        if (diffX * diffX + diffY * diffY >= 25) {
          touchAngle =
            (Math.atan2(Math.abs(diffY), Math.abs(diffX)) * 180) / Math.PI;
          data.isScrolling = swiper.isHorizontal()
            ? touchAngle > params.touchAngle
            : 90 - touchAngle > params.touchAngle;
        }
      }
    }

    if (data.isScrolling) {
      swiper.emit("touchMoveOpposite", e);
    }

    if (typeof data.startMoving === "undefined") {
      if (
        touches.currentX !== touches.startX ||
        touches.currentY !== touches.startY
      ) {
        data.startMoving = true;
      }
    }

    if (data.isScrolling) {
      data.isTouched = false;
      return;
    }

    if (!data.startMoving) {
      return;
    }

    swiper.allowClick = false;
    e.preventDefault();

    if (params.touchMoveStopPropagation && !params.nested) {
      e.stopPropagation();
    }

    if (!data.isMoved) {
      if (params.loop) {
        swiper.loopFix();
      }

      data.startTranslate = swiper.getTranslate();
      swiper.setTransition(0);

      if (swiper.animating) {
        swiper.$wrapperEl.trigger("webkitTransitionEnd transitionend");
      }

      data.allowMomentumBounce = false; // Grab Cursor

      if (
        params.grabCursor &&
        (swiper.allowSlideNext === true || swiper.allowSlidePrev === true)
      ) {
        swiper.setGrabCursor(true);
      }

      swiper.emit("sliderFirstMove", e);
    }

    swiper.emit("sliderMove", e);
    data.isMoved = true;
    var diff = swiper.isHorizontal() ? diffX : diffY;
    touches.diff = diff;
    diff *= params.touchRatio;

    if (rtl) {
      diff = -diff;
    }

    swiper.swipeDirection = diff > 0 ? "prev" : "next";
    data.currentTranslate = diff + data.startTranslate;
    var disableParentSwiper = true;
    var resistanceRatio = params.resistanceRatio;

    if (params.touchReleaseOnEdges) {
      resistanceRatio = 0;
    }

    if (diff > 0 && data.currentTranslate > swiper.minTranslate()) {
      disableParentSwiper = false;

      if (params.resistance) {
        data.currentTranslate =
          swiper.minTranslate() -
          1 +
          Math.pow(
            -swiper.minTranslate() + data.startTranslate + diff,
            resistanceRatio
          );
      }
    } else if (diff < 0 && data.currentTranslate < swiper.maxTranslate()) {
      disableParentSwiper = false;

      if (params.resistance) {
        data.currentTranslate =
          swiper.maxTranslate() +
          1 -
          Math.pow(
            swiper.maxTranslate() - data.startTranslate - diff,
            resistanceRatio
          );
      }
    }

    if (disableParentSwiper) {
      e.preventedByNestedSwiper = true;
    } // Directions locks

    if (
      !swiper.allowSlideNext &&
      swiper.swipeDirection === "next" &&
      data.currentTranslate < data.startTranslate
    ) {
      data.currentTranslate = data.startTranslate;
    }

    if (
      !swiper.allowSlidePrev &&
      swiper.swipeDirection === "prev" &&
      data.currentTranslate > data.startTranslate
    ) {
      data.currentTranslate = data.startTranslate;
    } // Threshold

    if (params.threshold > 0) {
      if (Math.abs(diff) > params.threshold || data.allowThresholdMove) {
        if (!data.allowThresholdMove) {
          data.allowThresholdMove = true;
          touches.startX = touches.currentX;
          touches.startY = touches.currentY;
          data.currentTranslate = data.startTranslate;
          touches.diff = swiper.isHorizontal()
            ? touches.currentX - touches.startX
            : touches.currentY - touches.startY;
          return;
        }
      } else {
        data.currentTranslate = data.startTranslate;
        return;
      }
    }

    if (!params.followFinger) {
      return;
    } // Update active index in free mode

    if (
      params.freeMode ||
      params.watchSlidesProgress ||
      params.watchSlidesVisibility
    ) {
      swiper.updateActiveIndex();
      swiper.updateSlidesClasses();
    }

    if (params.freeMode) {
      // Velocity
      if (data.velocities.length === 0) {
        data.velocities.push({
          position: touches[swiper.isHorizontal() ? "startX" : "startY"],
          time: data.touchStartTime,
        });
      }

      data.velocities.push({
        position: touches[swiper.isHorizontal() ? "currentX" : "currentY"],
        time: Utils.now(),
      });
    } // Update progress

    swiper.updateProgress(data.currentTranslate); // Update translate

    swiper.setTranslate(data.currentTranslate);
  }

  function onTouchEnd(event) {
    var swiper = this;
    var data = swiper.touchEventsData;
    var params = swiper.params;
    var touches = swiper.touches;
    var rtl = swiper.rtlTranslate;
    var $wrapperEl = swiper.$wrapperEl;
    var slidesGrid = swiper.slidesGrid;
    var snapGrid = swiper.snapGrid;
    var e = event;

    if (e.originalEvent) {
      e = e.originalEvent;
    }

    if (data.allowTouchCallbacks) {
      swiper.emit("touchEnd", e);
    }

    data.allowTouchCallbacks = false;

    if (!data.isTouched) {
      if (data.isMoved && params.grabCursor) {
        swiper.setGrabCursor(false);
      }

      data.isMoved = false;
      data.startMoving = false;
      return;
    } // Return Grab Cursor

    if (
      params.grabCursor &&
      data.isMoved &&
      data.isTouched &&
      (swiper.allowSlideNext === true || swiper.allowSlidePrev === true)
    ) {
      swiper.setGrabCursor(false);
    } // Time diff

    var touchEndTime = Utils.now();
    var timeDiff = touchEndTime - data.touchStartTime; // Tap, doubleTap, Click

    if (swiper.allowClick) {
      swiper.updateClickedSlide(e);
      swiper.emit("tap", e);

      if (timeDiff < 300 && touchEndTime - data.lastClickTime > 300) {
        if (data.clickTimeout) {
          clearTimeout(data.clickTimeout);
        }

        data.clickTimeout = Utils.nextTick(function () {
          if (!swiper || swiper.destroyed) {
            return;
          }

          swiper.emit("click", e);
        }, 300);
      }

      if (timeDiff < 300 && touchEndTime - data.lastClickTime < 300) {
        if (data.clickTimeout) {
          clearTimeout(data.clickTimeout);
        }

        swiper.emit("doubleTap", e);
      }
    }

    data.lastClickTime = Utils.now();
    Utils.nextTick(function () {
      if (!swiper.destroyed) {
        swiper.allowClick = true;
      }
    });

    if (
      !data.isTouched ||
      !data.isMoved ||
      !swiper.swipeDirection ||
      touches.diff === 0 ||
      data.currentTranslate === data.startTranslate
    ) {
      data.isTouched = false;
      data.isMoved = false;
      data.startMoving = false;
      return;
    }

    data.isTouched = false;
    data.isMoved = false;
    data.startMoving = false;
    var currentPos;

    if (params.followFinger) {
      currentPos = rtl ? swiper.translate : -swiper.translate;
    } else {
      currentPos = -data.currentTranslate;
    }

    if (params.freeMode) {
      if (currentPos < -swiper.minTranslate()) {
        swiper.slideTo(swiper.activeIndex);
        return;
      }

      if (currentPos > -swiper.maxTranslate()) {
        if (swiper.slides.length < snapGrid.length) {
          swiper.slideTo(snapGrid.length - 1);
        } else {
          swiper.slideTo(swiper.slides.length - 1);
        }

        return;
      }

      if (params.freeModeMomentum) {
        if (data.velocities.length > 1) {
          var lastMoveEvent = data.velocities.pop();
          var velocityEvent = data.velocities.pop();
          var distance = lastMoveEvent.position - velocityEvent.position;
          var time = lastMoveEvent.time - velocityEvent.time;
          swiper.velocity = distance / time;
          swiper.velocity /= 2;

          if (Math.abs(swiper.velocity) < params.freeModeMinimumVelocity) {
            swiper.velocity = 0;
          } // this implies that the user stopped moving a finger then released.
          // There would be no events with distance zero, so the last event is stale.

          if (time > 150 || Utils.now() - lastMoveEvent.time > 300) {
            swiper.velocity = 0;
          }
        } else {
          swiper.velocity = 0;
        }

        swiper.velocity *= params.freeModeMomentumVelocityRatio;
        data.velocities.length = 0;
        var momentumDuration = 1000 * params.freeModeMomentumRatio;
        var momentumDistance = swiper.velocity * momentumDuration;
        var newPosition = swiper.translate + momentumDistance;

        if (rtl) {
          newPosition = -newPosition;
        }

        var doBounce = false;
        var afterBouncePosition;
        var bounceAmount =
          Math.abs(swiper.velocity) * 20 * params.freeModeMomentumBounceRatio;
        var needsLoopFix;

        if (newPosition < swiper.maxTranslate()) {
          if (params.freeModeMomentumBounce) {
            if (newPosition + swiper.maxTranslate() < -bounceAmount) {
              newPosition = swiper.maxTranslate() - bounceAmount;
            }

            afterBouncePosition = swiper.maxTranslate();
            doBounce = true;
            data.allowMomentumBounce = true;
          } else {
            newPosition = swiper.maxTranslate();
          }

          if (params.loop && params.centeredSlides) {
            needsLoopFix = true;
          }
        } else if (newPosition > swiper.minTranslate()) {
          if (params.freeModeMomentumBounce) {
            if (newPosition - swiper.minTranslate() > bounceAmount) {
              newPosition = swiper.minTranslate() + bounceAmount;
            }

            afterBouncePosition = swiper.minTranslate();
            doBounce = true;
            data.allowMomentumBounce = true;
          } else {
            newPosition = swiper.minTranslate();
          }

          if (params.loop && params.centeredSlides) {
            needsLoopFix = true;
          }
        } else if (params.freeModeSticky) {
          var nextSlide;

          for (var j = 0; j < snapGrid.length; j += 1) {
            if (snapGrid[j] > -newPosition) {
              nextSlide = j;
              break;
            }
          }

          if (
            Math.abs(snapGrid[nextSlide] - newPosition) <
              Math.abs(snapGrid[nextSlide - 1] - newPosition) ||
            swiper.swipeDirection === "next"
          ) {
            newPosition = snapGrid[nextSlide];
          } else {
            newPosition = snapGrid[nextSlide - 1];
          }

          newPosition = -newPosition;
        }

        if (needsLoopFix) {
          swiper.once("transitionEnd", function () {
            swiper.loopFix();
          });
        } // Fix duration

        if (swiper.velocity !== 0) {
          if (rtl) {
            momentumDuration = Math.abs(
              (-newPosition - swiper.translate) / swiper.velocity
            );
          } else {
            momentumDuration = Math.abs(
              (newPosition - swiper.translate) / swiper.velocity
            );
          }
        } else if (params.freeModeSticky) {
          swiper.slideToClosest();
          return;
        }

        if (params.freeModeMomentumBounce && doBounce) {
          swiper.updateProgress(afterBouncePosition);
          swiper.setTransition(momentumDuration);
          swiper.setTranslate(newPosition);
          swiper.transitionStart(true, swiper.swipeDirection);
          swiper.animating = true;
          $wrapperEl.transitionEnd(function () {
            if (!swiper || swiper.destroyed || !data.allowMomentumBounce) {
              return;
            }

            swiper.emit("momentumBounce");
            swiper.setTransition(params.speed);
            swiper.setTranslate(afterBouncePosition);
            $wrapperEl.transitionEnd(function () {
              if (!swiper || swiper.destroyed) {
                return;
              }

              swiper.transitionEnd();
            });
          });
        } else if (swiper.velocity) {
          swiper.updateProgress(newPosition);
          swiper.setTransition(momentumDuration);
          swiper.setTranslate(newPosition);
          swiper.transitionStart(true, swiper.swipeDirection);

          if (!swiper.animating) {
            swiper.animating = true;
            $wrapperEl.transitionEnd(function () {
              if (!swiper || swiper.destroyed) {
                return;
              }

              swiper.transitionEnd();
            });
          }
        } else {
          swiper.updateProgress(newPosition);
        }

        swiper.updateActiveIndex();
        swiper.updateSlidesClasses();
      } else if (params.freeModeSticky) {
        swiper.slideToClosest();
        return;
      }

      if (!params.freeModeMomentum || timeDiff >= params.longSwipesMs) {
        swiper.updateProgress();
        swiper.updateActiveIndex();
        swiper.updateSlidesClasses();
      }

      return;
    } // Find current slide

    var stopIndex = 0;
    var groupSize = swiper.slidesSizesGrid[0];

    for (var i = 0; i < slidesGrid.length; i += params.slidesPerGroup) {
      if (typeof slidesGrid[i + params.slidesPerGroup] !== "undefined") {
        if (
          currentPos >= slidesGrid[i] &&
          currentPos < slidesGrid[i + params.slidesPerGroup]
        ) {
          stopIndex = i;
          groupSize = slidesGrid[i + params.slidesPerGroup] - slidesGrid[i];
        }
      } else if (currentPos >= slidesGrid[i]) {
        stopIndex = i;
        groupSize =
          slidesGrid[slidesGrid.length - 1] - slidesGrid[slidesGrid.length - 2];
      }
    } // Find current slide size

    var ratio = (currentPos - slidesGrid[stopIndex]) / groupSize;

    if (timeDiff > params.longSwipesMs) {
      // Long touches
      if (!params.longSwipes) {
        swiper.slideTo(swiper.activeIndex);
        return;
      }

      if (swiper.swipeDirection === "next") {
        if (ratio >= params.longSwipesRatio) {
          swiper.slideTo(stopIndex + params.slidesPerGroup);
        } else {
          swiper.slideTo(stopIndex);
        }
      }

      if (swiper.swipeDirection === "prev") {
        if (ratio > 1 - params.longSwipesRatio) {
          swiper.slideTo(stopIndex + params.slidesPerGroup);
        } else {
          swiper.slideTo(stopIndex);
        }
      }
    } else {
      // Short swipes
      if (!params.shortSwipes) {
        swiper.slideTo(swiper.activeIndex);
        return;
      }

      if (swiper.swipeDirection === "next") {
        swiper.slideTo(stopIndex + params.slidesPerGroup);
      }

      if (swiper.swipeDirection === "prev") {
        swiper.slideTo(stopIndex);
      }
    }
  }

  function onResize() {
    var swiper = this;
    var params = swiper.params;
    var el = swiper.el;

    if (el && el.offsetWidth === 0) {
      return;
    } // Breakpoints

    if (params.breakpoints) {
      swiper.setBreakpoint();
    } // Save locks

    var allowSlideNext = swiper.allowSlideNext;
    var allowSlidePrev = swiper.allowSlidePrev;
    var snapGrid = swiper.snapGrid; // Disable locks on resize

    swiper.allowSlideNext = true;
    swiper.allowSlidePrev = true;
    swiper.updateSize();
    swiper.updateSlides();

    if (params.freeMode) {
      var newTranslate = Math.min(
        Math.max(swiper.translate, swiper.maxTranslate()),
        swiper.minTranslate()
      );
      swiper.setTranslate(newTranslate);
      swiper.updateActiveIndex();
      swiper.updateSlidesClasses();

      if (params.autoHeight) {
        swiper.updateAutoHeight();
      }
    } else {
      swiper.updateSlidesClasses();

      if (
        (params.slidesPerView === "auto" || params.slidesPerView > 1) &&
        swiper.isEnd &&
        !swiper.params.centeredSlides
      ) {
        swiper.slideTo(swiper.slides.length - 1, 0, false, true);
      } else {
        swiper.slideTo(swiper.activeIndex, 0, false, true);
      }
    }

    if (swiper.autoplay && swiper.autoplay.running && swiper.autoplay.paused) {
      swiper.autoplay.run();
    } // Return locks after resize

    swiper.allowSlidePrev = allowSlidePrev;
    swiper.allowSlideNext = allowSlideNext;

    if (swiper.params.watchOverflow && snapGrid !== swiper.snapGrid) {
      swiper.checkOverflow();
    }
  }

  function onClick(e) {
    var swiper = this;

    if (!swiper.allowClick) {
      if (swiper.params.preventClicks) {
        e.preventDefault();
      }

      if (swiper.params.preventClicksPropagation && swiper.animating) {
        e.stopPropagation();
        e.stopImmediatePropagation();
      }
    }
  }

  var dummyEventAttached = false;

  function dummyEventListener() {}

  function attachEvents() {
    var swiper = this;
    var params = swiper.params;
    var touchEvents = swiper.touchEvents;
    var el = swiper.el;
    var wrapperEl = swiper.wrapperEl;
    {
      swiper.onTouchStart = onTouchStart.bind(swiper);
      swiper.onTouchMove = onTouchMove.bind(swiper);
      swiper.onTouchEnd = onTouchEnd.bind(swiper);
    }
    swiper.onClick = onClick.bind(swiper);
    var target = params.touchEventsTarget === "container" ? el : wrapperEl;
    var capture = !!params.nested; // Touch Events

    {
      if (
        !Support.touch &&
        (Support.pointerEvents || Support.prefixedPointerEvents)
      ) {
        target.addEventListener(touchEvents.start, swiper.onTouchStart, false);
        doc.addEventListener(touchEvents.move, swiper.onTouchMove, capture);
        doc.addEventListener(touchEvents.end, swiper.onTouchEnd, false);
      } else {
        if (Support.touch) {
          var passiveListener =
            touchEvents.start === "touchstart" &&
            Support.passiveListener &&
            params.passiveListeners
              ? {
                  passive: true,
                  capture: false,
                }
              : false;
          target.addEventListener(
            touchEvents.start,
            swiper.onTouchStart,
            passiveListener
          );
          target.addEventListener(
            touchEvents.move,
            swiper.onTouchMove,
            Support.passiveListener
              ? {
                  passive: false,
                  capture: capture,
                }
              : capture
          );
          target.addEventListener(
            touchEvents.end,
            swiper.onTouchEnd,
            passiveListener
          );

          if (!dummyEventAttached) {
            doc.addEventListener("touchstart", dummyEventListener);
            dummyEventAttached = true;
          }
        }

        if (
          (params.simulateTouch && !Device.ios && !Device.android) ||
          (params.simulateTouch && !Support.touch && Device.ios)
        ) {
          target.addEventListener("mousedown", swiper.onTouchStart, false);
          doc.addEventListener("mousemove", swiper.onTouchMove, capture);
          doc.addEventListener("mouseup", swiper.onTouchEnd, false);
        }
      } // Prevent Links Clicks

      if (params.preventClicks || params.preventClicksPropagation) {
        target.addEventListener("click", swiper.onClick, true);
      }
    } // Resize handler

    swiper.on(
      Device.ios || Device.android
        ? "resize orientationchange observerUpdate"
        : "resize observerUpdate",
      onResize,
      true
    );
  }

  function detachEvents() {
    var swiper = this;
    var params = swiper.params;
    var touchEvents = swiper.touchEvents;
    var el = swiper.el;
    var wrapperEl = swiper.wrapperEl;
    var target = params.touchEventsTarget === "container" ? el : wrapperEl;
    var capture = !!params.nested; // Touch Events

    {
      if (
        !Support.touch &&
        (Support.pointerEvents || Support.prefixedPointerEvents)
      ) {
        target.removeEventListener(
          touchEvents.start,
          swiper.onTouchStart,
          false
        );
        doc.removeEventListener(touchEvents.move, swiper.onTouchMove, capture);
        doc.removeEventListener(touchEvents.end, swiper.onTouchEnd, false);
      } else {
        if (Support.touch) {
          var passiveListener =
            touchEvents.start === "onTouchStart" &&
            Support.passiveListener &&
            params.passiveListeners
              ? {
                  passive: true,
                  capture: false,
                }
              : false;
          target.removeEventListener(
            touchEvents.start,
            swiper.onTouchStart,
            passiveListener
          );
          target.removeEventListener(
            touchEvents.move,
            swiper.onTouchMove,
            capture
          );
          target.removeEventListener(
            touchEvents.end,
            swiper.onTouchEnd,
            passiveListener
          );
        }

        if (
          (params.simulateTouch && !Device.ios && !Device.android) ||
          (params.simulateTouch && !Support.touch && Device.ios)
        ) {
          target.removeEventListener("mousedown", swiper.onTouchStart, false);
          doc.removeEventListener("mousemove", swiper.onTouchMove, capture);
          doc.removeEventListener("mouseup", swiper.onTouchEnd, false);
        }
      } // Prevent Links Clicks

      if (params.preventClicks || params.preventClicksPropagation) {
        target.removeEventListener("click", swiper.onClick, true);
      }
    } // Resize handler

    swiper.off(
      Device.ios || Device.android
        ? "resize orientationchange observerUpdate"
        : "resize observerUpdate",
      onResize
    );
  }

  var events = {
    attachEvents: attachEvents,
    detachEvents: detachEvents,
  };

  function setBreakpoint() {
    var swiper = this;
    var activeIndex = swiper.activeIndex;
    var initialized = swiper.initialized;
    var loopedSlides = swiper.loopedSlides;
    if (loopedSlides === void 0) loopedSlides = 0;
    var params = swiper.params;
    var breakpoints = params.breakpoints;

    if (
      !breakpoints ||
      (breakpoints && Object.keys(breakpoints).length === 0)
    ) {
      return;
    } // Set breakpoint for window width and update parameters

    var breakpoint = swiper.getBreakpoint(breakpoints);

    if (breakpoint && swiper.currentBreakpoint !== breakpoint) {
      var breakpointOnlyParams =
        breakpoint in breakpoints ? breakpoints[breakpoint] : undefined;

      if (breakpointOnlyParams) {
        ["slidesPerView", "spaceBetween", "slidesPerGroup"].forEach(function (
          param
        ) {
          var paramValue = breakpointOnlyParams[param];

          if (typeof paramValue === "undefined") {
            return;
          }

          if (
            param === "slidesPerView" &&
            (paramValue === "AUTO" || paramValue === "auto")
          ) {
            breakpointOnlyParams[param] = "auto";
          } else if (param === "slidesPerView") {
            breakpointOnlyParams[param] = parseFloat(paramValue);
          } else {
            breakpointOnlyParams[param] = parseInt(paramValue, 10);
          }
        });
      }

      var breakpointParams = breakpointOnlyParams || swiper.originalParams;
      var directionChanged =
        breakpointParams.direction &&
        breakpointParams.direction !== params.direction;
      var needsReLoop =
        params.loop &&
        (breakpointParams.slidesPerView !== params.slidesPerView ||
          directionChanged);

      if (directionChanged && initialized) {
        swiper.changeDirection();
      }

      Utils.extend(swiper.params, breakpointParams);
      Utils.extend(swiper, {
        allowTouchMove: swiper.params.allowTouchMove,
        allowSlideNext: swiper.params.allowSlideNext,
        allowSlidePrev: swiper.params.allowSlidePrev,
      });
      swiper.currentBreakpoint = breakpoint;

      if (needsReLoop && initialized) {
        swiper.loopDestroy();
        swiper.loopCreate();
        swiper.updateSlides();
        swiper.slideTo(
          activeIndex - loopedSlides + swiper.loopedSlides,
          0,
          false
        );
      }

      swiper.emit("breakpoint", breakpointParams);
    }
  }

  function getBreakpoint(breakpoints) {
    var swiper = this; // Get breakpoint for window width

    if (!breakpoints) {
      return undefined;
    }

    var breakpoint = false;
    var points = [];
    Object.keys(breakpoints).forEach(function (point) {
      points.push(point);
    });
    points.sort(function (a, b) {
      return parseInt(a, 10) - parseInt(b, 10);
    });

    for (var i = 0; i < points.length; i += 1) {
      var point = points[i];

      if (swiper.params.breakpointsInverse) {
        if (point <= win.innerWidth) {
          breakpoint = point;
        }
      } else if (point >= win.innerWidth && !breakpoint) {
        breakpoint = point;
      }
    }

    return breakpoint || "max";
  }

  var breakpoints = {
    setBreakpoint: setBreakpoint,
    getBreakpoint: getBreakpoint,
  };

  function addClasses() {
    var swiper = this;
    var classNames = swiper.classNames;
    var params = swiper.params;
    var rtl = swiper.rtl;
    var $el = swiper.$el;
    var suffixes = [];
    suffixes.push("initialized");
    suffixes.push(params.direction);

    if (params.freeMode) {
      suffixes.push("free-mode");
    }

    if (!Support.flexbox) {
      suffixes.push("no-flexbox");
    }

    if (params.autoHeight) {
      suffixes.push("autoheight");
    }

    if (rtl) {
      suffixes.push("rtl");
    }

    if (params.slidesPerColumn > 1) {
      suffixes.push("multirow");
    }

    if (Device.android) {
      suffixes.push("android");
    }

    if (Device.ios) {
      suffixes.push("ios");
    } // WP8 Touch Events Fix

    if (
      (Browser.isIE || Browser.isEdge) &&
      (Support.pointerEvents || Support.prefixedPointerEvents)
    ) {
      suffixes.push("wp8-" + params.direction);
    }

    suffixes.forEach(function (suffix) {
      classNames.push(params.containerModifierClass + suffix);
    });
    $el.addClass(classNames.join(" "));
  }

  function removeClasses() {
    var swiper = this;
    var $el = swiper.$el;
    var classNames = swiper.classNames;
    $el.removeClass(classNames.join(" "));
  }

  var classes = {
    addClasses: addClasses,
    removeClasses: removeClasses,
  };

  function loadImage(imageEl, src, srcset, sizes, checkForComplete, callback) {
    var image;

    function onReady() {
      if (callback) {
        callback();
      }
    }

    if (!imageEl.complete || !checkForComplete) {
      if (src) {
        image = new win.Image();
        image.onload = onReady;
        image.onerror = onReady;

        if (sizes) {
          image.sizes = sizes;
        }

        if (srcset) {
          image.srcset = srcset;
        }

        if (src) {
          image.src = src;
        }
      } else {
        onReady();
      }
    } else {
      // image already loaded...
      onReady();
    }
  }

  function preloadImages() {
    var swiper = this;
    swiper.imagesToLoad = swiper.$el.find("img");

    function onReady() {
      if (
        typeof swiper === "undefined" ||
        swiper === null ||
        !swiper ||
        swiper.destroyed
      ) {
        return;
      }

      if (swiper.imagesLoaded !== undefined) {
        swiper.imagesLoaded += 1;
      }

      if (swiper.imagesLoaded === swiper.imagesToLoad.length) {
        if (swiper.params.updateOnImagesReady) {
          swiper.update();
        }

        swiper.emit("imagesReady");
      }
    }

    for (var i = 0; i < swiper.imagesToLoad.length; i += 1) {
      var imageEl = swiper.imagesToLoad[i];
      swiper.loadImage(
        imageEl,
        imageEl.currentSrc || imageEl.getAttribute("src"),
        imageEl.srcset || imageEl.getAttribute("srcset"),
        imageEl.sizes || imageEl.getAttribute("sizes"),
        true,
        onReady
      );
    }
  }

  var images = {
    loadImage: loadImage,
    preloadImages: preloadImages,
  };

  function checkOverflow() {
    var swiper = this;
    var wasLocked = swiper.isLocked;
    swiper.isLocked = swiper.snapGrid.length === 1;
    swiper.allowSlideNext = !swiper.isLocked;
    swiper.allowSlidePrev = !swiper.isLocked; // events

    if (wasLocked !== swiper.isLocked) {
      swiper.emit(swiper.isLocked ? "lock" : "unlock");
    }

    if (wasLocked && wasLocked !== swiper.isLocked) {
      swiper.isEnd = false;
      swiper.navigation.update();
    }
  }

  var checkOverflow$1 = {
    checkOverflow: checkOverflow,
  };
  var defaults = {
    init: true,
    direction: "horizontal",
    touchEventsTarget: "container",
    initialSlide: 0,
    speed: 300,
    //
    preventInteractionOnTransition: false,
    // To support iOS's swipe-to-go-back gesture (when being used in-app, with UIWebView).
    edgeSwipeDetection: false,
    edgeSwipeThreshold: 20,
    // Free mode
    freeMode: false,
    freeModeMomentum: true,
    freeModeMomentumRatio: 1,
    freeModeMomentumBounce: true,
    freeModeMomentumBounceRatio: 1,
    freeModeMomentumVelocityRatio: 1,
    freeModeSticky: false,
    freeModeMinimumVelocity: 0.02,
    // Autoheight
    autoHeight: false,
    // Set wrapper width
    setWrapperSize: false,
    // Virtual Translate
    virtualTranslate: false,
    // Effects
    effect: "slide",
    // 'slide' or 'fade' or 'cube' or 'coverflow' or 'flip'
    // Breakpoints
    breakpoints: undefined,
    breakpointsInverse: false,
    // Slides grid
    spaceBetween: 0,
    slidesPerView: 1,
    slidesPerColumn: 1,
    slidesPerColumnFill: "column",
    slidesPerGroup: 1,
    centeredSlides: false,
    slidesOffsetBefore: 0,
    // in px
    slidesOffsetAfter: 0,
    // in px
    normalizeSlideIndex: true,
    centerInsufficientSlides: false,
    // Disable swiper and hide navigation when container not overflow
    watchOverflow: false,
    // Round length
    roundLengths: false,
    // Touches
    touchRatio: 1,
    touchAngle: 45,
    simulateTouch: true,
    shortSwipes: true,
    longSwipes: true,
    longSwipesRatio: 0.5,
    longSwipesMs: 300,
    followFinger: true,
    allowTouchMove: true,
    threshold: 0,
    touchMoveStopPropagation: true,
    touchStartPreventDefault: true,
    touchStartForcePreventDefault: false,
    touchReleaseOnEdges: false,
    // Unique Navigation Elements
    uniqueNavElements: true,
    // Resistance
    resistance: true,
    resistanceRatio: 0.85,
    // Progress
    watchSlidesProgress: false,
    watchSlidesVisibility: false,
    // Cursor
    grabCursor: false,
    // Clicks
    preventClicks: true,
    preventClicksPropagation: true,
    slideToClickedSlide: false,
    // Images
    preloadImages: true,
    updateOnImagesReady: true,
    // loop
    loop: false,
    loopAdditionalSlides: 0,
    loopedSlides: null,
    loopFillGroupWithBlank: false,
    // Swiping/no swiping
    allowSlidePrev: true,
    allowSlideNext: true,
    swipeHandler: null,
    // '.swipe-handler',
    noSwiping: true,
    noSwipingClass: "swiper-no-swiping",
    noSwipingSelector: null,
    // Passive Listeners
    passiveListeners: true,
    // NS
    containerModifierClass: "swiper-container-",
    // NEW
    slideClass: "swiper-slide",
    slideBlankClass: "swiper-slide-invisible-blank",
    slideActiveClass: "swiper-slide-active",
    slideDuplicateActiveClass: "swiper-slide-duplicate-active",
    slideVisibleClass: "swiper-slide-visible",
    slideDuplicateClass: "swiper-slide-duplicate",
    slideNextClass: "swiper-slide-next",
    slideDuplicateNextClass: "swiper-slide-duplicate-next",
    slidePrevClass: "swiper-slide-prev",
    slideDuplicatePrevClass: "swiper-slide-duplicate-prev",
    wrapperClass: "swiper-wrapper",
    // Callbacks
    runCallbacksOnInit: true,
  };
  /* eslint no-param-reassign: "off" */

  var prototypes = {
    update: update,
    translate: translate,
    transition: transition$1,
    slide: slide,
    loop: loop,
    grabCursor: grabCursor,
    manipulation: manipulation,
    events: events,
    breakpoints: breakpoints,
    checkOverflow: checkOverflow$1,
    classes: classes,
    images: images,
  };
  var extendedDefaults = {};

  var Swiper = /*@__PURE__*/ (function (SwiperClass) {
    function Swiper() {
      var assign;
      var args = [],
        len = arguments.length;

      while (len--) {
        args[len] = arguments[len];
      }

      var el;
      var params;

      if (
        args.length === 1 &&
        args[0].constructor &&
        args[0].constructor === Object
      ) {
        params = args[0];
      } else {
        (assign = args), (el = assign[0]), (params = assign[1]);
      }

      if (!params) {
        params = {};
      }

      params = Utils.extend({}, params);

      if (el && !params.el) {
        params.el = el;
      }

      SwiperClass.call(this, params);
      Object.keys(prototypes).forEach(function (prototypeGroup) {
        Object.keys(prototypes[prototypeGroup]).forEach(function (protoMethod) {
          if (!Swiper.prototype[protoMethod]) {
            Swiper.prototype[protoMethod] =
              prototypes[prototypeGroup][protoMethod];
          }
        });
      }); // Swiper Instance

      var swiper = this;

      if (typeof swiper.modules === "undefined") {
        swiper.modules = {};
      }

      Object.keys(swiper.modules).forEach(function (moduleName) {
        var module = swiper.modules[moduleName];

        if (module.params) {
          var moduleParamName = Object.keys(module.params)[0];
          var moduleParams = module.params[moduleParamName];

          if (_typeof(moduleParams) !== "object" || moduleParams === null) {
            return;
          }

          if (!(moduleParamName in params && "enabled" in moduleParams)) {
            return;
          }

          if (params[moduleParamName] === true) {
            params[moduleParamName] = {
              enabled: true,
            };
          }

          if (
            _typeof(params[moduleParamName]) === "object" &&
            !("enabled" in params[moduleParamName])
          ) {
            params[moduleParamName].enabled = true;
          }

          if (!params[moduleParamName]) {
            params[moduleParamName] = {
              enabled: false,
            };
          }
        }
      }); // Extend defaults with modules params

      var swiperParams = Utils.extend({}, defaults);
      swiper.useModulesParams(swiperParams); // Extend defaults with passed params

      swiper.params = Utils.extend({}, swiperParams, extendedDefaults, params);
      swiper.originalParams = Utils.extend({}, swiper.params);
      swiper.passedParams = Utils.extend({}, params); // Save Dom lib

      swiper.$ = $; // Find el

      var $el = $(swiper.params.el);
      el = $el[0];

      if (!el) {
        return undefined;
      }

      if ($el.length > 1) {
        var swipers = [];
        $el.each(function (index, containerEl) {
          var newParams = Utils.extend({}, params, {
            el: containerEl,
          });
          swipers.push(new Swiper(newParams));
        });
        return swipers;
      }

      el.swiper = swiper;
      $el.data("swiper", swiper); // Find Wrapper

      var $wrapperEl = $el.children("." + swiper.params.wrapperClass); // Extend Swiper

      Utils.extend(swiper, {
        $el: $el,
        el: el,
        $wrapperEl: $wrapperEl,
        wrapperEl: $wrapperEl[0],
        // Classes
        classNames: [],
        // Slides
        slides: $(),
        slidesGrid: [],
        snapGrid: [],
        slidesSizesGrid: [],
        // isDirection
        isHorizontal: function isHorizontal() {
          return swiper.params.direction === "horizontal";
        },
        isVertical: function isVertical() {
          return swiper.params.direction === "vertical";
        },
        // RTL
        rtl: el.dir.toLowerCase() === "rtl" || $el.css("direction") === "rtl",
        rtlTranslate:
          swiper.params.direction === "horizontal" &&
          (el.dir.toLowerCase() === "rtl" || $el.css("direction") === "rtl"),
        wrongRTL: $wrapperEl.css("display") === "-webkit-box",
        // Indexes
        activeIndex: 0,
        realIndex: 0,
        //
        isBeginning: true,
        isEnd: false,
        // Props
        translate: 0,
        previousTranslate: 0,
        progress: 0,
        velocity: 0,
        animating: false,
        // Locks
        allowSlideNext: swiper.params.allowSlideNext,
        allowSlidePrev: swiper.params.allowSlidePrev,
        // Touch Events
        touchEvents: (function touchEvents() {
          var touch = ["touchstart", "touchmove", "touchend"];
          var desktop = ["mousedown", "mousemove", "mouseup"];

          if (Support.pointerEvents) {
            desktop = ["pointerdown", "pointermove", "pointerup"];
          } else if (Support.prefixedPointerEvents) {
            desktop = ["MSPointerDown", "MSPointerMove", "MSPointerUp"];
          }

          swiper.touchEventsTouch = {
            start: touch[0],
            move: touch[1],
            end: touch[2],
          };
          swiper.touchEventsDesktop = {
            start: desktop[0],
            move: desktop[1],
            end: desktop[2],
          };
          return Support.touch || !swiper.params.simulateTouch
            ? swiper.touchEventsTouch
            : swiper.touchEventsDesktop;
        })(),
        touchEventsData: {
          isTouched: undefined,
          isMoved: undefined,
          allowTouchCallbacks: undefined,
          touchStartTime: undefined,
          isScrolling: undefined,
          currentTranslate: undefined,
          startTranslate: undefined,
          allowThresholdMove: undefined,
          // Form elements to match
          formElements: "input, select, option, textarea, button, video",
          // Last click time
          lastClickTime: Utils.now(),
          clickTimeout: undefined,
          // Velocities
          velocities: [],
          allowMomentumBounce: undefined,
          isTouchEvent: undefined,
          startMoving: undefined,
        },
        // Clicks
        allowClick: true,
        // Touches
        allowTouchMove: swiper.params.allowTouchMove,
        touches: {
          startX: 0,
          startY: 0,
          currentX: 0,
          currentY: 0,
          diff: 0,
        },
        // Images
        imagesToLoad: [],
        imagesLoaded: 0,
      }); // Install Modules

      swiper.useModules(); // Init

      if (swiper.params.init) {
        swiper.init();
      } // Return app instance

      return swiper;
    }

    if (SwiperClass) Swiper.__proto__ = SwiperClass;
    Swiper.prototype = Object.create(SwiperClass && SwiperClass.prototype);
    Swiper.prototype.constructor = Swiper;
    var staticAccessors = {
      extendedDefaults: {
        configurable: true,
      },
      defaults: {
        configurable: true,
      },
      Class: {
        configurable: true,
      },
      $: {
        configurable: true,
      },
    };

    Swiper.prototype.slidesPerViewDynamic = function slidesPerViewDynamic() {
      var swiper = this;
      var params = swiper.params;
      var slides = swiper.slides;
      var slidesGrid = swiper.slidesGrid;
      var swiperSize = swiper.size;
      var activeIndex = swiper.activeIndex;
      var spv = 1;

      if (params.centeredSlides) {
        var slideSize = slides[activeIndex].swiperSlideSize;
        var breakLoop;

        for (var i = activeIndex + 1; i < slides.length; i += 1) {
          if (slides[i] && !breakLoop) {
            slideSize += slides[i].swiperSlideSize;
            spv += 1;

            if (slideSize > swiperSize) {
              breakLoop = true;
            }
          }
        }

        for (var i$1 = activeIndex - 1; i$1 >= 0; i$1 -= 1) {
          if (slides[i$1] && !breakLoop) {
            slideSize += slides[i$1].swiperSlideSize;
            spv += 1;

            if (slideSize > swiperSize) {
              breakLoop = true;
            }
          }
        }
      } else {
        for (var i$2 = activeIndex + 1; i$2 < slides.length; i$2 += 1) {
          if (slidesGrid[i$2] - slidesGrid[activeIndex] < swiperSize) {
            spv += 1;
          }
        }
      }

      return spv;
    };

    Swiper.prototype.update = function update() {
      var swiper = this;

      if (!swiper || swiper.destroyed) {
        return;
      }

      var snapGrid = swiper.snapGrid;
      var params = swiper.params; // Breakpoints

      if (params.breakpoints) {
        swiper.setBreakpoint();
      }

      swiper.updateSize();
      swiper.updateSlides();
      swiper.updateProgress();
      swiper.updateSlidesClasses();

      function setTranslate() {
        var translateValue = swiper.rtlTranslate
          ? swiper.translate * -1
          : swiper.translate;
        var newTranslate = Math.min(
          Math.max(translateValue, swiper.maxTranslate()),
          swiper.minTranslate()
        );
        swiper.setTranslate(newTranslate);
        swiper.updateActiveIndex();
        swiper.updateSlidesClasses();
      }

      var translated;

      if (swiper.params.freeMode) {
        setTranslate();

        if (swiper.params.autoHeight) {
          swiper.updateAutoHeight();
        }
      } else {
        if (
          (swiper.params.slidesPerView === "auto" ||
            swiper.params.slidesPerView > 1) &&
          swiper.isEnd &&
          !swiper.params.centeredSlides
        ) {
          translated = swiper.slideTo(swiper.slides.length - 1, 0, false, true);
        } else {
          translated = swiper.slideTo(swiper.activeIndex, 0, false, true);
        }

        if (!translated) {
          setTranslate();
        }
      }

      if (params.watchOverflow && snapGrid !== swiper.snapGrid) {
        swiper.checkOverflow();
      }

      swiper.emit("update");
    };

    Swiper.prototype.changeDirection = function changeDirection(
      newDirection,
      needUpdate
    ) {
      if (needUpdate === void 0) needUpdate = true;
      var swiper = this;
      var currentDirection = swiper.params.direction;

      if (!newDirection) {
        // eslint-disable-next-line
        newDirection =
          currentDirection === "horizontal" ? "vertical" : "horizontal";
      }

      if (
        newDirection === currentDirection ||
        (newDirection !== "horizontal" && newDirection !== "vertical")
      ) {
        return swiper;
      }

      swiper.$el
        .removeClass(
          "" +
            swiper.params.containerModifierClass +
            currentDirection +
            " wp8-" +
            currentDirection
        )
        .addClass("" + swiper.params.containerModifierClass + newDirection);

      if (
        (Browser.isIE || Browser.isEdge) &&
        (Support.pointerEvents || Support.prefixedPointerEvents)
      ) {
        swiper.$el.addClass(
          swiper.params.containerModifierClass + "wp8-" + newDirection
        );
      }

      swiper.params.direction = newDirection;
      swiper.slides.each(function (slideIndex, slideEl) {
        if (newDirection === "vertical") {
          slideEl.style.width = "";
        } else {
          slideEl.style.height = "";
        }
      });
      swiper.emit("changeDirection");

      if (needUpdate) {
        swiper.update();
      }

      return swiper;
    };

    Swiper.prototype.init = function init() {
      var swiper = this;

      if (swiper.initialized) {
        return;
      }

      swiper.emit("beforeInit"); // Set breakpoint

      if (swiper.params.breakpoints) {
        swiper.setBreakpoint();
      } // Add Classes

      swiper.addClasses(); // Create loop

      if (swiper.params.loop) {
        swiper.loopCreate();
      } // Update size

      swiper.updateSize(); // Update slides

      swiper.updateSlides();

      if (swiper.params.watchOverflow) {
        swiper.checkOverflow();
      } // Set Grab Cursor

      if (swiper.params.grabCursor) {
        swiper.setGrabCursor();
      }

      if (swiper.params.preloadImages) {
        swiper.preloadImages();
      } // Slide To Initial Slide

      if (swiper.params.loop) {
        swiper.slideTo(
          swiper.params.initialSlide + swiper.loopedSlides,
          0,
          swiper.params.runCallbacksOnInit
        );
      } else {
        swiper.slideTo(
          swiper.params.initialSlide,
          0,
          swiper.params.runCallbacksOnInit
        );
      } // Attach events

      swiper.attachEvents(); // Init Flag

      swiper.initialized = true; // Emit

      swiper.emit("init");
    };

    Swiper.prototype.destroy = function destroy(deleteInstance, cleanStyles) {
      if (deleteInstance === void 0) deleteInstance = true;
      if (cleanStyles === void 0) cleanStyles = true;
      var swiper = this;
      var params = swiper.params;
      var $el = swiper.$el;
      var $wrapperEl = swiper.$wrapperEl;
      var slides = swiper.slides;

      if (typeof swiper.params === "undefined" || swiper.destroyed) {
        return null;
      }

      swiper.emit("beforeDestroy"); // Init Flag

      swiper.initialized = false; // Detach events

      swiper.detachEvents(); // Destroy loop

      if (params.loop) {
        swiper.loopDestroy();
      } // Cleanup styles

      if (cleanStyles) {
        swiper.removeClasses();
        $el.removeAttr("style");
        $wrapperEl.removeAttr("style");

        if (slides && slides.length) {
          slides
            .removeClass(
              [
                params.slideVisibleClass,
                params.slideActiveClass,
                params.slideNextClass,
                params.slidePrevClass,
              ].join(" ")
            )
            .removeAttr("style")
            .removeAttr("data-swiper-slide-index")
            .removeAttr("data-swiper-column")
            .removeAttr("data-swiper-row");
        }
      }

      swiper.emit("destroy"); // Detach emitter events

      Object.keys(swiper.eventsListeners).forEach(function (eventName) {
        swiper.off(eventName);
      });

      if (deleteInstance !== false) {
        swiper.$el[0].swiper = null;
        swiper.$el.data("swiper", null);
        Utils.deleteProps(swiper);
      }

      swiper.destroyed = true;
      return null;
    };

    Swiper.extendDefaults = function extendDefaults(newDefaults) {
      Utils.extend(extendedDefaults, newDefaults);
    };

    staticAccessors.extendedDefaults.get = function () {
      return extendedDefaults;
    };

    staticAccessors.defaults.get = function () {
      return defaults;
    };

    staticAccessors.Class.get = function () {
      return SwiperClass;
    };

    staticAccessors.$.get = function () {
      return $;
    };

    Object.defineProperties(Swiper, staticAccessors);
    return Swiper;
  })(SwiperClass);

  var Device$1 = {
    name: "device",
    proto: {
      device: Device,
    },
    static: {
      device: Device,
    },
  };
  var Support$1 = {
    name: "support",
    proto: {
      support: Support,
    },
    static: {
      support: Support,
    },
  };
  var Browser$1 = {
    name: "browser",
    proto: {
      browser: Browser,
    },
    static: {
      browser: Browser,
    },
  };
  var Resize = {
    name: "resize",
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        resize: {
          resizeHandler: function resizeHandler() {
            if (!swiper || swiper.destroyed || !swiper.initialized) {
              return;
            }

            swiper.emit("beforeResize");
            swiper.emit("resize");
          },
          orientationChangeHandler: function orientationChangeHandler() {
            if (!swiper || swiper.destroyed || !swiper.initialized) {
              return;
            }

            swiper.emit("orientationchange");
          },
        },
      });
    },
    on: {
      init: function init() {
        var swiper = this; // Emit resize

        win.addEventListener("resize", swiper.resize.resizeHandler); // Emit orientationchange

        win.addEventListener(
          "orientationchange",
          swiper.resize.orientationChangeHandler
        );
      },
      destroy: function destroy() {
        var swiper = this;
        win.removeEventListener("resize", swiper.resize.resizeHandler);
        win.removeEventListener(
          "orientationchange",
          swiper.resize.orientationChangeHandler
        );
      },
    },
  };
  var Observer = {
    func: win.MutationObserver || win.WebkitMutationObserver,
    attach: function attach(target, options) {
      if (options === void 0) options = {};
      var swiper = this;
      var ObserverFunc = Observer.func;
      var observer = new ObserverFunc(function (mutations) {
        // The observerUpdate event should only be triggered
        // once despite the number of mutations.  Additional
        // triggers are redundant and are very costly
        if (mutations.length === 1) {
          swiper.emit("observerUpdate", mutations[0]);
          return;
        }

        var observerUpdate = function observerUpdate() {
          swiper.emit("observerUpdate", mutations[0]);
        };

        if (win.requestAnimationFrame) {
          win.requestAnimationFrame(observerUpdate);
        } else {
          win.setTimeout(observerUpdate, 0);
        }
      });
      observer.observe(target, {
        attributes:
          typeof options.attributes === "undefined" ? true : options.attributes,
        childList:
          typeof options.childList === "undefined" ? true : options.childList,
        characterData:
          typeof options.characterData === "undefined"
            ? true
            : options.characterData,
      });
      swiper.observer.observers.push(observer);
    },
    init: function init() {
      var swiper = this;

      if (!Support.observer || !swiper.params.observer) {
        return;
      }

      if (swiper.params.observeParents) {
        var containerParents = swiper.$el.parents();

        for (var i = 0; i < containerParents.length; i += 1) {
          swiper.observer.attach(containerParents[i]);
        }
      } // Observe container

      swiper.observer.attach(swiper.$el[0], {
        childList: swiper.params.observeSlideChildren,
      }); // Observe wrapper

      swiper.observer.attach(swiper.$wrapperEl[0], {
        attributes: false,
      });
    },
    destroy: function destroy() {
      var swiper = this;
      swiper.observer.observers.forEach(function (observer) {
        observer.disconnect();
      });
      swiper.observer.observers = [];
    },
  };
  var Observer$1 = {
    name: "observer",
    params: {
      observer: false,
      observeParents: false,
      observeSlideChildren: false,
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        observer: {
          init: Observer.init.bind(swiper),
          attach: Observer.attach.bind(swiper),
          destroy: Observer.destroy.bind(swiper),
          observers: [],
        },
      });
    },
    on: {
      init: function init() {
        var swiper = this;
        swiper.observer.init();
      },
      destroy: function destroy() {
        var swiper = this;
        swiper.observer.destroy();
      },
    },
  };
  var Virtual = {
    update: function update(force) {
      var swiper = this;
      var ref = swiper.params;
      var slidesPerView = ref.slidesPerView;
      var slidesPerGroup = ref.slidesPerGroup;
      var centeredSlides = ref.centeredSlides;
      var ref$1 = swiper.params.virtual;
      var addSlidesBefore = ref$1.addSlidesBefore;
      var addSlidesAfter = ref$1.addSlidesAfter;
      var ref$2 = swiper.virtual;
      var previousFrom = ref$2.from;
      var previousTo = ref$2.to;
      var slides = ref$2.slides;
      var previousSlidesGrid = ref$2.slidesGrid;
      var renderSlide = ref$2.renderSlide;
      var previousOffset = ref$2.offset;
      swiper.updateActiveIndex();
      var activeIndex = swiper.activeIndex || 0;
      var offsetProp;

      if (swiper.rtlTranslate) {
        offsetProp = "right";
      } else {
        offsetProp = swiper.isHorizontal() ? "left" : "top";
      }

      var slidesAfter;
      var slidesBefore;

      if (centeredSlides) {
        slidesAfter =
          Math.floor(slidesPerView / 2) + slidesPerGroup + addSlidesBefore;
        slidesBefore =
          Math.floor(slidesPerView / 2) + slidesPerGroup + addSlidesAfter;
      } else {
        slidesAfter = slidesPerView + (slidesPerGroup - 1) + addSlidesBefore;
        slidesBefore = slidesPerGroup + addSlidesAfter;
      }

      var from = Math.max((activeIndex || 0) - slidesBefore, 0);
      var to = Math.min((activeIndex || 0) + slidesAfter, slides.length - 1);
      var offset = (swiper.slidesGrid[from] || 0) - (swiper.slidesGrid[0] || 0);
      Utils.extend(swiper.virtual, {
        from: from,
        to: to,
        offset: offset,
        slidesGrid: swiper.slidesGrid,
      });

      function onRendered() {
        swiper.updateSlides();
        swiper.updateProgress();
        swiper.updateSlidesClasses();

        if (swiper.lazy && swiper.params.lazy.enabled) {
          swiper.lazy.load();
        }
      }

      if (previousFrom === from && previousTo === to && !force) {
        if (
          swiper.slidesGrid !== previousSlidesGrid &&
          offset !== previousOffset
        ) {
          swiper.slides.css(offsetProp, offset + "px");
        }

        swiper.updateProgress();
        return;
      }

      if (swiper.params.virtual.renderExternal) {
        swiper.params.virtual.renderExternal.call(swiper, {
          offset: offset,
          from: from,
          to: to,
          slides: (function getSlides() {
            var slidesToRender = [];

            for (var i = from; i <= to; i += 1) {
              slidesToRender.push(slides[i]);
            }

            return slidesToRender;
          })(),
        });
        onRendered();
        return;
      }

      var prependIndexes = [];
      var appendIndexes = [];

      if (force) {
        swiper.$wrapperEl.find("." + swiper.params.slideClass).remove();
      } else {
        for (var i = previousFrom; i <= previousTo; i += 1) {
          if (i < from || i > to) {
            swiper.$wrapperEl
              .find(
                "." +
                  swiper.params.slideClass +
                  '[data-swiper-slide-index="' +
                  i +
                  '"]'
              )
              .remove();
          }
        }
      }

      for (var i$1 = 0; i$1 < slides.length; i$1 += 1) {
        if (i$1 >= from && i$1 <= to) {
          if (typeof previousTo === "undefined" || force) {
            appendIndexes.push(i$1);
          } else {
            if (i$1 > previousTo) {
              appendIndexes.push(i$1);
            }

            if (i$1 < previousFrom) {
              prependIndexes.push(i$1);
            }
          }
        }
      }

      appendIndexes.forEach(function (index) {
        swiper.$wrapperEl.append(renderSlide(slides[index], index));
      });
      prependIndexes
        .sort(function (a, b) {
          return b - a;
        })
        .forEach(function (index) {
          swiper.$wrapperEl.prepend(renderSlide(slides[index], index));
        });
      swiper.$wrapperEl
        .children(".swiper-slide")
        .css(offsetProp, offset + "px");
      onRendered();
    },
    renderSlide: function renderSlide(slide, index) {
      var swiper = this;
      var params = swiper.params.virtual;

      if (params.cache && swiper.virtual.cache[index]) {
        return swiper.virtual.cache[index];
      }

      var $slideEl = params.renderSlide
        ? $(params.renderSlide.call(swiper, slide, index))
        : $(
            '<div class="' +
              swiper.params.slideClass +
              '" data-swiper-slide-index="' +
              index +
              '">' +
              slide +
              "</div>"
          );

      if (!$slideEl.attr("data-swiper-slide-index")) {
        $slideEl.attr("data-swiper-slide-index", index);
      }

      if (params.cache) {
        swiper.virtual.cache[index] = $slideEl;
      }

      return $slideEl;
    },
    appendSlide: function appendSlide(slides) {
      var swiper = this;

      if (_typeof(slides) === "object" && "length" in slides) {
        for (var i = 0; i < slides.length; i += 1) {
          if (slides[i]) {
            swiper.virtual.slides.push(slides[i]);
          }
        }
      } else {
        swiper.virtual.slides.push(slides);
      }

      swiper.virtual.update(true);
    },
    prependSlide: function prependSlide(slides) {
      var swiper = this;
      var activeIndex = swiper.activeIndex;
      var newActiveIndex = activeIndex + 1;
      var numberOfNewSlides = 1;

      if (Array.isArray(slides)) {
        for (var i = 0; i < slides.length; i += 1) {
          if (slides[i]) {
            swiper.virtual.slides.unshift(slides[i]);
          }
        }

        newActiveIndex = activeIndex + slides.length;
        numberOfNewSlides = slides.length;
      } else {
        swiper.virtual.slides.unshift(slides);
      }

      if (swiper.params.virtual.cache) {
        var cache = swiper.virtual.cache;
        var newCache = {};
        Object.keys(cache).forEach(function (cachedIndex) {
          newCache[parseInt(cachedIndex, 10) + numberOfNewSlides] =
            cache[cachedIndex];
        });
        swiper.virtual.cache = newCache;
      }

      swiper.virtual.update(true);
      swiper.slideTo(newActiveIndex, 0);
    },
    removeSlide: function removeSlide(slidesIndexes) {
      var swiper = this;

      if (typeof slidesIndexes === "undefined" || slidesIndexes === null) {
        return;
      }

      var activeIndex = swiper.activeIndex;

      if (Array.isArray(slidesIndexes)) {
        for (var i = slidesIndexes.length - 1; i >= 0; i -= 1) {
          swiper.virtual.slides.splice(slidesIndexes[i], 1);

          if (swiper.params.virtual.cache) {
            delete swiper.virtual.cache[slidesIndexes[i]];
          }

          if (slidesIndexes[i] < activeIndex) {
            activeIndex -= 1;
          }

          activeIndex = Math.max(activeIndex, 0);
        }
      } else {
        swiper.virtual.slides.splice(slidesIndexes, 1);

        if (swiper.params.virtual.cache) {
          delete swiper.virtual.cache[slidesIndexes];
        }

        if (slidesIndexes < activeIndex) {
          activeIndex -= 1;
        }

        activeIndex = Math.max(activeIndex, 0);
      }

      swiper.virtual.update(true);
      swiper.slideTo(activeIndex, 0);
    },
    removeAllSlides: function removeAllSlides() {
      var swiper = this;
      swiper.virtual.slides = [];

      if (swiper.params.virtual.cache) {
        swiper.virtual.cache = {};
      }

      swiper.virtual.update(true);
      swiper.slideTo(0, 0);
    },
  };
  var Virtual$1 = {
    name: "virtual",
    params: {
      virtual: {
        enabled: false,
        slides: [],
        cache: true,
        renderSlide: null,
        renderExternal: null,
        addSlidesBefore: 0,
        addSlidesAfter: 0,
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        virtual: {
          update: Virtual.update.bind(swiper),
          appendSlide: Virtual.appendSlide.bind(swiper),
          prependSlide: Virtual.prependSlide.bind(swiper),
          removeSlide: Virtual.removeSlide.bind(swiper),
          removeAllSlides: Virtual.removeAllSlides.bind(swiper),
          renderSlide: Virtual.renderSlide.bind(swiper),
          slides: swiper.params.virtual.slides,
          cache: {},
        },
      });
    },
    on: {
      beforeInit: function beforeInit() {
        var swiper = this;

        if (!swiper.params.virtual.enabled) {
          return;
        }

        swiper.classNames.push(
          swiper.params.containerModifierClass + "virtual"
        );
        var overwriteParams = {
          watchSlidesProgress: true,
        };
        Utils.extend(swiper.params, overwriteParams);
        Utils.extend(swiper.originalParams, overwriteParams);

        if (!swiper.params.initialSlide) {
          swiper.virtual.update();
        }
      },
      setTranslate: function setTranslate() {
        var swiper = this;

        if (!swiper.params.virtual.enabled) {
          return;
        }

        swiper.virtual.update();
      },
    },
  };
  var Keyboard = {
    handle: function handle(event) {
      var swiper = this;
      var rtl = swiper.rtlTranslate;
      var e = event;

      if (e.originalEvent) {
        e = e.originalEvent;
      } // jquery fix

      var kc = e.keyCode || e.charCode; // Directions locks

      if (
        !swiper.allowSlideNext &&
        ((swiper.isHorizontal() && kc === 39) ||
          (swiper.isVertical() && kc === 40) ||
          kc === 34)
      ) {
        return false;
      }

      if (
        !swiper.allowSlidePrev &&
        ((swiper.isHorizontal() && kc === 37) ||
          (swiper.isVertical() && kc === 38) ||
          kc === 33)
      ) {
        return false;
      }

      if (e.shiftKey || e.altKey || e.ctrlKey || e.metaKey) {
        return undefined;
      }

      if (
        doc.activeElement &&
        doc.activeElement.nodeName &&
        (doc.activeElement.nodeName.toLowerCase() === "input" ||
          doc.activeElement.nodeName.toLowerCase() === "textarea")
      ) {
        return undefined;
      }

      if (
        swiper.params.keyboard.onlyInViewport &&
        (kc === 33 ||
          kc === 34 ||
          kc === 37 ||
          kc === 39 ||
          kc === 38 ||
          kc === 40)
      ) {
        var inView = false; // Check that swiper should be inside of visible area of window

        if (
          swiper.$el.parents("." + swiper.params.slideClass).length > 0 &&
          swiper.$el.parents("." + swiper.params.slideActiveClass).length === 0
        ) {
          return undefined;
        }

        var windowWidth = win.innerWidth;
        var windowHeight = win.innerHeight;
        var swiperOffset = swiper.$el.offset();

        if (rtl) {
          swiperOffset.left -= swiper.$el[0].scrollLeft;
        }

        var swiperCoord = [
          [swiperOffset.left, swiperOffset.top],
          [swiperOffset.left + swiper.width, swiperOffset.top],
          [swiperOffset.left, swiperOffset.top + swiper.height],
          [swiperOffset.left + swiper.width, swiperOffset.top + swiper.height],
        ];

        for (var i = 0; i < swiperCoord.length; i += 1) {
          var point = swiperCoord[i];

          if (
            point[0] >= 0 &&
            point[0] <= windowWidth &&
            point[1] >= 0 &&
            point[1] <= windowHeight
          ) {
            inView = true;
          }
        }

        if (!inView) {
          return undefined;
        }
      }

      if (swiper.isHorizontal()) {
        if (kc === 33 || kc === 34 || kc === 37 || kc === 39) {
          if (e.preventDefault) {
            e.preventDefault();
          } else {
            e.returnValue = false;
          }
        }

        if (
          ((kc === 34 || kc === 39) && !rtl) ||
          ((kc === 33 || kc === 37) && rtl)
        ) {
          swiper.slideNext();
        }

        if (
          ((kc === 33 || kc === 37) && !rtl) ||
          ((kc === 34 || kc === 39) && rtl)
        ) {
          swiper.slidePrev();
        }
      } else {
        if (kc === 33 || kc === 34 || kc === 38 || kc === 40) {
          if (e.preventDefault) {
            e.preventDefault();
          } else {
            e.returnValue = false;
          }
        }

        if (kc === 34 || kc === 40) {
          swiper.slideNext();
        }

        if (kc === 33 || kc === 38) {
          swiper.slidePrev();
        }
      }

      swiper.emit("keyPress", kc);
      return undefined;
    },
    enable: function enable() {
      var swiper = this;

      if (swiper.keyboard.enabled) {
        return;
      }

      $(doc).on("keydown", swiper.keyboard.handle);
      swiper.keyboard.enabled = true;
    },
    disable: function disable() {
      var swiper = this;

      if (!swiper.keyboard.enabled) {
        return;
      }

      $(doc).off("keydown", swiper.keyboard.handle);
      swiper.keyboard.enabled = false;
    },
  };
  var Keyboard$1 = {
    name: "keyboard",
    params: {
      keyboard: {
        enabled: false,
        onlyInViewport: true,
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        keyboard: {
          enabled: false,
          enable: Keyboard.enable.bind(swiper),
          disable: Keyboard.disable.bind(swiper),
          handle: Keyboard.handle.bind(swiper),
        },
      });
    },
    on: {
      init: function init() {
        var swiper = this;

        if (swiper.params.keyboard.enabled) {
          swiper.keyboard.enable();
        }
      },
      destroy: function destroy() {
        var swiper = this;

        if (swiper.keyboard.enabled) {
          swiper.keyboard.disable();
        }
      },
    },
  };

  function isEventSupported() {
    var eventName = "onwheel";
    var isSupported = eventName in doc;

    if (!isSupported) {
      var element = doc.createElement("div");
      element.setAttribute(eventName, "return;");
      isSupported = typeof element[eventName] === "function";
    }

    if (
      !isSupported &&
      doc.implementation &&
      doc.implementation.hasFeature && // always returns true in newer browsers as per the standard.
      // @see http://dom.spec.whatwg.org/#dom-domimplementation-hasfeature
      doc.implementation.hasFeature("", "") !== true
    ) {
      // This is the only way to test support for the `wheel` event in IE9+.
      isSupported = doc.implementation.hasFeature("Events.wheel", "3.0");
    }

    return isSupported;
  }

  var Mousewheel = {
    lastScrollTime: Utils.now(),
    event: (function getEvent() {
      if (win.navigator.userAgent.indexOf("firefox") > -1) {
        return "DOMMouseScroll";
      }

      return isEventSupported() ? "wheel" : "mousewheel";
    })(),
    normalize: function normalize(e) {
      // Reasonable defaults
      var PIXEL_STEP = 10;
      var LINE_HEIGHT = 40;
      var PAGE_HEIGHT = 800;
      var sX = 0;
      var sY = 0; // spinX, spinY

      var pX = 0;
      var pY = 0; // pixelX, pixelY
      // Legacy

      if ("detail" in e) {
        sY = e.detail;
      }

      if ("wheelDelta" in e) {
        sY = -e.wheelDelta / 120;
      }

      if ("wheelDeltaY" in e) {
        sY = -e.wheelDeltaY / 120;
      }

      if ("wheelDeltaX" in e) {
        sX = -e.wheelDeltaX / 120;
      } // side scrolling on FF with DOMMouseScroll

      if ("axis" in e && e.axis === e.HORIZONTAL_AXIS) {
        sX = sY;
        sY = 0;
      }

      pX = sX * PIXEL_STEP;
      pY = sY * PIXEL_STEP;

      if ("deltaY" in e) {
        pY = e.deltaY;
      }

      if ("deltaX" in e) {
        pX = e.deltaX;
      }

      if ((pX || pY) && e.deltaMode) {
        if (e.deltaMode === 1) {
          // delta in LINE units
          pX *= LINE_HEIGHT;
          pY *= LINE_HEIGHT;
        } else {
          // delta in PAGE units
          pX *= PAGE_HEIGHT;
          pY *= PAGE_HEIGHT;
        }
      } // Fall-back if spin cannot be determined

      if (pX && !sX) {
        sX = pX < 1 ? -1 : 1;
      }

      if (pY && !sY) {
        sY = pY < 1 ? -1 : 1;
      }

      return {
        spinX: sX,
        spinY: sY,
        pixelX: pX,
        pixelY: pY,
      };
    },
    handleMouseEnter: function handleMouseEnter() {
      var swiper = this;
      swiper.mouseEntered = true;
    },
    handleMouseLeave: function handleMouseLeave() {
      var swiper = this;
      swiper.mouseEntered = false;
    },
    handle: function handle(event) {
      var e = event;
      var swiper = this;
      var params = swiper.params.mousewheel;

      if (!swiper.mouseEntered && !params.releaseOnEdges) {
        return true;
      }

      if (e.originalEvent) {
        e = e.originalEvent;
      } // jquery fix

      var delta = 0;
      var rtlFactor = swiper.rtlTranslate ? -1 : 1;
      var data = Mousewheel.normalize(e);

      if (params.forceToAxis) {
        if (swiper.isHorizontal()) {
          if (Math.abs(data.pixelX) > Math.abs(data.pixelY)) {
            delta = data.pixelX * rtlFactor;
          } else {
            return true;
          }
        } else if (Math.abs(data.pixelY) > Math.abs(data.pixelX)) {
          delta = data.pixelY;
        } else {
          return true;
        }
      } else {
        delta =
          Math.abs(data.pixelX) > Math.abs(data.pixelY)
            ? -data.pixelX * rtlFactor
            : -data.pixelY;
      }

      if (delta === 0) {
        return true;
      }

      if (params.invert) {
        delta = -delta;
      }

      if (!swiper.params.freeMode) {
        if (Utils.now() - swiper.mousewheel.lastScrollTime > 60) {
          if (delta < 0) {
            if ((!swiper.isEnd || swiper.params.loop) && !swiper.animating) {
              swiper.slideNext();
              swiper.emit("scroll", e);
            } else if (params.releaseOnEdges) {
              return true;
            }
          } else if (
            (!swiper.isBeginning || swiper.params.loop) &&
            !swiper.animating
          ) {
            swiper.slidePrev();
            swiper.emit("scroll", e);
          } else if (params.releaseOnEdges) {
            return true;
          }
        }

        swiper.mousewheel.lastScrollTime = new win.Date().getTime();
      } else {
        // Freemode or scrollContainer:
        if (swiper.params.loop) {
          swiper.loopFix();
        }

        var position = swiper.getTranslate() + delta * params.sensitivity;
        var wasBeginning = swiper.isBeginning;
        var wasEnd = swiper.isEnd;

        if (position >= swiper.minTranslate()) {
          position = swiper.minTranslate();
        }

        if (position <= swiper.maxTranslate()) {
          position = swiper.maxTranslate();
        }

        swiper.setTransition(0);
        swiper.setTranslate(position);
        swiper.updateProgress();
        swiper.updateActiveIndex();
        swiper.updateSlidesClasses();

        if (
          (!wasBeginning && swiper.isBeginning) ||
          (!wasEnd && swiper.isEnd)
        ) {
          swiper.updateSlidesClasses();
        }

        if (swiper.params.freeModeSticky) {
          clearTimeout(swiper.mousewheel.timeout);
          swiper.mousewheel.timeout = Utils.nextTick(function () {
            swiper.slideToClosest();
          }, 300);
        } // Emit event

        swiper.emit("scroll", e); // Stop autoplay

        if (
          swiper.params.autoplay &&
          swiper.params.autoplayDisableOnInteraction
        ) {
          swiper.autoplay.stop();
        } // Return page scroll on edge positions

        if (
          position === swiper.minTranslate() ||
          position === swiper.maxTranslate()
        ) {
          return true;
        }
      }

      if (e.preventDefault) {
        e.preventDefault();
      } else {
        e.returnValue = false;
      }

      return false;
    },
    enable: function enable() {
      var swiper = this;

      if (!Mousewheel.event) {
        return false;
      }

      if (swiper.mousewheel.enabled) {
        return false;
      }

      var target = swiper.$el;

      if (swiper.params.mousewheel.eventsTarged !== "container") {
        target = $(swiper.params.mousewheel.eventsTarged);
      }

      target.on("mouseenter", swiper.mousewheel.handleMouseEnter);
      target.on("mouseleave", swiper.mousewheel.handleMouseLeave);
      target.on(Mousewheel.event, swiper.mousewheel.handle);
      swiper.mousewheel.enabled = true;
      return true;
    },
    disable: function disable() {
      var swiper = this;

      if (!Mousewheel.event) {
        return false;
      }

      if (!swiper.mousewheel.enabled) {
        return false;
      }

      var target = swiper.$el;

      if (swiper.params.mousewheel.eventsTarged !== "container") {
        target = $(swiper.params.mousewheel.eventsTarged);
      }

      target.off(Mousewheel.event, swiper.mousewheel.handle);
      swiper.mousewheel.enabled = false;
      return true;
    },
  };
  var Mousewheel$1 = {
    name: "mousewheel",
    params: {
      mousewheel: {
        enabled: false,
        releaseOnEdges: false,
        invert: false,
        forceToAxis: false,
        sensitivity: 1,
        eventsTarged: "container",
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        mousewheel: {
          enabled: false,
          enable: Mousewheel.enable.bind(swiper),
          disable: Mousewheel.disable.bind(swiper),
          handle: Mousewheel.handle.bind(swiper),
          handleMouseEnter: Mousewheel.handleMouseEnter.bind(swiper),
          handleMouseLeave: Mousewheel.handleMouseLeave.bind(swiper),
          lastScrollTime: Utils.now(),
        },
      });
    },
    on: {
      init: function init() {
        var swiper = this;

        if (swiper.params.mousewheel.enabled) {
          swiper.mousewheel.enable();
        }
      },
      destroy: function destroy() {
        var swiper = this;

        if (swiper.mousewheel.enabled) {
          swiper.mousewheel.disable();
        }
      },
    },
  };
  var Navigation = {
    update: function update() {
      // Update Navigation Buttons
      var swiper = this;
      var params = swiper.params.navigation;

      if (swiper.params.loop) {
        return;
      }

      var ref = swiper.navigation;
      var $nextEl = ref.$nextEl;
      var $prevEl = ref.$prevEl;

      if ($prevEl && $prevEl.length > 0) {
        if (swiper.isBeginning) {
          $prevEl.addClass(params.disabledClass);
        } else {
          $prevEl.removeClass(params.disabledClass);
        }

        $prevEl[
          swiper.params.watchOverflow && swiper.isLocked
            ? "addClass"
            : "removeClass"
        ](params.lockClass);
      }

      if ($nextEl && $nextEl.length > 0) {
        if (swiper.isEnd) {
          $nextEl.addClass(params.disabledClass);
        } else {
          $nextEl.removeClass(params.disabledClass);
        }

        $nextEl[
          swiper.params.watchOverflow && swiper.isLocked
            ? "addClass"
            : "removeClass"
        ](params.lockClass);
      }
    },
    onPrevClick: function onPrevClick(e) {
      var swiper = this;
      e.preventDefault();

      if (swiper.isBeginning && !swiper.params.loop) {
        return;
      }

      swiper.slidePrev();
    },
    onNextClick: function onNextClick(e) {
      var swiper = this;
      e.preventDefault();

      if (swiper.isEnd && !swiper.params.loop) {
        return;
      }

      swiper.slideNext();
    },
    init: function init() {
      var swiper = this;
      var params = swiper.params.navigation;

      if (!(params.nextEl || params.prevEl)) {
        return;
      }

      var $nextEl;
      var $prevEl;

      if (params.nextEl) {
        $nextEl = $(params.nextEl);

        if (
          swiper.params.uniqueNavElements &&
          typeof params.nextEl === "string" &&
          $nextEl.length > 1 &&
          swiper.$el.find(params.nextEl).length === 1
        ) {
          $nextEl = swiper.$el.find(params.nextEl);
        }
      }

      if (params.prevEl) {
        $prevEl = $(params.prevEl);

        if (
          swiper.params.uniqueNavElements &&
          typeof params.prevEl === "string" &&
          $prevEl.length > 1 &&
          swiper.$el.find(params.prevEl).length === 1
        ) {
          $prevEl = swiper.$el.find(params.prevEl);
        }
      }

      if ($nextEl && $nextEl.length > 0) {
        $nextEl.on("click", swiper.navigation.onNextClick);
      }

      if ($prevEl && $prevEl.length > 0) {
        $prevEl.on("click", swiper.navigation.onPrevClick);
      }

      Utils.extend(swiper.navigation, {
        $nextEl: $nextEl,
        nextEl: $nextEl && $nextEl[0],
        $prevEl: $prevEl,
        prevEl: $prevEl && $prevEl[0],
      });
    },
    destroy: function destroy() {
      var swiper = this;
      var ref = swiper.navigation;
      var $nextEl = ref.$nextEl;
      var $prevEl = ref.$prevEl;

      if ($nextEl && $nextEl.length) {
        $nextEl.off("click", swiper.navigation.onNextClick);
        $nextEl.removeClass(swiper.params.navigation.disabledClass);
      }

      if ($prevEl && $prevEl.length) {
        $prevEl.off("click", swiper.navigation.onPrevClick);
        $prevEl.removeClass(swiper.params.navigation.disabledClass);
      }
    },
  };
  var Navigation$1 = {
    name: "navigation",
    params: {
      navigation: {
        nextEl: null,
        prevEl: null,
        hideOnClick: false,
        disabledClass: "swiper-button-disabled",
        hiddenClass: "swiper-button-hidden",
        lockClass: "swiper-button-lock",
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        navigation: {
          init: Navigation.init.bind(swiper),
          update: Navigation.update.bind(swiper),
          destroy: Navigation.destroy.bind(swiper),
          onNextClick: Navigation.onNextClick.bind(swiper),
          onPrevClick: Navigation.onPrevClick.bind(swiper),
        },
      });
    },
    on: {
      init: function init() {
        var swiper = this;
        swiper.navigation.init();
        swiper.navigation.update();
      },
      toEdge: function toEdge() {
        var swiper = this;
        swiper.navigation.update();
      },
      fromEdge: function fromEdge() {
        var swiper = this;
        swiper.navigation.update();
      },
      destroy: function destroy() {
        var swiper = this;
        swiper.navigation.destroy();
      },
      click: function click(e) {
        var swiper = this;
        var ref = swiper.navigation;
        var $nextEl = ref.$nextEl;
        var $prevEl = ref.$prevEl;

        if (
          swiper.params.navigation.hideOnClick &&
          !$(e.target).is($prevEl) &&
          !$(e.target).is($nextEl)
        ) {
          var isHidden;

          if ($nextEl) {
            isHidden = $nextEl.hasClass(swiper.params.navigation.hiddenClass);
          } else if ($prevEl) {
            isHidden = $prevEl.hasClass(swiper.params.navigation.hiddenClass);
          }

          if (isHidden === true) {
            swiper.emit("navigationShow", swiper);
          } else {
            swiper.emit("navigationHide", swiper);
          }

          if ($nextEl) {
            $nextEl.toggleClass(swiper.params.navigation.hiddenClass);
          }

          if ($prevEl) {
            $prevEl.toggleClass(swiper.params.navigation.hiddenClass);
          }
        }
      },
    },
  };
  var Pagination = {
    update: function update() {
      // Render || Update Pagination bullets/items
      var swiper = this;
      var rtl = swiper.rtl;
      var params = swiper.params.pagination;

      if (
        !params.el ||
        !swiper.pagination.el ||
        !swiper.pagination.$el ||
        swiper.pagination.$el.length === 0
      ) {
        return;
      }

      var slidesLength =
        swiper.virtual && swiper.params.virtual.enabled
          ? swiper.virtual.slides.length
          : swiper.slides.length;
      var $el = swiper.pagination.$el; // Current/Total

      var current;
      var total = swiper.params.loop
        ? Math.ceil(
            (slidesLength - swiper.loopedSlides * 2) /
              swiper.params.slidesPerGroup
          )
        : swiper.snapGrid.length;

      if (swiper.params.loop) {
        current = Math.ceil(
          (swiper.activeIndex - swiper.loopedSlides) /
            swiper.params.slidesPerGroup
        );

        if (current > slidesLength - 1 - swiper.loopedSlides * 2) {
          current -= slidesLength - swiper.loopedSlides * 2;
        }

        if (current > total - 1) {
          current -= total;
        }

        if (current < 0 && swiper.params.paginationType !== "bullets") {
          current = total + current;
        }
      } else if (typeof swiper.snapIndex !== "undefined") {
        current = swiper.snapIndex;
      } else {
        current = swiper.activeIndex || 0;
      } // Types

      if (
        params.type === "bullets" &&
        swiper.pagination.bullets &&
        swiper.pagination.bullets.length > 0
      ) {
        var bullets = swiper.pagination.bullets;
        var firstIndex;
        var lastIndex;
        var midIndex;

        if (params.dynamicBullets) {
          swiper.pagination.bulletSize = bullets
            .eq(0)
            [swiper.isHorizontal() ? "outerWidth" : "outerHeight"](true);
          $el.css(
            swiper.isHorizontal() ? "width" : "height",
            swiper.pagination.bulletSize * (params.dynamicMainBullets + 4) +
              "px"
          );

          if (
            params.dynamicMainBullets > 1 &&
            swiper.previousIndex !== undefined
          ) {
            swiper.pagination.dynamicBulletIndex +=
              current - swiper.previousIndex;

            if (
              swiper.pagination.dynamicBulletIndex >
              params.dynamicMainBullets - 1
            ) {
              swiper.pagination.dynamicBulletIndex =
                params.dynamicMainBullets - 1;
            } else if (swiper.pagination.dynamicBulletIndex < 0) {
              swiper.pagination.dynamicBulletIndex = 0;
            }
          }

          firstIndex = current - swiper.pagination.dynamicBulletIndex;
          lastIndex =
            firstIndex +
            (Math.min(bullets.length, params.dynamicMainBullets) - 1);
          midIndex = (lastIndex + firstIndex) / 2;
        }

        bullets.removeClass(
          params.bulletActiveClass +
            " " +
            params.bulletActiveClass +
            "-next " +
            params.bulletActiveClass +
            "-next-next " +
            params.bulletActiveClass +
            "-prev " +
            params.bulletActiveClass +
            "-prev-prev " +
            params.bulletActiveClass +
            "-main"
        );

        if ($el.length > 1) {
          bullets.each(function (index, bullet) {
            var $bullet = $(bullet);
            var bulletIndex = $bullet.index();

            if (bulletIndex === current) {
              $bullet.addClass(params.bulletActiveClass);
            }

            if (params.dynamicBullets) {
              if (bulletIndex >= firstIndex && bulletIndex <= lastIndex) {
                $bullet.addClass(params.bulletActiveClass + "-main");
              }

              if (bulletIndex === firstIndex) {
                $bullet
                  .prev()
                  .addClass(params.bulletActiveClass + "-prev")
                  .prev()
                  .addClass(params.bulletActiveClass + "-prev-prev");
              }

              if (bulletIndex === lastIndex) {
                $bullet
                  .next()
                  .addClass(params.bulletActiveClass + "-next")
                  .next()
                  .addClass(params.bulletActiveClass + "-next-next");
              }
            }
          });
        } else {
          var $bullet = bullets.eq(current);
          $bullet.addClass(params.bulletActiveClass);

          if (params.dynamicBullets) {
            var $firstDisplayedBullet = bullets.eq(firstIndex);
            var $lastDisplayedBullet = bullets.eq(lastIndex);

            for (var i = firstIndex; i <= lastIndex; i += 1) {
              bullets.eq(i).addClass(params.bulletActiveClass + "-main");
            }

            $firstDisplayedBullet
              .prev()
              .addClass(params.bulletActiveClass + "-prev")
              .prev()
              .addClass(params.bulletActiveClass + "-prev-prev");
            $lastDisplayedBullet
              .next()
              .addClass(params.bulletActiveClass + "-next")
              .next()
              .addClass(params.bulletActiveClass + "-next-next");
          }
        }

        if (params.dynamicBullets) {
          var dynamicBulletsLength = Math.min(
            bullets.length,
            params.dynamicMainBullets + 4
          );
          var bulletsOffset =
            (swiper.pagination.bulletSize * dynamicBulletsLength -
              swiper.pagination.bulletSize) /
              2 -
            midIndex * swiper.pagination.bulletSize;
          var offsetProp = rtl ? "right" : "left";
          bullets.css(
            swiper.isHorizontal() ? offsetProp : "top",
            bulletsOffset + "px"
          );
        }
      }

      if (params.type === "fraction") {
        $el
          .find("." + params.currentClass)
          .text(params.formatFractionCurrent(current + 1));
        $el
          .find("." + params.totalClass)
          .text(params.formatFractionTotal(total));
      }

      if (params.type === "progressbar") {
        var progressbarDirection;

        if (params.progressbarOpposite) {
          progressbarDirection = swiper.isHorizontal()
            ? "vertical"
            : "horizontal";
        } else {
          progressbarDirection = swiper.isHorizontal()
            ? "horizontal"
            : "vertical";
        }

        var scale = (current + 1) / total;
        var scaleX = 1;
        var scaleY = 1;

        if (progressbarDirection === "horizontal") {
          scaleX = scale;
        } else {
          scaleY = scale;
        }

        $el
          .find("." + params.progressbarFillClass)
          .transform(
            "translate3d(0,0,0) scaleX(" + scaleX + ") scaleY(" + scaleY + ")"
          )
          .transition(swiper.params.speed);
      }

      if (params.type === "custom" && params.renderCustom) {
        $el.html(params.renderCustom(swiper, current + 1, total));
        swiper.emit("paginationRender", swiper, $el[0]);
      } else {
        swiper.emit("paginationUpdate", swiper, $el[0]);
      }

      $el[
        swiper.params.watchOverflow && swiper.isLocked
          ? "addClass"
          : "removeClass"
      ](params.lockClass);
    },
    render: function render() {
      // Render Container
      var swiper = this;
      var params = swiper.params.pagination;

      if (
        !params.el ||
        !swiper.pagination.el ||
        !swiper.pagination.$el ||
        swiper.pagination.$el.length === 0
      ) {
        return;
      }

      var slidesLength =
        swiper.virtual && swiper.params.virtual.enabled
          ? swiper.virtual.slides.length
          : swiper.slides.length;
      var $el = swiper.pagination.$el;
      var paginationHTML = "";

      if (params.type === "bullets") {
        var numberOfBullets = swiper.params.loop
          ? Math.ceil(
              (slidesLength - swiper.loopedSlides * 2) /
                swiper.params.slidesPerGroup
            )
          : swiper.snapGrid.length;

        for (var i = 0; i < numberOfBullets; i += 1) {
          if (params.renderBullet) {
            paginationHTML += params.renderBullet.call(
              swiper,
              i,
              params.bulletClass
            );
          } else {
            paginationHTML +=
              "<" +
              params.bulletElement +
              ' class="' +
              params.bulletClass +
              '"></' +
              params.bulletElement +
              ">";
          }
        }

        $el.html(paginationHTML);
        swiper.pagination.bullets = $el.find("." + params.bulletClass);
      }

      if (params.type === "fraction") {
        if (params.renderFraction) {
          paginationHTML = params.renderFraction.call(
            swiper,
            params.currentClass,
            params.totalClass
          );
        } else {
          paginationHTML =
            '<span class="' +
            params.currentClass +
            '"></span>' +
            " / " +
            '<span class="' +
            params.totalClass +
            '"></span>';
        }

        $el.html(paginationHTML);
      }

      if (params.type === "progressbar") {
        if (params.renderProgressbar) {
          paginationHTML = params.renderProgressbar.call(
            swiper,
            params.progressbarFillClass
          );
        } else {
          paginationHTML =
            '<span class="' + params.progressbarFillClass + '"></span>';
        }

        $el.html(paginationHTML);
      }

      if (params.type !== "custom") {
        swiper.emit("paginationRender", swiper.pagination.$el[0]);
      }
    },
    init: function init() {
      var swiper = this;
      var params = swiper.params.pagination;

      if (!params.el) {
        return;
      }

      var $el = $(params.el);

      if ($el.length === 0) {
        return;
      }

      if (
        swiper.params.uniqueNavElements &&
        typeof params.el === "string" &&
        $el.length > 1 &&
        swiper.$el.find(params.el).length === 1
      ) {
        $el = swiper.$el.find(params.el);
      }

      if (params.type === "bullets" && params.clickable) {
        $el.addClass(params.clickableClass);
      }

      $el.addClass(params.modifierClass + params.type);

      if (params.type === "bullets" && params.dynamicBullets) {
        $el.addClass("" + params.modifierClass + params.type + "-dynamic");
        swiper.pagination.dynamicBulletIndex = 0;

        if (params.dynamicMainBullets < 1) {
          params.dynamicMainBullets = 1;
        }
      }

      if (params.type === "progressbar" && params.progressbarOpposite) {
        $el.addClass(params.progressbarOppositeClass);
      }

      if (params.clickable) {
        $el.on("click", "." + params.bulletClass, function onClick(e) {
          e.preventDefault();
          var index = $(this).index() * swiper.params.slidesPerGroup;

          if (swiper.params.loop) {
            index += swiper.loopedSlides;
          }

          swiper.slideTo(index);
        });
      }

      Utils.extend(swiper.pagination, {
        $el: $el,
        el: $el[0],
      });
    },
    destroy: function destroy() {
      var swiper = this;
      var params = swiper.params.pagination;

      if (
        !params.el ||
        !swiper.pagination.el ||
        !swiper.pagination.$el ||
        swiper.pagination.$el.length === 0
      ) {
        return;
      }

      var $el = swiper.pagination.$el;
      $el.removeClass(params.hiddenClass);
      $el.removeClass(params.modifierClass + params.type);

      if (swiper.pagination.bullets) {
        swiper.pagination.bullets.removeClass(params.bulletActiveClass);
      }

      if (params.clickable) {
        $el.off("click", "." + params.bulletClass);
      }
    },
  };
  var Pagination$1 = {
    name: "pagination",
    params: {
      pagination: {
        el: null,
        bulletElement: "span",
        clickable: false,
        hideOnClick: false,
        renderBullet: null,
        renderProgressbar: null,
        renderFraction: null,
        renderCustom: null,
        progressbarOpposite: false,
        type: "bullets",
        // 'bullets' or 'progressbar' or 'fraction' or 'custom'
        dynamicBullets: false,
        dynamicMainBullets: 1,
        formatFractionCurrent: function formatFractionCurrent(number) {
          return number;
        },
        formatFractionTotal: function formatFractionTotal(number) {
          return number;
        },
        bulletClass: "swiper-pagination-bullet",
        bulletActiveClass: "swiper-pagination-bullet-active",
        modifierClass: "swiper-pagination-",
        // NEW
        currentClass: "swiper-pagination-current",
        totalClass: "swiper-pagination-total",
        hiddenClass: "swiper-pagination-hidden",
        progressbarFillClass: "swiper-pagination-progressbar-fill",
        progressbarOppositeClass: "swiper-pagination-progressbar-opposite",
        clickableClass: "swiper-pagination-clickable",
        // NEW
        lockClass: "swiper-pagination-lock",
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        pagination: {
          init: Pagination.init.bind(swiper),
          render: Pagination.render.bind(swiper),
          update: Pagination.update.bind(swiper),
          destroy: Pagination.destroy.bind(swiper),
          dynamicBulletIndex: 0,
        },
      });
    },
    on: {
      init: function init() {
        var swiper = this;
        swiper.pagination.init();
        swiper.pagination.render();
        swiper.pagination.update();
      },
      activeIndexChange: function activeIndexChange() {
        var swiper = this;

        if (swiper.params.loop) {
          swiper.pagination.update();
        } else if (typeof swiper.snapIndex === "undefined") {
          swiper.pagination.update();
        }
      },
      snapIndexChange: function snapIndexChange() {
        var swiper = this;

        if (!swiper.params.loop) {
          swiper.pagination.update();
        }
      },
      slidesLengthChange: function slidesLengthChange() {
        var swiper = this;

        if (swiper.params.loop) {
          swiper.pagination.render();
          swiper.pagination.update();
        }
      },
      snapGridLengthChange: function snapGridLengthChange() {
        var swiper = this;

        if (!swiper.params.loop) {
          swiper.pagination.render();
          swiper.pagination.update();
        }
      },
      destroy: function destroy() {
        var swiper = this;
        swiper.pagination.destroy();
      },
      click: function click(e) {
        var swiper = this;

        if (
          swiper.params.pagination.el &&
          swiper.params.pagination.hideOnClick &&
          swiper.pagination.$el.length > 0 &&
          !$(e.target).hasClass(swiper.params.pagination.bulletClass)
        ) {
          var isHidden = swiper.pagination.$el.hasClass(
            swiper.params.pagination.hiddenClass
          );

          if (isHidden === true) {
            swiper.emit("paginationShow", swiper);
          } else {
            swiper.emit("paginationHide", swiper);
          }

          swiper.pagination.$el.toggleClass(
            swiper.params.pagination.hiddenClass
          );
        }
      },
    },
  };
  var Scrollbar = {
    setTranslate: function setTranslate() {
      var swiper = this;

      if (!swiper.params.scrollbar.el || !swiper.scrollbar.el) {
        return;
      }

      var scrollbar = swiper.scrollbar;
      var rtl = swiper.rtlTranslate;
      var progress = swiper.progress;
      var dragSize = scrollbar.dragSize;
      var trackSize = scrollbar.trackSize;
      var $dragEl = scrollbar.$dragEl;
      var $el = scrollbar.$el;
      var params = swiper.params.scrollbar;
      var newSize = dragSize;
      var newPos = (trackSize - dragSize) * progress;

      if (rtl) {
        newPos = -newPos;

        if (newPos > 0) {
          newSize = dragSize - newPos;
          newPos = 0;
        } else if (-newPos + dragSize > trackSize) {
          newSize = trackSize + newPos;
        }
      } else if (newPos < 0) {
        newSize = dragSize + newPos;
        newPos = 0;
      } else if (newPos + dragSize > trackSize) {
        newSize = trackSize - newPos;
      }

      if (swiper.isHorizontal()) {
        if (Support.transforms3d) {
          $dragEl.transform("translate3d(" + newPos + "px, 0, 0)");
        } else {
          $dragEl.transform("translateX(" + newPos + "px)");
        }

        $dragEl[0].style.width = newSize + "px";
      } else {
        if (Support.transforms3d) {
          $dragEl.transform("translate3d(0px, " + newPos + "px, 0)");
        } else {
          $dragEl.transform("translateY(" + newPos + "px)");
        }

        $dragEl[0].style.height = newSize + "px";
      }

      if (params.hide) {
        clearTimeout(swiper.scrollbar.timeout);
        $el[0].style.opacity = 1;
        swiper.scrollbar.timeout = setTimeout(function () {
          $el[0].style.opacity = 0;
          $el.transition(400);
        }, 1000);
      }
    },
    setTransition: function setTransition(duration) {
      var swiper = this;

      if (!swiper.params.scrollbar.el || !swiper.scrollbar.el) {
        return;
      }

      swiper.scrollbar.$dragEl.transition(duration);
    },
    updateSize: function updateSize() {
      var swiper = this;

      if (!swiper.params.scrollbar.el || !swiper.scrollbar.el) {
        return;
      }

      var scrollbar = swiper.scrollbar;
      var $dragEl = scrollbar.$dragEl;
      var $el = scrollbar.$el;
      $dragEl[0].style.width = "";
      $dragEl[0].style.height = "";
      var trackSize = swiper.isHorizontal()
        ? $el[0].offsetWidth
        : $el[0].offsetHeight;
      var divider = swiper.size / swiper.virtualSize;
      var moveDivider = divider * (trackSize / swiper.size);
      var dragSize;

      if (swiper.params.scrollbar.dragSize === "auto") {
        dragSize = trackSize * divider;
      } else {
        dragSize = parseInt(swiper.params.scrollbar.dragSize, 10);
      }

      if (swiper.isHorizontal()) {
        $dragEl[0].style.width = dragSize + "px";
      } else {
        $dragEl[0].style.height = dragSize + "px";
      }

      if (divider >= 1) {
        $el[0].style.display = "none";
      } else {
        $el[0].style.display = "";
      }

      if (swiper.params.scrollbar.hide) {
        $el[0].style.opacity = 0;
      }

      Utils.extend(scrollbar, {
        trackSize: trackSize,
        divider: divider,
        moveDivider: moveDivider,
        dragSize: dragSize,
      });
      scrollbar.$el[
        swiper.params.watchOverflow && swiper.isLocked
          ? "addClass"
          : "removeClass"
      ](swiper.params.scrollbar.lockClass);
    },
    getPointerPosition: function getPointerPosition(e) {
      var swiper = this;

      if (swiper.isHorizontal()) {
        return e.type === "touchstart" || e.type === "touchmove"
          ? e.targetTouches[0].pageX
          : e.pageX || e.clientX;
      }

      return e.type === "touchstart" || e.type === "touchmove"
        ? e.targetTouches[0].pageY
        : e.pageY || e.clientY;
    },
    setDragPosition: function setDragPosition(e) {
      var swiper = this;
      var scrollbar = swiper.scrollbar;
      var rtl = swiper.rtlTranslate;
      var $el = scrollbar.$el;
      var dragSize = scrollbar.dragSize;
      var trackSize = scrollbar.trackSize;
      var dragStartPos = scrollbar.dragStartPos;
      var positionRatio;
      positionRatio =
        (scrollbar.getPointerPosition(e) -
          $el.offset()[swiper.isHorizontal() ? "left" : "top"] -
          (dragStartPos !== null ? dragStartPos : dragSize / 2)) /
        (trackSize - dragSize);
      positionRatio = Math.max(Math.min(positionRatio, 1), 0);

      if (rtl) {
        positionRatio = 1 - positionRatio;
      }

      var position =
        swiper.minTranslate() +
        (swiper.maxTranslate() - swiper.minTranslate()) * positionRatio;
      swiper.updateProgress(position);
      swiper.setTranslate(position);
      swiper.updateActiveIndex();
      swiper.updateSlidesClasses();
    },
    onDragStart: function onDragStart(e) {
      var swiper = this;
      var params = swiper.params.scrollbar;
      var scrollbar = swiper.scrollbar;
      var $wrapperEl = swiper.$wrapperEl;
      var $el = scrollbar.$el;
      var $dragEl = scrollbar.$dragEl;
      swiper.scrollbar.isTouched = true;
      swiper.scrollbar.dragStartPos =
        e.target === $dragEl[0] || e.target === $dragEl
          ? scrollbar.getPointerPosition(e) -
            e.target.getBoundingClientRect()[
              swiper.isHorizontal() ? "left" : "top"
            ]
          : null;
      e.preventDefault();
      e.stopPropagation();
      $wrapperEl.transition(100);
      $dragEl.transition(100);
      scrollbar.setDragPosition(e);
      clearTimeout(swiper.scrollbar.dragTimeout);
      $el.transition(0);

      if (params.hide) {
        $el.css("opacity", 1);
      }

      swiper.emit("scrollbarDragStart", e);
    },
    onDragMove: function onDragMove(e) {
      var swiper = this;
      var scrollbar = swiper.scrollbar;
      var $wrapperEl = swiper.$wrapperEl;
      var $el = scrollbar.$el;
      var $dragEl = scrollbar.$dragEl;

      if (!swiper.scrollbar.isTouched) {
        return;
      }

      if (e.preventDefault) {
        e.preventDefault();
      } else {
        e.returnValue = false;
      }

      scrollbar.setDragPosition(e);
      $wrapperEl.transition(0);
      $el.transition(0);
      $dragEl.transition(0);
      swiper.emit("scrollbarDragMove", e);
    },
    onDragEnd: function onDragEnd(e) {
      var swiper = this;
      var params = swiper.params.scrollbar;
      var scrollbar = swiper.scrollbar;
      var $el = scrollbar.$el;

      if (!swiper.scrollbar.isTouched) {
        return;
      }

      swiper.scrollbar.isTouched = false;

      if (params.hide) {
        clearTimeout(swiper.scrollbar.dragTimeout);
        swiper.scrollbar.dragTimeout = Utils.nextTick(function () {
          $el.css("opacity", 0);
          $el.transition(400);
        }, 1000);
      }

      swiper.emit("scrollbarDragEnd", e);

      if (params.snapOnRelease) {
        swiper.slideToClosest();
      }
    },
    enableDraggable: function enableDraggable() {
      var swiper = this;

      if (!swiper.params.scrollbar.el) {
        return;
      }

      var scrollbar = swiper.scrollbar;
      var touchEventsTouch = swiper.touchEventsTouch;
      var touchEventsDesktop = swiper.touchEventsDesktop;
      var params = swiper.params;
      var $el = scrollbar.$el;
      var target = $el[0];
      var activeListener =
        Support.passiveListener && params.passiveListeners
          ? {
              passive: false,
              capture: false,
            }
          : false;
      var passiveListener =
        Support.passiveListener && params.passiveListeners
          ? {
              passive: true,
              capture: false,
            }
          : false;

      if (!Support.touch) {
        target.addEventListener(
          touchEventsDesktop.start,
          swiper.scrollbar.onDragStart,
          activeListener
        );
        doc.addEventListener(
          touchEventsDesktop.move,
          swiper.scrollbar.onDragMove,
          activeListener
        );
        doc.addEventListener(
          touchEventsDesktop.end,
          swiper.scrollbar.onDragEnd,
          passiveListener
        );
      } else {
        target.addEventListener(
          touchEventsTouch.start,
          swiper.scrollbar.onDragStart,
          activeListener
        );
        target.addEventListener(
          touchEventsTouch.move,
          swiper.scrollbar.onDragMove,
          activeListener
        );
        target.addEventListener(
          touchEventsTouch.end,
          swiper.scrollbar.onDragEnd,
          passiveListener
        );
      }
    },
    disableDraggable: function disableDraggable() {
      var swiper = this;

      if (!swiper.params.scrollbar.el) {
        return;
      }

      var scrollbar = swiper.scrollbar;
      var touchEventsTouch = swiper.touchEventsTouch;
      var touchEventsDesktop = swiper.touchEventsDesktop;
      var params = swiper.params;
      var $el = scrollbar.$el;
      var target = $el[0];
      var activeListener =
        Support.passiveListener && params.passiveListeners
          ? {
              passive: false,
              capture: false,
            }
          : false;
      var passiveListener =
        Support.passiveListener && params.passiveListeners
          ? {
              passive: true,
              capture: false,
            }
          : false;

      if (!Support.touch) {
        target.removeEventListener(
          touchEventsDesktop.start,
          swiper.scrollbar.onDragStart,
          activeListener
        );
        doc.removeEventListener(
          touchEventsDesktop.move,
          swiper.scrollbar.onDragMove,
          activeListener
        );
        doc.removeEventListener(
          touchEventsDesktop.end,
          swiper.scrollbar.onDragEnd,
          passiveListener
        );
      } else {
        target.removeEventListener(
          touchEventsTouch.start,
          swiper.scrollbar.onDragStart,
          activeListener
        );
        target.removeEventListener(
          touchEventsTouch.move,
          swiper.scrollbar.onDragMove,
          activeListener
        );
        target.removeEventListener(
          touchEventsTouch.end,
          swiper.scrollbar.onDragEnd,
          passiveListener
        );
      }
    },
    init: function init() {
      var swiper = this;

      if (!swiper.params.scrollbar.el) {
        return;
      }

      var scrollbar = swiper.scrollbar;
      var $swiperEl = swiper.$el;
      var params = swiper.params.scrollbar;
      var $el = $(params.el);

      if (
        swiper.params.uniqueNavElements &&
        typeof params.el === "string" &&
        $el.length > 1 &&
        $swiperEl.find(params.el).length === 1
      ) {
        $el = $swiperEl.find(params.el);
      }

      var $dragEl = $el.find("." + swiper.params.scrollbar.dragClass);

      if ($dragEl.length === 0) {
        $dragEl = $(
          '<div class="' + swiper.params.scrollbar.dragClass + '"></div>'
        );
        $el.append($dragEl);
      }

      Utils.extend(scrollbar, {
        $el: $el,
        el: $el[0],
        $dragEl: $dragEl,
        dragEl: $dragEl[0],
      });

      if (params.draggable) {
        scrollbar.enableDraggable();
      }
    },
    destroy: function destroy() {
      var swiper = this;
      swiper.scrollbar.disableDraggable();
    },
  };
  var Scrollbar$1 = {
    name: "scrollbar",
    params: {
      scrollbar: {
        el: null,
        dragSize: "auto",
        hide: false,
        draggable: false,
        snapOnRelease: true,
        lockClass: "swiper-scrollbar-lock",
        dragClass: "swiper-scrollbar-drag",
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        scrollbar: {
          init: Scrollbar.init.bind(swiper),
          destroy: Scrollbar.destroy.bind(swiper),
          updateSize: Scrollbar.updateSize.bind(swiper),
          setTranslate: Scrollbar.setTranslate.bind(swiper),
          setTransition: Scrollbar.setTransition.bind(swiper),
          enableDraggable: Scrollbar.enableDraggable.bind(swiper),
          disableDraggable: Scrollbar.disableDraggable.bind(swiper),
          setDragPosition: Scrollbar.setDragPosition.bind(swiper),
          getPointerPosition: Scrollbar.getPointerPosition.bind(swiper),
          onDragStart: Scrollbar.onDragStart.bind(swiper),
          onDragMove: Scrollbar.onDragMove.bind(swiper),
          onDragEnd: Scrollbar.onDragEnd.bind(swiper),
          isTouched: false,
          timeout: null,
          dragTimeout: null,
        },
      });
    },
    on: {
      init: function init() {
        var swiper = this;
        swiper.scrollbar.init();
        swiper.scrollbar.updateSize();
        swiper.scrollbar.setTranslate();
      },
      update: function update() {
        var swiper = this;
        swiper.scrollbar.updateSize();
      },
      resize: function resize() {
        var swiper = this;
        swiper.scrollbar.updateSize();
      },
      observerUpdate: function observerUpdate() {
        var swiper = this;
        swiper.scrollbar.updateSize();
      },
      setTranslate: function setTranslate() {
        var swiper = this;
        swiper.scrollbar.setTranslate();
      },
      setTransition: function setTransition(duration) {
        var swiper = this;
        swiper.scrollbar.setTransition(duration);
      },
      destroy: function destroy() {
        var swiper = this;
        swiper.scrollbar.destroy();
      },
    },
  };
  var Parallax = {
    setTransform: function setTransform(el, progress) {
      var swiper = this;
      var rtl = swiper.rtl;
      var $el = $(el);
      var rtlFactor = rtl ? -1 : 1;
      var p = $el.attr("data-swiper-parallax") || "0";
      var x = $el.attr("data-swiper-parallax-x");
      var y = $el.attr("data-swiper-parallax-y");
      var scale = $el.attr("data-swiper-parallax-scale");
      var opacity = $el.attr("data-swiper-parallax-opacity");

      if (x || y) {
        x = x || "0";
        y = y || "0";
      } else if (swiper.isHorizontal()) {
        x = p;
        y = "0";
      } else {
        y = p;
        x = "0";
      }

      if (x.indexOf("%") >= 0) {
        x = parseInt(x, 10) * progress * rtlFactor + "%";
      } else {
        x = x * progress * rtlFactor + "px";
      }

      if (y.indexOf("%") >= 0) {
        y = parseInt(y, 10) * progress + "%";
      } else {
        y = y * progress + "px";
      }

      if (typeof opacity !== "undefined" && opacity !== null) {
        var currentOpacity = opacity - (opacity - 1) * (1 - Math.abs(progress));
        $el[0].style.opacity = currentOpacity;
      }

      if (typeof scale === "undefined" || scale === null) {
        $el.transform("translate3d(" + x + ", " + y + ", 0px)");
      } else {
        var currentScale = scale - (scale - 1) * (1 - Math.abs(progress));
        $el.transform(
          "translate3d(" + x + ", " + y + ", 0px) scale(" + currentScale + ")"
        );
      }
    },
    setTranslate: function setTranslate() {
      var swiper = this;
      var $el = swiper.$el;
      var slides = swiper.slides;
      var progress = swiper.progress;
      var snapGrid = swiper.snapGrid;
      $el
        .children(
          "[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]"
        )
        .each(function (index, el) {
          swiper.parallax.setTransform(el, progress);
        });
      slides.each(function (slideIndex, slideEl) {
        var slideProgress = slideEl.progress;

        if (
          swiper.params.slidesPerGroup > 1 &&
          swiper.params.slidesPerView !== "auto"
        ) {
          slideProgress +=
            Math.ceil(slideIndex / 2) - progress * (snapGrid.length - 1);
        }

        slideProgress = Math.min(Math.max(slideProgress, -1), 1);
        $(slideEl)
          .find(
            "[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]"
          )
          .each(function (index, el) {
            swiper.parallax.setTransform(el, slideProgress);
          });
      });
    },
    setTransition: function setTransition(duration) {
      if (duration === void 0) duration = this.params.speed;
      var swiper = this;
      var $el = swiper.$el;
      $el
        .find(
          "[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]"
        )
        .each(function (index, parallaxEl) {
          var $parallaxEl = $(parallaxEl);
          var parallaxDuration =
            parseInt($parallaxEl.attr("data-swiper-parallax-duration"), 10) ||
            duration;

          if (duration === 0) {
            parallaxDuration = 0;
          }

          $parallaxEl.transition(parallaxDuration);
        });
    },
  };
  var Parallax$1 = {
    name: "parallax",
    params: {
      parallax: {
        enabled: false,
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        parallax: {
          setTransform: Parallax.setTransform.bind(swiper),
          setTranslate: Parallax.setTranslate.bind(swiper),
          setTransition: Parallax.setTransition.bind(swiper),
        },
      });
    },
    on: {
      beforeInit: function beforeInit() {
        var swiper = this;

        if (!swiper.params.parallax.enabled) {
          return;
        }

        swiper.params.watchSlidesProgress = true;
        swiper.originalParams.watchSlidesProgress = true;
      },
      init: function init() {
        var swiper = this;

        if (!swiper.params.parallax.enabled) {
          return;
        }

        swiper.parallax.setTranslate();
      },
      setTranslate: function setTranslate() {
        var swiper = this;

        if (!swiper.params.parallax.enabled) {
          return;
        }

        swiper.parallax.setTranslate();
      },
      setTransition: function setTransition(duration) {
        var swiper = this;

        if (!swiper.params.parallax.enabled) {
          return;
        }

        swiper.parallax.setTransition(duration);
      },
    },
  };
  var Zoom = {
    // Calc Scale From Multi-touches
    getDistanceBetweenTouches: function getDistanceBetweenTouches(e) {
      if (e.targetTouches.length < 2) {
        return 1;
      }

      var x1 = e.targetTouches[0].pageX;
      var y1 = e.targetTouches[0].pageY;
      var x2 = e.targetTouches[1].pageX;
      var y2 = e.targetTouches[1].pageY;
      var distance = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
      return distance;
    },
    // Events
    onGestureStart: function onGestureStart(e) {
      var swiper = this;
      var params = swiper.params.zoom;
      var zoom = swiper.zoom;
      var gesture = zoom.gesture;
      zoom.fakeGestureTouched = false;
      zoom.fakeGestureMoved = false;

      if (!Support.gestures) {
        if (
          e.type !== "touchstart" ||
          (e.type === "touchstart" && e.targetTouches.length < 2)
        ) {
          return;
        }

        zoom.fakeGestureTouched = true;
        gesture.scaleStart = Zoom.getDistanceBetweenTouches(e);
      }

      if (!gesture.$slideEl || !gesture.$slideEl.length) {
        gesture.$slideEl = $(e.target).closest(".swiper-slide");

        if (gesture.$slideEl.length === 0) {
          gesture.$slideEl = swiper.slides.eq(swiper.activeIndex);
        }

        gesture.$imageEl = gesture.$slideEl.find("img, svg, canvas");
        gesture.$imageWrapEl = gesture.$imageEl.parent(
          "." + params.containerClass
        );
        gesture.maxRatio =
          gesture.$imageWrapEl.attr("data-swiper-zoom") || params.maxRatio;

        if (gesture.$imageWrapEl.length === 0) {
          gesture.$imageEl = undefined;
          return;
        }
      }

      gesture.$imageEl.transition(0);
      swiper.zoom.isScaling = true;
    },
    onGestureChange: function onGestureChange(e) {
      var swiper = this;
      var params = swiper.params.zoom;
      var zoom = swiper.zoom;
      var gesture = zoom.gesture;

      if (!Support.gestures) {
        if (
          e.type !== "touchmove" ||
          (e.type === "touchmove" && e.targetTouches.length < 2)
        ) {
          return;
        }

        zoom.fakeGestureMoved = true;
        gesture.scaleMove = Zoom.getDistanceBetweenTouches(e);
      }

      if (!gesture.$imageEl || gesture.$imageEl.length === 0) {
        return;
      }

      if (Support.gestures) {
        zoom.scale = e.scale * zoom.currentScale;
      } else {
        zoom.scale =
          (gesture.scaleMove / gesture.scaleStart) * zoom.currentScale;
      }

      if (zoom.scale > gesture.maxRatio) {
        zoom.scale =
          gesture.maxRatio -
          1 +
          Math.pow(zoom.scale - gesture.maxRatio + 1, 0.5);
      }

      if (zoom.scale < params.minRatio) {
        zoom.scale =
          params.minRatio + 1 - Math.pow(params.minRatio - zoom.scale + 1, 0.5);
      }

      gesture.$imageEl.transform(
        "translate3d(0,0,0) scale(" + zoom.scale + ")"
      );
    },
    onGestureEnd: function onGestureEnd(e) {
      var swiper = this;
      var params = swiper.params.zoom;
      var zoom = swiper.zoom;
      var gesture = zoom.gesture;

      if (!Support.gestures) {
        if (!zoom.fakeGestureTouched || !zoom.fakeGestureMoved) {
          return;
        }

        if (
          e.type !== "touchend" ||
          (e.type === "touchend" &&
            e.changedTouches.length < 2 &&
            !Device.android)
        ) {
          return;
        }

        zoom.fakeGestureTouched = false;
        zoom.fakeGestureMoved = false;
      }

      if (!gesture.$imageEl || gesture.$imageEl.length === 0) {
        return;
      }

      zoom.scale = Math.max(
        Math.min(zoom.scale, gesture.maxRatio),
        params.minRatio
      );
      gesture.$imageEl
        .transition(swiper.params.speed)
        .transform("translate3d(0,0,0) scale(" + zoom.scale + ")");
      zoom.currentScale = zoom.scale;
      zoom.isScaling = false;

      if (zoom.scale === 1) {
        gesture.$slideEl = undefined;
      }
    },
    onTouchStart: function onTouchStart(e) {
      var swiper = this;
      var zoom = swiper.zoom;
      var gesture = zoom.gesture;
      var image = zoom.image;

      if (!gesture.$imageEl || gesture.$imageEl.length === 0) {
        return;
      }

      if (image.isTouched) {
        return;
      }

      if (Device.android) {
        e.preventDefault();
      }

      image.isTouched = true;
      image.touchesStart.x =
        e.type === "touchstart" ? e.targetTouches[0].pageX : e.pageX;
      image.touchesStart.y =
        e.type === "touchstart" ? e.targetTouches[0].pageY : e.pageY;
    },
    onTouchMove: function onTouchMove(e) {
      var swiper = this;
      var zoom = swiper.zoom;
      var gesture = zoom.gesture;
      var image = zoom.image;
      var velocity = zoom.velocity;

      if (!gesture.$imageEl || gesture.$imageEl.length === 0) {
        return;
      }

      swiper.allowClick = false;

      if (!image.isTouched || !gesture.$slideEl) {
        return;
      }

      if (!image.isMoved) {
        image.width = gesture.$imageEl[0].offsetWidth;
        image.height = gesture.$imageEl[0].offsetHeight;
        image.startX = Utils.getTranslate(gesture.$imageWrapEl[0], "x") || 0;
        image.startY = Utils.getTranslate(gesture.$imageWrapEl[0], "y") || 0;
        gesture.slideWidth = gesture.$slideEl[0].offsetWidth;
        gesture.slideHeight = gesture.$slideEl[0].offsetHeight;
        gesture.$imageWrapEl.transition(0);

        if (swiper.rtl) {
          image.startX = -image.startX;
          image.startY = -image.startY;
        }
      } // Define if we need image drag

      var scaledWidth = image.width * zoom.scale;
      var scaledHeight = image.height * zoom.scale;

      if (
        scaledWidth < gesture.slideWidth &&
        scaledHeight < gesture.slideHeight
      ) {
        return;
      }

      image.minX = Math.min(gesture.slideWidth / 2 - scaledWidth / 2, 0);
      image.maxX = -image.minX;
      image.minY = Math.min(gesture.slideHeight / 2 - scaledHeight / 2, 0);
      image.maxY = -image.minY;
      image.touchesCurrent.x =
        e.type === "touchmove" ? e.targetTouches[0].pageX : e.pageX;
      image.touchesCurrent.y =
        e.type === "touchmove" ? e.targetTouches[0].pageY : e.pageY;

      if (!image.isMoved && !zoom.isScaling) {
        if (
          swiper.isHorizontal() &&
          ((Math.floor(image.minX) === Math.floor(image.startX) &&
            image.touchesCurrent.x < image.touchesStart.x) ||
            (Math.floor(image.maxX) === Math.floor(image.startX) &&
              image.touchesCurrent.x > image.touchesStart.x))
        ) {
          image.isTouched = false;
          return;
        }

        if (
          !swiper.isHorizontal() &&
          ((Math.floor(image.minY) === Math.floor(image.startY) &&
            image.touchesCurrent.y < image.touchesStart.y) ||
            (Math.floor(image.maxY) === Math.floor(image.startY) &&
              image.touchesCurrent.y > image.touchesStart.y))
        ) {
          image.isTouched = false;
          return;
        }
      }

      e.preventDefault();
      e.stopPropagation();
      image.isMoved = true;
      image.currentX =
        image.touchesCurrent.x - image.touchesStart.x + image.startX;
      image.currentY =
        image.touchesCurrent.y - image.touchesStart.y + image.startY;

      if (image.currentX < image.minX) {
        image.currentX =
          image.minX + 1 - Math.pow(image.minX - image.currentX + 1, 0.8);
      }

      if (image.currentX > image.maxX) {
        image.currentX =
          image.maxX - 1 + Math.pow(image.currentX - image.maxX + 1, 0.8);
      }

      if (image.currentY < image.minY) {
        image.currentY =
          image.minY + 1 - Math.pow(image.minY - image.currentY + 1, 0.8);
      }

      if (image.currentY > image.maxY) {
        image.currentY =
          image.maxY - 1 + Math.pow(image.currentY - image.maxY + 1, 0.8);
      } // Velocity

      if (!velocity.prevPositionX) {
        velocity.prevPositionX = image.touchesCurrent.x;
      }

      if (!velocity.prevPositionY) {
        velocity.prevPositionY = image.touchesCurrent.y;
      }

      if (!velocity.prevTime) {
        velocity.prevTime = Date.now();
      }

      velocity.x =
        (image.touchesCurrent.x - velocity.prevPositionX) /
        (Date.now() - velocity.prevTime) /
        2;
      velocity.y =
        (image.touchesCurrent.y - velocity.prevPositionY) /
        (Date.now() - velocity.prevTime) /
        2;

      if (Math.abs(image.touchesCurrent.x - velocity.prevPositionX) < 2) {
        velocity.x = 0;
      }

      if (Math.abs(image.touchesCurrent.y - velocity.prevPositionY) < 2) {
        velocity.y = 0;
      }

      velocity.prevPositionX = image.touchesCurrent.x;
      velocity.prevPositionY = image.touchesCurrent.y;
      velocity.prevTime = Date.now();
      gesture.$imageWrapEl.transform(
        "translate3d(" + image.currentX + "px, " + image.currentY + "px,0)"
      );
    },
    onTouchEnd: function onTouchEnd() {
      var swiper = this;
      var zoom = swiper.zoom;
      var gesture = zoom.gesture;
      var image = zoom.image;
      var velocity = zoom.velocity;

      if (!gesture.$imageEl || gesture.$imageEl.length === 0) {
        return;
      }

      if (!image.isTouched || !image.isMoved) {
        image.isTouched = false;
        image.isMoved = false;
        return;
      }

      image.isTouched = false;
      image.isMoved = false;
      var momentumDurationX = 300;
      var momentumDurationY = 300;
      var momentumDistanceX = velocity.x * momentumDurationX;
      var newPositionX = image.currentX + momentumDistanceX;
      var momentumDistanceY = velocity.y * momentumDurationY;
      var newPositionY = image.currentY + momentumDistanceY; // Fix duration

      if (velocity.x !== 0) {
        momentumDurationX = Math.abs(
          (newPositionX - image.currentX) / velocity.x
        );
      }

      if (velocity.y !== 0) {
        momentumDurationY = Math.abs(
          (newPositionY - image.currentY) / velocity.y
        );
      }

      var momentumDuration = Math.max(momentumDurationX, momentumDurationY);
      image.currentX = newPositionX;
      image.currentY = newPositionY; // Define if we need image drag

      var scaledWidth = image.width * zoom.scale;
      var scaledHeight = image.height * zoom.scale;
      image.minX = Math.min(gesture.slideWidth / 2 - scaledWidth / 2, 0);
      image.maxX = -image.minX;
      image.minY = Math.min(gesture.slideHeight / 2 - scaledHeight / 2, 0);
      image.maxY = -image.minY;
      image.currentX = Math.max(
        Math.min(image.currentX, image.maxX),
        image.minX
      );
      image.currentY = Math.max(
        Math.min(image.currentY, image.maxY),
        image.minY
      );
      gesture.$imageWrapEl
        .transition(momentumDuration)
        .transform(
          "translate3d(" + image.currentX + "px, " + image.currentY + "px,0)"
        );
    },
    onTransitionEnd: function onTransitionEnd() {
      var swiper = this;
      var zoom = swiper.zoom;
      var gesture = zoom.gesture;

      if (gesture.$slideEl && swiper.previousIndex !== swiper.activeIndex) {
        gesture.$imageEl.transform("translate3d(0,0,0) scale(1)");
        gesture.$imageWrapEl.transform("translate3d(0,0,0)");
        zoom.scale = 1;
        zoom.currentScale = 1;
        gesture.$slideEl = undefined;
        gesture.$imageEl = undefined;
        gesture.$imageWrapEl = undefined;
      }
    },
    // Toggle Zoom
    toggle: function toggle(e) {
      var swiper = this;
      var zoom = swiper.zoom;

      if (zoom.scale && zoom.scale !== 1) {
        // Zoom Out
        zoom.out();
      } else {
        // Zoom In
        zoom["in"](e);
      }
    },
    in: function in$1(e) {
      var swiper = this;
      var zoom = swiper.zoom;
      var params = swiper.params.zoom;
      var gesture = zoom.gesture;
      var image = zoom.image;

      if (!gesture.$slideEl) {
        gesture.$slideEl = swiper.clickedSlide
          ? $(swiper.clickedSlide)
          : swiper.slides.eq(swiper.activeIndex);
        gesture.$imageEl = gesture.$slideEl.find("img, svg, canvas");
        gesture.$imageWrapEl = gesture.$imageEl.parent(
          "." + params.containerClass
        );
      }

      if (!gesture.$imageEl || gesture.$imageEl.length === 0) {
        return;
      }

      gesture.$slideEl.addClass("" + params.zoomedSlideClass);
      var touchX;
      var touchY;
      var offsetX;
      var offsetY;
      var diffX;
      var diffY;
      var translateX;
      var translateY;
      var imageWidth;
      var imageHeight;
      var scaledWidth;
      var scaledHeight;
      var translateMinX;
      var translateMinY;
      var translateMaxX;
      var translateMaxY;
      var slideWidth;
      var slideHeight;

      if (typeof image.touchesStart.x === "undefined" && e) {
        touchX = e.type === "touchend" ? e.changedTouches[0].pageX : e.pageX;
        touchY = e.type === "touchend" ? e.changedTouches[0].pageY : e.pageY;
      } else {
        touchX = image.touchesStart.x;
        touchY = image.touchesStart.y;
      }

      zoom.scale =
        gesture.$imageWrapEl.attr("data-swiper-zoom") || params.maxRatio;
      zoom.currentScale =
        gesture.$imageWrapEl.attr("data-swiper-zoom") || params.maxRatio;

      if (e) {
        slideWidth = gesture.$slideEl[0].offsetWidth;
        slideHeight = gesture.$slideEl[0].offsetHeight;
        offsetX = gesture.$slideEl.offset().left;
        offsetY = gesture.$slideEl.offset().top;
        diffX = offsetX + slideWidth / 2 - touchX;
        diffY = offsetY + slideHeight / 2 - touchY;
        imageWidth = gesture.$imageEl[0].offsetWidth;
        imageHeight = gesture.$imageEl[0].offsetHeight;
        scaledWidth = imageWidth * zoom.scale;
        scaledHeight = imageHeight * zoom.scale;
        translateMinX = Math.min(slideWidth / 2 - scaledWidth / 2, 0);
        translateMinY = Math.min(slideHeight / 2 - scaledHeight / 2, 0);
        translateMaxX = -translateMinX;
        translateMaxY = -translateMinY;
        translateX = diffX * zoom.scale;
        translateY = diffY * zoom.scale;

        if (translateX < translateMinX) {
          translateX = translateMinX;
        }

        if (translateX > translateMaxX) {
          translateX = translateMaxX;
        }

        if (translateY < translateMinY) {
          translateY = translateMinY;
        }

        if (translateY > translateMaxY) {
          translateY = translateMaxY;
        }
      } else {
        translateX = 0;
        translateY = 0;
      }

      gesture.$imageWrapEl
        .transition(300)
        .transform("translate3d(" + translateX + "px, " + translateY + "px,0)");
      gesture.$imageEl
        .transition(300)
        .transform("translate3d(0,0,0) scale(" + zoom.scale + ")");
    },
    out: function out() {
      var swiper = this;
      var zoom = swiper.zoom;
      var params = swiper.params.zoom;
      var gesture = zoom.gesture;

      if (!gesture.$slideEl) {
        gesture.$slideEl = swiper.clickedSlide
          ? $(swiper.clickedSlide)
          : swiper.slides.eq(swiper.activeIndex);
        gesture.$imageEl = gesture.$slideEl.find("img, svg, canvas");
        gesture.$imageWrapEl = gesture.$imageEl.parent(
          "." + params.containerClass
        );
      }

      if (!gesture.$imageEl || gesture.$imageEl.length === 0) {
        return;
      }

      zoom.scale = 1;
      zoom.currentScale = 1;
      gesture.$imageWrapEl.transition(300).transform("translate3d(0,0,0)");
      gesture.$imageEl.transition(300).transform("translate3d(0,0,0) scale(1)");
      gesture.$slideEl.removeClass("" + params.zoomedSlideClass);
      gesture.$slideEl = undefined;
    },
    // Attach/Detach Events
    enable: function enable() {
      var swiper = this;
      var zoom = swiper.zoom;

      if (zoom.enabled) {
        return;
      }

      zoom.enabled = true;
      var passiveListener =
        swiper.touchEvents.start === "touchstart" &&
        Support.passiveListener &&
        swiper.params.passiveListeners
          ? {
              passive: true,
              capture: false,
            }
          : false; // Scale image

      if (Support.gestures) {
        swiper.$wrapperEl.on(
          "gesturestart",
          ".swiper-slide",
          zoom.onGestureStart,
          passiveListener
        );
        swiper.$wrapperEl.on(
          "gesturechange",
          ".swiper-slide",
          zoom.onGestureChange,
          passiveListener
        );
        swiper.$wrapperEl.on(
          "gestureend",
          ".swiper-slide",
          zoom.onGestureEnd,
          passiveListener
        );
      } else if (swiper.touchEvents.start === "touchstart") {
        swiper.$wrapperEl.on(
          swiper.touchEvents.start,
          ".swiper-slide",
          zoom.onGestureStart,
          passiveListener
        );
        swiper.$wrapperEl.on(
          swiper.touchEvents.move,
          ".swiper-slide",
          zoom.onGestureChange,
          passiveListener
        );
        swiper.$wrapperEl.on(
          swiper.touchEvents.end,
          ".swiper-slide",
          zoom.onGestureEnd,
          passiveListener
        );
      } // Move image

      swiper.$wrapperEl.on(
        swiper.touchEvents.move,
        "." + swiper.params.zoom.containerClass,
        zoom.onTouchMove
      );
    },
    disable: function disable() {
      var swiper = this;
      var zoom = swiper.zoom;

      if (!zoom.enabled) {
        return;
      }

      swiper.zoom.enabled = false;
      var passiveListener =
        swiper.touchEvents.start === "touchstart" &&
        Support.passiveListener &&
        swiper.params.passiveListeners
          ? {
              passive: true,
              capture: false,
            }
          : false; // Scale image

      if (Support.gestures) {
        swiper.$wrapperEl.off(
          "gesturestart",
          ".swiper-slide",
          zoom.onGestureStart,
          passiveListener
        );
        swiper.$wrapperEl.off(
          "gesturechange",
          ".swiper-slide",
          zoom.onGestureChange,
          passiveListener
        );
        swiper.$wrapperEl.off(
          "gestureend",
          ".swiper-slide",
          zoom.onGestureEnd,
          passiveListener
        );
      } else if (swiper.touchEvents.start === "touchstart") {
        swiper.$wrapperEl.off(
          swiper.touchEvents.start,
          ".swiper-slide",
          zoom.onGestureStart,
          passiveListener
        );
        swiper.$wrapperEl.off(
          swiper.touchEvents.move,
          ".swiper-slide",
          zoom.onGestureChange,
          passiveListener
        );
        swiper.$wrapperEl.off(
          swiper.touchEvents.end,
          ".swiper-slide",
          zoom.onGestureEnd,
          passiveListener
        );
      } // Move image

      swiper.$wrapperEl.off(
        swiper.touchEvents.move,
        "." + swiper.params.zoom.containerClass,
        zoom.onTouchMove
      );
    },
  };
  var Zoom$1 = {
    name: "zoom",
    params: {
      zoom: {
        enabled: false,
        maxRatio: 3,
        minRatio: 1,
        toggle: true,
        containerClass: "swiper-zoom-container",
        zoomedSlideClass: "swiper-slide-zoomed",
      },
    },
    create: function create() {
      var swiper = this;
      var zoom = {
        enabled: false,
        scale: 1,
        currentScale: 1,
        isScaling: false,
        gesture: {
          $slideEl: undefined,
          slideWidth: undefined,
          slideHeight: undefined,
          $imageEl: undefined,
          $imageWrapEl: undefined,
          maxRatio: 3,
        },
        image: {
          isTouched: undefined,
          isMoved: undefined,
          currentX: undefined,
          currentY: undefined,
          minX: undefined,
          minY: undefined,
          maxX: undefined,
          maxY: undefined,
          width: undefined,
          height: undefined,
          startX: undefined,
          startY: undefined,
          touchesStart: {},
          touchesCurrent: {},
        },
        velocity: {
          x: undefined,
          y: undefined,
          prevPositionX: undefined,
          prevPositionY: undefined,
          prevTime: undefined,
        },
      };
      "onGestureStart onGestureChange onGestureEnd onTouchStart onTouchMove onTouchEnd onTransitionEnd toggle enable disable in out"
        .split(" ")
        .forEach(function (methodName) {
          zoom[methodName] = Zoom[methodName].bind(swiper);
        });
      Utils.extend(swiper, {
        zoom: zoom,
      });
      var scale = 1;
      Object.defineProperty(swiper.zoom, "scale", {
        get: function get() {
          return scale;
        },
        set: function set(value) {
          if (scale !== value) {
            var imageEl = swiper.zoom.gesture.$imageEl
              ? swiper.zoom.gesture.$imageEl[0]
              : undefined;
            var slideEl = swiper.zoom.gesture.$slideEl
              ? swiper.zoom.gesture.$slideEl[0]
              : undefined;
            swiper.emit("zoomChange", value, imageEl, slideEl);
          }

          scale = value;
        },
      });
    },
    on: {
      init: function init() {
        var swiper = this;

        if (swiper.params.zoom.enabled) {
          swiper.zoom.enable();
        }
      },
      destroy: function destroy() {
        var swiper = this;
        swiper.zoom.disable();
      },
      touchStart: function touchStart(e) {
        var swiper = this;

        if (!swiper.zoom.enabled) {
          return;
        }

        swiper.zoom.onTouchStart(e);
      },
      touchEnd: function touchEnd(e) {
        var swiper = this;

        if (!swiper.zoom.enabled) {
          return;
        }

        swiper.zoom.onTouchEnd(e);
      },
      doubleTap: function doubleTap(e) {
        var swiper = this;

        if (
          swiper.params.zoom.enabled &&
          swiper.zoom.enabled &&
          swiper.params.zoom.toggle
        ) {
          swiper.zoom.toggle(e);
        }
      },
      transitionEnd: function transitionEnd() {
        var swiper = this;

        if (swiper.zoom.enabled && swiper.params.zoom.enabled) {
          swiper.zoom.onTransitionEnd();
        }
      },
    },
  };
  var Lazy = {
    loadInSlide: function loadInSlide(index, loadInDuplicate) {
      if (loadInDuplicate === void 0) loadInDuplicate = true;
      var swiper = this;
      var params = swiper.params.lazy;

      if (typeof index === "undefined") {
        return;
      }

      if (swiper.slides.length === 0) {
        return;
      }

      var isVirtual = swiper.virtual && swiper.params.virtual.enabled;
      var $slideEl = isVirtual
        ? swiper.$wrapperEl.children(
            "." +
              swiper.params.slideClass +
              '[data-swiper-slide-index="' +
              index +
              '"]'
          )
        : swiper.slides.eq(index);
      var $images = $slideEl.find(
        "." +
          params.elementClass +
          ":not(." +
          params.loadedClass +
          "):not(." +
          params.loadingClass +
          ")"
      );

      if (
        $slideEl.hasClass(params.elementClass) &&
        !$slideEl.hasClass(params.loadedClass) &&
        !$slideEl.hasClass(params.loadingClass)
      ) {
        $images = $images.add($slideEl[0]);
      }

      if ($images.length === 0) {
        return;
      }

      $images.each(function (imageIndex, imageEl) {
        var $imageEl = $(imageEl);
        $imageEl.addClass(params.loadingClass);
        var background = $imageEl.attr("data-background");
        var src = $imageEl.attr("data-src");
        var srcset = $imageEl.attr("data-srcset");
        var sizes = $imageEl.attr("data-sizes");
        swiper.loadImage(
          $imageEl[0],
          src || background,
          srcset,
          sizes,
          false,
          function () {
            if (
              typeof swiper === "undefined" ||
              swiper === null ||
              !swiper ||
              (swiper && !swiper.params) ||
              swiper.destroyed
            ) {
              return;
            }

            if (background) {
              $imageEl.css("background-image", 'url("' + background + '")');
              $imageEl.removeAttr("data-background");
            } else {
              if (srcset) {
                $imageEl.attr("srcset", srcset);
                $imageEl.removeAttr("data-srcset");
              }

              if (sizes) {
                $imageEl.attr("sizes", sizes);
                $imageEl.removeAttr("data-sizes");
              }

              if (src) {
                $imageEl.attr("src", src);
                $imageEl.removeAttr("data-src");
              }
            }

            $imageEl
              .addClass(params.loadedClass)
              .removeClass(params.loadingClass);
            $slideEl.find("." + params.preloaderClass).remove();

            if (swiper.params.loop && loadInDuplicate) {
              var slideOriginalIndex = $slideEl.attr("data-swiper-slide-index");

              if ($slideEl.hasClass(swiper.params.slideDuplicateClass)) {
                var originalSlide = swiper.$wrapperEl.children(
                  '[data-swiper-slide-index="' +
                    slideOriginalIndex +
                    '"]:not(.' +
                    swiper.params.slideDuplicateClass +
                    ")"
                );
                swiper.lazy.loadInSlide(originalSlide.index(), false);
              } else {
                var duplicatedSlide = swiper.$wrapperEl.children(
                  "." +
                    swiper.params.slideDuplicateClass +
                    '[data-swiper-slide-index="' +
                    slideOriginalIndex +
                    '"]'
                );
                swiper.lazy.loadInSlide(duplicatedSlide.index(), false);
              }
            }

            swiper.emit("lazyImageReady", $slideEl[0], $imageEl[0]);
          }
        );
        swiper.emit("lazyImageLoad", $slideEl[0], $imageEl[0]);
      });
    },
    load: function load() {
      var swiper = this;
      var $wrapperEl = swiper.$wrapperEl;
      var swiperParams = swiper.params;
      var slides = swiper.slides;
      var activeIndex = swiper.activeIndex;
      var isVirtual = swiper.virtual && swiperParams.virtual.enabled;
      var params = swiperParams.lazy;
      var slidesPerView = swiperParams.slidesPerView;

      if (slidesPerView === "auto") {
        slidesPerView = 0;
      }

      function slideExist(index) {
        if (isVirtual) {
          if (
            $wrapperEl.children(
              "." +
                swiperParams.slideClass +
                '[data-swiper-slide-index="' +
                index +
                '"]'
            ).length
          ) {
            return true;
          }
        } else if (slides[index]) {
          return true;
        }

        return false;
      }

      function slideIndex(slideEl) {
        if (isVirtual) {
          return $(slideEl).attr("data-swiper-slide-index");
        }

        return $(slideEl).index();
      }

      if (!swiper.lazy.initialImageLoaded) {
        swiper.lazy.initialImageLoaded = true;
      }

      if (swiper.params.watchSlidesVisibility) {
        $wrapperEl
          .children("." + swiperParams.slideVisibleClass)
          .each(function (elIndex, slideEl) {
            var index = isVirtual
              ? $(slideEl).attr("data-swiper-slide-index")
              : $(slideEl).index();
            swiper.lazy.loadInSlide(index);
          });
      } else if (slidesPerView > 1) {
        for (var i = activeIndex; i < activeIndex + slidesPerView; i += 1) {
          if (slideExist(i)) {
            swiper.lazy.loadInSlide(i);
          }
        }
      } else {
        swiper.lazy.loadInSlide(activeIndex);
      }

      if (params.loadPrevNext) {
        if (
          slidesPerView > 1 ||
          (params.loadPrevNextAmount && params.loadPrevNextAmount > 1)
        ) {
          var amount = params.loadPrevNextAmount;
          var spv = slidesPerView;
          var maxIndex = Math.min(
            activeIndex + spv + Math.max(amount, spv),
            slides.length
          );
          var minIndex = Math.max(activeIndex - Math.max(spv, amount), 0); // Next Slides

          for (
            var i$1 = activeIndex + slidesPerView;
            i$1 < maxIndex;
            i$1 += 1
          ) {
            if (slideExist(i$1)) {
              swiper.lazy.loadInSlide(i$1);
            }
          } // Prev Slides

          for (var i$2 = minIndex; i$2 < activeIndex; i$2 += 1) {
            if (slideExist(i$2)) {
              swiper.lazy.loadInSlide(i$2);
            }
          }
        } else {
          var nextSlide = $wrapperEl.children(
            "." + swiperParams.slideNextClass
          );

          if (nextSlide.length > 0) {
            swiper.lazy.loadInSlide(slideIndex(nextSlide));
          }

          var prevSlide = $wrapperEl.children(
            "." + swiperParams.slidePrevClass
          );

          if (prevSlide.length > 0) {
            swiper.lazy.loadInSlide(slideIndex(prevSlide));
          }
        }
      }
    },
  };
  var Lazy$1 = {
    name: "lazy",
    params: {
      lazy: {
        enabled: false,
        loadPrevNext: false,
        loadPrevNextAmount: 1,
        loadOnTransitionStart: false,
        elementClass: "swiper-lazy",
        loadingClass: "swiper-lazy-loading",
        loadedClass: "swiper-lazy-loaded",
        preloaderClass: "swiper-lazy-preloader",
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        lazy: {
          initialImageLoaded: false,
          load: Lazy.load.bind(swiper),
          loadInSlide: Lazy.loadInSlide.bind(swiper),
        },
      });
    },
    on: {
      beforeInit: function beforeInit() {
        var swiper = this;

        if (swiper.params.lazy.enabled && swiper.params.preloadImages) {
          swiper.params.preloadImages = false;
        }
      },
      init: function init() {
        var swiper = this;

        if (
          swiper.params.lazy.enabled &&
          !swiper.params.loop &&
          swiper.params.initialSlide === 0
        ) {
          swiper.lazy.load();
        }
      },
      scroll: function scroll() {
        var swiper = this;

        if (swiper.params.freeMode && !swiper.params.freeModeSticky) {
          swiper.lazy.load();
        }
      },
      resize: function resize() {
        var swiper = this;

        if (swiper.params.lazy.enabled) {
          swiper.lazy.load();
        }
      },
      scrollbarDragMove: function scrollbarDragMove() {
        var swiper = this;

        if (swiper.params.lazy.enabled) {
          swiper.lazy.load();
        }
      },
      transitionStart: function transitionStart() {
        var swiper = this;

        if (swiper.params.lazy.enabled) {
          if (
            swiper.params.lazy.loadOnTransitionStart ||
            (!swiper.params.lazy.loadOnTransitionStart &&
              !swiper.lazy.initialImageLoaded)
          ) {
            swiper.lazy.load();
          }
        }
      },
      transitionEnd: function transitionEnd() {
        var swiper = this;

        if (
          swiper.params.lazy.enabled &&
          !swiper.params.lazy.loadOnTransitionStart
        ) {
          swiper.lazy.load();
        }
      },
    },
  };
  /* eslint no-bitwise: ["error", { "allow": [">>"] }] */

  var Controller = {
    LinearSpline: function LinearSpline(x, y) {
      var binarySearch = (function search() {
        var maxIndex;
        var minIndex;
        var guess;
        return function (array, val) {
          minIndex = -1;
          maxIndex = array.length;

          while (maxIndex - minIndex > 1) {
            guess = (maxIndex + minIndex) >> 1;

            if (array[guess] <= val) {
              minIndex = guess;
            } else {
              maxIndex = guess;
            }
          }

          return maxIndex;
        };
      })();

      this.x = x;
      this.y = y;
      this.lastIndex = x.length - 1; // Given an x value (x2), return the expected y2 value:
      // (x1,y1) is the known point before given value,
      // (x3,y3) is the known point after given value.

      var i1;
      var i3;

      this.interpolate = function interpolate(x2) {
        if (!x2) {
          return 0;
        } // Get the indexes of x1 and x3 (the array indexes before and after given x2):

        i3 = binarySearch(this.x, x2);
        i1 = i3 - 1; // We have our indexes i1 & i3, so we can calculate already:
        // y2 := ((x2−x1) × (y3−y1)) ÷ (x3−x1) + y1

        return (
          ((x2 - this.x[i1]) * (this.y[i3] - this.y[i1])) /
            (this.x[i3] - this.x[i1]) +
          this.y[i1]
        );
      };

      return this;
    },
    // xxx: for now i will just save one spline function to to
    getInterpolateFunction: function getInterpolateFunction(c) {
      var swiper = this;

      if (!swiper.controller.spline) {
        swiper.controller.spline = swiper.params.loop
          ? new Controller.LinearSpline(swiper.slidesGrid, c.slidesGrid)
          : new Controller.LinearSpline(swiper.snapGrid, c.snapGrid);
      }
    },
    setTranslate: function setTranslate(setTranslate$1, byController) {
      var swiper = this;
      var controlled = swiper.controller.control;
      var multiplier;
      var controlledTranslate;

      function setControlledTranslate(c) {
        // this will create an Interpolate function based on the snapGrids
        // x is the Grid of the scrolled scroller and y will be the controlled scroller
        // it makes sense to create this only once and recall it for the interpolation
        // the function does a lot of value caching for performance
        var translate = swiper.rtlTranslate
          ? -swiper.translate
          : swiper.translate;

        if (swiper.params.controller.by === "slide") {
          swiper.controller.getInterpolateFunction(c); // i am not sure why the values have to be multiplicated this way, tried to invert the snapGrid
          // but it did not work out

          controlledTranslate = -swiper.controller.spline.interpolate(
            -translate
          );
        }

        if (
          !controlledTranslate ||
          swiper.params.controller.by === "container"
        ) {
          multiplier =
            (c.maxTranslate() - c.minTranslate()) /
            (swiper.maxTranslate() - swiper.minTranslate());
          controlledTranslate =
            (translate - swiper.minTranslate()) * multiplier + c.minTranslate();
        }

        if (swiper.params.controller.inverse) {
          controlledTranslate = c.maxTranslate() - controlledTranslate;
        }

        c.updateProgress(controlledTranslate);
        c.setTranslate(controlledTranslate, swiper);
        c.updateActiveIndex();
        c.updateSlidesClasses();
      }

      if (Array.isArray(controlled)) {
        for (var i = 0; i < controlled.length; i += 1) {
          if (
            controlled[i] !== byController &&
            controlled[i] instanceof Swiper
          ) {
            setControlledTranslate(controlled[i]);
          }
        }
      } else if (controlled instanceof Swiper && byController !== controlled) {
        setControlledTranslate(controlled);
      }
    },
    setTransition: function setTransition(duration, byController) {
      var swiper = this;
      var controlled = swiper.controller.control;
      var i;

      function setControlledTransition(c) {
        c.setTransition(duration, swiper);

        if (duration !== 0) {
          c.transitionStart();

          if (c.params.autoHeight) {
            Utils.nextTick(function () {
              c.updateAutoHeight();
            });
          }

          c.$wrapperEl.transitionEnd(function () {
            if (!controlled) {
              return;
            }

            if (c.params.loop && swiper.params.controller.by === "slide") {
              c.loopFix();
            }

            c.transitionEnd();
          });
        }
      }

      if (Array.isArray(controlled)) {
        for (i = 0; i < controlled.length; i += 1) {
          if (
            controlled[i] !== byController &&
            controlled[i] instanceof Swiper
          ) {
            setControlledTransition(controlled[i]);
          }
        }
      } else if (controlled instanceof Swiper && byController !== controlled) {
        setControlledTransition(controlled);
      }
    },
  };
  var Controller$1 = {
    name: "controller",
    params: {
      controller: {
        control: undefined,
        inverse: false,
        by: "slide", // or 'container'
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        controller: {
          control: swiper.params.controller.control,
          getInterpolateFunction: Controller.getInterpolateFunction.bind(
            swiper
          ),
          setTranslate: Controller.setTranslate.bind(swiper),
          setTransition: Controller.setTransition.bind(swiper),
        },
      });
    },
    on: {
      update: function update() {
        var swiper = this;

        if (!swiper.controller.control) {
          return;
        }

        if (swiper.controller.spline) {
          swiper.controller.spline = undefined;
          delete swiper.controller.spline;
        }
      },
      resize: function resize() {
        var swiper = this;

        if (!swiper.controller.control) {
          return;
        }

        if (swiper.controller.spline) {
          swiper.controller.spline = undefined;
          delete swiper.controller.spline;
        }
      },
      observerUpdate: function observerUpdate() {
        var swiper = this;

        if (!swiper.controller.control) {
          return;
        }

        if (swiper.controller.spline) {
          swiper.controller.spline = undefined;
          delete swiper.controller.spline;
        }
      },
      setTranslate: function setTranslate(translate, byController) {
        var swiper = this;

        if (!swiper.controller.control) {
          return;
        }

        swiper.controller.setTranslate(translate, byController);
      },
      setTransition: function setTransition(duration, byController) {
        var swiper = this;

        if (!swiper.controller.control) {
          return;
        }

        swiper.controller.setTransition(duration, byController);
      },
    },
  };
  var a11y = {
    makeElFocusable: function makeElFocusable($el) {
      $el.attr("tabIndex", "0");
      return $el;
    },
    addElRole: function addElRole($el, role) {
      $el.attr("role", role);
      return $el;
    },
    addElLabel: function addElLabel($el, label) {
      $el.attr("aria-label", label);
      return $el;
    },
    disableEl: function disableEl($el) {
      $el.attr("aria-disabled", true);
      return $el;
    },
    enableEl: function enableEl($el) {
      $el.attr("aria-disabled", false);
      return $el;
    },
    onEnterKey: function onEnterKey(e) {
      var swiper = this;
      var params = swiper.params.a11y;

      if (e.keyCode !== 13) {
        return;
      }

      var $targetEl = $(e.target);

      if (
        swiper.navigation &&
        swiper.navigation.$nextEl &&
        $targetEl.is(swiper.navigation.$nextEl)
      ) {
        if (!(swiper.isEnd && !swiper.params.loop)) {
          swiper.slideNext();
        }

        if (swiper.isEnd) {
          swiper.a11y.notify(params.lastSlideMessage);
        } else {
          swiper.a11y.notify(params.nextSlideMessage);
        }
      }

      if (
        swiper.navigation &&
        swiper.navigation.$prevEl &&
        $targetEl.is(swiper.navigation.$prevEl)
      ) {
        if (!(swiper.isBeginning && !swiper.params.loop)) {
          swiper.slidePrev();
        }

        if (swiper.isBeginning) {
          swiper.a11y.notify(params.firstSlideMessage);
        } else {
          swiper.a11y.notify(params.prevSlideMessage);
        }
      }

      if (
        swiper.pagination &&
        $targetEl.is("." + swiper.params.pagination.bulletClass)
      ) {
        $targetEl[0].click();
      }
    },
    notify: function notify(message) {
      var swiper = this;
      var notification = swiper.a11y.liveRegion;

      if (notification.length === 0) {
        return;
      }

      notification.html("");
      notification.html(message);
    },
    updateNavigation: function updateNavigation() {
      var swiper = this;

      if (swiper.params.loop) {
        return;
      }

      var ref = swiper.navigation;
      var $nextEl = ref.$nextEl;
      var $prevEl = ref.$prevEl;

      if ($prevEl && $prevEl.length > 0) {
        if (swiper.isBeginning) {
          swiper.a11y.disableEl($prevEl);
        } else {
          swiper.a11y.enableEl($prevEl);
        }
      }

      if ($nextEl && $nextEl.length > 0) {
        if (swiper.isEnd) {
          swiper.a11y.disableEl($nextEl);
        } else {
          swiper.a11y.enableEl($nextEl);
        }
      }
    },
    updatePagination: function updatePagination() {
      var swiper = this;
      var params = swiper.params.a11y;

      if (
        swiper.pagination &&
        swiper.params.pagination.clickable &&
        swiper.pagination.bullets &&
        swiper.pagination.bullets.length
      ) {
        swiper.pagination.bullets.each(function (bulletIndex, bulletEl) {
          var $bulletEl = $(bulletEl);
          swiper.a11y.makeElFocusable($bulletEl);
          swiper.a11y.addElRole($bulletEl, "button");
          swiper.a11y.addElLabel(
            $bulletEl,
            params.paginationBulletMessage.replace(
              /{{index}}/,
              $bulletEl.index() + 1
            )
          );
        });
      }
    },
    init: function init() {
      var swiper = this;
      swiper.$el.append(swiper.a11y.liveRegion); // Navigation

      var params = swiper.params.a11y;
      var $nextEl;
      var $prevEl;

      if (swiper.navigation && swiper.navigation.$nextEl) {
        $nextEl = swiper.navigation.$nextEl;
      }

      if (swiper.navigation && swiper.navigation.$prevEl) {
        $prevEl = swiper.navigation.$prevEl;
      }

      if ($nextEl) {
        swiper.a11y.makeElFocusable($nextEl);
        swiper.a11y.addElRole($nextEl, "button");
        swiper.a11y.addElLabel($nextEl, params.nextSlideMessage);
        $nextEl.on("keydown", swiper.a11y.onEnterKey);
      }

      if ($prevEl) {
        swiper.a11y.makeElFocusable($prevEl);
        swiper.a11y.addElRole($prevEl, "button");
        swiper.a11y.addElLabel($prevEl, params.prevSlideMessage);
        $prevEl.on("keydown", swiper.a11y.onEnterKey);
      } // Pagination

      if (
        swiper.pagination &&
        swiper.params.pagination.clickable &&
        swiper.pagination.bullets &&
        swiper.pagination.bullets.length
      ) {
        swiper.pagination.$el.on(
          "keydown",
          "." + swiper.params.pagination.bulletClass,
          swiper.a11y.onEnterKey
        );
      }
    },
    destroy: function destroy() {
      var swiper = this;

      if (swiper.a11y.liveRegion && swiper.a11y.liveRegion.length > 0) {
        swiper.a11y.liveRegion.remove();
      }

      var $nextEl;
      var $prevEl;

      if (swiper.navigation && swiper.navigation.$nextEl) {
        $nextEl = swiper.navigation.$nextEl;
      }

      if (swiper.navigation && swiper.navigation.$prevEl) {
        $prevEl = swiper.navigation.$prevEl;
      }

      if ($nextEl) {
        $nextEl.off("keydown", swiper.a11y.onEnterKey);
      }

      if ($prevEl) {
        $prevEl.off("keydown", swiper.a11y.onEnterKey);
      } // Pagination

      if (
        swiper.pagination &&
        swiper.params.pagination.clickable &&
        swiper.pagination.bullets &&
        swiper.pagination.bullets.length
      ) {
        swiper.pagination.$el.off(
          "keydown",
          "." + swiper.params.pagination.bulletClass,
          swiper.a11y.onEnterKey
        );
      }
    },
  };
  var A11y = {
    name: "a11y",
    params: {
      a11y: {
        enabled: true,
        notificationClass: "swiper-notification",
        prevSlideMessage: "Previous slide",
        nextSlideMessage: "Next slide",
        firstSlideMessage: "This is the first slide",
        lastSlideMessage: "This is the last slide",
        paginationBulletMessage: "Go to slide {{index}}",
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        a11y: {
          liveRegion: $(
            '<span class="' +
              swiper.params.a11y.notificationClass +
              '" aria-live="assertive" aria-atomic="true"></span>'
          ),
        },
      });
      Object.keys(a11y).forEach(function (methodName) {
        swiper.a11y[methodName] = a11y[methodName].bind(swiper);
      });
    },
    on: {
      init: function init() {
        var swiper = this;

        if (!swiper.params.a11y.enabled) {
          return;
        }

        swiper.a11y.init();
        swiper.a11y.updateNavigation();
      },
      toEdge: function toEdge() {
        var swiper = this;

        if (!swiper.params.a11y.enabled) {
          return;
        }

        swiper.a11y.updateNavigation();
      },
      fromEdge: function fromEdge() {
        var swiper = this;

        if (!swiper.params.a11y.enabled) {
          return;
        }

        swiper.a11y.updateNavigation();
      },
      paginationUpdate: function paginationUpdate() {
        var swiper = this;

        if (!swiper.params.a11y.enabled) {
          return;
        }

        swiper.a11y.updatePagination();
      },
      destroy: function destroy() {
        var swiper = this;

        if (!swiper.params.a11y.enabled) {
          return;
        }

        swiper.a11y.destroy();
      },
    },
  };
  var History = {
    init: function init() {
      var swiper = this;

      if (!swiper.params.history) {
        return;
      }

      if (!win.history || !win.history.pushState) {
        swiper.params.history.enabled = false;
        swiper.params.hashNavigation.enabled = true;
        return;
      }

      var history = swiper.history;
      history.initialized = true;
      history.paths = History.getPathValues();

      if (!history.paths.key && !history.paths.value) {
        return;
      }

      history.scrollToSlide(
        0,
        history.paths.value,
        swiper.params.runCallbacksOnInit
      );

      if (!swiper.params.history.replaceState) {
        win.addEventListener("popstate", swiper.history.setHistoryPopState);
      }
    },
    destroy: function destroy() {
      var swiper = this;

      if (!swiper.params.history.replaceState) {
        win.removeEventListener("popstate", swiper.history.setHistoryPopState);
      }
    },
    setHistoryPopState: function setHistoryPopState() {
      var swiper = this;
      swiper.history.paths = History.getPathValues();
      swiper.history.scrollToSlide(
        swiper.params.speed,
        swiper.history.paths.value,
        false
      );
    },
    getPathValues: function getPathValues() {
      var pathArray = win.location.pathname
        .slice(1)
        .split("/")
        .filter(function (part) {
          return part !== "";
        });
      var total = pathArray.length;
      var key = pathArray[total - 2];
      var value = pathArray[total - 1];
      return {
        key: key,
        value: value,
      };
    },
    setHistory: function setHistory(key, index) {
      var swiper = this;

      if (!swiper.history.initialized || !swiper.params.history.enabled) {
        return;
      }

      var slide = swiper.slides.eq(index);
      var value = History.slugify(slide.attr("data-history"));

      if (!win.location.pathname.includes(key)) {
        value = key + "/" + value;
      }

      var currentState = win.history.state;

      if (currentState && currentState.value === value) {
        return;
      }

      if (swiper.params.history.replaceState) {
        win.history.replaceState(
          {
            value: value,
          },
          null,
          value
        );
      } else {
        win.history.pushState(
          {
            value: value,
          },
          null,
          value
        );
      }
    },
    slugify: function slugify(text) {
      return text
        .toString()
        .replace(/\s+/g, "-")
        .replace(/[^\w-]+/g, "")
        .replace(/--+/g, "-")
        .replace(/^-+/, "")
        .replace(/-+$/, "");
    },
    scrollToSlide: function scrollToSlide(speed, value, runCallbacks) {
      var swiper = this;

      if (value) {
        for (var i = 0, length = swiper.slides.length; i < length; i += 1) {
          var slide = swiper.slides.eq(i);
          var slideHistory = History.slugify(slide.attr("data-history"));

          if (
            slideHistory === value &&
            !slide.hasClass(swiper.params.slideDuplicateClass)
          ) {
            var index = slide.index();
            swiper.slideTo(index, speed, runCallbacks);
          }
        }
      } else {
        swiper.slideTo(0, speed, runCallbacks);
      }
    },
  };
  var History$1 = {
    name: "history",
    params: {
      history: {
        enabled: false,
        replaceState: false,
        key: "slides",
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        history: {
          init: History.init.bind(swiper),
          setHistory: History.setHistory.bind(swiper),
          setHistoryPopState: History.setHistoryPopState.bind(swiper),
          scrollToSlide: History.scrollToSlide.bind(swiper),
          destroy: History.destroy.bind(swiper),
        },
      });
    },
    on: {
      init: function init() {
        var swiper = this;

        if (swiper.params.history.enabled) {
          swiper.history.init();
        }
      },
      destroy: function destroy() {
        var swiper = this;

        if (swiper.params.history.enabled) {
          swiper.history.destroy();
        }
      },
      transitionEnd: function transitionEnd() {
        var swiper = this;

        if (swiper.history.initialized) {
          swiper.history.setHistory(
            swiper.params.history.key,
            swiper.activeIndex
          );
        }
      },
    },
  };
  var HashNavigation = {
    onHashCange: function onHashCange() {
      var swiper = this;
      var newHash = doc.location.hash.replace("#", "");
      var activeSlideHash = swiper.slides
        .eq(swiper.activeIndex)
        .attr("data-hash");

      if (newHash !== activeSlideHash) {
        var newIndex = swiper.$wrapperEl
          .children(
            "." + swiper.params.slideClass + '[data-hash="' + newHash + '"]'
          )
          .index();

        if (typeof newIndex === "undefined") {
          return;
        }

        swiper.slideTo(newIndex);
      }
    },
    setHash: function setHash() {
      var swiper = this;

      if (
        !swiper.hashNavigation.initialized ||
        !swiper.params.hashNavigation.enabled
      ) {
        return;
      }

      if (
        swiper.params.hashNavigation.replaceState &&
        win.history &&
        win.history.replaceState
      ) {
        win.history.replaceState(
          null,
          null,
          "#" + swiper.slides.eq(swiper.activeIndex).attr("data-hash") || ""
        );
      } else {
        var slide = swiper.slides.eq(swiper.activeIndex);
        var hash = slide.attr("data-hash") || slide.attr("data-history");
        doc.location.hash = hash || "";
      }
    },
    init: function init() {
      var swiper = this;

      if (
        !swiper.params.hashNavigation.enabled ||
        (swiper.params.history && swiper.params.history.enabled)
      ) {
        return;
      }

      swiper.hashNavigation.initialized = true;
      var hash = doc.location.hash.replace("#", "");

      if (hash) {
        var speed = 0;

        for (var i = 0, length = swiper.slides.length; i < length; i += 1) {
          var slide = swiper.slides.eq(i);
          var slideHash = slide.attr("data-hash") || slide.attr("data-history");

          if (
            slideHash === hash &&
            !slide.hasClass(swiper.params.slideDuplicateClass)
          ) {
            var index = slide.index();
            swiper.slideTo(
              index,
              speed,
              swiper.params.runCallbacksOnInit,
              true
            );
          }
        }
      }

      if (swiper.params.hashNavigation.watchState) {
        $(win).on("hashchange", swiper.hashNavigation.onHashCange);
      }
    },
    destroy: function destroy() {
      var swiper = this;

      if (swiper.params.hashNavigation.watchState) {
        $(win).off("hashchange", swiper.hashNavigation.onHashCange);
      }
    },
  };
  var HashNavigation$1 = {
    name: "hash-navigation",
    params: {
      hashNavigation: {
        enabled: false,
        replaceState: false,
        watchState: false,
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        hashNavigation: {
          initialized: false,
          init: HashNavigation.init.bind(swiper),
          destroy: HashNavigation.destroy.bind(swiper),
          setHash: HashNavigation.setHash.bind(swiper),
          onHashCange: HashNavigation.onHashCange.bind(swiper),
        },
      });
    },
    on: {
      init: function init() {
        var swiper = this;

        if (swiper.params.hashNavigation.enabled) {
          swiper.hashNavigation.init();
        }
      },
      destroy: function destroy() {
        var swiper = this;

        if (swiper.params.hashNavigation.enabled) {
          swiper.hashNavigation.destroy();
        }
      },
      transitionEnd: function transitionEnd() {
        var swiper = this;

        if (swiper.hashNavigation.initialized) {
          swiper.hashNavigation.setHash();
        }
      },
    },
  };
  /* eslint no-underscore-dangle: "off" */

  var Autoplay = {
    run: function run() {
      var swiper = this;
      var $activeSlideEl = swiper.slides.eq(swiper.activeIndex);
      var delay = swiper.params.autoplay.delay;

      if ($activeSlideEl.attr("data-swiper-autoplay")) {
        delay =
          $activeSlideEl.attr("data-swiper-autoplay") ||
          swiper.params.autoplay.delay;
      }

      clearTimeout(swiper.autoplay.timeout);
      swiper.autoplay.timeout = Utils.nextTick(function () {
        if (swiper.params.autoplay.reverseDirection) {
          if (swiper.params.loop) {
            swiper.loopFix();
            swiper.slidePrev(swiper.params.speed, true, true);
            swiper.emit("autoplay");
          } else if (!swiper.isBeginning) {
            swiper.slidePrev(swiper.params.speed, true, true);
            swiper.emit("autoplay");
          } else if (!swiper.params.autoplay.stopOnLastSlide) {
            swiper.slideTo(
              swiper.slides.length - 1,
              swiper.params.speed,
              true,
              true
            );
            swiper.emit("autoplay");
          } else {
            swiper.autoplay.stop();
          }
        } else if (swiper.params.loop) {
          swiper.loopFix();
          swiper.slideNext(swiper.params.speed, true, true);
          swiper.emit("autoplay");
        } else if (!swiper.isEnd) {
          swiper.slideNext(swiper.params.speed, true, true);
          swiper.emit("autoplay");
        } else if (!swiper.params.autoplay.stopOnLastSlide) {
          swiper.slideTo(0, swiper.params.speed, true, true);
          swiper.emit("autoplay");
        } else {
          swiper.autoplay.stop();
        }
      }, delay);
    },
    start: function start() {
      var swiper = this;

      if (typeof swiper.autoplay.timeout !== "undefined") {
        return false;
      }

      if (swiper.autoplay.running) {
        return false;
      }

      swiper.autoplay.running = true;
      swiper.emit("autoplayStart");
      swiper.autoplay.run();
      return true;
    },
    stop: function stop() {
      var swiper = this;

      if (!swiper.autoplay.running) {
        return false;
      }

      if (typeof swiper.autoplay.timeout === "undefined") {
        return false;
      }

      if (swiper.autoplay.timeout) {
        clearTimeout(swiper.autoplay.timeout);
        swiper.autoplay.timeout = undefined;
      }

      swiper.autoplay.running = false;
      swiper.emit("autoplayStop");
      return true;
    },
    pause: function pause(speed) {
      var swiper = this;

      if (!swiper.autoplay.running) {
        return;
      }

      if (swiper.autoplay.paused) {
        return;
      }

      if (swiper.autoplay.timeout) {
        clearTimeout(swiper.autoplay.timeout);
      }

      swiper.autoplay.paused = true;

      if (speed === 0 || !swiper.params.autoplay.waitForTransition) {
        swiper.autoplay.paused = false;
        swiper.autoplay.run();
      } else {
        swiper.$wrapperEl[0].addEventListener(
          "transitionend",
          swiper.autoplay.onTransitionEnd
        );
        swiper.$wrapperEl[0].addEventListener(
          "webkitTransitionEnd",
          swiper.autoplay.onTransitionEnd
        );
      }
    },
  };
  var Autoplay$1 = {
    name: "autoplay",
    params: {
      autoplay: {
        enabled: false,
        delay: 3000,
        waitForTransition: true,
        disableOnInteraction: true,
        stopOnLastSlide: false,
        reverseDirection: false,
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        autoplay: {
          running: false,
          paused: false,
          run: Autoplay.run.bind(swiper),
          start: Autoplay.start.bind(swiper),
          stop: Autoplay.stop.bind(swiper),
          pause: Autoplay.pause.bind(swiper),
          onTransitionEnd: function onTransitionEnd(e) {
            if (!swiper || swiper.destroyed || !swiper.$wrapperEl) {
              return;
            }

            if (e.target !== this) {
              return;
            }

            swiper.$wrapperEl[0].removeEventListener(
              "transitionend",
              swiper.autoplay.onTransitionEnd
            );
            swiper.$wrapperEl[0].removeEventListener(
              "webkitTransitionEnd",
              swiper.autoplay.onTransitionEnd
            );
            swiper.autoplay.paused = false;

            if (!swiper.autoplay.running) {
              swiper.autoplay.stop();
            } else {
              swiper.autoplay.run();
            }
          },
        },
      });
    },
    on: {
      init: function init() {
        var swiper = this;

        if (swiper.params.autoplay.enabled) {
          swiper.autoplay.start();
        }
      },
      beforeTransitionStart: function beforeTransitionStart(speed, internal) {
        var swiper = this;

        if (swiper.autoplay.running) {
          if (internal || !swiper.params.autoplay.disableOnInteraction) {
            swiper.autoplay.pause(speed);
          } else {
            swiper.autoplay.stop();
          }
        }
      },
      sliderFirstMove: function sliderFirstMove() {
        var swiper = this;

        if (swiper.autoplay.running) {
          if (swiper.params.autoplay.disableOnInteraction) {
            swiper.autoplay.stop();
          } else {
            swiper.autoplay.pause();
          }
        }
      },
      destroy: function destroy() {
        var swiper = this;

        if (swiper.autoplay.running) {
          swiper.autoplay.stop();
        }
      },
    },
  };
  var Fade = {
    setTranslate: function setTranslate() {
      var swiper = this;
      var slides = swiper.slides;

      for (var i = 0; i < slides.length; i += 1) {
        var $slideEl = swiper.slides.eq(i);
        var offset = $slideEl[0].swiperSlideOffset;
        var tx = -offset;

        if (!swiper.params.virtualTranslate) {
          tx -= swiper.translate;
        }

        var ty = 0;

        if (!swiper.isHorizontal()) {
          ty = tx;
          tx = 0;
        }

        var slideOpacity = swiper.params.fadeEffect.crossFade
          ? Math.max(1 - Math.abs($slideEl[0].progress), 0)
          : 1 + Math.min(Math.max($slideEl[0].progress, -1), 0);
        $slideEl
          .css({
            opacity: slideOpacity,
          })
          .transform("translate3d(" + tx + "px, " + ty + "px, 0px)");
      }
    },
    setTransition: function setTransition(duration) {
      var swiper = this;
      var slides = swiper.slides;
      var $wrapperEl = swiper.$wrapperEl;
      slides.transition(duration);

      if (swiper.params.virtualTranslate && duration !== 0) {
        var eventTriggered = false;
        slides.transitionEnd(function () {
          if (eventTriggered) {
            return;
          }

          if (!swiper || swiper.destroyed) {
            return;
          }

          eventTriggered = true;
          swiper.animating = false;
          var triggerEvents = ["webkitTransitionEnd", "transitionend"];

          for (var i = 0; i < triggerEvents.length; i += 1) {
            $wrapperEl.trigger(triggerEvents[i]);
          }
        });
      }
    },
  };
  var EffectFade = {
    name: "effect-fade",
    params: {
      fadeEffect: {
        crossFade: false,
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        fadeEffect: {
          setTranslate: Fade.setTranslate.bind(swiper),
          setTransition: Fade.setTransition.bind(swiper),
        },
      });
    },
    on: {
      beforeInit: function beforeInit() {
        var swiper = this;

        if (swiper.params.effect !== "fade") {
          return;
        }

        swiper.classNames.push(swiper.params.containerModifierClass + "fade");
        var overwriteParams = {
          slidesPerView: 1,
          slidesPerColumn: 1,
          slidesPerGroup: 1,
          watchSlidesProgress: true,
          spaceBetween: 0,
          virtualTranslate: true,
        };
        Utils.extend(swiper.params, overwriteParams);
        Utils.extend(swiper.originalParams, overwriteParams);
      },
      setTranslate: function setTranslate() {
        var swiper = this;

        if (swiper.params.effect !== "fade") {
          return;
        }

        swiper.fadeEffect.setTranslate();
      },
      setTransition: function setTransition(duration) {
        var swiper = this;

        if (swiper.params.effect !== "fade") {
          return;
        }

        swiper.fadeEffect.setTransition(duration);
      },
    },
  };
  var Cube = {
    setTranslate: function setTranslate() {
      var swiper = this;
      var $el = swiper.$el;
      var $wrapperEl = swiper.$wrapperEl;
      var slides = swiper.slides;
      var swiperWidth = swiper.width;
      var swiperHeight = swiper.height;
      var rtl = swiper.rtlTranslate;
      var swiperSize = swiper.size;
      var params = swiper.params.cubeEffect;
      var isHorizontal = swiper.isHorizontal();
      var isVirtual = swiper.virtual && swiper.params.virtual.enabled;
      var wrapperRotate = 0;
      var $cubeShadowEl;

      if (params.shadow) {
        if (isHorizontal) {
          $cubeShadowEl = $wrapperEl.find(".swiper-cube-shadow");

          if ($cubeShadowEl.length === 0) {
            $cubeShadowEl = $('<div class="swiper-cube-shadow"></div>');
            $wrapperEl.append($cubeShadowEl);
          }

          $cubeShadowEl.css({
            height: swiperWidth + "px",
          });
        } else {
          $cubeShadowEl = $el.find(".swiper-cube-shadow");

          if ($cubeShadowEl.length === 0) {
            $cubeShadowEl = $('<div class="swiper-cube-shadow"></div>');
            $el.append($cubeShadowEl);
          }
        }
      }

      for (var i = 0; i < slides.length; i += 1) {
        var $slideEl = slides.eq(i);
        var slideIndex = i;

        if (isVirtual) {
          slideIndex = parseInt($slideEl.attr("data-swiper-slide-index"), 10);
        }

        var slideAngle = slideIndex * 90;
        var round = Math.floor(slideAngle / 360);

        if (rtl) {
          slideAngle = -slideAngle;
          round = Math.floor(-slideAngle / 360);
        }

        var progress = Math.max(Math.min($slideEl[0].progress, 1), -1);
        var tx = 0;
        var ty = 0;
        var tz = 0;

        if (slideIndex % 4 === 0) {
          tx = -round * 4 * swiperSize;
          tz = 0;
        } else if ((slideIndex - 1) % 4 === 0) {
          tx = 0;
          tz = -round * 4 * swiperSize;
        } else if ((slideIndex - 2) % 4 === 0) {
          tx = swiperSize + round * 4 * swiperSize;
          tz = swiperSize;
        } else if ((slideIndex - 3) % 4 === 0) {
          tx = -swiperSize;
          tz = 3 * swiperSize + swiperSize * 4 * round;
        }

        if (rtl) {
          tx = -tx;
        }

        if (!isHorizontal) {
          ty = tx;
          tx = 0;
        }

        var transform =
          "rotateX(" +
          (isHorizontal ? 0 : -slideAngle) +
          "deg) rotateY(" +
          (isHorizontal ? slideAngle : 0) +
          "deg) translate3d(" +
          tx +
          "px, " +
          ty +
          "px, " +
          tz +
          "px)";

        if (progress <= 1 && progress > -1) {
          wrapperRotate = slideIndex * 90 + progress * 90;

          if (rtl) {
            wrapperRotate = -slideIndex * 90 - progress * 90;
          }
        }

        $slideEl.transform(transform);

        if (params.slideShadows) {
          // Set shadows
          var shadowBefore = isHorizontal
            ? $slideEl.find(".swiper-slide-shadow-left")
            : $slideEl.find(".swiper-slide-shadow-top");
          var shadowAfter = isHorizontal
            ? $slideEl.find(".swiper-slide-shadow-right")
            : $slideEl.find(".swiper-slide-shadow-bottom");

          if (shadowBefore.length === 0) {
            shadowBefore = $(
              '<div class="swiper-slide-shadow-' +
                (isHorizontal ? "left" : "top") +
                '"></div>'
            );
            $slideEl.append(shadowBefore);
          }

          if (shadowAfter.length === 0) {
            shadowAfter = $(
              '<div class="swiper-slide-shadow-' +
                (isHorizontal ? "right" : "bottom") +
                '"></div>'
            );
            $slideEl.append(shadowAfter);
          }

          if (shadowBefore.length) {
            shadowBefore[0].style.opacity = Math.max(-progress, 0);
          }

          if (shadowAfter.length) {
            shadowAfter[0].style.opacity = Math.max(progress, 0);
          }
        }
      }

      $wrapperEl.css({
        "-webkit-transform-origin": "50% 50% -" + swiperSize / 2 + "px",
        "-moz-transform-origin": "50% 50% -" + swiperSize / 2 + "px",
        "-ms-transform-origin": "50% 50% -" + swiperSize / 2 + "px",
        "transform-origin": "50% 50% -" + swiperSize / 2 + "px",
      });

      if (params.shadow) {
        if (isHorizontal) {
          $cubeShadowEl.transform(
            "translate3d(0px, " +
              (swiperWidth / 2 + params.shadowOffset) +
              "px, " +
              -swiperWidth / 2 +
              "px) rotateX(90deg) rotateZ(0deg) scale(" +
              params.shadowScale +
              ")"
          );
        } else {
          var shadowAngle =
            Math.abs(wrapperRotate) -
            Math.floor(Math.abs(wrapperRotate) / 90) * 90;
          var multiplier =
            1.5 -
            (Math.sin((shadowAngle * 2 * Math.PI) / 360) / 2 +
              Math.cos((shadowAngle * 2 * Math.PI) / 360) / 2);
          var scale1 = params.shadowScale;
          var scale2 = params.shadowScale / multiplier;
          var offset = params.shadowOffset;
          $cubeShadowEl.transform(
            "scale3d(" +
              scale1 +
              ", 1, " +
              scale2 +
              ") translate3d(0px, " +
              (swiperHeight / 2 + offset) +
              "px, " +
              -swiperHeight / 2 / scale2 +
              "px) rotateX(-90deg)"
          );
        }
      }

      var zFactor =
        Browser.isSafari || Browser.isUiWebView ? -swiperSize / 2 : 0;
      $wrapperEl.transform(
        "translate3d(0px,0," +
          zFactor +
          "px) rotateX(" +
          (swiper.isHorizontal() ? 0 : wrapperRotate) +
          "deg) rotateY(" +
          (swiper.isHorizontal() ? -wrapperRotate : 0) +
          "deg)"
      );
    },
    setTransition: function setTransition(duration) {
      var swiper = this;
      var $el = swiper.$el;
      var slides = swiper.slides;
      slides
        .transition(duration)
        .find(
          ".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left"
        )
        .transition(duration);

      if (swiper.params.cubeEffect.shadow && !swiper.isHorizontal()) {
        $el.find(".swiper-cube-shadow").transition(duration);
      }
    },
  };
  var EffectCube = {
    name: "effect-cube",
    params: {
      cubeEffect: {
        slideShadows: true,
        shadow: true,
        shadowOffset: 20,
        shadowScale: 0.94,
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        cubeEffect: {
          setTranslate: Cube.setTranslate.bind(swiper),
          setTransition: Cube.setTransition.bind(swiper),
        },
      });
    },
    on: {
      beforeInit: function beforeInit() {
        var swiper = this;

        if (swiper.params.effect !== "cube") {
          return;
        }

        swiper.classNames.push(swiper.params.containerModifierClass + "cube");
        swiper.classNames.push(swiper.params.containerModifierClass + "3d");
        var overwriteParams = {
          slidesPerView: 1,
          slidesPerColumn: 1,
          slidesPerGroup: 1,
          watchSlidesProgress: true,
          resistanceRatio: 0,
          spaceBetween: 0,
          centeredSlides: false,
          virtualTranslate: true,
        };
        Utils.extend(swiper.params, overwriteParams);
        Utils.extend(swiper.originalParams, overwriteParams);
      },
      setTranslate: function setTranslate() {
        var swiper = this;

        if (swiper.params.effect !== "cube") {
          return;
        }

        swiper.cubeEffect.setTranslate();
      },
      setTransition: function setTransition(duration) {
        var swiper = this;

        if (swiper.params.effect !== "cube") {
          return;
        }

        swiper.cubeEffect.setTransition(duration);
      },
    },
  };
  var Flip = {
    setTranslate: function setTranslate() {
      var swiper = this;
      var slides = swiper.slides;
      var rtl = swiper.rtlTranslate;

      for (var i = 0; i < slides.length; i += 1) {
        var $slideEl = slides.eq(i);
        var progress = $slideEl[0].progress;

        if (swiper.params.flipEffect.limitRotation) {
          progress = Math.max(Math.min($slideEl[0].progress, 1), -1);
        }

        var offset = $slideEl[0].swiperSlideOffset;
        var rotate = -180 * progress;
        var rotateY = rotate;
        var rotateX = 0;
        var tx = -offset;
        var ty = 0;

        if (!swiper.isHorizontal()) {
          ty = tx;
          tx = 0;
          rotateX = -rotateY;
          rotateY = 0;
        } else if (rtl) {
          rotateY = -rotateY;
        }

        $slideEl[0].style.zIndex =
          -Math.abs(Math.round(progress)) + slides.length;

        if (swiper.params.flipEffect.slideShadows) {
          // Set shadows
          var shadowBefore = swiper.isHorizontal()
            ? $slideEl.find(".swiper-slide-shadow-left")
            : $slideEl.find(".swiper-slide-shadow-top");
          var shadowAfter = swiper.isHorizontal()
            ? $slideEl.find(".swiper-slide-shadow-right")
            : $slideEl.find(".swiper-slide-shadow-bottom");

          if (shadowBefore.length === 0) {
            shadowBefore = $(
              '<div class="swiper-slide-shadow-' +
                (swiper.isHorizontal() ? "left" : "top") +
                '"></div>'
            );
            $slideEl.append(shadowBefore);
          }

          if (shadowAfter.length === 0) {
            shadowAfter = $(
              '<div class="swiper-slide-shadow-' +
                (swiper.isHorizontal() ? "right" : "bottom") +
                '"></div>'
            );
            $slideEl.append(shadowAfter);
          }

          if (shadowBefore.length) {
            shadowBefore[0].style.opacity = Math.max(-progress, 0);
          }

          if (shadowAfter.length) {
            shadowAfter[0].style.opacity = Math.max(progress, 0);
          }
        }

        $slideEl.transform(
          "translate3d(" +
            tx +
            "px, " +
            ty +
            "px, 0px) rotateX(" +
            rotateX +
            "deg) rotateY(" +
            rotateY +
            "deg)"
        );
      }
    },
    setTransition: function setTransition(duration) {
      var swiper = this;
      var slides = swiper.slides;
      var activeIndex = swiper.activeIndex;
      var $wrapperEl = swiper.$wrapperEl;
      slides
        .transition(duration)
        .find(
          ".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left"
        )
        .transition(duration);

      if (swiper.params.virtualTranslate && duration !== 0) {
        var eventTriggered = false; // eslint-disable-next-line

        slides.eq(activeIndex).transitionEnd(function onTransitionEnd() {
          if (eventTriggered) {
            return;
          }

          if (!swiper || swiper.destroyed) {
            return;
          } // if (!$(this).hasClass(swiper.params.slideActiveClass)) return;

          eventTriggered = true;
          swiper.animating = false;
          var triggerEvents = ["webkitTransitionEnd", "transitionend"];

          for (var i = 0; i < triggerEvents.length; i += 1) {
            $wrapperEl.trigger(triggerEvents[i]);
          }
        });
      }
    },
  };
  var EffectFlip = {
    name: "effect-flip",
    params: {
      flipEffect: {
        slideShadows: true,
        limitRotation: true,
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        flipEffect: {
          setTranslate: Flip.setTranslate.bind(swiper),
          setTransition: Flip.setTransition.bind(swiper),
        },
      });
    },
    on: {
      beforeInit: function beforeInit() {
        var swiper = this;

        if (swiper.params.effect !== "flip") {
          return;
        }

        swiper.classNames.push(swiper.params.containerModifierClass + "flip");
        swiper.classNames.push(swiper.params.containerModifierClass + "3d");
        var overwriteParams = {
          slidesPerView: 1,
          slidesPerColumn: 1,
          slidesPerGroup: 1,
          watchSlidesProgress: true,
          spaceBetween: 0,
          virtualTranslate: true,
        };
        Utils.extend(swiper.params, overwriteParams);
        Utils.extend(swiper.originalParams, overwriteParams);
      },
      setTranslate: function setTranslate() {
        var swiper = this;

        if (swiper.params.effect !== "flip") {
          return;
        }

        swiper.flipEffect.setTranslate();
      },
      setTransition: function setTransition(duration) {
        var swiper = this;

        if (swiper.params.effect !== "flip") {
          return;
        }

        swiper.flipEffect.setTransition(duration);
      },
    },
  };
  var Coverflow = {
    setTranslate: function setTranslate() {
      var swiper = this;
      var swiperWidth = swiper.width;
      var swiperHeight = swiper.height;
      var slides = swiper.slides;
      var $wrapperEl = swiper.$wrapperEl;
      var slidesSizesGrid = swiper.slidesSizesGrid;
      var params = swiper.params.coverflowEffect;
      var isHorizontal = swiper.isHorizontal();
      var transform = swiper.translate;
      var center = isHorizontal
        ? -transform + swiperWidth / 2
        : -transform + swiperHeight / 2;
      var rotate = isHorizontal ? params.rotate : -params.rotate;
      var translate = params.depth; // Each slide offset from center

      for (var i = 0, length = slides.length; i < length; i += 1) {
        var $slideEl = slides.eq(i);
        var slideSize = slidesSizesGrid[i];
        var slideOffset = $slideEl[0].swiperSlideOffset;
        var offsetMultiplier =
          ((center - slideOffset - slideSize / 2) / slideSize) *
          params.modifier;
        var rotateY = isHorizontal ? rotate * offsetMultiplier : 0;
        var rotateX = isHorizontal ? 0 : rotate * offsetMultiplier; // var rotateZ = 0

        var translateZ = -translate * Math.abs(offsetMultiplier);
        var translateY = isHorizontal ? 0 : params.stretch * offsetMultiplier;
        var translateX = isHorizontal ? params.stretch * offsetMultiplier : 0; // Fix for ultra small values

        if (Math.abs(translateX) < 0.001) {
          translateX = 0;
        }

        if (Math.abs(translateY) < 0.001) {
          translateY = 0;
        }

        if (Math.abs(translateZ) < 0.001) {
          translateZ = 0;
        }

        if (Math.abs(rotateY) < 0.001) {
          rotateY = 0;
        }

        if (Math.abs(rotateX) < 0.001) {
          rotateX = 0;
        }

        var slideTransform =
          "translate3d(" +
          translateX +
          "px," +
          translateY +
          "px," +
          translateZ +
          "px)  rotateX(" +
          rotateX +
          "deg) rotateY(" +
          rotateY +
          "deg)";
        $slideEl.transform(slideTransform);
        $slideEl[0].style.zIndex = -Math.abs(Math.round(offsetMultiplier)) + 1;

        if (params.slideShadows) {
          // Set shadows
          var $shadowBeforeEl = isHorizontal
            ? $slideEl.find(".swiper-slide-shadow-left")
            : $slideEl.find(".swiper-slide-shadow-top");
          var $shadowAfterEl = isHorizontal
            ? $slideEl.find(".swiper-slide-shadow-right")
            : $slideEl.find(".swiper-slide-shadow-bottom");

          if ($shadowBeforeEl.length === 0) {
            $shadowBeforeEl = $(
              '<div class="swiper-slide-shadow-' +
                (isHorizontal ? "left" : "top") +
                '"></div>'
            );
            $slideEl.append($shadowBeforeEl);
          }

          if ($shadowAfterEl.length === 0) {
            $shadowAfterEl = $(
              '<div class="swiper-slide-shadow-' +
                (isHorizontal ? "right" : "bottom") +
                '"></div>'
            );
            $slideEl.append($shadowAfterEl);
          }

          if ($shadowBeforeEl.length) {
            $shadowBeforeEl[0].style.opacity =
              offsetMultiplier > 0 ? offsetMultiplier : 0;
          }

          if ($shadowAfterEl.length) {
            $shadowAfterEl[0].style.opacity =
              -offsetMultiplier > 0 ? -offsetMultiplier : 0;
          }
        }
      } // Set correct perspective for IE10

      if (Support.pointerEvents || Support.prefixedPointerEvents) {
        var ws = $wrapperEl[0].style;
        ws.perspectiveOrigin = center + "px 50%";
      }
    },
    setTransition: function setTransition(duration) {
      var swiper = this;
      swiper.slides
        .transition(duration)
        .find(
          ".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left"
        )
        .transition(duration);
    },
  };
  var EffectCoverflow = {
    name: "effect-coverflow",
    params: {
      coverflowEffect: {
        rotate: 50,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows: true,
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        coverflowEffect: {
          setTranslate: Coverflow.setTranslate.bind(swiper),
          setTransition: Coverflow.setTransition.bind(swiper),
        },
      });
    },
    on: {
      beforeInit: function beforeInit() {
        var swiper = this;

        if (swiper.params.effect !== "coverflow") {
          return;
        }

        swiper.classNames.push(
          swiper.params.containerModifierClass + "coverflow"
        );
        swiper.classNames.push(swiper.params.containerModifierClass + "3d");
        swiper.params.watchSlidesProgress = true;
        swiper.originalParams.watchSlidesProgress = true;
      },
      setTranslate: function setTranslate() {
        var swiper = this;

        if (swiper.params.effect !== "coverflow") {
          return;
        }

        swiper.coverflowEffect.setTranslate();
      },
      setTransition: function setTransition(duration) {
        var swiper = this;

        if (swiper.params.effect !== "coverflow") {
          return;
        }

        swiper.coverflowEffect.setTransition(duration);
      },
    },
  };
  var Thumbs = {
    init: function init() {
      var swiper = this;
      var ref = swiper.params;
      var thumbsParams = ref.thumbs;
      var SwiperClass = swiper.constructor;

      if (thumbsParams.swiper instanceof SwiperClass) {
        swiper.thumbs.swiper = thumbsParams.swiper;
        Utils.extend(swiper.thumbs.swiper.originalParams, {
          watchSlidesProgress: true,
          slideToClickedSlide: false,
        });
        Utils.extend(swiper.thumbs.swiper.params, {
          watchSlidesProgress: true,
          slideToClickedSlide: false,
        });
      } else if (Utils.isObject(thumbsParams.swiper)) {
        swiper.thumbs.swiper = new SwiperClass(
          Utils.extend({}, thumbsParams.swiper, {
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            slideToClickedSlide: false,
          })
        );
        swiper.thumbs.swiperCreated = true;
      }

      swiper.thumbs.swiper.$el.addClass(
        swiper.params.thumbs.thumbsContainerClass
      );
      swiper.thumbs.swiper.on("tap", swiper.thumbs.onThumbClick);
    },
    onThumbClick: function onThumbClick() {
      var swiper = this;
      var thumbsSwiper = swiper.thumbs.swiper;

      if (!thumbsSwiper) {
        return;
      }

      var clickedIndex = thumbsSwiper.clickedIndex;
      var clickedSlide = thumbsSwiper.clickedSlide;

      if (
        clickedSlide &&
        $(clickedSlide).hasClass(swiper.params.thumbs.slideThumbActiveClass)
      ) {
        return;
      }

      if (typeof clickedIndex === "undefined" || clickedIndex === null) {
        return;
      }

      var slideToIndex;

      if (thumbsSwiper.params.loop) {
        slideToIndex = parseInt(
          $(thumbsSwiper.clickedSlide).attr("data-swiper-slide-index"),
          10
        );
      } else {
        slideToIndex = clickedIndex;
      }

      if (swiper.params.loop) {
        var currentIndex = swiper.activeIndex;

        if (
          swiper.slides
            .eq(currentIndex)
            .hasClass(swiper.params.slideDuplicateClass)
        ) {
          swiper.loopFix(); // eslint-disable-next-line

          swiper._clientLeft = swiper.$wrapperEl[0].clientLeft;
          currentIndex = swiper.activeIndex;
        }

        var prevIndex = swiper.slides
          .eq(currentIndex)
          .prevAll('[data-swiper-slide-index="' + slideToIndex + '"]')
          .eq(0)
          .index();
        var nextIndex = swiper.slides
          .eq(currentIndex)
          .nextAll('[data-swiper-slide-index="' + slideToIndex + '"]')
          .eq(0)
          .index();

        if (typeof prevIndex === "undefined") {
          slideToIndex = nextIndex;
        } else if (typeof nextIndex === "undefined") {
          slideToIndex = prevIndex;
        } else if (nextIndex - currentIndex < currentIndex - prevIndex) {
          slideToIndex = nextIndex;
        } else {
          slideToIndex = prevIndex;
        }
      }

      swiper.slideTo(slideToIndex);
    },
    update: function update(initial) {
      var swiper = this;
      var thumbsSwiper = swiper.thumbs.swiper;

      if (!thumbsSwiper) {
        return;
      }

      var slidesPerView =
        thumbsSwiper.params.slidesPerView === "auto"
          ? thumbsSwiper.slidesPerViewDynamic()
          : thumbsSwiper.params.slidesPerView;

      if (swiper.realIndex !== thumbsSwiper.realIndex) {
        var currentThumbsIndex = thumbsSwiper.activeIndex;
        var newThumbsIndex;

        if (thumbsSwiper.params.loop) {
          if (
            thumbsSwiper.slides
              .eq(currentThumbsIndex)
              .hasClass(thumbsSwiper.params.slideDuplicateClass)
          ) {
            thumbsSwiper.loopFix(); // eslint-disable-next-line

            thumbsSwiper._clientLeft = thumbsSwiper.$wrapperEl[0].clientLeft;
            currentThumbsIndex = thumbsSwiper.activeIndex;
          } // Find actual thumbs index to slide to

          var prevThumbsIndex = thumbsSwiper.slides
            .eq(currentThumbsIndex)
            .prevAll('[data-swiper-slide-index="' + swiper.realIndex + '"]')
            .eq(0)
            .index();
          var nextThumbsIndex = thumbsSwiper.slides
            .eq(currentThumbsIndex)
            .nextAll('[data-swiper-slide-index="' + swiper.realIndex + '"]')
            .eq(0)
            .index();

          if (typeof prevThumbsIndex === "undefined") {
            newThumbsIndex = nextThumbsIndex;
          } else if (typeof nextThumbsIndex === "undefined") {
            newThumbsIndex = prevThumbsIndex;
          } else if (
            nextThumbsIndex - currentThumbsIndex ===
            currentThumbsIndex - prevThumbsIndex
          ) {
            newThumbsIndex = currentThumbsIndex;
          } else if (
            nextThumbsIndex - currentThumbsIndex <
            currentThumbsIndex - prevThumbsIndex
          ) {
            newThumbsIndex = nextThumbsIndex;
          } else {
            newThumbsIndex = prevThumbsIndex;
          }
        } else {
          newThumbsIndex = swiper.realIndex;
        }

        if (
          thumbsSwiper.visibleSlidesIndexes &&
          thumbsSwiper.visibleSlidesIndexes.indexOf(newThumbsIndex) < 0
        ) {
          if (thumbsSwiper.params.centeredSlides) {
            if (newThumbsIndex > currentThumbsIndex) {
              newThumbsIndex =
                newThumbsIndex - Math.floor(slidesPerView / 2) + 1;
            } else {
              newThumbsIndex =
                newThumbsIndex + Math.floor(slidesPerView / 2) - 1;
            }
          } else if (newThumbsIndex > currentThumbsIndex) {
            newThumbsIndex = newThumbsIndex - slidesPerView + 1;
          }

          thumbsSwiper.slideTo(newThumbsIndex, initial ? 0 : undefined);
        }
      } // Activate thumbs

      var thumbsToActivate = 1;
      var thumbActiveClass = swiper.params.thumbs.slideThumbActiveClass;

      if (swiper.params.slidesPerView > 1 && !swiper.params.centeredSlides) {
        thumbsToActivate = swiper.params.slidesPerView;
      }

      thumbsSwiper.slides.removeClass(thumbActiveClass);

      if (thumbsSwiper.params.loop || thumbsSwiper.params.virtual) {
        for (var i = 0; i < thumbsToActivate; i += 1) {
          thumbsSwiper.$wrapperEl
            .children(
              '[data-swiper-slide-index="' + (swiper.realIndex + i) + '"]'
            )
            .addClass(thumbActiveClass);
        }
      } else {
        for (var i$1 = 0; i$1 < thumbsToActivate; i$1 += 1) {
          thumbsSwiper.slides
            .eq(swiper.realIndex + i$1)
            .addClass(thumbActiveClass);
        }
      }
    },
  };
  var Thumbs$1 = {
    name: "thumbs",
    params: {
      thumbs: {
        swiper: null,
        slideThumbActiveClass: "swiper-slide-thumb-active",
        thumbsContainerClass: "swiper-container-thumbs",
      },
    },
    create: function create() {
      var swiper = this;
      Utils.extend(swiper, {
        thumbs: {
          swiper: null,
          init: Thumbs.init.bind(swiper),
          update: Thumbs.update.bind(swiper),
          onThumbClick: Thumbs.onThumbClick.bind(swiper),
        },
      });
    },
    on: {
      beforeInit: function beforeInit() {
        var swiper = this;
        var ref = swiper.params;
        var thumbs = ref.thumbs;

        if (!thumbs || !thumbs.swiper) {
          return;
        }

        swiper.thumbs.init();
        swiper.thumbs.update(true);
      },
      slideChange: function slideChange() {
        var swiper = this;

        if (!swiper.thumbs.swiper) {
          return;
        }

        swiper.thumbs.update();
      },
      update: function update() {
        var swiper = this;

        if (!swiper.thumbs.swiper) {
          return;
        }

        swiper.thumbs.update();
      },
      resize: function resize() {
        var swiper = this;

        if (!swiper.thumbs.swiper) {
          return;
        }

        swiper.thumbs.update();
      },
      observerUpdate: function observerUpdate() {
        var swiper = this;

        if (!swiper.thumbs.swiper) {
          return;
        }

        swiper.thumbs.update();
      },
      setTransition: function setTransition(duration) {
        var swiper = this;
        var thumbsSwiper = swiper.thumbs.swiper;

        if (!thumbsSwiper) {
          return;
        }

        thumbsSwiper.setTransition(duration);
      },
      beforeDestroy: function beforeDestroy() {
        var swiper = this;
        var thumbsSwiper = swiper.thumbs.swiper;

        if (!thumbsSwiper) {
          return;
        }

        if (swiper.thumbs.swiperCreated && thumbsSwiper) {
          thumbsSwiper.destroy();
        }
      },
    },
  }; // Swiper Class

  var components = [
    Device$1,
    Support$1,
    Browser$1,
    Resize,
    Observer$1,
    Virtual$1,
    Keyboard$1,
    Mousewheel$1,
    Navigation$1,
    Pagination$1,
    Scrollbar$1,
    Parallax$1,
    Zoom$1,
    Lazy$1,
    Controller$1,
    A11y,
    History$1,
    HashNavigation$1,
    Autoplay$1,
    EffectFade,
    EffectCube,
    EffectFlip,
    EffectCoverflow,
    Thumbs$1,
  ];

  if (typeof Swiper.use === "undefined") {
    Swiper.use = Swiper.Class.use;
    Swiper.installModule = Swiper.Class.installModule;
  }

  Swiper.use(components);
  return Swiper;
});
// abctesting

var _ref,
  _ref2,
  _ref3,
  _ref4,
  _ref5,
  _ref6,
  _ref7,
  _ref8,
  _ref9,
  _ref10,
  _ref11,
  _ref12,
  _ref13,
  _ref14,
  _ref15,
  _ref16,
  _ref17,
  _ref18,
  _ref19,
  _ref20,
  _ref21,
  _ref22,
  _ref23,
  _ref24,
  _ref25,
  _ref26,
  _ref27,
  _ref28,
  _ref29,
  _ref30,
  _ref31,
  _ref32,
  _ref33,
  _ref34,
  _ref35,
  _ref36,
  _ref37,
  _ref38,
  _ref39,
  _ref40,
  _ref41,
  _ref42,
  _ref43,
  _ref44,
  _ref45,
  _ref46,
  _ref47,
  _ref48,
  _ref49,
  _ref50,
  _ref51,
  _ref52,
  _ref53,
  _ref54,
  _ref55,
  _ref56,
  _ref57,
  _ref58,
  _ref59;

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true,
    });
  } else {
    obj[key] = value;
  }
  return obj;
}

var tagA = function tagA(url) {
  var text =
    arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  return '<a href="'
    .concat(url, '" target="_blank" >')
    .concat(text ? text : url, "</a>");
};

var modalx = [
  {
    product: 1,
    category: 1,
    model: 1,
  },
  {
    product: 2,
    category: 2,
    model: 2,
  },
  {
    product: 3,
    category: 3,
    model: 3,
  },
  {
    product: 4,
    category: 4,
    model: 4,
  },
  {
    product: 5,
    category: 5,
    model: 5,
  },
  {
    product: 6,
    category: 6,
    model: 6,
  },
  {
    product: 7,
    category: 7,
    model: 7,
  },
  {
    product: 8,
    category: 8,
    model: 8,
  },
  {
    product: 9,
    category: 9,
    model: 9,
  },
  {
    product: 10,
    category: 10,
    model: 10,
  },
  {
    product: 11,
    category: 11,
    model: 11,
  },
  {
    product: 12,
    category: 12,
    model: 12,
  },
  {
    product: 13,
    category: 13,
    model: 13,
  },
  {
    product: 14,
    category: 14,
    model: 14,
  },
  {
    product: 15,
    category: 15,
    model: 15,
  },
  {
    product: 16,
    category: 16,
    model: 16,
  },
  {
    product: 17,
    category: 17,
    model: 17,
  },
  {
    product: 18,
    category: 18,
    model: 18,
  },
  {
    product: 19,
    category: 19,
    model: 19,
  },
  {
    product: 20,
    category: 20,
    model: 20,
  },
  {
    product: 21,
    category: 21,
    model: 21,
  },
  {
    product: 22,
    category: 22,
    model: 22,
  },
  {
    product: 23,
    category: 23,
    model: 23,
  },
  {
    product: 24,
    category: 24,
    model: 24,
  },
  {
    product: 25,
    category: 25,
    model: 25,
  },
  {
    product: 26,
    category: 26,
    model: 26,
  },
  {
    product: 27,
    category: 27,
    model: 27,
  },
  {
    product: 28,
    category: 28,
    model: 28,
  },
  {
    product: 29,
    category: 29,
    model: 29,
  },
  {
    product: 30,
    category: 30,
    model: 30,
  },
  {
    product: 31,
    category: 31,
    model: 31,
  },
  {
    product: 32,
    category: 32,
    model: 32,
  },
  {
    product: 33,
    category: 33,
    model: 33,
  },
  {
    product: 34,
    category: 34,
    model: 34,
  },
  {
    product: 35,
    category: 35,
    model: 35,
  },
  {
    product: 36,
    category: 36,
    model: 36,
  },
  {
    product: 37,
    category: 37,
    model: 37,
  },
  {
    product: 38,
    category: 38,
    model: 38,
  },
  {
    product: 39,
    category: 39,
    model: 39,
  },
];
var modalx2 = [
  {
    product: 1,
    category: 1,
    model: 1,
  },
];
var modalItems = [
  {
    modalName: "modalx",
    modal: modalx,
  },
  {
    modalName: "modalx-2",
    modal: modalx2,
  },
];
var products = [
  {
    key: 1,
    name: "ทีวี",
  },
  {
    key: 2,
    name: "ทีวี",
  },
  {
    key: 3,
    name: "ทีวี",
  },
  {
    key: 4,
    name: "ทีวี",
  },
  {
    key: 5,
    name: "ทีวี",
  },
  {
    key: 6,
    name: "ทีวี",
  },
  {
    key: 7,
    name: "ทีวี",
  },
  {
    key: 8,
    name: "ทีวี",
  },
  {
    key: 9,
    name: "ทีวี",
  },
  {
    key: 10,
    name: "ทีวี",
  },
  {
    key: 11,
    name: "ทีวี",
  },
  {
    key: 12,
    name: "ทีวี",
  },
  {
    key: 13,
    name: "ทีวี",
  },
  {
    key: 14,
    name: "ทีวี",
  },
  {
    key: 15,
    name: "ทีวี",
  },
  {
    key: 16,
    name: "ทีวี",
  },
  {
    key: 17,
    name: "ทีวี",
  },
  {
    key: 18,
    name: "ทีวี",
  },
  {
    key: 19,
    name: "ทีวี",
  },
  {
    key: 20,
    name: "ทีวี",
  },
  {
    key: 21,
    name: "ทีวี",
  },
  {
    key: 22,
    name: "ทีวี",
  },
  {
    key: 23,
    name: "ทีวี",
  },
  {
    key: 24,
    name: "ทีวี",
  },
  {
    key: 25,
    name: "ทีวี",
  },
  {
    key: 26,
    name: "ทีวี",
  },
  {
    key: 27,
    name: "ทีวี",
  },
  {
    key: 28,
    name: "ทีวี",
  },
  {
    key: 29,
    name: "ทีวี",
  },
  {
    key: 30,
    name: "ทีวี",
  },
  {
    key: 31,
    name: "ทีวี",
  },
  {
    key: 32,
    name: "ทีวี",
  },
  {
    key: 33,
    name: "ทีวี",
  },
  {
    key: 34,
    name: "ทีวี",
  },
  {
    key: 35,
    name: "ทีวี",
  },
  {
    key: 36,
    name: "ทีวี",
  },
  {
    key: 37,
    name: "ตู้เย็น",
  },
  {
    key: 38,
    name: "ตู้เย็น",
  },
  {
    key: 39,
    name: "เครื่องทำความสะอาดและรีดผ้า",
  },
];
var categories = [
  {
    key: 1,
    name: "Q950TS QLED 8K (2020)",
  },
  {
    key: 2,
    name: "Q950TS QLED 8K (2020)",
  },
  {
    key: 3,
    name: "Q950TS QLED 8K (2020)",
  },
  {
    key: 4,
    name: "Q800T QLED 8K (2020)",
  },
  {
    key: 5,
    name: "Q800T QLED 8K (2020)",
  },
  {
    key: 6,
    name: "Q800T QLED 8K (2020)",
  },
  {
    key: 7,
    name: "Q900R QLED 8K (2019)",
  },
  {
    key: 8,
    name: "Q900R QLED 8K (2019)",
  },
  {
    key: 9,
    name: "Q900R QLED 8K (2019)",
  },
  {
    key: 10,
    name: "Q900R QLED 8K (2019)",
  },
  {
    key: 11,
    name: "Q900R QLED 8K (2019)",
  },
  {
    key: 12,
    name: "Q80T QLED 4K (2020)",
  },
  {
    key: 13,
    name: "Q80T QLED 4K (2020)",
  },
  {
    key: 14,
    name: "Q80T QLED 4K (2020)",
  },
  {
    key: 15,
    name: "Q80T QLED 4K (2020)",
  },
  {
    key: 16,
    name: "Q95T QLED 4K (2020)",
  },
  {
    key: 17,
    name: "Q95T QLED 4K (2020)",
  },
  {
    key: 18,
    name: "Q70T QLED 4K (2020)",
  },
  {
    key: 19,
    name: "Q70T QLED 4K (2020)",
  },
  {
    key: 20,
    name: "Q70T QLED 4K (2020)",
  },
  {
    key: 21,
    name: "Q70T QLED 4K (2020)",
  },
  {
    key: 22,
    name: "Q65T QLED 4K (2020)",
  },
  {
    key: 23,
    name: "Q65T QLED 4K (2020)",
  },
  {
    key: 24,
    name: "Q60T QLED 4K (2020)",
  },
  {
    key: 25,
    name: "Q60T QLED 4K (2020)",
  },
  {
    key: 26,
    name: "Q60T QLED 4K (2020)",
  },
  {
    key: 27,
    name: "Q60T QLED 4K (2020)",
  },
  {
    key: 28,
    name: "Q60T QLED 4K (2020)",
  },
  {
    key: 29,
    name: "Q60T QLED 4K (2020)",
  },
  {
    key: 30,
    name: "Q60T QLED 4K (2020)",
  },
  {
    key: 31,
    name: "The Frame Smart 4K (2020)",
  },
  {
    key: 32,
    name: "The Frame Smart 4K (2020)",
  },
  {
    key: 33,
    name: "The Frame Smart 4K (2020)",
  },
  {
    key: 34,
    name: "The Serif 4K Smart TV (2020)",
  },
  {
    key: 35,
    name: "The Serif 4K Smart TV (2020)",
  },
  {
    key: 36,
    name: "The Serif 4K Smart TV (2020)",
  },
  {
    key: 37,
    name: "Family Hub",
  },
  {
    key: 38,
    name: "Family Hub",
  },
  {
    key: 38,
    name: "AirDresser",
  },
];
var models = [
  {
    key: 1,
    name: "QA65Q950TSKXXT",
  },
  {
    key: 2,
    name: "QA75Q950TSKXXT",
  },
  {
    key: 3,
    name: "QA85Q950TSKXXT",
  },
  {
    key: 4,
    name: "QA65Q800TAKXXT",
  },
  {
    key: 5,
    name: "QA75Q800TAKXXT",
  },
  {
    key: 6,
    name: "QA82Q800TAKXXT",
  },
  {
    key: 7,
    name: "QA55Q900RBKXXT",
  },
  {
    key: 8,
    name: "QA65Q900RBKXXT",
  },
  {
    key: 9,
    name: "QA75Q900RBKXXT",
  },
  {
    key: 10,
    name: "QA82Q900RBKXXT",
  },
  {
    key: 11,
    name: "QA98Q900RBKXXT",
  },
  {
    key: 12,
    name: "QA55Q80TAKXXT",
  },
  {
    key: 13,
    name: "QA65Q80TAKXXT",
  },
  {
    key: 14,
    name: "QA75Q80TAKXXT",
  },
  {
    key: 15,
    name: "QA85Q80TAKXXT",
  },
  {
    key: 16,
    name: "QA65Q95TAKXXT",
  },
  {
    key: 17,
    name: "QA75Q95TAKXXT",
  },
  {
    key: 18,
    name: "QA55Q70TAKXXT",
  },
  {
    key: 19,
    name: "QA65Q70TAKXXT",
  },
  {
    key: 20,
    name: "QA75Q70TAKXXT",
  },
  {
    key: 21,
    name: "QA85Q70TAKXXT",
  },
  {
    key: 22,
    name: "QA55Q65TAKXXT",
  },
  {
    key: 23,
    name: "QA65Q65TAKXXT",
  },
  {
    key: 24,
    name: "QA43Q60TAKXXT",
  },
  {
    key: 25,
    name: "QA50Q60TAKXXT",
  },
  {
    key: 26,
    name: "QA55Q60TAKXXT",
  },
  {
    key: 27,
    name: "QA58Q60TAKXXT",
  },
  {
    key: 28,
    name: "QA65Q60TAKXXT",
  },
  {
    key: 29,
    name: "QA75Q60TAKXXT",
  },
  {
    key: 30,
    name: "QA85Q60TAKXXT",
  },
  {
    key: 31,
    name: "QA32LS03TBKXXT",
  },
  {
    key: 32,
    name: "QA55LS03TAKXXT",
  },
  {
    key: 33,
    name: "QA65LS03TAKXXT",
  },
  {
    key: 34,
    name: "QA43LS01TAKXXT",
  },
  {
    key: 35,
    name: "QA55LS01TAKXXT",
  },
  {
    key: 36,
    name: "QA43LS05TAKXXT",
  },
  {
    key: 37,
    name: "RF56N9740SG/ST ",
  },
  {
    key: 38,
    name: "RS64T5F01B4/ST",
  },
  {
    key: 39,
    name: "DF60R8600CG/ST",
  },
];
var items = {
  mobile: {
    Unbox: [
      {
        no: "01",
        head: "-",
        text: "-",
        image: "unbox-01_01",
        image2: "unbox-mb-01_01",
        title: "แกะกล่องเครื่องใหม่",
      },
      {
        no: "02",
        head: "-",
        text: "-",
        image: "unbox-01_02",
        image2: "unbox-mb-01_02",
        title: "At Your Service",
        mapName: "at-service",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง At Your Service เพื่อเป็นข้อมูล ประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "serviceat",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "03",
        head: "-",
        text: "-",
        image: "unbox-01_03",
        image2: "unbox-mb-01_03",
        title: "Samsung Members",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      }, // {
      //   no: "04",
      //   head: "-",
      //   text: "-",
      //   image: "unbox-01_04",
      //   image2: "unbox-mb-01_04",
      //   url: "serice-btn",
      //   title: "Extended Warranty",
      //   condition:
      //     "1. ลูกค้าต้องลงทะเบียนผ่านเว็บไซต์ SSTHWARRANTY  เพื่อเอาประกันเท่านั้น <br> 2. ระยะเวลาที่ขยายการรับประกันสินค้า ขึ้นอยู่กับรุ่นของสินค้าและโปรโมชั่นขณะนั้น <br> 3. การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      // },
    ],
    Inquiry: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-02_01",
        image2: "unbox-mb-02_01",
        title: "เครื่องใหม่ ยังไม่ชิน / ต้องการแก้ไขปัญหาเริ่มต้น? ",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "unbox-02_02",
        image2: "unbox-mb-02_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "unbox-02_03",
        image2: "unbox-mb-02_03",
        title: "Live Chat",
        mapName: "livechat",
        imageLinks: [
          {
            name: "livechat",
            url:
              "https://livechat.support.samsung.com/Customer_mys/CustomerChat_mys.aspx?roomNo=29352510&amp;encrypt=DA7538FE760DCE56801E33CA5AC8840F",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "unbox-02_04_mb",
        image2: "unbox-mb-02_04_mb",
        title: "Remote Service",
      }, // {
      //   no: "05",
      //   head: "-",
      //   text: "-",
      //   image: "unbox-02_05",
      //   image2: "unbox-mb-02_05",
      //   title: "Visual Support",
      // },
      // {
      //   no: "05",
      //   head: "-",
      //   text: "-",
      //   image: "unbox-02_06",
      //   image2: "unbox-mb-02_05",
      //   title: "Private Demo Service",
      //   condition:
      //     "1. ลูกค้าสามารถติดต่อขอใช้บริการได้ภายใน 90 วัน นับจากวันที่ลูกค้าซื้อสินค้า โดยอ้างอิงจากใบเสร็จ <br> 2.ลูกค้าต้องเตรียมหลักฐานประกอบการรับบริการ เช่น ใบเสร็จของสินค้าหรือใบรับของจากร้านค้าที่ซื้อมา โดยมีรายละเอียดของผู้ซื้อ ผู้ขายรวมถึงรุ่นของสินค้าที่ซื้อมาอย่างชัดเจน <br> 3.บริการสาธิตการใช้งานผลิตภัณฑ์สำหรับตู้เย็นชนิด Family Hub และ AirDresser ในรุ่นที่กำหนด ให้บริการเฉพาะพื้นที่กรุงเทพฯ และปริมณฑลเท่านั้น",
      //   modalName: "md-1",
      //   modalTitle: "Test by arm",
      //   modalType: "search",
      //   modal: [
      //     {
      //       product: 1,
      //       category: 1,
      //       model: 1,
      //     },
      //     {
      //       product: 2,
      //       category: 1,
      //       model: 1,
      //     },
      //     {
      //       product: 1,
      //       category: 1,
      //       model: 1,
      //     },
      //   ],
      // },
      // {
      //   no: "05",
      //   head: "-",
      //   text: "-",
      //   image: "unbox-02_07",
      //   image2: "unbox-mb-02_05",
      //   title: "e-Demo Service",
      // },
    ],
    Repair: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-03_01",
        image2: "unbox-mb-03_01",
        title: "พบช่องทางที่หลากหลายในการแจ้งซ่อมผลิตภัณฑ์",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "unbox-03_02",
        image2: "unbox-mb-03_02",
        title: "1282 Call Center",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "unbox-03_03",
        image2: "unbox-mb-03_03",
        title: "At Your Service",
        mapName: "atservice",
        imageLinks: [
          {
            name: "atservice",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "unbox-03_04",
        image2: "unbox-mb-03_04",
        title: "Samsung Members",
        condition: "แอปพลิเคชันนี้รองรับเฉพาะโทรศัพท์ซัมซุงเท่านั้น",
      },
      {
        no: "05",
        head: "-",
        text: "-",
        image: "unbox-03_05",
        image2: "unbox-mb-03_05",
        title: "Live Chat",
      }, // {
      //   no: "05",
      //   head: "-",
      //   text: "-",
      //   image: "unbox-03_06",
      //   image2: "unbox-mb-02_06",
      //   title: "For Home appliance",
      //   condition:
      //     "บริการนี้สำหรับเครื่องใช้ไฟฟ้าประเภทตู้เย็น เครื่องซักผ้า เครื่องปรับอากาศ และทีวีขนาด 32 นิ้วขึ้นไปเท่านั้น ",
      // },
    ],
    Extra: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-04_02",
        image2: "unbox-mb-04_02",
        title: "Extra benefit ",
        condition: "\u0E15\u0E23\u0E27\u0E08\u0E2A\u0E2D\u0E1A\u0E40\u0E07\u0E37\u0E48\u0E2D\u0E19\u0E44\u0E02\u0E40\u0E1E\u0E34\u0E48\u0E21\u0E40\u0E15\u0E34\u0E21\u0E17\u0E35\u0E48 ".concat(
          tagA("https://www.samsung.com/th/butler"),
          " "
        ),
        mapName: "extra",
        imageLinks: [
          {
            name: "extra",
            url: "https://www.samsung.com/th/butler/",
          },
        ],
      },
    ],
  },
  tv: {
    Unbox: [
      {
        no: "01",
        head: "-",
        text: "-",
        image: "unbox-01_01",
        image2: "unbox-mb-01_01",
        title: "แกะกล่องเครื่องใหม่",
      },
      {
        no: "02",
        head: "-",
        text: "-",
        image: "unbox-01_02",
        image2: "unbox-mb-01_02",
        title: "At Your Service",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง At Your Service เพื่อเป็นข้อมูล ประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "serviceat",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "03",
        head: "-",
        text: "-",
        image: "unbox-01_03",
        image2: "unbox-mb-01_03",
        title: "Samsung Members",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      },
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-01_04",
        image2: "unbox-mb-01_04",
        url: "serice-btn",
        title: "Extended Warranty",
        mapName: "warranty",
        condition:
          "1. ลูกค้าต้องลงทะเบียนผ่านเว็บไซต์ SSTHWARRANTY  เพื่อเอาประกันเท่านั้น <br> 2. ระยะเวลาที่ขยายการรับประกันสินค้า ขึ้นอยู่กับรุ่นของสินค้าและโปรโมชั่นขณะนั้น <br> 3. การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "warranty",
            url: "https://www.ssthwarranty.com/warranty",
          },
        ],
      },
    ],
    Inquiry: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-02_01",
        image2: "unbox-mb-02_01",
        title: "เครื่องใหม่ ยังไม่ชิน / ต้องการแก้ไขปัญหาเริ่มต้น? ",
      },
      ((_ref = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref, "image", "unbox-02_02"),
      _defineProperty(_ref, "image2", "unbox-mb-02_02"),
      _defineProperty(_ref, "title", "1282 Call Center"),
      _ref),
      ((_ref2 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref2, "image", "unbox-02_03"),
      _defineProperty(_ref2, "image2", "unbox-mb-02_03"),
      _defineProperty(_ref2, "title", "Live Chat"),
      _defineProperty(_ref2, "mapName", "livechat"),
      _defineProperty(_ref2, "imageLinks", [
        {
          name: "livechat",
          url:
            "https://livechat.support.samsung.com/Customer_mys/CustomerChat_mys.aspx?roomNo=29352510&amp;encrypt=DA7538FE760DCE56801E33CA5AC8840F",
        },
      ]),
      _ref2),
      ((_ref3 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref3, "image", "unbox-02_04"),
      _defineProperty(_ref3, "image2", "unbox-mb-02_04"),
      _defineProperty(_ref3, "title", "Remote Service"),
      _ref3),
      ((_ref4 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref4, "image", "unbox-02_05"),
      _defineProperty(_ref4, "image2", "unbox-mb-02_visual"),
      _defineProperty(_ref4, "title", "Visual Support"),
      _ref4),
      ((_ref5 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref5, "image", "unbox-02_06"),
      _defineProperty(_ref5, "image2", "unbox-mb-02_05"),
      _defineProperty(_ref5, "title", "Private Demo Service"),
      _defineProperty(
        _ref5,
        "condition",
        "1. ลูกค้าสามารถติดต่อขอใช้บริการได้ภายใน 90 วัน นับจากวันที่ลูกค้าซื้อสินค้า โดยอ้างอิงจากใบเสร็จ <br> 2.ลูกค้าต้องเตรียมหลักฐานประกอบการรับบริการ เช่น ใบเสร็จของสินค้าหรือใบรับของจากร้านค้าที่ซื้อมา โดยมีรายละเอียดของผู้ซื้อ ผู้ขายรวมถึงรุ่นของสินค้าที่ซื้อมาอย่างชัดเจน <br> 3.บริการสาธิตการใช้งานผลิตภัณฑ์สำหรับตู้เย็นชนิด Family Hub และ AirDresser ในรุ่นที่กำหนด ให้บริการเฉพาะพื้นที่กรุงเทพฯ และปริมณฑลเท่านั้น"
      ),
      _defineProperty(_ref5, "modalType", "search"),
      _defineProperty(_ref5, "modalName", "private-service"),
      _defineProperty(_ref5, "imageLinks", [
        {
          name: "modalversion1",
          modal: "modalx",
        },
      ]),
      _ref5),
    ],
    Repair: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-03_01",
        image2: "unbox-mb-03_01",
        title: "พบช่องทางที่หลากหลายในการแจ้งซ่อมผลิตภัณฑ์",
      },
      ((_ref6 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref6, "image", "unbox-03_02"),
      _defineProperty(_ref6, "image2", "unbox-mb-03_02"),
      _defineProperty(_ref6, "title", "1282 Call Center"),
      _ref6),
      ((_ref7 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref7, "image", "unbox-03_03"),
      _defineProperty(_ref7, "image2", "unbox-mb-03_03"),
      _defineProperty(_ref7, "title", "At Your Service"),
      _defineProperty(_ref7, "mapName", "atservice"),
      _defineProperty(_ref7, "imageLinks", [
        {
          name: "atservice",
          url: "https://www.samsung.com/th/support/your-service/main",
        },
      ]),
      _ref7),
      ((_ref8 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref8, "image", "unbox-03_04"),
      _defineProperty(_ref8, "image2", "unbox-mb-03_04"),
      _defineProperty(_ref8, "title", "Samsung Members"),
      _defineProperty(
        _ref8,
        "condition",
        "แอปพลิเคชันนี้รองรับเฉพาะโทรศัพท์ซัมซุงเท่านั้น"
      ),
      _ref8),
      ((_ref9 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref9, "image", "unbox-03_05"),
      _defineProperty(_ref9, "image2", "unbox-mb-03_05"),
      _defineProperty(_ref9, "title", "Live Chat"),
      _ref9),
      ((_ref10 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref10, "image", "unbox-03_06"),
      _defineProperty(_ref10, "image2", "unbox-mb-03_06"),
      _defineProperty(_ref10, "title", "For Home appliance"),
      _defineProperty(
        _ref10,
        "condition",
        "บริการนี้สำหรับเครื่องใช้ไฟฟ้าประเภทตู้เย็น เครื่องซักผ้า เครื่องปรับอากาศ และทีวีขนาด 32 นิ้วขึ้นไปเท่านั้น "
      ),
      _ref10),
    ],
    Extra: [
      ((_ref11 = {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-04_01",
        image2: "unbox-mb-04_01",
        title: " ",
      }),
      _defineProperty(_ref11, "title", "Extra benefit "),
      _defineProperty(
        _ref11,
        "condition",
        "เมื่อซื้อ Samsung Smart TV รุ่นปี 2020 และลงทะเบียนรับสิทธิ์ระหว่างวันที่ 1 ส.ค. 63 – 31 ต.ค. 63"
      ),
      _ref11),
    ],
  },
  wm: {
    Unbox: [
      {
        no: "01",
        head: "-",
        text: "-",
        image: "unbox-01_01",
        image2: "unbox-mb-01_01",
        title: "แกะกล่องเครื่องใหม่",
      },
      {
        no: "02",
        head: "-",
        text: "-",
        image: "unbox-01_02",
        image2: "unbox-mb-01_02",
        title: "At Your Service",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง At Your Service เพื่อเป็นข้อมูล ประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "serviceat",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "03",
        head: "-",
        text: "-",
        image: "unbox-01_03",
        image2: "unbox-mb-01_03",
        title: "Samsung Members",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      },
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-01_04",
        image2: "unbox-mb-01_04",
        url: "serice-btn",
        title: "Extended Warranty",
        mapName: "warranty",
        condition:
          "1. ลูกค้าต้องลงทะเบียนผ่านเว็บไซต์ SSTHWARRANTY  เพื่อเอาประกันเท่านั้น <br> 2. ระยะเวลาที่ขยายการรับประกันสินค้า ขึ้นอยู่กับรุ่นของสินค้าและโปรโมชั่นขณะนั้น <br> 3. การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "warranty",
            url: "https://www.ssthwarranty.com/warranty",
          },
        ],
      },
    ],
    Inquiry: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-02_01",
        image2: "unbox-mb-02_01",
        title: "เครื่องใหม่ ยังไม่ชิน / ต้องการแก้ไขปัญหาเริ่มต้น? ",
      },
      ((_ref12 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref12, "image", "unbox-02_02"),
      _defineProperty(_ref12, "image2", "unbox-mb-02_02"),
      _defineProperty(_ref12, "title", "1282 Call Center"),
      _ref12),
      ((_ref13 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref13, "image", "unbox-02_03"),
      _defineProperty(_ref13, "image2", "unbox-mb-02_03"),
      _defineProperty(_ref13, "title", "Live Chat"),
      _defineProperty(_ref13, "mapName", "livechat"),
      _defineProperty(_ref13, "imageLinks", [
        {
          name: "livechat",
          url:
            "https://livechat.support.samsung.com/Customer_mys/CustomerChat_mys.aspx?roomNo=29352510&amp;encrypt=DA7538FE760DCE56801E33CA5AC8840F",
        },
      ]),
      _ref13),
      ((_ref14 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref14, "image", "unbox-02_05"),
      _defineProperty(_ref14, "image2", "unbox-mb-02_visual"),
      _defineProperty(_ref14, "title", "Visual Support"),
      _ref14),
      {
        no: "05",
        head: "-",
        text: "-",
        image: "unbox-02_07",
        image2: "unbox-mb-02_06",
        title: "e-Demo Service",
        mapName: "digital-e-domo-service",
        imageLinks: [
          {
            name: "modalversion",
            modal: "modalx",
          },
          {
            name: "modalversion2",
            modal: "modalx2",
          },
        ],
        imageLinks2: [
          {
            name: "serviceat2",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
    ],
    Repair: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-03_01",
        image2: "unbox-mb-03_01",
        title: "พบช่องทางที่หลากหลายในการแจ้งซ่อมผลิตภัณฑ์",
      },
      ((_ref15 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref15, "image", "unbox-03_02"),
      _defineProperty(_ref15, "image2", "unbox-mb-03_02"),
      _defineProperty(_ref15, "title", "1282 Call Center"),
      _ref15),
      ((_ref16 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref16, "image", "unbox-03_03"),
      _defineProperty(_ref16, "image2", "unbox-mb-03_03"),
      _defineProperty(_ref16, "title", "At Your Service"),
      _defineProperty(_ref16, "mapName", "atservice"),
      _defineProperty(_ref16, "imageLinks", [
        {
          name: "atservice",
          url: "https://www.samsung.com/th/support/your-service/main",
        },
      ]),
      _ref16),
      ((_ref17 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref17, "image", "unbox-03_04"),
      _defineProperty(_ref17, "image2", "unbox-mb-03_04"),
      _defineProperty(_ref17, "title", "Samsung Members"),
      _defineProperty(
        _ref17,
        "condition",
        "แอปพลิเคชันนี้รองรับเฉพาะโทรศัพท์ซัมซุงเท่านั้น"
      ),
      _ref17),
      ((_ref18 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref18, "image", "unbox-03_05"),
      _defineProperty(_ref18, "image2", "unbox-mb-03_05"),
      _defineProperty(_ref18, "title", "Live Chat"),
      _ref18),
      ((_ref19 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref19, "image", "unbox-03_06"),
      _defineProperty(_ref19, "image2", "unbox-mb-03_06"),
      _defineProperty(_ref19, "title", "For Home appliance"),
      _defineProperty(
        _ref19,
        "condition",
        "บริการนี้สำหรับเครื่องใช้ไฟฟ้าประเภทตู้เย็น เครื่องซักผ้า เครื่องปรับอากาศ และทีวีขนาด 32 นิ้วขึ้นไปเท่านั้น "
      ),
      _ref19),
    ],
  },
  cloth: {
    Unbox: [
      {
        no: "01",
        head: "-",
        text: "-",
        image: "unbox-01_01",
        image2: "unbox-mb-01_01",
        title: "แกะกล่องเครื่องใหม่",
      },
      {
        no: "02",
        head: "-",
        text: "-",
        image: "unbox-01_02",
        image2: "unbox-mb-01_02",
        title: "At Your Service",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง At Your Service เพื่อเป็นข้อมูล ประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "serviceat",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "03",
        head: "-",
        text: "-",
        image: "unbox-01_03",
        image2: "unbox-mb-01_03",
        title: "Samsung Members",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      }, // {
      //   no: "04",
      //   head: "-",
      //   text: "-",
      //   image: "unbox-01_04",
      //   image2: "unbox-mb-01_04",
      //   url: "serice-btn",
      //   title: "Extended Warranty",
      //   condition:
      //     "1. ลูกค้าต้องลงทะเบียนผ่านเว็บไซต์ SSTHWARRANTY  เพื่อเอาประกันเท่านั้น <br> 2. ระยะเวลาที่ขยายการรับประกันสินค้า ขึ้นอยู่กับรุ่นของสินค้าและโปรโมชั่นขณะนั้น <br> 3. การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      // },
    ],
    Inquiry: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-02_01",
        image2: "unbox-mb-02_01",
        title: "เครื่องใหม่ ยังไม่ชิน / ต้องการแก้ไขปัญหาเริ่มต้น? ",
      },
      ((_ref20 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref20, "image", "unbox-02_02"),
      _defineProperty(_ref20, "image2", "unbox-mb-02_02"),
      _defineProperty(_ref20, "title", "1282 Call Center"),
      _ref20),
      ((_ref21 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref21, "image", "unbox-02_03"),
      _defineProperty(_ref21, "image2", "unbox-mb-02_03"),
      _defineProperty(_ref21, "title", "Live Chat"),
      _defineProperty(_ref21, "mapName", "livechat"),
      _defineProperty(_ref21, "imageLinks", [
        {
          name: "livechat",
          url:
            "https://livechat.support.samsung.com/Customer_mys/CustomerChat_mys.aspx?roomNo=29352510&amp;encrypt=DA7538FE760DCE56801E33CA5AC8840F",
        },
      ]),
      _ref21),
      ((_ref22 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref22, "image", "unbox-02_05"),
      _defineProperty(_ref22, "image2", "unbox-mb-02_visual"),
      _defineProperty(_ref22, "title", "Visual Support"),
      _ref22),
      ((_ref23 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref23, "image", "unbox-02_06"),
      _defineProperty(_ref23, "image2", "unbox-mb-02_05"),
      _defineProperty(_ref23, "title", "Private Demo Service"),
      _defineProperty(_ref23, "modalName", "e-demo"),
      _defineProperty(
        _ref23,
        "condition",
        "1. ลูกค้าสามารถติดต่อขอใช้บริการได้ภายใน 90 วัน นับจากวันที่ลูกค้าซื้อสินค้า โดยอ้างอิงจากใบเสร็จ <br> 2.ลูกค้าต้องเตรียมหลักฐานประกอบการรับบริการ เช่น ใบเสร็จของสินค้าหรือใบรับของจากร้านค้าที่ซื้อมา โดยมีรายละเอียดของผู้ซื้อ ผู้ขายรวมถึงรุ่นของสินค้าที่ซื้อมาอย่างชัดเจน <br> 3.บริการสาธิตการใช้งานผลิตภัณฑ์สำหรับตู้เย็นชนิด Family Hub และ AirDresser ในรุ่นที่กำหนด ให้บริการเฉพาะพื้นที่กรุงเทพฯ และปริมณฑลเท่านั้น"
      ),
      _defineProperty(_ref23, "modalType", "search"),
      _defineProperty(_ref23, "imageLinks", [
        {
          name: "modalversion1",
          url: "https://www.samsung.com/th/support/your-service/main",
        },
      ]),
      _ref23),
      {
        no: "05",
        head: "-",
        text: "-",
        image: "unbox-02_07",
        image2: "unbox-mb-02_06",
        title: "e-Demo Service",
        imageLinks: [
          {
            name: "modalversion",
            modal: "modalx",
          },
          {
            name: "modalversion2",
            modal: "modalx2",
          },
        ],
        imageLinks2: [
          {
            name: "serviceat2",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
    ],
    Repair: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-03_01",
        image2: "unbox-mb-03_01",
        title: "พบช่องทางที่หลากหลายในการแจ้งซ่อมผลิตภัณฑ์",
      },
      ((_ref24 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref24, "image", "unbox-03_02"),
      _defineProperty(_ref24, "image2", "unbox-mb-03_02"),
      _defineProperty(_ref24, "title", "1282 Call Center"),
      _ref24),
      ((_ref25 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref25, "image", "unbox-03_03"),
      _defineProperty(_ref25, "image2", "unbox-mb-03_03"),
      _defineProperty(_ref25, "title", "At Your Service"),
      _defineProperty(_ref25, "mapName", "atservice"),
      _defineProperty(_ref25, "imageLinks", [
        {
          name: "atservice",
          url: "https://www.samsung.com/th/support/your-service/main",
        },
      ]),
      _ref25),
      ((_ref26 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref26, "image", "unbox-03_04"),
      _defineProperty(_ref26, "image2", "unbox-mb-03_04"),
      _defineProperty(_ref26, "title", "Samsung Members"),
      _defineProperty(
        _ref26,
        "condition",
        "แอปพลิเคชันนี้รองรับเฉพาะโทรศัพท์ซัมซุงเท่านั้น"
      ),
      _ref26),
      ((_ref27 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref27, "image", "unbox-03_05"),
      _defineProperty(_ref27, "image2", "unbox-mb-03_05"),
      _defineProperty(_ref27, "title", "Live Chat"),
      _ref27),
      ((_ref28 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref28, "image", "unbox-03_06"),
      _defineProperty(_ref28, "image2", "unbox-mb-03_06"),
      _defineProperty(_ref28, "title", "For Home appliance"),
      _defineProperty(
        _ref28,
        "condition",
        "บริการนี้สำหรับเครื่องใช้ไฟฟ้าประเภทตู้เย็น เครื่องซักผ้า เครื่องปรับอากาศ และทีวีขนาด 32 นิ้วขึ้นไปเท่านั้น "
      ),
      _ref28),
    ],
  },
  refrig: {
    Unbox: [
      {
        no: "01",
        head: "-",
        text: "-",
        image: "unbox-01_01",
        image2: "unbox-mb-01_01",
        title: "แกะกล่องเครื่องใหม่",
      },
      {
        no: "02",
        head: "-",
        text: "-",
        image: "unbox-01_02",
        image2: "unbox-mb-01_02",
        title: "At Your Service",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง At Your Service เพื่อเป็นข้อมูล ประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "serviceat",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "03",
        head: "-",
        text: "-",
        image: "unbox-01_03",
        image2: "unbox-mb-01_03",
        title: "Samsung Members",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      },
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-01_04",
        image2: "unbox-mb-01_04",
        url: "serice-btn",
        title: "Extended Warranty",
        mapName: "warranty",
        condition:
          "1. ลูกค้าต้องลงทะเบียนผ่านเว็บไซต์ SSTHWARRANTY  เพื่อเอาประกันเท่านั้น <br> 2. ระยะเวลาที่ขยายการรับประกันสินค้า ขึ้นอยู่กับรุ่นของสินค้าและโปรโมชั่นขณะนั้น <br> 3. การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "warranty",
            url: "https://www.ssthwarranty.com/warranty",
          },
        ],
      },
    ],
    Inquiry: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-02_01",
        image2: "unbox-mb-02_01",
        title: "เครื่องใหม่ ยังไม่ชิน / ต้องการแก้ไขปัญหาเริ่มต้น? ",
      },
      ((_ref29 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref29, "image", "unbox-02_02"),
      _defineProperty(_ref29, "image2", "unbox-mb-02_02"),
      _defineProperty(_ref29, "title", "1282 Call Center"),
      _ref29),
      ((_ref30 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref30, "image", "unbox-02_03"),
      _defineProperty(_ref30, "image2", "unbox-mb-02_03"),
      _defineProperty(_ref30, "title", "Live Chat"),
      _defineProperty(_ref30, "mapName", "livechat"),
      _defineProperty(_ref30, "imageLinks", [
        {
          name: "livechat",
          url:
            "https://livechat.support.samsung.com/Customer_mys/CustomerChat_mys.aspx?roomNo=29352510&amp;encrypt=DA7538FE760DCE56801E33CA5AC8840F",
        },
      ]),
      _ref30),
      ((_ref31 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref31, "image", "unbox-02_05"),
      _defineProperty(_ref31, "image2", "unbox-mb-02_visual"),
      _defineProperty(_ref31, "title", "Visual Support"),
      _ref31),
      ((_ref32 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref32, "image", "unbox-02_06"),
      _defineProperty(_ref32, "image2", "unbox-mb-02_05"),
      _defineProperty(_ref32, "title", "Private Demo Service"),
      _defineProperty(
        _ref32,
        "condition",
        "1. ลูกค้าสามารถติดต่อขอใช้บริการได้ภายใน 90 วัน นับจากวันที่ลูกค้าซื้อสินค้า โดยอ้างอิงจากใบเสร็จ <br> 2.ลูกค้าต้องเตรียมหลักฐานประกอบการรับบริการ เช่น ใบเสร็จของสินค้าหรือใบรับของจากร้านค้าที่ซื้อมา โดยมีรายละเอียดของผู้ซื้อ ผู้ขายรวมถึงรุ่นของสินค้าที่ซื้อมาอย่างชัดเจน <br> 3.บริการสาธิตการใช้งานผลิตภัณฑ์สำหรับตู้เย็นชนิด Family Hub และ AirDresser ในรุ่นที่กำหนด ให้บริการเฉพาะพื้นที่กรุงเทพฯ และปริมณฑลเท่านั้น"
      ),
      _defineProperty(_ref32, "modalType", "search"),
      _defineProperty(_ref32, "imageLinks", [
        {
          name: "modalversion1",
          url: "https://www.samsung.com/th/support/your-service/main",
        },
      ]),
      _ref32),
      {
        no: "05",
        head: "-",
        text: "-",
        image: "unbox-02_07",
        image2: "unbox-mb-02_06",
        title: "e-Demo Service",
        imageLinks: [
          {
            name: "modalversion",
            modal: "modalx",
          },
          {
            name: "modalversion2",
            modal: "modalx2",
          },
        ],
        imageLinks2: [
          {
            name: "serviceat2",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
    ],
    Repair: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-03_01",
        image2: "unbox-mb-03_01",
        title: "พบช่องทางที่หลากหลายในการแจ้งซ่อมผลิตภัณฑ์",
      },
      ((_ref33 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref33, "image", "unbox-03_02"),
      _defineProperty(_ref33, "image2", "unbox-mb-03_02"),
      _defineProperty(_ref33, "title", "1282 Call Center"),
      _ref33),
      ((_ref34 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref34, "image", "unbox-03_03"),
      _defineProperty(_ref34, "image2", "unbox-mb-03_03"),
      _defineProperty(_ref34, "title", "At Your Service"),
      _defineProperty(_ref34, "mapName", "atservice"),
      _defineProperty(_ref34, "imageLinks", [
        {
          name: "atservice",
          url: "https://www.samsung.com/th/support/your-service/main",
        },
      ]),
      _ref34),
      ((_ref35 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref35, "image", "unbox-03_04"),
      _defineProperty(_ref35, "image2", "unbox-mb-03_04"),
      _defineProperty(_ref35, "title", "Samsung Members"),
      _defineProperty(
        _ref35,
        "condition",
        "แอปพลิเคชันนี้รองรับเฉพาะโทรศัพท์ซัมซุงเท่านั้น"
      ),
      _ref35),
      ((_ref36 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref36, "image", "unbox-03_05"),
      _defineProperty(_ref36, "image2", "unbox-mb-03_05"),
      _defineProperty(_ref36, "title", "Live Chat"),
      _ref36),
      ((_ref37 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref37, "image", "unbox-03_06"),
      _defineProperty(_ref37, "image2", "unbox-mb-03_06"),
      _defineProperty(_ref37, "title", "For Home appliance"),
      _defineProperty(
        _ref37,
        "condition",
        "บริการนี้สำหรับเครื่องใช้ไฟฟ้าประเภทตู้เย็น เครื่องซักผ้า เครื่องปรับอากาศ และทีวีขนาด 32 นิ้วขึ้นไปเท่านั้น "
      ),
      _ref37),
    ],
  },
  ac: {
    Unbox: [
      {
        no: "01",
        head: "-",
        text: "-",
        image: "unbox-01_01",
        image2: "unbox-mb-01_01",
        title: "แกะกล่องเครื่องใหม่",
      },
      {
        no: "02",
        head: "-",
        text: "-",
        image: "unbox-01_02",
        image2: "unbox-mb-01_02",
        title: "At Your Service",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง At Your Service เพื่อเป็นข้อมูล ประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "serviceat",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "03",
        head: "-",
        text: "-",
        image: "unbox-01_03",
        image2: "unbox-mb-01_03",
        title: "Samsung Members",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      }, // {
      //   no: "04",
      //   head: "-",
      //   text: "-",
      //   image: "unbox-01_04",
      //   image2: "unbox-mb-01_04",
      //   url: "serice-btn",
      //   title: "Extended Warranty",
      //   condition:
      //     "1. ลูกค้าต้องลงทะเบียนผ่านเว็บไซต์ SSTHWARRANTY  เพื่อเอาประกันเท่านั้น <br> 2. ระยะเวลาที่ขยายการรับประกันสินค้า ขึ้นอยู่กับรุ่นของสินค้าและโปรโมชั่นขณะนั้น <br> 3. การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      // },
    ],
    Inquiry: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-02_01",
        image2: "unbox-mb-02_01",
        title: "เครื่องใหม่ ยังไม่ชิน / ต้องการแก้ไขปัญหาเริ่มต้น? ",
      },
      ((_ref38 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref38, "image", "unbox-02_02"),
      _defineProperty(_ref38, "image2", "unbox-mb-02_02"),
      _defineProperty(_ref38, "title", "1282 Call Center"),
      _ref38),
      ((_ref39 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref39, "image", "unbox-02_03"),
      _defineProperty(_ref39, "image2", "unbox-mb-02_03"),
      _defineProperty(_ref39, "title", "Live Chat"),
      _defineProperty(_ref39, "mapName", "livechat"),
      _defineProperty(_ref39, "imageLinks", [
        {
          name: "livechat",
          url:
            "https://livechat.support.samsung.com/Customer_mys/CustomerChat_mys.aspx?roomNo=29352510&amp;encrypt=DA7538FE760DCE56801E33CA5AC8840F",
        },
      ]),
      _ref39),
      ((_ref40 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref40, "image", "unbox-02_05"),
      _defineProperty(_ref40, "image2", "unbox-mb-02_visual"),
      _defineProperty(_ref40, "title", "Visual Support"),
      _ref40),
      {
        no: "05",
        head: "-",
        text: "-",
        image: "unbox-02_07",
        image2: "unbox-mb-02_06",
        title: "e-Demo Service",
        imageLinks: [
          {
            name: "modalversion",
            modal: "modalx",
          },
          {
            name: "modalversion2",
            modal: "modalx2",
          },
        ],
        imageLinks2: [
          {
            name: "serviceat2",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
    ],
    Repair: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-03_01",
        image2: "unbox-mb-03_01",
        title: "พบช่องทางที่หลากหลายในการแจ้งซ่อมผลิตภัณฑ์",
      },
      ((_ref41 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref41, "image", "unbox-03_02"),
      _defineProperty(_ref41, "image2", "unbox-mb-03_02"),
      _defineProperty(_ref41, "title", "1282 Call Center"),
      _ref41),
      ((_ref42 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref42, "image", "unbox-03_03"),
      _defineProperty(_ref42, "image2", "unbox-mb-03_03"),
      _defineProperty(_ref42, "title", "At Your Service"),
      _defineProperty(_ref42, "mapName", "atservice"),
      _defineProperty(_ref42, "imageLinks", [
        {
          name: "atservice",
          url: "https://www.samsung.com/th/support/your-service/main",
        },
      ]),
      _ref42),
      ((_ref43 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref43, "image", "unbox-03_04"),
      _defineProperty(_ref43, "image2", "unbox-mb-03_04"),
      _defineProperty(_ref43, "title", "Samsung Members"),
      _defineProperty(
        _ref43,
        "condition",
        "แอปพลิเคชันนี้รองรับเฉพาะโทรศัพท์ซัมซุงเท่านั้น"
      ),
      _ref43),
      ((_ref44 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref44, "image", "unbox-03_05"),
      _defineProperty(_ref44, "image2", "unbox-mb-03_05"),
      _defineProperty(_ref44, "title", "Live Chat"),
      _ref44),
      ((_ref45 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref45, "image", "unbox-03_06"),
      _defineProperty(_ref45, "image2", "unbox-mb-03_06"),
      _defineProperty(_ref45, "title", "For Home appliance"),
      _defineProperty(
        _ref45,
        "condition",
        "บริการนี้สำหรับเครื่องใช้ไฟฟ้าประเภทตู้เย็น เครื่องซักผ้า เครื่องปรับอากาศ และทีวีขนาด 32 นิ้วขึ้นไปเท่านั้น "
      ),
      _ref45),
    ],
  },
  mc: {
    Unbox: [
      {
        no: "01",
        head: "-",
        text: "-",
        image: "unbox-01_01",
        image2: "unbox-mb-01_01",
        title: "แกะกล่องเครื่องใหม่",
      },
      {
        no: "02",
        head: "-",
        text: "-",
        image: "unbox-01_02",
        image2: "unbox-mb-01_02",
        title: "At Your Service",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง At Your Service เพื่อเป็นข้อมูล ประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "serviceat",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "03",
        head: "-",
        text: "-",
        image: "unbox-01_03",
        image2: "unbox-mb-01_03",
        title: "Samsung Members",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      }, // {
      //   no: "04",
      //   head: "-",
      //   text: "-",
      //   image: "unbox-01_04",
      //   image2: "unbox-mb-01_04",
      //   url: "serice-btn",
      //   title: "Extended Warranty",
      //   condition:
      //     "1. ลูกค้าต้องลงทะเบียนผ่านเว็บไซต์ SSTHWARRANTY  เพื่อเอาประกันเท่านั้น <br> 2. ระยะเวลาที่ขยายการรับประกันสินค้า ขึ้นอยู่กับรุ่นของสินค้าและโปรโมชั่นขณะนั้น <br> 3. การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      // },
    ],
    Inquiry: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-02_01",
        image2: "unbox-mb-02_01",
        title: "เครื่องใหม่ ยังไม่ชิน / ต้องการแก้ไขปัญหาเริ่มต้น? ",
      },
      ((_ref46 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref46, "image", "unbox-02_02"),
      _defineProperty(_ref46, "image2", "unbox-mb-02_02"),
      _defineProperty(_ref46, "title", "1282 Call Center"),
      _ref46),
      ((_ref47 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref47, "image", "unbox-02_03"),
      _defineProperty(_ref47, "image2", "unbox-mb-02_03"),
      _defineProperty(_ref47, "title", "Live Chat"),
      _defineProperty(_ref47, "mapName", "livechat"),
      _defineProperty(_ref47, "imageLinks", [
        {
          name: "livechat",
          url:
            "https://livechat.support.samsung.com/Customer_mys/CustomerChat_mys.aspx?roomNo=29352510&amp;encrypt=DA7538FE760DCE56801E33CA5AC8840F",
        },
      ]),
      _ref47),
      ((_ref48 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref48, "image", "unbox-02_05"),
      _defineProperty(_ref48, "image2", "unbox-mb-02_visual"),
      _defineProperty(_ref48, "title", "Visual Support"),
      _ref48),
    ],
    Repair: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-03_01",
        image2: "unbox-mb-03_01",
        title: "พบช่องทางที่หลากหลายในการแจ้งซ่อมผลิตภัณฑ์",
      },
      ((_ref49 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref49, "image", "unbox-03_02"),
      _defineProperty(_ref49, "image2", "unbox-mb-03_02"),
      _defineProperty(_ref49, "title", "1282 Call Center"),
      _ref49),
      ((_ref50 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref50, "image", "unbox-03_03"),
      _defineProperty(_ref50, "image2", "unbox-mb-03_03"),
      _defineProperty(_ref50, "title", "At Your Service"),
      _defineProperty(_ref50, "mapName", "atservice"),
      _defineProperty(_ref50, "imageLinks", [
        {
          name: "atservice",
          url: "https://www.samsung.com/th/support/your-service/main",
        },
      ]),
      _ref50),
      ((_ref51 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref51, "image", "unbox-03_04"),
      _defineProperty(_ref51, "image2", "unbox-mb-03_04"),
      _defineProperty(_ref51, "title", "Samsung Members"),
      _defineProperty(
        _ref51,
        "condition",
        "แอปพลิเคชันนี้รองรับเฉพาะโทรศัพท์ซัมซุงเท่านั้น"
      ),
      _ref51),
      ((_ref52 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref52, "image", "unbox-03_05"),
      _defineProperty(_ref52, "image2", "unbox-mb-03_05"),
      _defineProperty(_ref52, "title", "Live Chat"),
      _ref52),
    ],
  },
  robot: {
    Unbox: [
      {
        no: "01",
        head: "-",
        text: "-",
        image: "unbox-01_01",
        image2: "unbox-mb-01_01",
        title: "แกะกล่องเครื่องใหม่",
      },
      {
        no: "02",
        head: "-",
        text: "-",
        image: "unbox-01_02",
        image2: "unbox-mb-01_02",
        title: "At Your Service",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง At Your Service เพื่อเป็นข้อมูล ประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
        imageLinks: [
          {
            name: "serviceat",
            url: "https://www.samsung.com/th/support/your-service/main",
          },
        ],
      },
      {
        no: "03",
        head: "-",
        text: "-",
        image: "unbox-01_03",
        image2: "unbox-mb-01_03",
        title: "Samsung Members",
        condition:
          "การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      }, // {
      //   no: "04",
      //   head: "-",
      //   text: "-",
      //   image: "unbox-01_04",
      //   image2: "unbox-mb-01_04",
      //   url: "serice-btn",
      //   title: "Extended Warranty",
      //   condition:
      //     "1. ลูกค้าต้องลงทะเบียนผ่านเว็บไซต์ SSTHWARRANTY  เพื่อเอาประกันเท่านั้น <br> 2. ระยะเวลาที่ขยายการรับประกันสินค้า ขึ้นอยู่กับรุ่นของสินค้าและโปรโมชั่นขณะนั้น <br> 3. การลงทะเบียนผลิตภัณฑ์ผ่านช่องทาง Samsung Members เพื่อเป็นข้อมูลประกันสินค้าทั่วไป ไม่รวมถึงการขยายระยะรับประกันเพิ่มเติม",
      // },
    ],
    Inquiry: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-02_01",
        image2: "unbox-mb-02_01",
        title: "เครื่องใหม่ ยังไม่ชิน / ต้องการแก้ไขปัญหาเริ่มต้น? ",
      },
      ((_ref53 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref53, "image", "unbox-02_02"),
      _defineProperty(_ref53, "image2", "unbox-mb-02_02"),
      _defineProperty(_ref53, "title", "1282 Call Center"),
      _ref53),
      ((_ref54 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref54, "image", "unbox-02_03"),
      _defineProperty(_ref54, "image2", "unbox-mb-02_03"),
      _defineProperty(_ref54, "title", "Live Chat"),
      _defineProperty(_ref54, "mapName", "livechat"),
      _defineProperty(_ref54, "imageLinks", [
        {
          name: "livechat",
          url:
            "https://livechat.support.samsung.com/Customer_mys/CustomerChat_mys.aspx?roomNo=29352510&amp;encrypt=DA7538FE760DCE56801E33CA5AC8840F",
        },
      ]),
      _ref54),
      ((_ref55 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref55, "image", "unbox-02_05"),
      _defineProperty(_ref55, "image2", "unbox-mb-02_visual"),
      _defineProperty(_ref55, "title", "Visual Support"),
      _ref55),
    ],
    Repair: [
      {
        no: "04",
        head: "-",
        text: "-",
        image: "unbox-03_01",
        image2: "unbox-mb-03_01",
        title: "พบช่องทางที่หลากหลายในการแจ้งซ่อมผลิตภัณฑ์",
      },
      ((_ref56 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref56, "image", "unbox-03_02"),
      _defineProperty(_ref56, "image2", "unbox-mb-03_02"),
      _defineProperty(_ref56, "title", "1282 Call Center"),
      _ref56),
      ((_ref57 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref57, "image", "unbox-03_03"),
      _defineProperty(_ref57, "image2", "unbox-mb-03_03"),
      _defineProperty(_ref57, "title", "At Your Service"),
      _defineProperty(_ref57, "mapName", "atservice"),
      _defineProperty(_ref57, "imageLinks", [
        {
          name: "atservice",
          url: "https://www.samsung.com/th/support/your-service/main",
        },
      ]),
      _ref57),
      ((_ref58 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref58, "image", "unbox-03_04"),
      _defineProperty(_ref58, "image2", "unbox-mb-03_04"),
      _defineProperty(_ref58, "title", "Samsung Members"),
      _defineProperty(
        _ref58,
        "condition",
        "แอปพลิเคชันนี้รองรับเฉพาะโทรศัพท์ซัมซุงเท่านั้น"
      ),
      _ref58),
      ((_ref59 = {
        no: "05",
        head: "-",
        text: "-",
        image: "slide",
      }),
      _defineProperty(_ref59, "image", "unbox-03_05"),
      _defineProperty(_ref59, "image2", "unbox-mb-03_05"),
      _defineProperty(_ref59, "title", "Live Chat"),
      _ref59),
    ],
  },
};
var mainActive = "mobile";
var subActive = "Unbox";
var No = 1;

var eventSlider = function eventSlider(current) {
  var totalSlides = items[mainActive][subActive].length;
  var btnNext = ".swiper-button-next";
  var btnPrev = ".swiper-button-prev";
  No = current.activeIndex + 1;

  if (No >= totalSlides) {
    $(btnNext).hide();
  } else {
    $(btnNext).show();
  }

  if (No == 1) {
    $(btnPrev).hide();
  } else {
    $(btnPrev).show();
  }

  $("#".concat(subActive, " #currentslide")).text(No);
  var find = items[mainActive][subActive][current.activeIndex];
  var title = find.title ? find.title : "no-title";
  var condition = find.condition ? find.condition : false;

  if (condition) {
    $(".tool-tip-ex .tooltiptext").html(condition);
    $(".tool-tip-ex").show();
  } else {
    $(".tool-tip-ex").hide();
  }

  $("#".concat(subActive, " #totalslide")).html(
    ""
      .concat(totalSlides, ' <span class="title-page">')
      .concat(title, " </span>")
  );
};

var callSlider = function callSlider() {
  var swiper2 = new Swiper(".s2", {
    slidesPerView: "auto",
    // spaceBetween: 30,
    loop: false,
    // width: 1166,
    // setWrapperSize: true,
    // mousewheel: {
    //   invert: true,
    // },
    simulateTouch: false,
    centeredSlides: true,
    autoHeight: true,
    pagination: {
      el: ".swiper-pagination,.swiper-paginations",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    on: {
      init: function init(current) {
        console.log("#".concat(subActive, " #currentslide"));
        // eventSlider(current);
        // console.log(swiper2);
      },
      slideChange: function slideChange(current) {
        // console.log("current", current );
        console.log("swiper change");
        console.log(swiper2);
        // eventSlider(current);
      },
    },
    breakpoints: {
      768: {
        slidesPerView: "auto",
        // spaceBetween: 100,
        autoHeight: true,
      },
    },
  });
};

var renderAImageLink = function renderAImageLink(links) {
  var html = "";

  if (links && links.length) {
    links.forEach(function (el) {
      var textModalAttr = "";

      if (el.modal) {
        textModalAttr = 'data-modal="'.concat(el.modal, '"');
      }

      html += '<a class="link-ss--'
        .concat(el.name, '" ')
        .concat(textModalAttr, ' href="')
        .concat(el.modal ? "javascript:void(0)" : el.url, '" ')
        .concat(el.modal ? "" : 'target = "_blank"', "></a>");
    });
  }

  return html;
};

var renderSlider = function renderSlider(name) {
  var templateHtml = function templateHtml(data) {
    var textModalAttr = "";

    if (data.modal) {
      textModalAttr = 'data-modal="'.concat(data.modalName, '"');
    } // if (subActive == 'Unbox' || subActive == 'Inquiry' ){

    return ' \n        <div class="swiper-slide slide--01" >\n           <div class="slide--item">\n           \n             <div class="image-service">\n              <button class="outline" '
      .concat(textModalAttr, '>\n               <img src="images/')
      .concat(data.image, ".png\" usemap='#")
      .concat(
        data.mapName,
        '\' width="100%" class="dt-show"/>\n               \n               <img src="images/'
      )
      .concat(data.image2, ".png\" usemap='#")
      .concat(
        data.mapName,
        '-m\' alt="" width="100%" class="mb-show"/>\n               </button>\n               '
      )
      .concat(renderAImageLink(data.imageLinks), "\n               ")
      .concat(renderAImageLink(data.imageLinks2), "\n               ")
      .concat(
        renderAImageLink(data.imageLinksmodal2),
        "\n              </div>\n            </div>\n        </div>\n        "
      ); // }
    // else if (subActive == "Extra") {
    //   return `
    //   <div class="swiper-slide slide--01">
    //      <div class="slide--item">
    //          <img src="images/${data.image}.png" alt=""  width="100%" class="dt-show"/>
    //          <img src="images/${data.image2}.png" alt=""  width="100%" class="mb-show"/>
    //       </div>
    //   </div>
    //   `;
    // }
  };

  var render = "";
  items[name][subActive].forEach(function (el) {
    render += templateHtml(el);
  });
  return render;
};

var modalActive = "";

var setModalLength = function setModalLength(n) {
  $("#modal-html .m--result span").text(n);
};

var init = {
  menuClick: function menuClick() {
    $(".button-menu--click").click(function () {
      var dataSlider = $(this).data("slider");
      mainActive = dataSlider; //$(".wrap-s2 .swiper-wrapper").empty();

      var content = $("#".concat(subActive, " .swiper-wrapper"));
      var activeArray = items[mainActive];
      $(".tablinks").hide();

      for (var key in activeArray) {
        $("[data-tabname='".concat(key, "']")).show();
      }

      content.html(renderSlider(mainActive)); //  let content2 = $(`#${subActive} .titleslide`);
      //  content2.html(rendertitle(mainActive));
      // console.log(renderSlider(mainActive));
      // console.log(dataSlider);
      // console.log(subActive);

      callSlider();
    });
  },
  tabClick: function tabClick() {
    $(".service--tab .tablinks").click(function () {
      var dataTabname = $(this).data("tabname");
      subActive = dataTabname;
      var content = $("#".concat(subActive, " .swiper-wrapper"));
      content.html(renderSlider(mainActive));
      console.log(subActive); // let content2 = $(`#${subActive} .titleslide`);
      // content2.html(rendertitle(mainActive));

      callSlider();
    });
  },
  modal: {
    click: function click() {
      $("[data-modal]").live("click", function () {
        modalActive = $(this).data("modal");
        var itemActive = items[mainActive][subActive];

        var findModalActiveByName = _.find(modalItems, function (el) {
          return el.modalName == modalActive;
        });

        var datas = findModalActiveByName.modal; // let modalTitle = findModalActiveByName.modalTitle

        var resultData = _.map(datas, function (el) {
          var product_text = _.find(products, function (p) {
            return p.key == el.product;
          });

          var category_text = _.find(categories, function (p) {
            return p.key == el.category;
          });

          var model_text = _.find(models, function (p) {
            return p.key == el.model;
          });

          return {
            product: product_text ? product_text : el.product,
            category: category_text ? category_text : el.category,
            model: model_text ? model_text : el.model,
          };
        });

        console.log("map", {
          resultData: resultData,
          itemActive: itemActive,
          modalActive: modalActive,
        });
        $("#s-modal .modal-content").css("display", "block");
        $("#s-modal").css("display", "block");
        var renderModal = renderModalSearch(resultData);
        $("#s-modal .modal-content #modal-html .table-list").html(renderModal); // $(`#s-modal .modal-content #modal-html h1.modal-title`).html(modalTitle)
      });
    },
    close: function close() {
      $(".close").live("click", function () {
        $("#s-modal").css("display", "none");
      });
    },
    search: function search() {
      $(".myInputModal").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function () {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });
        var countSearch = $("#myTable").children("tr:visible").length;
        setModalLength(countSearch);
      });
    },
  },
};
$(window).ready(function () {
  init.menuClick();
  init.tabClick();
  init.modal.click();
  init.modal.close();
  init.modal.search();
});

var renderTable = function renderTable(header, bodyCol, bodyDatas) {
  var head =
    '\n  <thead class="head-d">\n  <tr>\n  $COLUMN\n  </tr>\n  </thead>\n  ';
  var headColumn = "";
  header.forEach(function (el) {
    headColumn += "<td>".concat(el, "</td>");
  });
  head = head.replace("$COLUMN", headColumn);
  var body = '\n  <tbody id="myTable">\n  $COLUMN\n  </tbody>\n  ';
  var bodyColumn = "";
  bodyDatas.forEach(function (el) {
    bodyColumn += "<tr>";
    bodyCol.forEach(function (ibody) {
      var x = el[ibody];
      bodyColumn += "<td>".concat(x.name, "</td>");
    });
    bodyColumn += "</tr>";
  });
  body = body.replace("$COLUMN", bodyColumn);
  var html = "\n  <table >\n  "
    .concat(head, "\n  ")
    .concat(body, "\n  </table>\n  ");
  return html;
};

var renderModalSearch = function renderModalSearch(resultData) {
  var table = renderTable(
    ["Products", "Category", "Models"],
    ["product", "category", "model"],
    resultData
  );
  setModalLength(resultData.length);
  var html = "\n  ".concat(table, "\n  ");
  return html;
};
